<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\PosLog
 *
 * @property integer $id
 * @property string $event
 * @property float $transaksi
 * @property float $pos_cash
 * @property integer $merchant_id
 * @property integer $cashier_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read User $cashier
 * @property-read User $merchant
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PosLog whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PosLog whereEvent($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PosLog whereTransaksi($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PosLog wherePosCash($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PosLog whereMerchantId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PosLog whereCashierId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PosLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PosLog whereUpdatedAt($value)
 */
class PosLog extends Model
{
    public function cashier()
    {
        return $this->belongsTo(User::class);
    }

    public function merchant()
    {
        return $this->belongsTo(User::class);
    }
}
