<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\Color
 *
 * @property integer $id
 * @property string $text
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Product[] $products
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Color whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Color whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Color whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Color whereUpdatedAt($value)
 */
class Color extends Model
{
    protected $fillable = [
        'text'
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
