<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\Slider
 *
 * @property integer $id
 * @property string $image
 * @property string $url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Slider whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Slider whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Slider whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Slider whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Slider whereUpdatedAt($value)
 */
class Slider extends Model
{
    //
}
