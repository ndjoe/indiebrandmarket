<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * OBID\Models\OrderItem
 *
 * @property integer $id
 * @property integer $product_item_id
 * @property integer $order_id
 * @property integer $qty
 * @property float $adjusment
 * @property string $accepted_at
 * @property \Carbon\Carbon $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Order $order
 * @property-read ProductItem $productItem
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderItem whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderItem whereProductItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderItem whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderItem whereQty($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderItem whereAdjusment($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderItem whereAcceptedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderItem whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderItem whereUpdatedAt($value)
 */
class OrderItem extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
    protected $table = 'order_items';

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function productItem()
    {
        return $this->belongsTo(ProductItem::class);
    }
}
