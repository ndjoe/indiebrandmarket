<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\Discount
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $value
 * @property \Carbon\Carbon $expired_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Product $product
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Discount whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Discount whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Discount whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Discount whereExpiredAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Discount whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Discount whereUpdatedAt($value)
 */
class Discount extends Model
{
    protected $table = 'discounts';

    protected $dates = ['created_at', 'updated_at', 'expired_at'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
