<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\BirthdayDiscount
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Order $order
 * @property-read User $user
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\BirthdayDiscount whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\BirthdayDiscount whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\BirthdayDiscount whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\BirthdayDiscount whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\BirthdayDiscount whereUpdatedAt($value)
 */
class BirthdayDiscount extends Model
{
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
