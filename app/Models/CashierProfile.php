<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\CashierProfile
 *
 * @property integer $id
 * @property integer $merchant_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read User $merchant
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\CashierProfile whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\CashierProfile whereMerchantId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\CashierProfile whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\CashierProfile whereUpdatedAt($value)
 */
class CashierProfile extends Model
{
    public function user()
    {
        return $this->morphOne(User::class, 'profileable');
    }

    public function merchant()
    {
        return $this->belongsTo(User::class);
    }
}
