<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * OBID\Models\Category
 *
 * @property integer $id
 * @property string $text
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Product[] $products
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Category whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Category whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Category whereUpdatedAt($value)
 */
class Category extends Model
{
    protected $table = 'categories';

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
