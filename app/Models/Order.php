<?php

namespace OBID\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * OBID\Models\Order
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $type
 * @property float $grossTotal
 * @property float $taxTotal
 * @property float $birthdayAdjusment
 * @property float $adjustmentCoupon
 * @property float $shippingCost
 * @property float $netTotal
 * @property float $uniqueCost
 * @property \Carbon\Carbon $confirmed_at
 * @property \Carbon\Carbon $expired_at
 * @property string $cancelled_at
 * @property string $payment_method
 * @property float $total_weight
 * @property string $currency
 * @property float $currency_value
 * @property string $shippingType
 * @property string $order_code
 * @property \Carbon\Carbon $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read OrderAddress $address
 * @property-read \Illuminate\Database\Eloquent\Collection|OrderItem[] $items
 * @property-read OrderPayment $payment
 * @property-read Shipping $shipping
 * @property-read User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|Coupon[] $coupons
 * @property-read OrderConfirmation $orderConfirmation
 * @property-read BirthdayDiscount $birthday
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereGrossTotal($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereTaxTotal($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereBirthdayAdjusment($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereAdjustmentCoupon($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereShippingCost($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereNetTotal($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereUniqueCost($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereConfirmedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereExpiredAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereCancelledAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order wherePaymentMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereTotalWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereCurrency($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereCurrencyValue($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereShippingType($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereOrderCode($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Order whereUpdatedAt($value)
 */
class Order extends Model
{
    use SoftDeletes;

    protected $table = 'orders';

    protected $dates = [
        'created_at',
        'expired_at',
        'confirmed_at',
        'updated_at',
        'deleted_at',
        'cancelled_at'
    ];

    protected $appends = ['is_active', 'is_paid'];

    public function address()
    {
        return $this->hasOne(OrderAddress::class);
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function payment()
    {
        return $this->hasOne(OrderPayment::class);
    }

    public function shipping()
    {
        return $this->hasOne(Shipping::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function coupons()
    {
        return $this->belongsToMany(Coupon::class)->withTimestamps();
    }

    public function orderConfirmation()
    {
        return $this->hasOne(OrderConfirmation::class);
    }

    public function birthday()
    {
        return $this->hasOne(BirthdayDiscount::class);
    }

    public function getIsActiveAttribute()
    {
        $expiration_date = $this->attributes['expired_at'];
        $cancellation_date = $this->attributes['cancelled_at'];

        return $expiration_date > Carbon::now() && is_null($cancellation_date);
    }

    public function getIsPaidAttribute()
    {
        $confirmation_date = $this->attributes['confirmed_at'];
        $cancellation_date = $this->attributes['cancelled_at'];

        return !is_null($confirmation_date) && is_null($cancellation_date);
    }
}
