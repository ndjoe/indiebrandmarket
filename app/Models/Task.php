<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * OBID\Models\Task
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $shipped_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Order $order
 * @property-read Shipping $shipping
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Task whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Task whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Task whereShippedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Task whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Task whereUpdatedAt($value)
 */
class Task extends Model
{
    protected $table = 'tasks';

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function shipping()
    {
        return $this->hasOne(Shipping::class);
    }
}
