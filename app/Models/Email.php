<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\Email
 *
 * @property integer $id
 * @property string $email
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Email whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Email whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Email whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Email whereUpdatedAt($value)
 */
class Email extends Model
{
    //
}
