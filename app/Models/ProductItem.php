<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * OBID\Models\ProductItem
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $size_id
 * @property integer $qty
 * @property string $sizing_info
 * @property string $uid
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Product $product
 * @property-read Size $size
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\ProductItem whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\ProductItem whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\ProductItem whereSizeId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\ProductItem whereQty($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\ProductItem whereSizingInfo($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\ProductItem whereUid($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\ProductItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\ProductItem whereUpdatedAt($value)
 */
class ProductItem extends Model
{
    protected $table = 'product_items';

    protected $fillable = [
        'product_id',
        'size_id',
        'qty'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function size()
    {
        return $this->belongsTo(Size::class);
    }
}
