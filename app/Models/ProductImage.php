<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\ProductImage
 *
 * @property integer $id
 * @property string $type
 * @property string $name
 * @property integer $product_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Product $product
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\ProductImage whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\ProductImage whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\ProductImage whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\ProductImage whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\ProductImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\ProductImage whereUpdatedAt($value)
 */
class ProductImage extends Model
{
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
