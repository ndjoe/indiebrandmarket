<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\MerchantProfile
 *
 * @property integer $id
 * @property string $alamat
 * @property string $kota
 * @property string $provinsi
 * @property string $owner_name
 * @property string $kodepos
 * @property string $bank
 * @property string $bank_owner
 * @property string $norek
 * @property string $banner_image
 * @property string $logo
 * @property string $about
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Subcategory[] $subcategories
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\MerchantProfile whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\MerchantProfile whereAlamat($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\MerchantProfile whereKota($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\MerchantProfile whereProvinsi($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\MerchantProfile whereOwnerName($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\MerchantProfile whereKodepos($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\MerchantProfile whereBank($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\MerchantProfile whereBankOwner($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\MerchantProfile whereNorek($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\MerchantProfile whereBannerImage($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\MerchantProfile whereLogo($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\MerchantProfile whereAbout($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\MerchantProfile whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\MerchantProfile whereUpdatedAt($value)
 */
class MerchantProfile extends Model
{
    protected $table = 'merchant_profiles';

    public function user()
    {
        return $this->morphOne(User::class, 'profileable');
    }

    public function subcategories()
    {
        return $this->hasMany(Subcategory::class);
    }
}
