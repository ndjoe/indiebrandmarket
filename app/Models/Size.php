<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * OBID\Models\Size
 *
 * @property integer $id
 * @property string $text
 * @property integer $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|ProductItem[] $items
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Size whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Size whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Size whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Size whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Size whereUpdatedAt($value)
 */
class Size extends Model
{
    protected $table = 'sizes';

    protected $fillable = [
        'text'
    ];

    public function items()
    {
        return $this->hasMany(ProductItem::class);
    }
}
