<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\StaffProfile
 *
 * @property integer $id
 * @property \Carbon\Carbon $birthday
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\StaffProfile whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\StaffProfile whereBirthday($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\StaffProfile whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\StaffProfile whereUpdatedAt($value)
 */
class StaffProfile extends Model
{
    protected $dates = ['created_at', 'updated_at', 'birthday'];
}
