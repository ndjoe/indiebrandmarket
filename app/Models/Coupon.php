<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * OBID\Models\Coupon
 *
 * @property integer $id
 * @property string $code
 * @property float $value
 * @property integer $brand_id
 * @property float $minimum_order
 * @property \Carbon\Carbon $expired_at
 * @property string $notavailable
 * @property integer $qty
 * @property \Carbon\Carbon $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Order[] $orders
 * @property-read User $brand
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Coupon whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Coupon whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Coupon whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Coupon whereBrandId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Coupon whereMinimumOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Coupon whereExpiredAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Coupon whereNotavailable($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Coupon whereQty($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Coupon whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Coupon whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Coupon whereUpdatedAt($value)
 */
class Coupon extends Model
{
    use SoftDeletes;

    protected $dates = ['create_at', 'updated_at', 'expired_at', 'deleted_at'];

    public function orders()
    {
        return $this->belongsToMany(Order::class)->withTimestamps();
    }

    public function brand()
    {
        return $this->belongsTo(User::class);
    }
}
