<?php
namespace OBID\Models;


use Zizaco\Entrust\EntrustRole;

/**
 * OBID\Models\Role
 *
 * @property integer $id
 * @property string $name
 * @property string $display_name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Config::get('auth.model')[] $users
 * @property-read \Illuminate\Database\Eloquent\Collection|\Config::get('entrust.permission')[] $perms
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Role whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Role whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Role whereDisplayName($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Role whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Role whereUpdatedAt($value)
 */
class Role extends EntrustRole
{

}