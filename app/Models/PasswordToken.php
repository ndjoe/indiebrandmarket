<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\PasswordToken
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read User $user
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PasswordToken whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PasswordToken whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PasswordToken whereToken($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PasswordToken whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PasswordToken whereUpdatedAt($value)
 */
class PasswordToken extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
