<?php

namespace OBID\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * OBID\Models\Product
 *
 * @property integer $id
 * @property integer $author_id
 * @property string $name
 * @property float $price
 * @property integer $subcategory_id
 * @property integer $category_id
 * @property \Carbon\Carbon $published_at
 * @property Discount $discount
 * @property float $weight
 * @property integer $color_id
 * @property string $description
 * @property string $gender
 * @property \Carbon\Carbon $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $slug
 * @property-read \Illuminate\Database\Eloquent\Collection|ProductItem[] $items
 * @property-read Subcategory $subcategory
 * @property-read User $author
 * @property-read \Illuminate\Database\Eloquent\Collection|Size[] $sizes
 * @property-read Color $color
 * @property-read Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|ProductImage[] $images
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product whereAuthorId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product whereSubcategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product wherePublishedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product whereDiscount($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product whereWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product whereColorId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product whereGender($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product published()
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Product discounted()
 */
class Product extends Model implements SluggableInterface
{
    use SluggableTrait, softDeletes;

    protected $table = 'products';

    protected $sluggable = [
        'build_from' => 'name',
        'save_to' => 'slug'
    ];

    protected $dates = ['created_at', 'published_at', 'updated_at', 'deleted_at'];

    public function items()
    {
        return $this->hasMany(ProductItem::class);
    }

    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    public function sizes()
    {
        return $this->belongsToMany(Size::class, 'product_items', 'product_id', 'size_id')
            ->withPivot('qty')->withTimestamps();
    }

    public function color()
    {
        return $this->belongsTo(Color::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function discount()
    {
        return $this->hasOne(Discount::class, 'product_id');
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class)->orderBy('id');
    }

    public function scopePublished($query)
    {
        return $query->whereNotNull('published_at');
    }

    public function scopeDiscounted($query)
    {
        return $query->whereHas('discount', function ($q) {
            $q->where('expired_at', '>', Carbon::now())
                ->where('value', '>', 0);
        });
    }
}
