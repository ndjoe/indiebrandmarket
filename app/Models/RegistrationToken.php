<?php

namespace OBID\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\RegistrationToken
 *
 * @property integer $id
 * @property string $token
 * @property \Carbon\Carbon $expired_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\RegistrationToken whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\RegistrationToken whereToken($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\RegistrationToken whereExpiredAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\RegistrationToken whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\RegistrationToken whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\RegistrationToken valid()
 */
class RegistrationToken extends Model
{
    protected $dates = ['expired_at', 'created_at', 'updated_at'];

    public function scopeValid($query)
    {
        return $query->where('expired_at', '>', Carbon::now());
    }
}
