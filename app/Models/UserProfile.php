<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\UserProfile
 *
 * @property integer $id
 * @property string $kodepos
 * @property string $kota
 * @property string $gender
 * @property \Carbon\Carbon $birthday
 * @property string $provinsi
 * @property string $alamat
 * @property integer $ongkir_province_id
 * @property integer $ongkir_city_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\UserProfile whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\UserProfile whereKodepos($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\UserProfile whereKota($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\UserProfile whereGender($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\UserProfile whereBirthday($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\UserProfile whereProvinsi($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\UserProfile whereAlamat($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\UserProfile whereOngkirProvinceId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\UserProfile whereOngkirCityId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\UserProfile whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\UserProfile whereUpdatedAt($value)
 */
class UserProfile extends Model
{
    protected $table = 'user_profiles';

    protected $dates = ['created_at', 'updated_at', 'birthday'];

    public function user()
    {
        return $this->morphOne(User::class, 'profileable');
    }
}
