<?php
namespace OBID\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\Subcategory
 *
 * @property integer $id
 * @property string $text
 * @property integer $merchant_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Product[] $products
 * @property-read \Illuminate\Database\Eloquent\Collection|Size[] $sizes
 * @property-read MerchantProfile $merchant
 * @property-read Category $category
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Subcategory whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Subcategory whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Subcategory whereMerchantId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Subcategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Subcategory whereUpdatedAt($value)
 */
class Subcategory extends Model
{
    protected $table = 'subcategories';

    protected $fillable = [
        'text',
        'category_id',
        'merchant_id'
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function sizes()
    {
        return $this->belongsToMany(Size::class);
    }

    public function merchant()
    {
        return $this->belongsTo(MerchantProfile::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}