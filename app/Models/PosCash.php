<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\PosCash
 *
 * @property integer $id
 * @property float $cash
 * @property integer $merchant_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read User $merchant
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PosCash whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PosCash whereCash($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PosCash whereMerchantId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PosCash whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\PosCash whereUpdatedAt($value)
 */
class PosCash extends Model
{
    public function merchant()
    {
        return $this->belongsTo(User::class);
    }
}
