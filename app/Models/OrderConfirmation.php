<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\OrderConfirmation
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $bank
 * @property string $atas_nama
 * @property float $amount
 * @property \Carbon\Carbon $date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Order $order
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderConfirmation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderConfirmation whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderConfirmation whereBank($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderConfirmation whereAtasNama($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderConfirmation whereAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderConfirmation whereDate($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderConfirmation whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderConfirmation whereUpdatedAt($value)
 */
class OrderConfirmation extends Model
{
    protected $dates = ['created_at', 'updated_at', 'date'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
