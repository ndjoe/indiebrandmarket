<?php

namespace OBID\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\Traits\EntrustUserTrait;


/**
 * OBID\Models\User
 *
 * @property integer $id
 * @property string $nama
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $nohp
 * @property integer $profileable_id
 * @property string $profileable_type
 * @property boolean $is_cashier
 * @property integer $merchant_id
 * @property boolean $is_active
 * @property string $activation_code
 * @property \Carbon\Carbon $deleted_at
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \ $profileable
 * @property-read \Illuminate\Database\Eloquent\Collection|Order[] $orders
 * @property-read \Illuminate\Database\Eloquent\Collection|Coupon[] $coupons
 * @property-read PasswordToken $resetToken
 * @property-read SalesNotification $salesNotification
 * @property-read \Illuminate\Database\Eloquent\Collection|BirthdayDiscount[] $birthdayPurchase
 * @property-read \Illuminate\Database\Eloquent\Collection|Product[] $products
 * @property-read \Illuminate\Database\Eloquent\Collection|\Config::get('entrust.role')[] $roles
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User whereNama($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User whereNohp($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User whereProfileableId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User whereProfileableType($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User whereIsCashier($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User whereMerchantId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User whereActivationCode($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User active()
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\User affiliate()
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, EntrustUserTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $dates = ['created_at', 'deleted_at', 'updated_at'];

    public function profileable()
    {
        return $this->morphTo();
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function coupons()
    {
        return $this->hasMany(Coupon::class);
    }

    public function resetToken()
    {
        return $this->hasOne(PasswordToken::class);
    }

    public function salesNotification()
    {
        return $this->hasOne(SalesNotification::class);
    }

    public function birthdayPurchase()
    {
        return $this->hasMany(BirthdayDiscount::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'author_id');
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    public function scopeAffiliate($query)
    {
        return $query->whereHas('roles', function ($q) {
            $q->where('name', 'affiliate');
        });
    }
}
