<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * OBID\Models\OrderPayment
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $type
 * @property string $bank
 * @property string $transaction_number
 * @property string $account_number
 * @property string $expiration_date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Order $order
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderPayment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderPayment whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderPayment whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderPayment whereBank($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderPayment whereTransactionNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderPayment whereAccountNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderPayment whereExpirationDate($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderPayment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderPayment whereUpdatedAt($value)
 */
class OrderPayment extends Model
{
    protected $table = 'order_payments';

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
