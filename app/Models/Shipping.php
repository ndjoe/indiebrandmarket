<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * OBID\Models\Shipping
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $no_resi
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Order $order
 * @property-read Task $task
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Shipping whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Shipping whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Shipping whereNoResi($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Shipping whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Shipping whereUpdatedAt($value)
 */
class Shipping extends Model
{
    protected $table = 'shippings';

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function task()
    {
        return $this->belongsTo(Task::class);
    }
}
