<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\SalesNotification
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $notif_count
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read User $user
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\SalesNotification whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\SalesNotification whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\SalesNotification whereNotifCount($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\SalesNotification whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\SalesNotification whereUpdatedAt($value)
 */
class SalesNotification extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
