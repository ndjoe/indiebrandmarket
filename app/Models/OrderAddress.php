<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * OBID\Models\OrderAddress
 *
 * @property integer $id
 * @property string $nama
 * @property string $kota
 * @property string $kodepos
 * @property string $nohp
 * @property string $alamat
 * @property string $provinsi
 * @property string $negara
 * @property integer $order_id
 * @property \Carbon\Carbon $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read OrderAddress $order
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderAddress whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderAddress whereNama($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderAddress whereKota($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderAddress whereKodepos($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderAddress whereNohp($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderAddress whereAlamat($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderAddress whereProvinsi($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderAddress whereNegara($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderAddress whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderAddress whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderAddress whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\OrderAddress whereUpdatedAt($value)
 */
class OrderAddress extends Model
{
    use SoftDeletes;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $table = 'order_addresses';

    public function order()
    {
        return $this->belongsTo(OrderAddress::class);
    }
}
