<?php

namespace OBID\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * OBID\Models\Currency
 *
 * @property integer $id
 * @property float $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Currency whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Currency whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Currency whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OBID\Models\Currency whereUpdatedAt($value)
 */
class Currency extends Model
{
    //
}
