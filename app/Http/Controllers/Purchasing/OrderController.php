<?php


namespace OBID\Http\Controllers\Purchasing;


use Carbon\Carbon;
use OBID\Events\Order\OrderConfirmed;
use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Order\PaymentConfirmationRequest;
use OBID\Models\OrderItem;
use OBID\Models\OrderPayment;
use OBID\Repositories\OrderItemRepository;
use OBID\Repositories\OrderRepository;
use OBID\Repositories\ProductItemRepository;

class OrderController extends Controller
{
    /**
     * @var OrderRepository
     */
    protected $orderRepo;

    /**
     * @var OrderItemRepository
     */
    protected $orderItemRepo;

    /**
     * @var ProductItemRepository
     */
    protected $productItemRepo;

    /**
     * @param OrderRepository $repo
     * @param OrderItemRepository $orderItemRepository
     * @param ProductItemRepository $productItemRepository
     */
    public function __construct(
        OrderRepository $repo,
        OrderItemRepository $orderItemRepository,
        ProductItemRepository $productItemRepository
    )
    {
        $this->orderRepo = $repo;
        $this->orderItemRepo = $orderItemRepository;
        $this->productItemRepo = $productItemRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $page = \Input::get('page', 1);
        $filter = \Input::get('filter', null);
        $query = \Input::get('query', null);

        $orders = $this->orderRepo->getOrdersPaginated($page, $query, $filter, 'desc', 'id');

        return \Response::json($orders);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function statuses()
    {
        $stats = collect([['value' => 'offline', 'text' => 'Offline'], ['value' => 'online', 'text' => 'Online']]);

        return \Response::json($stats);
    }

    public function confirmPayment(PaymentConfirmationRequest $request)
    {
        $req = $request->all();

        $order = $this->orderRepo->findById($req['orderId']);
        $payment = new OrderPayment;
        $payment->type = 'atm';
        $payment->bank = $req['bank'];

        $order->payment()->save($payment);
        $order->confirmed_at = Carbon::now();
        $order->expired_at = null;
        $order->save();

        $orderItems = $order->items;
        $items = $this->orderItemRepo->getMultipleByIds(array_column($orderItems->toArray(), 'id'));

        $items->each(function (OrderItem $o) {
            $this->productItemRepo->decrementQty($o->productItem->product_id, $o->productItem->size_id, $o->qty);
            event(new OrderConfirmed($o->productItem->product->author_id));
        });

        return response()->json(['created' => true], 201);
    }

    public function getOrderNotif()
    {
        $count = $this->orderRepo->getOrderUnconfirmedCount();

        return response()->json(['count' => $count]);
    }

    public function orders()
    {
        return view('backend.purchasing.orders');
    }
}