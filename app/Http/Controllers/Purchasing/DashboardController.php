<?php

namespace OBID\Http\Controllers\Purchasing;


use Carbon\Carbon;
use OBID\Helpers\ChartFormatter;
use OBID\Http\Controllers\Controller;
use OBID\Repositories\StatRepository;

class DashboardController extends Controller
{
    public function index(StatRepository $statRepository, ChartFormatter $chartFormatter)
    {
        $todayOrderCountList = $statRepository->getOrderCount('day', 0, Carbon::now());
        $totalOrderCountList = $statRepository->getOrderCount('month', 0, Carbon::now());
        $totalOrderCount = array_reduce($totalOrderCountList, function ($i, $obj) {
            return $i += $obj->value;
        });
        $todayOrderCount = array_reduce($todayOrderCountList, function ($i, $obj) {
            return $i += $obj->value;
        });
        $unconfirmedOrderCount = $statRepository->unConfirmedOrderCount();
        $confirmedOrderCount = $statRepository->confirmedOrderCount();

        \JavaScript::put([
            'todayOrderCount' => $todayOrderCount,
            'totalOrderCount' => $totalOrderCount,
            'totalOrderCountChart' => $chartFormatter->formatSalesChartLine($totalOrderCountList, 'month'),
            'todayOrderCountChart' => $chartFormatter->formatSalesChartLine($todayOrderCountList, 'day'),
            'confirmedOrderCount' => $confirmedOrderCount,
            'unconfirmedOrderCount' => $unconfirmedOrderCount
        ]);

        return view('backend.purchasing.index');
    }
}