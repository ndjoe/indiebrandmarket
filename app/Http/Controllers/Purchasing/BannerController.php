<?php


namespace OBID\Http\Controllers\Purchasing;


use Illuminate\Support\Str;
use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Slider\UpdateSliderRequest;
use OBID\Models\Slider;

class BannerController extends Controller
{
    public function index()
    {
        $sliders = Slider::orderBy('id', 'asc')->get();

        return response()->json($sliders);
    }

    public function updateImage(UpdateSliderRequest $request)
    {
        $req = $request->all();

        $slide = Slider::whereId($req['slideId'])->first();

        $name = Str::slug($request->file('image')->getClientOriginalName()) . Str::random(3) . '.' . $request->file('image')->getClientOriginalExtension();
        $request->file('image')->move(base_path() . '/public/images/sliders/', $name);

        $slide->image = $name;
        $slide->save();

        return response()->json(['updated' => true]);
    }

    public function clearImage($id)
    {
        $slide = Slider::whereId($id)->first();

        $slide->image = null;

        $slide->save();

        return response()->json(['cleared' => true]);
    }

    public function banner()
    {
        return view('backend.purchasing.banner');
    }
}