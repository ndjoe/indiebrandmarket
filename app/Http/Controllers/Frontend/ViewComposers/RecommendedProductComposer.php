<?php

namespace OBID\Http\Controllers\Frontend\ViewComposers;


use Illuminate\Contracts\View\View;
use OBID\Repositories\ProductRepository;

class RecommendedProductComposer
{
    /**
     * @var ProductRepository
     */
    protected $products;

    /**
     * RecommendedProductComposer constructor.
     * @param ProductRepository $products
     */
    public function __construct(ProductRepository $products)
    {
        $this->products = $products;
    }


    public function compose(View $view)
    {
        $brand = \Route::current()->getParameter('name');
        $view->with('anotherProducts', $this->products->getBrandRandomProducts(6, $brand));
    }
}