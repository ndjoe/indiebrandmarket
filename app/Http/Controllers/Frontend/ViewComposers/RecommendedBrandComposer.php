<?php

namespace OBID\Http\Controllers\Frontend\ViewComposers;


use Illuminate\Contracts\View\View;
use OBID\Repositories\UserRepository;

class RecommendedBrandComposer
{
    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * RecommendedBrandComposer constructor.
     * @param UserRepository $users
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    public function compose(View $view)
    {
        $view->with('anotherBrands', $this->users->getRandomBrands(6));
    }
}