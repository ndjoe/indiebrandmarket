<?php


namespace OBID\Http\Controllers\Frontend;


use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use OBID\Helpers\MoneyService;
use OBID\Http\Controllers\Controller;
use OBID\Models\Product;
use OBID\Models\User;
use OBID\Repositories\ProductRepository;

class ProductController extends Controller
{
    /**
     * @var ProductRepository
     */
    protected $repo;

    /**
     * @param ProductRepository $repo
     */
    public function __construct(ProductRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
        $page = \Input::get('page', 1);
        $category = \Input::get('category', 0);
        $size = \Input::get('size', 0);
        $color = \Input::get('color', 0);
        $price = \Input::get('price', 0);
        $gender = \Input::get('gender', 'all');

        $products = $this->repo->getFrontendProductsPaginated(
            $page,
            null,
            $category,
            $color,
            $price,
            $gender,
            $size,
            'desc',
            'id',
            18
        );

        if (LaravelLocalization::getCurrentLocale() === 'en') {
            $products->each(function (Product $product) {
                $money = new MoneyService($product->price);
                $product->price = $money->getDollarValue();
            });
        }
        $products = $products->toArray();

        $products['data'] = array_chunk($products['data'], 6);

        return \Response::json($products);
    }

    public function saleProducts()
    {
        $page = \Input::get('page', 1);
        $category = \Input::get('category', 0);
        $size = \Input::get('size', 0);
        $color = \Input::get('color', 0);
        $price = \Input::get('price', 0);
        $gender = \Input::get('gender', 'all');

        $products = $this->repo->getSaleProductPaginated(
            $page,
            null,
            $category,
            $color,
            $price,
            $gender,
            $size,
            'desc',
            'id',
            18
        );

        if (LaravelLocalization::getCurrentLocale() === 'en') {
            $products->each(function (Product $product) {
                $money = new MoneyService($product->price);
                $product->price = $money->getDollarValue();
            });
        }
        $products = $products->toArray();

        $products['data'] = array_chunk($products['data'], 6);

        return \Response::json($products);
    }

    public function frontProducts()
    {
        $page = \Input::get('page', 1);
        $filter = \Input::get('filter', 0);
        $query = \Input::get('query', null);

        $products = $this->repo->getFrontendProductsPaginated($page, $query, $filter, 'desc', 'id', 12);
        if (LaravelLocalization::getCurrentLocale() === 'en') {
            $products->each(function (Product $product) {
                $money = new MoneyService($product->price);
                $product->price = $money->getDollarValue();
            });
        }
        $products = $products->toArray();

        $products['data'] = array_chunk($products['data'], 6);

        return \Response::json($products);
    }

    public function categoryProduct()
    {

    }

    public function brandProduct($name)
    {
        $brand = User::with('profileable')->whereUsername($name)->first();

        $page = \Input::get('page', 1);
        $category = \Input::get('category', 0);
        $size = \Input::get('size', 0);
        $color = \Input::get('color', 0);
        $price = \Input::get('price', 0);
        $gender = \Input::get('gender', 'all');

        $products = $this->repo->getBrandProductsBySubcategory(
            $brand,
            $page,
            $category,
            $size,
            $gender,
            'desc',
            'id',
            $color,
            $price
        );

        if (LaravelLocalization::getCurrentLocale() === 'en') {
            $products->each(function (Product $product) {
                $money = new MoneyService($product->price);
                $product->price = $money->getDollarValue();
            });
        }

        $products = $products->toArray();
        $products['data'] = array_chunk($products['data'], 6);

        return response()->json($products);
    }
}