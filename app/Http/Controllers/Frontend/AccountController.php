<?php


namespace OBID\Http\Controllers\Frontend;


use Carbon\Carbon;
use Illuminate\Support\Str;
use OBID\Events\Member\MemberActivated;
use OBID\Events\Member\MemberRegistered;
use OBID\Events\Member\ResetPassword;
use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Account\ActivateByCodeRequest;
use OBID\Http\Requests\Account\EditProfileRequest;
use OBID\Http\Requests\Account\LoginRequest;
use OBID\Http\Requests\Account\NewAccountRequest;
use OBID\Http\Requests\Account\RequestForgotPasswordRequest;
use OBID\Http\Requests\Account\SetPasswordRequest;
use OBID\Models\User;
use OBID\Repositories\PasswordTokenRepository;
use OBID\Repositories\UserRepository;

class AccountController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $accountRepo;

    /**
     * @var PasswordTokenRepository
     */
    protected $tokenRepo;

    /**
     * @param UserRepository $accountRepo
     * @param PasswordTokenRepository $passwordTokenRepository
     */
    public function __construct(
        UserRepository $accountRepo,
        PasswordTokenRepository $passwordTokenRepository
    )
    {
        $this->accountRepo = $accountRepo;
        $this->tokenRepo = $passwordTokenRepository;
    }


    public function getAuthProfile()
    {
        $user = \Auth::user();
        $user->load('profileable');

        return response()->json($user);
    }

    public function editProfile(EditProfileRequest $request)
    {
        $req = $request->all();

        $user = \Auth::user();
        $profile = $user->profileable;

        $user->nama = Str::ucfirst($req['nama']);
        $user->nohp = $req['nohp'];
        $profile->provinsi = $req['provinsi'];
        $profile->alamat = $req['alamat'];
        $profile->gender = $req['gender'];
        $profile->kota = $req['kota'];
        $profile->ongkir_province_id = $req['ongkir_province_id'];
        $profile->ongkir_city_id = $req['ongkir_city_id'];
        if ($req['birthday'] !== null) {
            $profile->birthday = Carbon::parse($req['birthday']);
        }
        $profile->kodepos = $req['kodepos'];

        $profile->save();
        $user->save();

        return response()->json(['updated' => true]);
    }

    public function registerAccount(NewAccountRequest $request)
    {
        $req = $request->all();
        $created = $this->accountRepo->createNewMember($req);

        event(new MemberRegistered($created));
        if ($created) {
            \Session::flash('register_success', "terimakasih sudah register, kami sudah kirim kode aktifasi ke email dana nomor anda, silahkan klik link yang ada di email anda atau masukkan kode aktifasi yang sudah kami kirimkan");
            \Session::set('user:registered', $created);
            return response()->json(['created' => true, 'redirect' => url('/activate')]);
        }

        return redirect()->back();
    }

    public function resendCode()
    {
        $user = \Session::pull('user:registered');

        event(new MemberRegistered($user));

        return response()->json(['sent' => true]);
    }

    public function logout()
    {
        \Auth::logout();
        \Cart::destroy();

        return redirect()->to('/');
    }

    public function login(LoginRequest $request)
    {
        $req = $request->all();

        $user = User::whereUsername($req['username'])->whereIsActive(true)->first();
        $success = false;

        if ($user) {
            $success = \Auth::attempt(['username' => $user->username, 'password' => $req['password']]);
            if ($success) {
                if (\Session::has('redirect')) {
                    return redirect()->to(\Session::pull('redirect'));
                } else {
                    return redirect()->to('/');
                }
            } else {
                \Session::flash('notice.login.error', 'password salah');
                return redirect()->back();
            }
        }

        \Session::flash('notice.login.error', 'Akun tidak ditemukan');
        return redirect()->back();
    }

    public function getBrands()
    {
        $brands = $this->accountRepo->getRandomBrands(12);

        return response()->json($brands);
    }

    public function getBrandsPaginated()
    {
        $page = \Input::get('page', 1);

        $brands = $this->accountRepo->getUsersPaginated($page, null, 'affiliate', 'DESC', 'id', 18);

        return response()->json($brands);
    }

    public function activateAccount()
    {
        $code = \Input::get('code', null);

        if (is_null($code)) {
            return redirect()->to('/login');
        }

        $user = User::whereActivationCode($code)->first();
        if ($user->activation_code === $code) {
            $user->is_active = true;
            $user->activation_code = null;
            $user->save();
            event(new MemberActivated($user));
            \Auth::login($user);
        }

        if (\Session::has('redirect')) {
            return redirect()->to(\Session::pull('redirect'));
        }

        return redirect()->to('/login');
    }

    public function activateCode(ActivateByCodeRequest $request)
    {
        $req = $request->all();

        $user = User::whereActivationCode($req['code'])->first();
        if ($user) {
            $user->is_active = true;
            $user->activation_code = null;
            $user->save();
            event(new MemberActivated($user));
            \Auth::loginUsingId($user->id);
        }

        if (\Session::has('redirect')) {
            return redirect()->to(\Session::pull('redirect'));
        }

        return redirect()->to('/login');
    }

    public function requestForgotPasswordToken(RequestForgotPasswordRequest $request)
    {
        $req = $request->all();

        $user = $this->accountRepo->findByEmail($req['email']);


        if (!$user) {
            \Session::flash('notice.error', 'User dengan email tersebut tidak ditemukan mohon cek lagi email anda');
            return redirect()->back();
        }

        $this->tokenRepo->createToken($user->id);

        event(new ResetPassword($user));

        \Session::flash('notice.info', 'Kami sudah mengirimkan link reset password ke email anda silahkan cek email anda');
        return redirect()->back();
    }

    /**
     * @param SetPasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setPassword(SetPasswordRequest $request)
    {
        $req = $request->all();

        if (!\Session::has('user.forgot')) {
            abort(403);
        }

        $user = \Session::pull('user.forgot');

        $saved = $this->accountRepo->setPassword($user->id, $req['password']);

        if ($saved) {
            \Session::flash('notice.login.info', 'Password anda sudah diupdate silahkan login');
            return redirect()->to('/login');
        }

        \Session::flash('notice.error', "ooops... somethings wrong, we'll try to fix it");
        return redirect()->back();
    }

    public function confirmResetToken()
    {
        $token = \Input::get('token');

        if (!$token) {
            abort(403);
        }

        $user = $this->tokenRepo->findUserByToken($token);

        if (!$user) {
            abort(403, 'whoops you cant access this');
        }

        $deleteToken = $this->tokenRepo->deleteUserToken($user->id);
        \Session::flash('notice.info', 'Please Set Your new Password');
        \Session::set('user.forgot', $user);
        return view('frontend.shop.setpassword');
    }

    public function getProfile()
    {
        $user = \Auth::user();

        $user->load('profileable');

        $profile = [
            'nama' => $user->nama,
            'kodepos' => $user->profileable->kodepos,
            'alamat' => $user->profileable->alamat,
            'nohp' => $user->nohp,
            'ongkir_city_id' => $user->profileable->ongkir_city_id,
            'ongkir_province_id' => $user->profileable->ongkir_province_id
        ];

        return response()->json($profile);

    }
}