<?php

namespace OBID\Http\Controllers\Frontend;


use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Email\EmailRequest;
use OBID\Jobs\Newsletter\NewSubscriber;
use OBID\Models\Email;

class EmailController extends Controller
{
    public function store(EmailRequest $request)
    {
        $req = $request->all();

        $check = Email::whereEmail($req['email'])->exists();

        if (!$check) {
            $coupond = $this->dispatch(new NewSubscriber($req['email']));

            return response()->json(['subscribed' => true, 'couponCode' => $coupond->code]);
        }

        return response()->json(['subscribed' => false]);
    }
}