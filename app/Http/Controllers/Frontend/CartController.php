<?php
namespace OBID\Http\Controllers\Frontend;


use Carbon\Carbon;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use OBID\Helpers\MoneyService;
use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Cart\DelCartRowRequest;
use OBID\Http\Requests\Cart\NewCartRequest;
use OBID\Http\Requests\Discount\SubmitDiscountRequest;
use OBID\Repositories\CouponRepository;
use OBID\Repositories\DiscountRepository;
use OBID\Repositories\ProductItemRepository;

class CartController extends Controller
{
    /**
     * @var ProductItemRepository
     */
    protected $itemRepo;

    protected $couponRepo;

    protected $discountRepo;

    /**
     * @param ProductItemRepository $itemRepo
     * @param CouponRepository $couponRepository
     * @param DiscountRepository $discountRepository
     */
    public function __construct(
        ProductItemRepository $itemRepo,
        CouponRepository $couponRepository,
        DiscountRepository $discountRepository
    )
    {
        $this->itemRepo = $itemRepo;
        $this->couponRepo = $couponRepository;
        $this->discountRepo = $discountRepository;
    }

    public function getAll()
    {
        $currency = LaravelLocalization::getCurrentLocale() === 'id' ? 'IDR' : 'USD';

//        $this->checkCurrencyAndDestroyCart($currency);

        return response()->json($this->formatCartItem($currency));
    }

    public function addCart(NewCartRequest $request)
    {
        $req = $request->all();
        $discount = 0;
        $locale = LaravelLocalization::getCurrentLocale();
        $currency = $locale === 'id' ? 'IDR' : 'USD';

        $item = $this->itemRepo->getItemBySizeAndProductId($req['productId'], $req['sizeId']);

        $itemPrice = new MoneyService($item->product->price);
        $search = \Cart::search(['id' => $item->id]);
        $discountObj = $this->discountRepo->findProductDiscount($item->product_id);

        if ($discountObj) {
            if ($discountObj->expired_at > Carbon::now()) {
                $discount = $discountObj->value;
            }
        }

        if (is_array($search)) {
            \Cart::update($search[0], $req['qty']);
        } else {
            \Cart::add([
                'id' => $item->id,
                'name' => $item->product->name,
                'qty' => $req['qty'],
                'price' => $locale === 'id' ? $itemPrice->getRupiahValue() : $itemPrice->getDollarValue(),
                'options' => [
                    'productId' => $item->product_id,
                    'image' => $item->product->images[0]->name,
                    'slug' => $item->product->slug,
                    'subcategory' => $item->product->subcategory->text,
                    'sizeId' => $item->size_id,
                    'sizeText' => $item->size->text,
                    'weight' => $item->product->weight,
                    'discount' => $discount,
                    'currency' => $locale === 'id' ? 'IDR' : 'USD',
                    'brand' => $item->product->author->username
                ]
            ]);
        }

        return response()->json($this->formatCartItem($currency));
    }

    public function deleteItem(DelCartRowRequest $request)
    {
        $req = $request->all();
        $currency = LaravelLocalization::getCurrentLocale() === 'id' ? 'IDR' : 'USD';

        \Cart::remove($req['rowid']);

        return response()->json($this->formatCartItem($currency));
    }

    public function AddCoupon(SubmitDiscountRequest $request)
    {
        $req = $request->all();

        $locale = LaravelLocalization::getCurrentLocale();
        $currency = LaravelLocalization::getCurrentLocale() === 'id' ? 'IDR' : 'USD';

        $coupon = $this->couponRepo->findByCode($req['code']);

        if ($coupon) {
            if ($this->couponRepo->checkCouponValidity($coupon, \Cart::total())) {
                if (($coupon->notavailable === 'discount') && $this->isThereItemDiscounted()) {
                    return response()->json(['added' => false, 'reason' => 'Kupon yang anda masukkan tidak dapat untuk pesanan ini']);
                }
                $couponValue = new MoneyService($coupon->value);
                \Session::set('coupon', [
                    'couponId' => $coupon->id,
                    'value' => $locale === 'id' ? $couponValue->getRupiahValue() : $couponValue->getDollarValue(),
                    'currency' => $locale === 'id' ? 'IDR' : 'USD'
                ]);
            } else {
                return response()->json(['added' => false, 'reason' => 'kupon yang anda masukkan tidak berlaku']);
            }
        } else {
            return response()->json(['added' => false, 'reason' => 'Kupon tidak ditemukan']);
        }

        return response()->json($this->formatCartItem($currency));
    }

    public function deleteCoupon()
    {
        \Session::forget('coupon');
        $currency = LaravelLocalization::getCurrentLocale() === 'id' ? 'IDR' : 'USD';

        return response()->json($this->formatCartItem($currency));
    }

    private function formatCartItem($currency = 'IDR')
    {
        $cartArray['items'] = [];

        $cart = \Cart::content();

        foreach ($cart as $item) {
            $price = new MoneyService($item['price'], $item['options']['currency']);
            $item['price'] = $currency === 'IDR' ? $price->getRupiahValue() : $price->getDollarValue();
            $item['options']['currency'] = $currency;
            array_push($cartArray['items'], $item);
        }

        $cartArray['coupon'] = \Session::get('coupon', ['couponId' => 0, 'value' => 0, 'currency' => 'IDR']);

        $couponMoney = new MoneyService($cartArray['coupon']['value'], $cartArray['coupon']['currency']);
        $cartArray['coupon']['value'] = $currency === 'IDR' ? $couponMoney->getRupiahValue() : $couponMoney->getDollarValue();

        return $cartArray;
    }

    /**
     * @return bool
     */
    private function isThereItemDiscounted()
    {
        $cart = $this->formatCartItem();

        $result = false;

        foreach ($cart['items'] as $item) {
            if ($item['options']['discount'] > 0) {
                $result = true;
                break;
            }
        }

        return $result;
    }

    private function checkCurrencyAndDestroyCart($currency)
    {
        $cart = \Cart::content();

        foreach ($cart['items'] as $item) {
            if ($item['options']['currency'] === $currency) {
                \Cart::destroy();
                break;
            }
        }
    }

    private function in_array_r($needle, $haystack, $strict = false)
    {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }

        return false;
    }
}