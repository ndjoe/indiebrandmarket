<?php


namespace OBID\Http\Controllers\Frontend;


use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use OBID\Helpers\MoneyService;
use OBID\Http\Controllers\Controller;

class OngkirController extends Controller
{
    protected static $apiKey = '3bb3f21b214a4aec5059504ac0f752b4';
    protected static $baseUrl = 'http://rajaongkir.com/api/starter';
    /**
     * @var Client
     */
    protected $client;


    public function __construct()
    {
        $this->client = new Client(['headers' => ['key' => static::$apiKey]]);
    }

    public function getProvinces()
    {
        $url = static::$baseUrl . '/' . 'province';
        $client = $this->client;

        $provinces = \Cache::remember(
            'provinces',
            Carbon::now()->diffInMinutes(Carbon::now()->addDay(1)),
            function () use ($client, $url) {
                $response = $client->get($url);
                $result = json_decode($response->getBody());

                $provinces = [];
                foreach ($result->rajaongkir->results as $r) {
                    array_push($provinces, [
                        'value' => [
                            'id' => $r->province_id,
                            'text' => $r->province
                        ],
                        'text' => $r->province
                    ]);
                }

                return $provinces;
            });

        return response()->json($provinces);
    }

    public function getCitiesByProvince($provinceId)
    {
        $url = static::$baseUrl . '/' . 'city';
        $client = $this->client;

        $cities = \Cache::remember(
            'city:' . $provinceId,
            Carbon::now()->diffInMinutes(Carbon::now()->addDay(1)),
            function () use ($client, $url, $provinceId) {
                $response = $client->request('GET', $url, ['query' => ['province' => $provinceId]]);
                $result = json_decode($response->getBody());

                $cities = [];
                foreach ($result->rajaongkir->results as $r) {
                    array_push($cities, [
                        'value' => [
                            'id' => $r->city_id,
                            'text' => $r->type . ' ' . $r->city_name
                        ],
                        'text' => $r->type . ' ' . $r->city_name
                    ]);
                }

                return $cities;
            });

        return response()->json($cities);
    }

    public function getOngkir($cityId)
    {
        $url = static::$baseUrl . '/' . 'cost';
        $weight = 0;
        $weight = \Cart::content()->sum(function ($i) {
            return $i['options']['weight'];
        });
        $weight = ceil($weight / 1000) * 1000;
        $courier = 'jne';
        $from = 23;
        $locale = LaravelLocalization::getCurrentLocale();

        $response = $this->client->request('POST', $url, [
            'form_params' => [
                'origin' => $from,
                'destination' => $cityId,
                'weight' => $weight,
                'courier' => $courier
            ]
        ]);

        $result = json_decode($response->getBody());
        $arrayResponse = [];

        foreach ($result->rajaongkir->results[0]->costs as $c) {
            if (Str::lower($c->service) === 'reg' || Str::lower($c->service) === 'ctcoke') {
                $harga = $c->cost[0]->value;
                $harga = $harga * 1;
                $harga += 3000;

                array_push($arrayResponse, [
                    'value' => [
                        'cost' => $harga,
                        'service' => $c->service
                    ],
                    'text' => 'JNE ' . '- IDR ' . $harga
                ]);
            }
        }

        if ($locale === 'en') {
            $cost = new MoneyService($arrayResponse[0]['value']['cost'], 'IDR');
            $arrayResponse[0]['value']['cost'] = $cost->getDollarValue();
            $arrayResponse[0]['text'] = 'JNE ' . '- USD ' . number_format($arrayResponse[0]['value']['cost'], 2, '.', ',');
        }

        return response()->json($arrayResponse);
    }
}