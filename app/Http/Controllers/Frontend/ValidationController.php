<?php

namespace OBID\Http\Controllers\Frontend;


use OBID\Http\Controllers\Controller;
use OBID\Repositories\UserRepository;

class ValidationController extends Controller
{
    protected $userRepo;

    /**
     * ValidationController constructor.
     * @param UserRepository $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function checkUsername($username)
    {
        return response()->json([
            'exists' => $this->userRepo->checkUsername($username)
        ]);
    }

    public function checkEmail($email)
    {
        return response()->json([
            'exists' => $this->userRepo->checkEmail($email)
        ]);
    }

    public function checkNohp($nohp)
    {
        return response()->json([
            'exists' => $this->userRepo->checkNohp($nohp)
        ]);
    }
}