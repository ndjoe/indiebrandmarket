<?php


namespace OBID\Http\Controllers\Frontend;


use Illuminate\Support\Str;
use OBID\Http\Controllers\Controller;
use OBID\Models\Category;
use OBID\Models\Color;
use OBID\Models\Size;
use OBID\Models\Subcategory;

class OptionsController extends Controller
{
    public function getOptionsForBrandPage($brandId)
    {
        $sizes = Size::whereHas('items.product', function ($q) use ($brandId) {
            $q->where('author_id', $brandId)->published();
        })->get();
        $categoriesses = Subcategory::whereHas('products', function ($q) use ($brandId) {
            $q->where('author_id', $brandId)->published();
        })->get();
        $colors = Color::whereHas('products', function ($q) use ($brandId) {
            $q->where('author_id', $brandId)->published();
        })->get();

        $size = [];
        $categories = [];
        $colours = [];

        foreach ($sizes as $s) {
            array_push($size, [
                'text' => Str::upper($s->text),
                'value' => $s->id
            ]);
        }

        foreach ($categoriesses as $subcat) {
            array_push($categories, [
                'text' => Str::upper($subcat->text),
                'value' => $subcat->id
            ]);
        }

        foreach ($colors as $col) {
            array_push($colours, [
                'text' => Str::upper($col->text),
                'value' => $col->id
            ]);
        }

        return response()->json(compact('size', 'categories', 'colours'));
    }

    public function getAllOptions()
    {
        $sizes = Size::whereHas('items.product', function ($q) {
            $q->published();
        })->get();

        $categoriesses = Category::whereHas('products', function ($q) {
            $q->published();
        })->get();
        $colors = Color::whereHas('products', function ($q) {
            $q->published();
        })->get();

        $size = [];
        $categories = [];
        $colours = [];

        foreach ($sizes as $s) {
            array_push($size, [
                'text' => Str::upper($s->text),
                'value' => $s->id
            ]);
        }

        foreach ($categoriesses as $subcat) {
            array_push($categories, [
                'text' => Str::upper($subcat->text),
                'value' => $subcat->id
            ]);
        }

        foreach ($colors as $col) {
            array_push($colours, [
                'text' => Str::upper($col->text),
                'value' => $col->id
            ]);
        }

        return response()->json(compact('size', 'categories', 'colours'));
    }
}