<?php


namespace OBID\Http\Controllers\Frontend;


use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Order\OrderConfirmationRequest;
use OBID\Repositories\OrderConfirmationRepository;
use OBID\Repositories\OrderRepository;

class OrderConfirmationController extends Controller
{

    /**
     * @var OrderConfirmationRepository
     */
    protected $orderConfirmRepo;

    /**
     * @var OrderRepository
     */
    protected $order;

    /**
     * OrderConfirmationController constructor.
     * @param OrderConfirmationRepository $orderConfirmRepo
     * @param OrderRepository $orderRepository
     */
    public function __construct(
        OrderConfirmationRepository $orderConfirmRepo,
        OrderRepository $orderRepository
    )
    {
        $this->orderConfirmRepo = $orderConfirmRepo;
        $this->order = $orderRepository;
    }

    public function create(OrderConfirmationRequest $request)
    {
        $req = $request->all();
        $order = $this->order->findByCode($req['nomororder']);

        if (!$order) {
            \Session::flash('notice.error', 'Nomor Order ' . $req['nomororder'] . ' tidak ditemukan');
            return redirect()->back();
        }

        $data = [
            'order_id' => $order->id,
            'amount' => $req['amount'],
            'atas_nama' => $req['nama'],
            'bank' => $req['bank'],
            'date' => $req['tgl']
        ];

        $this->orderConfirmRepo->create($data);

        \Session::flash('notice.info', 'terimakasih sudah melakukan konfirmasi pembayaran sejumlah '
            . $req['amount'] . ' melalui bank ' . $req['bank'] . ' dengan nomor order ' . $req['nomororder'] .
            ', order anda akan segera kami proses');
        return redirect()->back();
    }
}