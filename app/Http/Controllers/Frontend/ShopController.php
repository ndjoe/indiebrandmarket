<?php


namespace OBID\Http\Controllers\Frontend;


use Carbon\Carbon;
use Illuminate\Support\Str;
use JavaScript;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use OBID\Helpers\MoneyService;
use OBID\Http\Controllers\Controller;
use OBID\Models\Product;
use OBID\Models\Slider;
use OBID\Models\User;
use OBID\Repositories\OrderRepository;

class ShopController extends Controller
{
    /**
     * @var OrderRepository
     */
    private $orderRepo;

    /**
     * @param OrderRepository $orderRepo
     */
    public function __construct(OrderRepository $orderRepo)
    {
        $this->orderRepo = $orderRepo;
    }


    public function index()
    {
        $sliders = Slider::orderBy('id', 'asc')->get();

        JavaScript::put([
            'locale' => LaravelLocalization::getCurrentLocale()
        ]);

        return view('frontend.shop.front', compact('sliders'));
    }

    public function singleProduct($name, $slug)
    {
        $product = Product::with(
            'sizes',
            'discount',
            'author',
            'images',
            'author.profileable',
            'category',
            'items',
            'items.size'
        )
            ->published()
            ->where('slug', $slug)
            ->first();
        if (!$product) {
            abort(404);
        }
        $locale = LaravelLocalization::getCurrentLocale();
        $sizes = $product->sizes;
        $price = new MoneyService($product->price);
        $product->price = $locale === 'id' ? $price->getRupiahValue() : $price->getDollarValue();
        $product = $product->toArray();
        $options = [];

        foreach ($sizes->toArray() as $s) {
            array_push($options, [
                'value' => $s['id'],
                'text' => Str::upper($s['text']),
                'qty' => $s['pivot']['qty']
            ]);
        }

        JavaScript::put([
            'locale' => $locale,
            'options' => $options
        ]);

        \SEO::opengraph()->setDescription($product['description']);
        \SEO::opengraph()->setTitle($product['name'] . ' | OBID');
        \SEO::opengraph()->setUrl(\Request::url());
        \SEO::opengraph()->addProperty('type', 'product');
        \SEO::opengraph()->addProperty('image', url('/images/catalog/' . $product['images'][0]['name']));


        return view('frontend.shop.single', compact('name', 'options', 'product'));
    }

    public function cartDetail()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        if (\Auth::check() && \Auth::user()->profileable->birthday) {
            if (\Auth::user()->birthdayPurchase->count() < 1 && \Auth::user()->profileable->birthday->isBirthday(Carbon::today())) {
                JavaScript::put([
                    'birthday' => true,
                    'locale' => $locale
                ]);
            } else {
                JavaScript::put([
                    'birthday' => false,
                    'locale' => $locale
                ]);
            }
        } else {
            JavaScript::put([
                'birthday' => false,
                'locale' => $locale
            ]);
        }

        return view('frontend.shop.cart');
    }

    public function checkout()
    {
        $guest = \Input::get('guest');
        $js = [];
        $locale = LaravelLocalization::getCurrentLocale();

        if (!\Auth::check() && !$guest) {
            \Session::set('redirect', \URL::current());
            return redirect()->to('/login?from=checkout');
        }

        $cart = \Cart::content();

        if (sizeof($cart) === 0) {
            return redirect()->to('/cart');
        }

        if (\Auth::check()) {
            if (\Auth::user()->profileable->birthday) {
                if (\Auth::user()->birthdayPurchase->count() < 1 && \Auth::user()->profileable->birthday->isBirthday(Carbon::today())) {
                    $js['birthday'] = true;
                } else {
                    $js['birthday'] = false;
                }
            } else {
                $js['birthday'] = false;
            }

            $user = \Auth::user();
            $js['guest'] = false;
            $js['provinces'] = \Cache::get('provinces');
            $js['cities'] = \Cache::get('city:' . $user->profileable->ongkir_province_id);
        } else {
            if ($guest) {
                $js['provinces'] = \Cache::get('provinces');
                $js['cities'] = \Cache::get('city:' . $js['provinces'][0]['value']['id']);
                $js['guest'] = true;
            }
        }

        $js['locale'] = $locale;
        $uniqueTrans = mt_rand(1, 500);
        \Session::set('uniqueTrans', $uniqueTrans);
        $js['uniqueTrans'] = $uniqueTrans;
        JavaScript::put($js);
        return view('frontend.shop.checkout');
    }

    public function accountOrders()
    {
        $page = \Input::get('page', 1);

        $orders = $this->orderRepo->getUserOrders($page, 'desc', 'id', \Auth::user());
        $locale = LaravelLocalization::getCurrentLocale();
        JavaScript::put([
            'locale' => $locale
        ]);
        return view('frontend.account.index', compact('orders'));
    }

    public function accountProfile()
    {
        if (!\Auth::check()) {
            \Session::set('redirect', \URL::current());
            return redirect()->to('/');
        }
        $user = \Auth::user()->load('profileable');
        $locale = LaravelLocalization::getCurrentLocale();
        $provinces = \Cache::get('provinces');
        $userProvinceId = is_null($user->profileable->ongkir_province_id) ?
            $provinces[0]['value']['id'] : $user->profileable->ongkir_province_id;

        $cities = \Cache::get('city:' . $userProvinceId);

        JavaScript::put([
            'locale' => $locale,
            'provinces' => $provinces,
            'cities' => $cities,
            'user' => $user
        ]);

        return view('frontend.account.userProfile');
    }

    public function login()
    {
        $from = \Input::get('from');
        if (\Auth::check()) {
            if (\Session::has('redirect')) {
                return redirect()->to(\Session::pull('redirect'));
            }
            return redirect()->to('/');
        }

        if ($from && $from === 'checkout') {
            \Session::flash('checkout', true);
        }
        $locale = LaravelLocalization::getCurrentLocale();
        $js['locale'] = $locale;
        $js['provinces'] = \Cache::get('provinces');
        $js['city'] = \Cache::get('city:' . $js['provinces'][0]['value']['id']);
        JavaScript::put($js);
        return view('frontend.shop.login');
    }

    public function brandPage($name)
    {
        $brand = User::with('profileable')->whereUsername($name)->active()->first();
        if (!$brand) {
            abort(404);
        }
        $locale = LaravelLocalization::getCurrentLocale();
        JavaScript::put([
            'locale' => $locale,
            'brand' => $brand
        ]);
        return view('frontend.shop.brand', compact('brand'));
    }

    public function bantuan()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        JavaScript::put([
            'locale' => $locale
        ]);
        return view('frontend.pages.generalQ');
    }

    public function privasi()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        JavaScript::put([
            'locale' => $locale
        ]);
        return view('frontend.pages.privacy');
    }

    public function prosespengiriman()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        JavaScript::put([
            'locale' => $locale
        ]);
        return view('frontend.pages.prosespengiriman');
    }

    public function ordertracking()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        JavaScript::put([
            'locale' => $locale
        ]);
        return view('frontend.pages.ordertracking');
    }

    public function pengembalian()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        JavaScript::put([
            'locale' => $locale
        ]);
        return view('frontend.pages.pengembalian');
    }

    public function terms()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        JavaScript::put([
            'locale' => $locale
        ]);
        return view('frontend.pages.Terms');
    }

    public function activationPage()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        JavaScript::put([
            'counter' => 10,
            'locale' => $locale
        ]);
        return view('frontend.shop.activationForm');
    }

    public function orderCreated()
    {
        if (!\Session::has('order:created')) {
            return redirect()->to('/');
        }

        $order = \Session::pull('order:created');

        $locale = LaravelLocalization::getCurrentLocale();
        \Cart::destroy();
        JavaScript::put([
            'locale' => $locale
        ]);
        return view('frontend.pages.orderplaced', compact('order'));
    }

    public function newArrival()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        JavaScript::put([
            'locale' => $locale
        ]);
        return view('frontend.shop.newarrival');
    }

    public function brands()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        JavaScript::put([
            'locale' => $locale
        ]);
        return view('frontend.shop.brands');
    }

    public function forgotpassword()
    {
        return view('frontend.shop.forgotpassword');
    }

    public function setPassword()
    {
        if (!\Session::has('user:forgot')) {
            abort(403);
        }
        $locale = LaravelLocalization::getCurrentLocale();
        JavaScript::put([
            'locale' => $locale
        ]);
        return view('frontend.shop.setpassword');
    }

    public function confirmOrder()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        JavaScript::put([
            'locale' => $locale
        ]);
        return view('frontend.shop.confirmOrder');
    }

    public function contactUs()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        JavaScript::put([
            'locale' => $locale
        ]);
        return view('frontend.pages.contactUs');
    }

    public function category()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        JavaScript::put([
            'locale' => $locale
        ]);
        return view('frontend.shop.category');
    }

    public function sale()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        JavaScript::put([
            'locale' => $locale
        ]);

        return view('frontend.shop.sale');
    }
}