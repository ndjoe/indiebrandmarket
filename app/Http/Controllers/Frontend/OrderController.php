<?php


namespace OBID\Http\Controllers\Frontend;


use Carbon\Carbon;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use OBID\Helpers\MoneyService;
use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Order\NewCheckoutOrderRequest;
use OBID\Http\Requests\Order\OrderTrackingRequest;
use OBID\Jobs\ConfirmPaypalPayment;
use OBID\Jobs\ProcessOnlinePuchase;
use OBID\Models\Currency;
use OBID\Models\Order;
use Omnipay\Common\CreditCard;
use Omnipay\Omnipay;
use Session;

class OrderController extends Controller
{
    public function create(NewCheckoutOrderRequest $request)
    {
        $req = $request->all();
        $locale = LaravelLocalization::getCurrentLocale();
        $moneySymbol = $locale === 'id' ? 'IDR' : 'USD';
        $cart = \Cart::content();
        $netTotal = 0;
        $data['address'] = [];
        $data['items'] = [];
        $data['order'] = [];
        $coupon = Session::get('coupon', ['couponId' => 0, 'value' => 0, 'currency' => 'IDR']);
        $uniqueTrans = Session::pull('uniqueTrans');
        $weightTotal = 0;
        foreach ($cart as $c) {
            $adjusment = 100 - $c['options']['discount'];
            $adjusment = $adjusment / 100;
            $price = new MoneyService($c['price'], $moneySymbol);
            $netTotal += $price->getRupiahValue() * $c['qty'] * $adjusment;
            $weightTotal += $c['options']['weight'];
            array_push($data['items'], [
                'productId' => $c['options']['productId'],
                'itemId' => $c['id'],
                'sizeId' => $c['options']['sizeId'],
                'qty' => $c['qty'],
                'adjusment' => $price->getRupiahValue() * $c['qty'] * $adjusment
            ]);
        }

        $shipping = new MoneyService($req['shippingCost'], $moneySymbol);
        $couponMoney = new MoneyService($coupon['value'], $coupon['currency']);
        $coupon['value'] = $couponMoney->getRupiahValue();
        $data['order'] = [
            'netTotal' => $netTotal,
            'taxTotal' => 0,
            'shippingCost' => $shipping->getRupiahValue(),
            'shippingType' => $req['shippingName'],
            'adjusmentCoupon' => $coupon['value'],
            'total_weight' => $weightTotal,
            'expired_at' => Carbon::now()->addYear(1),
            'type' => 'online',
            'payment_method' => $req['payment'],
            'currency' => $moneySymbol,
            'currency_value' => Currency::find(1)->value,
            'uniqueCost' => $uniqueTrans
        ];
        $data['address'] = [
            'nama' => $req['nama'],
            'alamat' => $req['alamat'],
            'kodepos' => $req['kodepos'],
            'negara' => $req['negara'],
            'provinsi' => $req['provinsi'],
            'kota' => $req['kota'],
            'nohp' => $req['nohp']
        ];
        if (\Auth::check()) {
            $data['userId'] = \Auth::id();
            if (\Auth::user()->profileable->birthday) {
                if (\Auth::user()->profileable->birthday->isBirthday(Carbon::now())) {
                    $data['order']['birthdayAdjusment'] = $netTotal * 5 / 100;
                }
            }
        } else {
            $data['userId'] = 0;
        }

        $data['couponId'] = $coupon['couponId'];

        $order = $this->dispatch(new ProcessOnlinePuchase($data));
        Session::forget('coupon');
        Session::set('order:created', $order);

        if ($locale === 'us') {
            if ($req['payment'] === 'paypal') {
                $gateway = Omnipay::create('PayPal_Express');
                $gateway->setUsername('rumput.tetangga-facilitator_api1.gmail.com');
                $gateway->setPassword('TJXE2J64CESA8VXR');
                $gateway->setSignature('AFcWxV21C7fd0v3bYYYRCpSSRl31A89PO.NLTHWX2z1WCvvobt1Naerb');
                $gateway->setTestMode(true);
                $dollarMoney = new MoneyService($order->grossTotal, 'IDR');
                $params = [
                    'amount' => $dollarMoney->getDollarValue(),
                    'headerImageUrl' => 'http://originalbrands.co.id/wp-content/uploads/2015/11/LOGO-WEBSITE-300x30.png',
                    'cancelUrl' => url('order/cancel'),
                    'currency' => 'USD',
                    'returnUrl' => url('order/paid')
                ];

                Session::set('order:params', $params);
                $payment = $gateway->purchase($params)->send();

                if ($payment->isRedirect()) {
                    return response()->json(['created' => true, 'paypal' => true, 'redirect' => $payment->getRedirectUrl()]);
                }
            }
        } else {
            if ($req['payment'] === 'ebank') {
                $gateway = Omnipay::create('Veritrans_VTWeb');
                $gateway->setServerKey('VT-server-DIkm96JjtSizZl2acwxdWtw0');
                $gateway->setEnvironment('sandbox');

                $params = [
                    'transactionId' => $order->order_code,
                    'amount' => $order->grossTotal,
                    'currency' => 'IDR',
                    'card' => new CreditCard(),
                    'vtwebconfiguration' => [
                        'credit_card_3d_secure' => true,
                        "finish_redirect_url" => "http://originalbrands.localapp/payment/success",
                        "unfinish_redirect_url" => "http://originalbrands.localapp/payment/pending",
                        "error_redirect_url" => "http://originalbrands.localapp/payment/error"
                    ]
                ];

                Session::set('order:params', $params);
                $payment = $gateway->purchase($params)->send();

                if ($payment->isRedirect()) {
                    return response()->json(['created' => true, 'veritrans' => true, 'redirect' => $payment->getRedirectUrl()]);
                }
            }
        }

        Session::forget('coupon');
        return response()->json(['created' => true, 'redirect' => url('/order-created')]);
    }

    public function detail(OrderTrackingRequest $request)
    {
        $req = $request->all();

        $order = Order::whereOrderCode($req['nomororder'])
            ->with(
                'user',
                'items',
                'items.productItem.product',
                'items.productItem.size',
                'shipping',
                'items.productItem.product.author',
                'address'
            )
            ->first();

        return view('frontend.pages.ordertracking', compact('order'));
    }

    public function paidOrder()
    {
        $gateway = Omnipay::create('PayPal_Express');
        $gateway->setUsername('rumput.tetangga-facilitator_api1.gmail.com');
        $gateway->setPassword('TJXE2J64CESA8VXR');
        $gateway->setSignature('AFcWxV21C7fd0v3bYYYRCpSSRl31A89PO.NLTHWX2z1WCvvobt1Naerb');
        $gateway->setTestMode(true);

        $locale = LaravelLocalization::getCurrentLocale();
        $params = Session::pull('order:params');

        $payment = $gateway->completePurchase($params)->send();
        $response = $payment->getData();
        $order = Session::pull('order:created');
        if (isset($response['PAYMENTINFO_0_ACK']) && $response['PAYMENTINFO_0_ACK'] === 'Success') {
            $this->dispatch(new ConfirmPaypalPayment($order->id));
        }

        \JavaScript::put([
            'locale' => $locale
        ]);

        return view('frontend.pages.paidPaypal', compact('order'));
    }
}