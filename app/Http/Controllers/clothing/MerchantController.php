<?php


namespace OBID\Http\Controllers\clothing;


use OBID\Http\Controllers\Controller;
use OBID\Repositories\UserRepository;

class MerchantController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepo;

    /**
     * @param UserRepository $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function index()
    {

    }

}