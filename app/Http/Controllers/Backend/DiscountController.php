<?php


namespace OBID\Http\Controllers\Backend;


use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Discount\UpdateDiscountRequest;
use OBID\Repositories\DiscountRepository;

class DiscountController extends Controller
{
    /**
     * @var DiscountRepository
     */
    protected $discountRepo;

    /**
     * @param DiscountRepository $discountRepo
     */
    public function __construct(DiscountRepository $discountRepo)
    {
        $this->discountRepo = $discountRepo;
    }

    public function updateDiscount(UpdateDiscountRequest $request)
    {
        $req = $request->all();

        $input = [
            'value' => $req['percent'],
            'expired_at' => $req['expired']
        ];

        $this->discountRepo->edit($input, $req['discountId']);

        return response()->json(['updated' => true]);
    }
}