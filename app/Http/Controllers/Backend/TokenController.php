<?php

namespace OBID\Http\Controllers\Backend;


use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\RegToken\CreateRegTokenRequest;
use OBID\Repositories\TokenRepository;

class TokenController extends Controller
{
    private $tokenRepo;

    /**
     * TokenController constructor.
     * @param TokenRepository $tokenRepo
     */
    public function __construct(TokenRepository $tokenRepo)
    {
        $this->tokenRepo = $tokenRepo;
    }

    public function index()
    {
        $page = \Input::get('page', 1);

        return $this->tokenRepo->getTokensPaginated($page);
    }


    public function create(CreateRegTokenRequest $request)
    {
        $input = [
            'token' => $request->get('token'),
            'expired_at' => $request->get('expired')
        ];

        $created = $this->tokenRepo->createToken($input);

        if ($created) {
            return response()->json(['created' => true]);
        }

        return response()->json(['created' => false]);
    }

    public function delete($id)
    {
        $deleted = $this->tokenRepo->deleteToken($id);

        if ($deleted) {
            return response()->json(['deleted' => true]);
        }

        return response()->json(['deleted' => false]);
    }
}