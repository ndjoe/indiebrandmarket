<?php

namespace OBID\Http\Controllers\Backend;


use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Currency\SetCurrencyRequest;
use OBID\Models\Currency;

class CurrencyController extends Controller
{
    public function setCurrency(SetCurrencyRequest $request)
    {
        $req = $request->all();

        $currency = Currency::find(1);

        $currency->value = $req['new'];

        if ($currency->save()) {
            return response()->json(['newValue' => $currency->value]);
        }

        return response()->json(['updated' => false]);
    }

    public function getCurrency()
    {
        $currency = Currency::find(1);

        return response()->json(['value' => $currency->value]);
    }
}