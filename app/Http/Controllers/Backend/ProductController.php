<?php


namespace OBID\Http\Controllers\Backend;


use Illuminate\Support\Str;
use OBID\Helpers\ImageService;
use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Product\CreateProductRequest;
use OBID\Http\Requests\Product\EditProductRequest;
use OBID\Jobs\Product\ProcessNewProduct;
use OBID\Jobs\Product\UpdateProduct;
use OBID\Models\Color;
use OBID\Models\Size;
use OBID\Models\Subcategory;
use OBID\Repositories\DiscountRepository;
use OBID\Repositories\ProductRepository;

class ProductController extends Controller
{
    /**
     * @var ProductRepository
     */
    protected $productRepo;
    /**
     * @var DiscountRepository
     */
    protected $discountRepo;

    /**
     * @param ProductRepository $productRepo
     * @param DiscountRepository $discountRepository
     */
    public function __construct(ProductRepository $productRepo, DiscountRepository $discountRepository)
    {
        $this->productRepo = $productRepo;
        $this->discountRepo = $discountRepository;
    }

    public function index()
    {
        $page = \Input::get('page', 1);
        $filter = \Input::get('filter', 0);
        $query = \Input::get('query', null);

        $products = $this->productRepo->getProductsPaginated($page, $query, $filter, 'desc', 'id');

        return \Response::json($products);
    }

    public function store(CreateProductRequest $request)
    {
        $input = $request->all();
        $imageService = new ImageService;
        $images = [];

        if ($request->hasFile('photo-1')) {
            if ($imageService->checkImageSize($request->file('photo-1'), 599, 799)) {
                array_push($images, [
                    'type' => 'photo-1',
                    'file' => $request->file('photo-1')
                ]);
            } else {
                return response()->json(
                    [
                        'created' => false,
                        'reason' => 'Gambar utama tidak valid mohon cek lagi gambar anda'
                    ]
                );
            }
        } else {
            array_push($images, [
                'type' => 'photo-1'
            ]);
        }

        if ($request->hasFile('photo-2')) {
            if ($imageService->checkImageSize($request->file('photo-2'), 599, 799)) {
                array_push($images, [
                    'type' => 'photo-2',
                    'file' => $request->file('photo-2')
                ]);
            } else {
                return response()->json(
                    [
                        'created' => false,
                        'reason' => 'Gambar 2 tidak valid mohon cek lagi gambar anda'
                    ]
                );
            }
        } else {
            array_push($images, [
                'type' => 'photo-2'
            ]);
        }

        if ($request->hasFile('photo-3')) {
            if ($imageService->checkImageSize($request->file('photo-3'), 599, 799)) {
                array_push($images, [
                    'type' => 'photo-3',
                    'file' => $request->file('photo-3')
                ]);
            } else {
                return response()->json(
                    [
                        'created' => false,
                        'reason' => 'Gambar 3 tidak valid mohon cek lagi gambar anda'
                    ]
                );
            }
        } else {
            array_push($images, [
                'type' => 'photo-3'
            ]);
        }

        if ($request->hasFile('photo-4')) {
            if ($imageService->checkImageSize($request->file('photo-4'), 599, 799)) {
                array_push($images, [
                    'type' => 'photo-4',
                    'file' => $request->file('photo-4')
                ]);
            } else {
                return response()->json(
                    [
                        'created' => false,
                        'reason' => 'Gambar 4 tidak valid mohon cek lagi gambar anda'
                    ]
                );
            }
        } else {
            array_push($images, [
                'type' => 'photo-4'
            ]);
        }

        if ($request->hasFile('photo-5')) {
            if ($imageService->checkImageSize($request->file('photo-5'), 599, 799)) {
                array_push($images, [
                    'type' => 'photo-5',
                    'file' => $request->file('photo-5')
                ]);
            } else {
                return response()->json(
                    [
                        'created' => false,
                        'reason' => 'Gambar 5 tidak valid mohon cek lagi gambar anda'
                    ]
                );
            }
        } else {
            array_push($images, [
                'type' => 'photo-5'
            ]);
        }

        if ($request->hasFile('photo-6')) {
            if ($imageService->checkImageSize($request->file('photo-6'), 599, 799)) {
                array_push($images, [
                    'type' => 'photo-6',
                    'file' => $request->file('photo-6')
                ]);
            } else {
                return response()->json(
                    [
                        'created' => false,
                        'reason' => 'Gambar 6 tidak valid mohon cek lagi gambar anda'
                    ]
                );
            }
        } else {
            array_push($images, [
                'type' => 'photo-6'
            ]);
        }

        if ($request->hasFile('photo-7')) {
            if ($imageService->checkImageSize($request->file('photo-7'), 599, 799)) {
                array_push($images, [
                    'type' => 'photo-7',
                    'file' => $request->file('photo-7')
                ]);
            } else {
                return response()->json(
                    [
                        'created' => false,
                        'reason' => 'Gambar 7 tidak valid mohon cek lagi gambar anda'
                    ]
                );
            }
        } else {
            array_push($images, [
                'type' => 'photo-7'
            ]);
        }

        if ($request->hasFile('photo-8')) {
            if ($imageService->checkImageSize($request->file('photo-8'), 599, 799)) {
                array_push($images, [
                    'type' => 'photo-8',
                    'file' => $request->file('photo-8')
                ]);
            } else {
                return response()->json(
                    [
                        'created' => false,
                        'reason' => 'Gambar 8 tidak valid mohon cek lagi gambar anda'
                    ]
                );
            }
        } else {
            array_push($images, [
                'type' => 'photo-8'
            ]);
        }

        if (!is_numeric($input['color'])) {
            $input['color'] = Color::firstOrCreate(['text' => Str::lower($input['color'])])->id;
        }

        if (!is_numeric($input['subkategori'])) {
            $input['subkategori'] = Subcategory::firstOrCreate([
                'text' => Str::lower($input['subkategori']),
                'merchant_id' => $input['merchant']
            ])->id;
        }

        $product = [
            'name' => $input['nama'],
            'price' => $input['harga'],
            'category' => $input['kategori'],
            'color' => $input['color'],
            'subcategory' => $input['subkategori'],
            'weight' => $input['berat'],
            'author' => $input['merchant'],
            'description' => $input['description'],
            'gender' => $input['gender']
        ];

        $discount = [
            'value' => $input['percent'],
            'expired_at' => $input['expired']
        ];

        $items = [];

        if (array_key_exists('size', $input)) {
            for ($i = 0; $i < count($input['size']); $i++) {
                if (!is_numeric($input['size'][$i])) {
                    $input['size'][$i] = Size::create(['text' => Str::lower($input['size'][$i])])->id;
                }

                $items[$i] = [
                    'size' => $input['size'][$i],
                    'qty' => $input['qty'][$i],
                    'desc' => $input['desc'][$i]
                ];
            }
        }

        $saved = $this->dispatch(new ProcessNewProduct(compact('product', 'images', 'discount', 'items')));

        if ($saved) {
            return \Response::json(['created' => true, 'redirect' => url('/products')], 201);
        }

        return \Response::json(['created' => false], 500);
    }

    public function update(EditProductRequest $request, $id)
    {
        $input = $request->all();
        $imageService = new ImageService;
        $images = [];

        if ($request->hasFile('photo-1')) {
            if ($imageService->checkImageSize($request->file('photo-1'), 599, 799)) {
                array_push($images, [
                    'type' => 'photo-1',
                    'file' => $request->file('photo-1')
                ]);
            } else {
                return response()->json(
                    [
                        'updated' => false,
                        'reason' => 'Gambar utama tidak valid mohon cek lagi gambar anda'
                    ]
                );
            }
        }

        if ($request->hasFile('photo-2')) {
            if ($imageService->checkImageSize($request->file('photo-2'), 599, 799)) {
                array_push($images, [
                    'type' => 'photo-2',
                    'file' => $request->file('photo-2')
                ]);
            } else {
                return response()->json(
                    [
                        'updated' => false,
                        'reason' => 'Gambar 2 tidak valid mohon cek lagi gambar anda'
                    ]
                );
            }
        } else {
            if (array_key_exists('delphoto-2', $input)) {
                array_push($images, [
                    'type' => 'photo-2'
                ]);
            }
        }

        if ($request->hasFile('photo-3')) {
            if ($imageService->checkImageSize($request->file('photo-3'), 599, 799)) {
                array_push($images, [
                    'type' => 'photo-3',
                    'file' => $request->file('photo-3')
                ]);
            } else {
                return response()->json(
                    [
                        'updated' => false,
                        'reason' => 'Gambar 3 tidak valid mohon cek lagi gambar anda'
                    ]
                );
            }
        } else {
            if (array_key_exists('delphoto-3', $input)) {
                array_push($images, [
                    'type' => 'photo-3'
                ]);
            }
        }

        if ($request->hasFile('photo-4')) {
            if ($imageService->checkImageSize($request->file('photo-4'), 599, 799)) {
                array_push($images, [
                    'type' => 'photo-4',
                    'file' => $request->file('photo-4')
                ]);
            } else {
                return response()->json(
                    [
                        'updated' => false,
                        'reason' => 'Gambar 4 tidak valid mohon cek lagi gambar anda'
                    ]
                );
            }
        } else {
            if (array_key_exists('delphoto-4', $input)) {
                array_push($images, [
                    'type' => 'photo-4'
                ]);
            }
        }

        if ($request->hasFile('photo-5')) {
            if ($imageService->checkImageSize($request->file('photo-5'), 599, 799)) {
                array_push($images, [
                    'type' => 'photo-5',
                    'file' => $request->file('photo-5')
                ]);
            } else {
                return response()->json(
                    [
                        'updated' => false,
                        'reason' => 'Gambar 5 tidak valid mohon cek lagi gambar anda'
                    ]
                );
            }
        } else {
            if (array_key_exists('delphoto-5', $input)) {
                array_push($images, [
                    'type' => 'photo-5'
                ]);
            }
        }

        if ($request->hasFile('photo-6')) {
            if ($imageService->checkImageSize($request->file('photo-6'), 599, 799)) {
                array_push($images, [
                    'type' => 'photo-6',
                    'file' => $request->file('photo-6')
                ]);
            } else {
                return response()->json(
                    [
                        'updated' => false,
                        'reason' => 'Gambar 6 tidak valid mohon cek lagi gambar anda'
                    ]
                );
            }
        } else {
            if (array_key_exists('delphoto-6', $input)) {
                array_push($images, [
                    'type' => 'photo-6'
                ]);
            }
        }

        if ($request->hasFile('photo-7')) {
            if ($imageService->checkImageSize($request->file('photo-7'), 599, 799)) {
                array_push($images, [
                    'type' => 'photo-7',
                    'file' => $request->file('photo-7')
                ]);
            } else {
                return response()->json(
                    [
                        'updated' => false,
                        'reason' => 'Gambar 7 tidak valid mohon cek lagi gambar anda'
                    ]
                );
            }
        } else {
            if (array_key_exists('delphoto-7', $input)) {
                array_push($images, [
                    'type' => 'photo-7'
                ]);
            }
        }

        if ($request->hasFile('photo-8')) {
            if ($imageService->checkImageSize($request->file('photo-8'), 599, 799)) {
                array_push($images, [
                    'type' => 'photo-8',
                    'file' => $request->file('photo-8')
                ]);
            } else {
                return response()->json(
                    [
                        'updated' => false,
                        'reason' => 'Gambar 8 tidak valid mohon cek lagi gambar anda'
                    ]
                );
            }
        } else {
            if (array_key_exists('delphoto-8', $input)) {
                array_push($images, [
                    'type' => 'photo-8'
                ]);
            }
        }

        if (!is_numeric($input['color'])) {
            $input['color'] = Color::firstOrCreate(['text' => Str::lower($input['color'])])->id;
        }

        if (!is_numeric($input['subkategori'])) {
            $input['subkategori'] = Subcategory::firstOrCreate([
                'text' => Str::lower($input['subkategori']),
                'merchant_id' => $input['merchant']
            ])->id;
        }

        $product = [
            'name' => $input['nama'],
            'price' => $input['harga'],
            'category' => $input['kategori'],
            'color' => $input['color'],
            'subcategory' => $input['subkategori'],
            'weight' => $input['berat'],
            'author' => $input['merchant'],
            'description' => $input['description'],
            'sizing' => $input['sizing'],
            'gender' => $input['gender']
        ];

        $discount = [
            'value' => $input['percent'],
            'expired_at' => $input['expired']
        ];

        $updated = $this->dispatch(new UpdateProduct($id, compact('product', 'images', 'discount')));

        if ($updated) {
            return response()->json(['updated' => true, 'redirect' => url('/products')]);
        }
        return \Response::json(['updated' => false], 500);
    }

    public function delete($id)
    {
        $product = $this->productRepo->findProductById($id);

        if ($product->delete()) {
            return \Response::json(['deleted' => true], 200);
        }

        return \Response::json(['deleted' => false], 500);
    }

    public function publish($id)
    {
        $published = $this->productRepo->publishProduct($id);

        if ($published) {
            return \Response::json(['published' => true], 200);
        }

        return \Response::json(['published' => false], 200);
    }

    public function unpublish($id)
    {
        $unpublished = $this->productRepo->unpublishProduct($id);

        if ($unpublished) {
            return \Response::json(['unpublished' => true], 200);
        }

        return \Response::json(['unpublished' => false], 200);
    }

    public function detail($id)
    {
        $product = $this->productRepo->findProductById($id);
        $product->load(
            'items',
            'items.size',
            'discount',
            'images',
            'author',
            'category',
            'subcategory',
            'color'
        );

        return response()->json($product);
    }

    public function getItems($id)
    {
        $product = $this->productRepo->findProductById($id);

        return response()->json($product->items);
    }
}