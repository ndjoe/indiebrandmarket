<?php

namespace OBID\Http\Controllers\Backend;


use OBID\Http\Controllers\Controller;
use OBID\Repositories\OrderAddressRepository;

class GuestController extends Controller
{
    /**
     * @var OrderAddressRepository
     */
    protected $guestRepo;

    /**
     * GuestController constructor.
     * @param OrderAddressRepository $guestRepo
     */
    public function __construct(OrderAddressRepository $guestRepo)
    {
        $this->guestRepo = $guestRepo;
    }

    public function index()
    {
        $page = \Input::get('page', 1);
        $query = \Input::get('query', null);

        $guests = $this->guestRepo->getPaginated($page, $query, 'desc', 'id');

        return response()->json($guests);
    }
}