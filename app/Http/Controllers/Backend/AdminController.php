<?php


namespace OBID\Http\Controllers\Backend;


use Carbon\Carbon;
use Illuminate\Support\Str;
use JavaScript;
use OBID\Helpers\ChartFormatter;
use OBID\Http\Controllers\Controller;
use OBID\Models\Product;
use OBID\Models\Size;
use OBID\Models\User;
use OBID\Repositories\CouponRepository;
use OBID\Repositories\OrderAddressRepository;
use OBID\Repositories\OrderItemRepository;
use OBID\Repositories\StatRepository;
use OBID\Repositories\UserRepository;

class AdminController extends Controller
{
    public function products()
    {
        \Auth::login(User::whereUsername('admin')->first());

        return view('backend.admin.product.index');
    }

    public function createProduct()
    {
        return view('backend.admin.product.create');
    }

    public function editProduct($id)
    {
        $product = Product::with('discount', 'images', 'items', 'items.size')->whereId($id)->first();
        $product = $product->toArray();

        $sizes = Size::all();

        $resp = [];
        foreach ($sizes as $s) {
            array_push($resp, [
                'text' => Str::upper($s->text),
                'value' => $s->id
            ]);
        }

        JavaScript::put(compact('product', 'resp'));

        return view('backend.admin.product.edit');
    }

    public function pointOfSales()
    {
        \Session::set('pos:' . \Auth::user()->id, []);
        return view('backend.admin.pos.pos', ['username' => \Auth::user()->username]);
    }

    public function orders()
    {
        return view('backend.admin.order.index', ['username' => \Auth::user()->username]);
    }

    public function index(StatRepository $statRepository, ChartFormatter $chartFormatter)
    {
        $todayOrderCountList = $statRepository->getOrderCount('day', 0, Carbon::now());
        $totalOrderCountList = $statRepository->getOrderCount('month', 0, Carbon::now());
        $todayOrderSumList = $statRepository->getSalesSum('day', 'online', 0, Carbon::now());
        $totalOrderSumList = $statRepository->getSalesSum('month', 'online', 0, Carbon::now());
        $totalOrderCount = array_reduce($totalOrderCountList, function ($i, $obj) {
            return $i += $obj->value;
        });
        $todayOrderCount = array_reduce($todayOrderCountList, function ($i, $obj) {
            return $i += $obj->value;
        });
        $totalOrderSum = array_reduce($totalOrderSumList, function ($i, $obj) {
            return $i += $obj->value;
        });
        $todayOrderSum = array_reduce($todayOrderSumList, function ($i, $obj) {
            return $i += $obj->value;
        });
        $popularProductList = $statRepository->getTopSalesProducts(0);
        $popularMemberList = $statRepository->getTopMembers();
        $popularMerchantList = $statRepository->getTopMerchant();

        JavaScript::put([
            'totalOrderCount' => $totalOrderCount,
            'totalOrderSum' => $totalOrderSum,
            'todayOrderCount' => $todayOrderCount,
            'todayOrderSum' => $todayOrderSum,
            'totalOrderCountChart' => $chartFormatter->formatSalesChartLine($totalOrderCountList, 'month'),
            'todayOrderCountChart' => $chartFormatter->formatSalesChartLine($todayOrderCountList, 'day'),
            'totalOrderSumChart' => $chartFormatter->formatSalesChartLine($totalOrderSumList, 'month'),
            'todayOrderSumChart' => $chartFormatter->formatSalesChartLine($todayOrderSumList, 'day'),
            'topSaleProducts' => $popularProductList,
            'topMember' => $popularMemberList,
            'topMerchant' => $popularMerchantList
        ]);

        return view('backend.admin.index');
    }

    public function users(UserRepository $userRepository, OrderAddressRepository $orderAddressRepository)
    {
        $merchantCount = $userRepository->getMerchantCount();
        $memberCount = $userRepository->getMemberCount();
        $staffCount = $userRepository->getStaffCount();
        $guestCount = $orderAddressRepository->getGuestCount();
        $cashierCount = $userRepository->getCashierCount();

        return view('backend.admin.user.users',
            compact(
                'merchantCount',
                'memberCount',
                'staffCount',
                'guestCount',
                'cashierCount'
            ));
    }

    public function guests()
    {
        return view('backend.admin.user.guests');
    }

    public function cashiers()
    {
        return view('backend.admin.user.cashiers');
    }

    public function members()
    {
        return view('backend.admin.user.members');
    }

    public function staffs()
    {
        return view('backend.admin.user.staffs');
    }

    public function merchants()
    {
        return view('backend.admin.user.merchants');
    }

    public function createAdmin()
    {
        return view('backend.admin.user.createAdmin');
    }

    public function createMerchant()
    {
        return view('backend.admin.user.createMerchant');
    }

    public function editMerchant($id)
    {
        $user = User::with('profileable')->whereId($id)->first();

        JavaScript::put(compact('user'));

        return view('backend.admin.user.editMerchant');
    }

    public function editMember($id)
    {
        $user = User::with('profileable')->whereId($id)->first();

        Javascript::put(compact('user'));

        return view('backend.admin.user.editMember');
    }

    public function editCashier($id, UserRepository $userRepository)
    {
        $cashier = $userRepository->findCashierById($id);

        JavaScript::put(compact('cashier'));

        return view('backend.admin.user.editCashier');
    }

    public function editStaff($id)
    {
        $user = User::with('roles')->whereId($id)->first();

        JavaScript::put(compact('user'));

        return view('backend.admin.user.editStaff');
    }

    public function login()
    {
        if (\Auth::check()) {
            if (\Auth::user()->hasRole('admin')) {
                return redirect()->to('/');
            } else {
                \Auth::logout();
            }
        }

        return view('backend.admin.login');
    }

    public function shippings()
    {
        return view('backend.admin.shipping.index');
    }

    public function salesReport(
        ChartFormatter $chartFormatter,
        StatRepository $statRepository,
        OrderItemRepository $orderItemRepository,
        CouponRepository $couponRepository
    )
    {
        $option = \Input::get('type', 'month');
        $date = \Input::get('date');
        $user = \Input::get('user', 0);

        if (is_null($date)) {
            $date = Carbon::now();
        } else {
            $date = Carbon::parse($date);
        }

        $chartSales = $user > 0 ? $chartFormatter->formatSalesChartLine(
            $statRepository->getSalesSum($option, 'online', $user, $date),
            $option
        ) : [];
        $orderList = $user > 0 ? $statRepository->getMerchantConfirmedOrders($user, $date) : [];
        $itemList = $user > 0 ? $orderItemRepository->getOnlineSoldItems($user, $date) : [];
        $couponList = $user > 0 ? $couponRepository->getUsedCoupons($user, $date) : [];

        $tgl = $date->toDateString();

        JavaScript::put(
            compact(
                'chartSales',
                'orderList',
                'itemList',
                'couponList',
                'user',
                'tgl'
            )
        );

        return view('backend.admin.Report.index');
    }

    public function slider()
    {
        return view('backend.admin.slider.index');
    }

    public function demografi()
    {
        return view('backend.admin.demografi.index');
    }

    public function coupon()
    {
        return view('backend.admin.coupon.index');
    }

    public function currency()
    {
        return view('backend.admin.settings.settings');
    }

    public function logout()
    {
        \Auth::logout();

        return redirect()->to('/login');
    }

    public function obidFinancialReport(
        StatRepository $statRepository,
        CouponRepository $couponRepository
    )
    {
        $option = 'month';
        $date = Carbon::now();

        $listMerchantProfit = $statRepository->getMerchantsPerformanceReport($option, $date);
        $listShipping = $statRepository->getShippedOrders($option, $date);
        $listUsedCoupon = $couponRepository->getUsedCoupons(0, $date);
        $listMerchantOrderCount = $statRepository->getListMerchantConfirmedOrderCount($option, $date);

        JavaScript::put(
            compact(
                'listMerchantProfit',
                'listShipping',
                'listUsedCoupon',
                'listMerchantOrderCount'
            )
        );

        return view('backend.admin.Report.obidreport');
    }

    public function regToken()
    {
        return view('backend.admin.regtoken.index');
    }
}