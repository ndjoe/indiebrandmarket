<?php


namespace OBID\Http\Controllers\Backend;


use OBID\Http\Controllers\Controller;
use OBID\Models\Size;
use Illuminate\Support\Str;

class SizeController extends Controller
{
    public function getSizesForSelect()
    {
        $sizes = Size::all();

        $resp = [];
        foreach ($sizes as $s) {
            array_push($resp, [
                'text' => Str::upper($s->text),
                'value' => $s->id
            ]);
        }

        return response()->json($resp);
    }
}