<?php


namespace OBID\Http\Controllers\Backend;


use OBID\Events\Order\OrderShipped;
use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Shipping\NewShippingRequest;
use OBID\Models\Order;
use OBID\Models\Shipping;
use OBID\Repositories\OrderRepository;

class ShippingController extends Controller
{
    /**
     * @var OrderRepository
     */
    protected $orderRepo;

    /**
     * @param OrderRepository $orderRepo
     */
    public function __construct(OrderRepository $orderRepo)
    {
        $this->orderRepo = $orderRepo;
    }

    public function index()
    {
        $page = \Input::get('page', 1);
        $query = \Input::get('query', null);

        $orders = $this->orderRepo->getConfirmedOrdersPaginated($page, $query, 'desc', 'id');

        return response()->json($orders);
    }

    public function submitResi(NewShippingRequest $request)
    {
        $req = $request->all();
        $order = Order::whereId($req['orderId'])->first();
        $shipping = new Shipping;
        $shipping->no_resi = $req['noResi'];
        $order->shipping()->save($shipping);
        event(new OrderShipped($shipping, $order, $order->user));
        return response()->json(['created' => true], 201);
    }
}