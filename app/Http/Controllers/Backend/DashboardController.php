<?php


namespace OBID\Http\Controllers\Backend;


use Carbon\Carbon;
use OBID\Helpers\ChartFormatter;
use OBID\Http\Controllers\Controller;
use OBID\Repositories\OrderItemRepository;
use OBID\Repositories\OrderRepository;
use OBID\Repositories\StatRepository;
use OBID\Repositories\UserRepository;

class DashboardController extends Controller
{
    /**
     * @var OrderItemRepository
     */
    protected $orderItemRepo;

    /**
     * @var UserRepository
     */
    protected $userRepo;

    /**
     * @var OrderRepository
     */
    protected $orderRepo;

    protected $statRepo;

    /**
     * @param OrderItemRepository $orderItemRepo
     * @param UserRepository $userRepo
     * @param OrderRepository $orderRepository
     * @param StatRepository $statRepository
     */
    public function __construct(
        OrderItemRepository $orderItemRepo,
        UserRepository $userRepo,
        OrderRepository $orderRepository,
        StatRepository $statRepository
    )
    {
        $this->orderItemRepo = $orderItemRepo;
        $this->userRepo = $userRepo;
        $this->orderRepo = $orderRepository;
        $this->statRepo = $statRepository;
    }

    public function salesReport(ChartFormatter $chartFormatter)
    {
        $option = \Input::get('time', 'month');
        $date = Carbon::now();
        $type = \Input::get('type', 'all');
        $user = \Input::get('merchant', 0);

        $todayOrderSumList = $this->statRepo->getSalesSum($option, 'online', $user, $date);


        $report = $chartFormatter->formatSalesChartLine($todayOrderSumList, $option);

        return response()->json($report);
    }

    public function getMerchantMoney()
    {
        $option = \Input::get('time', 'month');
        $user = \Input::get('merchant', 0);
        $onlineSumList = $this->statRepo->getSalesSum($option, 'online', $user, Carbon::now());
        $offlineSumList = $this->statRepo->getSalesSum($option, 'offline', $user, Carbon::now());
        $onlineSumList = array_reduce($onlineSumList, function ($i, $obj) {
            return $i += $obj->value;
        });
        $offlineSum = array_reduce($offlineSumList, function ($i, $obj) {
            return $i += $obj->value;
        });

        return response()->json(compact('onlineSum', 'offlineSum'));
    }

    public function getBrandStat($brandId)
    {
        $user = \Input::get('merchant', 0);

        $buyerIds = $this->orderRepo->getBuyersIdByBrand($brandId);
        $stats = $this->userRepo->getStatsFromUserIds($buyerIds);
        return response()->json($stats);
    }

    public function getUserChart(ChartFormatter $chartFormatter)
    {
        $newAge = $chartFormatter->formatChartDonut($this->statRepo->getNewAgeChart());
        $newGender = $chartFormatter->formatChartDonut($this->statRepo->getNewUserGenderChart());
        $totalAge = $chartFormatter->formatChartDonut($this->statRepo->getTotalAgeChart());
        $totalGender = $chartFormatter->formatChartDonut($this->statRepo->getTotalUserGenderChart());

        return response()->json(compact('newAge', 'newGender', 'totalAge', 'totalGender'));
    }
}