<?php


namespace OBID\Http\Controllers\Backend;


use OBID\Http\Controllers\Controller;
use OBID\Repositories\OrderRepository;
use OBID\Repositories\StatRepository;
use OBID\Repositories\UserRepository;

class DemografiController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepo;

    /**
     * @var OrderRepository
     */
    protected $orderRepo;

    protected $statRepo;

    /**
     * @param UserRepository $userRepo
     * @param OrderRepository $orderRepository
     * @param StatRepository $statRepository
     */
    public function __construct(
        UserRepository $userRepo,
        OrderRepository $orderRepository,
        StatRepository $statRepository
    )
    {
        $this->userRepo = $userRepo;
        $this->orderRepo = $orderRepository;
        $this->statRepo = $statRepository;
    }

    public function getUserStat()
    {
        $totalGender = $this->statRepo->getTotalUserGenderChart();
        $newGender = $this->statRepo->getNewUserGenderChart();
        $totalAge = $this->statRepo->getTotalAgeChart();
        $newAge = $this->statRepo->getNewAgeChart();

        return response()->json(compact('totalGender', 'newGender', 'totalAge', 'newAge'));
    }

    public function getBrandStat($brandId)
    {
        $buyerIds = $this->orderRepo->getBuyersIdByBrand($brandId);
        $stats = $this->userRepo->getStatsFromUserIds($buyerIds);
        return response()->json($stats);
    }
}