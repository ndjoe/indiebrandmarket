<?php


namespace OBID\Http\Controllers\Backend;


use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Account\LoginRequest;
use OBID\Models\User;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $req = $request->all();

        $success = false;

        $user = User::whereUsername($req['username'])->whereHas('roles', function ($q) {
            $q->where('name', 'admin');
        })->active()->first();

        if ($user) {
            $success = \Auth::attempt(['username' => $user->username, 'password' => $req['password']]);
        }

        if ($success) {
            return redirect()->to('/');
        }

        return redirect()->back();
    }
}