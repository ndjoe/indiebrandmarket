<?php


namespace OBID\Http\Controllers\Backend;


use OBID\Helpers\ImageService;
use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Account\EditMemberRequest;
use OBID\Http\Requests\Account\EditMerchantRequest;
use OBID\Http\Requests\Account\EditStaffRequest;
use OBID\Http\Requests\Account\NewAdminRequest;
use OBID\Http\Requests\Account\NewMerchantRequest;
use OBID\Models\User;
use OBID\Repositories\UserRepository;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepo;

    /**
     * @param UserRepository $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function index()
    {
        $page = \Input::get('page', 1);
        $filter = \Input::get('filter', 'all');
        $query = \Input::get('query', null);

        $users = $this->userRepo->getUsersPaginated($page, $query, $filter, 'desc', 'id');

        return response()->json($users);
    }

    public function indexCashier()
    {
        $page = \Input::get('page', 1);
        $filter = \Input::get('filter', 0);
        $query = \Input::get('query', null);

        $users = $this->userRepo->getCashiersByMerchant($page, $query, 'desc', 'id', $filter);

        return response()->json($users);
    }

    public function createAdmin(NewAdminRequest $request)
    {
        $req = $request->all();

        $user = $this->userRepo->createNewAdmin($req);

        if ($user) {
            return response()->json(['created' => true, 'redirect' => url('/users/staffs')], 201);
        }

        return response()->json(['created' => false], 500);
    }

    public function createMerchant(NewMerchantRequest $request)
    {
        $req = $request->all();

        $user = $this->userRepo->createNewMerchant($req);

        if ($user) {
            return response()->json(['created' => true, 'redirect' => url('/users')], 201);
        }

        return response()->json(['created' => false], 500);
    }

    public function editMerchant($id, EditMerchantRequest $request)
    {
        $input = $request->all();
        $imageService = new ImageService;

        $serializeForm = [
            'nama' => $input['nama'],
            'kota' => $input['kota'],
            'owner' => $input['owner'],
            'bank' => $input['bank'],
            'provinsi' => $input['provinsi'],
            'norek' => $input['norek'],
            'kodepos' => $input['kodepos'],
            'nohp' => $input['nohp'],
            'about' => $input['about'],
            'alamat' => $input['alamat'],
            'ownerRek' => $input['ownerRek']
        ];

        if ($request->hasFile('banner')) {
            if ($imageService->checkImageSize($request->file('banner'), 999, 299)) {
                $serializeForm['bannerImage'] = $request->file('banner');
            } else {
                return response()->json(
                    [
                        'updated' => false,
                        'reason' => 'Banner Image Tidak Valid Silahkan Check Lagi'
                    ]
                );
            }
        }

        if ($request->hasFile('logo')) {
            if ($imageService->checkImageSize($request->file('logo'), 299, 299)) {
                $serializeForm['logoImage'] = $request->file('logo');
            } else {
                return response()->json(
                    [
                        'updated' => false,
                        'reason' => 'Logo Image Tidak Valid Silahkan Check Lagi'
                    ]
                );
            }
        }

        if ($request->has('password')) {
            $serializeForm['password'] = $input['password'];
        }

        $updated = $this->userRepo->editMerchant($id, $serializeForm);

        if ($updated) {
            return response()->json(['updated' => true, 'redirect' => url('/users/affiliates')]);
        }

        return response()->json(['updated' => false]);
    }

    public function editMember($id, EditMemberRequest $request)
    {
        $req = $request->all();

        $updated = $this->userRepo->editMember($id, $req);

        if ($updated) {
            return response()->json(['updated' => true, 'redirect' => url('/users/customers')], 200);
        }

        return response()->json(['updated' => false], 500);
    }

    public function editStaff($id, EditStaffRequest $request)
    {
        $req = $request->all();

        $updated = $this->userRepo->editStaff($id, $req);

        if ($updated) {
            return response()->json(['updated' => true, 'redirect' => url('/users/staffs')]);
        }

        return response()->json(['updated' => false]);
    }

    public function editCashier($id, EditStaffRequest $request)
    {
        $req = $request->all();

        $updated = $this->userRepo->editStaff($id, $req);

        if ($updated) {
            return response()->json(['updated' => true, 'redirect' => url('/users/cashiers')]);
        }

        return response()->json(['updated' => false]);

    }

    public function getMerchantForSelect()
    {
        $merchants = User::whereHas('roles', function ($q) {
            $q->where('name', 'affiliate');
        })->get();

        $response = [];

        foreach ($merchants as $m) {
            array_push($response, [
                'text' => $m->username,
                'value' => $m->id
            ]);
        }

        return response()->json($response);
    }

    public function activate($id)
    {
        $activated = $this->userRepo->activateAccount($id);

        return response()->json(['activated' => $activated]);
    }

    public function deactivate($id)
    {
        $deactivated = $this->userRepo->deactivateAccount($id);

        return response()->json(['deactivated' => $deactivated]);
    }

    public function delete($id)
    {
        $deleted = $this->userRepo->deleteAccount($id);

        return response()->json(['deleted' => $deleted]);
    }
}