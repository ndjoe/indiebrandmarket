<?php
namespace OBID\Http\Controllers\Backend;


use OBID\Http\Controllers\Controller;
use OBID\Models\Subcategory;
use Illuminate\Support\Str;

class SubcategoryController extends Controller
{
    public function getForSelect()
    {
        $subcategories = Subcategory::all();

        $response = [];

        foreach ($subcategories as $subcat) {
            array_push($response, [
                'text' => Str::upper($subcat->text),
                'value' => $subcat->id
            ]);
        }

        return \Response::json($response);
    }

    public function getSizesForSelect($id)
    {
        $sizes = Subcategory::whereId($id)->first()->sizes;

        $response = [];
        
        foreach ($sizes as $s) {
            array_push($response, [
                'text' => Str::upper($s->text),
                'value' => $s->id
            ]);
        }

        return \Response::json($response);
    }
}