<?php


namespace OBID\Http\Controllers\Backend;


use Carbon\Carbon;
use OBID\Helpers\ChartFormatter;
use OBID\Http\Controllers\Controller;
use OBID\Repositories\CouponRepository;
use OBID\Repositories\OrderItemRepository;
use OBID\Repositories\StatRepository;

class ReportController extends Controller
{
    /**
     * @var OrderItemRepository
     */
    protected $orderItemRepo;

    /**
     * @var StatRepository
     */
    protected $statRepo;

    /**
     * @var CouponRepository
     */
    protected $couponRepo;

    /**
     * @param OrderItemRepository $orderItemRepo
     * @param StatRepository $statRepository
     * @param CouponRepository $couponRepo
     */
    public function __construct(
        OrderItemRepository $orderItemRepo,
        StatRepository $statRepository,
        CouponRepository $couponRepo
    )
    {
        $this->orderItemRepo = $orderItemRepo;
        $this->statRepo = $statRepository;
        $this->couponRepo = $couponRepo;
    }


    public function salesReport(ChartFormatter $chartFormatter)
    {
        $option = \Input::get('type', 'month');
        $date = \Input::get('date');
        $user = \Input::get('user', 0);

        if (is_null($date)) {
            $date = Carbon::now();
        } else {
            $date = Carbon::parse($date);
        }
        $chartSales = $chartFormatter
            ->formatSalesChartLine($this->statRepo->getSalesSum($option, 'online', $user, $date), $option);
        $orderList = $this->statRepo->getMerchantConfirmedOrders($user, $date);
        $itemList = $this->orderItemRepo->getOnlineSoldItems($user, $date);
        $couponList = $this->couponRepo->getUsedCoupons($user, $date);

        return response()->json(compact('chartSales', 'itemList', 'couponList', 'orderList'));
    }


    public function obidReport(
        StatRepository $statRepository,
        CouponRepository $couponRepository
    )
    {
        $option = \Input::get('type', 'month');
        $date = \Input::get('date');

        if (is_null($date)) {
            $date = Carbon::now();
        } else {
            $date = Carbon::parse($date);
        }

        $listMerchantProfit = $statRepository->getMerchantsPerformanceReport($option, $date);
        $listShipping = $statRepository->getShippedOrders($option, $date);
        $listUsedCoupon = $couponRepository->getUsedCoupons(0, $date);
        $listMerchantOrderCount = $statRepository->getListMerchantConfirmedOrderCount($option, $date);

        return response()->json(
            compact(
                'listMerchantProfit',
                'listShipping',
                'listUsedCoupon',
                'listMerchantOrderCount'
            )
        );
    }
}