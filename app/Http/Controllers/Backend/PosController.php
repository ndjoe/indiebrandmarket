<?php


namespace OBID\Http\Controllers\Backend;


use Event;
use OBID\Events\PurchaseByPos;
use OBID\Http\Controllers\Controller;
use OBID\Jobs\NewPosPurchase;
use OBID\Repositories\ProductItemRepository;

class PosController extends Controller
{
    /**
     * @var ProductItemRepository
     */
    protected $repo;

    /**
     * @param ProductItemRepository $repo
     */
    public function __construct(ProductItemRepository $repo)
    {
        $this->repo = $repo;
    }


    public function getProductByBarcode($barcode)
    {
        $item = $this->repo->getItemByBarcode($barcode);

        if (!\Session::has('pos:' . \Auth::user()->id)) {
            \Session::set('pos:' . \Auth::user()->id, []);
        }

        if (!$item) {
            return \Response::json([], 404);
        }

        $carts = \Session::get('pos:' . \Auth::user()->id);

        $key = array_search($item->id, array_column($carts, 'id'), true);

        if (!($key === 0)) {
            array_push($carts, [
                'id' => $item->id,
                'nama' => $item->product->name,
                'harga' => $item->product->price,
                'image' => $item->product->image,
                'qty' => 1
            ]);
        } else {
            $carts[$key]['qty']++;
        }

        \Session::set('pos:' . \Auth::user()->id, $carts);

        return \Response::json($carts);
    }

    public function resetCart()
    {
        \Session::set('pos:' . \Auth::user()->id, []);

        return \Response::json([]);
    }

    public function posPurchase()
    {
        $input = \Session::pull('pos:' . \Auth::user()->id);

        $order = $this->dispatch(new NewPosPurchase($input, \Auth::user()));

        \Session::forget('pos:' . \Auth::user()->id);

        event(new PurchaseByPos($order));

        return \Response::json([], 204);
    }
}