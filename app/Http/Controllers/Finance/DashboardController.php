<?php

namespace OBID\Http\Controllers\Finance;


use Carbon\Carbon;
use JavaScript;
use OBID\Helpers\ChartFormatter;
use OBID\Http\Controllers\Controller;
use OBID\Repositories\StatRepository;

class DashboardController extends Controller
{
    public function index(StatRepository $statRepository, ChartFormatter $chartFormatter)
    {
        $todayOrderCountList = $statRepository->getOrderCount('day', 0, Carbon::now());
        $totalOrderCountList = $statRepository->getOrderCount('month', 0, Carbon::now());
        $todayOrderSumList = $statRepository->getSalesSum('day', 'online', 0, Carbon::now());
        $totalOrderSumList = $statRepository->getSalesSum('month', 'online', 0, Carbon::now());

        $popularProductList = $statRepository->getTopSalesProducts(0);
        $popularMemberList = $statRepository->getTopMembers();
        $popularMerchantList = $statRepository->getTopMerchant();

        JavaScript::put([
            'totalOrderCountChart' => $chartFormatter->formatSalesChartLine($totalOrderCountList, 'month'),
            'todayOrderCountChart' => $chartFormatter->formatSalesChartLine($todayOrderCountList, 'day'),
            'totalOrderSumChart' => $chartFormatter->formatSalesChartLine($totalOrderSumList, 'month'),
            'todayOrderSumChart' => $chartFormatter->formatSalesChartLine($todayOrderSumList, 'day'),
            'topSaleProducts' => $popularProductList,
            'topMember' => $popularMemberList,
            'topMerchant' => $popularMerchantList
        ]);

        return view('backend.finance.dashboard');
    }
}