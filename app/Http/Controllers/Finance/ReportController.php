<?php


namespace OBID\Http\Controllers\Finance;


use Carbon\Carbon;
use JavaScript;
use OBID\Helpers\ChartFormatter;
use OBID\Http\Controllers\Controller;
use OBID\Repositories\CouponRepository;
use OBID\Repositories\OrderItemRepository;
use OBID\Repositories\StatRepository;

class ReportController extends Controller
{
    /**
     * @var OrderItemRepository
     */
    protected $orderItemRepo;

    /**
     * @var StatRepository
     */
    protected $statRepo;

    /**
     * @var CouponRepository
     */
    protected $couponRepo;

    /**
     * @param OrderItemRepository $orderItemRepo
     * @param StatRepository $statRepository
     * @param CouponRepository $couponRepo
     */
    public function __construct(
        OrderItemRepository $orderItemRepo,
        StatRepository $statRepository,
        CouponRepository $couponRepo
    )
    {
        $this->orderItemRepo = $orderItemRepo;
        $this->statRepo = $statRepository;
        $this->couponRepo = $couponRepo;
    }


    public function report(
        ChartFormatter $chartFormatter,
        StatRepository $statRepository,
        OrderItemRepository $orderItemRepository,
        CouponRepository $couponRepository
    )
    {
        $option = \Input::get('type', 'month');
        $date = \Input::get('date');
        $user = \Input::get('user', 0);

        if (is_null($date)) {
            $date = Carbon::now();
        } else {
            $date = Carbon::parse($date);
        }

        $chartSales = $user > 0 ? $chartFormatter->formatSalesChartLine(
            $statRepository->getSalesSum($option, 'online', $user, $date),
            $option
        ) : [];
        $orderList = $user > 0 ? $statRepository->getMerchantConfirmedOrders($user, $date) : [];
        $itemList = $user > 0 ? $orderItemRepository->getOnlineSoldItems($user, $date) : [];
        $couponList = $user > 0 ? $couponRepository->getUsedCoupons($user, $date) : [];

        $tgl = $date->toDateString();

        JavaScript::put(
            compact(
                'chartSales',
                'orderList',
                'itemList',
                'couponList',
                'user',
                'tgl'
            )
        );

        return view('backend.finance.report');
    }

    public function salesReport(ChartFormatter $chartFormatter)
    {
        $option = \Input::get('type', 'month');
        $date = \Input::get('date');
        $user = \Input::get('user', 0);

        if (is_null($date)) {
            $date = Carbon::now();
        } else {
            $date = Carbon::parse($date);
        }
        $chartSales = $chartFormatter
            ->formatSalesChartLine($this->statRepo->getSalesSum($option, 'online', $user, $date), $option);
        $orderList = $this->statRepo->getMerchantConfirmedOrders($user, $date);
        $itemList = $this->orderItemRepo->getOnlineSoldItems($user, $date);
        $couponList = $this->couponRepo->getUsedCoupons($user, $date);

        return response()->json(compact('chartSales', 'itemList', 'couponList', 'orderList'));
    }

    public function obidFinancialReport(
        StatRepository $statRepository,
        CouponRepository $couponRepository
    )
    {
        $option = 'month';
        $date = Carbon::now();

        $listMerchantProfit = $statRepository->getMerchantsPerformanceReport($option, $date);
        $listShipping = $statRepository->getShippedOrders($option, $date);
        $listUsedCoupon = $couponRepository->getUsedCoupons(0, $date);
        $listMerchantOrderCount = $statRepository->getListMerchantConfirmedOrderCount($option, $date);

        JavaScript::put(
            compact(
                'listMerchantProfit',
                'listShipping',
                'listUsedCoupon',
                'listMerchantOrderCount'
            )
        );

        return view('backend.finance.obidreport');
    }

    public function obidReport(
        StatRepository $statRepository,
        CouponRepository $couponRepository
    )
    {
        $option = \Input::get('type', 'month');
        $date = \Input::get('date');

        if (is_null($date)) {
            $date = Carbon::now();
        } else {
            $date = Carbon::parse($date);
        }

        $listMerchantProfit = $statRepository->getMerchantsPerformanceReport($option, $date);
        $listShipping = $statRepository->getShippedOrders($option, $date);
        $listUsedCoupon = $couponRepository->getUsedCoupons(0, $date);
        $listMerchantOrderCount = $statRepository->getListMerchantConfirmedOrderCount($option, $date);

        return response()->json(
            compact(
                'listMerchantProfit',
                'listShipping',
                'listUsedCoupon',
                'listMerchantOrderCount'
            )
        );
    }
}