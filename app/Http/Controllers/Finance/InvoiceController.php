<?php

namespace OBID\Http\Controllers\Finance;


use Carbon\Carbon;
use OBID\Helpers\ChartFormatter;
use OBID\Http\Controllers\Controller;
use OBID\Models\User;
use OBID\Repositories\CouponRepository;
use OBID\Repositories\OrderItemRepository;
use OBID\Repositories\StatRepository;

class InvoiceController extends Controller
{
    /**
     * @var OrderItemRepository
     */
    protected $orderItemRepo;

    /**
     * @var StatRepository
     */
    protected $statRepo;

    /**
     * @var CouponRepository
     */
    protected $couponRepo;

    /**
     * @param OrderItemRepository $orderItemRepo
     * @param StatRepository $statRepository
     * @param CouponRepository $couponRepo
     */
    public function __construct(
        OrderItemRepository $orderItemRepo,
        StatRepository $statRepository,
        CouponRepository $couponRepo
    )
    {
        $this->orderItemRepo = $orderItemRepo;
        $this->statRepo = $statRepository;
        $this->couponRepo = $couponRepo;
    }

    public function generateInvoiceMerchant(ChartFormatter $chartFormatter)
    {
        $option = \Input::get('type', 'month');
        $date = \Input::get('date');
        $user = \Input::get('user', 0);

        if (is_null($date)) {
            $date = Carbon::now();
        } else {
            $date = Carbon::parse($date);
        }

        $orderList = $this->statRepo->getMerchantConfirmedOrders($user, $date);
        $itemList = $this->orderItemRepo->getOnlineSoldItems($user, $date);
        $couponList = $this->couponRepo->getUsedCoupons($user, $date);

        $totalOrderAdmin = count($orderList) * 2000;
        $totalSoldItems = array_reduce($itemList->toArray(), function ($i, $obj) {
            return $i += $obj['adjusment'];
        });
        $totalUsedCoupon = count($couponList->toArray()) > 0 ? array_reduce($couponList->toArray(), function ($i, $obj) {
            return $i += $obj['value'];
        }) : 0;

        $grandTotal = $totalSoldItems - $totalOrderAdmin - $totalUsedCoupon;
        $grandTotal = number_format($grandTotal, 0, ',', '.');

        $brand = User::with('profileable')->find($user);

        $invoice = [
            [
                'description' => 'Total item anda yang terjual',
                'total' => 'IDR ' . number_format($totalSoldItems, 0, ',', '.')
            ],
            [
                'description' => 'Total Biaya Admin OBID per order (IDR 5000)',
                'total' => 'IDR ' . number_format($totalOrderAdmin, 0, ',', '.')
            ],
            [
                'description' => 'Total voucher yang terpakai',
                'total' => 'IDR ' . number_format($totalUsedCoupon, 0, ',', '.')
            ]
        ];

        $pdf = \PDF::loadView('backend.admin.invoice.invoicemerchant', compact('brand', 'grandTotal', 'invoice'));
        return $pdf->stream('invoice' . $brand->username . '.pdf');

    }
}