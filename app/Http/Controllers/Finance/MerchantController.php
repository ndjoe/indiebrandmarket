<?php

namespace OBID\Http\Controllers\Finance;


use OBID\Http\Controllers\Controller;
use OBID\Models\User;

class MerchantController extends Controller
{
    public function getMerchantForSelect()
    {
        $merchants = User::whereHas('roles', function ($q) {
            $q->where('name', 'affiliate');
        })->get();

        $response = [];

        foreach ($merchants as $m) {
            array_push($response, [
                'text' => $m->username,
                'value' => $m->id
            ]);
        }

        return response()->json($response);
    }
}