<?php

namespace OBID\Http\Controllers\Inventory;


use OBID\Http\Controllers\Controller;
use OBID\Repositories\StatRepository;

class DashboardController extends Controller
{
    public function index(StatRepository $statRepository)
    {
        $productCount = $statRepository->productCount();
        $publishedProducts = $statRepository->productCount('published_at');
        $productByCategory = $statRepository->productCountByCategory();

        \JavaScript::put([
            'productCount' => $productCount,
            'publishedProductCount' => $publishedProducts,
            'productByCategory' => $productByCategory
        ]);

        return view('backend.inventory.dashboard');
    }
}