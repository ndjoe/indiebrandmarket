<?php
namespace OBID\Http\Controllers\Regmerchant;


use Illuminate\Support\Str;
use OBID\Helpers\ImageService;
use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Account\NewMerchantRequest;
use OBID\Http\Requests\Account\SetTokenRequest;
use OBID\Models\RegistrationToken;
use OBID\Repositories\UserRepository;

class RegmerchantController extends Controller
{
    public function create()
    {
        if (\Session::has('reg:token')) {
            return redirect()->to('/create');
        }

        return view('backend.reg.login');
    }

    public function index()
    {
        if (!\Session::has('reg:token')) {
            return redirect()->to('/');
        }

        return view('backend.reg.reg');
    }

    public function store(NewMerchantRequest $request, UserRepository $userRepository)
    {
        $input = $request->all();
        $imageService = new ImageService;

        $serializeForm = [
            'nama' => $input['nama'],
            'kota' => $input['kota'],
            'owner' => $input['owner'],
            'bank' => $input['bank'],
            'provinsi' => $input['provinsi'],
            'norek' => $input['norek'],
            'kodepos' => $input['kodepos'],
            'nohp' => $input['nohp'],
            'about' => $input['about'],
            'alamat' => $input['alamat'],
            'ownerRek' => $input['ownerRek'],
            'password' => $input['password'],
            'email' => $input['email'],
            'username' => Str::lower($input['username']),
            'is_active' => true
        ];

        if ($request->hasFile('banner')) {
            if ($imageService->checkImageSize($request->file('banner'), 999, 299)) {
                $serializeForm['bannerImage'] = $request->file('banner');
            } else {
                return response()->json(
                    [
                        'created' => false,
                        'reason' => 'Banner Image Tidak Valid Silahkan Check Lagi'
                    ]
                );
            }
        }

        if ($request->hasFile('logo')) {
            if ($imageService->checkImageSize($request->file('logo'), 299, 299)) {
                $serializeForm['logoImage'] = $request->file('logo');
            } else {
                return response()->json(
                    [
                        'created' => false,
                        'reason' => 'Logo Image Tidak Valid Silahkan Check Lagi'
                    ]
                );
            }
        }

        $created = $userRepository->createNewMerchant($serializeForm);

        if ($created) {
            \Session::forget('reg:token');
            return response()->json(['created' => true, 'redirect' => 'http://merchantcenter.originalbrands.localapp']);
        }

        return response()->json(['created' => false]);
    }

    public function setToken(SetTokenRequest $request)
    {
        $token = $request->get('token');
        $valid = RegistrationToken::where('token', $token)->valid()->first();

        if ($valid) {
            \Session::set('reg:token', $token);
            return redirect()->to('/create');
        }

        \Session::flash('alert:error', 'Token Not Found');
        return redirect()->back();
    }

    public function thankyou()
    {
        return view('backend.reg.thankyou');
    }
}