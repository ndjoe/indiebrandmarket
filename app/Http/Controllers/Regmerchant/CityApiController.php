<?php

namespace OBID\Http\Controllers\Regmerchant;


use Carbon\Carbon;
use GuzzleHttp\Client;
use OBID\Http\Controllers\Controller;

class CityApiController extends Controller
{
    protected static $apiKey = '3bb3f21b214a4aec5059504ac0f752b4';
    protected static $baseUrl = 'http://rajaongkir.com/api/starter';
    /**
     * @var Client
     */
    protected $client;


    public function __construct()
    {
        $this->client = new Client(['headers' => ['key' => static::$apiKey]]);
    }

    public function getProvinces()
    {
        $url = static::$baseUrl . '/' . 'province';
        $client = $this->client;

        $provinces = \Cache::remember(
            'provinces',
            Carbon::now()->diffInMinutes(Carbon::now()->addDay(1)),
            function () use ($client, $url) {
                $response = $client->get($url);
                $result = json_decode($response->getBody());

                $provinces = [];
                foreach ($result->rajaongkir->results as $r) {
                    array_push($provinces, [
                        'value' => [
                            'id' => $r->province_id,
                            'text' => $r->province
                        ],
                        'text' => $r->province
                    ]);
                }

                return $provinces;
            });

        return response()->json($provinces);
    }

    public function getCitiesByProvince($provinceId)
    {
        $url = static::$baseUrl . '/' . 'city';
        $client = $this->client;

        $cities = \Cache::remember(
            'city:' . $provinceId,
            Carbon::now()->diffInMinutes(Carbon::now()->addDay(1)),
            function () use ($client, $url, $provinceId) {
                $response = $client->request('GET', $url, ['query' => ['province' => $provinceId]]);
                $result = json_decode($response->getBody());

                $cities = [];
                foreach ($result->rajaongkir->results as $r) {
                    array_push($cities, [
                        'value' => [
                            'id' => $r->city_id,
                            'text' => $r->type . ' ' . $r->city_name
                        ],
                        'text' => $r->type . ' ' . $r->city_name
                    ]);
                }

                return $cities;
            });

        return response()->json($cities);
    }
}