<?php

namespace OBID\Http\Controllers\Shipping;


use OBID\Http\Controllers\Controller;
use OBID\Repositories\OrderRepository;

class OrderController extends Controller
{
    /**
     * @var OrderRepository
     */
    protected $orderRepo;

    /**
     * OrderController constructor.
     * @param OrderRepository $repo
     */
    public function __construct(
        OrderRepository $repo
    )
    {
        $this->orderRepo = $repo;
    }

    public function printOrder($orderid)
    {
        $order = $this->orderRepo->findById($orderid);

        $order->load('user', 'items', 'items.productItem.product', 'items.productItem.product.author', 'items.productItem.size', 'address');
        $order = $order->toArray();
        $pdf = \PDF::loadView('pdf.invoice', compact('order'));

        return $pdf->stream('invoice-shipping-' . $order['order_code'] . '.pdf');
    }
}