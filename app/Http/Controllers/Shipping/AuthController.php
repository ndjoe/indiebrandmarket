<?php

namespace OBID\Http\Controllers\Shipping;


use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Account\LoginRequest;
use OBID\Models\User;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $req = $request->all();

        $success = false;

        $user = User::whereUsername($req['username'])->whereHas('roles', function ($q) {
            $q->where('name', 'shipper');
        })->active()->first();

        if ($user) {
            $success = \Auth::attempt(['username' => $user->username, 'password' => $req['password']]);
        }

        if ($success) {
            return redirect()->to('/');
        }

        return redirect()->back();
    }

    public function loginPage()
    {
        if (\Auth::check()) {
            if (\Auth::user()->hasRole('shipper')) {
                return redirect()->to('/');
            } else {
                \Auth::logout();
            }
        }

        return view('backend.shipping.login');
    }

    public function logout()
    {
        \Auth::logout();

        return redirect()->to('/login');
    }
}