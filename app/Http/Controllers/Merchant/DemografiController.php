<?php

namespace OBID\Http\Controllers\Merchant;


use OBID\Http\Controllers\Controller;
use OBID\Repositories\OrderRepository;
use OBID\Repositories\UserRepository;

class DemografiController extends Controller
{
    protected $orderRepo;
    protected $userRepo;

    /**
     * DemografiController constructor.
     * @param OrderRepository $orderRepo
     * @param UserRepository $userRepo
     */
    public function __construct(OrderRepository $orderRepo, UserRepository $userRepo)
    {
        $this->orderRepo = $orderRepo;
        $this->userRepo = $userRepo;
    }

    public function getBrandStat($brandId)
    {
        $buyerIds = $this->orderRepo->getBuyersIdByBrand($brandId);
        $stats = $this->userRepo->getStatsFromUserIds($buyerIds);
        return response()->json($stats);
    }
}