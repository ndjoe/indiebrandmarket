<?php


namespace OBID\Http\Controllers\Merchant;


use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Pos\SetPosCashRequest;
use OBID\Repositories\PosRepository;

class PosController extends Controller
{
    /**
     * @var PosRepository
     */
    protected $posRepo;

    /**
     * @param PosRepository $posRepo
     */
    public function __construct(PosRepository $posRepo)
    {
        $this->posRepo = $posRepo;
    }

    public function getPosCash($id)
    {
        $cash = $this->posRepo->getPosCash($id);

        return response()->json($cash);
    }

    public function setPosCash(SetPosCashRequest $request)
    {
        $req = $request->all();

        $pos = $this->posRepo->setMerchantPosCash(\Auth::id(), $req['new']);

        return response()->json(['updated' => true, 'newValue' => $pos->cash]);
    }
}