<?php


namespace OBID\Http\Controllers\Merchant;


use Carbon\Carbon;
use JavaScript;
use OBID\Helpers\ChartFormatter;
use OBID\Http\Controllers\Controller;
use OBID\Repositories\StatRepository;

class DashboardController extends Controller
{
    /**
     * @var StatRepository
     */
    protected $statRepo;

    /**
     * @param StatRepository $statRepo
     */
    public function __construct(StatRepository $statRepo)
    {
        $this->statRepo = $statRepo;
    }

    /**
     * @param ChartFormatter $chartFormatter
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ChartFormatter $chartFormatter)
    {
        $todayOrderCountList = $this->statRepo->getOrderCount('day', \Auth::id(), Carbon::now());
        $totalOrderCountList = $this->statRepo->getOrderCount('month', \Auth::id(), Carbon::now());
        $todayOrderSumList = $this->statRepo->getSalesSum('day', 'online', \Auth::id(), Carbon::now());
        $totalOrderSumList = $this->statRepo->getSalesSum('month', 'online', \Auth::id(), Carbon::now());
        $totalOrderCount = array_reduce($totalOrderCountList, function ($i, $obj) {
            return $i += $obj->value;
        });
        $todayOrderCount = array_reduce($todayOrderCountList, function ($i, $obj) {
            return $i += $obj->value;
        });
        $totalOrderSum = array_reduce($totalOrderSumList, function ($i, $obj) {
            return $i += $obj->value;
        });
        $todayOrderSum = array_reduce($todayOrderSumList, function ($i, $obj) {
            return $i += $obj->value;
        });
        $popularProductList = $this->statRepo->getTopSalesProducts(\Auth::id());

        JavaScript::put([
            'totalOrderCount' => $totalOrderCount,
            'totalOrderSum' => $totalOrderSum,
            'todayOrderCount' => $todayOrderCount,
            'todayOrderSum' => $todayOrderSum,
            'totalOrderCountChart' => $chartFormatter->formatSalesChartLine($totalOrderCountList, 'month'),
            'todayOrderCountChart' => $chartFormatter->formatSalesChartLine($todayOrderCountList, 'day'),
            'totalOrderSumChart' => $chartFormatter->formatSalesChartLine($totalOrderSumList, 'month'),
            'todayOrderSumChart' => $chartFormatter->formatSalesChartLine($todayOrderSumList, 'day'),
            'topSaleProducts' => $popularProductList,
            'authId' => \Auth::id()
        ]);

        return view('backend.merchant.index', compact('buyerCount'));
    }

    public function getProductsByCategory($brandId)
    {
        return response()->json($this->statRepo->getProductsByCategoryCount($brandId, 'donut'));
    }

    public function getSalesChart()
    {
        $time = \Input::get('time', 'month');
        $type = \Input::get('type', 'all');

        return response()->json($this->statRepo->getSalesSum($time, $type, \Auth::id(), Carbon::now()));
    }
}