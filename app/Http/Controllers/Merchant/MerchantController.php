<?php

namespace OBID\Http\Controllers\Merchant;


use Illuminate\Support\Str;
use OBID\Http\Controllers\Controller;
use OBID\Models\Product;
use OBID\Models\Size;
use OBID\Models\User;

class MerchantController extends Controller
{
    public function products()
    {
        \JavaScript::put([
            'authId' => \Auth::id()
        ]);
        return view('backend.merchant.product.index');
    }

    public function createProduct()
    {
        \JavaScript::put([
            'authId' => \Auth::id()
        ]);
        return view('backend.merchant.product.create');
    }

    public function editProduct($id)
    {
        $product = Product::with('discount', 'images', 'items', 'items.size')
            ->whereId($id)
            ->whereAuthorId(\Auth::user()->id)
            ->first();
        $product = $product->toArray();
        $sizes = Size::all();

        $resp = [];
        foreach ($sizes as $s) {
            array_push($resp, [
                'text' => Str::upper($s->text),
                'value' => $s->id
            ]);
        }
        \JavaScript::put([
            'product' => $product,
            'authId' => \Auth::id(),
            'resp' => $resp
        ]);

        return view('backend.merchant.product.edit');
    }

    public function orders()
    {

        \JavaScript::put([
            'authId' => \Auth::id()
        ]);

        return view('backend.merchant.order.index');
    }

    public function report()
    {
        \JavaScript::put([
            'authId' => \Auth::id()
        ]);

        return view('backend.merchant.report.index');
    }

    public function editAccount()
    {
        $user = \Auth::user()->load('profileable');

        \JavaScript::put([
            'user' => $user,
            'authId' => \Auth::id()
        ]);

        return view('backend.merchant.account.edit');
    }

    public function posOrders()
    {
        \JavaScript::put([
            'authId' => \Auth::id()
        ]);

        return view('backend.merchant.pos.orders');
    }

    public function posAccounts()
    {
        \JavaScript::put([
            'merchantId' => \Auth::id(),
            'authId' => \Auth::id()
        ]);

        return view('backend.merchant.pos.posAccount');
    }

    public function createPosAccount()
    {
        \JavaScript::put([
            'authId' => \Auth::id()
        ]);

        return view('backend.merchant.pos.createCashier');
    }

    public function editPosAccount($id)
    {
        $user = User::whereId($id)->first();

        \JavaScript::put([
            'cashier' => $user,
            'authId' => \Auth::id()
        ]);

        return view('backend.merchant.pos.cashierEdit', compact('user'));
    }

    public function demografi()
    {
        \JavaScript::put([
            'authId' => \Auth::id()
        ]);

        return view('backend.merchant.demografi.demografi');
    }

    public function coupons()
    {
        \JavaScript::put([
            'authId' => \Auth::id()
        ]);

        return view('backend.merchant.coupon.index');
    }

    public function staffs()
    {
        \JavaScript::put([
            'authId' => \Auth::id()
        ]);

        return view('backend.merchant.staffs.index');
    }

    public function createStaff()
    {
        \JavaScript::put([
            'authId' => \Auth::id()
        ]);

        return view('backend.merchant.staffs.create');
    }

    public function editStaff($id)
    {
        $user = User::whereId($id)->first();

        \JavaScript::put([
            'cashier' => $user,
            'authId' => \Auth::id()
        ]);

        return view('backend.merchant.staffs.edit', compact('user'));
    }
}