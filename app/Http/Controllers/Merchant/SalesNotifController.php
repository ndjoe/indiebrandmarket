<?php

namespace OBID\Http\Controllers\Merchant;


use OBID\Http\Controllers\Controller;
use OBID\Repositories\OrderItemRepository;
use OBID\Repositories\SalesNotificationRepository;

class SalesNotifController extends Controller
{
    /**
     * @var SalesNotificationRepository
     */
    protected $notifRepo;

    /**
     * @var OrderItemRepository
     */
    protected $orderItemRepo;

    /**
     * SalesNotifController constructor.
     * @param SalesNotificationRepository $notifRepo
     * @param OrderItemRepository $orderItemRepository
     */
    public function __construct(SalesNotificationRepository $notifRepo, OrderItemRepository $orderItemRepository)
    {
        $this->notifRepo = $notifRepo;
        $this->orderItemRepo = $orderItemRepository;
    }

    public function getNotif()
    {
        $count = $this->orderItemRepo->getUnacceptedOrderItemCount(\Auth::id());

        return response()->json(['count' => $count]);
    }

    public function clearNotif()
    {
        $this->notifRepo->clearNotif(\Auth::id());

        return response()->json(['cleared' => true]);
    }
}