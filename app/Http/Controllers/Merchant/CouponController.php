<?php

namespace OBID\Http\Controllers\Merchant;

use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Coupon\AddCouponRequest;
use OBID\Repositories\CouponRepository;

class CouponController extends Controller
{
    protected $couponRepo;

    /**
     * @param CouponRepository $couponRepo
     */
    public function __construct(CouponRepository $couponRepo)
    {
        $this->couponRepo = $couponRepo;
    }

    public function getCoupons()
    {
        $page = \Input::get('page', 1);

        $coupons = $this->couponRepo->getMerchantCouponsPaginated($page, \Auth::id());

        return response()->json($coupons);
    }

    public function store(AddCouponRequest $request)
    {
        $req = $request->all();

        $input = [
            'value' => $req['value'],
            'expired_at' => $req['expired'],
            'qty' => 1,
            'notavailable' => $req['setting'],
            'minimum_order' => $req['minimum'],
            'brand_id' => \Auth::id()
        ];

        $this->couponRepo->createBulk($input, $req['qty']);

        return response()->json([]);
    }

    public function customStore(AddCouponRequest $request)
    {
        $req = $request->all();

        $input = [
            'value' => $req['value'],
            'expired_at' => $req['expired'],
            'qty' => $req['qty'],
            'notavailable' => $req['setting'],
            'minimum_order' => $req['minimum'],
            'brand_id' => \Auth::id()
        ];

        $this->couponRepo->create($input);

        return response()->json([]);
    }

    public function deleteCoupon($couponId)
    {
        $this->couponRepo->deleteCoupon($couponId);

        return response()->json(['deleted' => true]);
    }
}