<?php


namespace OBID\Http\Controllers\Merchant;


use OBID\Http\Controllers\Controller;
use OBID\Repositories\OrderRepository;

class OrderController extends Controller
{
    /**
     * @var OrderRepository
     */
    protected $repo;

    /**
     * @param OrderRepository $repo
     */
    public function __construct(OrderRepository $repo)
    {
        $this->repo = $repo;
    }

    public function getPosOrders()
    {
        $page = \Input::get('page', 1);
        $type = \Input::get('type', 'all');

        $orders = $this->repo->getPosOrders($page, 'desc', 'id', \Auth::id(), $type);

        return response()->json($orders);
    }
}