<?php


namespace OBID\Http\Controllers\Merchant;


use OBID\Helpers\ImageService;
use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Account\CreateCashierRequest;
use OBID\Http\Requests\Account\EditCashierRequest;
use OBID\Http\Requests\Account\EditMerchantRequest;
use OBID\Http\Requests\Account\LoginRequest;
use OBID\Models\CashierProfile;
use OBID\Models\Role;
use OBID\Models\User;
use OBID\Repositories\UserRepository;

class AccountController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepo;

    /**
     * @param UserRepository $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function editProfile(EditMerchantRequest $request)
    {
        $input = $request->all();
        $imageService = new ImageService;

        $serializeForm = [
            'nama' => $input['nama'],
            'kota' => $input['kota'],
            'owner' => $input['owner'],
            'bank' => $input['bank'],
            'provinsi' => $input['provinsi'],
            'norek' => $input['norek'],
            'kodepos' => $input['kodepos'],
            'nohp' => $input['nohp'],
            'about' => $input['about'],
            'alamat' => $input['alamat'],
            'ownerRek' => $input['ownerRek']
        ];

        if ($request->hasFile('banner')) {
            if ($imageService->checkImageSize($request->file('banner'), 999, 299)) {
                $serializeForm['bannerImage'] = $request->file('banner');
            } else {
                return response()->json(
                    [
                        'updated' => false,
                        'reason' => 'Banner Image Tidak Valid Silahkan Check Lagi'
                    ]
                );
            }
        }

        if ($request->hasFile('logo')) {
            if ($imageService->checkImageSize($request->file('logo'), 299, 299)) {
                $serializeForm['logoImage'] = $request->file('logo');
            } else {
                return response()->json(
                    [
                        'updated' => false,
                        'reason' => 'Logo Image Tidak Valid Silahkan Check Lagi'
                    ]
                );
            }
        }

        if ($request->has('password')) {
            $serializeForm['password'] = $input['password'];
        }

        $updated = $this->userRepo->editMerchant(\Auth::user()->id, $serializeForm);

        if ($updated) {
            return response()->json(['updated' => true, 'redirect' => url('/settings')]);
        }

        return response()->json(['updated' => false]);
    }

    public function getCashiersProfile()
    {
        $page = \Input::get('page', 1);
        $kuery = \Input::get('query', null);

        $cashiers = $this->userRepo->getCashiersByMerchant($page, $kuery, 'desc', 'id', \Auth::id());

        return response()->json($cashiers);
    }

    public function getStaffsMerchant()
    {
        $page = \Input::get('page', 1);
        $kuery = \Input::get('query', null);

        $cashiers = $this->userRepo->getStaffsMerchant(
            $page,
            $kuery,
            'desc',
            'id',
            \Auth::id()
        );

        return response()->json($cashiers);

    }

    public function storeCashier(CreateCashierRequest $request)
    {
        $req = $request->all();

        $user = new User;
        $user->username = $req['username'];
        $user->email = $req['email'];
        $user->password = bcrypt($req['password']);
        $user->nama = $req['nama'];
        $user->nohp = $req['nohp'];

        $profile = new CashierProfile;
        $profile->merchant_id = \Auth::id();

        $profile->save();
        $profile->user()->save($user);

        $role = Role::whereName('cashier')->first();
        $user->attachRole($role);

        return response()->json(['created' => true, 'redirect' => url('/staffs')]);
    }

    public function editCashier($id, EditCashierRequest $request)
    {
        $req = $request->all();

        $updated = $this->userRepo->editStaff($id, $req);

        if (!$updated) {
            return response()->json(['updated' => false]);
        }

        return response()->json(['updated' => true, 'redirect' => url('/pos/accounts')]);
    }

    public function storeStaff(CreateCashierRequest $request)
    {
        $req = $request->all();

        $user = new User;
        $user->username = $req['username'];
        $user->email = $req['email'];
        $user->password = bcrypt($req['password']);
        $user->nama = $req['nama'];
        $user->nohp = $req['nohp'];

        $profile = new CashierProfile;
        $profile->merchant_id = \Auth::id();

        $profile->save();
        $profile->user()->save($user);

        $role = Role::whereName('staffadmin')->first();
        $user->attachRole($role);

        return response()->json(['created' => true, 'redirect' => url('/staffs')]);
    }

    public function editStaff($id, EditCashierRequest $request)
    {
        $req = $request->all();

        $updated = $this->userRepo->editStaff($id, $req);

        if (!$updated) {
            return response()->json(['updated' => false]);
        }

        return response()->json(['updated' => true, 'redirect' => url('/staffs')]);
    }

    public function deleteAccount($id)
    {
        $deleted = $this->userRepo->deleteAccount($id);

        return response()->json(['deleted' => $deleted]);
    }

    public function activateAccount($id)
    {
        $activated = $this->userRepo->activateAccount($id);

        return response()->json(['activated' => $activated]);
    }

    public function deactivateAccount($id)
    {
        $deactivated = $this->userRepo->deactivateAccount($id);

        return response()->json(['deactivated' => $deactivated]);
    }

    public function login()
    {
        if (\Auth::check()) {
            if (\Auth::user()->hasRole('affiliate')) {
                return redirect()->to('/user');
            } else {
                \Auth::logout();
            }
        }

        return view('backend.merchant.login');
    }

    public function postLogin(LoginRequest $request)
    {
        $req = $request->all();

        $success = false;

        $user = User::whereUsername($req['username'])->whereHas('roles', function ($q) {
            $q->where('name', 'affiliate');
        })->first();

        if ($user) {
            $success = \Auth::attempt(['username' => $user->username, 'password' => $req['password']]);
        }

        if ($success) {
            return redirect()->to('/');
        }

        return redirect()->back();
    }

    public function logout()
    {
        \Auth::logout();

        return redirect()->to('/login');
    }

}