<?php


namespace OBID\Http\Controllers\Merchant;


use Carbon\Carbon;
use OBID\Helpers\ChartFormatter;
use OBID\Http\Controllers\Controller;
use OBID\Repositories\CouponRepository;
use OBID\Repositories\OrderItemRepository;
use OBID\Repositories\StatRepository;

class ReportController extends Controller
{
    /**
     * @var OrderItemRepository
     */
    protected $orderItemRepo;

    /**
     * @var StatRepository
     */
    protected $statRepo;

    /**
     * @var CouponRepository
     */
    protected $couponRepo;

    /**
     * @param OrderItemRepository $orderItemRepo
     * @param StatRepository $statRepository
     * @param CouponRepository $couponRepository
     */
    public function __construct(
        OrderItemRepository $orderItemRepo,
        StatRepository $statRepository,
        CouponRepository $couponRepository
    )
    {
        $this->orderItemRepo = $orderItemRepo;
        $this->statRepo = $statRepository;
        $this->couponRepo = $couponRepository;
    }

    public function generateReport(ChartFormatter $chartFormatter)
    {
        $type = \Input::get('type', 'month');
        $date = \Input::get('date');
        $user = \Auth::user();

        if (is_null($date)) {
            $date = Carbon::now();
        } else {
            $date = Carbon::parse($date);
        }

        $chartSales = $chartFormatter->formatSalesChartLine($this->statRepo->getSalesSum($type, 'online', $user->id, $date), $type);
        $orderList = $this->statRepo->getMerchantConfirmedOrders($user->id, $date);
        $itemList = $this->orderItemRepo->getOnlineSoldItems($user->id, $date);
        $couponList = $this->couponRepo->getUsedCoupons($user->id, $date);

        return response()->json(compact('chartSales', 'itemList', 'couponList', 'orderList'));
    }
}