<?php


namespace OBID\Http\Controllers\Merchant;


use Illuminate\Support\Str;
use OBID\Http\Controllers\Controller;
use OBID\Models\Subcategory;

class SubcategoryController extends Controller
{
    public function getForSelect()
    {
        $subcategories = Subcategory::whereMerchantId(\Auth::id())->get()->all();

        $response = [];

        foreach ($subcategories as $subcat) {
            array_push($response, [
                'text' => Str::upper($subcat->text),
                'value' => $subcat->id
            ]);
        }

        return \Response::json($response);
    }

    public function getSizesForSelect($id)
    {
        $sizes = Subcategory::whereId($id)->first()->sizes;

        $response = [];

        foreach ($sizes as $s) {
            array_push($response, [
                'text' => Str::upper($s->text),
                'value' => $s->id
            ]);
        }

        return \Response::json($response);
    }
}