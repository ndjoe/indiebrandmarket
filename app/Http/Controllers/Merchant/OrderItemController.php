<?php


namespace OBID\Http\Controllers\Merchant;


use Carbon\Carbon;
use OBID\Events\Order\OrderItemConfirmed;
use OBID\Http\Controllers\Controller;
use OBID\Models\OrderItem;
use OBID\Repositories\OrderItemRepository;

class OrderItemController extends Controller
{
    /**
     * @var OrderItemRepository
     */
    protected $orderItemRepo;

    /**
     * @param OrderItemRepository $orderItemRepo
     */
    public function __construct(OrderItemRepository $orderItemRepo)
    {
        $this->orderItemRepo = $orderItemRepo;
    }

    public function getMerchantOrderItems()
    {
        $page = \Input::get('page', 1);
        $filter = \Input::get('filter', 0);

        $items = $this->orderItemRepo->getMerchantOrderItems(\Auth::user(), $filter, 'desc', 'id', $page);

        \JavaScript::put([
            'userId' => \Auth::id()
        ]);

        return response()->json($items);
    }

    public function acceptOrderItem($id)
    {
        $oi = OrderItem::whereId($id)->first();

        $oi->accepted_at = Carbon::now();

        if ($oi->save()) {
            event(new OrderItemConfirmed($oi));
            return response()->json(['accepted' => true], 200);
        }

        return response()->json(['accepted' => false]);

    }

}