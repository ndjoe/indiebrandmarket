<?php


namespace OBID\Http\Controllers\StaffMerchant;


use Illuminate\Support\Str;
use OBID\Http\Controllers\Controller;
use OBID\Models\Size;

class SizeController extends Controller
{
    public function getSizesForSelect()
    {
        $sizes = Size::all();

        $resp = [];
        foreach ($sizes as $s) {
            array_push($resp, [
                'text' => Str::upper($s->text),
                'value' => $s->id
            ]);
        }

        return response()->json($resp);
    }
}