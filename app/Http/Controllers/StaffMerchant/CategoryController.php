<?php


namespace OBID\Http\Controllers\StaffMerchant;


use Illuminate\Support\Str;
use OBID\Http\Controllers\Controller;
use OBID\Models\Category;

class CategoryController extends Controller
{
    public function getForSelect()
    {
        $categories = Category::all();

        $response = [];

        foreach ($categories as $cat) {
            array_push($response, [
                'text' => Str::Upper($cat->text),
                'value' => $cat->id
            ]);
        }

        return \Response::json($response);
    }

    public function getSizeBasedOnCategory($category)
    {
        $temp = Category::whereId($category)->first();
        $response = [];
        switch ($temp->text) {
            case 'shoes':
                $sizes = \Config::get('ibm.sizes.shoes');
                break;
            default:
                $sizes = \Config::get('ibm.sizes.clothing');
                $sizes['allsize'] = \Config::get('ibm.sizes.allsize');
                break;
        }

        foreach ($sizes as $key => $val) {
            array_push($response, ['value' => $val, 'text' => $key]);
        }

        return \Response::json($response);

    }
}
