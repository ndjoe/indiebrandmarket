<?php

namespace OBID\Http\Controllers\StaffMerchant;


use Illuminate\Support\Str;
use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Item\IncrementProductItemRequest;
use OBID\Http\Requests\Item\ProductItemInfoRequest;
use OBID\Models\Size;
use OBID\Repositories\ProductItemRepository;

class ProductItemController extends Controller
{
    protected $repo;

    /**
     * ProductItemController constructor.
     * @param $repo
     */
    public function __construct(ProductItemRepository $repo)
    {
        $this->repo = $repo;
    }


    public function incrementItem(IncrementProductItemRequest $request)
    {
        $req = $request->all();

        if (!is_numeric($req['size'])) {
            $req['size'] = Size::create(['text' => Str::lower($req['size'])])->id;
        }

        $saved = $this->repo->incrementQty($req['product'], $req['size'], $req['qty'], $req['desc']);


        if ($saved) {
            return \Response::json(['created' => true], 201);
        }

        return \Response::json(['created' => false], 500);
    }

    public function decrementItem(IncrementProductItemRequest $request)
    {
        $req = $request->all();

        $saved = $this->repo->decrementQty($req['product'], $req['size'], $req['qty']);

        if ($saved) {
            return \Response::json(['created' => true], 201);
        }

        return \Response::json(['created' => false]);
    }

    public function setItem(IncrementProductItemRequest $request)
    {
        $req = $request->all();

        if (!is_numeric($req['size'])) {
            $req['size'] = Size::create(['text' => Str::lower($req['size'])])->id;
        }

        $saved = $this->repo->setQty($req['product'], $req['size'], $req['qty'], $req['desc']);

        if ($saved) {
            return response()->json(['created' => true]);
        }

        return response()->json(['created' => false]);
    }

    public function getSizeInfo(ProductItemInfoRequest $request)
    {
        $req = $request->all();

        $item = $this->repo->findProductItem($req['productId'], $req['sizeId']);

        return response()->json($item);
    }
}