<?php


namespace OBID\Http\Controllers\StaffMerchant;


use Illuminate\Support\Str;
use OBID\Http\Controllers\Controller;
use OBID\Models\Color;

class ColorController extends Controller
{
    public function getAllForSelect()
    {
        $colors = Color::all();

        $resp = [];
        foreach ($colors as $c) {
            array_push($resp, [
                'value' => $c->id,
                'text' => Str::upper($c->text)
            ]);
        }

        return response()->json($resp);
    }
}