<?php

namespace OBID\Http\Controllers\StaffMerchant;


use Illuminate\Support\Str;
use OBID\Http\Controllers\Controller;
use OBID\Models\Product;
use OBID\Models\Size;
use OBID\Repositories\StatRepository;

class PagesController extends Controller
{
    public function products()
    {
        \JavaScript::put([
            'authId' => \Auth::id()
        ]);

        return view('backend.staffmerchant.products');
    }

    public function index(StatRepository $statRepository)
    {
        $brandId = \Auth::user()->profileable->merchant_id;
        $productCount = $statRepository->getMerchantProductCount($brandId);
        $publishedProducts = $statRepository->getMerchantPublishedProductCount($brandId);
        $productByCategory = $statRepository->getMerchantProductCountByCategory($brandId);

        \JavaScript::put([
            'productCount' => $productCount,
            'publishedProductCount' => $publishedProducts,
            'productByCategory' => $productByCategory
        ]);

        return view('backend.staffmerchant.dashboard');
    }

    public function createProduct()
    {
        return view('backend.staffmerchant.createProduct');
    }

    public function editProduct($username, $id)
    {
        $product = Product::with('discount', 'images', 'items', 'items.size')
            ->whereId($id)
            ->whereAuthorId(\Auth::user()->profileable->merchant_id)
            ->first();
        $product = $product->toArray();
        $sizes = Size::all();

        $resp = [];
        foreach ($sizes as $s) {
            array_push($resp, [
                'text' => Str::upper($s->text),
                'value' => $s->id
            ]);
        }
        \JavaScript::put([
            'product' => $product,
            'resp' => $resp
        ]);

        return view('backend.staffmerchant.editProduct');
    }
}