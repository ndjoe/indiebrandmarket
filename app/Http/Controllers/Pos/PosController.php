<?php

namespace OBID\Http\Controllers\Pos;

use Cart;
use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Pos\AddPosItemRequest;
use OBID\Http\Requests\Pos\AddProductRequest;
use OBID\Http\Requests\Pos\DelPosItemRequest;
use OBID\Http\Requests\Pos\NewCashPaymentRequest;
use OBID\Http\Requests\Pos\NewEdcPaymentRequest;
use OBID\Http\Requests\Pos\UpdateQtyPosItemRequest;
use OBID\Jobs\ProcessPosPurchase;
use OBID\Repositories\ProductItemRepository;

class PosController extends Controller
{
    /**
     * @var ProductItemRepository
     */
    protected $itemRepo;

    /**
     * @param ProductItemRepository $itemRepo
     */
    public function __construct(ProductItemRepository $itemRepo)
    {
        $this->itemRepo = $itemRepo;
    }

    public function index()
    {
        return view('backend.pos.pos');
    }


    public function getAll()
    {
        return response()->json($this->formatCartItem());
    }

    public function storeCash(NewCashPaymentRequest $request)
    {
        $req = $request->all();
        $cart = Cart::content();
        $paymentType = 'cash';
        $paymentData = ['cash' => $req['cash'], 'nama' => $req['nama']];
        $cashierId = \Auth::id();
        $merchantId = \Auth::user()->profileable->merchant_id;

        $order = $this->dispatch(new ProcessPosPurchase($cart, $paymentType, $paymentData, $cashierId, $merchantId));

        Cart::destroy();
        return response()->json(['created' => true, 'orderId' => $order->id]);
    }

    public function storeEdc(NewEdcPaymentRequest $request)
    {
        $req = $request->all();

        $cart = Cart::content();
        $paymentType = 'edc';
        $paymentData = [
            'bank' => $req['bank'],
            'noref' => $req['noref'],
            'cardNumber' => $req['cardNumber'],
            'expirationDate' => $req['expirationDate'],
            'nama' => $req['nama']
        ];
        $cashierId = \Auth::user()->id;
        $merchantId = \Auth::user()->profileable->merchant_id;

        $order = $this->dispatch(new ProcessPosPurchase($cart, $paymentType, $paymentData, $cashierId, $merchantId));

        Cart::destroy();
        return response()->json(['created' => true, 'orderId' => $order->id]);
    }

    /**
     * @param AddPosItemRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addCart(AddPosItemRequest $request)
    {
        $req = $request->all();

        $item = $this->itemRepo->getPosItem($req['uid'], \Auth::user()->profileable->merchant_id);

        if (!$item) {
            return response()->json(['found' => false], 404);
        }

        $search = Cart::search(['id' => $item->id]);

        if (is_array($search)) {
            $newVal = ++Cart::get($search[0])->qty;
            Cart::update($search[0], $newVal);
        } else {
            Cart::add([
                'id' => $item->id,
                'name' => $item->product->name,
                'qty' => 1,
                'price' => $item->product->price,
                'options' => [
                    'productId' => $item->product_id,
                    'image' => $item->product->images[0]->name,
                    'sizeId' => $item->size_id,
                    'sizeText' => $item->size->text
                ]
            ]);
        }

        return response()->json($this->formatCartItem());
    }

    public function addProduct(AddProductRequest $request)
    {
        $req = $request->all();

        $item = $this->itemRepo->getItemBySizeAndProductId($req['productId'], $req['sizeId']);

        if (is_null($item)) {
            return response()->json(['found' => false], 404);
        }

        $search = Cart::search(['id' => $item->id]);

        if (is_array($search)) {
            $newVal = Cart::get($search[0])->qty + $req['qty'];
            Cart::update($search[0], $newVal);
        } else {
            Cart::add([
                'id' => $item->id,
                'name' => $item->product->name,
                'qty' => $req['qty'],
                'price' => $item->product->price,
                'options' => [
                    'productId' => $item->product_id,
                    'image' => $item->product->images[0]->name,
                    'sizeId' => $item->size_id,
                    'sizeText' => $item->size->text
                ]
            ]);
        }

        return response()->json($this->formatCartItem());
    }

    public function deleteItem(DelPosItemRequest $request)
    {
        $req = $request->all();

        $search = \Cart::search(['id' => (int)$req['id']]);

        if (is_array($search)) {
            \Cart::remove($search[0]);
        }

        return response()->json($this->formatCartItem());
    }

    public function updateItem(UpdateQtyPosItemRequest $request)
    {
        $req = $request->all();

        $search = \Cart::search(['id' => (int)$req['id']]);

        if (is_array($search) && $req['qty'] >= 0) {
            \Cart::update($search[0], $req['qty']);
        }

        return response()->json($this->formatCartItem());
    }

    public function getCartTotalPrice()
    {
        $cart = Cart::total();

        return response()->json($cart);
    }

    /**
     * @return array
     */
    private function formatCartItem()
    {
        $cartArray = [];

        $cart = Cart::content();

        foreach ($cart as $item) {
            array_push($cartArray, $item);
        }

        return $cartArray;
    }
}