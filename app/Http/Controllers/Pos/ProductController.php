<?php


namespace OBID\Http\Controllers\Pos;


use OBID\Http\Controllers\Controller;
use OBID\Models\User;
use OBID\Repositories\ProductRepository;

class ProductController extends Controller
{
    /**
     * @var ProductRepository
     */
    protected $productRepo;

    /**
     * @param ProductRepository $productRepo
     */
    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepo = $productRepo;
    }

    public function getProducts($username, $merchantId)
    {
        $brand = User::whereId($merchantId)->first();
        $page = \Input::get('page', 1);
        $category = \Input::get('category', 0);
        $size = \Input::get('size', 0);
        $color = \Input::get('color', 0);
        $price = \Input::get('price', 0);
        $gender = \Input::get('gender', 'all');

        $products = $this->productRepo->getBrandProductsBySubcategory(
            $brand,
            $page,
            $category,
            $size,
            $gender,
            'desc',
            'id',
            $color,
            $price
        );

        return response()->json($products);
    }
}