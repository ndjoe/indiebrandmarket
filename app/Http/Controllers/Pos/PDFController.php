<?php


namespace OBID\Http\Controllers\Pos;


use OBID\Http\Controllers\Controller;
use OBID\Models\OrderItem;

class PDFController extends Controller
{
    public function generatePDF($username, $id)
    {
        $orderItems = OrderItem::with('productItem.product', 'productItem.size', 'order')->whereHas('order', function ($q) use ($id) {
            $q->where('type', 'offline')->where('id', $id);
        })->get();
        $data['total'] = $orderItems[0]->order->grossTotal;
        $data['orderId'] = $orderItems[0]->order->id;
        $data['date'] = $orderItems[0]->created_at;
        $data['items'] = [];
        foreach ($orderItems as $o) {
            array_push($data['items'], [
                'nama' => $o->productItem->product->name,
                'size' => $o->productItem->size->text,
                'qty' => $o->qty,
                'price' => $o->productItem->product->price,
                'subTotal' => $o->productItem->product->price * $o->qty
            ]);
        }
        $pdf = \PDF::loadView('pdf.faktur', compact('data'));
//        $im = \Im::loadView('pdf.faktur', compact('data'));
//        dd($pdf->stream());
        return $pdf->stream('invoice-pos-' . $data['orderId'] . '.pdf');
    }
}