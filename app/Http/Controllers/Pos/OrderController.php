<?php

namespace OBID\Http\Controllers\Pos;


use OBID\Http\Controllers\Controller;
use OBID\Repositories\OrderRepository;

class OrderController extends Controller
{
    protected $order;

    /**
     * OrderController constructor.
     * @param OrderRepository $order
     */
    public function __construct(OrderRepository $order)
    {
        $this->order = $order;
    }

    public function getOrder($username, $id)
    {
        $order = $this->order->getPosOrderById($id, \Auth::user()->profileable->merchant_id);

        return response()->json(['found' => true, 'order' => $order]);
    }
}