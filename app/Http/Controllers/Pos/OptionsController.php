<?php


namespace OBID\Http\Controllers\Pos;


use Illuminate\Support\Str;
use OBID\Http\Controllers\Controller;
use OBID\Models\Subcategory;

class OptionsController extends Controller
{
    public function getOptionsMerchant($username, $merchantId)
    {
        $categoriesses = Subcategory::whereHas('products', function ($q) use ($merchantId) {
            $q->where('author_id', $merchantId);
        })->get();

        $categories = [];

        foreach ($categoriesses as $subcat) {
            array_push($categories, [
                'text' => Str::upper($subcat->text),
                'value' => $subcat->id
            ]);
        }


        return response()->json(compact('categories'));
    }
}