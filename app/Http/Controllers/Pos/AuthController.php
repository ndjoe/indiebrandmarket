<?php

namespace OBID\Http\Controllers\Pos;


use OBID\Http\Controllers\Controller;
use OBID\Http\Requests\Account\LoginRequest;
use OBID\Models\User;

class AuthController extends Controller
{
    public function logout()
    {
        \Auth::logout();

        return redirect()->to('/pos/login');
    }

    public function login($username)
    {
        $merchant = User::whereHas('roles', function ($q) {
            $q->where('name', 'affiliate');
        })->where('username', $username)
            ->active()
            ->first();

        if (!$merchant) {
            abort(404);
        }

        if (\Auth::check()) {
            if (\Auth::user()->hasRole('cashier')) {
                return redirect()->to('/pos');
            } else {
                \Auth::logout();
            }
        }

        return view('backend.pos.login');
    }

    public function postLogin(LoginRequest $request)
    {
        $req = $request->all();

        $success = false;

        $user = User::whereUsername($req['username'])->whereHas('roles', function ($q) {
            $q->where('name', 'cashier');
        })->active()->first();

        if ($user) {
            $success = \Auth::attempt(['username' => $user->username, 'password' => $req['password']]);
        }

        if ($success) {
            return redirect()->to('/pos/login');
        }

        return redirect()->back();
    }
}