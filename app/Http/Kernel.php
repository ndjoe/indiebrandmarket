<?php

namespace OBID\Http;

use Illuminate\Auth\Middleware\AuthenticateWithBasicAuth;
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Mcamara\LaravelLocalization\Middleware\LaravelLocalizationRedirectFilter;
use Mcamara\LaravelLocalization\Middleware\LaravelLocalizationRoutes;
use Mcamara\LaravelLocalization\Middleware\LocaleSessionRedirect;
use OBID\Http\Middleware\Authenticate;
use OBID\Http\Middleware\EncryptCookies;
use OBID\Http\Middleware\Language;
use OBID\Http\Middleware\MustAdmin;
use OBID\Http\Middleware\MustCashier;
use OBID\Http\Middleware\MustFinance;
use OBID\Http\Middleware\MustInventory;
use OBID\Http\Middleware\MustLoggedinCheckout;
use OBID\Http\Middleware\MustMerchant;
use OBID\Http\Middleware\MustPurchase;
use OBID\Http\Middleware\MustShipper;
use OBID\Http\Middleware\MustStaffMerchant;
use OBID\Http\Middleware\RedirectIfAuthenticated;
use OBID\Http\Middleware\VerifyCsrfToken;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        CheckForMaintenanceMode::class,
        EncryptCookies::class,
        AddQueuedCookiesToResponse::class,
        StartSession::class,
        ShareErrorsFromSession::class,
        VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => Authenticate::class,
        'auth.basic' => AuthenticateWithBasicAuth::class,
        'guest' => RedirectIfAuthenticated::class,
        'mustAdmin' => MustAdmin::class,
        'mustCashier' => MustCashier::class,
        'mustMerchant' => MustMerchant::class,
        'mustShipper' => MustShipper::class,
        'mustFinance' => MustFinance::class,
        'mustInventory' => MustInventory::class,
        'mustPurchase' => MustPurchase::class,
        'mustMember' => MustLoggedinCheckout::class,
//        'mustStaffMerchant' => MustStaffMerchant::class,
        'language' => Language::class,
        'localize' => LaravelLocalizationRoutes::class,
        'localizationRedirect' => LaravelLocalizationRedirectFilter::class,
        'localeSessionRedirect' => LocaleSessionRedirect::class
    ];
}
