<?php
Route::group(['domain' => env('APP_DOMAIN')], function () {
    Route::group(['namespace' => 'Frontend', 'middleware' => ['mustMember']], function () {
        Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect']], function () {
            get('/', 'ShopController@index');
            get('/cart', 'ShopController@cartDetail');
            get('/checkout', 'ShopController@checkout');
            post('/checkout', 'OrderController@create');
            get('/account', 'ShopController@accountOrders');
            get('/account/profile', 'ShopController@accountProfile');
            get('/login', 'ShopController@login');
            post('/login', 'AccountController@login');
            post('/register', 'AccountController@registerAccount');
            get('/logout', 'AccountController@logout');
            get('/brand/{name}', 'ShopController@brandPage');
            get('/brand/{name}/{slug}', 'ShopController@singleProduct');
            get('/activation', 'AccountController@activateAccount');
            get('/bantuan', 'ShopController@bantuan');
            get('/privasi', 'ShopController@privasi');
            get('/pengiriman', 'ShopController@prosespengiriman');
            get('/tracking', 'ShopController@ordertracking');
            post('/tracking', 'OrderController@detail');
            get('/pengembalian', 'ShopController@pengembalian');
            get('/terms', 'ShopController@terms');
            get('/activate', 'ShopController@activationPage');
            post('/activate', 'AccountController@activateCode');
            get('/order-created', 'ShopController@orderCreated');
            get('/new-arrival', 'ShopController@newArrival');
            get('/brands', 'ShopController@brands');
            get('/forgot-password', 'ShopController@forgotpassword');
            get('/set-password', 'ShopController@setPassword');
            post('/forgot-password', 'AccountController@requestForgotPasswordToken');
            post('/set-password', 'AccountController@setPassword');
            get('/confirm-token', 'AccountController@confirmResetToken');
            get('/confirm-order', 'ShopController@confirmOrder');
            post('/confirm-order', 'OrderConfirmationController@create');
            get('/order/paid', 'OrderController@paidOrder');
            get('/contact-us', 'ShopController@contactUs');
            get('/category', 'ShopController@category');
            get('/sale', 'ShopController@sale');

            Route::group(['prefix' => 'api'], function () {
                get('/products', 'ProductController@index');
                get('/products/sale', 'ProductController@saleProducts');
                get('/products/front', 'ProductController@frontProducts');
                get('/cart', 'CartController@getAll');
                post('/cart', 'CartController@addCart');
                post('/cart/delete', 'CartController@deleteItem');
                get('/account/profile', 'AccountController@getProfile');
                post('/account/profile/edit', 'AccountController@editProfile');
                get('/brand/{brandId}/options', 'OptionsController@getOptionsForBrandPage');
                get('/options', 'OptionsController@getAllOptions');
                get('/brand/{name}/products', 'ProductController@brandProduct');
                get('/brands', 'AccountController@getBrands');
                get('/brands/paginated', 'AccountController@getBrandsPaginated');
                get('/provinces', 'OngkirController@getProvinces');
                get('/provinces/{provinceId}/city', 'OngkirController@getCitiesByProvince');
                get('/ongkir/{cityId}', 'OngkirController@getOngkir');
                post('/coupon', 'CartController@addCoupon');
                get('/coupon/delete', 'CartController@deleteCoupon');
                get('/profile', 'AccountController@getProfile');
                get('/check/username/{username}', 'ValidationController@checkUsername');
                get('/check/email/{email}', 'ValidationController@checkEmail');
                get('/check/nohp/{nohp}', 'ValidationController@checkNohp');
                get('/resend-code', 'AccountController@resendCode');
                post('/newsletter', 'EmailController@store');
            });
        });
    });
});

Route::group(['domain' => 'master.'.env('APP_DOMAIN')], function () {
    get('/login', 'Backend\AdminController@login');
    post('/login', 'Backend\AuthController@login');
    Route::group(['namespace' => 'Backend', 'middleware' => 'mustAdmin'], function () {
        get('/', 'AdminController@index');
        get('/products', ['name' => 'products.index', 'uses' => 'AdminController@products']);
        get('/products/create', 'AdminController@createProduct');
        get('/products/{id}', 'AdminController@editProduct');
        get('/pos', 'AdminController@pointOfSales');
        get('/orders', 'AdminController@orders');
        get('/users', 'AdminController@users');
        get('/users/customers', 'AdminController@members');
        get('/users/affiliates', 'AdminController@merchants');
        get('/users/guests', 'AdminController@guests');
        get('/users/staffs', 'AdminController@staffs');
        get('/users/cashiers', 'AdminController@cashiers');
        get('/users/cashiers/{id}/edit', 'AdminController@editCashier');
        get('/users/staffs/{id}/edit', 'AdminController@editStaff');
        get('/users/staffs/create', 'AdminController@createAdmin');
        get('/users/affiliates/create', 'AdminController@createMerchant');
        get('/users/affiliates/{id}/edit', 'AdminController@editMerchant');
        get('/users/customer/{id}/edit', 'AdminController@editMember');
        get('/users/admin/{id}/edit', 'AdminController@editMember');
        get('/shippings', 'AdminController@shippings');
        get('/report', 'AdminController@salesReport');
        get('/sliders', 'AdminController@slider');
        get('/demografi', 'AdminController@demografi');
        get('/coupons', 'AdminController@coupon');
        get('/currency', 'AdminController@currency');
        get('/logout', 'AdminController@logout');
        get('/financial-report', 'AdminController@obidFinancialReport');
        get('/invoice/merchant', 'InvoiceController@generateInvoiceMerchant');
        get('/token-registration', 'AdminController@regToken');
        get('/orders/{orderid}/print', 'OrderController@printOrder');

        Route::group(['prefix' => 'api'], function () {
            get('/products', 'ProductController@index');
            post('/products', 'ProductController@store');
            post('/product/{id}', 'ProductController@update');
            get('/product/item/{id}', 'ProductController@getItems');
            get('/product/detail/{id}', 'ProductController@detail');
            get('/product/delete/{id}', 'ProductController@delete');
            get('/product/publish/{id}', 'ProductController@publish');
            get('/product/unpublish/{id}', 'ProductController@unpublish');
            get('/subcategory/{id}/sizes', 'SubcategoryController@getSizesForSelect');
            get('/categories', 'CategoryController@getForSelect');
            get('/subcategories', 'SubcategoryController@getForSelect');
            post('/items', 'ProductItemController@incrementItem');
            post('/items/set', 'ProductItemController@setItem');
            post('/items/detail', 'ProductItemController@getSizeInfo');
            post('/items/decrement', 'ProductItemController@decrementItem');
            get('/pos/items/{barcode}', 'PosController@getProductByBarcode');
            get('/pos/reset', 'PosController@resetCart');
            get('/pos/purchase', 'PosController@posPurchase');
            get('/orders', 'OrderController@index');
            get('/order/statuses', 'OrderController@statuses');
            post('/orders/confirmpayment', 'OrderController@confirmPayment');
            get('/users', 'UserController@index');
            get('/users/cashiers', 'UserController@indexCashier');
            get('/user/{id}/activate', 'UserController@activate');
            get('/user/{id}/deactivate', 'UserController@deactivate');
            get('/user/{id}/delete', 'UserController@delete');
            post('/users/staffs', 'UserController@createAdmin');
            post('/users/merchants', 'UserController@createMerchant');
            post('/users/merchants/{id}/edit', 'UserController@editMerchant');
            post('/users/member/{id}/edit', 'UserController@editMember');
            post('/users/cashiers/{id}/edit', 'UserController@editCashier');
            post('/users/staffs/{id}/edit', 'UserController@editStaff');
            get('/users/guests', 'GuestController@index');
            get('/orders/confirmed', 'ShippingController@index');
            post('/orders/resi', 'ShippingController@submitResi');
            get('/report/sales', 'ReportController@salesReport');
            get('/report/financial', 'ReportController@obidReport');
            get('/users/merchants/select', 'UserController@getMerchantForSelect');
            get('/sliders', 'SliderController@index');
            post('/sliders', 'SliderController@updateImage');
            get('/sliders/{id}/clear', 'SliderController@clearImage');
            get('/colors/option', 'ColorController@getAllForSelect');
            get('/demografi/users', 'DemografiController@getUserStat');
            get('/demografi/brand/{brandId}', 'DemografiController@getBrandStat');
            get('/sizes', 'SizeController@getSizesForSelect');
            get('/dashboard/sales/chart', 'DashboardController@salesReport');
            get('/dashboard/money', 'DashboardController@getMerchantMoney');
            get('/dashboard/user', 'DashboardController@getUserChart');
            get('/coupons', 'CouponController@getCoupons');
            post('/coupons', 'CouponController@store');
            post('/coupons/custom', 'CouponController@customStore');
            get('/coupons/delete/{couponId}', 'CouponController@deleteCoupon');
            post('/discount', 'DiscountController@updateDiscount');
            get('/currency', 'CurrencyController@getCurrency');
            post('/currency', 'CurrencyController@setCurrency');
            get('/token', 'TokenController@index');
            post('/token', 'TokenController@create');
            get('/token/delete/{id}', 'TokenController@delete');
            get('/notif', 'OrderController@getOrderNotif');
        });
    });
});

Route::group(['domain' => 'purchase.'.env('APP_DOMAIN')], function () {
    get('/login', 'Purchasing\AuthController@loginPage');
    post('/login', 'Purchasing\AuthController@login');
    Route::group(['namespace' => 'Purchasing', 'middleware' => 'mustPurchase'], function () {
        get('/', 'DashboardController@index');
        get('/orders', 'OrderController@orders');
        get('/logout', 'AuthController@logout');
        Route::group(['prefix' => 'api'], function () {
            get('/orders', 'OrderController@index');
            get('/order/statuses', 'OrderController@statuses');
            post('/orders/confirmpayment', 'OrderController@confirmPayment');
            get('/notif', 'OrderController@getOrderNotif');
        });
    });
});

Route::group(['domain' => 'inventory.'.env('APP_DOMAIN')], function () {
    get('/login', 'Inventory\AuthController@loginPage');
    post('/login', 'Inventory\AuthController@login');
    Route::group(['namespace' => 'Inventory', 'middleware' => 'mustInventory'], function () {
        get('/', 'DashboardController@index');
        get('/products', 'ProductController@products');
        get('/products/create', 'ProductController@create');
        get('/products/{id}', 'ProductController@edit');
        get('/logout', 'AuthController@logout');
        Route::group(['prefix' => 'api'], function () {
            get('/products', 'ProductController@index');
            post('/products', 'ProductController@store');
            post('/product/{id}', 'ProductController@update');
            get('/product/item/{id}', 'ProductController@getItems');
            get('/product/detail/{id}', 'ProductController@detail');
            get('/product/delete/{id}', 'ProductController@delete');
            get('/product/publish/{id}', 'ProductController@publish');
            get('/product/unpublish/{id}', 'ProductController@unpublish');
            get('/subcategory/{id}/sizes', 'SubcategoryController@getSizesForSelect');
            get('/categories', 'CategoryController@getForSelect');
            get('/subcategories', 'SubcategoryController@getForSelect');
            post('/items', 'ProductItemController@incrementItem');
            post('/items/set', 'ProductItemController@setItem');
            post('/items/detail', 'ProductItemController@getSizeInfo');
            post('/items/decrement', 'ProductItemController@decrementItem');
            get('/colors/option', 'ColorController@getAllForSelect');
            get('/sizes', 'SizeController@getSizesForSelect');
            post('/discount', 'DiscountController@updateDiscount');
            get('/users/merchants/select', 'MerchantController@getMerchantForSelect');
        });

    });
});

Route::group(['domain' => 'shipper.'.env('APP_DOMAIN')], function () {
    get('/login', 'Shipping\AuthController@loginPage');
    post('/login', 'Shipping\AuthController@login');
    Route::group(['namespace' => 'Shipping', 'middleware' => 'mustShipper'], function () {
        get('/', 'ShippingController@dashboard');
        get('/shippings', 'ShippingController@shippings');
        get('/logout', 'AuthController@logout');
        get('/orders/{orderid}/print', 'OrderController@printOrder');
        Route::group(['prefix' => 'api'], function () {
            get('/orders/confirmed', 'ShippingController@index');
            post('/orders/resi', 'ShippingController@submitResi');
        });
    });
});

Route::group(['domain' => 'finance.'.env('APP_DOMAIN')], function () {
    get('/login', 'Finance\AuthController@loginPage');
    post('/login', 'Finance\AuthController@login');
    Route::group(['namespace' => 'Finance', 'middleware' => 'mustFinance'], function () {
        get('/', 'DashboardController@index');
        get('/report', 'ReportController@report');
        get('/currency', 'CurrencyController@currency');
        get('/originalbrands-report', 'ReportController@obidFinancialReport');
        get('/invoice/merchant', 'InvoiceController@generateInvoiceMerchant');
        get('/logout', 'AuthController@logout');
        Route::group(['prefix' => 'api'], function () {
            get('/report/sales', 'ReportController@salesReport');
            get('/users/merchants/select', 'MerchantController@getMerchantForSelect');
            get('/currency', 'CurrencyController@getCurrency');
            post('/currency', 'CurrencyController@setCurrency');
            get('/report/financial', 'ReportController@obidReport');
        });
    });
});

Route::group(['domain' => 'reg.'.env('APP_DOMAIN'), 'namespace' => 'Regmerchant'], function () {
    get('/', 'RegmerchantController@create');
    get('/create', 'RegmerchantController@index');
    post('/reg', 'RegmerchantController@store');
    post('/token', 'RegmerchantController@setToken');
    get('/provinces', 'CityApiController@getProvinces');
    get('/provinces/{provinceId}/city', 'CityApiController@getCitiesByProvince');
    get('/check/username/{username}', 'ValidationController@checkUsername');
    get('/check/nohp/{nohp}', 'ValidationController@checkNohp');
    get('/check/email/{email}', 'ValidationController@checkEmail');
});

Route::group(['domain' => 'merchantcenter.'.env('APP_DOMAIN')], function () {
    get('/login', 'Merchant\AccountController@login');
    post('/login', 'Merchant\AccountController@postLogin');
    Route::group(['namespace' => 'Merchant', 'middleware' => 'mustMerchant'], function () {
        get('/', 'DashboardController@index');
        get('/products', 'MerchantController@products');
        get('/products/create', 'MerchantController@createProduct');
        get('/product/{id}', 'MerchantController@editProduct');
        get('/orders', 'MerchantController@orders');
        get('/report', 'MerchantController@report');
        get('/settings', 'MerchantController@editAccount');
        get('/pos/orders', 'MerchantController@posOrders');
        get('/pos/accounts', 'MerchantController@posAccounts');
        get('/pos/account/create', 'MerchantController@createPosAccount');
        get('/pos/account/{id}/edit', 'MerchantController@editPosAccount');
        get('/logout', 'AccountController@logout');
        get('/demografi', 'MerchantController@demografi');
        get('/voucher', 'MerchantController@coupons');
        get('/staffs', 'MerchantController@staffs');
        get('/staffs/create', 'MerchantController@createStaff');
        get('/staffs/{id}/edit', 'MerchantController@editStaff');
        Route::group(['prefix' => 'api'], function () {
            get('/products', 'ProductController@index');
            post('/products', 'ProductController@store');
            get('/product/detail/{id}', 'ProductController@detail');
            post('/product/{id}', 'ProductController@update');
            get('/product/item/{id}', 'ProductController@getItems');
            get('/product/delete/{id}', 'ProductController@delete');
            get('/orders', 'OrderItemController@getMerchantOrderItems');
            get('/orders/accept/{id}', 'OrderItemController@acceptOrderItem');
            get('/subcategories', 'SubcategoryController@getForSelect');
            get('/categories', 'CategoryController@getForSelect');
            get('/subcategory/{id}/sizes', 'SubcategoryController@getSizesForSelect');
            post('/items', 'ProductItemController@incrementItem');
            post('/items/set', 'ProductItemController@setItem');
            post('/items/detail', 'ProductItemController@getSizeInfo');
            post('/items/decrement', 'ProductItemController@decrementItem');
            get('/report', 'ReportController@generateReport');
            post('/account/edit', 'AccountController@editProfile');
            get('/colors/option', 'ColorController@getAllForSelect');
            get('/sizes', 'SizeController@getSizesForSelect');
            get('/pos/orders', 'OrderController@getPosOrders');
            get('/pos/accounts', 'AccountController@getCashiersProfile');
            get('/pos/{id}/cash', 'PosController@getPosCash');
            get('/pos/account/{id}/activate', 'AccountController@activateAccount');
            get('/pos/account/{id}/deactivate', 'AccountController@deactivateAccount');
            get('/pos/account/{id}/delete', 'AccountController@deleteAccount');
            post('/pos/cash', 'PosController@setPosCash');
            post('/pos/accounts', 'AccountController@storeCashier');
            post('/pos/account/{id}/edit', 'AccountController@editCashier');
            get('/staffs', 'AccountController@getStaffsMerchant');
            post('/staffs', 'AccountController@storeStaff');
            post('/staffs/{id}/edit', 'AccountController@editStaff');
            get('/dashboard/{brandId}/products', 'DashboardController@getProductsByCategory');
            get('/dasboard/sales/chart', 'DashboardController@getSalesChart');
            post('/discount', 'DiscountController@updateDiscount');
            get('/notif', 'SalesNotifController@getNotif');
            get('/demografi/brand/{brandId}', 'DemografiController@getBrandStat');
            get('/coupons', 'CouponController@getCoupons');
            post('/coupons', 'CouponController@store');
            post('/coupons/custom', 'CouponController@customStore');
            get('/coupons/delete/{couponId}', 'CouponController@deleteCoupon');
        });
    });
});
Route::group(['domain' => '{username}.'.env('APP_DOMAIN')], function () {
    get('/', function ($username) {
        $merchant = \OBID\Models\User::whereHas('roles', function ($q) {
            $q->where('name', 'affiliate');
        })->where('username', $username)->active()->first();

        if ($merchant) {
            return redirect()->to(env('APP_DOMAIN').'/brand/' . $username);
        }

        abort(404);
    });

    get('/pos/login', 'Pos\AuthController@login');
    post('/pos/login', 'Pos\AuthController@postLogin');
    Route::group(['prefix' => 'pos', 'namespace' => 'Pos', 'middleware' => 'mustCashier'], function () {
        get('/', 'PosController@index');
        get('/logout', 'AuthController@logout');
        get('/faktur/{id}', 'PDFController@generatePDF');

        Route::group(['prefix' => 'api'], function () {
            get('/cart/total', 'PosController@getCartTotalPrice');
            post('/payment/cash', 'PosController@storeCash');
            post('/payment/edc', 'PosController@storeEdc');
            get('/options/merchant/{merchantId}', 'OptionsController@getOptionsMerchant');
            get('/products/merchant/{merchantId}', 'ProductController@getProducts');
            post('/items/product', 'PosController@addProduct');
            post('/items/update', 'PosController@updateItem');
            post('/items/delete', 'PosController@deleteItem');
            post('/items', 'PosController@addCart');
            get('/items', 'PosController@getAll');
            post('/items', 'ProductItemController@incrementItem');
            post('/items/set', 'ProductItemController@setItem');
            post('/items/detail', 'ProductItemController@getSizeInfo');
            post('/items/decrement', 'ProductItemController@decrementItem');
            get('/order/{id}', 'OrderController@getOrder');
        });
    });

    get('/staff/login', 'StaffMerchant\AuthController@login');
    post('/staff/login', 'StaffMerchant\AuthController@postLogin');
    Route::group([
        'prefix' => 'staff',
        'namespace' => 'StaffMerchant',
//        'middleware' => 'mustStaffMerchant'
    ], function () {
        get('/', 'PagesController@index');
        get('/products', 'PagesController@products');
        get('/products/create', 'PagesController@createProduct');
        get('/product/{id}', 'PagesController@editProduct');
        get('/logout', 'AuthController@logout');

        Route::group(['prefix' => 'api'], function () {
            get('/products', 'ProductController@index');
            post('/products', 'ProductController@store');
            post('/product/{id}', 'ProductController@update');
            get('/product/delete/{id}', 'ProductController@delete');
            get('/product/publish/{id}', 'ProductController@publish');
            get('/product/unpublish/{id}', 'ProductController@unpublish');
            get('/product/detail/{id}', 'ProductController@detail');
            post('/discount', 'DiscountController@updateDiscount');
            get('/subcategory/{id}/sizes', 'SubcategoryController@getSizesForSelect');
            get('/categories', 'CategoryController@getForSelect');
            get('/subcategories', 'SubcategoryController@getForSelect');
            post('/items', 'ProductItemController@incrementItem');
            post('/items/set', 'ProductItemController@setItem');
            post('/items/detail', 'ProductItemController@getSizeInfo');
            post('/items/decrement', 'ProductItemController@decrementItem');
            get('/colors/option', 'ColorController@getAllForSelect');
            get('/sizes', 'SizeController@getSizesForSelect');
            post('/discount', 'DiscountController@updateDiscount');
            get('/users/merchants/select', 'MerchantController@getMerchantForSelect');
        });
    });
});