<?php


namespace OBID\Http\Middleware;


use Closure;
use Illuminate\Contracts\Auth\Guard;

class MustLoggedinCheckout
{
    /**
     * @var Guard
     */
    protected $auth;

    /**
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->check()) {
            if (!$this->auth->user()->hasRole('customer')) {
                $this->auth->logout();
            }
        }

        return $next($request);
    }
}