<?php

namespace OBID\Http\Middleware;


use Closure;
use Illuminate\Contracts\Auth\Guard;

class MustStaffMerchant
{
    /**
     * @var Guard
     */
    protected $auth;

    /**
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest() || !$this->auth->user()->hasRole('staffmerchant')) {
            if ($request->ajax()) {
                return response()->json([
                    'status' => 'unauthorized',
                    'redirect' => url('/staff/login')
                ], 401);
            } else {
                return redirect()->guest('/staff/login');
            }
        }

        return $next($request);
    }
}