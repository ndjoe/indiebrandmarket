<?php

namespace OBID\Http\Requests\Email;


use OBID\Http\Requests\Request;

class EmailRequest extends Request
{
    public function rules()
    {
        return [
            'email' => 'required|email'
        ];
    }

    public function authorize()
    {
        return true;
    }
}