<?php

namespace OBID\Http\Requests\RegToken;


use OBID\Http\Requests\Request;

class CreateRegTokenRequest extends Request
{
    public function rules()
    {
        return [
            'token' => 'required|string',
            'expired' => 'required|date'
        ];
    }

    public function authorize()
    {
        return true;
    }
}