<?php


namespace OBID\Http\Requests\Slider;


use OBID\Http\Requests\Request;

class UpdateSliderRequest extends Request
{
    public function rules()
    {
        return [
            'slideId' => 'required|integer',
            'image' => 'required|mimes:jpg,jpeg,png'
        ];
    }

    public function authorize()
    {
        return true;
    }
}