<?php
namespace OBID\Http\Requests\Cart;


use OBID\Http\Requests\Request;

class NewCartRequest extends Request
{
    public function rules()
    {
        return [
            'productId' => 'integer|required',
            'sizeId' => 'integer|required',
            'qty' => 'integer|required'
        ];
    }

    public function authorize()
    {
        return true;
    }
}