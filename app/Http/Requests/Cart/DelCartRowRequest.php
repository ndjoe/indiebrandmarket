<?php


namespace OBID\Http\Requests\Cart;


use OBID\Http\Requests\Request;

class DelCartRowRequest extends Request
{
    public function rules()
    {
        return [
            'rowid' => 'required|string'
        ];
    }

    public function authorize()
    {
        return true;
    }
}