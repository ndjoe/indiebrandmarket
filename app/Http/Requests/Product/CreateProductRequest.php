<?php


namespace OBID\Http\Requests\Product;


use OBID\Http\Requests\Request;

class CreateProductRequest extends Request
{
    public function rules()
    {
        $rules = [
            'nama' => 'required|string',
            'kategori' => 'integer|required',
            'photo-1' => 'required|mimes:jpg,jpeg,png',
            'harga' => 'numeric|required',
            'color' => 'required',
            'berat' => 'numeric|required',
            'subkategori' => 'required',
            'percent' => 'required|numeric',
            'expired' => 'required|date',
            'photo-2' => 'sometimes|mimes:jpg,jpeg,png',
            'photo-3' => 'sometimes|mimes:jpg,jpeg,png',
            'photo-4' => 'sometimes|mimes:jpg,jpeg,png',
            'photo-5' => 'sometimes|mimes:jpg,jpeg,png',
            'photo-6' => 'sometimes|mimes:jpg,jpeg,png',
            'photo-7' => 'sometimes|mimes:jpg,jpeg,png',
            'photo-8' => 'sometimes|mimes:jpg,jpeg,png',
            'description' => 'string|max:600',
        ];

        if ($this->request->has('size')) {
            foreach ($this->request->get('size') as $key => $value) {
                $rules['size.' . $key] = 'required';
            }
        }

        if ($this->request->has('qty')) {
            foreach ($this->request->get('qty') as $key => $value) {
                $rules['qty.' . $key] = 'required|numeric';
            }
        }

        if ($this->request->has('desc')) {
            foreach ($this->request->get('desc') as $key => $value) {
                $rules['desc.' . $key] = 'string';
            }
        }

        return $rules;
    }

    public function authorize()
    {
        return true;
    }
}