<?php

namespace OBID\Http\Requests\Product;


use OBID\Http\Requests\Request;

class EditProductRequest extends Request
{
    public function rules()
    {
        return [
            'nama' => 'required|string',
            'kategori' => 'integer|required',
            'photo-1' => 'sometimes|mimes:jpg,jpeg,png',
            'harga' => 'numeric|required',
            'color' => 'required',
            'berat' => 'numeric|required',
            'subkategori' => 'required',
            'percent' => 'required|numeric',
            'expired' => 'required|date',
            'photo-2' => 'sometimes|mimes:jpg,jpeg,png',
            'photo-3' => 'sometimes|mimes:jpg,jpeg,png',
            'photo-4' => 'sometimes|mimes:jpg,jpeg,png',
            'photo-5' => 'sometimes|mimes:jpg,jpeg,png',
            'photo-6' => 'sometimes|mimes:jpg,jpeg,png',
            'photo-7' => 'sometimes|mimes:jpg,jpeg,png',
            'photo-8' => 'sometimes|mimes:jpg,jpeg,png',
            'delphoto-2' => 'sometimes|string',
            'delphoto-3' => 'sometimes|string',
            'delphoto-4' => 'sometimes|string',
            'delphoto-5' => 'sometimes|string',
            'delphoto-6' => 'sometimes|string',
            'delphoto-7' => 'sometimes|string',
            'delphoto-8' => 'sometimes|string',
            'description' => 'string|max:600',
            'sizing' => 'string|max:600'
        ];
    }

    public function authorize()
    {
        return true;
    }
}