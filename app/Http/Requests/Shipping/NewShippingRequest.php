<?php


namespace OBID\Http\Requests\Shipping;


use OBID\Http\Requests\Request;

class NewShippingRequest extends Request
{
    public function rules()
    {
        return [
            'orderId' => 'required|integer',
            'noResi' => 'required|numeric',
        ];
    }

    public function authorize()
    {
        return true;
    }
}