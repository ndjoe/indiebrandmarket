<?php


namespace OBID\Http\Requests\Order;


use OBID\Http\Requests\Request;

class NewCheckoutOrderRequest extends Request
{
    public function rules()
    {
        return [
            'nama' => 'required|string',
            'nohp' => 'required|numeric',
            'kota' => 'required|string',
            'kodepos' => 'required|numeric',
            'alamat' => 'required|string',
            'negara' => 'string|required',
            'provinsi' => 'string|required',
            'shippingCost' => 'required|numeric',
            'shippingName' => 'required|string',
            'payment' => 'required|string'
        ];
    }

    public function authorize()
    {
        return true;
    }
}