<?php

namespace OBID\Http\Requests\Order;


use OBID\Http\Requests\Request;

class OrderTrackingRequest extends Request
{
    public function rules()
    {
        return [
            'nomororder' => 'required'
        ];
    }

    public function authorize()
    {
        return true;
    }
}