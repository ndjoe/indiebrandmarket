<?php


namespace OBID\Http\Requests\Order;


use OBID\Http\Requests\Request;

class PaymentConfirmationRequest extends Request
{

    public function rules()
    {
        return [
            'orderId' => 'required|integer',
            'bank' => 'required|string',
        ];
    }

    public function authorize()
    {
        return true;
    }
}