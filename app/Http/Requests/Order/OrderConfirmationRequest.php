<?php


namespace OBID\Http\Requests\Order;


use OBID\Http\Requests\Request;

class OrderConfirmationRequest extends Request
{
    public function rules()
    {
        return [
            'nama' => 'required|string',
            'bank' => 'required|string',
            'amount' => 'required',
            'nomororder' => 'required',
            'tgl' => 'required|date'
        ];
    }

    public function authorize()
    {
        return true;
    }
}