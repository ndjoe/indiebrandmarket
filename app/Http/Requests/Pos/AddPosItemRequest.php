<?php


namespace OBID\Http\Requests\Pos;


use OBID\Http\Requests\Request;

class AddPosItemRequest extends Request
{
    public function rules()
    {
        return [
            'uid' => 'required|string'
        ];
    }

    public function authorize()
    {
        return true;
    }
}