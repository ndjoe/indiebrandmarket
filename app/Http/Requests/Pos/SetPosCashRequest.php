<?php


namespace OBID\Http\Requests\Pos;


use OBID\Http\Requests\Request;

class SetPosCashRequest extends Request
{
    public function rules()
    {
        return [
            'new' => 'required|numeric'
        ];
    }

    public function authorize()
    {
        return true;
    }
}