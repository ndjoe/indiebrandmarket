<?php


namespace OBID\Http\Requests\Pos;


use OBID\Http\Requests\Request;

class AddProductRequest extends Request
{
    public function rules()
    {
        return [
            'productId' => 'required',
            'sizeId' => 'required',
            'qty' => 'required'
        ];
    }

    public function authorize()
    {
        return true;
    }
}