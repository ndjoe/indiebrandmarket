<?php


namespace OBID\Http\Requests\Pos;


use OBID\Http\Requests\Request;

class UpdateQtyPosItemRequest extends Request
{
    public function rules()
    {
        return [
            'id' => 'required|integer',
            'qty' => 'required|integer'
        ];
    }

    public function authorize()
    {
        return true;
    }
}