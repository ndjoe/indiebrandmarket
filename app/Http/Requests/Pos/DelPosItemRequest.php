<?php


namespace OBID\Http\Requests\Pos;


use OBID\Http\Requests\Request;

class DelPosItemRequest extends Request
{
    public function rules()
    {
        return [
            'id' => 'required|integer'
        ];
    }

    public function authorize()
    {
        return true;
    }
}