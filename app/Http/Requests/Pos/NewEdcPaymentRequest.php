<?php


namespace OBID\Http\Requests\Pos;


use OBID\Http\Requests\Request;

class NewEdcPaymentRequest extends Request
{
    public function rules()
    {
        return [
            'bank' => 'required|string',
            'noref' => 'required|string',
            'cardNumber' => 'required|numeric',
            'expirationDate' => 'required',
            'nama' => 'required|string'
        ];
    }

    public function authorize()
    {
        return true;
    }
}