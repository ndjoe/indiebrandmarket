<?php


namespace OBID\Http\Requests\Pos;


use OBID\Http\Requests\Request;

class NewCashPaymentRequest extends Request
{
    public function rules()
    {
        return [
            'cash' => 'required|numeric',
            'nama' => 'required|string'
        ];
    }

    public function authorize()
    {
        return true;
    }
}