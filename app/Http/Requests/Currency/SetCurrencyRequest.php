<?php

namespace OBID\Http\Requests\Currency;


use OBID\Http\Requests\Request;

class SetCurrencyRequest extends Request
{
    public function rules()
    {
        return [
            'new' => 'required|numeric'
        ];
    }

    public function authorize()
    {
        return true;
    }
}