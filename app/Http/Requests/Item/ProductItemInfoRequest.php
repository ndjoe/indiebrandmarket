<?php

namespace OBID\Http\Requests\Item;


use OBID\Http\Requests\Request;

class ProductItemInfoRequest extends Request
{
    public function rules()
    {
        return [
            'productId' => 'required|numeric',
            'sizeId' => 'required'
        ];
    }

    public function authorize()
    {
        return true;
    }
}