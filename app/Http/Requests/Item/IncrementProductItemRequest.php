<?php


namespace OBID\Http\Requests\Item;


use OBID\Http\Requests\Request;

class IncrementProductItemRequest extends Request
{
    public function rules()
    {
        return [
            'product' => 'required|numeric',
            'size' => 'required',
            'qty' => 'required|numeric',
            'desc' => 'sometimes|string'
        ];
    }

    public function authorize()
    {
        return true;
    }
}