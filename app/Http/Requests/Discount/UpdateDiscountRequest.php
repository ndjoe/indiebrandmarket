<?php


namespace OBID\Http\Requests\Discount;


use OBID\Http\Requests\Request;

class UpdateDiscountRequest extends Request
{
    public function rules()
    {
        return [
            'discountId' => 'required|numeric',
            'expired' => 'required|date',
            'percent' => 'required|numeric'
        ];
    }

    public function authorize()
    {
        return true;
    }
}