<?php


namespace OBID\Http\Requests\Discount;


use OBID\Http\Requests\Request;

class SubmitDiscountRequest extends Request
{
    public function rules()
    {
        return [
            'code' => 'required|string'
        ];
    }

    public function authorize()
    {
        return true;
    }
}