<?php


namespace OBID\Http\Requests\Account;


use OBID\Http\Requests\Request;

class ActivateByCodeRequest extends Request
{
    public function rules()
    {
        return [
            'code' => 'required|string'
        ];
    }

    public function authorize()
    {
        return true;
    }
}