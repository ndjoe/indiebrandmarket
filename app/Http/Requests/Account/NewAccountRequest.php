<?php


namespace OBID\Http\Requests\Account;


use OBID\Http\Requests\Request;

class NewAccountRequest extends Request
{
    public function rules()
    {
        return [
            'username' => 'required|alpha_dash|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|alpha_num|confirmed|min:6',
            'nama' => 'required|string',
            'nohp' => 'required|regex:/^(0+8*\d)\d+/|unique:users,nohp'
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'username.required' => 'Username tidak diisi',
            'username.unique' => 'Username sudah terambil',
            'email.required' => 'Email tidak diisi',
            'email.unique' => 'email sudah terambil',
            'password.min' => 'password minimal 6 karakter',
            'password.required' => 'passwordnya mana',
            'password.alpha_num' => 'harus alpha numerik',
            'password.confirmed' => 'password dan konfirmasi password tidak sama',
            'nama.required' => 'namanya mana',
            'nohp.regex' => 'nomor hape tidak sesuai format',
            'nohp.unique' => 'nomor hape sudah terambil'
        ];
    }
}