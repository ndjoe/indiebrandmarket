<?php


namespace OBID\Http\Requests\Account;


use OBID\Http\Requests\Request;

class NewAdminRequest extends Request
{
    public function rules()
    {
        return [
            'username' => 'required|alpha_dash|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|alphanum|confirmed',
            'nama' => 'required|string',
            'nohp' => 'required|numeric',
            'role' => 'required|string'
        ];
    }

    public function authorize()
    {
        return \Auth::user()->hasRole('admin');
    }
}