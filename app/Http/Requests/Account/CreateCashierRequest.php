<?php


namespace OBID\Http\Requests\Account;


use OBID\Http\Requests\Request;

class CreateCashierRequest extends Request
{
    public function rules()
    {
        return [
            'username' => 'required|alpha_dash|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|alphanum|confirmed',
            'nama' => 'required|string'
        ];
    }

    public function authorize()
    {
        return true;
    }
}