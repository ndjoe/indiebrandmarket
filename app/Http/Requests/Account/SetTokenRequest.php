<?php

namespace OBID\Http\Requests\Account;


use OBID\Http\Requests\Request;

class SetTokenRequest extends Request
{
    public function rules()
    {
        return [
            'token' => 'required|string'
        ];
    }

    public function authorize()
    {
        return true;
    }
}