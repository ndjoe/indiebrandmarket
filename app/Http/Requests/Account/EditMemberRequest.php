<?php


namespace OBID\Http\Requests\Account;


use OBID\Http\Requests\Request;

class EditMemberRequest extends Request
{
    public function rules()
    {
        return [
            'name' => 'string',
            'birthday' => 'date',
            'nohp' => 'numeric',
            'password' => 'sometimes|alphanum|confirmed'
        ];
    }

    public function authorize()
    {
        return \Auth::user()->hasRole('admin');
    }
}