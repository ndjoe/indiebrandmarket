<?php


namespace OBID\Http\Requests\Account;


use OBID\Http\Requests\Request;

class EditProfileRequest extends Request
{
    public function rules()
    {
        return [
            'name' => 'string',
            'birthday' => 'date',
            'gender' => 'string',
            'kota' => 'string',
            'provinsi' => 'string',
            'alamat' => 'string',
            'kodepos' => 'numeric',
            'nohp' => 'numeric'
        ];
    }

    public function authorize()
    {
        return true;
    }
}