<?php


namespace OBID\Http\Requests\Account;


use OBID\Http\Requests\Request;

class LoginRequest extends Request
{
    public function rules()
    {
        return [
            'username' => 'required|alpha_dash',
            'password' => 'required|alpha_num'
        ];
    }

    public function authorize()
    {
        return true;
    }
}