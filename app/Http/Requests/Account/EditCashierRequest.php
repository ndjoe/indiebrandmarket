<?php


namespace OBID\Http\Requests\Account;


use OBID\Http\Requests\Request;

class EditCashierRequest extends Request
{
    public function rules()
    {
        return [
            'password' => 'sometimes|alphanum|confirmed',
            'nama' => 'required|string'
        ];
    }

    public function authorize()
    {
        return true;
    }
}