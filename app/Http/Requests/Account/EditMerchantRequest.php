<?php


namespace OBID\Http\Requests\Account;


use OBID\Http\Requests\Request;

class EditMerchantRequest extends Request
{
    public function rules()
    {
        return [
            'password' => 'sometimes|alphanum|confirmed',
            'nama' => 'required|string',
            'kota' => 'required|string',
            'owner' => 'required|string',
            'bank' => 'required|string',
            'provinsi' => 'required|string',
            'norek' => 'required|numeric',
            'kodepos' => 'required|numeric',
            'nohp' => 'required|numeric',
            'about' => 'string|max:600',
            'alamat' => 'required|string',
            'bannerImage' => 'sometimes|mimes:jpg,jpeg,png',
            'logoImage' => 'sometimes|mimes:jpg,jpeg,png',
            'ownerRek' => 'required|string'
        ];
    }

    public function authorize()
    {
        return true;
    }
}