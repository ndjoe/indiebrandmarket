<?php


namespace OBID\Http\Requests\Account;


use OBID\Http\Requests\Request;

class RequestForgotPasswordRequest extends Request
{
    public function rules()
    {
        return [
            'email' => 'required|email'
        ];
    }

    public function authorize()
    {
        return true;
    }
}