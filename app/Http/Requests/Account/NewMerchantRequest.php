<?php


namespace OBID\Http\Requests\Account;


use OBID\Http\Requests\Request;

class NewMerchantRequest extends Request
{
    public function rules()
    {
        return [
            'username' => 'required|alpha_dash|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'nama' => 'required|string',
            'kota' => 'required|string',
            'owner' => 'required|string',
            'bank' => 'required|string',
            'provinsi' => 'required|string',
            'norek' => 'required|numeric',
            'kodepos' => 'required|numeric',
            'nohp' => 'required|numeric',
            'about' => 'string|max:600',
            'alamat' => 'required|string',
            'ownerRek' => 'required|string'
        ];
    }

    public function authorize()
    {
        return true;
    }
}