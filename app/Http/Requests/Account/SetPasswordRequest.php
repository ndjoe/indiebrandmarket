<?php


namespace OBID\Http\Requests\Account;


use OBID\Http\Requests\Request;

class SetPasswordRequest extends Request
{
    public function rules()
    {
        return [
            'password' => 'required|confirmed|min:6|alpha_num'
        ];
    }

    public function authorize()
    {
        return true;
    }
}