<?php

namespace OBID\Http\Requests\Account;


use OBID\Http\Requests\Request;

class EditStaffRequest extends Request
{
    public function rules()
    {
        return [
            'name' => 'string',
            'nohp' => 'numeric',
            'password' => 'sometimes|alphanum|confirmed'
        ];
    }

    public function authorize()
    {
        return \Auth::user()->hasRole('admin');
    }
}