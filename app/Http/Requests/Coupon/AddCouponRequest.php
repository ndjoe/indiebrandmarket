<?php


namespace OBID\Http\Requests\Coupon;


use OBID\Http\Requests\Request;

class AddCouponRequest extends Request
{
    public function rules()
    {
        return [
            'value' => 'required|numeric',
            'brand' => 'sometimes|numeric',
            'minimum' => 'required|numeric',
            'expired' => 'required|date',
            'setting' => 'required|string',
            'qty' => 'required|numeric'
        ];
    }

    public function authorize()
    {
        return true;
    }
}