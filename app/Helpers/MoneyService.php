<?php

namespace OBID\Helpers;


use OBID\Models\Currency;

class MoneyService
{
    protected $amount;
    protected $currency;

    /**
     * MoneyService constructor.
     * @param $amount
     * @param string $currency
     */
    public function __construct($amount, $currency = 'IDR')
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function getDollarValue()
    {
        if ($this->currency === 'USD') {
            return $this->amount;
        }

        $currency = Currency::find(1);

        return $this->amount / $currency->value;
    }

    public function getRupiahValue()
    {
        if ($this->currency === 'IDR') {
            return $this->amount;
        }

        $currency = Currency::find(1);

        return $this->amount * $currency->value;
    }
}