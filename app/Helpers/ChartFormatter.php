<?php


namespace OBID\Helpers;


use Carbon\Carbon;
use Illuminate\Support\Str;

class ChartFormatter
{
    /**
     * @param array $input
     * @return array
     */
    public function formatChartDonut($input = [])
    {
        $result = [];
        foreach ($input as $key => $value) {
            array_push($result, [
                'label' => Str::upper($key),
                'value' => $value
            ]);
        }

        return $result;
    }

    /**
     * @param $input
     * @param $type
     * @return array
     */
    public function formatSalesChartLine($input, $type)
    {
        $arrayResult = $this->generateEmptyChartData($type);

        foreach ($input as $r) {
            if ($type === 'month') {
                foreach ($arrayResult as $key => $value) {
                    $weekNumber = Carbon::parse($r->time)->weekOfMonth;
                    if ($value['time'] === $weekNumber) {
                        $arrayResult[$key]['value'] += $r->value;
                    }
                }
            } elseif ($type === 'day') {
                foreach ($arrayResult as $key => $value) {
                    $hour = Carbon::parse($r->time)->hour;
                    if ($value['time'] === $hour) {
                        $arrayResult[$key]['value'] += $r->value;
                    }
                }
            } elseif ($type === 'year') {
                foreach ($arrayResult as $key => $value) {
                    $monthNumber = Carbon::parse($r->time)->month;
                    if ($value['time'] === $monthNumber) {
                        $arrayResult[$key]['value'] += $r->value;
                    }
                }
            }
        }

        if ($type === 'month') {
            foreach ($arrayResult as $key => $value) {
                $arrayResult[$key]['time'] = 'Minggu ' . $value['time'];
            }
        } elseif ($type === 'day') {
            foreach ($arrayResult as $key => $value) {
                $arrayResult[$key]['time'] = 'Jam ' . $value['time'];
            }
        } else {
            foreach ($arrayResult as $key => $value) {
                $arrayResult[$key]['time'] = 'Bulan  ' . $value['time'];
            }
        }

        return $arrayResult;
    }

    /**
     * @param $type
     * @return array
     */
    private function generateEmptyChartData($type)
    {
        switch ($type) {
            case 'day':
                $result = [
                    ['time' => 1, 'value' => 0],
                    ['time' => 2, 'value' => 0],
                    ['time' => 3, 'value' => 0],
                    ['time' => 4, 'value' => 0],
                    ['time' => 5, 'value' => 0],
                    ['time' => 6, 'value' => 0],
                    ['time' => 7, 'value' => 0],
                    ['time' => 8, 'value' => 0],
                    ['time' => 9, 'value' => 0],
                    ['time' => 10, 'value' => 0],
                    ['time' => 11, 'value' => 0],
                    ['time' => 12, 'value' => 0],
                    ['time' => 13, 'value' => 0],
                    ['time' => 14, 'value' => 0],
                    ['time' => 15, 'value' => 0],
                    ['time' => 16, 'value' => 0],
                    ['time' => 17, 'value' => 0],
                    ['time' => 18, 'value' => 0],
                    ['time' => 19, 'value' => 0],
                    ['time' => 20, 'value' => 0],
                    ['time' => 21, 'value' => 0],
                    ['time' => 22, 'value' => 0],
                    ['time' => 23, 'value' => 0],
                ];
                break;
            case 'month':
                $result = [
                    ['time' => 1, 'value' => 0,],
                    ['time' => 2, 'value' => 0,],
                    ['time' => 3, 'value' => 0,],
                    ['time' => 4, 'value' => 0,],
                ];
                break;
            case 'year':
                $result = [
                    ['time' => 1, 'value' => 0],
                    ['time' => 2, 'value' => 0],
                    ['time' => 3, 'value' => 0],
                    ['time' => 4, 'value' => 0],
                    ['time' => 5, 'value' => 0],
                    ['time' => 6, 'value' => 0],
                    ['time' => 7, 'value' => 0],
                    ['time' => 8, 'value' => 0],
                    ['time' => 9, 'value' => 0],
                    ['time' => 10, 'value' => 0],
                    ['time' => 11, 'value' => 0],
                    ['time' => 12, 'value' => 0],
                    ['time' => 13, 'value' => 0],
                    ['time' => 14, 'value' => 0],
                    ['time' => 15, 'value' => 0],
                    ['time' => 16, 'value' => 0],
                    ['time' => 17, 'value' => 0],
                    ['time' => 18, 'value' => 0],
                    ['time' => 19, 'value' => 0],
                    ['time' => 20, 'value' => 0],
                    ['time' => 21, 'value' => 0],
                    ['time' => 22, 'value' => 0],
                    ['time' => 23, 'value' => 0],
                    ['time' => 24, 'value' => 0],
                    ['time' => 25, 'value' => 0],
                    ['time' => 26, 'value' => 0],
                    ['time' => 27, 'value' => 0],
                    ['time' => 28, 'value' => 0],
                    ['time' => 29, 'value' => 0],
                    ['time' => 30, 'value' => 0]
                ];
                break;
        }

        return $result;
    }
}