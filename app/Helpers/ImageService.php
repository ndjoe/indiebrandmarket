<?php

namespace OBID\Helpers;


use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageService
{
    public function checkImageSize(UploadedFile $file, $width, $height)
    {
        $im = \Image::make($file);

        if ($im->width() > $width && $im->height() > $height) {
            return true;
        }

        return false;
    }
}