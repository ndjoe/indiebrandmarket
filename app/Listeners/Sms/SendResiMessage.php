<?php

namespace OBID\Listeners\Sms;

use Illuminate\Contracts\Queue\ShouldQueue;
use OBID\Events\Order\OrderShipped;
use OBID\Repositories\SmsRepositories;

class SendResiMessage implements ShouldQueue
{
    public $sms;

    /**
     * SendResiMessage constructor.
     * @param SmsRepositories $smsRepositories
     */
    public function __construct(SmsRepositories $smsRepositories)
    {
        $this->sms = $smsRepositories;
    }

    /**
     * Handle the event.
     *
     * @param  OrderShipped $event
     * @return void
     */
    public function handle(OrderShipped $event)
    {
        $this->sms->sendResiMessage(
            $event->shipping->order->address->nohp,
            $event->shipping->no_resi,
            $event->order->order_code
        );
    }
}
