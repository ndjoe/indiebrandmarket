<?php

namespace OBID\Listeners\Sms;

use OBID\Events\Member\MemberRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use OBID\Repositories\SmsRepositories;

class SendActivationCode implements ShouldQueue
{
    public $smsRepo;

    /**
     * @param SmsRepositories $smsRepositories
     */
    public function __construct(SmsRepositories $smsRepositories)
    {
        $this->smsRepo = $smsRepositories;
    }

    /**
     * Handle the event.
     *
     * @param  MemberRegistered $event
     * @return void
     */
    public function handle(MemberRegistered $event)
    {
        $this->smsRepo->sendActivationMessage(
            $event->user->nohp,
            $event->user->nama,
            $event->user->activation_code
        );
    }
}
