<?php

namespace OBID\Listeners\Sms;

use OBID\Events\Member\MemberActivated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use OBID\Repositories\MailRepository;
use OBID\Repositories\SmsRepositories;

class SendWelcomeMessage implements ShouldQueue
{

    public $sms;

    /**
     * @param SmsRepositories $smsRepositories
     */
    public function __construct(SmsRepositories $smsRepositories)
    {
        $this->sms = $smsRepositories;
    }

    /**
     * Handle the event.
     *
     * @param  MemberActivated $event
     * @return void
     */
    public function handle(MemberActivated $event)
    {
        $this->sms->sendWelcomeMessage($event->user->nohp, $event->user->nama);
    }
}
