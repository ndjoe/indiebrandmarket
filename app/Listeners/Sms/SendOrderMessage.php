<?php

namespace OBID\Listeners\Sms;

use Illuminate\Contracts\Queue\ShouldQueue;
use OBID\Events\Order\OrderCreated;
use OBID\Repositories\SmsRepositories;

class SendOrderMessage implements ShouldQueue
{
    public $sms;

    /**
     * SendOrderMessage constructor.
     * @param SmsRepositories $sms
     */
    public function __construct(SmsRepositories $sms)
    {
        $this->sms = $sms;
    }

    /**
     * Handle the event.
     *
     * @param  OrderCreated $event
     * @return void
     */
    public function handle(OrderCreated $event)
    {
        if ($event->order->payment_method === 'tbca') {
            $pilihanbank = 'BCA';
            $norek = '131-00-11262666';
            $this->sms->sendOrderMessage(
                $event->order->address->nohp,
                $event->order->order_code,
                $event->order->grossTotal,
                $pilihanbank,
                $norek
            );
        } elseif ($event->order->payment_method === 'tmandiri') {
            $pilihanbank = 'Mandiri';
            $norek = '131-00-11262666';
            $this->sms->sendOrderMessage(
                $event->order->address->nohp,
                $event->order->order_code,
                $event->order->grossTotal,
                $pilihanbank,
                $norek
            );
        }

    }
}
