<?php

namespace OBID\Listeners\Log;

use OBID\Events\Product\ProductCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TellAdmin implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductCreated $event
     * @return void
     */
    public function handle(ProductCreated $event)
    {
        \Notifynder::category($event->category)
            ->from($event->fromId)
            ->to(1)
            ->extra(['name' => $event->product->name])
            ->url('/products')
            ->send();
    }
}
