<?php

namespace OBID\Listeners\Notif;

use OBID\Events\Order\OrderConfirmed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use OBID\Repositories\SalesNotificationRepository;

class SendNotifMerchant
{
    protected $salesNotifRepo;

    /**
     * SendNotifMerchant constructor.
     * @param SalesNotificationRepository $salesNotificationRepository
     */
    public function __construct(SalesNotificationRepository $salesNotificationRepository)
    {
        $this->salesNotifRepo = $salesNotificationRepository;
    }

    /**
     * Handle the event.
     *
     * @param  OrderConfirmed $event
     * @return void
     */
    public function handle(OrderConfirmed $event)
    {
        $this->salesNotifRepo->addNotif($event->merchantId);
    }
}
