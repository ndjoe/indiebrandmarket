<?php

namespace OBID\Listeners\Email;

use OBID\Events\Member\MemberRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use OBID\Repositories\MailRepository;

class SendActivationEmail implements ShouldQueue
{
    public $mailer;

    /**
     * @param MailRepository $mailRepository
     */
    public function __construct(MailRepository $mailRepository)
    {
        $this->mailer = $mailRepository;
    }

    /**
     * Handle the event.
     *
     * @param  MemberRegistered $event
     * @return void
     */
    public function handle(MemberRegistered $event)
    {
        $this->mailer->sendActivationEmail($event->user);
    }
}
