<?php

namespace OBID\Listeners\Email;

use OBID\Events\Member\ResetPassword;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use OBID\Repositories\MailRepository;

class SendForgotPasswordEmail implements ShouldQueue
{
    /**
     * @var
     */
    public $mailer;

    /**
     * @param MailRepository $mailRepository
     */
    public function __construct(MailRepository $mailRepository)
    {
        $this->mailer = $mailRepository;
    }

    /**
     * Handle the event.
     *
     * @param  ResetPassword $event
     * @return void
     */
    public function handle(ResetPassword $event)
    {
        $this->mailer->sendResetPasswordEmail($event->user);
    }
}
