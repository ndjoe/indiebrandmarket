<?php

namespace OBID\Listeners;

use Fenos\Notifynder\Facades\Notifynder;
use OBID\Events\NewPosPurchase;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TellMerchant implements ShouldQueue
{
    /**
     * @var Notifynder
     */
    public $notify;

    /**
     * @param Notifynder $notify
     */
    public function __construct(Notifynder $notify)
    {
        $this->notify = $notify;
    }

    /**
     * Handle the event.
     *
     * @param  NewPosPurchase $event
     * @return void
     */
    public function handle(NewPosPurchase $event)
    {
        \Notifynder::category('pos.neworder')
            ->from($event->cashierId)
            ->to($event->merchantId)
            ->url(url('/merchant/orders'))
            ->extra(['orderid' => $event->order->id])
            ->send();
    }
}
