<?php

namespace OBID\Console\Commands;


use Cache;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class CacheRajaOngkir extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cacherajaongkir';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cache Raja Ongkir Api';

    protected static $apiKey = 'a6d0aa618112982d4f8a4d33dc418caf';
    protected static $baseUrl = 'http://api.rajaongkir.com/basic';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client(['headers' => ['key' => static::$apiKey]]);
        $url = static::$baseUrl . '/' . 'province';
        $cityUrl = static::$baseUrl . '/' . 'city';

        $provinces = Cache::remember(
            'provinces',
            Carbon::now()->diffInMinutes(Carbon::now()->addDay(1)),
            function () use ($client, $url) {
                $response = $client->get($url);
                $result = json_decode($response->getBody());

                $provinces = [];
                foreach ($result->rajaongkir->results as $r) {
                    array_push($provinces, [
                        'value' => [
                            'id' => $r->province_id,
                            'text' => $r->province
                        ],
                        'text' => $r->province
                    ]);
                }

                return $provinces;
            });

        foreach ($provinces as $p) {
            Cache::remember(
                'city:' . $p['value']['id'],
                Carbon::now()->diffInMinutes(Carbon::now()->addDay(1)),
                function () use ($client, $cityUrl, $p) {
                    $response = $client->request('GET', $cityUrl, ['query' => ['province' => $p['value']['id']]]);
                    $result = json_decode($response->getBody());

                    $cities = [];
                    foreach ($result->rajaongkir->results as $r) {
                        array_push($cities, [
                            'value' => [
                                'id' => $r->city_id,
                                'text' => $r->type . ' ' . $r->city_name
                            ],
                            'text' => $r->type . ' ' . $r->city_name
                        ]);
                    }

                    return $cities;
                });
        }
    }
}