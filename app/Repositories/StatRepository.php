<?php


namespace OBID\Repositories;


use Carbon\Carbon;
use DB;
use Illuminate\Support\Str;
use OBID\Models\Coupon;
use OBID\Models\Order;
use OBID\Models\Product;
use OBID\Models\UserProfile;

class StatRepository
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var Coupon
     */
    protected $coupon;

    /**
     * @var UserProfile
     */
    protected $userProfile;

    protected $order;

    /**
     * StatRepository constructor.
     * @param Product $product
     * @param Coupon $coupon
     * @param UserProfile $userProfile
     * @param Order $order
     */
    public function __construct(
        Product $product,
        Coupon $coupon,
        UserProfile $userProfile,
        Order $order
    )
    {
        $this->product = $product;
        $this->coupon = $coupon;
        $this->userProfile = $userProfile;
        $this->order = $order;
    }

    /**
     * @return array
     */
    public
    function getTotalUserGenderChart()
    {
        $male = $this->userProfile
            ->whereGender('male')
            ->count('id');
        $female = $this->userProfile
            ->whereGender('female')
            ->count('id');
        $unknown = $this->userProfile
            ->whereNull('gender')
            ->count('id');

        $chart = $this->formatChartDonut(compact('male', 'female', 'unknown'));

        return $chart;
    }

    /**
     * @return array
     */
    public function getNewUserGenderChart()
    {
        $male = $this->userProfile
            ->whereGender('male')
            ->where(DB::raw('extract(month from created_at)'), '=', Carbon::now()->month)
            ->count('id');
        $female = $this->userProfile
            ->whereGender('female')
            ->where(DB::raw('extract(month from created_at)'), '=', Carbon::now()->month)
            ->count('id');
        $unknown = $this->userProfile
            ->whereNull('gender')
            ->where(DB::raw('extract(month from created_at)'), '=', Carbon::now()->month)
            ->count('id');

        $chart = $this->formatChartDonut(compact('male', 'female', 'unknown'));

        return $chart;
    }

    public function getTotalAgeChart()
    {
        $under18 = $this->userProfile
            ->where(DB::raw("date_part('year', age(birthday))"), '<=', DB::raw('18'))
            ->count();
        $over18 = $this->userProfile
            ->where(DB::raw("date_part('year', age(birthday))"), '>', DB::raw('18'))
            ->where(DB::raw("date_part('year', age(birthday))"), '<=', DB::raw('25'))
            ->count();
        $over25 = $this->userProfile
            ->where(DB::raw("date_part('year', age(birthday))"), '>', DB::raw('25'))
            ->where(DB::raw("date_part('year', age(birthday))"), '<=', DB::raw('40'))
            ->count();
        $over40 = $this->userProfile
            ->where(DB::raw("date_part('year', age(birthday))"), '>', DB::raw('40'))
            ->count();

        $data = [
            ['label' => '< 18', 'value' => $under18],
            ['label' => '19 - 25', 'value' => $over18],
            ['label' => '26 - 40', 'value' => $over25],
            ['label' => '> 40', 'value' => $over40]
        ];

        return $data;
    }

    public
    function getNewAgeChart()
    {
        $under18 = $this->userProfile
            ->where(DB::raw("date_part('year', age(birthday))"), '<=', DB::raw('18'))
            ->where(DB::raw('extract(month from created_at)'), '=', Carbon::now()->month)
            ->count();
        $over18 = $this->userProfile
            ->where(DB::raw("date_part('year', age(birthday))"), '>', DB::raw('18'))
            ->where(DB::raw("date_part('year', age(birthday))"), '<=', DB::raw('25'))
            ->where(DB::raw('extract(month from created_at)'), '=', Carbon::now()->month)
            ->count();
        $over25 = $this->userProfile
            ->where(DB::raw("date_part('year', age(birthday))"), '>', DB::raw('25'))
            ->where(DB::raw("date_part('year', age(birthday))"), '<=', DB::raw('40'))
            ->where(DB::raw('extract(month from created_at)'), '=', Carbon::now()->month)
            ->count();
        $over40 = $this->userProfile
            ->where(DB::raw("date_part('year', age(birthday))"), '>', DB::raw('40'))
            ->where(DB::raw('extract(month from created_at)'), '=', Carbon::now()->month)
            ->count();

        $data = [
            ['label' => '< 18', 'value' => $under18],
            ['label' => '19 - 25', 'value' => $over18],
            ['label' => '26 - 40', 'value' => $over25],
            ['label' => '> 40', 'value' => $over40]
        ];

        return $data;
    }

    /**
     * @param string $time
     * @param string $type
     * @param int $user
     * @param Carbon $date
     * @return array|static[]
     */
    public function getSalesSum($time = 'month', $type = 'all', $user = 0, Carbon $date)
    {
        $query = DB::table('order_items')
            ->join('product_items', 'product_items.id', '=', 'order_items.product_item_id')
            ->join('products', 'products.id', '=', 'product_items.product_id')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->whereNotNull('orders.confirmed_at')
            ->whereNotNull('order_items.accepted_at');

        if ($user > 0) {
            $query = $query->where('products.author_id', '=', $user);
        }

        if ($type !== 'all') {
            $query = $query
                ->where('orders.type', $type);
        }

        switch ($time) {
            case 'day':
                $query = $query->where(DB::raw('extract(day from order_items.created_at)'), '=', $date->day)
                    ->select(DB::raw("SUM(order_items.adjusment) as value"))
                    ->addSelect(DB::raw("date_trunc('hour', order_items.created_at) as time"));
                break;
            case 'month':
                $query = $query->where(DB::raw('extract(month from order_items.created_at)'), '=', $date->month)
                    ->select(DB::raw("SUM(order_items.adjusment) as value"))
                    ->addSelect(DB::raw("order_items.created_at as time"));
                break;
            case 'year':
                $query = $query->where(DB::raw('extract(year from order_items.created_at)'), '=', $date->year)
                    ->select(DB::raw("SUM(order_items.adjusment) as value"))
                    ->addSelect(DB::raw("date_trunc('month', order_items.created_at) as time"));
                break;
        }

        $result = $query->groupBy("time")->orderBy(DB::raw("time"))->get();

        return $result;
    }

    /**
     * @param int $brandId
     * @param string $chart
     * @return array
     */
    public function getProductsByCategoryCount($brandId = 0, $chart = 'donut')
    {
        $topCount = $this->product
            ->whereHas('category', function ($q) {
                $q->where('text', 'top');
            });
        if ($brandId > 0) {
            $topCount = $topCount->where('author_id', $brandId)->count();
        } else {
            $topCount = $topCount->count();
        }
        $bottomCount = $this->product
            ->whereHas('category', function ($q) {
                $q->where('text', 'bottom');
            });
        if ($brandId > 0) {
            $bottomCount = $bottomCount->where('author_id', $brandId)->count();
        } else {
            $bottomCount = $bottomCount->count();
        }
        $hatCount = $this->product
            ->whereHas('category', function ($q) {
                $q->where('text', 'hat');
            });
        if ($brandId > 0) {
            $hatCount = $hatCount->where('author_id', $brandId)->count();
        } else {
            $hatCount = $hatCount->count();
        }
        $outerwearCount = $this->product
            ->whereHas('category', function ($q) {
                $q->where('text', 'outerwear');
            });
        if ($brandId > 0) {
            $outerwearCount = $outerwearCount->where('author_id', $brandId)->count();
        } else {
            $outerwearCount = $outerwearCount->count();
        }
        $shoesCount = $this->product
            ->whereHas('category', function ($q) {
                $q->where('text', 'shoes');
            });
        if ($brandId > 0) {
            $shoesCount = $shoesCount->where('author_id', $brandId)->count();
        } else {
            $shoesCount = $shoesCount->count();
        }
        $bagCount = $this->product
            ->whereHas('category', function ($q) {
                $q->where('text', 'bag');
            });
        if ($brandId > 0) {
            $bagCount = $bagCount->where('author_id', $brandId)->count();
        } else {
            $bagCount = $bagCount->count();
        }
        $accesoriesCount = $this->product
            ->whereHas('category', function ($q) {
                $q->where('text', 'accesories');
            });
        if ($brandId > 0) {
            $accesoriesCount = $accesoriesCount->where('author_id', $brandId)->count();
        } else {
            $accesoriesCount = $accesoriesCount->count();
        }
        $otherCount = $this->product
            ->whereHas('category', function ($q) {
                $q->where('text', 'other');
            });
        if ($brandId > 0) {
            $otherCount = $otherCount->where('author_id', $brandId)->count();
        } else {
            $otherCount = $otherCount->count();
        }

        $result = [];
        $data = [
            'top' => $topCount,
            'bottom' => $bottomCount,
            'bag' => $bagCount,
            'hat' => $hatCount,
            'outerwear' => $outerwearCount,
            'shoes' => $shoesCount,
            'accesories' => $accesoriesCount,
            'other' => $otherCount
        ];

        return $result;
    }

    /**
     * @param string $time
     * @param int $user
     * @param Carbon $date
     * @return array|static[]
     */
    public function getOrderCount($time = 'day', $user = 0, Carbon $date)
    {
        $query = DB::table('order_items')
            ->join('product_items', 'product_items.id', '=', 'order_items.product_item_id')
            ->join('products', 'products.id', '=', 'product_items.product_id')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->where('orders.type', 'online');

        if ($user > 0) {
            $query = $query->where('products.author_id', '=', $user);
        }

        switch ($time) {
            case 'day':
                $query = $query->where(DB::raw('extract(day from order_items.created_at)'), '=', $date->day)
                    ->select(DB::raw("COUNT(orders.id) as value"))
                    ->addSelect(DB::raw("date_trunc('hour', order_items.created_at) as time"));
                break;
            case 'month':
                $query = $query->where(DB::raw('extract(month from order_items.created_at)'), '=', $date->month)
                    ->select(DB::raw("COUNT(orders.id) as value"))
                    ->addSelect(DB::raw("order_items.created_at as time"));
                break;
            case 'year':
                $query = $query->where(DB::raw('extract(year from order_items.created_at)'), '=', $date->year)
                    ->select(DB::raw("COUNT(orders.id) as value"))
                    ->addSelect(DB::raw("date_trunc('month', order_items.created_at) as time"));
                break;
        }

        $result = $query->groupBy("time")->orderBy(DB::raw("time"))->get();

        return $result;
    }

    /**
     * @param int $userId
     * @param Carbon $date
     * @return mixed
     */
    public function getUsedCouponSum($userId = 0, Carbon $date)
    {
        $query = $this->coupon;

        if ($userId > 0) {
            $query = $query
                ->whereHas('orders', function ($q) {
                    $q->whereNotNull('confirmed_at');
                })
                ->where('brand_id', $userId)
                ->where('extract(month from created_at)', '=', $date->month);
        } else {
            $query = $query
                ->whereHas('orders', function ($q) {
                    $q->whereNotNull('confirmed_at');
                })
                ->where('extract(month from created_at)', '=', $date->month);
        }

        $result = $query->sum('value');

        return $result;
    }

    public function getTopSalesProducts($userId = 0)
    {
        $query = DB::table('products')
            ->join('product_items', 'product_items.product_id', '=', 'products.id')
            ->join('order_items', 'order_items.product_item_id', '=', 'product_items.id')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->whereNotNull('orders.confirmed_at')
            ->where('orders.type', 'online');

        if ($userId > 0) {
            $query = $query->where('products.author_id', '=', $userId);
        }

        $query = $query->select(DB::raw("SUM(order_items.qty) as value"))
            ->addSelect(DB::raw("products.name as name"));

        $result = $query->groupBy(DB::raw("name"))->orderBy(DB::raw("value"))->take(5)->get();

        return $result;
    }

    public function getTopMembers($limit = 5)
    {
        $query = DB::table('users')
            ->join('orders', 'users.id', '=', 'orders.user_id')
            ->whereNotNull('orders.confirmed_at')
            ->where('orders.type', 'online')
            ->select(DB::raw('SUM("grossTotal") as value'))
            ->addSelect(DB::raw("users.username as username"));

        $result = $query->groupBy(DB::raw("username"))->orderBy(DB::raw("value"))->take($limit)->get();

        return $result;
    }

    public function getTopMerchant($limit = 5)
    {
        $query = DB::table('products')
            ->join('product_items', 'product_items.product_id', '=', 'products.id')
            ->join('order_items', 'order_items.product_item_id', '=', 'product_items.id')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->join('users', 'users.id', '=', 'products.author_id')
            ->whereNotNull('orders.confirmed_at')
            ->whereNotNull('order_items.accepted_at')
            ->where('orders.type', 'online');

        $query = $query->select(DB::raw("SUM(order_items.adjusment) as value"))
            ->addSelect(DB::raw("users.username as username"));

        $result = $query->groupBy(DB::raw("username"))->orderBy(DB::raw("value"))->take(5)->get();

        return $result;
    }

    /**
     * @param $author
     * @param Carbon $date
     * @return mixed
     */
    public function getMerchantConfirmedOrders($author, Carbon $date)
    {
        $query = $this->order
            ->whereHas('items.productItem.product', function ($q) use ($author) {
                $q->where('author_id', $author);
            })
            ->whereHas('items', function ($q) {
                $q->whereNotNull('accepted_at');
            })
            ->whereNotNull('confirmed_at')
            ->where(DB::raw('extract(month from created_at)'), '=', $date->month);

        $result = $query->get();

        return $result;
    }

    /**
     * @param array $input
     * @return array
     */
    private function formatChartDonut($input = [])
    {
        $result = [];
        foreach ($input as $key => $value) {
            array_push($result, [
                'label' => Str::upper($key),
                'value' => $value
            ]);
        }

        return $result;
    }

    /**
     * @param $input
     * @param $type
     * @return array
     */
    private function formatSalesChartLine($input, $type)
    {
        $arrayResult = $this->generateSalesEmptyChartData($type);

        foreach ($input as $r) {
            if ($type === 'month') {
                foreach ($arrayResult as $key => $value) {
                    if ($value['time'] === Carbon::parse($r->time)->weekOfMonth) {
                        $arrayResult[$key]['value'] = $r->sum;
                        $arrayResult[$key]['final'] = $r->final;
                        $arrayResult[$key]['adjusment'] = $r->adjusment;
                    }
                    $arrayResult[$key]['time'] = 'Minggu ' . $value['time'];
                }
            } elseif ($type === 'day') {
                foreach ($arrayResult as $key => $value) {
                    if ($value['time'] === Carbon::parse($r->time)->hour) {
                        $arrayResult[$key]['value'] = $r->sum;
                        $arrayResult[$key]['final'] = $r->final;
                        $arrayResult[$key]['adjusment'] = $r->adjusment;
                    }
                }
            } elseif ($type === 'year') {
                foreach ($arrayResult as $key => $value) {
                    if ($value['time'] === Carbon::parse($r->time)->month) {
                        $arrayResult[$key]['value'] = $r->sum;
                        $arrayResult[$key]['final'] = $r->final;
                        $arrayResult[$key]['adjusment'] = $r->adjusment;
                    }
                    $arrayResult[$key]['time'] = 'Bulan ' . $value['time'];
                }
            }
        }

        return $arrayResult;
    }

    /**
     * @param $type
     * @return array
     */
    private function generateSalesEmptyChartData($type)
    {
        switch ($type) {
            case 'day':
                $result = [
                    ['time' => 1, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 2, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 3, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 4, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 5, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 6, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 7, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 8, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 9, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 10, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 11, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 12, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 13, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 14, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 15, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 16, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 17, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 18, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 19, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 20, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 21, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 22, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 23, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                ];
                break;
            case 'month':
                $result = [
                    ['time' => 1, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 2, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 3, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 4, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                ];
                break;
            case 'year':
                $result = [
                    ['time' => 1, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 2, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 3, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 4, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 5, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 6, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 7, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 8, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 9, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 10, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 11, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 12, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 13, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 14, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 15, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 16, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 17, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 18, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 19, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 20, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 21, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 22, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 23, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 24, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 25, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 26, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 27, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 28, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 29, 'value' => 0, 'adjusment' => 0, 'final' => 0],
                    ['time' => 30, 'value' => 0, 'adjusment' => 0, 'final' => 0]
                ];
                break;
        }

        return $result;
    }

    public function confirmedOrderCount()
    {
        $query = DB::table('order_items')
            ->join('product_items', 'product_items.id', '=', 'order_items.product_item_id')
            ->join('products', 'products.id', '=', 'product_items.product_id')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->whereNotNull('confirmed_at')
            ->where('orders.type', 'online');

        return $query->count();
    }

    public function unConfirmedOrderCount()
    {
        $query = DB::table('order_items')
            ->join('product_items', 'product_items.id', '=', 'order_items.product_item_id')
            ->join('products', 'products.id', '=', 'product_items.product_id')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->whereNull('confirmed_at')
            ->where('orders.type', 'online');

        return $query->count();
    }

    public function productCountByCategory()
    {
        $query = DB::table('products')
            ->join('categories', 'products.category_id', '=', 'categories.id');

        $query = $query->select(DB::raw("COUNT(products.id) as value"))
            ->addSelect(DB::raw("categories.text as label"));

        $result = $query->groupBy(DB::raw("label"))->get();

        return $result;
    }

    public function productCount($parameter = null)
    {
        $query = $this->product;

        if ($parameter) {
            $query = $query->whereNotNull($parameter);
        }

        return $query->count();
    }

    public function getOrderCountReadyToShip()
    {
        $query = DB::table('order_items')
            ->join('product_items', 'product_items.id', '=', 'order_items.product_item_id')
            ->join('products', 'products.id', '=', 'product_items.product_id')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->whereNotNull('orders.confirmed_at')
            ->whereNotNull('order_items.accepted_at');

        $result = $query->count(DB::raw("orders.id"));

        return $result;
    }

    public function getShippingsCount()
    {
        $query = DB::table('shippings')
            ->count();

        return $query;
    }

    /**
     * @param string $time
     * @param Carbon $date
     * @return array|static[]
     */
    public function getMerchantsPerformanceReport($time = 'month', Carbon $date)
    {
        $query = DB::table('products')
            ->join('product_items', 'product_items.product_id', '=', 'products.id')
            ->join('order_items', 'order_items.product_item_id', '=', 'product_items.id')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->join('users', 'users.id', '=', 'products.author_id')
            ->whereNotNull('orders.confirmed_at')
            ->whereNotNull('order_items.accepted_at')
            ->where('orders.type', 'online');

        switch ($time) {
            case 'day':
                $query = $query->where(DB::raw('extract(day from orders.created_at)'), '=', $date->day)
                    ->select(DB::raw("SUM(order_items.adjusment) as value"))
                    ->addSelect(DB::raw("SUM(order_items.adjusment)*10/100 as profit"))
                    ->addSelect(DB::raw("users.username as username"))
                    ->addSelect(DB::raw("users.id as userid"));
                break;
            case 'month':
                $query = $query->where(DB::raw('extract(month from orders.created_at)'), '=', $date->month)
                    ->select(DB::raw("SUM(order_items.adjusment) as value"))
                    ->addSelect(DB::raw("SUM(order_items.adjusment)*10/100 as profit"))
                    ->addSelect(DB::raw("users.username as username"))
                    ->addSelect(DB::raw("users.id as userid"));
                break;
            case 'year':
                $query = $query->where(DB::raw('extract(year from orders.created_at)'), '=', $date->year)
                    ->select(DB::raw("SUM(order_items.adjusment) as value"))
                    ->addSelect(DB::raw("SUM(order_items.adjusment)*10/100 as profit"))
                    ->addSelect(DB::raw("users.username as username"))
                    ->addSelect(DB::raw("users.id as userid"));
                break;
        }

        $result = $query->groupBy(DB::raw("username, userid"))->orderBy(DB::raw("value"))->get();

        return $result;
    }

    /**
     * @param string $time
     * @param Carbon $date
     * @return mixed
     */
    public function getShippedOrders($time = 'month', Carbon $date)
    {
        $query = DB::table('shippings')
            ->join('orders', 'orders.id', '=', 'shippings.order_id');

        switch ($time) {
            case 'day':
                $query = $query->where(DB::raw('extract(day from shippings.created_at)'), '=', $date->day)
                    ->select('orders.order_code as order_code')
                    ->addSelect('shippings.created_at as shipping_date');
                break;
            case 'month':
                $query = $query->where(DB::raw('extract(month from shippings.created_at)'), '=', $date->month)
                    ->select('orders.order_code as order_code')
                    ->addSelect('shippings.created_at as shipping_date');
                break;
            case 'year':
                $query = $query->where(DB::raw('extract(year from shippings.created_at)'), '=', $date->year)
                    ->select('orders.order_code as order_code')
                    ->addSelect('shippings.created_at as shipping_date');
                break;
        }

        $result = $query->groupBy('shipping_date', 'order_code')->orderBy('shipping_date')->get();

        return $result;
    }

    /**
     * @param string $time
     * @param Carbon $date
     * @return array|static[]
     */
    public function getListMerchantConfirmedOrderCount($time = 'month', Carbon $date)
    {
        $query = DB::table('orders')
            ->join('order_items', 'orders.id', '=', 'order_items.order_id')
            ->join('product_items', 'product_items.id', '=', 'order_items.product_item_id')
            ->join('products', 'products.id', '=', 'product_items.product_id')
            ->join('users', 'users.id', '=', 'products.author_id')
            ->whereNotNull('orders.confirmed_at');

        switch ($time) {
            case 'day':
                $query = $query->where(DB::raw('extract(day from orders.created_at)'), '=', $date->day)
                    ->select(DB::raw('COUNT(orders.id) as count'))
                    ->addSelect('users.username as username');
                break;
            case 'month':
                $query = $query->where(DB::raw('extract(month from orders.created_at)'), '=', $date->month)
                    ->select(DB::raw('COUNT(orders.id) as count'))
                    ->addSelect('users.username as username');
                break;
            case 'year':
                $query = $query->where(DB::raw('extract(year from orders.created_at)'), '=', $date->year)
                    ->select(DB::raw('COUNT(orders.id) as count'))
                    ->addSelect('users.username as username');
                break;
        }

        $result = $query->groupBy(DB::raw('username'))->orderBy(DB::raw('count'))->get();

        return $result;
    }

    public function getMerchantProductCount($brand)
    {
        $count = $this->product->whereAuthorId($brand)->count();

        return $count;
    }


    public function getMerchantPublishedProductCount($brand)
    {
        $count = $this->product->whereAuthorId($brand)->whereNotNull('published_at')->count();


        return $count;
    }

    public function getMerchantProductCountByCategory($brand)
    {
        $query = DB::table('products')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->where('products.author_id', '=', $brand);

        $query = $query->select(DB::raw("COUNT(products.id) as value"))
            ->addSelect(DB::raw("categories.text as label"));

        $result = $query->groupBy(DB::raw("label"))->get();

        return $result;
    }
}