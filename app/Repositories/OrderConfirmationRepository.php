<?php


namespace OBID\Repositories;


use OBID\Models\OrderConfirmation;

class OrderConfirmationRepository
{
    /**
     * @var OrderConfirmation
     */
    protected $orderConfirmation;

    /**
     * @param OrderConfirmation $orderConfirmation
     */
    public function __construct(OrderConfirmation $orderConfirmation)
    {
        $this->orderConfirmation = $orderConfirmation;
    }

    /**
     * @param $input
     * @return bool|OrderConfirmation
     */
    public function create($input)
    {
        $orderConfirmation = $this->stub($input, new OrderConfirmation);

        if ($orderConfirmation->save()) {
            return $orderConfirmation;
        }

        return false;
    }

    /**
     * @param $input
     * @param OrderConfirmation $orderConfirmation
     * @return OrderConfirmation
     */
    private function stub($input, OrderConfirmation $orderConfirmation)
    {
        $orderConfirmation->amount = $input['amount'];
        $orderConfirmation->atas_nama = $input['atas_nama'];
        $orderConfirmation->bank = $input['bank'];
        $orderConfirmation->date = $input['date'];
        $orderConfirmation->order_id = $input['order_id'];

        return $orderConfirmation;
    }

}