<?php


namespace OBID\Repositories;


use OBID\Models\OrderPayment;

class PaymentRepository
{
    /**
     * @var OrderPayment
     */
    protected $orderPayment;

    /**
     * @param OrderPayment $orderPayment
     */
    public function __construct(OrderPayment $orderPayment)
    {
        $this->orderPayment = $orderPayment;
    }

    /**
     * @param $input
     * @param OrderPayment $orderPayment
     * @return OrderPayment
     */
    public function paymentStub($input, OrderPayment $orderPayment)
    {
        $orderPayment->type = $input['type'];
        $orderPayment->order_id = $input['orderId'];

        if ($input['type'] === 'manualtrans') {
            $orderPayment->bank = $input['bank'];
        } elseif ($input['type'] === 'edc') {
            $orderPayment->bank = $input['bank'];
            $orderPayment->transaction_number = $input['noref'];
            $orderPayment->account_number = $input['card_number'];
            $orderPayment->expiration_date = $input['expiration_date'];
        }

        return $orderPayment;
    }

    /**
     * @param $input
     * @return bool|OrderPayment
     */
    public function create($input)
    {
        $orderPayment = $this->paymentStub($input, new OrderPayment);

        if ($orderPayment->save()) {
            return $orderPayment;
        }

        return false;
    }

}