<?php


namespace OBID\Repositories;


use Illuminate\Support\Str;
use OBID\Models\PasswordToken;
use OBID\Models\User;

class PasswordTokenRepository
{
    /**
     * @var PasswordToken
     */
    protected $passwordToken;

    /**
     * @param PasswordToken $passwordToken
     */
    public function __construct(PasswordToken $passwordToken)
    {
        $this->passwordToken = $passwordToken;
    }

    /**
     * @param $token
     * @return User|bool
     */
    public function findUserByToken($token)
    {
        $token = $this->passwordToken->whereToken($token)->first();

        if (!$token) {
            return false;
        }

        return $token->user;
    }

    public function createToken($userId)
    {
        $token = new PasswordToken;

        $token->user_id = $userId;
        $token->token = Str::lower(str_random(4));

        $token->save();
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function deleteUserToken($userId)
    {
        $token = $this->passwordToken->where('user_id', $userId)->first();

        $deleted = $token->delete();

        return $deleted;
    }
}