<?php


namespace OBID\Repositories;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use OBID\Models\OrderItem;

class OrderItemRepository
{
    /**
     * @var OrderItem
     */
    protected $orderItem;

    /**
     * @param OrderItem $orderItem
     */
    public function __construct(OrderItem $orderItem)
    {
        $this->orderItem = $orderItem;
    }

    /**
     * @param $input
     * @return bool|OrderItem
     */
    public function create($input)
    {
        $orderItem = $this->orderItemStub($input, new OrderItem);

        if ($orderItem->save()) {
            return $orderItem;
        }

        return false;
    }

    /**
     * @param $input
     * @param OrderItem $orderItem
     * @return OrderItem
     */
    protected function orderItemStub($input, OrderItem $orderItem)
    {
        $orderItem->order_id = $input['orderId'];
        $orderItem->product_item_id = $input['productItemId'];
        $orderItem->qty = $input['qty'];
        if (array_key_exists('accepted_at', $input)) {
            $orderItem->accepted_at = $input['accepted_at'];
        }
        if (array_key_exists('adjusment', $input)) {
            $orderItem->adjusment = $input['adjusment'];
        }

        return $orderItem;
    }

    /**
     * @param array $ids
     * @return Collection
     */
    public function getMultipleByIds($ids = [])
    {
        $items = $this->orderItem->with('productItem.product')->whereIn('id', $ids)->get();

        return $items;
    }

    public function getOnlineSoldItems($merchantId = 0, Carbon $date)
    {
        if ($merchantId > 0) {
            $items = $this->orderItem
                ->with('order', 'productItem.product', 'productItem.size', 'productItem.product.author')
                ->whereHas('order', function ($q) {
                    $q->where('type', 'online')->whereNotNull('confirmed_at');
                })
                ->whereHas('productItem.product', function ($q) use ($merchantId) {
                    $q->where('author_id', $merchantId);
                })
                ->where(\DB::raw('extract(month from created_at)'), '=', $date->month)
                ->whereNotNull('accepted_at')
                ->get();
        } else {
            $items = $this->orderItem
                ->with('order', 'productItem.product', 'productItem.size', 'productItem.product.author')
                ->whereHas('order', function ($q) {
                    $q->where('type', 'online')->whereNotNull('confirmed_at');
                })
                ->where(\DB::raw('extract(month from created_at)'), '=', $date->month)
                ->whereNotNull('accepted_at')
                ->get();
        }

        return $items;
    }

    public function getItemsPaginated($page, $filter, $sort, $orderBy)
    {
        $query = $this->orderItem->with('order', 'productItem.size', 'productItem.product', 'productItem.product.author')->whereHas('order', function ($q) {
            $q->whereNotNull('confirmed_at');
        })->where(\DB::raw('extract(month from created_at)'), '=', date('n'));

        if ($filter > 0) {
            $query = $query->whereHas('productItem.product', function ($q) use ($filter) {
                $q->where('author_id', $filter);
            });
        }

        $items = $query->orderBy($orderBy, $sort)->paginate(20);

        return $items;

    }

    public function getMerchantOrderItems($merchant, $filter, $sort, $orderBy, $page)
    {
        $query = $this->orderItem
            ->with('order', 'productItem.size', 'productItem.product', 'productItem.product.author', 'productItem.product.discount')
            ->whereHas('order', function ($q) {
                $q->where('type', 'online')->whereNotNull('confirmed_at');
            })
            ->whereHas('productItem.product.author', function ($q) use ($merchant) {
                $q->where('id', $merchant->id);
            });

        if ($filter > 0) {
            if ($filter === 2) {
                $query = $query->whereNotNull('accepted_at');
            } else {
                $query = $query->whereNull('accepted_at');
            }
        }

        $items = $query->orderByRaw('accepted_at NULLS FIRST')->paginate(20);

        return $items;
    }

    public function getUnacceptedOrderItemCount($userId)
    {
        $count = $this->orderItem
            ->whereHas('productItem.product.author', function ($q) use ($userId) {
                $q->where('id', $userId);
            })
            ->whereNull('accepted_at')
            ->whereHas('order', function ($q) {
                $q->whereNotNull('confirmed_at');
            })
            ->count();

        return $count;
    }
}