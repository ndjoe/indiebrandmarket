<?php


namespace OBID\Repositories;


use Illuminate\Contracts\Mail\Mailer;
use OBID\Models\User;

class MailRepository
{
    protected $mailer;

    /**
     * MailRepository constructor.
     * @param $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }


    public function sendActivationEmail(User $user)
    {
        $data = [
            'user' => $user,
            'url' => 'http://originalbrands.localapp/activation?code=' . $user->activation_code
        ];

        $this->mailer->send('email.basic', $data, function ($message) use ($user) {
            $message->to($user->email)->subject('account activation');
        });
    }

    public function sendResetPasswordEmail(User $user)
    {
        $data = [
            'user' => $user,
            'url' => 'http://originalbrands.localapp/confirm-token?token=' . $user->resetToken->token
        ];

        $this->mailer->send('email.resetpassword', $data, function ($message) use ($user) {
            $message->to($user->email)->subject('Password Reset');
        });
    }


}