<?php


namespace OBID\Repositories;


use Carbon\Carbon;
use Illuminate\Support\Str;
use OBID\Models\Coupon;
use OBID\Models\Product;

class CouponRepository
{
    protected $coupon;
    protected $product;

    /**
     * @param Coupon $coupon
     * @param Product $product
     */
    public function __construct(Coupon $coupon, Product $product)
    {
        $this->coupon = $coupon;
        $this->product = $product;
    }

    public function getCouponsPaginated($page)
    {
        $coupons = $this->coupon->with('brand')->orderBy('id', 'desc')->paginate(20);

        return $coupons;
    }

    public function getMerchantCouponsPaginated($page, $brandId)
    {
        $coupons = $this->coupon->with('brand')
            ->where('brand_id', $brandId)->orderBy('id', 'desc')->paginate(20);

        return $coupons;
    }


    /**
     * @param $id
     * @return Coupon
     */
    public function findById($id)
    {
        $coupon = $this->coupon->whereId($id)->first();

        return $coupon;
    }

    /**
     * @param $code
     * @return Coupon
     */
    public function findByCode($code)
    {
        $coupon = $this->coupon->whereCode($code)->first();

        return $coupon;
    }

    /**
     * @param $input
     * @return bool|Coupon
     */
    public function create($input)
    {
        $coupon = $this->couponStub($input, new Coupon);

        if ($coupon->save()) {
            return $coupon;
        }

        return false;
    }

    public function createBulk($input, $qty)
    {
        for ($i = 0; $i < $qty; $i++) {
            $this->create($input);
        }
    }

    /**
     * @param $input
     * @param $couponId
     * @return bool|Coupon
     */
    public function edit($input, $couponId)
    {
        $coupon = $this->coupon->whereId($couponId)->first();

        $coupon = $this->couponStub($input, $coupon);

        if ($coupon->save()) {
            return $coupon;
        }

        return false;
    }

    /**
     * @param int $userId
     * @param Carbon $date
     * @return \OBID\Models\Coupon[]
     */
    public function getUsedCoupons($userId = 0, Carbon $date)
    {
        $coupons = $this->coupon
            ->with('orders')
            ->where('brand_id', $userId)
            ->whereHas('orders', function ($q) use ($date) {
                $q->where(\DB::raw('extract(month from coupon_order.created_at)'), '=', $date->month);
            })
            ->get();

        return $coupons;
    }

    /**
     * @param $orderId
     * @param $couponId
     * @return Coupon
     */
    public function attachCouponToOrder($orderId, $couponId)
    {
        $coupon = $this->findById($couponId);

        $coupon->orders()->attach($orderId);
        $this->decrementQty($coupon->id);

        return $coupon;
    }

    /**
     * @param Coupon $coupon
     * @param $orderTotal
     * @return bool
     */
    public function checkCouponValidity(Coupon $coupon, $orderTotal)
    {
        if (($coupon->expired_at > Carbon::now()) || ($coupon->qty > 0) || ($orderTotal <= $coupon->minimum_order)) {
            return true;
        }

        return false;
    }

    /**
     * @param $couponId
     * @return Coupon
     */
    public function decrementQty($couponId)
    {
        $coupon = $this->coupon->whereId($couponId)->first();

        $coupon->qty -= 1;

        if ($coupon->qty < 0) {
            $coupon->qty = 0;
        }

        $coupon->save();

        return $coupon;
    }

    /**
     * @param $couponId
     */
    public function deleteCoupon($couponId)
    {
        $coupon = $this->coupon->whereId($couponId)->first();

        $coupon->delete();
    }

    /**
     * @param $input
     * @param Coupon $coupon
     * @return Coupon
     */
    public function couponStub($input, Coupon $coupon)
    {
        $coupon->value = $input['value'];
        $coupon->expired_at = $input['expired_at'];
        if (array_key_exists('kode', $input)) {
            $coupon->code = Str::upper($input['kode']);
        } else {
            $coupon->code = Str::upper(str_random(8));
        }
        $coupon->qty = $input['qty'];
        if (array_key_exists('notavailable', $input)) {
            $coupon->notavailable = $input['notavailable'];
        }
        if (array_key_exists('minimum_order', $input)) {
            $coupon->minimum_order = $input['minimum_order'];
        }
        if (array_key_exists('brand_id', $input)) {
            $coupon->brand_id = $input['brand_id'];
        }

        return $coupon;
    }
}