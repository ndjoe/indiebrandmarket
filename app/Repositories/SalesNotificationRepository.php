<?php

namespace OBID\Repositories;


use OBID\Models\SalesNotification;

class SalesNotificationRepository
{
    /**
     * @var SalesNotification
     */
    protected $notif;

    /**
     * SalesNotificationRepository constructor.
     * @param SalesNotification $notif
     */
    public function __construct(SalesNotification $notif)
    {
        $this->notif = $notif;
    }

    /**
     * @param $userId
     * @return int
     */
    public function getNotif($userId)
    {
        return $this->notif->whereUserId($userId)->first()->notif_count;
    }

    /**
     * @param $userId
     */
    public function clearNotif($userId)
    {
        $notif = $this->notif->whereUserId($userId)->first();

        $notif->notif_count = 0;

        $notif->save();
    }

    /**
     * @param $userId
     */
    public function addNotif($userId)
    {
        $this->notif->whereUserId($userId)->increment('notif_count');
    }
}