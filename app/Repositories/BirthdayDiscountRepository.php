<?php
namespace OBID\Repositories;


use OBID\Models\BirthdayDiscount;

class BirthdayDiscountRepository
{
    protected $birthday;

    /**
     * BirthdayDiscountRepository constructor.
     * @param BirthdayDiscount $birthday
     */
    public function __construct(BirthdayDiscount $birthday)
    {
        $this->birthday = $birthday;
    }


    public function create($input)
    {
        $birthday = new BirthdayDiscount;

        $birthday->order_id = $input['order_id'];
        $birthday->user_id = $input['user_id'];

        $birthday->save();
    }
}