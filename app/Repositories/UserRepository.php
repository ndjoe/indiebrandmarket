<?php


namespace OBID\Repositories;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;
use OBID\Models\CashierProfile;
use OBID\Models\MerchantProfile;
use OBID\Models\PosCash;
use OBID\Models\Role;
use OBID\Models\User;
use OBID\Models\UserProfile;

class UserRepository
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @var UserProfile
     */
    protected $userProfile;

    /**
     * @var Role
     */
    protected $role;

    /**
     * @var CashierProfile
     */
    protected $cashierProfile;

    /**
     * @param User $repo
     * @param UserProfile $userProfile
     * @param Role $role
     * @param CashierProfile $cashierProfile
     */
    public function __construct(
        User $repo,
        UserProfile $userProfile,
        Role $role,
        CashierProfile $cashierProfile
    )
    {
        $this->user = $repo;
        $this->userProfile = $userProfile;
        $this->role = $role;
        $this->cashierProfile = $cashierProfile;
    }

    /**
     * @param $id
     * @return User
     */
    public function findById($id)
    {
        $user = $this->user->whereId($id)->first();

        return $user;
    }

    /**
     * @param $id
     * @return bool
     */
    public function activateAccount($id)
    {
        $user = $this->findById($id);

        $user->is_active = true;
        $user->activation_code = null;

        return $user->save();
    }

    /**
     * @param $email
     * @return User
     */
    public function findByEmail($email)
    {
        $user = $this->user->whereEmail($email)->first();

        return $user;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deactivateAccount($id)
    {
        $user = $this->findById($id);

        $user->is_active = false;

        return $user->save();
    }

    /**
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function deleteAccount($id)
    {
        $user = $this->findById($id);

        if ($user->delete()) {
            return true;
        }

        return false;
    }

    public function createNewMember($input)
    {
        $profile = new UserProfile;
        $profile->alamat = $input['alamat'];
        $profile->birthday = Carbon::parse($input['birthday']);
        $profile->kodepos = $input['kodepos'];
        $profile->kota = $input['kota'];
        $profile->provinsi = $input['provinsi'];
        $profile->ongkir_city_id = $input['ongkir_kota_id'];
        $profile->ongkir_province_id = $input['ongkir_provinsi_id'];
        $profile->save();

        $role = $this->role->whereName('customer')->first();

        $user = new User;
        $user->username = $input['username'];
        $user->nama = $input['nama'];
        $user->nohp = $input['nohp'];
        $user->email = $input['email'];
        $user->password = bcrypt($input['password']);
        $user->activation_code = str_pad(mt_rand(0, 99999), 5, '0', STR_PAD_LEFT);
        $profile->user()->save($user);
        $user->attachRole($role);

        return $user;
    }

    /**
     * @param $page
     * @param $kuery
     * @param $filter
     * @param $sort
     * @param $orderBy
     * @return Collection|User[]
     */
    public function getUsersPaginated($page, $kuery, $filter, $sort, $orderBy, $perpage = 20)
    {
        $query = $this->user->with('roles', 'profileable');

        if ($filter !== 'all') {
            if ($filter === 'staff') {
                $query = $query->whereHas('roles', function ($q) {
                    $q->whereIn('name', ['admin', 'purchase', 'inventory', 'finance', 'shipper']);
                });
            } else {
                $query = $query->whereHas('roles', function ($q) use ($filter) {
                    $q->where('name', $filter);
                });
            }
        }

        if (!is_null($kuery)) {
            $query = $query
                ->where(function ($q) use ($kuery) {
                    $q->where('username', 'like', '%' . $kuery . '%')
                        ->orWhere('nama', 'like', '%' . $kuery . '%');
                });
        }

        $users = $query->orderBy($orderBy, $sort)->paginate($perpage);

        return $users;
    }

    public function createNewAdmin($input)
    {
        $role = $this->role->whereName($input['role'])->first();

        $user = new User;
        $user->username = $input['username'];
        $user->nama = $input['nama'];
        $user->nohp = $input['nohp'];
        $user->password = bcrypt($input['password']);
        $user->email = $input['email'];
        $user->is_active = false;
        $user->save();
        $user->attachRole($role);

        return $user;
    }

    public function createNewMerchant($input)
    {
        $profile = new MerchantProfile;
        $profile->owner_name = $input['owner'];
        $profile->alamat = $input['alamat'];
        $profile->bank = $input['bank'];
        $profile->kodepos = $input['kodepos'];
        $profile->alamat = $input['alamat'];
        $profile->norek = $input['norek'];
        $profile->provinsi = $input['provinsi'];
        $profile->bank_owner = $input['ownerRek'];
        if (array_key_exists('bannerImage', $input)) {
            $name = Str::slug($input['nama']) . Str::random(3) . '.' . $input['bannerImage']->getClientOriginalExtension();
            \Image::make($input['bannerImage']->getRealPath())->fit(1000, 300)->save(base_path() . '/public/images/banner/' . $name);

            $profile->banner_image = $name;
        } else {
            $profile->banner_image = 'default-banner.png';
        }

        if (array_key_exists('logoImage', $input)) {
            $name = Str::slug($input['nama']) . Str::random(3) . '.' . $input['logoImage']->getClientOriginalExtension();
            \Image::make($input['logoImage']->getRealPath())->fit(300, 300)->save(base_path() . '/public/images/logo/' . $name);

            $profile->logo = $name;
        } else {
            $profile->logo = 'default-logo.png';
        }

        $profile->about = $input['about'];
        $profile->kota = $input['kota'];

        $profile->save();

        $role = $this->role->whereName('affiliate')->first();

        $user = new User;
        $user->email = $input['email'];
        $user->nama = $input['nama'];
        $user->nohp = $input['nohp'];
        $user->username = Str::lower($input['username']);
        $user->password = bcrypt($input['password']);
        $user->is_active = array_key_exists('is_active', $input) ? true : false;
        $profile->user()->save($user);
        $user->attachRole($role);

        $pos = new PosCash;
        $pos->merchant_id = $user->id;
        $pos->cash = 0;
        $pos->save();

        return $user;
    }

    public function editMerchant($id, $input)
    {
        $user = User::with('profileable')->whereId($id)->first();
        $profile = $user->profileable;
        $user->nama = $input['nama'];
        $user->nohp = $input['nohp'];

        $profile->owner_name = $input['owner'];
        $profile->alamat = $input['alamat'];
        $profile->bank = $input['bank'];
        $profile->kodepos = $input['kodepos'];
        $profile->alamat = $input['alamat'];
        $profile->norek = $input['norek'];
        $profile->provinsi = $input['provinsi'];
        $profile->about = $input['about'];
        $profile->kota = $input['kota'];
        $profile->bank_owner = $input['ownerRek'];

        if (array_key_exists('bannerImage', $input)) {
            $name = Str::slug($user->nama) . Str::random(3) . '.' . $input['bannerImage']->getClientOriginalExtension();
            \Image::make($input['bannerImage']->getRealPath())->fit(1000, 300)->save(base_path() . '/public/images/banner/' . $name);

            $profile->banner_image = $name;
        }

        if (array_key_exists('logoImage', $input)) {
            $name = Str::slug($user->nama) . Str::random(3) . '.' . $input['logoImage']->getClientOriginalExtension();
            \Image::make($input['logoImage']->getRealPath())->fit(300, 300)->save(base_path() . '/public/images/logo/' . $name);

            $profile->logo = $name;
        }

        $profile->save();

        if (array_key_exists('password', $input)) {
            $user->password = bcrypt($input['password']);
        }

        $user->save();

        return true;
    }

    public function editMember($id, $input)
    {
        $user = $this->user->with('profileable')->whereId($id)->first();

        $profile = $user->profileable;
        $user->nama = $input['nama'];
        $user->nohp = $input['nohp'];
        $profile->provinsi = $input['provinsi'];
        $profile->alamat = $input['alamat'];
        $profile->birthday = $input['ttl'];
        $profile->gender = $input['gender'];
        $profile->kota = $input['kota'];
        $profile->kodepos = $input['kodepos'];

        $profile->save();

        if (array_key_exists('password', $input)) {
            $user->password = bcrypt($input['password']);
        }

        $user->save();

        return true;
    }

    public function getStatsFromUserIds($ids)
    {
        $query = $this->userProfile
            ->whereHas('user', function ($q) use ($ids) {
                $q->whereIn('id', array_column($ids->toArray(), 'user_id'));
            });

        $under18 = $this->userProfile
            ->whereHas('user', function ($q) use ($ids) {
                $q->whereIn('id', array_column($ids->toArray(), 'user_id'));
            })
            ->where(\DB::raw("date_part('year', age(birthday))"), '<=', \DB::raw('18'))
            ->count();
        $over18 = $this->userProfile
            ->whereHas('user', function ($q) use ($ids) {
                $q->whereIn('id', array_column($ids->toArray(), 'user_id'));
            })
            ->where(\DB::raw("date_part('year', age(birthday))"), '>', \DB::raw('18'))
            ->where(\DB::raw("date_part('year', age(birthday))"), '<=', \DB::raw('25'))
            ->count();
        $over25 = $this->userProfile
            ->whereHas('user', function ($q) use ($ids) {
                $q->whereIn('id', array_column($ids->toArray(), 'user_id'));
            })
            ->where(\DB::raw("date_part('year', age(birthday))"), '>', \DB::raw('25'))
            ->where(\DB::raw("date_part('year', age(birthday))"), '<=', \DB::raw('40'))
            ->count();
        $over40 = $this->userProfile
            ->whereHas('user', function ($q) use ($ids) {
                $q->whereIn('id', array_column($ids->toArray(), 'user_id'));
            })
            ->where(\DB::raw("date_part('year', age(birthday))"), '>', \DB::raw('40'))
            ->count();
        $age = [
            ['label' => 'dibawah 18 tahun', 'value' => $under18],
            ['label' => '18 - 25 tahun', 'value' => $over18],
            ['label' => '25 - 40 tahun', 'value' => $over25],
            ['label' => 'diatas 40 tahun', 'value' => $over40]
        ];
        $male = $this->userProfile
            ->whereHas('user', function ($q) use ($ids) {
                $q->whereIn('id', array_column($ids->toArray(), 'user_id'));
            })
            ->whereGender('male')
            ->count();
        $female = $this->userProfile
            ->whereHas('user', function ($q) use ($ids) {
                $q->whereIn('id', array_column($ids->toArray(), 'user_id'));
            })
            ->whereGender('female')
            ->count();
        $unknown = $this->userProfile
            ->whereHas('user', function ($q) use ($ids) {
                $q->whereIn('id', array_column($ids->toArray(), 'user_id'));
            })
            ->whereNull('gender')
            ->orWhere('gender', 'unknown')
            ->count();
        $gender = [
            ['label' => 'pria', 'value' => $male],
            ['label' => 'wanita', 'value' => $female],
            ['label' => 'unknown', 'value' => $unknown]
        ];
        return compact('age', 'gender');
    }

    public function getCashiersByMerchant($page, $kuery, $sort, $orderBy, $merchantId)
    {
        $query = $this->cashierProfile
            ->with('user', 'merchant')
            ->whereHas('user.roles', function ($q) {
                $q->where('name', 'cashier');
            });

        if ($merchantId > 0) {
            $query = $query->whereMerchantId($merchantId);
        }

        if (!is_null($kuery)) {
            $query = $query
                ->whereHas('user', function ($q) use ($kuery) {
                    $q->where('username', 'like', '%' . $kuery . '%');
                });
        }

        $cashiers = $query->orderBy($orderBy, $sort)->paginate(20);

        return $cashiers;
    }

    public function getStaffsMerchant($page, $kuery, $sort, $orderBy, $merchantId)
    {
        $query = $this->cashierProfile
            ->with('user', 'merchant')
            ->whereHas('user.roles', function ($q) {
                $q->where('roles.name', '=', 'staffadmin');
            });

        if ($merchantId > 0) {
            $query = $query->whereMerchantId($merchantId);
        }

        if (!is_null($kuery)) {
            $query = $query
                ->whereHas('user', function ($q) use ($kuery) {
                    $q->where('username', 'like', '%' . $kuery . '%');
                });
        }

        $cashiers = $query->orderBy($orderBy, $sort)->paginate(20);

        return $cashiers;
    }

    /**
     * @param $id
     * @return CashierProfile
     */
    public function findCashierById($id)
    {
        $user = $this->cashierProfile->with('user', 'merchant')->whereHas('user', function ($q) use ($id) {
            $q->where('id', $id);
        })->first();

        return $user;
    }

    /**
     * @return mixed
     */
    public function getMerchantCount()
    {
        $count = $this->user->whereHas('roles', function ($q) {
            $q->where('name', 'affiliate');
        })->count();

        return $count;
    }

    /**
     * @return mixed
     */
    public function getStaffCount()
    {
        $count = $this->user->whereHas('roles', function ($q) {
            $q->whereIn('name', ['purchase', 'admin', 'shipper', 'finance', 'inventory']);
        })->count();

        return $count;
    }

    /**
     * @return mixed
     */
    public function getMemberCount()
    {
        $count = $this->user->whereHas('roles', function ($q) {
            $q->where('name', 'customer');
        })->count();

        return $count;
    }

    /**
     * @param $userId
     * @param $newPassword
     * @return bool
     */
    public function setPassword($userId, $newPassword)
    {
        $user = $this->findById($userId);

        $user->password = bcrypt($newPassword);

        $saved = $user->save();
        return $saved;
    }

    public function editStaff($userId, $input)
    {
        $user = $this->findById($userId);

        $user->nama = $input['nama'];
        $user->nohp = $input['nohp'];

        if (array_key_exists('password', $input)) {
            $user->password = bcrypt($input['password']);
        }

        $user->save();

        return $user;
    }

    public function checkUsername($username)
    {
        return $this->user->whereUsername($username)->count() > 0;
    }

    public function checkEmail($email)
    {
        return $this->user->whereEmail($email)->count() > 0;
    }

    public function checkNohp($nohp)
    {
        return $this->user->whereNohp($nohp)->count() > 0;
    }

    public function getCashierCount()
    {
        $count = $this->user->whereHas('roles', function ($q) {
            $q->where('name', 'cashier');
        })->count();

        return $count;
    }

    public function getRandomBrands($count, $active = true)
    {
        $brands = $this->user->with('profileable')->affiliate();

        if ($active) {
            $brands = $brands->active();
        }

        $brands = $brands->get();

        $chunks = array_chunk($brands->shuffle()->all(), $count);

        return $chunks[0];
    }

    public function getBrandsPaginated()
    {

    }
}