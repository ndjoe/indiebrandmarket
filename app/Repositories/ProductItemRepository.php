<?php


namespace OBID\Repositories;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;
use OBID\Models\ProductItem;

class ProductItemRepository
{
    /**
     * @var ProductItem
     */
    protected $productItem;

    /**
     * ProductItemRepository constructor.
     * @param $productItem
     */
    public function __construct(ProductItem $productItem)
    {
        $this->productItem = $productItem;
    }

    /**
     * @param $productId
     * @param $sizeId
     * @param $qty
     * @param string $desc
     * @return bool
     */
    public function incrementQty($productId, $sizeId, $qty, $desc = '')
    {
        $item = ProductItem::firstOrNew([
            'product_id' => $productId,
            'size_id' => $sizeId
        ]);

        if ($item->id) {
            $item->sizing_info = $desc;
            $item->qty += $qty;
        } else {
            $item->qty = $qty;
            $item->uid = Str::random(8);
            $item->sizing_info = $desc;
        }

        return $item->save();
    }

    /**
     * @param $productId
     * @param $sizeId
     * @param $qty
     * @return mixed
     */
    public function decrementQty($productId, $sizeId, $qty)
    {
        $item = ProductItem::where('product_id', $productId)->where('size_id', $sizeId)->first();

        $item->qty -= $qty;

        if ($item->qty < 0) {
            $item->qty = 0;
        }

        return $item->save();
    }

    /**
     * @param $productId
     * @param $sizeId
     * @param $qty
     * @param string $desc
     * @return mixed
     */
    public function setQty($productId, $sizeId, $qty, $desc = '')
    {
        $item = ProductItem::firstOrNew([
            'product_id' => $productId,
            'size_id' => $sizeId
        ]);

        $item->qty = $qty;

        if (!isset($item->id)) {
            $item->uid = Str::random(8);
        }

        $item->sizing_info = $desc;

        return $item->save();
    }

    /**
     * @param $barcode
     * @return bool|ProductItem
     */
    public function getItemByBarcode($barcode)
    {
        $item = $this->productItem->with('product')->where('uid', $barcode)->first();

        if ($item && $item->qty > 0) {
            return $item;
        }

        return false;
    }

    /**
     * @param array $ids
     * @return Collection
     */
    public function getItemsWithProduct($ids = [])
    {
        $items = $this->productItem->with('product')->whereIn('id', $ids)->get();

        return $items;
    }

    /**
     * @param $productId
     * @param $sizeId
     * @return bool|\Illuminate\Database\Eloquent\Model|null|static
     */
    public function getItemBySizeAndProductId($productId, $sizeId)
    {
        $item = $this->productItem->with('product')
            ->where('product_id', $productId)
            ->where('size_id', $sizeId)
            ->first();

        if ($item->qty > 0) {
            return $item;
        }

        return false;
    }

    /**
     * @param $barcode
     * @param $merchantId
     * @return ProductItem|null|static
     */
    public function getPosItem($barcode, $merchantId)
    {
        $item = $this->productItem
            ->with('product', 'size')
            ->whereHas('product', function ($q) use ($merchantId) {
                $q->where('author_id', $merchantId);
            })
            ->where('uid', $barcode)
            ->first();

        return $item;
    }

    /**
     * @param $productId
     * @param $sizeId
     * @return ProductItem
     */
    public function findProductItem($productId, $sizeId)
    {
        $item = $this->productItem->whereProductId($productId)->whereSizeId($sizeId)->first();

        return $item;
    }
}