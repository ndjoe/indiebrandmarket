<?php


namespace OBID\Repositories;


use OBID\Models\ProductImage;

class ProductImageRepository
{
    /**
     * @var ProductImage
     */
    protected $productImage;

    /**
     * @param ProductImage $productImage
     */
    public function __construct(ProductImage $productImage)
    {
        $this->productImage = $productImage;
    }

    /**
     * @param $input
     * @return bool|ProductImage
     */
    public function create($input)
    {
        $productImage = $this->stub($input, new ProductImage);

        $productImage->save();
    }

    /**
     * @param $input
     * @param $productId
     */
    public function update($input, $productId)
    {
        $productImage = $this->productImage->whereProductId($productId)->whereType($input['type'])->first();

        $pi = $this->stub($input, $productImage);

        $pi->save();
    }

    /**
     * @param $input
     * @param ProductImage $productImage
     * @return ProductImage
     */
    private function stub($input, ProductImage $productImage)
    {
        $productImage->type = $input['type'];
        $productImage->name = $input['name'];
        if (array_key_exists('product_id', $input)) {
            $productImage->product_id = $input['product_id'];
        }

        return $productImage;
    }
}