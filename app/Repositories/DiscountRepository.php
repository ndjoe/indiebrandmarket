<?php


namespace OBID\Repositories;


use OBID\Models\Discount;

class DiscountRepository
{
    protected $discount;

    /**
     * @param Discount $discount
     */
    public function __construct(Discount $discount)
    {
        $this->discount = $discount;
    }

    /**
     * @param $productId
     * @return Discount
     */
    public function findProductDiscount($productId)
    {
        $discount = $this->discount->whereProductId($productId)->first();

        return $discount;
    }

    /**
     * @param $input
     * @return bool|Discount
     */
    public function create($input)
    {
        $discount = $this->discountStub($input, new Discount);

        if ($discount->save()) {
            return $discount;
        }

        return false;
    }

    /**
     * @param $input
     * @param $discountId
     * @return bool|Discount
     */
    public function edit($input, $discountId)
    {
        $discount = $this->discount->whereId($discountId)->first();

        $discount = $this->discountStub($input, $discount);

        if ($discount->save()) {
            return $discount;
        }

        return false;
    }

    /**
     * @param $input
     * @param Discount $discount
     * @return Discount
     */
    public function discountStub($input, Discount $discount)
    {
        if (array_key_exists('product_id', $input)) {
            $discount->product_id = $input['product_id'];
        }
        $discount->expired_at = $input['expired_at'];
        $discount->value = $input['value'];

        return $discount;
    }
}