<?php

namespace OBID\Repositories;


use OBID\Models\RegistrationToken;

class TokenRepository
{
    /**
     * @var RegistrationToken
     */
    protected $token;

    /**
     * TokenRepository constructor.
     * @param RegistrationToken $token
     */
    public function __construct(RegistrationToken $token)
    {
        $this->token = $token;
    }

    /**
     * @param $page
     * @param string $query
     * @param string $order
     * @param string $sortBy
     * @param int $perPage
     * @return \Illuminate\Database\Query\Builder|RegistrationToken|string
     */
    public function getTokensPaginated($page, $query = null, $order = 'desc', $sortBy = 'id', $perPage = 20)
    {
        $query = $this->token;

        $query = $query->orderBy($sortBy, $order)->paginate($perPage);

        return $query;
    }

    public function deleteToken($id)
    {
        $token = $this->token->whereId($id)->first();

        if ($token->delete()) {
            return true;
        }

        return false;
    }

    /**
     * @param $input
     * @return RegistrationToken
     */
    public function createToken($input)
    {
        $newToken = $this->tokenStub($input, new RegistrationToken);

        if ($newToken->save()) {
            return $newToken;
        }

        return false;
    }

    /**
     * @param $input
     * @param RegistrationToken $token
     * @return RegistrationToken
     */
    private function tokenStub($input, RegistrationToken $token)
    {
        $token->token = $input['token'];
        $token->expired_at = $input['expired_at'];

        return $token;
    }
}