<?php


namespace OBID\Repositories;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use OBID\Models\Category;
use OBID\Models\Product;
use OBID\Models\ProductImage;
use OBID\Models\User;

class ProductRepository
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var Category
     */
    protected $category;

    /**
     * @var ProductImage
     */
    protected $productImage;

    /**
     * @param Product $product
     * @param Category $category
     * @param ProductImage $productImage
     */
    public function __construct(Product $product, Category $category, ProductImage $productImage)
    {
        $this->product = $product;
        $this->category = $category;
        $this->productImage = $productImage;
    }

    /**
     * @param $input
     * @param Product $product
     * @return Product
     */
    protected function productStub($input, Product $product)
    {
        $product->name = $input['name'];
        $product->price = $input['price'];
        $product->category_id = $input['category'];
        if (array_key_exists('author', $input)) {
            $product->author()->associate($input['author']);
        }
        $product->color_id = $input['color'];
        $product->subcategory_id = $input['subcategory'];
        $product->weight = $input['weight'];
        $product->description = $input['description'];
        $product->gender = $input['gender'];

        return $product;
    }

    /**
     * @param $input
     * @return bool|Product
     */
    public function create($input)
    {
        $newProduct = $this->productStub($input, new Product);

        if ($newProduct->save()) {
            return $newProduct;
        }

        return false;
    }

    /**
     * @param $page
     * @param $kuery
     * @param $filter
     * @param $sort
     * @param $orderBy
     * @return Collection|Product[]
     */
    public function getProductsPaginated($page, $kuery, $filter, $sort, $orderBy)
    {
        $query = $this->product->with('items', 'subcategory', 'author', 'discount', 'items.size');

        if ($filter > 0) {
            $subcats = $this->category->whereId($filter)->first()->subcategories;
            $query = $query->whereIn('subcategory_id', array_column($subcats->toArray(), 'id'));
        }

        if ($kuery) {
            $query = $query->where('name', 'LIKE', '%' . $kuery . '%');
        }

        $products = $query->orderBy($orderBy, $sort)->paginate(20);

        return $products;
    }

    /**
     * @param $page
     * @param $kuery
     * @param int $category
     * @param int $color
     * @param int $price
     * @param string $gender
     * @param int $size
     * @param $sort
     * @param $orderBy
     * @param int $perpage
     * @return mixed
     */
    public function getFrontendProductsPaginated(
        $page,
        $kuery,
        $category = 0,
        $color = 0,
        $price = 0,
        $gender = 'all',
        $size = 0,
        $sort,
        $orderBy,
        $perpage = 20
    )
    {
        $query = $this->product
            ->with('subcategory', 'author', 'discount', 'images')
            ->published();

        if ($category > 0) {
            $query = $query->where('category_id', $category);
        }
        if ($size > 0) {
            $query = $query->whereHas('items', function ($q) use ($size) {
                $q->where('size_id', $size);
            });
        }

        if ($color > 0) {
            $query = $query->whereHas('color', function ($q) use ($color) {
                $q->where('color_id', $color);
            });
        }

        if ($price > 0) {
            switch ($price) {
                case 1:
                    $query = $query->where('price', '<=', 50000);
                    break;
                case 2:
                    $query = $query->where('price', '>', 50000)
                        ->where('price', '<=', 100000);
                    break;
                case 3:
                    $query = $query->where('price', '>', 100000)
                        ->where('price', '<=', 300000);
                    break;
                case 4:
                    $query = $query->where('price', '>', 300000)
                        ->where('price', '<=', 600000);
                    break;
                case 5:
                    $query = $query->where('price', '>', 600000);
                    break;
            }
        }

        if ($gender !== 'all') {
            $query = $query->where(function ($q) use ($gender) {
                $q->where('gender', $gender)->orWhere('gender', 'all');
            });
        }

        $products = $query->orderBy($orderBy, $sort)->paginate($perpage);

        return $products;
    }

    public function getSaleProductPaginated(
        $page = 1,
        $kuery,
        $category = 0,
        $color = 0,
        $price = 0,
        $gender = 'all',
        $size = 0,
        $sort,
        $orderBy,
        $perpage = 20
    )
    {
        $query = $this->product
            ->with('subcategory', 'author', 'discount', 'images')
            ->published()
            ->discounted();

        if ($category > 0) {
            $query = $query->where('category_id', $category);
        }

        if ($size > 0) {
            $query = $query->whereHas('items', function ($q) use ($size) {
                $q->where('size_id', $size);
            });
        }

        if ($color > 0) {
            $query = $query->whereHas('color', function ($q) use ($color) {
                $q->where('color_id', $color);
            });
        }

        if ($price > 0) {
            switch ($price) {
                case 1:
                    $query = $query->where('price', '<=', 50000);
                    break;
                case 2:
                    $query = $query->where('price', '>', 50000)
                        ->where('price', '<=', 100000);
                    break;
                case 3:
                    $query = $query->where('price', '>', 100000)
                        ->where('price', '<=', 300000);
                    break;
                case 4:
                    $query = $query->where('price', '>', 300000)
                        ->where('price', '<=', 600000);
                    break;
                case 5:
                    $query = $query->where('price', '>', 600000);
                    break;
            }
        }

        if ($gender !== 'all') {
            $query = $query->where(function ($q) use ($gender) {
                $q->where('gender', $gender)->orWhere('gender', 'all');
            });
        }

        $products = $query->orderBy($orderBy, $sort)->paginate($perpage);

        return $products;
    }

    /**
     * @param $id
     * @return bool
     */
    public function publishProduct($id)
    {
        $product = $this->findProductById($id);

        $product->published_at = Carbon::now();

        if ($product->save()) {
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function unpublishProduct($id)
    {
        $product = $this->findProductById($id);

        $product->published_at = null;

        if ($product->save()) {
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @param $input
     * @return Product
     */
    public function update($id, $input)
    {
        $product = $this->product->whereId($id)->first();

        $updated = $this->productStub($input, $product);

        if ($updated->save()) {
            return $updated;
        }

        return false;
    }

    /**
     * only for typehinting actually lol
     *
     * @param $id
     * @return Product
     */
    public function findProductById($id)
    {
        $product = $this->product->whereId($id)->first();

        return $product;
    }

    public function getMerchantProducts($merchant, $page, $kuery, $filter, $sort, $orderBy)
    {
        $query = $this->product->with('author', 'subcategory', 'items', 'discount', 'items.size')->whereAuthorId($merchant->id);

        if ($filter > 0) {
            $query = $query->where('category_id', $filter);
        }

        if ($kuery) {
            $query = $query->where('name', 'LIKE', '%' . $kuery . '%');
        }

        $products = $query->orderBy($orderBy, $sort)->paginate(20);

        return $products;
    }

    public function getBrandProductsBySubcategory(
        User $brand,
        $page = 1,
        $category = 0,
        $size = 0,
        $gender = 'all',
        $sort,
        $orderBy,
        $color,
        $price
    )
    {
        $query = $this->product
            ->with('author', 'subcategory', 'items', 'items.size', 'images', 'discount')
            ->whereAuthorId($brand->id)
            ->published();

        if ($category > 0) {
            $query = $query->where('subcategory_id', $category);
        }

        if ($size > 0) {
            $query = $query->whereHas('items', function ($q) use ($size) {
                $q->where('size_id', $size);
            });
        }

        if ($color > 0) {
            $query = $query->whereHas('color', function ($q) use ($color) {
                $q->where('color_id', $color);
            });
        }

        if ($price > 0) {
            switch ($price) {
                case 1:
                    $query = $query->where('price', '<=', 50000);
                    break;
                case 2:
                    $query = $query->where('price', '>', 50000)
                        ->where('price', '<=', 100000);
                    break;
                case 3:
                    $query = $query->where('price', '>', 100000)
                        ->where('price', '<=', 300000);
                    break;
                case 4:
                    $query = $query->where('price', '>', 300000)
                        ->where('price', '<=', 600000);
                    break;
                case 5:
                    $query = $query->where('price', '>', 600000);
                    break;
            }
        }

        if ($gender !== 'all') {
            $query = $query->where(function ($q) use ($gender) {
                $q->where('gender', $gender)->orWhere('gender', 'all');
            });
        }

        $products = $query->orderBy($orderBy, $sort)->paginate(12);

        return $products;
    }

    public function getPublishedProductCount()
    {
        $count = $this->product->whereNotNull('published_at')->count();

        return $count;
    }

    public function getUnpublishedProductCount()
    {
        $count = $this->product->whereNull('published_at')->count();

        return $count;
    }

    public function getRandomProducts($count = 4)
    {
        $products = $this->product
            ->with('subcategory', 'author', 'discount', 'images')
            ->published()
            ->get();

        $chunks = array_chunk($products->shuffle()->all(), $count);

        return $chunks[0];
    }

    public function getBrandRandomProducts($count = 6, $brandUsername)
    {
        $products = $this->product
            ->with('subcategory', 'author', 'discount', 'images')
            ->whereHas('author', function ($q) use ($brandUsername) {
                $q->where('username', $brandUsername);
            })
            ->published()
            ->get();

        $products->shuffle();

        $chunks = array_chunk($products->toArray(), $count);

        return $chunks[0];
    }
}