<?php


namespace Repositories;


class CartRepository
{
    protected $session;

    /**
     * CartRepository constructor.
     * @param $session
     */
    public function __construct($session)
    {
        $this->session = $session;
    }
}