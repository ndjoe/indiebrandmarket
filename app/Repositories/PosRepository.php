<?php


namespace OBID\Repositories;


use Carbon\Carbon;
use OBID\Models\Order;
use OBID\Models\PosCash;

class PosRepository
{
    /**
     * @var PosCash
     */
    protected $posCash;
    /**
     * @var OrderRepository
     */
    protected $orderRepo;
    /**
     * @var PaymentRepository
     */
    protected $paymentRepo;
    /**
     * @var OrderItemRepository
     */
    protected $orderItemRepo;
    /**
     * @var ProductItemRepository
     */
    protected $productItemRepo;

    /**
     * @var OrderAddressRepository
     */
    protected $orderAddressRepo;

    /**
     * @param PosCash $posCash
     * @param OrderRepository $orderRepo
     * @param PaymentRepository $paymentRepo
     * @param OrderItemRepository $orderItemRepo
     * @param ProductItemRepository $productItemRepo
     * @param OrderAddressRepository $orderAddressRepository
     */
    public function __construct(
        PosCash $posCash,
        OrderRepository $orderRepo,
        PaymentRepository $paymentRepo,
        OrderItemRepository $orderItemRepo,
        ProductItemRepository $productItemRepo,
        OrderAddressRepository $orderAddressRepository
    )
    {
        $this->posCash = $posCash;
        $this->orderRepo = $orderRepo;
        $this->paymentRepo = $paymentRepo;
        $this->orderItemRepo = $orderItemRepo;
        $this->productItemRepo = $productItemRepo;
        $this->orderAddressRepo = $orderAddressRepository;
    }

    /**
     * @param array $cart
     * @param $cashierId
     * @param $paymentType
     * @param $paymentData
     * @param $merchantId
     * @return bool|Order
     */
    public function processPosPurchase($cart = [], $cashierId, $paymentType, $paymentData, $merchantId)
    {
        $preparedCart = $this->prepareCart($cart);
        $order = $this->createOrder($preparedCart, $cashierId);
        $this->createOrderItems($preparedCart['items'], $order);
        $this->createPayment($paymentType, $paymentData, $order->id);
        $this->createAddress($paymentData, $order->id);
        $this->decrementItem($preparedCart['items']);

        if ($paymentType === 'cash') {
            $this->updateCashAccount($merchantId, $paymentData, $order->netTotal);
        }

        return $order;
    }

    public function createAddress($data, $orderId)
    {
        $input = [
            'nama' => $data['nama'],
            'order_id' => $orderId
        ];

        $this->orderAddressRepo->store($input, 'offline');
    }

    /**
     * @param $merchantId
     * @param $paymentData
     * @param $orderTotal
     * @return bool|PosCash|static
     */
    public function updateCashAccount($merchantId, $paymentData, $orderTotal)
    {
        $account = $this->posCash->whereMerchantId($merchantId)->first();
        $account->cash += $orderTotal;

        if ($account->save()) {
            return $account;
        }

        return false;
    }

    public function decrementItem($items = [])
    {
        foreach ($items as $i) {
            $this->productItemRepo->decrementQty($i['productId'], $i['sizeId'], $i['qty']);
        }
    }

    public function createPayment($paymentType, $paymentData, $orderId)
    {
        if ($paymentType === 'edc') {
            $input = [
                'type' => 'edc',
                'bank' => $paymentData['bank'],
                'noref' => $paymentData['noref'],
                'card_number' => $paymentData['cardNumber'],
                'expiration_date' => $paymentData['expirationDate'],
                'orderId' => $orderId
            ];
        } else {
            $input = [
                'type' => 'cash',
                'orderId' => $orderId
            ];
        }

        $this->paymentRepo->create($input);
    }

    public function createOrderItems($items = [], Order $order)
    {
        foreach ($items as $i) {
            $input = [
                'orderId' => $order->id,
                'productItemId' => $i['itemId'],
                'accepted_at' => Carbon::now(),
                'qty' => $i['qty'],
                'adjusment' => $i['price'] * $i['qty']
            ];
            $this->orderItemRepo->create($input);
        }
    }

    /**
     * @param array $cart
     * @param $userId
     * @return bool|\OBID\Models\Order
     */
    public function createOrder($cart = [], $userId)
    {
        $serializeInput = [
            'type' => 'offline',
            'netTotal' => $cart['total'],
        ];

        return $this->orderRepo->create($serializeInput, $userId);
    }

    public function getPosCash($merchantId)
    {
        $cash = $this->posCash->whereMerchantId($merchantId)->first();

        return $cash->cash;
    }

    public function setMerchantPosCash($merchantId, $cash)
    {
        $pos = $this->posCash->whereMerchantId($merchantId)->first();

        $pos->cash = $cash;

        $pos->save();

        return $pos;
    }

    /**
     * @param array $cart
     * @return array
     */
    private function prepareCart($cart = [])
    {
        $total = 0;
        $items = [];
        foreach ($cart as $c) {
            array_push($items, [
                'qty' => $c['qty'],
                'itemId' => $c['id'],
                'sizeId' => $c['options']['sizeId'],
                'productId' => $c['options']['productId'],
                'price' => $c['price']
            ]);
            $total += $c['price'] * $c['qty'];
        }

        return compact('items', 'total');
    }

}