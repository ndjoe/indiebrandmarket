<?php


namespace OBID\Repositories;


use OBID\Models\Order;
use OBID\Models\OrderAddress;

class OrderAddressRepository
{
    /**
     * @var OrderAddress
     */
    protected $orderAddress;

    /**
     * @var Order
     */
    protected $order;

    /**
     * OrderAddressRepository constructor.
     * @param OrderAddress $orderAddress
     * @param Order $order
     */
    public function __construct(OrderAddress $orderAddress, Order $order)
    {
        $this->orderAddress = $orderAddress;
        $this->order = $order;
    }

    /**
     * @param $input
     * @param $type
     * @return bool|OrderAddress
     */
    public function store($input, $type)
    {
        $orderAddress = $this->make($input, new OrderAddress, $type);

        if ($orderAddress->save()) {
            return $orderAddress;
        }

        return false;
    }

    /**
     * @param $page
     * @param $kuery
     * @param $sort
     * @param $orderBy
     * @return mixed
     */
    public function getPaginated($page, $kuery, $sort, $orderBy)
    {
        $ids = $this->order->whereUserId(0)->lists('id')->all();
        $query = $this->orderAddress->whereIn('order_id', $ids);

        if ($kuery) {
            $query = $query->where('nama', 'LIKE', '%' . $kuery . '%');
        }

        $result = $query->orderBy($orderBy, $sort)->paginate(20);

        return $result;
    }

    public function getGuestCount()
    {
        $ids = $this->order->whereUserId(0)->lists('id')->all();

        $query = $this->orderAddress->whereIn('order_id', $ids);

        $result = $query->count();

        return $result;
    }

    /**
     * @param $input
     * @param OrderAddress $orderAddress
     * @param $type
     * @return OrderAddress
     */
    private function make($input, OrderAddress $orderAddress, $type)
    {
        $orderAddress->nama = $input['nama'];
        $orderAddress->order_id = $input['order_id'];
        if ($type == 'online') {
            $orderAddress->alamat = $input['alamat'];
            $orderAddress->kodepos = $input['kodepos'];
            $orderAddress->kota = $input['kota'];
            $orderAddress->negara = $input['negara'];
            $orderAddress->nohp = $input['nohp'];
            $orderAddress->provinsi = $input['provinsi'];
        }

        return $orderAddress;
    }
}