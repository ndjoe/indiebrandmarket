<?php


namespace OBID\Repositories;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use OBID\Models\Order;

class OrderRepository
{
    /**
     * @var Order
     */
    protected $order;

    /**
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param $input
     * @param $userId
     * @param Order $order
     * @return Order
     */
    protected function orderStub($input, $userId, Order $order)
    {
        if ($input['type'] === 'offline') {
            $order->taxTotal = 0;
            $order->shippingCost = 0;
            $order->netTotal = $input['netTotal'];
            $order->grossTotal = $order->netTotal + $order->shippingCost + $order->taxTotal;
            $order->order_code = str_pad(mt_rand(0, 99999), 5, '0', STR_PAD_LEFT);
            $order->uniqueCost = 0;

            $order->type = 'offline';
            $order->total_weight = 0;
            $order->user_id = $userId;
            $order->confirmed_at = Carbon::now();
            $order->expired_at = null;
        } else {
            $order->taxTotal = 0;
            $order->shippingCost = $input['shippingCost'];
            $order->shippingType = $input['shippingType'];
            $order->netTotal = $input['netTotal'];
            $order->adjustmentCoupon = $input['adjusmentCoupon'];
            if (array_key_exists('birthdayAdjusment', $input)) {
                $order->birthdayAdjusment = $input['birthdayAdjusment'];
            } else {
                $order->birthdayAdjusment = 0;
            }
            $order->grossTotal = $order->netTotal + $order->shippingCost + $order->taxTotal - $order->birthdayAdjusment - $order->adjustmentCoupon;
            $order->payment_method = $input['payment_method'];
            $order->order_code = str_pad(mt_rand(0, 99999), 5, '0', STR_PAD_LEFT);
            $order->currency = $input['currency'];
            $order->currency_value = $input['currency_value'];
            $order->type = 'online';
            $order->uniqueCost = $input['uniqueCost'];
            $order->total_weight = $input['total_weight'];
            $order->user_id = $userId;
            $order->confirmed_at = null;
            $order->expired_at = Carbon::now()->addDay(1);
        }

        return $order;
    }

    /**
     * @param $input
     * @param $userId
     * @return bool|Order
     */
    public function create($input, $userId)
    {
        $newOrder = $this->orderStub($input, $userId, new Order);

        if ($newOrder->save()) {
            return $newOrder;
        }

        return false;
    }

    /**
     * @param $page
     * @param $kuery
     * @param $filter
     * @param $sort
     * @param $orderBy
     * @return Collection
     */
    public function getOrdersPaginated($page, $kuery, $filter, $sort, $orderBy)
    {
        $query = $this->order
            ->with('user', 'items.productItem.product', 'items.productItem.size', 'orderConfirmation')
            ->where('type', 'online');

        if ($filter) {
            $query = $query->where('type', $filter);
        }

        if ($kuery) {
            $query = $query->where('id', 'LIKE', '%' . $kuery . '%');
        }

        $orders = $query->orderByRaw("confirmed_at NULLS FIRST")->paginate(20);

        return $orders;
    }

    public function getConfirmedOrdersPaginated($page, $kuery, $sort, $orderBy)
    {
        $query = $this->order->with('user', 'items.productItem.product', 'items.productItem.size', 'shipping', 'items.productItem.product.author', 'address')
            ->whereNotNull('confirmed_at')
            ->where('type', 'online');

        if ($kuery) {
            $query = $query->where('id', 'LIKE', '%' . $kuery . '%');
        }

        $orders = $query->orderBy($orderBy, $sort)->paginate(20);

        return $orders;
    }

    /**
     * @param $id
     * @return Order
     */
    public function findById($id)
    {
        $product = $this->order->whereId($id)->first();

        return $product;
    }

    /**
     * @param $code
     * @return Order
     */
    public function findByCode($code)
    {
        $order = $this->order->whereOrderCode($code)->first();

        return $order;
    }

    public function getUserOrders($page, $sort, $orderBy, $user)
    {
        $query = $this->order->with('shipping')->whereUserId($user->id);

        $orders = $query->orderBy($orderBy, $sort)->paginate(20);

        return $orders;
    }

    public function orderStats()
    {
        $thisMonthOfflineNetSales = $this->order
            ->where(\DB::raw('extract(month from created_at)'), '=', date('n'))
            ->where('type', 'offline')
            ->whereNotNull('confirmed_at')
            ->sum('netTotal');
        $thisMonthOnlineNetSales = $this->order
            ->where(\DB::raw('extract(month from created_at)'), '=', date('n'))
            ->whereNotNull('confirmed_at')
            ->where('type', 'online')
            ->sum('netTotal');
        $thisMonthSalesTotal = $thisMonthOfflineNetSales + $thisMonthOnlineNetSales;

        return compact('thisMonthOfflineNetSales', 'thisMonthOnlineNetSales', 'thisMonthSalesTotal');
    }

    public function getBuyersIdByBrand($brandid)
    {
        $ids = $this->order->whereHas('items.productItem.product', function ($q) use ($brandid) {
            $q->where('author_id', $brandid);
        })->select('user_id')->get();

        return $ids;
    }

    public function getPosOrders($page, $sort, $orderBy, $merchantId, $type)
    {
        $query = $this->order
            ->with('items.productItem.product', 'items.productItem.size', 'payment', 'user.profileable', 'address')
            ->whereType('offline')
            ->whereHas('items.productItem.product', function ($q) use ($merchantId) {
                $q->where('author_id', $merchantId);
            });

        if ($type !== 'all') {
            $query = $query->whereHas('payment', function ($q) use ($type) {
                $q->where('type', $type);
            });
        }

        $orders = $query->orderBy($orderBy, $sort)->paginate(20);

        return $orders;
    }

    /**
     * @return mixed
     */
    public function getOrderConfirmedCount()
    {
        $query = $this->order->whereNotNull('confirmed_at')->count();

        return $query;
    }

    public function getOrderUnconfirmedCount()
    {
        $query = $this->order->whereNull('confirmed_at')->count();

        return $query;
    }

    public function getOrderNotYetShippedCount()
    {
        $count = $this->order->doesntHave('shipping')->count();

        return $count;
    }

    public function getShippedOrderCount()
    {
        $count = $this->order->has('shipping')->count();

        return $count;
    }

    /**
     * @param $id
     * @param $brandId
     * @return Order
     */
    public function getPosOrderById($id, $brandId)
    {
        $order = $this
            ->order
            ->with('items.productItem.product', 'items.productItem.size')
            ->where('id', $id)
            ->whereHas('items.productItem.product', function ($q) use ($brandId) {
                $q->where('author_id', $brandId);
            })->first();

        return $order;
    }
}