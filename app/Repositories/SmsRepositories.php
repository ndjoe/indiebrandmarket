<?php


namespace OBID\Repositories;


use GuzzleHttp\Client;

class SmsRepositories
{
    protected static $baseurl = 'https://alpha.zenziva.net/apps/smsapi.php';
    protected $userkey;
    protected $passkey;
    protected $client;

    public function __construct()
    {
        $this->userkey = 'keglh0';
        $this->passkey = 'S3mp4K@s1k';
        $this->client = new Client(['base_uri' => static::$baseurl]);
    }

    public function sendActivationMessage($nohp, $nama, $activationKey)
    {
        $message = "kode verifikasi originalbrands.id anda adalah $activationKey";

        $this->client->request('GET', static::$baseurl, [
            'query' => [
                'userkey' => $this->userkey,
                'passkey' => $this->passkey,
                'nohp' => $nohp,
                'pesan' => $message
            ]
        ]);
    }

    public function sendWelcomeMessage($nohp, $nama)
    {
        $message = "hai $nama, selamat datang di Originalbrands.id";

        $this->client->request('GET', static::$baseurl, [
            'query' => [
                'userkey' => $this->userkey,
                'passkey' => $this->passkey,
                'nohp' => $nohp,
                'pesan' => $message
            ]
        ]);
    }

    public function sendResiMessage($nohp, $resi, $orderId)
    {
        $message = "Order anda dengan nomor $orderId sudah dikirim dengan resi $resi, selengkapnya silahkan cek pada halaman profil anda";

        $this->client->request('GET', static::$baseurl, [
            'query' => [
                'userkey' => $this->userkey,
                'passkey' => $this->passkey,
                'nohp' => $nohp,
                'pesan' => $message
            ]
        ]);
    }

    public function sendOrderMessage($nohp, $orderId, $ordertagihan, $pilihanbank, $norek)
    {
        $message = "Nomor order $orderId silahkan transfer Rp. $ordertagihan via $pilihanbank : $norek a.n PT. Trilogi Cipta Reswara.";

        $this->client->request('GET', static::$baseurl, [
            'query' => [
                'userkey' => $this->userkey,
                'passkey' => $this->passkey,
                'nohp' => $nohp,
                'pesan' => $message
            ]
        ]);
    }

    public function sendConfirmedMessage($nohp, $orderId)
    {
        $message = '';
    }

}