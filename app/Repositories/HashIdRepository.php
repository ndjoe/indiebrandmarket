<?php


namespace OBID\Repositories;


use Jenssegers\Optimus\Optimus;

class HashIdRepository
{
    /**
     * @var Optimus
     */
    protected $optimus;

    public function __construct()
    {
        $this->optimus = new Optimus(2013587231, 1242289375, 1357632514);
    }

    /**
     * @param $id
     * @return int
     */
    public function encode($id)
    {
        return $this->optimus->encode($id);
    }

    /**
     * @param $number
     * @return int
     */
    public function decode($number)
    {
        return $this->optimus->decode($number);
    }
}