<?php

namespace OBID\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'OBID\Events\NewPosPurchase' => [],
        'OBID\Events\NewOnlinePurchase' => [],
        'OBID\Events\Member\MemberRegistered' => [
            'OBID\Listeners\Sms\SendActivationCode',
            'OBID\Listeners\Email\SendActivationEmail'
        ],
        'OBID\Events\Member\MemberActivated' => [
        ],
        'OBID\Events\Product\ProductCreated' => [
        ],
        'OBID\Events\Order\OrderCreated' => [
            'OBID\Listeners\Sms\SendOrderMessage'
        ],
        'OBID\Events\Order\OrderConfirmed' => [
        ],
        'OBID\Events\Order\OrderItemConfirmed' => [],
        'OBID\Events\Order\OrderShipped' => [
            'OBID\Listeners\Sms\SendResiMessage'
        ],
        'OBID\Events\Member\ResetPassword' => [
            'OBID\Listeners\Email\SendForgotPasswordEmail'
        ]
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
