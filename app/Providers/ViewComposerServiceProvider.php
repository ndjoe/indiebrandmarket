<?php

namespace OBID\Providers;


use Illuminate\Support\ServiceProvider;
use OBID\Http\Controllers\Frontend\ViewComposers\RecommendedBrandComposer;
use OBID\Http\Controllers\Frontend\ViewComposers\RecommendedProductComposer;

class ViewComposerServiceProvider extends ServiceProvider
{

    public function boot()
    {
        view()->composer(
            'frontend.partials.anotherbrands', RecommendedBrandComposer::class
        );

        view()->composer(
            'frontend.partials.anotherProducts', RecommendedProductComposer::class
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // TODO: Implement register() method.
    }
}