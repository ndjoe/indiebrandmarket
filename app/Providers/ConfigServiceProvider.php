<?php

namespace OBID\Providers;


use Illuminate\Support\ServiceProvider;

class ConfigServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        config([
            'laravellocalization.supportedLocales' => [
                'id' => array('name' => 'Indonesia', 'script' => 'Latn', 'native' => 'Indonesia'),
                'en' => array('name' => 'English', 'script' => 'Latn', 'native' => 'English')
            ],

            'laravellocalization.useAcceptLanguageHeader' => true,

            'laravellocalization.hideDefaultLocaleInURL' => false
        ]);
    }
}