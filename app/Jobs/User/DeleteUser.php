<?php

namespace OBID\Jobs\User;


use Illuminate\Contracts\Bus\SelfHandling;
use OBID\Jobs\Job;
use OBID\Repositories\UserRepository;

class DeleteUser extends Job implements selfHandling
{
    public $userId;

    /**
     * DeleteUser constructor.
     * @param $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    public function handle(
        UserRepository $userRepository
    )
    {
        $user = $userRepository->findById($this->userId);

        if ($user->hasRole('affiliate')) {

        }
    }
}