<?php

namespace OBID\Jobs\Newsletter;


use Carbon\Carbon;
use Illuminate\Contracts\Bus\SelfHandling;
use OBID\Jobs\Job;
use OBID\Models\Email;
use OBID\Repositories\CouponRepository;

class NewSubscriber extends Job implements SelfHandling
{
    public $email;

    /**
     * NewSubscriber constructor.
     * @param $email
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    public function handle(CouponRepository $couponRepository)
    {
        $email = new Email;
        $email->email = $this->email;

        if ($email->save()) {
            $coupon = $couponRepository->create([
                'value' => 20000,
                'expired_at' => Carbon::now()->addMonth(),
                'qty' => 1
            ]);

            return $coupon;
        }
    }
}