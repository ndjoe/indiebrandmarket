<?php

namespace OBID\Jobs;

use OBID\Events\NewPosPurchase;
use OBID\Jobs\Job;
use OBID\Repositories\PosRepository;
use Illuminate\Contracts\Bus\SelfHandling;

class ProcessPosPurchase extends Job implements SelfHandling
{
    /**
     * @var array
     */
    public $cart;

    public $paymentType;

    public $paymentData;

    public $cashierId;

    public $merchantId;

    /**
     * @param array $cart
     * @param $paymentType
     * @param array $paymentData
     * @param $cashierId
     * @param $merchantId
     */
    public function __construct($cart = [], $paymentType, $paymentData = [], $cashierId, $merchantId)
    {
        $this->cart = $cart;
        $this->paymentType = $paymentType;
        $this->paymentData = $paymentData;
        $this->cashierId = $cashierId;
        $this->merchantId = $merchantId;
    }

    /**
     * @param PosRepository $pos
     * @return bool|\OBID\Models\Order
     */
    public function handle(PosRepository $pos)
    {
        $order = $pos->processPosPurchase(
            $this->cart,
            $this->cashierId,
            $this->paymentType,
            $this->paymentData,
            $this->merchantId
        );

        event(new NewPosPurchase($order, $this->merchantId, $this->cashierId));

        return $order;
    }
}
