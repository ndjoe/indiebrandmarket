<?php

namespace OBID\Jobs;


use Carbon\Carbon;
use Illuminate\Contracts\Bus\SelfHandling;
use OBID\Events\Order\OrderConfirmed;
use OBID\Models\OrderItem;
use OBID\Models\OrderPayment;
use OBID\Repositories\OrderItemRepository;
use OBID\Repositories\OrderRepository;
use OBID\Repositories\ProductItemRepository;

class ConfirmPaypalPayment extends Job implements selfHandling
{
    public $orderId;

    /**
     * ConfirmPaypalPayment constructor.
     * @param $orderId
     */
    public function __construct($orderId)
    {
        $this->orderId = $orderId;
    }

    public function handle(
        OrderRepository $orderRepository,
        ProductItemRepository $productItemRepository,
        OrderItemRepository $orderItemRepository)
    {
        $order = $orderRepository->findById($this->orderId);
        $payment = new OrderPayment;
        $payment->type = 'online';
        $payment->bank = 'paypal';

        $order->payment()->save($payment);
        $order->confirmed_at = Carbon::now();
        $order->expired_at = null;
        $order->save();

        $orderItems = $order->items;
        $items = $orderItemRepository->getMultipleByIds(array_column($orderItems->toArray(), 'id'));

        $items->each(function (OrderItem $o) use ($productItemRepository) {
            $productItemRepository->decrementQty($o->productItem->product_id, $o->productItem->size_id, $o->qty);
            event(new OrderConfirmed($o->productItem->product->author_id));
        });
    }

}