<?php


namespace OBID\Jobs\Product;


use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Support\Str;
use OBID\Jobs\Job;
use OBID\Repositories\DiscountRepository;
use OBID\Repositories\ProductImageRepository;
use OBID\Repositories\ProductItemRepository;
use OBID\Repositories\ProductRepository;

class ProcessNewProduct extends Job implements SelfHandling
{
    public $data;

    /**
     * ProcessNewProduct constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handle(
        ProductRepository $productRepository,
        DiscountRepository $discountRepository,
        ProductImageRepository $productImageRepository,
        ProductItemRepository $productItemRepository
    )
    {
        $product = $productRepository->create($this->data['product']);

        foreach ($this->data['images'] as $image) {
            $name = '';
            if (array_key_exists('file', $image)) {
                $name = Str::slug($product->name) . Str::random(3) . '.' . $image['file']->getClientOriginalExtension();
                \Image::make($image['file']->getRealPath())
                    ->fit(600, 800)
                    ->save(base_path() . '/public/images/catalog/' . $name);
            }
            $imagesData = [
                'type' => $image['type'],
                'name' => $name,
                'product_id' => $product->id
            ];
            $i = $productImageRepository->create($imagesData);
        }

        $discountData = $this->data['discount'];

        $discountData['product_id'] = $product->id;

        $disc = $discountRepository->create($discountData);

        foreach ($this->data['items'] as $item) {
            $productItemRepository->incrementQty($product->id, $item['size'], $item['qty'], $item['desc']);
        }

        if ($product) {
            return true;
        }

        return false;
    }
}