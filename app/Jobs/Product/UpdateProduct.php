<?php


namespace OBID\Jobs\Product;


use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Support\Str;
use OBID\Jobs\Job;
use OBID\Repositories\DiscountRepository;
use OBID\Repositories\ProductImageRepository;
use OBID\Repositories\ProductRepository;

class UpdateProduct extends Job implements SelfHandling
{
    public $data;
    public $productId;

    /**
     * @param $productId
     * @param $data
     */
    public function __construct($productId, $data)
    {
        $this->data = $data;
        $this->productId = $productId;
    }

    public function handle(
        ProductRepository $productRepository,
        DiscountRepository $discountRepository,
        ProductImageRepository $productImageRepository
    )
    {
        $product = $productRepository->update($this->productId, $this->data['product']);

        foreach ($this->data['images'] as $image) {
            $name = '';
            if (array_key_exists('file', $image)) {
                $name = Str::slug($product->name) . Str::random(3) . '.' . $image['file']->getClientOriginalExtension();
                \Image::make($image['file']->getRealPath())
                    ->fit(600, 800)
                    ->save(base_path() . '/public/images/catalog/' . $name);
            }
            $imagesData = [
                'type' => $image['type'],
                'name' => $name,
                'product_id' => $product->id
            ];
            $i = $productImageRepository->update($imagesData, $this->productId);
        }
        if (!$product) {
            dd($this->data);
        }

        $disc = $discountRepository->edit($this->data['discount'], $product->discount()->first()->id);

        if ($product) {
            return true;
        }

        return false;
    }
}