<?php


namespace OBID\Jobs;


use Illuminate\Contracts\Bus\SelfHandling;
use OBID\Events\Order\OrderCreated;
use OBID\Models\Order;
use OBID\Repositories\CouponRepository;
use OBID\Repositories\OrderAddressRepository;
use OBID\Repositories\OrderItemRepository;
use OBID\Repositories\OrderRepository;
use OBID\Repositories\BirthdayDiscountRepository;

class ProcessOnlinePuchase extends Job implements selfHandling
{
    public $cart;

    /**
     * @param array $cart
     */
    public function __construct($cart = [])
    {
        $this->cart = $cart;
    }

    /**
     * @param OrderRepository $orderRepo
     * @param OrderItemRepository $orderItemRepo
     * @param OrderAddressRepository $orderAddressRepo
     * @param CouponRepository $couponRepo
     * @param BirthdayDiscountRepository $birthdayDiscountRepository
     * @return Order
     */
    public function handle(
        OrderRepository $orderRepo,
        OrderItemRepository $orderItemRepo,
        OrderAddressRepository $orderAddressRepo,
        CouponRepository $couponRepo,
        BirthdayDiscountRepository $birthdayDiscountRepository
    )
    {
        $order = $orderRepo->create($this->cart['order'], $this->cart['userId']);

        if ($order->birthdayAdjusment > 0) {
            $birthdayDiscountRepository->create(
                [
                    'order_id' => $order->id,
                    'user_id' => $order->user_id
                ]
            );
        }

        foreach ($this->cart['items'] as $i) {
            $orderItemRepo->create([
                'orderId' => $order->id,
                'adjusment' => $i['adjusment'],
                'productItemId' => $i['itemId'],
                'qty' => $i['qty']
            ]);
        }

        $adressData = $this->cart['address'];
        $adressData['order_id'] = $order->id;
        $orderAddressRepo->store($adressData, 'online');
        if ($this->cart['couponId'] > 0) {
            $couponRepo->attachCouponToOrder($order->id, $this->cart['couponId']);
        }

        event(new OrderCreated($order));

        return $order;
    }
}