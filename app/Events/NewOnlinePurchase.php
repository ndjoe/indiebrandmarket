<?php

namespace OBID\Events;

use OBID\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use OBID\Models\Order;
use OBID\Models\OrderItem;

class NewOnlinePurchase extends Event implements shouldBroadCast
{
    use SerializesModels;

    /**
     * @var Order
     */
    public $orderItem;

    public $merchantId;

    /**
     * @param Order $orderItem
     * @param $merchantId
     */
    public function __construct(Order $orderItem, $merchantId)
    {
        $this->orderItem = $orderItem;
        $this->merchantId = $merchantId;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['merchant' . $this->merchantId];
    }

    public function broadcastAs()
    {
        return ['orders.payment-confirmed'];
    }
}
