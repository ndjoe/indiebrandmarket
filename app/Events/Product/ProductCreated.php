<?php

namespace OBID\Events\Product;

use OBID\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use OBID\Models\Product;

class ProductCreated extends Event
{
    use SerializesModels;

    public $product;
    public $category;
    public $fromId;

    /**
     * @param Product $product
     * @param $fromId
     */
    public function __construct(Product $product, $fromId)
    {
        $this->product = $product;
        $this->category = 'product.created';
        $this->fromId = $fromId;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
