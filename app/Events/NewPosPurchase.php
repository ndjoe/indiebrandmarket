<?php

namespace OBID\Events;

use OBID\Events\Event;
use OBID\Models\Order;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewPosPurchase extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $order;
    public $merchantId;
    public $cashierId;

    /**
     * @param Order $order
     * @param $merchantId
     * @param $cashierId
     */
    public function __construct(Order $order, $merchantId, $cashierId = null)
    {
        $this->order = $order;
        $this->merchantId = $merchantId;
        $this->cashierId = $cashierId;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['merchant.' . $this->merchantId];
    }

    public function broadcastAs()
    {
        return ['pos.order-created'];
    }
}
