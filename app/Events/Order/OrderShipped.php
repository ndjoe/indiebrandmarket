<?php

namespace OBID\Events\Order;

use OBID\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use OBID\Models\Order;
use OBID\Models\Shipping;
use OBID\Models\User;

class OrderShipped extends Event
{
    use SerializesModels;

    public $shipping;
    public $order;
    public $author;

    /**
     * @param Shipping $shipping
     * @param Order $order
     * @param User $author
     */
    public function __construct(Shipping $shipping, Order $order, $author = null)
    {
        $this->shipping = $shipping;
        $this->author = $author;
        $this->order = $order;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public
    function broadcastOn()
    {
        return [];
    }
}
