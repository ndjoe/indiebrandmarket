<?php

namespace OBID\Events\Order;

use OBID\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use OBID\Models\Order;

class OrderConfirmed extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $merchantId;

    /**
     * @param $merchantId
     */
    public function __construct($merchantId)
    {
        $this->merchantId = $merchantId;
    }

    /**
     * @return array
     */
    public function broadcastAs()
    {
        return ['orders.confirmed'];
    }

    public function broadcastOn()
    {
        return ['merchant.' . $this->merchantId];
    }
}
