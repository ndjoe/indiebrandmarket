<?php

namespace OBID\Events\Order;

use OBID\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use OBID\Models\OrderItem;

class OrderItemConfirmed extends Event implements ShouldBroadcast
{
    use SerializesModels;
    public $orderItem;

    /**
     * @param OrderItem $orderItem
     */
    public function __construct(OrderItem $orderItem)
    {
        $this->orderItem = $orderItem;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['orders'];
    }

    public function broadcastAs()
    {
        return ['item.accepted'];
    }
}
