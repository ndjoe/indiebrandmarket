<?php

namespace OBID\Events\Order;

use OBID\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use OBID\Models\Order;

class OrderCreated extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $order;

    /**
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['orders'];
    }

    public function broadcastAs()
    {
        return ['created'];
    }
}
