@extends('backend.base')

@section('title')
    <title>Dashboard Staff Inventory IBM</title>
@endsection

@section('menuuser')
    @include('backend.inventory.partials.usermenu')
@endsection

@section('menu')
    @include('backend.inventory.partials.menu')
@endsection

@section('morecss')
    <link rel="stylesheet" href="/css/inventory/dashboard.css">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6 col-xs-6">
            <order-stat :color="'blue'" :value="productCount*1" :title="'Total Produk'"
                        :icon="'fa fa-comments'"></order-stat>
        </div>
        <div class="col-md-6 col-xs-6">
            <order-stat :color="'green'" :value="publishedProductCount*1" :title="'Published Products'"
                        :icon="'fa fa-shopping-cart'"></order-stat>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h2 v-cloak>Products Count By Category</h2>

            <div id="chart-category"></div>
        </div>
    </div>
    @include('backend.merchant.partials.dashboardstat')
@endsection

@section('scripts')
    <script src="/js/inventory/dashboard.js"></script>
@endsection