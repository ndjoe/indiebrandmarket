@extends('backend.base')

@section('title')
    <title>Staff Inventori Produk</title>
@endsection

@section('menuuser')
    @include('backend.inventory.partials.usermenu')
@endsection

@section('menu')
    @include('backend.inventory.partials.menu')
@endsection

@section('morecss')
    <link rel="stylesheet" href="/css/inventory/products.css">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="inventory">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Inventori Produk
                        </span>
                    </div>
                    <div class="actions">
                        <div class="portlet-input input-inline input-small">
                            <select v-model="selected" class="form-control" style="width: 100%">
                                <option value="0">All</option>
                                <option v-for="option in options" v-bind:value="option.value">
                                    @{{ option.text }}
                                </option>
                            </select>
                        </div>
                        <div class="portlet-input input-inline input-small ">
                            <div class="input-icon right">
                                <i class="icon-magnifier"></i>
                                <input type="text" v-model="query" debounce="500"
                                       class="form-control form-control-solid"
                                       placeholder="search...">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <a href="/products/create" class="btn btn-primary">Add New</a>
                        </div>
                        <div class="col-md-4">
                            <nav>
                                <ul class="pager" style="margin: 0">
                                    <li><a class="btn btn-default" v-on:click="prev"
                                           v-bind:class="{'disabled': isFirst}">Previous</a>
                                    </li>
                                    <li><a class="btn btn-default" v-on:click="next"
                                           v-bind:class="{'disabled': isLast}">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-4">
                            <p v-cloak>Page - @{{ currentPage }} | Total Page - @{{ totalPage }}</p>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nama Produk</th>
                                <th>Kategori</th>
                                <th>SKU</th>
                                <th>Discount</th>
                                <th>Harga</th>
                                <th>Pemilik</th>
                                <th>Dipublish</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="entry in data" v-cloak>
                                <td>
                                    @{{ entry.id }}
                                </td>
                                <td>
                                    @{{ entry.name | capitalize }}
                                </td>
                                <td>
                                    @{{ entry['subcategory'].text | capitalize }}
                                </td>
                                <td>
                                    <p v-for="item in entry['items']">
                                        @{{ item.size.text | uppercase }} - @{{ item.qty }}
                                    </p>
                                    Total - @{{ entry['items'] | sumArray }}
                                </td>
                                <td>
                                    <template v-if="!isDiscounted(entry)">
                                        Sedang tidak didiskon
                                    </template>
                                    <template v-if="isDiscounted(entry)">
                                        Persen discount: @{{ entry['discount']['value'] }} % <br>
                                        Expired at: @{{ entry['discount']['expired_at'] | formatDate }}<br>
                                    </template>
                                </td>
                                <td>
                                    @{{ entry['price'] | formatMoney }}
                                </td>
                                <td>
                                    @{{ entry['author'].username }}
                                </td>
                                <td>
                                    <template v-if="entry['published_at'] == null" v-cloak>
                                        <span class="label label-sm label-warning">Belum Dipublish</span>
                                    </template>
                                    <template v-if="entry['published_at'] !== null" v-cloak>
                                        <span class="label label-sm label-success">@{{ entry['published_at'] }}</span>
                                    </template>
                                </td>
                                <td>
                                    <button class="btn blue btn-sm" v-on:click="showMItem(entry)">
                                        Add Item
                                    </button>
                                    <button class="btn blue btn-sm" v-on:click="showDItemForm(entry)">
                                        Decrement Item
                                    </button>
                                    <button class="btn blue btn-sm" v-on:click="showMDiscount(entry)">
                                        Set Discount
                                    </button>
                                    <template v-if="entry['published_at'] == null" v-cloak>
                                        <button class="btn blue btn-sm" v-on:click="publish(entry)">
                                            Publish
                                        </button>
                                    </template>
                                    <template v-if="entry['published_at'] !== null" v-cloak>
                                        <button class="btn blue btn-sm" v-on:click="unpublish(entry)">
                                            Unpublish
                                        </button>
                                    </template>
                                    <a href="/products/@{{ entry['id'] }}" class="btn blue btn-sm">
                                        Edit
                                    </a>
                                    <button class="btn blue btn-sm" v-on:click="viewItem(entry)">
                                        View
                                    </button>
                                    <button class="btn blue btn-sm" v-on:click="deleteItem(entry)">
                                        Delete
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <modal :show.sync="showModalItemForm" v-cloak>
        <div class="modal-header">
            <button type="button" v-on:click="showModalItemForm = false" class="close" data-dismiss="modal"
                    aria-hidden="true"></button>
            <h4 class="modal-title">Form Input Item</h4>
        </div>
        <form v-on:submit="submitItem">
            <div class="modal-body">
                <div class="form-group">
                    <label>Id Produk</label>
                    <input type="text" v-model="formItem.productId" disabled class="form-control">
                </div>
                <div class="form-group">
                    <label>Size</label>
                    <select class="form-control" v-selectize="formItem.sizeId"
                            options="listSize" settings="{create:true}">
                        <option value="">Pick Size</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>QTY</label>
                    <input type="text" class="form-control" v-model="formItem.qty">
                </div>
                <div class="form-group">
                    <label>Info Sizing Produk</label>
                    <textarea cols="20" rows="5" class="form-control" v-model="formItem.desc"
                              placeholder="masukkan keterangan ukuran produk ini"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal"
                        v-on:click="showModalItemForm = false">Close
                </button>
                <button type="submit" class="btn green">Submit</button>
            </div>
        </form>
    </modal>
    <modal :show.sync="showModalDiscountForm" v-cloak>
        <div class="modal-header">
            <button type="button" v-on:click="showModalDiscountForm = false" class="close" data-dismiss="modal"
                    aria-hidden="true"></button>
            <h4 class="modal-title">Form Add Discount</h4>
        </div>
        <form v-on:submit="submitDiscount">
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label">Nilai Discount</label>
                    <input v-numberpicker="formDiscount.percentDiscount" postfix="%" max="100" type="text"
                           class="form-control">
                </div>
                <div class="form-group">
                    <label class="control-label">Expired at</label>
                    <input type="text" class="form-control" v-datepicker="formDiscount.expiredDiscount">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal"
                        v-on:click="showModalDiscountForm = false">Close
                </button>
                <button type="submit" class="btn green">Submit</button>
            </div>
        </form>
    </modal>
    <modal :show.sync="showDecrementItemForm" v-cloak>
        <div class="modal-header">
            <button type="button" v-on:click="showDecrementItemForm = false" class="close" data-dismiss="modal"
                    aria-hidden="true"></button>
            <h4 class="modal-title">Form Decrement Item</h4>
        </div>
        <form v-on:submit="decrementItem">
            <div class="modal-body">
                <div class="form-group">
                    <label>Id Produk</label>
                    <input type="text" v-model="formDecrement.productId" disabled class="form-control">
                </div>
                <div class="form-group">
                    <label>Size</label>
                    <select class="form-control" v-model="formDecrement.sizeId">
                        <option value="">Pick Size</option>
                        <option v-for="l in formDecrement.listSize" v-bind:value="l.size.id" v-if="l.qty > 0">
                            @{{ l.size.text | uppercase }}
                        </option>
                    </select>
                </div>
                <div class="form-group">
                    <label>QTY</label>
                    <input type="text" class="form-control" v-model="formDecrement.qty">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal"
                        v-on:click="showDecrementItemForm = false">Close
                </button>
                <button type="submit" class="btn green">Submit</button>
            </div>
        </form>
    </modal>
    <modal :show.sync="showViewItemModal" v-cloak>
        <div class="modal-header">
            <button type="button" v-on:click="showViewItemModal = false" class="close" data-dismiss="modal"
                    aria-hidden="true"></button>
            <h4 class="modal-title">Detail @{{ detailProduct.name }}</h4>
        </div>
        <div class="modal-body">
            <h5>Gambar Produk</h5>

            <div class="row">
                <div class="col-md-3" v-for="im in detailProduct.images">
                    <img :src="'/images/catalog/'+im.name" width="75" height="100" v-if="im.name !== ''">
                </div>
            </div>

            <h5>Info Produk</h5>

            <div class="row">
                <div class="col-md-6">
                    Harga: <p> @{{ detailProduct.price | formatMoney }}</p>
                </div>
                <div class="col-md-6">
                    Kategori: <p>@{{ detailProduct.category.text | uppercase }}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    Subkategori: <p>@{{ detailProduct.subcategory.text | uppercase }}</p>
                </div>
                <div class="col-md-6">
                    Warna: <p>@{{ detailProduct.color.text | uppercase }}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    Deskripsi Produk: <p>@{{ detailProduct.description }}</p>
                </div>
            </div>
            <h5>Info Stok</h5>

            <div class="row">
                <div class="col-md-12">
                    Total - @{{ detailProduct.items | sumArray }}
                    <p v-for="item in detailProduct.items">
                        @{{ item.size.text | uppercase }} : @{{ item.qty }}<br>
                        Keterangan: @{{ item.sizing_info }}
                    </p>
                </div>
            </div>
            <h5>Info Diskon</h5>

            <div class="row">
                <div class="col-md-6">
                    Nilai Diskon: <p>@{{ detailProduct.discount.value }}</p>
                </div>
                <div class="col-md-6">
                    Diskon Berakhir Pada: <p>@{{ detailProduct.discount.expired_at }}</p>
                </div>
            </div>
            <h5>Berat Product</h5>

            <div class="row">
                <div class="col-md-12">
                    Berat: <p>@{{ detailProduct.weight }}</p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn dark btn-outline" data-dismiss="modal"
                    v-on:click="showViewItemModal = false">Cancel
            </button>
            <template v-if="detailProduct.published_at == null" v-cloak>
                <button class="btn blue btn-sm" v-on:click="publish(detailProduct)">
                    Publish
                </button>
            </template>
            <template v-else v-cloak>
                <button class="btn blue btn-sm" v-on:click="unpublish(detailProduct)">
                    Unpublish
                </button>
            </template>
        </div>
    </modal>
    @include('backend.merchant.partials.modal')
@endsection

@section('scripts')
    <script src="/js/inventory/products.js"></script>
@endsection