<html>
<head>
    <title>Login Staff Inventory</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link href="/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/login-2.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/assets/css/components.css">
    <link rel="stylesheet" href="/assets/css/plugins.css">
    <link rel="stylesheet" href="/assets/css/layout.css">
</head>
<body class="login">
<div class="content">
    <form action="/login" method="post" class="login-form">
        {{ csrf_field() }}
        <div class="form-title">
            <span class="form-title">Inventory Login</span>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Username</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off"
                   placeholder="Username" name="username"/>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off"
                   placeholder="Password" name="password"/>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-primary btn-block uppercase">Login</button>
        </div>
    </form>
</div>
<script src="/assets/plugins/jquery.min.js"></script>
<script src="/assets/plugins/jquery-migrate.min.js"></script>
<script src="/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/plugins/jquery.blockui.min.js"></script>
<script src="/assets/plugins/jquery.cokie.min.js"></script>
<script src="/assets/plugins/uniform/jquery.uniform.min.js"></script>
<script src="/assets/scripts/metronic.js"></script>
<script src="/assets/scripts/layout.js"></script>
<script>
    $(function () {
        Metronic.init();
        Layout.init();
    });
</script>
</body>
</html>