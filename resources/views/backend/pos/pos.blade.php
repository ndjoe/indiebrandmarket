@extends('backend.base')

@section('title')
    <title>Point Of Sales</title>
@endsection

@section('menuuser')
    @include('backend.pos.partials.usermenu')
@endsection

@section('menu')
    @include('backend.pos.partials.menu')
@endsection

@section('morecss')
    <link rel="stylesheet" href="/assets/css/cart.css">
    <link rel="stylesheet" href="/assets/plugins/bootstrap-switch/css/bootstrap-switch.css">
    <link rel="stylesheet" href="/assets/css/invoice.css">
    <style>
        div.checker {
            display: none !important;
        }
    </style>
@endsection

@section('content')
    <div class="row" id="pos">
        <div class="col-md-6">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            List Produk
                        </span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group" id="brand-products" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default" v-for="cat in optionsCategory">
                                    <div class="panel-heading text-center" role="tab" id="top">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#brand-products"
                                               href="#@{{ cat['text'] }}-content" aria-expanded="true"
                                               aria-controls="top-content"
                                               v-on:click="setCategory(cat.value)" class="btn btn-block" v-cloak>
                                                @{{ cat['text'] }}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="@{{ cat['text'] }}-content" class="panel-collapse collapse"
                                         role="tabpanel"
                                         aria-labelledby="top">
                                        <div class="panel-body">
                                            <div class="row margin-bottom-10">
                                                <div class="col-md-3" v-for="entry in data">
                                                    <div class="product-item">
                                                        <div class="pi-img-wrapper">
                                                            <a href="javascript:;" v-on:click="showPicker(entry)">
                                                                <img v-bind:src="'/images/catalog/'+entry.images[0].name"
                                                                     class="img-responsive" alt="@{{ entry['name'] }}">
                                                            </a>
                                                        </div>
                                                        <h4 v-cloak>@{{ entry['name'] }}</h4>

                                                        <h5 v-cloak>IDR @{{ entry['price']*1 }}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <modal :show.sync="showModalPicker" v-cloak>
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"
                            v-on:click="showModalPicker = false">&times;</button>
                    <h4 class="modal-title">Pick Size and Qty</h4>
                </div>
                <form v-on:submit="submitProduct">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">Size</label>
                            <select v-model="formItem.size" class="form-control">
                                <option value="0">Pick Size</option>
                                <option v-for="l in formItem.listSize" v-bind:value="l.value">
                                    @{{ l.text }} - Sisa barang @{{ l.qty }}
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Qty</label>
                            <input type="text" class="form-control" v-model="formItem.qty">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" v-on:click="showModalPicker = false" class="btn btn-warning">Cancel
                        </button>
                        <template v-if="formItem.listSize.length > 0">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </template>
                        <template v-else>
                            <button class="btn btn-primary" disabled>Sold Out</button>
                        </template>
                    </div>
                </form>
            </modal>
        </div>
        <div class="col-md-6">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Point Of Sales App
                        </span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form v-on:submit="submitBarcode">
                                <div class="form-group">
                                    <input type="text" v-model="barcode" class="form-control" autofocus>
                                </div>
                            </form>
                        </div>
                    </div>
                    <table class="table table-hover table-condensed table-cart">
                        <thead>
                        <tr>
                            <th style="width:50%">Product</th>
                            <th style="width:10%">Price</th>
                            <th style="width:8%">Quantity</th>
                            <th style="width:22%" class="text-center">Subtotal</th>
                            <th style="width:10%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="entry in items" v-cloak>
                            <td data-th="Product">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <img src="/images/catalog/@{{ entry['options']['image'] }}"
                                             class="img-responsive">
                                    </div>
                                    <div class="col-sm-10">
                                        <h4>@{{ entry['name'] }} - @{{ entry['options']['sizeText'] }}</h4>
                                    </div>
                                </div>
                            </td>
                            <td data-th="Price">
                                @{{ entry['price']*1 }}
                            </td>
                            <td data-th="Quantity">
                                @{{ entry['qty'] }}
                            </td>
                            <td data-th="Subtotal" class="text-right">
                                @{{ entry | subTotalPrice  }}
                            </td>
                            <td data-th="">
                                <button class="btn btn-danger btn-sm" v-on:click="deleteItem(entry)">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="5">
                                <strong class="pull-right">
                                    Total - @{{ items | totalPrice }}
                                </strong>
                            </td>
                            <td data-th="">

                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                Payment Method
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" v-checkbox="paymentEdc" data-on-text="EDC" data-off-text="CASH">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <button class="btn blue btn-block" v-on:click="showModal">
                                    Submit
                                </button>
                                <button class="btn yellow btn-block" v-on:click="resetCart">
                                    Reset
                                </button>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <modal :show.sync="showModalCash" v-cloak>
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"
                                v-on:click="showModalCash = false">&times;</button>
                        <h4 class="modal-title">Finalize Cash Payment</h4>
                    </div>
                    <form v-on:submit="submitCash">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">Harga Total</label>
                                <input type="text" class="form-control" disabled v-model="formCashItem.orderPrice"
                                       number>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Pembayaran</label>
                                <input type="text" class="form-control" v-model="formCashItem.cash" number>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Kembalian</label>
                                <input type="text" class="form-control" disabled v-model="changes" number>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nama Pembeli</label>
                                <input type="text" class="form-control" v-model="formCashItem.nama">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" v-on:click="showModalCash = false" class="btn btn-warning">Cancel
                            </button>
                            <template v-if="isValid">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </template>
                            <template v-else>
                                <button type="button" class="btn btn-primary" disabled>
                                    Submit
                                </button>
                            </template>
                        </div>
                    </form>
                </modal>
                <modal :show.sync="showModalEdc" v-cloak>
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"
                                v-on:click="showModalEdc = false">&times;</button>
                        <h4 class="modal-title">Finalize EDC Payment</h4>
                    </div>
                    <form v-on:submit="submitEdc">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">Harga Total</label>
                                <input type="text" class="form-control" disabled v-model="formEdcItem.orderPrice">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Bank</label>
                                <input type="text" class="form-control" v-model="formEdcItem.bank">
                            </div>
                            <div class="form-group">
                                <label class="control-label">No. Ref</label>
                                <input type="text" class="form-control" v-model="formEdcItem.noref">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Card Number</label>
                                <input type="text" class="form-control" v-model="formEdcItem.cardNumber" number>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Expiration Date</label>

                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-xs"
                                                   v-model="formEdcItem.expirationMonth" placeholder="Month" number>
                                        </div>
                                    </div>
                                    <div class="col-md-2">

                                        <div class="form-group">
                                            <input type="text" class="form-control input-xs"
                                                   v-model="formEdcItem.expirationYear" placeholder="Year" number>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nama Pembeli</label>
                                <input type="text" class="form-control" v-model="formEdcItem.nama">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" v-on:click="showModalEdc = false" class="btn btn-warning">Cancel
                            </button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </modal>
                <modal :show.sync="showModalInvoice" v-cloak>
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"
                                v-on:click="showModalInvoice = false">&times;</button>
                        <h4 class="modal-title">faktur transaksi</h4>
                    </div>
                    <div class="modal-body">
                        @include('backend.pos.partials.invoiceTemplate')
                    </div>
                    <div class="modal-footer">
                        <button type="button" v-on:click="showModalInvoice = false" class="btn btn-warning">Cancel
                        </button>
                        <button class="btn btn-primary" v-on:click="printInvoice(modalItem.viewItem.id)">
                            Print
                        </button>
                    </div>
                </modal>
            </div>
        </div>
    </div>
    @include('backend.merchant.partials.modal')
@endsection

@section('scripts')
    <script>
        Vue.filter('totalPrice', function (data) {
            var total = 0;

            data.forEach(function (item) {
                var subtotal = item.price * item.qty * 1;
                total += subtotal;
            });

            return total;
        });
        Vue.filter('subTotalPrice', function (data) {
            return data['price'] * data['qty'];
        });
        Vue.directive('checkbox', {
            twoWay: true,
            bind: function () {
                var self = this;
                $(this.el).bootstrapSwitch().on('switchChange.bootstrapSwitch', function (event, state) {
                    self.set(state);
                });
            },
            update: function (value) {
                $(this.el).bootstrapSwitch('state', value);
            }
        });
        Vue.component('modal', {
            template: '#modal',
            props: ['show']
        });
        var vm = new Vue({
            el: '#app',
            data: {
                items: [],
                barcode: '',
                totalPrice: 0,
                paymentEdc: true,
                showModalCash: false,
                showModalEdc: false,
                showModalPicker: false,
                formCashItem: {
                    orderPrice: 0,
                    cash: 0,
                    changes: 0,
                    nama: ''
                },
                formEdcItem: {
                    orderPrice: 0,
                    bank: '',
                    noref: '',
                    cardNumber: '',
                    expirationMonth: '',
                    expirationYear: '',
                    nama: ''
                },
                formItem: {
                    product: 0,
                    size: 0,
                    qty: 0,
                    listSize: []
                },
                optionsCategory: [],
                category: 0,
                data: [],
                showModalInvoice: false,
                invoiceOrder: 0,
                invoiceCash: 0,
                modalItem: {
                    viewItem: {},
                    nama: '',
                    change: 0
                }
            },
            methods: {
                submitBarcode: function (e) {
                    e.preventDefault();
                    var self = this;
                    var formData = new FormData;
                    formData.append('uid', this.barcode);
                    $.ajax({
                        url: '/pos/api/items',
                        method: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false
                    }).done(function (data) {
                        self.items = data;
                        self.barcode = '';
                    });
                },
                deleteItem: function (entry) {
                    var self = this;
                    var formData = new FormData;
                    formData.append('id', entry.id);
                    $.ajax({
                        url: '/pos/api/items/delete',
                        method: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false
                    }).done(function (data) {
                        self.items = data;
                    });
                },
                submitCart: function () {
                    var self = this;
                    $.get('/admin/api/pos/purchase').done(function () {
                        self.items = [];
                    });
                },
                resetCart: function () {
                    var self = this;
                    $.get('/pos/api/reset').done(function () {
                        self.items = [];
                    });
                },
                getItems: function () {
                    var self = this;
                    $.get('/pos/api/items').done(function (data) {
                        self.items = data;
                    });
                },
                showModal: function () {
                    console.log(this.paymentEdc);
                    this.paymentEdc ? this.showEdc() : this.showCash();
                },
                showPicker: function (data) {
                    var self = this;
                    this.formItem.product = data['id'];
                    this.formItem.size = 0;
                    this.formItem.qty = 0;
                    this.formItem.listSize = [];
                    data['items'].forEach(function (s) {
                        var arraytemp = [];
                        if (s.qty > 0) {
                            arraytemp['value'] = s['size']['id'];
                            arraytemp['text'] = s['size']['text'];
                            arraytemp['qty'] = s['qty'];
                            self.formItem.listSize.push(arraytemp);
                        }
                    });
                    this.showModalPicker = true;
                },
                submitProduct: function (e) {
                    e.preventDefault();
                    var self = this;
                    var formData = new FormData;
                    formData.append('productId', this.formItem.product);
                    formData.append('sizeId', this.formItem.size);
                    formData.append('qty', this.formItem.qty);
                    $.ajax({
                        url: '/pos/api/items/product',
                        method: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false
                    }).done(function (data) {
                        self.items = data;
                        self.showModalPicker = false;
                        self.formItem.product = 0;
                        self.formItem.size = 0;
                        self.formItem.qty = 0;
                    });
                },
                showEdc: function () {
                    var self = this;
                    $.get('/pos/api/cart/total').done(function (data) {
                        self.formEdcItem.orderPrice = data;
                    });
                    this.formCashItem.bank = '';
                    this.formCashItem.noref = '';
                    this.formCashItem.cardNumber = '';
                    this.formCashItem.expirationMonth = '';
                    this.formCashItem.expirationYear = '';
                    this.showModalEdc = true;
                },
                showCash: function () {
                    var self = this;
                    $.get('/pos/api/cart/total').done(function (data) {
                        self.formCashItem.orderPrice = data;
                    });
                    this.formCashItem.cash = 0;
                    this.showModalCash = true;
                },
                getCartTotal: function () {
                    var total = 0;
                    $.get('/pos/api/cart/total').done(function (data) {
                        total = data;
                    });
                    return total;
                },
                submitEdc: function (e) {
                    e.preventDefault();
                    var self = this;
                    var formData = new FormData;
                    formData.append('bank', this.formEdcItem.bank);
                    formData.append('noref', this.formEdcItem.noref);
                    formData.append('cardNumber', this.formEdcItem.cardNumber * 1);
                    formData.append('expirationDate', this.formEdcItem.expirationMonth * 1 + '-' + this.formEdcItem.expirationYear * 1);
                    formData.append('nama', this.formEdcItem.nama);
                    $.ajax({
                        url: '/pos/api/payment/edc',
                        method: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false
                    }).done(function (data) {
                        self.items = [];
                        self.getData();
                        $.get('/pos/api/order/' + data.orderId)
                                .done(function (dataOrder) {
                                    self.showModalEdc = false;
                                    self.modalItem.viewItem = dataOrder.order;
                                    self.modalItem.nama = self.formEdcItem.nama;
                                    self.modalItem.change = 0;
                                    self.formEdcItem.bank = '';
                                    self.formEdcItem.noref = '';
                                    self.formEdcItem.orderPrice = '';
                                    self.formEdcItem.cardNumber = '';
                                    self.formEdcItem.expirationMonth = '';
                                    self.formEdcItem.expirationYear = '';
                                    self.formEdcItem.nama = '';
                                    self.showModalInvoice = true;
                                });
                    });
                },
                submitCash: function (e) {
                    e.preventDefault();
                    var self = this;

                    var formData = new FormData;
                    if (this.formCashItem.cash < this.formCashItem.orderPrice) {
                        return;
                    }
                    formData.append('cash', this.formCashItem.cash * 1);
                    formData.append('nama', this.formCashItem.nama);
                    $.ajax({
                        url: '/pos/api/payment/cash',
                        method: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false
                    }).done(function (data) {
                        self.items = [];

                        self.getData();
                        $.get('/pos/api/order/' + data.orderId)
                                .done(function (dataOrder) {
                                    self.modalItem.viewItem = dataOrder.order;
                                    self.modalItem.nama = self.formCashItem.nama;
                                    self.modalItem.change = self.formCashItem.orderPrice - self.formCashItem.cash;
                                    self.showModalCash = false;
                                    self.formCashItem.cash = 0;
                                    self.formCashItem.orderPrice = 0;
                                    self.formCashItem.nama = 0;
                                    self.showModalInvoice = true;
                                });
                    });

                },
                getOptions: function () {
                    var self = this;
                    $.get('/pos/api/options/merchant/' + '{{ \Auth::user()->profileable->merchant_id }}')
                            .done(function (resp) {
                                self.optionsCategory = resp.categories;
                            });
                },
                getData: function (page) {
                    if (!page) {
                        page = 1;
                    }
                    var self = this;
                    $.get('/pos/api/products/merchant/' + '{{ Auth::user()->profileable->merchant_id }}', {
                        category: this.category,
                        page: page
                    }).done(function (data) {
                        self.data = data.data;
                    });
                },
                setCategory: function (id) {
                    this.category = !isNaN(id) ? id : id.value;
                    console.log(this.category);
                    this.currentPage = 1;
                    this.getData();
                },
                confirmPayment: function (e) {
                    e.preventDefault();
                    this.showModalCash = false;
                    this.modalInvoice = true;
                },
                printInvoice: function (orderId) {
                    var newTab = window.open('', '_blank');
                    newTab.location = "http://{{ Auth::user()->profileable->merchant->username }}.originalbrands.localapp/pos/faktur/" + orderId;
                }
            },
            watch: {},
            ready: function () {
                App.init();
                Layout.init();
                this.getItems();
                this.getOptions();
                this.getData();
            },
            computed: {
                changes: function () {
                    return this.formCashItem.orderPrice > this.formCashItem.cash ?
                            0 : (this.formCashItem.cash - this.formCashItem.orderPrice) * 1;
                },
                isValid: function () {
                    return this.formCashItem.orderPrice <= this.formCashItem.cash;
                }
            }
        });
    </script>
@endsection