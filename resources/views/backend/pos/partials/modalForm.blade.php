<template id="modalItemForm">
    <div class="modal fade" v-show="show" v-transition="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <content select=".modal-header">
                    <div class="modal-header">
                        default
                    </div>
                </content>
                <form v-on="submit: submitAction">
                    <content select=".modal-body">
                        <div class="modal-body">
                            default
                        </div>
                    </content>
                    <content select=".modal-footer">
                        <div class="modal-footer">
                            default
                        </div>
                    </content>
                </form>
            </div>
        </div>
    </div>
</template>