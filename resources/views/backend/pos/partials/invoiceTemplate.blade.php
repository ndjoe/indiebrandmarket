<div class="invoice-content-2 bordered">
    <div class="row invoice-head">
        <div class="col-md-7 col-xs-6">
            <div class="invoice-logo">
                <img src="/assets/img/logo-original-brands.png" class="img-responsive" alt="">
            </div>
        </div>
        <div class="col-md-5 col-xs-6">
            <div class="company-address">
                <span class="bold uppercase">Originalbrands.id</span>
                <br> 25, Lorem Lis Street, Orange C
                <br> California, US
                <br>
                <span class="bold">T</span> 1800 123 456
                <br>
                <span class="bold">E</span> support@keenthemes.com
                <br>
                <span class="bold">W</span> www.keenthemes.com
            </div>
        </div>
    </div>
    <div class="row invoice-cust-add">
        <div class="col-xs-6">
            <h2 class="invoice-title uppercase">Customer</h2>

            <p class="invoice-desc">
                Nama: @{{ modalItem.nama }}
            </p>
        </div>
        <div class="col-xs-6">
            <h2 class="invoice-title uppercase">Date</h2>

            <p class="invoice-desc">@{{ modalItem.viewItem.created_at }}</p>
        </div>
    </div>
    <div class="row invoice-body">
        <div class="col-xs-12 table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th class="invoice-title uppercase text-center">Nama Produk</th>
                    <th class="invoice-title uppercase text-center">Brand</th>
                    <th class="invoice-title uppercase text-center">Size</th>
                    <th class="invoice-title uppercase text-center">Qty</th>
                    <th class="invoice-title uppercase text-center">Unit Price</th>
                    <th class="invoice-title uppercase text-center">Subtotal</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="i in modalItem.viewItem.items">
                    <td class="text-center sbold">
                        @{{ i.product_item.product.name }}
                    </td>
                    <td class="text-center sbold">
                        @{{ i.product_item.product.author.nama }}
                    </td>
                    <td class="text-center sbold">@{{ i.product_item.size.text }}</td>
                    <td class="text-center sbold">@{{ i.qty }}</td>
                    <td class="text-center sbold">
                        IDR @{{ i.product_item.product.price | formatNumber }}
                    </td>
                    <td class="text-center sbold">
                        IDR @{{ i.qty * i.product_item.product.price | formatNumber }}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row invoice-subtotal">
        <div class="col-xs-4">
            <h2 class="invoice-title uppercase">#Order</h2>

            <p class="invoice-desc">@{{ modalItem.viewItem.order_code }}</p>
        </div>
        <div class="col-xs-4">
            <h2 class="invoice-title uppercase">Total</h2>

            <p class="invoice-desc grand-total">IDR @{{ modalItem.viewItem.grossTotal | formatNumber }}</p>
        </div>
        <div class="col-xs-4">
            <h2 class="invoice-title uppercase">Change</h2>

            <p class="invoice-desc grand-total">IDR @{{ modalItem.change | formatNumber }}</p>
        </div>
    </div>
</div>