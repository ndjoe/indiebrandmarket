<html>
<head>
    <link rel="stylesheet" href="/assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/plugins/simple-line-icons/simple-line-icons.min.css">
    <link rel="stylesheet" href="/assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css">
    <link href="/assets/css/invoice.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/assets/css/components.css">
    <link rel="stylesheet" href="/assets/css/plugins.css">
    <link rel="stylesheet" href="/assets/css/layout.css">
</head>
<body class="page-header-fixed">
<div class="clearfix">
</div>
<div class="page-container page-container-bg-solid">
    <div class="container-fluid container-lf-space page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="invoice">
                    <div class="row invoice-logo">
                        <div class="col-xs-6 invoice-logo-space">
                            <img src="/assets/img/logo-original-brands.png" class="img-responsive"
                                 alt="" width="300px"/>
                        </div>
                        <div class="col-xs-6">
                            <p>
                                #5652256 / 28 Feb 2013 <span class="muted">
									Consectetuer adipiscing elit </span>
                            </p>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Item
                                    </th>
                                    <th class="hidden-480">
                                        Description
                                    </th>
                                    <th class="hidden-480">
                                        Quantity
                                    </th>
                                    <th class="hidden-480">
                                        Unit Cost
                                    </th>
                                    <th>
                                        Total
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        1
                                    </td>
                                    <td>
                                        Hardware
                                    </td>
                                    <td class="hidden-480">
                                        Server hardware purchase
                                    </td>
                                    <td class="hidden-480">
                                        32
                                    </td>
                                    <td class="hidden-480">
                                        $75
                                    </td>
                                    <td>
                                        $2152
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        2
                                    </td>
                                    <td>
                                        Furniture
                                    </td>
                                    <td class="hidden-480">
                                        Office furniture purchase
                                    </td>
                                    <td class="hidden-480">
                                        15
                                    </td>
                                    <td class="hidden-480">
                                        $169
                                    </td>
                                    <td>
                                        $4169
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        3
                                    </td>
                                    <td>
                                        Foods
                                    </td>
                                    <td class="hidden-480">
                                        Company Anual Dinner Catering
                                    </td>
                                    <td class="hidden-480">
                                        69
                                    </td>
                                    <td class="hidden-480">
                                        $49
                                    </td>
                                    <td>
                                        $1260
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        3
                                    </td>
                                    <td>
                                        Software
                                    </td>
                                    <td class="hidden-480">
                                        Payment for Jan 2013
                                    </td>
                                    <td class="hidden-480">
                                        149
                                    </td>
                                    <td class="hidden-480">
                                        $12
                                    </td>
                                    <td>
                                        $866
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="well">
                                <address>
                                    <strong>Trilogi Cipta Reswara, Inc.</strong><br/>
                                    ORIGINALBRANDS.ID<br/>
                                    Jln. Anyer No. 48, Bandung - Indonesia 40272,<br/>
                                    <abbr title="Phone">P:</abbr> (234) 145-1810
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/assets/plugins/jquery.min.js"></script>
<script src="/assets/plugins/jquery-migrate.min.js"></script>
<script src="/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script src="/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="/assets/plugins/jquery.blockui.min.js"></script>
<script src="/assets/plugins/jquery.cokie.min.js"></script>
<script src="/assets/plugins/uniform/jquery.uniform.min.js"></script>
<script src="/assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="/assets/scripts/metronic.js"></script>
<script src="/assets/scripts/layout.js"></script>
<script>
    $(function () {
        Metronic.init();
        Layout.init();
    });
</script>
</body>
</html>