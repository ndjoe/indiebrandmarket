@extends('backend.base')

@section('title')
    <title>Originalbrands Orders</title>
@endsection

@section('menu')
    @include('backend.merchant.partials.menu')
@endsection

@section('menuuser')
    @include('backend.merchant.partials.userMenu')
@endsection

@section('notif')
    @include('backend.merchant.partials.notif')
@endsection

@section('header-title')
    Merchant Center
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="orders">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Orders
                        </span>
                    </div>
                    <div class="actions">
                        <div class="portlet-input input-inline input-small">
                            {{--<select v-model="selected" class="form-control" style="width: 100%">--}}
                            {{--<option value="0">All</option>--}}
                            {{--<option value="1">Unaccepted</option>--}}
                            {{--<option value="2">Accepted</option>--}}
                            {{--</select>--}}
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-4">
                            <nav>
                                <ul class="pager" style="margin: 0">
                                    <li><a class="btn btn-default" v-on:click="prev"
                                           v-bind:class="isFirst?'disabled':''">Previous</a>
                                    </li>
                                    <li><a class="btn btn-default" v-on:click="next"
                                           v-bind:class="isLast?'disabled':''">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-4">
                            <p v-cloak>Page - @{{ currentPage }} | Total Page - @{{ totalPage }}</p>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Id Order Barang</th>
                                <th>Nomor Order</th>
                                <th>Detail Barang</th>
                                <th>Total Harga Beli</th>
                                <th>Tipe</th>
                                <th>Komisi IBM</th>
                                <th>Komisi Merchant</th>
                                <th>Status Accept</th>
                                <th>Status Order</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="entry in data" v-cloak>
                                <td>
                                    @{{ entry['id'] }}
                                </td>
                                <td>
                                    @{{ entry.order.order_code }}
                                </td>
                                <td>
                                    <p>
                                        Nama Barang: @{{ entry['product_item']['product']['name'] }}<br>
                                        Size: @{{ entry['product_item']['size']['text'] }}<br>
                                        Pemilik: @{{ entry['product_item']['product']['author']['username'] }}<br>
                                        Qty: @{{ entry['qty'] }} <br>
                                    </p>
                                </td>
                                <td>
                                    IDR @{{ entry['adjusment'] | formatNumber }}
                                </td>
                                <td>
                                    @{{ entry['order']['type'] }}
                                </td>
                                <td>
                                    IDR @{{ entry | komisiIbm | formatNumber }}
                                </td>
                                <td>
                                    IDR @{{ entry | komisiMerchant | formatNumber }}
                                </td>
                                <td>
                                    <span v-if="entry['accepted_at'] == null">Not Accepted</span>
                                    <span v-if="entry['accepted_at'] !== null">Accepted at: @{{ entry['accepted_at'] }}</span>
                                </td>
                                <td>
                                    <span v-if="entry.order.confirmed_at == null">Not Confirmed</span>
                                    <span v-else>Confirmed at: @{{ entry.order.confirmed_at }}</span>
                                </td>
                                <td>
                                    <button class="btn btn-primary"
                                            v-if="entry['accepted_at'] !== null" disabled>
                                        Accepted
                                    </button>
                                    <button class="btn btn-primary"
                                            v-if="entry['accepted_at'] === null" v-on:click="acceptItem(entry)">
                                        Accept
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/merchant/onlineOrders.js"></script>
@endsection