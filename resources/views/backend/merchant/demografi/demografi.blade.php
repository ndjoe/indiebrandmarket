@extends('backend.base')

@section('title')
    <title>Demografi Pembeli</title>
@endsection

@section('menu')
    @include('backend.merchant.partials.menu')
@endsection

@section('menuuser')
    @include('backend.merchant.partials.userMenu')
@endsection

@section('morecss')
    <link rel="stylesheet" href="/css/merchant/demografi.css">
@endsection

@section('header-title')
    Merchant Center
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="orders">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Demografi Pembeli
                        </span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row margin-bottom-10">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Umur Pembeli</h3>

                                    <div id="brand-chart-age"></div>
                                </div>
                                <div class="col-md-6">
                                    <h3>Gender Pembeli</h3>

                                    <div id="brand-chart-gender"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/merchant/demografi.js"></script>
@endsection