@extends('backend.base')

@section('title')
    <title>Coupons</title>
@endsection

@section('menuuser')
    @include('backend.merchant.partials.userMenu')
@endsection

@section('menu')
    @include('backend.merchant.partials.menu')
@endsection

@section('morecss')
    <link rel="stylesheet" href="/css/merchant/coupons.css">
@endsection

@section('notif')
    @include('backend.merchant.partials.notif')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="inventory">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Coupons
                        </span>
                    </div>
                </div>
                <div class="portlet-body" v-cloak>
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <button class="btn btn-primary" v-on:click="showModalAdd">
                                Add New Random Code
                            </button>
                            <button class="btn btn-primary" v-on:click="showModalAddCustomCodeForm = true">
                                Add new Custom Code
                            </button>
                        </div>
                        <div class="col-md-4">
                            <nav>
                                <ul class="pager" style="margin: 0">
                                    <li><a class="btn btn-default" v-on:click="prev"
                                           v-bind:class="{'disabled': isFirst}">Previous</a>
                                    </li>
                                    <li><a class="btn btn-default" v-on:click="next"
                                           v-bind:class="{'disabled': isLast}">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-4">
                            <p>Page - @{{ currentPage }} | Total Page - @{{ totalPage }}</p>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Milik</th>
                                <th>Value</th>
                                <th>Minimum Order</th>
                                <th>Expired at</th>
                                <th>Status Voucher</th>
                                <th>Behaviour</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="entry in data" v-cloak>
                                <td>
                                    @{{ entry.id }}
                                </td>
                                <td>
                                    @{{ entry['code'] }}
                                </td>
                                <td>
                                    <template v-if="entry['brand_id'] > 0">
                                        @{{ entry['brand']['username'] }}
                                    </template>
                                    <template v-else>
                                        OriginalBrands
                                    </template>
                                </td>
                                <td>
                                    @{{ entry['value'] }}
                                </td>
                                <td>
                                    @{{ entry['minimum_order'] }}
                                </td>
                                <td>
                                    @{{ entry['expired_at'] }}
                                </td>
                                <td>
                                    <template v-if="isValid(entry)">
                                        Valid - sisa: @{{ entry.qty }}
                                    </template>
                                    <template v-if="!isValid(entry)">
                                        Tidak Valid
                                    </template>
                                </td>
                                <td>
                                    <template v-if="entry['notavailable'] !== discount">
                                        Berlaku Pada Semua Produk
                                    </template>
                                    <template v-if="entry['notavailable'] === discount">
                                        Tidak Berlaku Pada Produk Yang Didiskon
                                    </template>
                                </td>
                                <td>
                                    <button class="btn red btn-sm" v-on:click="deleteCoupon(entry)">
                                        Delete
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <modal :show.sync="showModalAddForm" v-cloak>
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"
                                v-on:click="showModalAddForm = false">&times;</button>
                        <h4 class="modal-title">Add New Coupon</h4>
                    </div>
                    <form v-on:submit="submitAddCoupon">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">Coupon Value</label>
                                <input type="text" class="form-control" v-model="formAdd.value">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Minimum Order</label>
                                <input type="text" class="form-control" v-model="formAdd.minimum">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Expired At</label>

                                <div class="row">
                                    <input type="text" class="form-control" v-datepicker="formAdd.expired">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Setting</label>
                                <select v-model="formAdd.setting" class="form-control">
                                    <option value="none">Berlaku Untuk Semua Produk</option>
                                    <option value="discount">Hanya Berlaku Untuk Produk Yang Tidak didiskon</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Jumlah Generate</label>
                                <input type="text" v-model="formAdd.qty" class="form-control">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" v-on:click="showModalAddForm = false" class="btn btn-warning">Cancel
                            </button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </modal>
                <modal :show.sync="showModalAddCustomCodeForm" v-cloak>
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"
                                v-on:click="showModalAddCustomCodeForm = false">&times;</button>
                        <h4 class="modal-title">Add New Coupon</h4>
                    </div>
                    <form v-on:submit="submitAddCustomCoupon">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="" class="control-label">Kode Voucher</label>
                                <input type="text" class="form-control" v-model="formCustom.kode">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Coupon Value</label>
                                <input type="text" class="form-control" v-model="formCustom.value">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Minimum Order</label>
                                <input type="text" class="form-control" v-model="formCustom.minimum">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Expired At</label>

                                <div class="row">
                                    <input type="text" class="form-control" v-datepicker="formCustom.expired">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Setting</label>
                                <select v-model="formCustom.setting" class="form-control">
                                    <option value="none">Berlaku Untuk Semua Produk</option>
                                    <option value="discount">Hanya Berlaku Untuk Produk Yang Tidak didiskon</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Jumlah Generate</label>
                                <input type="text" v-model="formCustom.qty" class="form-control">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" v-on:click="showModalAddCustomCodeForm = false"
                                    class="btn btn-warning">Cancel
                            </button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </modal>
            </div>
        </div>
    </div>
    @include('backend.merchant.partials.modal')
@endsection

@section('scripts')
    <script src="/js/merchant/coupons.js"></script>
@endsection