<script type="text/x-template" id="dashboardstat">
    <div class="dashboard-stat" v-bind:class="color">
        <div class="visual">
            <i v-bind:class="icon"></i>
        </div>
        <div class="details">
            <div class="number">
                <span v-text="value">0</span>
            </div>
            <div class="desc" v-text="title"></div>
        </div>
        <a class="more" v-bind:href="link" v-if="link.trim()"> View more
            <i class="m-icon-swapright m-icon-white"></i>
        </a>
    </div>
</script>