<script type="text/x-template" id="modal">
    <div class="modal fade in" tabindex="-1" role="basic" aria-hidden="true"
         v-bind:style="{display: show?'block':'none',paddingRight:'15px'}">
        <div class="modal-dialog">
            <div class="modal-content">
                <slot>
                    <div class="modal-header">
                        <button type="button" v-on:click="show = false" class="close" data-dismiss="modal"
                                aria-hidden="true"></button>
                        <h4 class="modal-title">Modal Title</h4>
                    </div>
                    <div class="modal-body"> Modal body goes here</div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal"
                                v-on:click="show = false">Close
                        </button>
                        <button type="button" class="btn green">Save changes</button>
                    </div>
                </slot>
            </div>
        </div>
    </div>
</script>