<li>
    <a href="/">
        Dashboard
    </a>
</li>
<li>
    <a href="/products">
        Produk & Inventori
    </a>
</li>
<li>
    <a href="/pos/orders">
        Point Of Sales Purchases
    </a>
</li>
<li>
    <a href="/demografi">
        Demografi
    </a>
</li>
<li>
    <a href="/orders">
        Original Brands Orders
    </a>
</li>
<li>
    <a href="/report">
        Original Brands Report
    </a>
</li>
<li>
    <a href="/pos/accounts">
        Pos Settings
    </a>
</li>
<li>
    <a href="/voucher">
        Voucher
    </a>
</li>
<li>
    <a href="/staffs">
        Staffs
    </a>
</li>
<li>
    <a href="/settings">
        Account settings
    </a>
</li>
