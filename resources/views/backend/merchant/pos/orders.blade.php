@extends('backend.base')


@section('title')
    <title>POS Orders</title>
@endsection

@section('menu')
    @include('backend.merchant.partials.menu')
@endsection

@section('menuuser')
    @include('backend.merchant.partials.userMenu')
@endsection

@section('notif')
    @include('backend.merchant.partials.notif')
@endsection

@section('header-title')
    Merchant Center
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="orders">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Purchases
                        </span>
                    </div>
                    <div class="actions">
                        <div class="portlet-input input-inline input-small">
                            <select class="form-control" v-model="paymentType">
                                <option value="all">All</option>
                                <option value="edc">Edc</option>
                                <option value="cash">Cash</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <nav>
                                <ul class="pager" style="margin: 0">
                                    <li><a class="btn btn-default" v-on:click="prev"
                                           v-bind:class="isFirst?'disabled': ''">Previous</a>
                                    </li>
                                    <li><a class="btn btn-default" v-on:click="next"
                                           v-bind:class="isLast ? 'disabled':''">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-4">
                            <p v-cloak>Page - @{{ currentPage }} | Total Page - @{{ totalPage }}</p>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>List Barang</th>
                                <th>Total Harga</th>
                                <th>Tipe</th>
                                <th>ShopKeeper</th>
                                <th>Nama Pembeli</th>
                                <th>Tanggal Transaksi</th>
                                <th>Detail Pembayaran</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="entry in data" v-cloak>
                                <td>
                                    @{{ entry['id'] }}
                                </td>
                                <td>
                                    <p v-for="item in entry['items']">
                                        Nama - @{{ item['product_item']['product']['name'] }}<br>
                                        Size - @{{ item['product_item']['size']['text'] }}<br>
                                        Qty - @{{ item['qty'] }}<br>
                                    </p>
                                </td>
                                <td>
                                    IDR @{{ entry['grossTotal'] | formatNumber }}
                                </td>
                                <td>
                                    @{{ entry['payment']['type'] }}
                                </td>
                                <td>@{{ entry['user']['nama'] }}</td>
                                <td>@{{ entry['address']['nama'] }}</td>
                                <td>
                                    @{{ entry['created_at'] }}
                                </td>
                                <td>
                                    <template v-if="entry['payment']['type'] == 'edc'">
                                        <p>
                                            Bank - @{{ entry['payment']['bank'] }}<br>
                                            Referensi - @{{ entry['payment']['transaction_number'] }}<br>
                                            Nomor Kartu - @{{  entry['payment']['account_number'] }}<br>
                                            Tanggal Expirasi - @{{ entry['payment']['expiration_date'] }}
                                        </p>
                                    </template>
                                    <template v-if="entry['payment']['type'] == 'cash'">
                                        Via Cash
                                    </template>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/merchant/posOrders.js"></script>
@endsection