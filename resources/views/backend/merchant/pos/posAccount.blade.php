@extends('backend.base')

@section('title')
    <title>Daftar Kasir</title>
@endsection

@section('menu')
    @include('backend.merchant.partials.menu')
@endsection

@section('menuuser')
    @include('backend.merchant.partials.userMenu')
@endsection

@section('notif')
    @include('backend.merchant.partials.notif')
@endsection

@section('header-title')
    Merchant Center
@endsection

@section('content')
    <div id="user">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <div class="alert alert-block alert-info fade in">
                    <h4 class="alert-heading">Info POS Cash</h4>

                    <h3 v-cloak>
                        IDR @{{ posCash*1 }}
                    </h3>

                    <p>
                        <button class="btn purple" v-on:click="showModal">
                            Set Pos Cash
                        </button>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Daftar Kasir
                        </span>
                        </div>
                        <div class="actions">
                            <div class="portlet-input input-inline input-small ">
                                <div class="input-icon right">
                                    <i class="icon-magnifier"></i>
                                    <input type="text" v-model="query" debounce="500"
                                           class="form-control form-control-solid"
                                           placeholder="search...">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row margin-bottom-10">
                            <div class="col-md-4 col-sm-12">
                                <a href="/pos/account/create" class="btn btn-primary">Add New Kasir</a>
                            </div>
                            <div class="col-md-4">
                                <nav>
                                    <ul class="pager" style="margin: 0">
                                        <li><a class="btn btn-default" v-on:click="prev"
                                               v-bind:class="isFirst?'disabled':''">Previous</a>
                                        </li>
                                        <li><a class="btn btn-default" v-on:click="next"
                                               v-bind:class="isLast?'disabled':''">Next</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="col-md-4">
                                <p v-cloak>Page - @{{ currentPage }} | Total Page - @{{ totalPage }}</p>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Username</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="entry in data" v-cloak>
                                    <td>
                                        @{{ entry['user']['id'] }}
                                    </td>
                                    <td>
                                        @{{ entry['user']['username'] }}
                                    </td>
                                    <td>
                                        @{{ entry['user']['nama'] }}
                                    </td>
                                    <td>
                                        @{{ entry['user']['email'] }}
                                    </td>
                                    <td>
                                        <template v-if="entry.user.is_active">
                                            Aktif
                                        </template>
                                        <template v-if="!entry.user.is_active">
                                            Tidak Aktif
                                        </template>
                                    </td>
                                    <td>
                                        <a href="/pos/account/@{{ entry['user']['id'] }}/edit"
                                           class="btn btn-warning">Edit</a>
                                        <template v-if="entry.user.is_active">
                                            <button class="btn btn-warning" v-on:click="deactivate(entry.user)">
                                                Deactivate
                                            </button>
                                        </template>
                                        <template v-if="!entry.user.is_active">
                                            <button class="btn btn-success" v-on:click="activate(entry.user)">
                                                Activate
                                            </button>
                                        </template>
                                        <button type="button" class="btn btn-danger"
                                                v-on:click="deleteUser(entry.user)">
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <modal :show.sync="showModalCashForm" v-cloak>
            <div class="modal-header">
                <button type="button" v-on:click="showModalCashForm = false" class="close" data-dismiss="modal"
                        aria-hidden="true"></button>
                <h4 class="modal-title">Set Pos Cash</h4>
            </div>
            <form v-on:submit="submitCash">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Cash Now</label>
                        <input type="text" class="form-control" v-model="formCash.now" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label">New Cash</label>
                        <input type="text" class="form-control" v-model="formCash.new">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" v-on:click="showModalCashForm = false" class="btn btn-warning">Cancel
                    </button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </modal>
    </div>
    @include('backend.merchant.partials.modal')
@endsection

@section('scripts')
    <script src="/js/merchant/pos.js"></script>
@endsection