@extends('backend.base')

@section('title')
    <title>Create New User</title>
@endsection

@section('morecss')
@endsection

@section('menu')
    @include('backend.merchant.partials.menu')
@endsection

@section('menuuser')
    @include('backend.merchant.partials.userMenu')
@endsection

@section('notif')
    @include('backend.merchant.partials.notif')
@endsection

@section('header-title')
    Merchant Center
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-body form" id="form">
                    <form v-on:submit="submitAdmin" class="form-horizontal">
                        <h3 class="form-section">Account Info</h3>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" v-bind:class="validates.username ? '': 'has-error'">
                                        <label class="control-label col-md-3">
                                            Username
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="username">
                                            <span class="help-block" v-show="!validates.username">
                                                Field username harus terisi dan alphanumeric minimal 3 karakter
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" v-bind:class="validates.email ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Email
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="email">
                                            <span class="help-block" v-show="!validates.email">
                                                Field Email Harus Terisi sesuai email
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" v-bind:class="validates.password ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Password
                                        </label>

                                        <div class="col-md-9">
                                            <input type="password" class="form-control" v-model="password">
                                            <span class="help-block" v-show="!validates.password">
                                                Password harus terisi minimal 6 karakter
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" v-bind:class="validates.passwordConfirm ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Confirm Password
                                        </label>

                                        <div class="col-md-9">
                                            <input type="password" class="form-control" v-model="passwordConfirm">
                                            <span class="help-block" v-show="!validates.passwordConfirm">
                                                harus sama dengan password yang anda masukkan
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h3 class="form-section">
                                Tipe Akun
                            </h3>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Tipe Akun
                                        </label>

                                        <div class="col-md-9">
                                            <p class="form-control-static">Kasir</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3 class="form-section">
                                Account Profile
                            </h3>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" v-bind:class="validates.nama ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Nama
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="nama">
                                            <span class="help-block" v-show="!validates.nama">
                                                Harus terisi minimal 2 karakter
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.nohp ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Handphone
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="nohp">
                                            <span class="help-block" v-show="!validation.nohp">
                                                format 08xxx
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-9">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/merchant/createCashier.js"></script>
@endsection