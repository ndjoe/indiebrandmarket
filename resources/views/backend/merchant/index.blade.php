@extends('backend.base')

@section('title')
    <title>Dashboard Merchant IBM</title>
@endsection

@section('menuuser')
    @include('backend.merchant.partials.userMenu')
@endsection

@section('menu')
    @include('backend.merchant.partials.menu')
@endsection

@section('morecss')
    <link rel="stylesheet" href="/css/merchant/dashboard.css">
@endsection

@section('notif')
    @include('backend.merchant.partials.notif')
@endsection

@section('header-title')
    Merchant Center
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3 col-xs-6">
            <order-stat :color="'blue'" :value="totalOrderCount*1" :title="'Banyak Order Bulan Ini'"
                        :icon="'fa fa-comments'" :link="'/orders'"></order-stat>
        </div>
        <div class="col-md-3 col-xs-6">
            <order-stat :color="'red'" :value="totalOrderSum*1" :title="'Jumlah Penjualan Bulan Ini'"
                        :icon="'fa fa-bar-chart-o'" :link="'/orders'"></order-stat>
        </div>
        <div class="col-md-3 col-xs-6">
            <order-stat :color="'green'" :value="todayOrderCount*1" :title="'Banyak Order Hari Ini'"
                        :icon="'fa fa-shopping-cart'" :link="'/orders'"></order-stat>
        </div>
        <div class="col-md-3 col-xs-6">
            <order-stat :color="'purple'" :value="todayOrderSum*1" :title="'Jumlah Penjualan Hari Ini'"
                        :icon="'fa fa-globe'" :link="'/orders'"></order-stat>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Banyak Order</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <label class="btn red btn-outline btn-circle btn-sm"
                                   v-bind:class="[optionOrderCount == 'day' ? 'active' : '']"
                                   v-on:click="setOptionCount('day')">
                                <input type="radio" v-model="optionOrderCount" value="day"
                                       class="toggle">
                                Hari Ini
                            </label>
                            <label class="btn red btn-outline btn-circle btn-sm"
                                   v-bind:class="[optionOrderCount == 'month' ? 'active' : '']"
                                   v-on:click="setOptionCount('month')">
                                <input type="radio" v-model="optionOrderCount" value="month"
                                       class="toggle">
                                Bulan Ini
                            </label>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="chartOrderCount"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Jumlah Penjualan</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <label class="btn red btn-outline btn-circle btn-sm"
                                   v-bind:class="[optionOrderSum == 'day' ? 'active' : '']"
                                   v-on:click="setOptionSum('day')">
                                <input type="radio" v-model="optionOrderSum" value="day"
                                       class="toggle">
                                Hari Ini
                            </label>
                            <label class="btn red btn-outline btn-circle btn-sm"
                                   v-bind:class="[optionOrderSum == 'month' ? 'active' : '']"
                                   v-on:click="setOptionSum('month')">
                                <input type="radio" v-model="optionOrderSum" value="month"
                                       class="toggle">
                                Bulan Ini
                            </label>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="chartOrderSum"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Top Selling Products</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Nama Produk</th>
                                <th>Banyak Penjualan</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="p in topProducts" v-cloak>
                                <td>
                                    @{{ p.name }}
                                </td>
                                <td>
                                    @{{ p.value }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.merchant.partials.dashboardstat')
@endsection

@section('scripts')
    <script src="/js/merchant/dashboard.js"></script>
@endsection