<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
   data-close-others="true">
    <div class="dropdown-user-inner">
        <span class="username username-hide-on-mobile">
            {{ Auth::user()->username }}
        </span>
        <img src="/images/logo/{{ Auth::user()->profileable->merchant->profileable->logo }}" alt="avatar">
    </div>
</a>
<ul class="dropdown-menu dropdown-menu-default">
    <li>
        <a href="/staff/logout">
            <i class="icon-key"></i> Log Out </a>
    </li>
</ul>