@extends('backend.base')

@section('title')
    <title>Buat Produk Baru</title>
@endsection

@section('menuuser')
    @include('backend.staffmerchant.partials.usermenu')
@endsection

@section('menu')
    @include('backend.staffmerchant.partials.menu')
@endsection

@section('morecss')
    <link rel="stylesheet" href="/css/staffmerchant/createproduct.css">
@endsection



@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="formCreateProduct">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Buat Produk Baru
                        </span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form v-on:submit="submitProduct">
                        <div class="form-body">
                            <h3 class="form-section">Info Produk</h3>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" v-bind:class="!validation.nama ? 'has-error' : ''">
                                        <label class="control-label">Nama Produk</label>
                                        <input class="form-control" type="text" v-model="nama"
                                               placeholder="Tuliskan nama produk....">
                                        <span class="help-block" v-show="!validation.nama">
                                            Field Ini Harus Terisi
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" v-bind:class="validation.kategori ? '':'has-error'">
                                        <label class="control-label">Kategori Produk</label>
                                        <select v-model="kategori" class="form-control">
                                            <option value="">
                                                Pilih kategori...
                                            </option>
                                            <option v-for="kat in listKategori" v-bind:value="kat.value">
                                                @{{ kat.text }}
                                            </option>
                                        </select>
                                        <span class="help-block" v-show="!validation.kategori">
                                            Pilih Kategori yang valid
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" v-bind:class="validation.harga ? '':'has-error'">
                                        <label class="control-label">Harga Produk</label>
                                        <input type="text" class="form-control" v-model="harga"
                                               placeholder="masukkan harga produk anda dalam rupiah cth: 200000 (tanpa titik)">
                                        <span class="help-block" v-show="!validation.harga">
                                            Harga hanya boleh angka
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" v-bind:class="validation.subKategori ? '':'has-error'">
                                        <label class="control-label">Subcategory Produk</label>
                                        <select v-selectize="subKategori" options="listSubkategori"
                                                class="form-control" settings="selectizeOpts">
                                        </select>
                                        <span class="help-block" v-show="!validation.subKategori">
                                            Harus diisi
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" v-bind:class="validation.color ? '':'has-error'">
                                        <label class="control-label">Warna Produk</label>
                                        <select v-selectize="color" class="form-control" options="colorOption"
                                                settings="selectizeOpts">
                                        </select>
                                        <span class="help-block" v-show="!validation.color">
                                            Harus diisi
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">For whom ?</label>
                                        <select v-model="gender" class="form-control">
                                            <option value="all" selected>All</option>
                                            <option value="male">Man</option>
                                            <option value="female">Woman</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-primary" v-on:click="modalItemForm=true">Add
                                        Item
                                    </button>
                                    <table class="table table-responsive">
                                        <thead>
                                        <tr>
                                            <th>Size</th>
                                            <th>Qty</th>
                                            <th>Description</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="i in items">
                                            <td>@{{ i.size | size listSize }}</td>
                                            <td>@{{ i.qty }}</td>
                                            <td>@{{ i.desc }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.description ? '':'has-error'">
                                        <label class="control-label">Deskripsi Produk</label>
                                        <textarea class="form-control" v-model="description" id="" cols="30"
                                                  rows="8"
                                                  placeholder="masukkan keterangan singkat tentang produk"></textarea>
                                        <span class="help-block" v-show="!validation.description">
                                            Maksimal 600 karakter
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <h3 class="form-section">Gambar Produk</h3>

                            <div class="row">
                                <div class="alert alert-info">
                                    <p>Ukuran gambar harus minimal sebesar 600 x 800 piksel</p>
                                </div>

                                <div class="col-md-3">
                                    <h4>Gambar Utama</h4>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                     style="width: 150px; height: 200px;"></div>
                                                <div>
                                                            <span class="btn red btn-outline btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="photo-1" id="photo-1"
                                                                       v-el:fileInput1>
                                                            </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists"
                                                       data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h4>Gambar Dua</h4>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                     style="width: 150px; height: 200px;"></div>
                                                <div>
                                                            <span class="btn red btn-outline btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" id="photo-2" name="photo-2"
                                                                       v-el:fileInput2>
                                                            </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists"
                                                       data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h4>Gambar Tiga</h4>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                     style="width: 150px; height: 200px;"></div>
                                                <div>
                                                            <span class="btn red btn-outline btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" id="photo-3" name="photo-3"
                                                                       v-el:fileInput3>
                                                            </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists"
                                                       data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h4>Gambar Empat</h4>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                     style="width: 150px; height: 200px;"></div>
                                                <div>
                                                            <span class="btn red btn-outline btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" id="photo-4" name="photo-4"
                                                                       v-el:fileInput4>
                                                            </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists"
                                                       data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Gambar Lima</h4>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                     style="width: 150px; height: 200px;"></div>
                                                <div>
                                                            <span class="btn red btn-outline btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" id="photo-5" name="photo-5"
                                                                       v-el:fileInput5>
                                                            </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists"
                                                       data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h4>Gambar Enam</h4>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                     style="width: 150px; height: 200px;"></div>
                                                <div>
                                                            <span class="btn red btn-outline btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" id="photo-6" name="photo-6"
                                                                       v-el:fileInput6>
                                                            </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists"
                                                       data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h4>Gambar Tujuh</h4>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                     style="width: 150px; height: 200px;"></div>
                                                <div>
                                                            <span class="btn red btn-outline btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" id="photo-7" name="photo-7"
                                                                       v-el:fileInput7>
                                                            </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists"
                                                       data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h4>Gambar Delapan</h4>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                     style="width: 150px; height: 200px;"></div>
                                                <div>
                                                            <span class="btn red btn-outline btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" id="photo-8" name="photo-8"
                                                                       v-el:fileInput8>
                                                            </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists"
                                                       data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h3 class="form-section">Discount</h3>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group" v-bind:class="validation.percentDiscount ? '':'has-error'">
                                        <label class="control-label">Persen Diskon</label>
                                        <input v-numberpicker="percentDiscount" postfix="%" max="100" type="text"
                                               class="form-control">
                                        <span class="help-block" v-show="!validation.percentDiscount">
                                            Harus Angka
                                        </span>
                                        <span class="help-block">
                                            Masukkan nilai discount yang anda inginkan dalam persen
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" v-bind:class="validation.expiredDiscount ? '':'has-error'">
                                        <label class="control-label">Expired At:</label>
                                        <input type="text" class="form-control" v-datepicker="expiredDiscount">
                                        <span class="help-block" v-show="!validation.expiredDiscount">
                                            Harus diisi
                                        </span>
                                        <span class="help-block">
                                            Masukkan tanggal berakhirnya discount
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <h3 class="form-section">Berat Produk</h3>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group" v-bind:class="validation.berat ? '':'has-error'">
                                        <label class="control-label">Berat (dalam gram)</label>
                                        <input v-numberpicker="berat" postfix="gram" max="5000" type="text"
                                               class="form-control">
                                        <span class="help-block" v-show="!validation.berat">
                                            Harus angka dan lebih besar dari 0
                                        </span>
                                        <span class="help-block">
                                            Masukkan berat produk anda dalam gram
                                        </span>
                                        <span class="help-block">
                                            1Kg = 1000gram
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions right">
                            <button type="reset" class="btn btn-default">Cancel</button>
                            <button type="submit" class="btn blue" v-if="formValid && !submitted">Submit
                            </button>
                            <button class="btn blue" v-else disabled>Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <modal :show.sync="modalItemForm" v-cloak>
        <div class="modal-header">
            <button type="button" v-on:click="modalItemForm = false" class="close" data-dismiss="modal"
                    aria-hidden="true"></button>
            <h4 class="modal-title">Form Input Item</h4>
        </div>
        <form v-on:submit="submitItem">
            <div class="modal-body">
                <div class="form-group">
                    <label>Size</label>
                    <select class="form-control" v-selectize="formItem.sizeId"
                            options="listSize" settings="{create:true}">
                        <option value="">Pick Size</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>QTY</label>
                    <input type="text" class="form-control" v-model="formItem.qty">
                </div>
                <div class="form-group">
                    <label>Info Sizing Produk</label>
                    <textarea cols="20" rows="5" class="form-control" v-model="formItem.desc"
                              placeholder="masukkan keterangan ukuran produk ini"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal"
                        v-on:click="modalItemForm = false">Close
                </button>
                <button type="submit" class="btn green">Submit</button>
            </div>
        </form>
    </modal>
    @include('backend.merchant.partials.modal')
@endsection

@section('scripts')
    <script src="/js/staffmerchant/createproduct.js"></script>
@endsection
