@extends('backend.base')

@section('title')
    <title>Dashboard Staff Shipping Originalbrands</title>
@endsection

@section('menuuser')
    @include('backend.shipping.partials.usermenu')
@endsection

@section('menu')
    @include('backend.shipping.partials.menu')
@endsection

@section('morecss')
    <link rel="stylesheet" href="/css/shipping/dashboard.css">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6 col-xs-6">
            <order-stat :color="'blue'" :value="tobeshipped*1" :title="'Order Siap Kirim'"
                        :icon="'fa fa-comments'"></order-stat>
        </div>
        <div class="col-md-6 col-xs-6">
            <order-stat :color="'green'" :value="totalShipping*1" :title="'Total Pengiriman'"
                        :icon="'fa fa-shopping-cart'"></order-stat>
        </div>
    </div>
    @include('backend.merchant.partials.dashboardstat')
@endsection

@section('scripts')
    <script src="/js/shipping/dashboard.js"></script>
@endsection