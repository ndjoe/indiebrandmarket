@extends('backend.base')

@section('title')
    <title>Staff Shippings</title>
@endsection

@section('menu')
    @include('backend.shipping.partials.menu')
@endsection

@section('menuuser')
    @include('backend.shipping.partials.usermenu')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="orders">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Orders To Ship
                        </span>
                    </div>
                    <div class="actions">
                        <div class="portlet-input input-inline input-small ">
                            <div class="input-icon right">
                                <i class="icon-magnifier"></i>
                                <input type="text" v-model="query" debounce="500"
                                       class="form-control form-control-solid"
                                       placeholder="search...">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body" v-cloak>
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <nav>
                                <ul class="pager" style="margin: 0">
                                    <li><a class="btn btn-default" v-on:click="prev"
                                           v-bind:class="{'disabled': isFirst}">Previous</a>
                                    </li>
                                    <li><a class="btn btn-default" v-on:click="next"
                                           v-bind:class="{'disabled': isLast}">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-4">
                            <p v-cloak>Page - @{{ currentPage }} | Total Page - @{{ totalPage }}</p>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nomor Order</th>
                                <th>Username Pemesan</th>
                                <th>List Barang</th>
                                <th>Biaya Pengiriman</th>
                                <th>Alamat Pengiriman</th>
                                <th>Status Pengiriman</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="entry in data" v-cloak>
                                <td>
                                    @{{ entry['id'] }}
                                </td>
                                <td>@{{ entry['order_code'] }}</td>
                                <td>
                                    <template v-if="entry['user']['username']">
                                        @{{ entry['user']['username'] }}
                                    </template>
                                    <template v-else>
                                        Guest
                                    </template>
                                </td>
                                <td>
                                    <p v-for="item in entry['items']">
                                        Nama - @{{ item['product_item']['product']['name'] }}<br>
                                        Size - @{{ item['product_item']['size']['text'] }}<br>
                                        Qty - @{{ item['qty'] }}<br>
                                        Pemilik - @{{ item['product_item']['product']['author']['username'] }}<br>
                                        Status -
                                        <template v-if="item['accepted_at'] !== null">
                                            accepted at: @{{ item['accepted_at'] }}
                                        </template>
                                        <template v-if="item['accepted_at'] == null">
                                            Not Yet Accepted
                                        </template>
                                    </p>
                                </td>
                                <td>
                                    <p>
                                        Jenis Pengiriman: JNE - @{{ entry['shippingType'] }}<br>
                                        <template v-if="entry.currency === 'USD'">
                                            Biaya Pengiriman:
                                            USD @{{ entry['shippingCost'] / entry.currency_value | formatNumber }} /
                                            IDR @{{ entry.shippingCost | formatNumber }}
                                        </template>
                                        <template v-else>
                                            Biaya Pengiriman: IDR @{{ entry['shippingCost'] | formatNumber }}
                                        </template>
                                    </p>
                                </td>
                                <td>
                                    <p>
                                        Nama : @{{ entry['address']['nama'] }}<br>
                                        Kota : @{{ entry['address']['kota'] }}<br>
                                        Nomor HP : @{{ entry['address']['nohp'] }}<br>
                                        Kode Pos : @{{ entry['address']['kodepos'] }}<br>
                                        Alamat : @{{ entry['address']['alamat'] }}<br>
                                        Provinsi : @{{ entry['address']['provinsi'] }}<br>
                                        Negara : @{{ entry['address']['negara'] }}
                                    </p>
                                </td>
                                <td>
                                    <span v-if="entry['shipping'] == null">Belum Terkirim</span>
                                    <span v-if="entry['shipping'] != null">Terkirim! - No Resi: @{{ entry['shipping']['no_resi'] }}</span>
                                </td>
                                <td>
                                    <button class="btn btn-sm btn-primary"
                                            v-if="entry['shipping'] == null" v-on:click="showMItem(entry)">
                                        Input resi
                                    </button>
                                    <button class="btn btn-sm btn-primary"
                                            v-if="entry['shipping'] != null" disabled>
                                        Terkirim
                                    </button>
                                    <button class="btn btn-sm btn-primary" v-on:click="view(entry)">
                                        View Order
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <modal :show.sync="showModalShipmentForm" v-cloak>
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"
                                v-on:click="showModalShipmentForm = false">&times;</button>
                        <h4 class="modal-title">Confirm Shipment</h4>
                    </div>
                    <form v-on:submit="submitResi">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">Order</label>
                                <input type="text" class="form-control" v-model="formItem.orderId" disabled>
                            </div>
                            <div class="form-group">
                                <label class="control-label">No Resi</label>
                                <input type="text" class="form-control" v-model="formItem.noResi">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Jenis Pengiriman</label>
                                <input type="text" class="form-control" disabled v-model="formItem.type">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Berat</label>
                                <input type="text" class="form-control" disabled v-model="formItem.berat">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" v-on:click="showModalShipmentForm = false" class="btn btn-warning">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </modal>
                <modal :show.sync="showModalView" v-cloak>
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"
                                v-on:click="showModalView = false">&times;</button>
                        <h4 class="modal-title">Order Detail</h4>
                    </div>
                    <div class="modal-body">
                        @include('backend.admin.partials.invoice')
                    </div>
                    <div class="modal-footer">
                        <button type="button" v-on:click="showModalView = false" class="btn btn-warning">
                            Cancel
                        </button>
                        <a class="btn btn-lg green-haze hidden-print uppercase print-btn"
                           v-on:click="printInvoice(viewItem.id)">Print</a>
                    </div>
                </modal>
                @include('backend.merchant.partials.modal')
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/shipping/shippings.js"></script>
@endsection

