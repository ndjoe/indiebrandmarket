@extends('backend.base')

@section('title')
    <title>Purchasing</title>
@endsection

@section('menu')
    @include('backend.admin.partials.menu')
@endsection

@section('menuuser')
    @include('backend.admin.partials.usermenu', ['username' => 'admin'])
@endsection

@section('notif')
    @include('backend.admin.partials.notif')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="orders" v-cloak>
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Purchases
                        </span>
                    </div>
                    <div class="actions">
                        <div class="portlet-input input-inline input-small">
                            <select v-model="selected" class="form-control" style="width: 100%">
                                <option value="0">All</option>
                                <option v-for="option in options" v-bind:value="option.value">
                                    @{{ option.text }}
                                </option>
                            </select>
                        </div>
                        <div class="portlet-input input-inline input-small ">
                            <div class="input-icon right">
                                <i class="icon-magnifier"></i>
                                <input type="text" v-model="query" debounce="500"
                                       class="form-control form-control-solid"
                                       placeholder="search...">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <nav>
                                <ul class="pager" style="margin: 0">
                                    <li><a class="btn btn-default" v-on:click="prev"
                                           v-bind:class="{'disabled': isFirst}">Previous</a>
                                    </li>
                                    <li><a class="btn btn-default" v-on:click="next"
                                           v-bind:class="{'disabled': isLast}">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-4">
                            <p v-cloak>Page - @{{ currentPage }} | Total Page - @{{ totalPage }}</p>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nomor Order</th>
                                <th>Username Pembeli</th>
                                <th>List Barang</th>
                                <th>Total Harga</th>
                                <th>Tipe</th>
                                <th>Status konfirmasi user</th>
                                <th>Status Pembayaran</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="entry in data" v-cloak>
                                <td>
                                    @{{ entry['id'] }}
                                </td>
                                <td>@{{ entry['order_code'] }}</td>
                                <td>
                                    <template v-if="!entry.user_id">
                                        Guest
                                    </template>
                                    <template v-if="entry.user_id>0">
                                        @{{ entry['user']['username'] }}
                                    </template>
                                </td>
                                <td>
                                    <p v-for="item in entry['items']">
                                        Nama - @{{ item['product_item']['product']['name'] }}<br>
                                        Size - @{{ item['product_item']['size']['text'] }}<br>
                                        Qty - @{{ item['qty'] }}<br>
                                        <template v-if="item.accepted_at">
                                            Status - Accepted <br>
                                        </template>
                                        <template v-else>
                                            Status - Unaccepted <br>
                                        </template>
                                    </p>
                                </td>
                                <td>
                                    <template v-if="entry['currency'] === 'USD'">
                                        <p>
                                            USD @{{ entry.grossTotal / entry.currency_value | formatNumber }} /
                                            IDR @{{ entry['grossTotal'] | formatNumber }}
                                        </p>
                                    </template>
                                    <template v-else>
                                        <p>Total Order:</p>
                                        <p>
                                            IDR @{{ entry['grossTotal'] | formatNumber }}
                                        </p>
                                        <p>Unique Order:</p>
                                        <p>
                                            IDR @{{ entry['grossTotal'] - entry.uniqueCost | formatNumber }}
                                        </p>
                                    </template>
                                </td>
                                <td>
                                    @{{ entry['type'] }}
                                </td>
                                <td>
                                    <template v-if="entry.order_confirmation === null">
                                        Belum ada konfirmasi user
                                    </template>
                                    <template v-if="entry.order_confirmation !== null">
                                        <p>
                                            Transfer ke - @{{ entry['order_confirmation']['bank'] }} <br>
                                            Atas Nama - @{{ entry['order_confirmation']['atas_nama'] }} <br>
                                            Tanggal - @{{ entry['order_confirmation']['date'] }} <br>
                                            Jumlah Transfer - @{{ entry['order_confirmation']['amount'] }}
                                        </p>
                                    </template>
                                </td>
                                <td>
                                    <span v-if="entry.is_paid">
                                        Paid
                                    </span>
                                    <span v-if="!entry.is_paid && entry.is_active">
                                        Unpaid
                                    </span>
                                    <span v-if="!entry.is_paid && !entry.is_active">
                                        Expired/Cancelled
                                    </span>
                                </td>
                                <td>
                                    <button class="btn btn-primary"
                                            v-if="entry.is_active && !entry.is_paid" v-on:click="showMItem(entry)">
                                        Confirm Payment
                                    </button>
                                    <button class="btn btn-primary"
                                            v-if="entry.is_paid" disabled>
                                        Paid
                                    </button>
                                    <button class="btn btn-primary" v-if="!entry.is_active && !entry.is_paid" disabled>
                                        Expired
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <modal :show.sync="showModalPaymentForm" v-cloak>
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"
                                v-on:click="showModalPaymentForm = false">&times;</button>
                        <h4 class="modal-title">Confirm Payment</h4>
                    </div>
                    <form v-on:submit="submitPayment">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">Order</label>
                                <input type="text" class="form-control" v-model="formItem.orderId" disabled>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Bank</label>
                                <select class="form-control" v-model="formItem.bank">
                                    <option value="mandiri">Mandiri</option>
                                    <option value="bca">BCA</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" v-on:click="showModalPaymentForm = false" class="btn btn-warning">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </modal>
                @include('backend.merchant.partials.modal')
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/admin/orders.js"></script>
@endsection