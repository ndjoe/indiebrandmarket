@extends('backend.base')

@section('title')
    <title>Point Of Sales</title>
@endsection

@section('menuuser')
    @include('backend.admin.partials.usermenu', ['username' => $username])
@endsection

@section('menu')
    @include('backend.admin.partials.menu')
@endsection

@section('morecss')
    <link rel="stylesheet" href="/assets/css/cart.css">
@endsection

@section('notif')
    @include('backend.admin.partials.notif')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="pos" v-cloak>
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Point Of Sales App
                        </span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form v-on="submit: submitBarcode">
                                <div class="form-group">
                                    <input type="text" v-model="barcode" class="form-control" autofocus>
                                </div>
                            </form>
                        </div>
                    </div>
                    <table class="table table-hover table-condensed table-cart">
                        <thead>
                        <tr>
                            <th style="width:50%">Product</th>
                            <th style="width:10%">Price</th>
                            <th style="width:8%">Quantity</th>
                            <th style="width:22%" class="text-center">Subtotal</th>
                            <th style="width:10%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-repeat="entry: items">
                            <td data-th="Product">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <img src="/images/catalog/@{{ entry['image'] }}" class="img-responsive">
                                    </div>
                                    <div class="col-sm-10">
                                        <h4>@{{ entry['nama'] }}</h4>
                                    </div>
                                </div>
                            </td>
                            <td data-th="Price">
                                @{{ entry['harga'] }}
                            </td>
                            <td data-th="Quantity">
                                @{{ entry['qty'] }}
                            </td>
                            <td data-th="Subtotal" class="text-right">
                                @{{ entry | subTotalPrice  }}
                            </td>
                            <td data-th="">
                                <button class="btn btn-danger btn-sm">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="4">
                                <strong class="pull-right">
                                    Total - @{{ totalPrice }}
                                </strong>
                            </td>
                            <td data-th="">

                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <button class="btn blue btn-block" v-on="click: submitCart">
                                    Submit
                                </button>
                                <button class="btn yellow btn-block" v-on="click: resetCart">
                                    Reset
                                </button>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        Vue.filter('totalPrice', function (data) {
            var total = 0;

            data.forEach(function (item) {
                total = total + item.harga * 1;
            });

            return total;
        });
        Vue.filter('subTotalPrice', function (data) {
            return data['harga'] * data['qty'];
        });
        var vm = new Vue({
            el: '#pos',
            data: {
                items: [],
                barcode: '',
                totalPrice: 0
            },
            methods: {
                submitBarcode: function (e) {
                    e.preventDefault();
                    var self = this;
                    $.get('/admin/api/pos/items/' + this.barcode).done(function (data) {
                        self.barcode = '';
                        self.items = data;
                    });
                },
                deleteItem: function (data) {

                },
                submitCart: function () {
                    var self = this;
                    $.get('/admin/api/pos/purchase').done(function () {
                        self.items = [];
                    });
                },
                resetCart: function () {
                    var self = this;
                    $.get('/admin/api/pos/reset').done(function () {
                        self.items = [];
                    });
                }
            },
            watch: {
                'items': function (nv, ov) {
                    var self = this;

                    this.totalPrice = 0;
                    nv.forEach(function (data) {
                        self.totalPrice = self.totalPrice * 1 + data.harga * 1 * data.qty;
                    });
                }
            }
        });

        $(function () {
            Metronic.init();
            Layout.init();
        });
    </script>
@endsection