@extends('backend.base')

@section('title')
    <title>Token Registrasi</title>
@endsection

@section('menuuser')
    @include('backend.admin.partials.usermenu')
@endsection

@section('menu')
    @include('backend.admin.partials.menu')
@endsection

@section('morecss')
    <link rel="stylesheet" href="/css/admin/tokens.css">
@endsection

@section('notif')
    @include('backend.admin.partials.notif')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="inventory">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Token Registrasi
                        </span>
                    </div>
                </div>
                <div class="portlet-body" v-cloak>
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <button class="btn btn-primary" v-on:click="showModalAdd">Add New</button>
                        </div>
                        <div class="col-md-4">
                            <nav>
                                <ul class="pager" style="margin: 0">
                                    <li><a class="btn btn-default" v-on:click="prev"
                                           v-bind:class="{'disabled': isFirst}">Previous</a>
                                    </li>
                                    <li><a class="btn btn-default" v-on:click="next"
                                           v-bind:class="{'disabled': isLast}">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-4">
                            <p>Page - @{{ currentPage }} | Total Page - @{{ totalPage }}</p>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Token</th>
                                <th>Expired at</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="entry in data" v-cloak>
                                <td>
                                    @{{ entry.id }}
                                </td>
                                <td>
                                    @{{ entry['token'] }}
                                </td>
                                <td>
                                    @{{ entry['expired_at'] }}
                                </td>
                                <td>
                                    <button class="btn red btn-sm" v-on:click="deleteToken(entry)">
                                        Delete
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <modal :show.sync="showModalAddForm" v-cloak>
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"
                                v-on:click="showModalAddForm = false">&times;</button>
                        <h4 class="modal-title">Add New Token</h4>
                    </div>
                    <form v-on:submit="submitAddToken">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">
                                    Kode Token
                                </label>
                                <input type="text" class="form-control" placeholder="masukkan kode token..."
                                       v-model="formAdd.kode">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Expired At</label>

                                <div class="row">
                                    <input type="text" class="form-control" v-datepicker="formAdd.expired">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" v-on:click="showModalAddForm = false" class="btn btn-warning">Cancel
                            </button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </modal>
            </div>
        </div>
    </div>
    @include('backend.merchant.partials.modal')
@endsection

@section('scripts')
    <script src="/js/admin/tokens.js"></script>
@endsection