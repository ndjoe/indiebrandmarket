@extends('backend.base')

@section('title')
    <title>Slider Management</title>
@endsection

@section('menu')
    @include('backend.admin.partials.menu')
@endsection

@section('menuuser')
    @include('backend.admin.partials.usermenu')
@endsection

@section('notif')
    @include('backend.admin.partials.notif')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="orders">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Slider Promotional
                        </span>
                    </div>
                </div>
                <div class="portlet-body" v-cloak>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Image</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="entry in data" v-cloak>
                                <td>
                                    @{{ entry['id'] }}
                                </td>
                                <td>
                                    <img :src="'/images/sliders/'+entry['image']" alt="" class="img-responsive"
                                         v-if="entry['image'] !== null">
                                </td>
                                <td>
                                    <button class="btn btn-primary" v-on:click="showMItem(entry)">Change Image</button>
                                    <button class="btn btn-primary" v-on:click="clearImage(entry)">Clear Image</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <modal :show.sync="showChangeImageForm" v-cloak>
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"
                                v-on:click="showChangeImageForm = false">&times;</button>
                        <h4 class="modal-title">Change Slider Image</h4>
                    </div>
                    <form v-on:submit="submitImage">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">Slide #</label>
                                <input type="text" class="form-control" v-model="formItem.slideId" disabled>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <img :src="'/images/sliders/'+formItem.image" alt="" class="img-responsive"
                                         id="preview">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Change Image</label>
                                <input type="file" class="form-control" v-el:fileinput id="image">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" v-on:click="showChangeImageForm = false" class="btn btn-warning">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </modal>
                @include('backend.merchant.partials.modal')
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/admin/slider.js"></script>
@endsection

