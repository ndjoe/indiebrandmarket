@extends('backend.base')

@section('title')
    <title>Currency</title>
@endsection

@section('menu')
    @include('backend.admin.partials.menu')
@endsection

@section('menuuser')
    @include('backend.admin.partials.usermenu')
@endsection

@section('notif')
    @include('backend.admin.partials.notif')
@endsection

@section('content')
    <div id="user">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <div class="alert alert-block alert-info fade in">
                    <h4 class="alert-heading">Rate Dollar</h4>

                    <h3 v-cloak>
                        IDR @{{ currency*1 }}
                    </h3>

                    <p>
                        <button class="btn purple" v-on:click="showModal">
                            Set Rate Dollar
                        </button>
                    </p>
                </div>
            </div>
        </div>
        <modal :show.sync="showModalCashForm" v-cloak>
            <div class="modal-header">
                <button type="button" v-on:click="showModalCashForm = false" class="close" data-dismiss="modal"
                        aria-hidden="true"></button>
                <h4 class="modal-title">Set Pos Cash</h4>
            </div>
            <form v-on:submit="submitCash">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Cash Now</label>
                        <input type="text" class="form-control" v-model="formCash.now" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label">New Cash</label>
                        <input type="text" class="form-control" v-model="formCash.new">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" v-on:click="showModalCashForm = false" class="btn btn-warning">Cancel
                    </button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </modal>
    </div>
    @include('backend.merchant.partials.modal')
@endsection

@section('scripts')
    <script src="/js/admin/currency.js"></script>
@endsection