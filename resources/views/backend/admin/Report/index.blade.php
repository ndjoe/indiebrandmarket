@extends('backend.base')

@section('title')
    <title>Sales Report</title>
@endsection

@section('menu')
    @include('backend.admin.partials.menu')
@endsection

@section('menuuser')
    @include('backend.admin.partials.usermenu')
@endsection

@section('morecss')
    <link rel="stylesheet" href="/css/admin/report.css">
@endsection

@section('notif')
    @include('backend.admin.partials.notif')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="orders">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Sales
                        </span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row margin-bottom-10">
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" v-datepicker="date">
                                </div>
                                <div class="col-md-6">
                                    <button class="btn btn-primary" v-on:click="setToday">Set Today</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select class="form-control" v-model="options">
                                    <option value="day">day</option>
                                    <option value="month">month</option>
                                    <option value="year">year</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select v-model="merchant" class="form-control">
                                    <option value="0">Pilih Merchant</option>
                                    <option v-for="m in merchants" :value="m.value">@{{ m.text }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-primary" v-on:click="getData">Generate Report</button>
                            <a :href="'/invoice/merchant?user='+merchant+'&date='+date+'&type=month'"
                               class="btn green">
                                Generate Invoice
                            </a>
                        </div>
                    </div>
                    <div class="row margin-bottom-10">
                        <h4 v-cloak>Bulan: @{{ monthName }}</h4>
                        <div class="col-md-3">
                            <order-stat :color="'blue'" :value="pendapatan*1 | formatMoney"
                                        :title="'Total Pendapatan Merchant'"
                                        :icon="'fa fa-comments'" :link="''"></order-stat>
                        </div>
                        <div class="col-md-3">
                            <order-stat :color="'red'" :value="totalKuponSum*1 | formatMoney"
                                        :title="'Total Penggunaan Voucher'"
                                        :icon="'fa fa-bar-chart-o'" :link="''"></order-stat>
                        </div>
                        <div class="col-md-3">
                            <order-stat :color="'green'" :value="totalAdminSum*1 | formatMoney"
                                        :title="'Total Biaya Administrasi:'"
                                        :icon="'fa fa-shopping-cart'" :link="''"></order-stat>
                        </div>
                        <div class="col-md-3">
                            <order-stat :color="'purple'"
                                        :value="pendapatan - totalKuponSum - totalAdminSum | formatMoney"
                                        :title="'Final Statement'"
                                        :icon="'fa fa-globe'" :link="''"></order-stat>
                        </div>
                    </div>
                    <div class="row margin-bottom-10">
                        <div class="col-md-12">
                            <div id="chart"></div>
                        </div>
                    </div>
                    <div class="row margin-bottom-10">
                        <div class="col-md-4">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Id Order</th>
                                        <th>Detail Barang</th>
                                        <th>Harga Jual</th>
                                        <th>Pendapatan Merchant</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="entry in data" v-cloak>
                                        <td>
                                            @{{ entry['order']['id'] }}
                                        </td>
                                        <td>
                                            <p>
                                                Nama Barang: @{{ entry['product_item']['product']['name'] }}<br>
                                                Size: @{{ entry['product_item']['size']['text'] }}<br>
                                                Qty: @{{ entry['qty'] }}
                                            </p>
                                        </td>
                                        <td>
                                            IDR @{{ entry['adjusment']*1 | formatNumber }}
                                        </td>
                                        <td>
                                            IDR @{{ entry | komisiMerchant | formatNumber }}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Id Kupon</th>
                                        <th>Nilai Kupon</th>
                                        <th>Digunakan Pada Order</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="entry in dataKupon" v-cloak>
                                        <td>
                                            @{{ entry['id'] }}
                                        </td>
                                        <td>
                                            IDR @{{ entry['value'] | formatNumber }}
                                        </td>
                                        <td>
                                            #@{{ entry['orders'][0]['id'] }}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Kode Order</th>
                                        <th>Tanggal</th>
                                        <th>Biaya Administrasi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="entry in dataOrder" v-cloak>
                                        <td>
                                            @{{ entry['order_code'] }}
                                        </td>
                                        <td>
                                            @{{ entry['created_at'] }}
                                        </td>
                                        <td>
                                            IDR 5000
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.merchant.partials.dashboardstat')
@endsection

@section('scripts')
    <script src="/js/admin/report.js"></script>
@endsection