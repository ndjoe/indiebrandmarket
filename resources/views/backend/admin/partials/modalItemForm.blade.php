<script type="text/x-template" id="modalitemform">
    <div class="modal fade in" tabindex="-1" role="basic" aria-hidden="true"
         v-bind:style="{display: show ? 'block' : 'none', paddingRight: '15px'}">
        <div class="modal-dialog">
            <div class="modal-content">
                <slot></slot>
            </div>
        </div>
    </div>
</script>