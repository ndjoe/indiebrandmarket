<div class="invoice-content-2 bordered">
    <div class="row invoice-head">
        <div class="col-md-7 col-xs-6">
            <div class="invoice-logo">
                <img src="/assets/img/logo-original-brands.png" class="img-responsive" alt="">
            </div>
        </div>
        <div class="col-md-5 col-xs-6">
            <div class="company-address">
                <span class="bold uppercase">Originalbrands.id</span>
                <br> 25, Lorem Lis Street, Orange C
                <br> California, US
                <br>
                <span class="bold">T</span> 1800 123 456
                <br>
                <span class="bold">E</span> support@keenthemes.com
                <br>
                <span class="bold">W</span> www.keenthemes.com
            </div>
        </div>
    </div>
    <div class="row invoice-cust-add">
        <div class="col-xs-3">
            <h2 class="invoice-title uppercase">Customer</h2>

            <p class="invoice-desc">
                Nama: @{{ viewItem.address.nama }}<br>
                Handphone: @{{ viewItem.address.nohp }}
            </p>
        </div>
        <div class="col-xs-3">
            <h2 class="invoice-title uppercase">Date</h2>

            <p class="invoice-desc">@{{ viewItem.created_at }}</p>
        </div>
        <div class="col-xs-6">
            <h2 class="invoice-title uppercase">Address</h2>

            <p class="invoice-desc inv-address">
                @{{ viewItem.address.alamat }}
                , @{{ viewItem.address.kota }} @{{ viewItem.address.kodepos }} @{{ viewItem.address.provinsi }} @{{ viewItem.address.negara }}
                <br>
            </p>
        </div>
    </div>
    <div class="row invoice-body">
        <div class="col-xs-12 table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th class="invoice-title uppercase text-center">Nama Produk</th>
                    <th class="invoice-title uppercase text-center">Brand</th>
                    <th class="invoice-title uppercase text-center">Size</th>
                    <th class="invoice-title uppercase text-center">Qty</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="i in viewItem.items">
                    <td class="text-center sbold">
                        @{{ i.product_item.product.name }}
                    </td>
                    <td class="text-center sbold">
                        @{{ i.product_item.product.author.nama }}
                    </td>
                    <td class="text-center sbold">@{{ i.product_item.size.text }}</td>
                    <td class="text-center sbold">@{{ i.qty }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row invoice-subtotal">
        <div class="col-xs-6">
            <h2 class="invoice-title uppercase">#Order</h2>

            <p class="invoice-desc">@{{ viewItem.order_code }}</p>
        </div>
        <div class="col-xs-6">
            <h2 class="invoice-title uppercase">Total</h2>
            <template v-if="viewItem.currency === 'USD'">
                <p class="invoice-desc grand-total">
                    USD @{{ viewItem.grossTotal/viewItem.currency_value | formatNumber }}/
                    IDR @{{ viewItem.grossTotal | formatNumber }}
                </p>
            </template>
            <template v-else>
                <p class="invoice-desc grand-total">IDR @{{ viewItem.grossTotal | formatNumber }}</p>
            </template>
        </div>
    </div>
</div>