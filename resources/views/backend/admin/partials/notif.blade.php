<li class="dropdown dropdown-extended dropdown-notification dropdown-dark">
    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
       data-close-others="true">
        <i class="icon-basket"></i>
        <span class="badge badge-success" v-cloak> @{{ notification }} </span>
    </a>
    <ul class="dropdown-menu">
        <li class="external">
            <h3>
                <span class="bold" v-cloak>@{{ notification }} Order baru</span>
            </h3>
            <a href="/orders">view all</a>
        </li>
    </ul>
</li>