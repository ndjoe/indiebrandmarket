<li>
    <a href="/">
        Dashboard
    </a>
</li>
<li>
    <a href="/users">
        Manajemen Akun
    </a>
</li>
<li>
    <a href="/products">
        Produk & Inventori
    </a>
</li>
<li>
    <a href="/orders">
        Orders
    </a>
</li>
<li>
    <a href="/shippings">
        Shippings
    </a>
</li>
<li>
    <a href="/demografi">
        Demografi
    </a>
</li>
<li>
    <a href="/report">
        Sales Reports
    </a>
</li>
<li>
    <a href="/financial-report">
        Originalbrands financial report
    </a>
</li>
<li>
    <a href="/sliders">
        Promotional Sliders
    </a>
</li>
<li>
    <a href="/coupons">
        Coupons
    </a>
</li>
<li>
    <a href="/token-registration">
        Registration Token
    </a>
</li>
<li>
    <a href="/currency">
        Set Currency
    </a>
</li>