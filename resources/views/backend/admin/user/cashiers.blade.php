@extends('backend.base')

@section('title')
    <title>Manajemen Cashiers</title>
@endsection

@section('menu')
    @include('backend.admin.partials.menu')
@endsection

@section('menuuser')
    @include('backend.admin.partials.usermenu')
@endsection

@section('notif')
    @include('backend.admin.partials.notif')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="user" v-cloak>
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Cashier List
                        </span>
                    </div>
                    <div class="actions">
                        <div class="portlet-input input-inline input-small">
                            <select v-model="selected" class="form-control" style="width: 100%">
                                <option value="0">All</option>
                                <option v-for="option in options" v-bind:value="option.value">
                                    @{{ option.text }}
                                </option>
                            </select>
                        </div>
                        <div class="portlet-input input-inline input-small ">
                            <div class="input-icon right">
                                <i class="icon-magnifier"></i>
                                <input type="text" v-model="query" debounce="500"
                                       class="form-control form-control-solid"
                                       placeholder="search...">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                        </div>
                        <div class="col-md-4">
                            <nav>
                                <ul class="pager" style="margin: 0">
                                    <li><a class="btn btn-default" v-on:click="prev" :class="isFirst ? 'disabled':''">Previous</a>
                                    </li>
                                    <li><a class="btn btn-default" v-on:click="next"
                                           :class="isLast ? 'disabled':''">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-4">
                            <p>Page - @{{ currentPage }} | Total Page - @{{ totalPage }}</p>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Username</th>
                                <th>Type</th>
                                <th>Name</th>
                                <th>Merchant</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="entry in data">
                                <td>
                                    @{{ entry['user']['id'] }}
                                </td>
                                <td>
                                    @{{ entry['user']['username'] }}
                                </td>
                                <td>
                                    Cashier
                                </td>
                                <td>
                                    @{{ entry['user']['nama'] }}
                                </td>
                                <td>
                                    @{{ entry.merchant.username }}
                                </td>
                                <td>
                                    @{{ entry['user']['email'] }}
                                </td>
                                <td>
                                    <template v-if="entry.user.is_active">
                                        Active
                                    </template>
                                    <template v-if="!entry.user.is_active">
                                        Inactive
                                    </template>
                                </td>
                                <td>
                                    <a v-bind:href="'/users/'+'cashiers'+'/'+entry['user']['id']+'/edit'"
                                       class="btn btn-warning">Edit</a>
                                    <template v-if="entry.user.is_active">
                                        <button class="btn btn-warning" v-on:click="deactivate(entry)">
                                            Deactivate
                                        </button>
                                    </template>
                                    <template v-if="!entry.user.is_active">
                                        <button class="btn btn-success" v-on:click="activate(entry)">
                                            Activate
                                        </button>
                                    </template>
                                    <button type="button" class="btn btn-danger" v-on:click="deleteAccount(entry)">
                                        Delete
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/admin/cashiers.js"></script>
@endsection