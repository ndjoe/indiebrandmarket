@extends('backend.base')

@section('title')
    <title>Edit Member</title>
@endsection

@section('morecss')
    <link rel="stylesheet" href="/css/admin/editmember.css">
@endsection

@section('menu')
    @include('backend.admin.partials.menu')
@endsection

@section('menuuser')
    @include('backend.admin.partials.usermenu')
@endsection

@section('notif')
    @include('backend.admin.partials.notif')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" v-cloak>
                <div class="portlet-body form" id="form">
                    <form v-on:submit="submitAdmin" class="form-horizontal">
                        <h3 class="form-section">Account Info</h3>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Username
                                        </label>

                                        <div class="col-md-9">
                                            <p class="form-control-static">@{{ username }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Email
                                        </label>

                                        <div class="col-md-9">
                                            <p class="form-control-static">@{{ email }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.password ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Password
                                        </label>

                                        <div class="col-md-9">
                                            <input type="password" class="form-control" v-model="password">
                                            <span class="help-block" v-show="!validation.password">
                                                Minimal 6 karakter
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.passwordConfirm ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Confirm Password
                                        </label>

                                        <div class="col-md-9">
                                            <input type="password" class="form-control" v-model="passwordConfirm">
                                            <span class="help-block" v-show="!validation.passwordConfirm">
                                                Harus sama dengan password yang anda input
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3 class="form-section">
                                Account Profile
                            </h3>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" v-bind:class="validation.nama ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Name
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="nama">
                                            <span class="help-block" v-show="!validation.nama">
                                                required
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Birthday
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-datepicker="ttl">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Gender
                                        </label>

                                        <div class="col-md-9">
                                            <select v-model="gender" class="form-control">
                                                <option value="unknown">Unknown</option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.nohp ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Hanphone Number
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="nohp">
                                            <span class="help-block" v-show="!validation.nohp">
                                                Format: 08xxxx
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            City
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="kota">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Kodepos
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="kodepos">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="control-label col-md-3">
                                            Province
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="provinsi">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="control-label col-md-3">
                                            Address
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="alamat">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-9">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/admin/editmember.js"></script>
@endsection