@extends('backend.base')

@section('title')
    <title>Users</title>
@endsection

@section('morecss')
@endsection

@section('menu')
    @include('backend.admin.partials.menu')
@endsection

@section('menuuser')
    @include('backend.admin.partials.usermenu')
@endsection

@section('notif')
    @include('backend.admin.partials.notif')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="1349">{{ $memberCount }}</span>
                    </div>
                    <div class="desc">Customers Total</div>
                </div>
                <a class="more" href="/users/customers">To Customer Management
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="1349">{{ $staffCount }}</span>
                    </div>
                    <div class="desc">Staffs Total</div>
                </div>
                <a class="more" href="/users/staffs">To Staff Management
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="1349">{{ $merchantCount }}</span>
                    </div>
                    <div class="desc">Affiliates Total</div>
                </div>
                <a class="more" href="/users/affiliates">To Affiliate Management
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="1349">{{ $guestCount }}</span>
                    </div>
                    <div class="desc">Guest Total</div>
                </div>
                <a class="more" href="/users/guests">To Guest Database
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="1349">{{ $cashierCount }}</span>
                    </div>
                    <div class="desc">Cashier Total</div>
                </div>
                <a class="more" href="/users/cashiers">To Cashier Management
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var socket = io(window.location.hostname + ':6001');
        new Vue({
            el: '#app',
            data: {
                notification: 0
            },
            methods: {
                getNotif: function () {
                    var self = this;
                    $.get('/api/notif')
                            .done(function (data) {
                                self.notification = data.count;
                            });
                }
            },
            ready: function () {
                App.init();
                Layout.init();
                var self = this;
                socket.on('orders:created', function (message) {
                    self.notification += 1;
                });
                this.getNotif();
            }
        });
    </script>
@endsection