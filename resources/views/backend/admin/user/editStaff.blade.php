@extends('backend.base')

@section('title')
    <title>Edit Staff Originalbrands</title>
@endsection

@section('morecss')
@endsection

@section('menu')
    @include('backend.admin.partials.menu')
@endsection

@section('menuuser')
    @include('backend.admin.partials.usermenu')
@endsection

@section('notif')
    @include('backend.admin.partials.notif')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-body form" id="form">
                    <form v-on:submit="submitAdmin" class="form-horizontal">
                        <h3 class="form-section">Account Info</h3>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Username
                                        </label>

                                        <div class="col-md-9">
                                            <p class="form-control-static" v-cloak>@{{ username }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Email
                                        </label>

                                        <div class="col-md-9">
                                            <p class="form-control-static" v-cloak>@{{ email }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.password ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Password
                                        </label>

                                        <div class="col-md-9">
                                            <input type="password" class="form-control" v-model="password">
                                            <span class="help-block" v-show="validation.password.required">
                                                Required
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.passwordConfirm ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Confirm Password
                                        </label>

                                        <div class="col-md-9">
                                            <input type="password" class="form-control" v-model="passwordConfirm">
                                            <span class="help-block" v-show="validation.passwordConfirm.required">
                                                Required
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h3 class="form-section">
                                Account Type
                            </h3>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Account Type
                                        </label>

                                        <div class="col-md-9">
                                            <p class="form-control-static">@{{ role }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3 class="form-section">
                                Account Profile
                            </h3>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.nama ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Name
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="nama">
                                            <span class="help-block" v-show="!validation.nama">
                                                Required
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.nohp ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Handphone
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="nohp">
                                            <span class="help-block" v-show="!validation.nohp">
                                                format 08xxx
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-9">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/admin/editstaff.js"></script>
@endsection