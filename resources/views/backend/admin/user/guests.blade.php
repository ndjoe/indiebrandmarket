@extends('backend.base')

@section('title')
    <title>Database Guest</title>
@endsection

@section('menu')
    @include('backend.admin.partials.menu')
@endsection

@section('menuuser')
    @include('backend.admin.partials.usermenu')
@endsection

@section('notif')
    @include('backend.admin.partials.notif')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="user" v-cloak>
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Guest List
                        </span>
                    </div>
                    <div class="actions">
                        <div class="portlet-input input-inline input-small ">
                            <div class="input-icon right">
                                <i class="icon-magnifier"></i>
                                <input type="text" v-model="query" debounce="500"
                                       class="form-control form-control-solid"
                                       placeholder="search...">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                        </div>
                        <div class="col-md-4">
                            <nav>
                                <ul class="pager" style="margin: 0">
                                    <li><a class="btn btn-default" v-on:click="prev" :class="isFirst ? 'disabled':''">Previous</a>
                                    </li>
                                    <li><a class="btn btn-default" v-on:click="next"
                                           :class="isLast ? 'disabled':''">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-4">
                            <p>Page - @{{ currentPage }} | Total Page - @{{ totalPage }}</p>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Nomor Handphone</th>
                                <th>Kota</th>
                                <th>Kodepos</th>
                                <th>Alamat</th>
                                <th>Provinsi</th>
                                <th>Negara</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="entry in data">
                                <td>
                                    @{{ entry['nama'] }}
                                </td>
                                <td>
                                    @{{ entry['nohp'] }}
                                </td>
                                <td>
                                    @{{ entry['kota'] }}
                                </td>
                                <td>
                                    @{{ entry.kodepos }}
                                </td>
                                <td>
                                    <p>@{{ entry.alamat }}</p>
                                </td>
                                <td>
                                    @{{ entry.provinsi }}
                                </td>
                                <td>
                                    @{{ entry.negara }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/admin/guests.js"></script>
@endsection