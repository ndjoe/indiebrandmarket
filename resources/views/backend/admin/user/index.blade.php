@extends('backend.base')

@section('title')
    <title>Accounts</title>
@endsection

@section('menu')
    @include('backend.admin.partials.menu')
@endsection

@section('menuuser')
    @include('backend.admin.partials.usermenu')
@endsection

@section('notif')
    @include('backend.admin.partials.notif')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="user">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Daftar User
                        </span>
                    </div>
                    <div class="actions">
                        <div class="portlet-input input-inline input-small">
                            <select v-model="selected" class="form-control" style="width: 100%">
                                <option value="all">All</option>
                                <option value="member">Member</option>
                                <option value="merchant">Merchant</option>
                                <option value="admin">Admin</option>
                                <option value="cashier">Cashier</option>
                            </select>
                        </div>
                        <div class="portlet-input input-inline input-small ">
                            <div class="input-icon right">
                                <i class="icon-magnifier"></i>
                                <input type="text" v-model="query" debounce="500"
                                       class="form-control form-control-solid"
                                       placeholder="search...">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <a href="/users/admin/create" class="btn btn-primary">Add New Admin</a>
                            <a href="/users/merchant/create" class="btn purple">Add New Merchant</a>
                        </div>
                        <div class="col-md-4">
                            <nav>
                                <ul class="pager" style="margin: 0">
                                    <li><a class="btn btn-default" v-on="click: prev" v-class="disabled: isFirst">Previous</a>
                                    </li>
                                    <li><a class="btn btn-default" v-on="click: next"
                                           v-class="disabled: isLast">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-4">
                            <p>Page - @{{ currentPage }} | Total Page - @{{ totalPage }}</p>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Username</th>
                                <th>Tipe</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-repeat="entry: data">
                                <td>
                                    @{{ entry['id'] }}
                                </td>
                                <td>
                                    @{{ entry['username'] }}
                                </td>
                                <td>
                                    @{{ entry['roles'][0]['display_name'] }}
                                </td>
                                <td>
                                    @{{ entry['nama'] }}
                                </td>
                                <td>
                                    @{{ entry['email'] }}
                                </td>
                                <td>
                                    <template v-if="entry.is_active">
                                        Aktif
                                    </template>
                                    <template v-if="!entry.is_active">
                                        Tidak Aktif
                                    </template>
                                </td>
                                <td>
                                    <a href="/users/@{{ entry['roles'][0]['name'] }}/@{{ entry['id'] }}/edit"
                                       class="btn btn-warning">Edit</a>
                                    <template v-if="entry.is_active">
                                        <button class="btn btn-warning" v-on="click: deactivate(entry)">
                                            Deactivate
                                        </button>
                                    </template>
                                    <template v-if="!entry.is_active">
                                        <button class="btn btn-success" v-on="click: activate(entry)">
                                            Activate
                                        </button>
                                    </template>
                                    <button type="button" class="btn btn-danger" v-on="click: delete(entry)">
                                        Delete
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            Metronic.init();
            Layout.init();
        });
        var vm = new Vue({
            el: '#user',
            data: {
                selected: 'all',
                query: '',
                data: [],
                currentPage: 1,
                totalPage: 1,
            },
            computed: {
                isLast: function () {
                    return this.currentPage === this.totalPage;
                },
                isFirst: function () {
                    return this.currentPage === 1;
                }
            },
            watch: {
                'selected': function (nv, ov) {
                    if (nv === ov) {
                        return;
                    }
                    this.currentPage = 1;
                    this.getData();
                },
                'query': function (nv, ov) {
                    if (nv === ov) {
                        return;
                    }
                    this.currentPage = 1;
                    this.getData();
                }
            },
            methods: {
                getData: function (page) {
                    if (!page) {
                        page = 1;
                    }
                    var self = this;
                    $.get('/api/users', {
                        query: this.query,
                        filter: this.selected,
                        page: page
                    }).done(function (data) {
                        self.data = data.data;
                        self.currentPage = data.current_page;
                        self.totalPage = data.last_page;
                    });
                },
                prev: function () {
                    if (this.currentPage === 1) {
                        return;
                    }
                    this.getData(this.currentPage - 1);
                },
                next: function () {
                    if (this.currentPage === this.totalPage) {
                        return;
                    }
                    this.getData(this.currentPage + 1);
                },
                activate: function (data) {
                    var self = this;
                    $.get('/api/user/' + data.id + '/activate')
                            .done(function () {
                                self.getData();
                            });
                },
                deactivate: function (data) {
                    var self = this;
                    $.get('/api/user/' + data.id + '/deactivate')
                            .done(function () {
                                self.getData();
                            });
                },
                deleteMember: function (data) {
                    var self = this;
                    $.get('/api/user/' + data.id + '/delete')
                            .done(function () {
                                self.getData();
                            });
                }
            },
            ready: function () {
                this.getData();
            }
        });
    </script>
@endsection