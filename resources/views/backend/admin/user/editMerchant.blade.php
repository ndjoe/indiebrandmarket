@extends('backend.base')

@section('title')
    <title>Edit Merchant</title>
@endsection

@section('morecss')
    <link rel="stylesheet" href="/css/admin/editmerchant.css">
@endsection

@section('menu')
    @include('backend.admin.partials.menu')
@endsection

@section('menuuser')
    @include('backend.admin.partials.usermenu')
@endsection

@section('notif')
    @include('backend.admin.partials.notif')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" v-cloak>
                <div class="portlet-body form" id="form">
                    <form v-on:submit="submitAccount" class="form-horizontal">
                        <h3 class="form-section">Account Info</h3>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Username
                                        </label>

                                        <div class="col-md-9">
                                            <p class="form-control-static" v-text="username"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Email
                                        </label>

                                        <div class="col-md-9">
                                            <p class="form-control-static" v-text="email"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.password ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Password
                                        </label>

                                        <div class="col-md-9">
                                            <input type="password" class="form-control" v-model="password">
                                            <span class="help-block" v-show="!validation.password">
                                                Minimal 6 karakter
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.passwordConfirm ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Confirm Password
                                        </label>

                                        <div class="col-md-9">
                                            <input type="password" class="form-control" v-model="passwordConfirm">
                                            <span class="help-block" v-show="!validation.passwordConfirm">
                                                Harus sama dengan password yang anda masukkan
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3 class="form-section">
                                Logo
                            </h3>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="fileinput"
                                         :class="logo.trim() ? 'fileinput-exists':'fileinput-new'"
                                         data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                             style="width: 200px; height: 200px;">
                                            <img v-bind:src="logo"
                                                 v-if="logo.trim()">
                                        </div>
                                        <div>
                                            <span class="btn red btn-outline btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" id="logoimage" name="logo"
                                                       v-el:logo>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3 class="form-section">
                                Banner Image
                            </h3>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="fileinput"
                                         :class="banner.trim() ? 'fileinput-exists':'fileinput-new'"
                                         data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                             style="width: 1000px; height: 300px;">
                                            <img v-bind:src="banner"
                                                 v-if="banner.trim()">
                                        </div>
                                        <div>
                                            <span class="btn red btn-outline btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" id="bannerimage" name="banner"
                                                       v-el:banner>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3 class="form-section">
                                Account Profile
                            </h3>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.nama ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Brand Name
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="nama">
                                            <span class="help-block" v-show="!validation.nama">
                                                Required
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.owner ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Owner Name
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="owner">
                                            <span class="help-block" v-show="!validation.owner">
                                                Required
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.nohp ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Handphone
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="nohp">
                                            <span class="help-block" v-show="!validation.nohp">
                                                format 08xxxx
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.about ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            About
                                        </label>

                                        <div class="col-md-9">
                                            <textarea cols="30" rows="10" class="form-control"
                                                      v-model="about"></textarea>
                                            <span class="help-block" v-show="!validation.about">
                                                Maksimal 600 karakter
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.kota ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            City
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="kota">
                                            <span class="help-block" v-show="!validation.kota">
                                                Required
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.provinsi ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Province
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="provinsi">
                                            <span class="help-block" v-show="!validation.provinsi">
                                                Required
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.kodepos ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Kode Pos
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="kodepos">
                                            <span class="help-block" v-show="!validation.kodepos">
                                                Harus angka 5 karakter
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" :class="validation.alamat ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Address
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="alamat">
                                            <span class="help-block" v-show="!validation.alamat">
                                                Required
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h3 class="form-section">
                                Bank Info
                            </h3>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group" :class="validation.bank ? '':'has-error'">
                                        <label class="control-label col-md-3">
                                            Bank
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="bank">
                                            <span class="help-block" v-show="!validation.bank">
                                                Required
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Nomor Rekening
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="norek">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" :class="validation.ownerRek ? '':'has-error'">
                                        <label class="control-label col-md-3">Atas Nama</label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="ownerRek">
                                            <span class="help-block" v-show="!validation.ownerRek">
                                                Harus Terisi
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-9">
                                    <button type="submit" class="btn btn-primary" v-if="formValid">Submit</button>
                                    <button type="button" class="btn btn-primary" disabled v-if="!formValid">Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/admin/editmerchant.js"></script>
@endsection