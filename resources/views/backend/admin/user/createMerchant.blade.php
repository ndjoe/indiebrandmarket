@extends('backend.base')

@section('title')
    <title>Create New User</title>
@endsection

@section('morecss')
@endsection

@section('menu')
    @include('backend.admin.partials.menu')
@endsection

@section('menuuser')
    @include('backend.admin.partials.usermenu')
@endsection

@section('notif')
    @include('backend.admin.partials.notif')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" v-cloak>
                <div class="portlet-body form" id="form">
                    <form v-on="submit: submitAdmin" class="form-horizontal">
                        <h3 class="form-section">Account Info</h3>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" v-class="has-error: validation.username.required">
                                        <label class="control-label col-md-3">
                                            Username
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="username"
                                                   v-validate="required">
                                            <span class="help-block" v-show="validation.username.required">
                                                Field username harus terisi
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" v-class="has-error: validation.email.required">
                                        <label class="control-label col-md-3">
                                            Email
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="email"
                                                   v-validate="required">
                                            <span class="help-block" v-show="validation.email.required">Field Email Harus Terisi</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" v-class="has-error: validation.password.required">
                                        <label class="control-label col-md-3">
                                            Password
                                        </label>

                                        <div class="col-md-9">
                                            <input type="password" class="form-control" v-model="password"
                                                   v-validate="required">
                                            <span class="help-block" v-show="validation.password.required">
                                                Password harus terisi
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" v-class="has-error: validation.passwordConfirm.required">
                                        <label class="control-label col-md-3">
                                            Confirm Password
                                        </label>

                                        <div class="col-md-9">
                                            <input type="password" class="form-control" v-model="passwordConfirm"
                                                   v-validate="required">
                                            <span class="help-block" v-show="validation.passwordConfirm.required">
                                                harus terisi
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h3 class="form-section">
                                Tipe Akun
                            </h3>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Tipe Akun
                                        </label>

                                        <div class="col-md-9">
                                            <p class="form-control-static">Merchant</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3 class="form-section">
                                Account Profile
                            </h3>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" v-class="has-error: validation.nama.required">
                                        <label class="control-label col-md-3">
                                            Nama Brand
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="nama"
                                                   v-validate="required">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Nama Pemilik
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="owner">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Nomor Handphone
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="nohp">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            About
                                        </label>

                                        <div class="col-md-9">
                                            <textarea cols="30" rows="10" class="form-control"
                                                      v-model="about"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Bank
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="bank">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Nomor Rekening
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="norek">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Kota
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="kota">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Provinsi
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="provinsi">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Kode Pos
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="kodepos">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Alamat
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="alamat">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-9">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/assets/plugins/vue-validator.min.js"></script>
    <script src="/assets/plugins/validator.min.js"></script>
    <script>
        Vue.use(window['vue-validator']);
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            Metronic.init();
            Layout.init();
        });
        var vm = new Vue({
            el: '#form',
            data: {
                username: '',
                email: '',
                password: '',
                passwordConfirm: '',
                nama: '',
                owner: '',
                nohp: '',
                bank: '',
                norek: '',
                about: '',
                kota: '',
                kodepos: '',
                alamat: '',
                provinsi: ''
            },
            validator: {
                validates: {
                    numeric: function (val) {
                        return validator.isNumeric(val);
                    },
                    email: function (val) {
                        return validator.isEmail(val);
                    }
                }
            },
            methods: {
                submitAdmin: function (e) {
                    e.preventDefault();
                    var formData = new FormData;
                    formData.append('username', this.username);
                    formData.append('password', this.password);
                    formData.append('password_confirmation', this.passwordConfirm);
                    formData.append('nama', this.nama);
                    formData.append('email', this.email);
                    formData.append('kota', this.kota);
                    formData.append('owner', this.owner);
                    formData.append('bank', this.bank);
                    formData.append('provinsi', this.provinsi);
                    formData.append('norek', this.norek);
                    formData.append('kodepos', this.kodepos);
                    formData.append('nohp', this.nohp);
                    formData.append('about', this.about);
                    formData.append('alamat', this.alamat);

                    $.ajax({
                        url: '/api/users/merchant',
                        data: formData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        aync: false,
                        type: 'POST'
                    }).done(function (data) {
                        if (data.created) {
                            window.location.href = data.redirect;
                        } else {
                            console.log(data);
                        }
                    });
                }
            },
            components: {}
        });
    </script>
@endsection