@extends('baseinvoice')

@section('title')
    <title>Invoice</title>
@endsection

@section('notif')
    @include('backend.admin.partials.notif')
@endsection

@section('content')
    <div class="invoice-content-2 bordered">
        <div class="row invoice-head">
            <div class="col-md-7 col-xs-6">
                <div class="invoice-logo">
                    <img src="{{ asset('img/OBIDSTORE.png') }}" class="img-responsive" alt="">
                    <h1 class="uppercase">Invoice</h1>
                </div>
            </div>
            <div class="col-md-5 col-xs-6">
                <div class="company-address">
                    <span class="bold uppercase">Originalbrands.id</span>
                    <br> 25, Lorem Lis Street, Orange C
                    <br> California, US
                    <br>
                    <span class="bold">T</span> 1800 123 456
                    <br>
                    <span class="bold">E</span> support@originalbrands.id
                    <br>
                    <span class="bold">W</span> www.originalbrands.id
                </div>
            </div>
        </div>
        <div class="row invoice-cust-add">
            <div class="col-xs-3">
                <h2 class="invoice-title uppercase">Customer</h2>
                <p class="invoice-desc">{{ $brand->nama }}</p>
            </div>
            <div class="col-xs-3">
                <h2 class="invoice-title uppercase">Date</h2>
                <p class="invoice-desc">{{ \Carbon\Carbon::now()->toDateString() }}</p>
            </div>
            <div class="col-xs-6">
                <h2 class="invoice-title uppercase">Address</h2>
                <p class="invoice-desc inv-address">{{ $brand->profileable->alamat }}</p>
                <p class="invoice-desc inv-address">{{ $brand->profileable->kodepos }}
                    , {{ $brand->profileable->kota }}</p>
                <p class="invoice-desc inv-address">{{ $brand->profileable->provinsi }}</p>
            </div>
        </div>
        <div class="row invoice-body">
            <div class="col-xs-12 table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="invoice-title uppercase">Description</th>
                        <th class="invoice-title uppercase text-center">Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($invoice as $i)
                        <tr>
                            <td>{{ $i['description'] }}</td>
                            <td class="text-center sbold">{{ $i['total'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row invoice-subtotal">
            <div class="col-xs-6">
                <h2 class="invoice-title uppercase">Total</h2>
                <p class="invoice-desc grand-total">{{ $grandTotal }}</p>
            </div>
        </div>
    </div>
@endsection