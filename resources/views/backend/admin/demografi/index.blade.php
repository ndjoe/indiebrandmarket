@extends('backend.base')

@section('title')
    <title xmlns:v-on="http://www.w3.org/1999/xhtml">Sales Report</title>
@endsection

@section('menu')
    @include('backend.admin.partials.menu')
@endsection

@section('menuuser')
    @include('backend.admin.partials.usermenu')
@endsection

@section('morecss')
    <link rel="stylesheet" href="/css/admin/demografi.css">
@endsection

@section('notif')
    @include('backend.admin.partials.notif')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="orders">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Demografi Member
                        </span>
                    </div>
                </div>
                <div class="portlet-body" v-cloak>
                    <div class="row margin-bottom-10">
                        <div class="col-md-3">
                            <h3>Statistik Gender User Baru Bulan ini</h3>

                            <div id="chart-new-user-gender"></div>
                        </div>
                        <div class="col-md-3">
                            <h3>Statistik Umur User Baru Bulan ini</h3>

                            <div id="chart-new-user-age"></div>
                        </div>
                        <div class="col-md-3">
                            <h3>Statistik Gender Total User</h3>

                            <div id="chart-total-user-gender"></div>
                        </div>
                        <div class="col-md-3">
                            <h3>Statistik Umur Total User</h3>

                            <div id="chart-total-user-age"></div>
                        </div>
                    </div>
                    <div class="row margin-bottom-10">
                        <div class="col-md-12">
                            <h3>Demografi User Tiap Brand</h3>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control" v-model="merchant">
                                            <option value="0">Select Merchant</option>
                                            <option v-for="m in listmerchant" :value="m.value">@{{ m.text }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <button class="btn btn-primary" v-on:click="getBrandStat">Generate Statistics
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Umur Pembeli</h3>

                                    <div id="brand-chart-age"></div>
                                </div>
                                <div class="col-md-6">
                                    <h3>Gender Pembeli</h3>

                                    <div id="brand-chart-gender"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/admin/demografi.js"></script>
@endsection