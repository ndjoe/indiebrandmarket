<!doctype html>
<html>
<head>
    <title>Merchant Registration Login</title>
    <meta name="_token" content="{{ csrf_token() }}">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet"
          type="text/css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/reg/lock.min.css">
    <style>
        .page-lock .page-body {
            padding: 15px;
            margin-top: 0;
        }

        .lock-form {
            padding: 0 70px;
        }
    </style>
</head>
<body>
<div class="page-lock">
    <div class="page-logo">
        <a class="brand">
            <img src="/img/OBIDSTORE.png" class="img-responsive" alt="logo"/>
        </a>
    </div>
    <div class="page-body">
        <div class="lock-head"> Locked</div>
        <div class="lock-body">
            <form class="lock-form" action="/token" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off"
                           placeholder="enter your token .... " name="token"/></div>
                <div class="form-actions">
                    <button type="submit" class="btn red uppercase">Unlock</button>
                </div>
            </form>
        </div>
    </div>
    <div class="page-footer-custom"> {{ \Carbon\Carbon::now()->year }} &copy; Originalbrands.id</div>
</div>
<script src="/js/mandatory.js"></script>
<script src="/js/reg/lock.min.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    $(function () {
        App.init();
        Layout.init();
    });
</script>
</body>
</html>