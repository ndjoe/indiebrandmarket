@extends('backend.base')

@section('title')
    <title>Thank You</title>
@endsection

@section('morecss')
@endsection

@section('content')
    <div class="row">
        <div class="alert alert-success">
            <p>
                Terimakasih sudah registrasi di originalbrands.id, akun anda akan segera kami proses mohon tunggu
                konfirmasi dari tim kami untuk langkah selanjutnya
            </p>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            App.init();
            Layout.init();
        });
    </script>
@endsection