<!DOCTYPE html>
<html>
<head>
    @yield('title')
    <meta name="_token" content="{{ csrf_token() }}">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet"
          type="text/css">
    @yield('morecss')
    <link rel="stylesheet" href="/css/style.css">
    <style>
        .page-header {
            background: #333;
        }

        .page-header .burger-trigger .menu-trigger {
            background: #333;
        }

        .page-header .top-menu .dropdown-user .dropdown-toggle .dropdown-user-inner {
            background: #333;
        }

        .page-header .top-menu .nav .open > a {
            background: #333;
        }

        .page-header .top-menu .nav .open > a:hover {
            background: #333;
        }

        .page-header .burger-trigger .menu-overlay-bg-transparent.menu-overlay-show ~ .menu-bg-overlay {
            background: rgba(0, 0, 0, 0.8);
        }

        .page-header .burger-trigger .menu-overlay-nav > li > a {
            color: #eee;
        }

        .page-header .burger-trigger .menu-overlay-nav > li > a:hover {
            color: #aaa;
        }

        .page-header .burger-trigger .menu-close {
            position: absolute;
            display: inline-block;
            font-size: 40px;
            color: #eee;
            background: transparent;
            border: none;
            outline: 0;
        }

        .page-header .burger-trigger .menu-close:hover {
            color: #aaa;
        }

        .page-header-title {
            float: left;
            position: relative;
            color: #eee;
        }

        @media screen and (min-width: 1250px) {
            .menu-overlay {
                padding-left: 40%;
                padding-right: 40%;
            }

            .menu-overlay-content {
                border: #eee 1px solid;
                background: #333 !important;
            }
        }

        @media screen and (max-width: 471px) {
            .page-header-title {
                display: none;
            }
        }
    </style>
</head>
<body class="page-container-bg-solid" id="app">
<div class="page-header navbar-fixed-top">
    <div class="clearfix">
        <div class="burger-trigger">
            <button class="menu-trigger">
                <img src="/assets/img/menu.png" alt="toggler">
            </button>
            <div class="menu-overlay menu-overlay-bg-transparent">
                <div class="menu-overlay-content">
                    <ul class="menu-overlay-nav text-uppercase">
                        @yield('menu')
                    </ul>
                </div>
            </div>
            <div class="menu-bg-overlay">
                <button class="menu-close">x</button>
            </div>
        </div>
        <div class="page-header-title">
            <h1>@yield('header-title')</h1>
        </div>
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                @yield('notif')
                <li class="dropdown dropdown-user dropdown-dark">
                    @yield('menuuser')
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="clearfix">
</div>
<div class="page-container page-container-bg-solid">
    <div class="container-fluid container-lf-space page-content">
        @yield('content')
    </div>
</div>
<script src="/js/mandatory.js"></script>
@include('js')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    Vue.config.debug = true;
</script>
@yield('scripts')
</body>
</html>