@extends('backend.base')

@section('title')
    Shippings
@endsection

@section('menu')
    @include('backend.purchasing.partials.menu')
@endsection

@section('menuuser')
    @include('backend.admin.partials.usermenu')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="orders">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Slider Promotional
                        </span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Image</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-repeat="entry: data">
                                <td>
                                    @{{ entry['id'] }}
                                </td>
                                <td>
                                    <img src="/images/sliders/@{{ entry['image'] }}" alt="" class="img-responsive"
                                         v-if="entry['image'] !== null">
                                </td>
                                <td>
                                    <button class="btn btn-primary" v-on="click: showMItem(entry)">Change Image</button>
                                    <button class="btn btn-primary" v-on="click: clearImage(entry)">Clear Image</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <image-modal show="@{{showChangeImageForm}}" submit-action="@{{ submitImage }}">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"
                                v-on="click: showChangeImageForm = false">&times;</button>
                        <h4 class="modal-title">Change Slider Image</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">Slide #</label>
                            <input type="text" class="form-control" v-model="formItem.slideId" disabled>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <img src="/images/sliders/@{{ formItem.image }}" alt="" class="img-responsive"
                                     id="preview">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Change Image</label>
                            <input type="file" class="form-control" v-el="fileInput" id="image">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" v-on="click: showChangeImageForm = false" class="btn btn-warning">Cancel
                        </button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </image-modal>
                @include('backend.admin.partials.modalItemForm')
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            Metronic.init();
            Layout.init();
            $('#image').change(function () {
                readImageURL(this);
            })
        });

        function readImageURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        Vue.filter('length', function (data) {
            return data.length;
        });
        var vm = new Vue({
            el: '#orders',
            data: {
                data: [],
                formItem: {
                    slideId: '',
                    image: ''
                },
                showChangeImageForm: false
            },
            computed: {},
            watch: {},
            methods: {
                getData: function () {
                    var self = this;
                    $.get('/api/sliders').done(function (data) {
                        self.data = data;
                    });
                },
                showMItem: function (data) {
                    this.formItem.slideId = data['id'];
                    this.formItem.image = data['image'];
                    this.$$.fileInput.value = '';
                    this.showChangeImageForm = true;
                },
                submitImage: function (e) {
                    e.preventDefault();
                    var formData = new FormData();

                    formData.append('slideId', this.formItem.slideId);
                    if (this.$$.fileInput.files.length === 0) {
                        return;
                    }
                    formData.append('image', this.$$.fileInput.files[0]);
                    var self = this;
                    $.ajax({
                        url: '/api/sliders',
                        method: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false
                    }).done(function () {
                        self.showChangeImageForm = false;
                        self.getData();
                    });
                },
                clearImage: function (data) {
                    var self = this;
                    $.get('/api/sliders/' + data.id + '/clear')
                            .done(function (res) {
                                if (res.cleared) {
                                    self.getData();
                                }
                            });
                }
            },
            ready: function () {
                this.getData();
            },
            components: {
                "image-modal": {
                    props: ["show", "submitAction"],
                    template: '#modalItemForm',
                    data: function () {
                        return {
                            show: false
                        }
                    },
                    watch: {
                        show: function (val) {
                            if (val) $(this.$el).modal('show');
                            else $(this.$el).modal('hide');
                        }
                    },
                    replace: true
                }
            }
        });
    </script>
@endsection

