@extends('backend.base')

@section('title')
    <title>Dashboard Staff Finance</title>
@endsection

@section('menuuser')
    @include('backend.finance.partials.usermenu')
@endsection

@section('menu')
    @include('backend.finance.partials.menu')
@endsection

@section('morecss')
    <link rel="stylesheet" href="/css/finance/dashboard.css">
@endsection

@section('content')
    <div class="row" v-cloak>
        <div class="col-md-4 col-xs-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Top Member</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Pembelian</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="data in topMember">
                                <td>@{{ data.username }}</td>
                                <td>@{{ data.value | formatNumber }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Top Merchant</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Penghasilan</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="data in topMerchant">
                                <td>@{{ data.username }}</td>
                                <td>@{{ data.value | formatNumber }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Top Selling Products</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Nama Produk</th>
                                <th>Banyak Penjualan</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="p in topProducts">
                                <td>
                                    @{{ p.name }}
                                </td>
                                <td>
                                    @{{ p.value }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Jumlah Penjualan</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <label class="btn red btn-outline btn-circle btn-sm"
                                   v-bind:class="[optionOrderSum == 'day' ? 'active' : '']"
                                   v-on:click="setOptionSum('day')">
                                <input type="radio" v-model="optionOrderSum" value="day"
                                       class="toggle">
                                Hari Ini
                            </label>
                            <label class="btn red btn-outline btn-circle btn-sm"
                                   v-bind:class="[optionOrderSum == 'month' ? 'active' : '']"
                                   v-on:click="setOptionSum('month')">
                                <input type="radio" v-model="optionOrderSum" value="month"
                                       class="toggle">
                                Bulan Ini
                            </label>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="chartOrderSum"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Banyak Order</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <label class="btn red btn-outline btn-circle btn-sm"
                                   v-bind:class="[optionOrderCount == 'day' ? 'active' : '']"
                                   v-on:click="setOptionCount('day')">
                                <input type="radio" v-model="optionOrderCount" value="day"
                                       class="toggle">
                                Hari Ini
                            </label>
                            <label class="btn red btn-outline btn-circle btn-sm"
                                   v-bind:class="[optionOrderCount == 'month' ? 'active' : '']"
                                   v-on:click="setOptionCount('month')">
                                <input type="radio" v-model="optionOrderCount" value="month"
                                       class="toggle">
                                Bulan Ini
                            </label>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="chartOrderCount"></div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.merchant.partials.dashboardstat')
@endsection

@section('scripts')
    <script src="/js/finance/dashboard.js"></script>
@endsection