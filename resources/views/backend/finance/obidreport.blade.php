@extends('backend.base')

@section('title')
    <title>Original Brands Financial Report</title>
@endsection

@section('menu')
    @include('backend.finance.partials.menu')
@endsection

@section('menuuser')
    @include('backend.finance.partials.usermenu')
@endsection

@section('morecss')
    <link rel="stylesheet" href="/css/finance/obidreport.css">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="orders">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject theme-font-color bold uppercase">
                            Obid Financial Report
                        </span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row margin-bottom-10">
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" v-datepicker="date">
                                </div>
                                <div class="col-md-6">
                                    <button class="btn btn-primary" v-on:click="setToday">Set Today</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select class="form-control" v-model="options">
                                    <option value="day">day</option>
                                    <option value="month">month</option>
                                    <option value="year">year</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-primary" v-on:click="getData">Generate Report</button>
                        </div>
                    </div>
                    <div class="row margin-bottom-10">
                        <div class="col-md-12">
                            <h3>Grafik Produktivitas Merchant</h3>

                            <div id="chart-merchant"></div>
                        </div>
                    </div>
                    <div class="row margin-bottom-10">
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <order-stat :color="'blue'" :value="totalProfitMerchant*1 | formatMoney"
                                        :title="'Total Profit Merchant OBID'"
                                        :icon="'fa fa-comments'" :link="''"></order-stat>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <order-stat :color="'red'" :value="totalKuponSum*1 | formatMoney"
                                        :title="'Total Penggunaan Voucher'"
                                        :icon="'fa fa-bar-chart-o'" :link="''"></order-stat>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <order-stat :color="'green'" :value="totalOrderSum*1 | formatMoney"
                                        :title="'Total Profit Order'"
                                        :icon="'fa fa-shopping-cart'" :link="''"></order-stat>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 text-stat">
                            <order-stat :color="'purple'" :value="totalShipping*1 | formatMoney"
                                        :title="'Total Pemasukan Shipping'"
                                        :icon="'fa fa-globe'" :link="''"></order-stat>
                        </div>
                    </div>
                    <div class="row margin-bottom-10">
                        <div class="col-md-4 col-sm-4 col-xs-6 text-stat">
                            <order-stat :color="'blue'" :value="pemasukan*1 | formatMoney"
                                        :title="'Total Pemasukan'"
                                        :icon="'fa fa-comments'" :link="''">
                            </order-stat>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6 text-stat">
                            <order-stat :color="'red'" :value="pengeluaran*1 | formatMoney"
                                        :title="'Total Pengeluaran'"
                                        :icon="'fa fa-shopping-cart'" :link="''"></order-stat>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6 text-stat">
                            <order-stat :color="'green'" :value="grandTotal*1 | formatMoney"
                                        :title="'Grand Total'"
                                        :icon="'fa fa-shopping-cart'" :link="''"></order-stat>
                        </div>
                    </div>
                    <div class="row margin-bottom-10">
                        <div class="col-md-3">
                            <h2>Penjualan Merchant</h2>

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Merchant</th>
                                        <th>Total Penjualan</th>
                                        <th>Profit OBID</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="entry in profitMerchant" v-cloak>
                                        <td>
                                            @{{ entry['username'] }}
                                            <a :href="'/report?user='+entry.userid+'&type=month&date='+date"
                                               target="_blank">Detail</a>
                                        </td>
                                        <td>
                                            @{{ entry.value | formatMoney }}
                                        </td>
                                        <td>
                                            @{{ entry.profit | formatMoney }}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <h2>Penggunaan Voucher</h2>

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Id Kupon</th>
                                        <th>Nilai Kupon</th>
                                        <th>Digunakan Pada Order</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="entry in dataKupon" v-cloak>
                                        <td>
                                            @{{ entry['id'] }}
                                        </td>
                                        <td>
                                            IDR @{{ entry['value'] | formatNumber }}
                                        </td>
                                        <td>
                                            #@{{ entry['orders'][0]['id'] }}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <h2>Banyak Order Merchant</h2>

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Merchant</th>
                                        <th>Banyak Order</th>
                                        <th>Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="entry in dataOrder" v-cloak>
                                        <td>
                                            @{{ entry['username'] }}
                                        </td>
                                        <td>
                                            @{{ entry['count'] }}
                                        </td>
                                        <td>
                                            @{{ entry.count * 5000 | formatMoney }}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <h2>Pengiriman</h2>

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Kode Order</th>
                                        <th>Tanggal</th>
                                        <th>Keuntungan</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="entry in dataShipping" v-cloak>
                                        <td>
                                            @{{ entry['order_code'] }}
                                        </td>
                                        <td>
                                            @{{ entry['shipping_date'] }}
                                        </td>
                                        <td>
                                            @{{ 2000 | formatMoney }}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.merchant.partials.dashboardstat')
@endsection

@section('scripts')
    <script src="/js/finance/obidreport.js"></script>
@endsection