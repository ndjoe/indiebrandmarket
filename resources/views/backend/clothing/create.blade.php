@extends('backend.base')

@section('title')
    <title>Create New User</title>
@endsection

@section('morecss')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-body form" id="form">
                    <form v-on="submit: submitAdmin" class="form-horizontal">
                        <h3 class="form-section">Account Info</h3>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Username
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="username">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Email
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="email">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Password
                                        </label>

                                        <div class="col-md-9">
                                            <input type="password" class="form-control" v-model="password">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Confirm Password
                                        </label>

                                        <div class="col-md-9">
                                            <input type="password" class="form-control" v-model="passwordConfirm">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3 class="form-section">
                                Logo
                            </h3>

                            <div class="row">
                                <div class="col-md-6">
                                    <img src="/images/logo/default-logo.png" alt=""
                                         class="img-responsive" id="logo-preview">
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Logo Image
                                        </label>

                                        <div class="col-md-9">
                                            <input type="file" class="form-control" v-el="logoInput" id="logo-image">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3 class="form-section">
                                Account Profile
                            </h3>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" v-class="has-error: validation.nama.required">
                                        <label class="control-label col-md-3">
                                            Nama Brand
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="nama"
                                                   v-validate="required">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Nama Pemilik
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="owner">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Nomor Handphone
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="nohp">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            About
                                        </label>

                                        <div class="col-md-9">
                                            <textarea cols="30" rows="10" class="form-control"
                                                      v-model="about"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                    Bank
                                                </label>

                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" v-model="bank">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                    Atas Nama
                                                </label>

                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" v-model="rekeningOwner">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Nomor Rekening
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="norek">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Kota
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="kota">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Provinsi
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="provinsi">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Kode Pos
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="kodepos">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Alamat
                                        </label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" v-model="alamat">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-9">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/assets/plugins/validator.min.js"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            Metronic.init();
            Layout.init();
            $('#banner-image').change(function () {
                readBannerURL(this);
            });
            $('#logo-image').change(function () {
                readLogoURL(this);
            });
        });
        function readBannerURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#banner-preview').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function readLogoURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#logo-preview').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        var vm = new Vue({
            el: '#form',
            data: {
                username: '',
                email: '',
                password: '',
                passwordConfirm: '',
                nama: '',
                owner: '',
                nohp: '',
                bank: '',
                norek: '',
                about: '',
                kota: '',
                kodepos: '',
                alamat: '',
                provinsi: ''
            },
            computed: {
                validator: function () {

                }
            },
            methods: {
                submitAdmin: function (e) {
                    e.preventDefault();
                    var formData = new FormData;
                    if (!validator.isNull(this.password)) {
                        formData.append('password', this.password);
                        formData.append('password_confirmation', this.passwordConfirm);
                    }
                    if (this.$$.logoInput.files.length > 0) {
                        formData.append('logoImage', this.$$.logoInput.files[0]);
                    }
                    formData.append('username', this.username);
                    formData.append('email', this.email);
                    formData.append('nama', this.nama);
                    formData.append('kota', this.kota);
                    formData.append('owner', this.owner);
                    formData.append('bank', this.bank);
                    formData.append('provinsi', this.provinsi);
                    formData.append('norek', this.norek);
                    formData.append('kodepos', this.kodepos);
                    formData.append('nohp', this.nohp);
                    formData.append('about', this.about);
                    formData.append('alamat', this.alamat);

                    $.ajax({
                        url: '/user/api/account/edit',
                        data: formData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        aync: false,
                        type: 'POST'
                    }).done(function (data) {
                        if (data.updated) {
                            window.location.href = data.redirect;
                        } else {
                            console.log(data);
                        }
                    });
                }
            },
            components: {},
            ready: function () {
                this.about = $('#about').val();
            }
        });
    </script>
@endsection