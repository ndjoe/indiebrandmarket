<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
@include('email.partials.oneheader_head')
<body>
<center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        <tr>
            <td align="center" valign="top" id="bodyCell">
                <!-- BEGIN TEMPLATE // -->
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center" valign="top" id="templatePreheader">
                            <!--[if gte mso 9]>
                            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600"
                                   style="width:600px;">
                                <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                            <![endif]-->
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                                   class="templateContainer">
                                <tr>
                                    <td valign="top" class="preheaderContainer">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                               class="mcnTextBlock">
                                            <tbody class="mcnTextBlockOuter">
                                            <tr>
                                                <td valign="top" class="mcnTextBlockInner">

                                                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                                                           width="366" class="mcnTextContentContainer">
                                                        <tbody>
                                                        <tr>

                                                            <td valign="top" class="mcnTextContent"
                                                                style="padding: 9px 0px 9px 18px; line-height: normal;">

                                                                Use this area to offer a short preview of your email's
                                                                content.
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                    <table align="right" border="0" cellpadding="0" cellspacing="0"
                                                           width="197" class="mcnTextContentContainer">
                                                        <tbody>
                                                        <tr>

                                                            <td valign="top" class="mcnTextContent"
                                                                style="padding: 9px 18px 9px 0px; line-height: normal;">

                                                                <a href="*|ARCHIVE|*" target="_blank">View this email in
                                                                    your browser</a>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!--[if gte mso 9]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" id="templateHeader">
                            <!--[if gte mso 9]>
                            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600"
                                   style="width:600px;">
                                <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                            <![endif]-->
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                                   class="templateContainer">
                                <tr>
                                    <td valign="top" class="headerContainer">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                               class="mcnImageBlock" style="min-width:100%;">
                                            <tbody class="mcnImageBlockOuter">
                                            <tr>
                                                <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                                                    <table align="left" width="100%" border="0" cellpadding="0"
                                                           cellspacing="0" class="mcnImageContentContainer"
                                                           style="min-width:100%;">
                                                        <tbody>
                                                        <tr>
                                                            <td class="mcnImageContent" valign="top"
                                                                style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">

                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!--[if gte mso 9]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" id="templateBody">
                            <!--[if gte mso 9]>
                            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600"
                                   style="width:600px;">
                                <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                            <![endif]-->
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                                   class="templateContainer">
                                <tr>
                                    <td valign="top" class="bodyContainer">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                               class="mcnTextBlock">
                                            <tbody class="mcnTextBlockOuter">
                                            <tr>
                                                <td valign="top" class="mcnTextBlockInner">

                                                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                                                           width="600" class="mcnTextContentContainer">
                                                        <tbody>
                                                        <tr>

                                                            <td valign="top" class="mcnTextContent"
                                                                style="padding: 9px 18px; line-height: normal;">

                                                                <h1>It's time to design your email.</h1>

                                                                <p style="line-height: normal;">Now that you've selected
                                                                    a template, you'll define the layout of your email
                                                                    and give your content a place to live by adding,
                                                                    rearranging, and deleting content blocks.</p>

                                                                <p style="line-height: normal;">When you're ready to
                                                                    change the look of your email, take a look through
                                                                    the “design” tab to set background colors, borders,
                                                                    and other styles.</p>

                                                                <p style="line-height: normal;">If you need a bit of
                                                                    inspiration, you can <a
                                                                            href="http://inspiration.mailchimp.com"
                                                                            class="mc-template-link">see what other
                                                                        MailChimp users are doing</a>, or <a
                                                                            href="http://mailchimp.com/resources/email-design-guide/"
                                                                            class="mc-template-link">learn about email
                                                                        design</a> and blaze your own trail.</p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!--[if gte mso 9]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" id="templateFooter">
                            <!--[if gte mso 9]>
                            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600"
                                   style="width:600px;">
                                <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                            <![endif]-->
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                                   class="templateContainer">
                                <tr>
                                    <td valign="top" class="footerContainer">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                               class="mcnFollowBlock">
                                            <tbody class="mcnFollowBlockOuter">
                                            <tr>
                                                <td align="center" valign="top" style="padding:9px"
                                                    class="mcnFollowBlockInner">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                                           class="mcnFollowContentContainer">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center"
                                                                style="padding-left:9px;padding-right:9px;">
                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       width="100%" class="mcnFollowContent">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td align="center" valign="top"
                                                                            style="padding-top:9px; padding-right:9px; padding-left:9px;">
                                                                            <table border="0" cellpadding="0"
                                                                                   cellspacing="0">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        <!--[if mso]>
                                                                                        <table align="left" border="0"
                                                                                               cellspacing="0"
                                                                                               cellpadding="0"
                                                                                               width="524">
                                                                                            <tr>
                                                                                                <td align="left"
                                                                                                    valign="top"
                                                                                                    width="524">
                                                                                        <![endif]-->


                                                                                        <table align="left" border="0"
                                                                                               cellpadding="0"
                                                                                               cellspacing="0">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td valign="top"
                                                                                                    style="padding-right:10px; padding-bottom:9px;"
                                                                                                    class="mcnFollowContentItemContainer">
                                                                                                    <table border="0"
                                                                                                           cellpadding="0"
                                                                                                           cellspacing="0"
                                                                                                           width="100%"
                                                                                                           class="mcnFollowContentItem">
                                                                                                        <tbody>
                                                                                                        <tr>
                                                                                                            <td align="left"
                                                                                                                valign="middle"
                                                                                                                style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                                                <table align="left"
                                                                                                                       border="0"
                                                                                                                       cellpadding="0"
                                                                                                                       cellspacing="0"
                                                                                                                       width="">
                                                                                                                    <tbody>
                                                                                                                    <tr>

                                                                                                                        <td align="center"
                                                                                                                            valign="middle"
                                                                                                                            width="24"
                                                                                                                            class="mcnFollowIconContent">
                                                                                                                            <a href="http://www.twitter.com/"
                                                                                                                               target="_blank"><img
                                                                                                                                        src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png"
                                                                                                                                        style="display:block;"
                                                                                                                                        height="24"
                                                                                                                                        width="24"
                                                                                                                                        class=""></a>
                                                                                                                        </td>


                                                                                                                    </tr>
                                                                                                                    </tbody>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>


                                                                                        <table align="left" border="0"
                                                                                               cellpadding="0"
                                                                                               cellspacing="0">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td valign="top"
                                                                                                    style="padding-right:10px; padding-bottom:9px;"
                                                                                                    class="mcnFollowContentItemContainer">
                                                                                                    <table border="0"
                                                                                                           cellpadding="0"
                                                                                                           cellspacing="0"
                                                                                                           width="100%"
                                                                                                           class="mcnFollowContentItem">
                                                                                                        <tbody>
                                                                                                        <tr>
                                                                                                            <td align="left"
                                                                                                                valign="middle"
                                                                                                                style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                                                <table align="left"
                                                                                                                       border="0"
                                                                                                                       cellpadding="0"
                                                                                                                       cellspacing="0"
                                                                                                                       width="">
                                                                                                                    <tbody>
                                                                                                                    <tr>

                                                                                                                        <td align="center"
                                                                                                                            valign="middle"
                                                                                                                            width="24"
                                                                                                                            class="mcnFollowIconContent">
                                                                                                                            <a href="http://www.facebook.com"
                                                                                                                               target="_blank"><img
                                                                                                                                        src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png"
                                                                                                                                        style="display:block;"
                                                                                                                                        height="24"
                                                                                                                                        width="24"
                                                                                                                                        class=""></a>
                                                                                                                        </td>


                                                                                                                    </tr>
                                                                                                                    </tbody>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>


                                                                                        <table align="left" border="0"
                                                                                               cellpadding="0"
                                                                                               cellspacing="0">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td valign="top"
                                                                                                    style="padding-right:0; padding-bottom:9px;"
                                                                                                    class="mcnFollowContentItemContainer">
                                                                                                    <table border="0"
                                                                                                           cellpadding="0"
                                                                                                           cellspacing="0"
                                                                                                           width="100%"
                                                                                                           class="mcnFollowContentItem">
                                                                                                        <tbody>
                                                                                                        <tr>
                                                                                                            <td align="left"
                                                                                                                valign="middle"
                                                                                                                style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                                                <table align="left"
                                                                                                                       border="0"
                                                                                                                       cellpadding="0"
                                                                                                                       cellspacing="0"
                                                                                                                       width="">
                                                                                                                    <tbody>
                                                                                                                    <tr>

                                                                                                                        <td align="center"
                                                                                                                            valign="middle"
                                                                                                                            width="24"
                                                                                                                            class="mcnFollowIconContent">
                                                                                                                            <a href="http://mailchimp.com"
                                                                                                                               target="_blank"><img
                                                                                                                                        src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png"
                                                                                                                                        style="display:block;"
                                                                                                                                        height="24"
                                                                                                                                        width="24"
                                                                                                                                        class=""></a>
                                                                                                                        </td>


                                                                                                                    </tr>
                                                                                                                    </tbody>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>


                                                                                        <!--[if mso]>
                                                                                        </td>
                                                                                        </tr>
                                                                                        </table>
                                                                                        <![endif]-->
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                               class="mcnDividerBlock" style="min-width:100%;">
                                            <tbody class="mcnDividerBlockOuter">
                                            <tr>
                                                <td class="mcnDividerBlockInner"
                                                    style="min-width: 100%; padding: 10px 18px 25px;">
                                                    <table class="mcnDividerContent" border="0" cellpadding="0"
                                                           cellspacing="0" width="100%"
                                                           style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EEEEEE;">
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <span></span>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <!--
                                                                    <td class="mcnDividerBlockInner" style="padding: 18px;">
                                                                    <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
                                                    -->
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                               class="mcnTextBlock">
                                            <tbody class="mcnTextBlockOuter">
                                            <tr>
                                                <td valign="top" class="mcnTextBlockInner">

                                                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                                                           width="600" class="mcnTextContentContainer">
                                                        <tbody>
                                                        <tr>

                                                            <td valign="top" class="mcnTextContent"
                                                                style="padding: 9px 18px; line-height: normal;">

                                                                <em>Copyright © 2015 originalbrands.id, All rights
                                                                    reserved.</em>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!--[if gte mso 9]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                        </td>
                    </tr>
                </table>
                <!-- // END TEMPLATE -->
            </td>
        </tr>
    </table>
</center>
</body>
</html>