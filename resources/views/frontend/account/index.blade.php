@extends('frontend.frontend')

@section('content')
    <div class="row margin-bottom-40">
        <div class="sidebar col-md-3 col-sm-3">
            <ul class="list-group margin-bottom-25 sidebar-menu">
                <li class="list-group-item clearfix">
                    <a href="/account">
                        <i class="fa fa-angle-right">
                        </i>
                        Histori Transaksi
                    </a>
                </li>
                <li class="list-group-item clearfix">
                    <a href="/account/profile">
                        <i class="fa fa-angle-right">
                        </i>
                        My Profile
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-md-9 col-sm-7">
            <h1>Histori Transaksi</h1>

            <div class="content-page">
                <div class="table-wrapper-responsive">
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <th>No Order</th>
                            <th>Tanggal Order</th>
                            <th>Harga</th>
                            <th>Tipe Pengiriman</th>
                            <th>Status</th>
                            <th>No Resi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $o)
                            <tr>
                                <th>
                                    {{ $o->order_code }}
                                </th>
                                <th>
                                    {{ $o->created_at }}
                                </th>
                                <th>
                                    {{ $o->currency }} {{ number_format($o->grossTotal - $o->uniqueCost,0, ',', '.') }}
                                </th>
                                <th>
                                    JNE - {{ $o->shippingType }}
                                </th>
                                <th>
                                    @if($o->confirmed_at === null && $o->shipping === null && $o->expired_at > \Carbon\Carbon::now() && $o->cancelled_at === null)
                                        Pending
                                    @elseif($o->confirmed_at !== null && $o->shipping === null)
                                        Paid
                                    @elseif($o->confirmed_at === null && ($o->expired_at < \Carbon\Carbon::now() || $o->cancelled_at !== null))
                                        Expired/Cancelled
                                    @elseif($o->confirmed_at !== null && $o->shipping !== null)
                                        Sent
                                    @endif
                                </th>
                                <th>
                                    @if($o->shipping !== null)
                                        {{ $o->shipping->no_resi }}
                                    @else
                                        Belum Dikirim
                                    @endif
                                </th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/js/shop.js"></script>
@endsection