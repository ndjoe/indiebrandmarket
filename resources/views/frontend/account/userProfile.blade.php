@extends('frontend.frontend')

@section('title')
    <title>Edit Your Profile</title>
@endsection

@section('morecss')
@endsection

@section('content')
    <div class="row margin-bottom-40">
        <div class="sidebar col-md-3 col-sm-3">
            <ul class="list-group margin-bottom-25 sidebar-menu">
                <li class="list-group-item clearfix">
                    <a href="/account">
                        <i class="fa fa-angle-right">
                        </i>
                        Histori Transaksi
                    </a>
                </li>
                <li class="list-group-item clearfix">
                    <a href="/account/profile">
                        <i class="fa fa-angle-right">
                        </i>
                        My Profile
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-md-9 col-sm-7">
            <h1>My Profile</h1>

            <div class="content-form-page">
                <form class="form-horizontal" v-on:submit="saveProfile">
                    <div class="form-group">
                        <label for="name" class="col-lg-2 control-label">
                            Nama
                        </label>

                        <div class="col-lg-8">
                            <input type="text" id="name" class="form-control" v-model="nama">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nohp" class="col-lg-2 control-label">
                            Nomor Handphone
                        </label>

                        <div class="col-lg-8">
                            <input type="text" id="nohp" class="form-control" v-model="nohp">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="birthday" class="col-lg-2 control-label">
                            Birthday
                        </label>

                        <div class="col-lg-8">
                            <fieldset class="birthday-picker">
                                <select name="year" id="year" class="year_picker" v-model="year">
                                    <option value="0">Tahun</option>
                                    <option v-for="d in listYear" class="form-control"
                                            :value="d.value">@{{ d.text }}</option>
                                </select>
                                <select name="month" id="month" class="month_picker" v-model="month">
                                    <option value="0">Bulan</option>
                                    <option v-for="d in listMonth" class="form-control"
                                            :value="d.value">@{{ d.text }}</option>
                                </select>
                                <select name="date" id="date" class="date_picker" v-model="date">
                                    <option value="0">Tgl</option>
                                    <option v-for="d in listDate" class="form-control"
                                            :value="d.value">@{{ d.text }}</option>
                                </select>
                            </fieldset>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="gender" class="col-lg-2 control-label">
                            Gender
                        </label>

                        <div class="col-lg-8">
                            <select id="gender" class="form-control" v-model="gender">
                                <option value="">Saya tidak tahu</option>
                                <option value="male">Man</option>
                                <option value="female">Woman</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="provinsi" class="col-lg-2 control-label">
                            Provinsi
                        </label>

                        <div class="col-lg-8">
                            <select v-selectize="provinsi" class="form-control" options="listProvince"
                                    placeholder="Pilih Provinsi"
                                    settings="{create: false, selectOnTab: false}"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="kota" class="col-lg-2 control-label">
                            Kota
                        </label>

                        <div class="col-lg-8">
                            <select v-selectize="kota" class="form-control" options="listCity"
                                    placeholder="Pilih Kota"
                                    settings="{create: false, selectOnTab: false}"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="kodepos" class="col-lg-2 control-label">
                            Kode Pos
                        </label>

                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="kodepos" v-model="kodepos">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="alamat" class="col-lg-2 control-label">
                            Alamat
                        </label>

                        <div class="col-lg-8">
                            <input type="text" id="alamat" cols="30" rows="10" class="form-control"
                                   v-model="alamat">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-10">
                            <div class="well profile-overview">
                                <p v-cloak class="nama">@{{ profile.nama }}</p>
                                <p v-cloak class="alamat">
                                    @{{ profile.profileable.alamat }},
                                    @{{ profile.profileable.kota }}
                                </p>
                                <p v-cloak class="alamat">
                                    @{{ profile.profileable.provinsi }}
                                    Indonesia @{{ profile.profileable.kodepos }}
                                </p>
                                <p v-cloak class="nohp">Tel: @{{ profile.profileable.nohp }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-button">
                            <button type="submit" class="btn btn-primary" v-show="!submitting">Save Profile</button>
                            <button type="button" class="btn btn-primary" disabled v-show="submitting" v-cloak>
                                <i class="fa fa-spinner fa-spin"></i>
                                Saving...
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/js/shop-account.js"></script>
@endsection