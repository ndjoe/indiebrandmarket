<!DOCTYPE html>
<html lang="{{ LaravelLocalization::getCurrentLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="_token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    @yield('title')
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Roboto:300,400,600,700|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all"
          rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/shop-assets/css/main.css">
    @yield('morecss')
</head>

<body class="ecommerce" id="shop">
<div id="body-overlay" style="display: none;"></div>
<div class="mobile-header">
    <div class="container">
        <div class="row">
            <div class="col-md-1 col-xs-10 brand-menu">
                <ul class="nav nav-pills">
                    <li class="toggler-section">
                        <a href="#" class="menu-toggler">
                            <i class="fa fa-bars"></i>
                        </a>
                    </li>
                    <li class="logo-section">
                        <a href="/">
                            <img src="/img/OBIDSTORE-white.png" class="img-responsive" alt="Originalbrands">
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-9 hidden-xs main-menu">
                <div class="header-navigation">
                    <ul class="nav nav-pills">
                        <li><a href="/">Magazine</a></li>
                        <li><a href="/new-arrival">What's New</a></li>
                        <li><a href="/brands">Brands</a></li>
                        <li><a href="/category">Categories</a></li>
                        <li><a href="/sale">Sale</a></li>
                        @if(Auth::check())
                            <li><a href="/confirm-order">Confirm Order</a></li>
                            <li><a href="/account">My Profile</a></li>
                            <li><a href="/logout">Logout</a></li>
                        @else
                            <li><a href="/login">Login/Register</a></li>
                        @endif
                    </ul>
                </div>
            </div>
            <div class="col-md-2 col-xs-2 cart-menu">
                <ul class="nav nav-pills">
                    @foreach(LaravelLocalization::getSupportedLocales() as $locale => $properties)
                        <li class="currency-section @if(LaravelLocalization::getCurrentLocale() === $locale) active @endif">
                            <a class="currency" href="{{ LaravelLocalization::getLocalizedURL($locale) }}"
                               hreflang="{{ $locale }}">
                                @if($locale === 'id')
                                    IDR
                                @else
                                    USD
                                @endif
                            </a>
                        </li>
                    @endforeach
                    <li class="cart-section">
                        <a href="/cart" class="cart-info">
                            <i class="fa fa-shopping-cart"></i>
                            <span class="cart-count" v-text="cart | sumArray" v-cloak></span>
                        </a>
                        <ul class="cart dropdown-menu pull-right menu_level_1 cart-content">
                            <li class="first">
                                <a href="/cart" class="checkout-now">
                                    <template v-if="cart.length > 0">
                                        Check Out Now!
                                    </template>
                                    <template v-else>
                                        0 items in your cart
                                    </template>
                                </a>
                            </li>
                            <li v-for="c in cart">
                                <div class="row cart-item">
                                    <div class="cart-media col-md-10 col-xs-10">
                                        <a class="media" :href="'/brand/'+c.options.brand+'/'+c.options.slug">
                                            <div class="pull-left">
                                                <img class="media-object"
                                                     :src="'/images/catalog/'+c.options.image">
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">
                                                    @{{ c.name }}
                                                </h5>
                                        <span class="item-price">
                                            @{{ currency }} @{{ c.price }}<br>
                                        </span>
                                        <span class="item-options">
                                            Size: @{{ c.options.sizeText | uppercase }}<br> Qty: @{{ c.qty }}
                                        </span>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="cart-delete col-md-2 col-xs-2">
                                        <a href="javascript:;" class="delete-button" v-on:click="delItem(c)">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="slide-menu">
    <div class="slide-menu-header">
        <a href="/">
            <img src="/img/OBIDSTORE.png" class="logo" alt="Originalbrands">
        </a>
        <a href="#" class="close-btn">x</a>
    </div>
    <div class="slide-menu-content">
        <ul class="nav nav-list">
            <li class="first">
                <a href="/">Magazine</a>
            </li>
            <li><a href="/new-arrival">What's New</a></li>
            <li><a href="/brands">Brands</a></li>
            <li><a href="/category">Categories</a></li>
            <li><a href="/sale">Sale</a></li>
            <li class="currency-menu">
                <ul class="nav nav-pills">
                    @foreach(LaravelLocalization::getSupportedLocales() as $locale => $properties)
                        <li class="currency-section @if(LaravelLocalization::getCurrentLocale() === $locale) active @endif">
                            <a class="currency" href="{{ LaravelLocalization::getLocalizedURL($locale) }}"
                               hreflang="{{ $locale }}">
                                @if($locale === 'id')
                                    IDR
                                @else
                                    USD
                                @endif
                            </a>
                        </li>
                    @endforeach
                </ul>
            </li>
        @if(Auth::check())
                <li><a href="/confirm-order">Confirm Order</a></li>
                <li><a href="/account">My Profile</a></li>
                <li><a href="/logout">Logout</a></li>
            @else
                <li><a href="/login">Login/Register</a></li>
            @endif
        </ul>
    </div>
</div>
<div class="main">
    <div class="container">
        @yield('content')
    </div>
</div>
@include('frontend.partials.preFooter')
<div class="pre-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4 pre-footer-col">
                <h2 class="text-center">About us</h2>

                <p class="text-center">Originalbrands.id is an online magazine that inform Indonesia Local fashion and
                    supported by an
                    online store that has been integrated with the local fashion community so that the information
                    seekers or fashion lovers can easily search for high quality local products.</p>
            </div>
            <div class="col-md-3 col-sm-4 pre-footer-col">
                <h2 class="text-center">Information</h2>
                <ul class="list-unstyled">
                    <li><i class="fa fa-angle-right"></i><a href="/confirm-order">Confirm Order</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="/bantuan">Bantuan Pertanyaan Umum</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="/privasi">Kebijakan Privasi</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="/pengiriman">Ketentuan Pengiriman</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="/tracking">Order Tracking</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="/pengembalian">Syarat Dan Ketentuan Pengembalian</a>
                    </li>
                    <li><i class="fa fa-angle-right"></i> <a href="/terms">Syarat dan Ketentuan Penggunaan</a></li>
                    <li><i class="fa fa-angle-right"></i><a href="/contact-us">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-4 pre-footer-col">
                <h2 class="text-center">Payment Methods</h2>
                <h6 class="subtitle">Online Payment</h6>
                <ul class="list-inline pull-left list-unstyled">
                    <li>
                        <img src="/img/bcaklikpay-logo-originalbrands.png" class="img-responsive" alt="mandiri"
                             width="75px">
                    </li>
                    <li>
                        <img src="/img/mandiriclickpay-logo-originalbrands.png" class="img-responsive" alt="mandiri"
                             width="75px">
                    </li>
                    <li>
                        <img src="/img/cimbclick-logo-originalbrands.png" class="img-responsive" alt="mandiri"
                             width="75px">
                    </li>
                    <li>
                        <img src="/img/mastercard-logo-originalbrands.png" class="img-responsive" alt="mandiri"
                             width="75px">
                    </li>

                    <li>
                        <img src="/img/visa-logo-originalbrands.png" class="img-responsive" alt="mandiri"
                             width="75px">
                    </li>
                </ul>
                <h6 class="subtitle">Transfer</h6>
                <ul class="list-inline pull-left list-unstyled">
                    <li>
                        <img src="/img/mandiri-logo-originalbrands.png" class="img-responsive" alt="mandiri"
                             width="75px">
                    </li>
                    <li>
                        <img src="/img/bca-logo-originalbrands.png" class="img-responsive" alt="mandiri" width="75px">
                    </li>
                    <li>
                        <img src="/img/bri-logo-originalbrands.png" class="img-responsive" alt="mandiri" width="75px">
                    </li>
                    <li>
                        <img src="/img/cimb-logo-originalbrands.png" class="img-responsive" alt="mandiri" width="75px">
                    </li>
                    <li>
                        <img src="/img/indomaret-logo-originalbrands.png" class="img-responsive" alt="mandiri"
                             width="75px">
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-4 pre-footer-col">
                <h2 class="text-center">Shipping Methods</h2>
                <h6 class="subtitle">Couriers</h6>
                <ul class="list-inline pull-left list-unstyled">
                    <li>
                        <img src="/img/jne-logo-originalbrands.png" class="img-responsive" alt="mandiri"
                             width="75px">
                    </li>
                    <li>
                        <img src="/img/fedex-logo-originalbrands.png" class="img-responsive" alt="mandiri" width="75px">
                    </li>
                    <li>
                        <img src="/img/rpx-logo-originalbrands.png" class="img-responsive" alt="mandiri" width="75px">
                    </li>
                    <li>
                        <img src="/img/pos-logo-originalbrands.png" class="img-responsive" alt="mandiri" width="75px">
                    </li>
                    <li>
                        <img src="/img/wahana-logo-originalbrands.png" class="img-responsive" alt="mandiri"
                             width="75px">
                    </li>
                </ul>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6 col-sm-6">

            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 padding-top-10">
                2015 © Originalbrands. ALL Rights Reserved.
            </div>
            <div class="col-md-6 col-sm-6">
                <ul class="list-unstyled list-inline pull-right">

                </ul>
            </div>
        </div>
    </div>
</div>
@include('js')
<script src="http://instansive.com/widget/js/instansive.js" async></script>
<script type="text/javascript" src="/shop-assets/js/main.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    Vue.filter('sumArray', function (data) {
        var qty = 0;
        data.forEach(function (d) {
            qty += d.qty * 1;
        });

        return qty;
    });

    Vue.filter('sumPrice', function (data) {
        var qty = data['qty'] * 1;
        var price = data['price'] * 1;

        return qty * price;
    });
</script>
@yield('pagescripts')
</body>
</html>
