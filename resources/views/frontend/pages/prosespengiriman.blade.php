@extends('frontend.frontend')

@section('title')

@endsection

@section('morecss')
@endsection


@section('content')
    <div class="row margin-bottom-10">
        <div class="col-md-12">
            <h1>Ketentuan Pengiriman Produk</h1>

            <div class="content-page">
                <h2>
                    PROSES PENGIRIMAN PRODUK
                </h2>

                <p>
                    Pemesanan akan diproses akan setelah pembayaran atau konfirmasi pembayaran telah kami terima.
                </p>

                <h2>
                    WAKTU PENGIRIMAN
                </h2>

                <p>
                    Setiap Hari Pada Pukul 20.00 WIB (kecuali tanggal Merah atau Libur Nasional)<br>
                    Jika pemesanan dilakukan lebih dari pukul 20.00 WIB maka pengiriman akan dilakukan pengiriman pada
                    hari berikutnya
                </p>

                <h2>
                    BIAYA PENGIRIMAN
                </h2>

                <p>
                    Biaya pengiriman tergantung harga jasa pengiriman sesuai lokasi pengiriman
                </p>

                <h2>
                    Cek Status Pengiriman
                </h2>

                <p>
                    Originalbrands akan mengirimkan informasi pengiriman melalui sms & email beserta tautan untuk cek
                    status pesanan dan order tracking
                </p>

                <p>
                    Pelanggan dapat cek ke website masing - masing jasa pengiriman atau pada halaman order tracking kami
                </p>

                <p>
                    Originalbrands bekerja sama dengan beberapa jasa pengiriman sebagai berikut:
                </p>

                <ul>
                    <li>
                        Nasional
                        <p>
                            <img src="/img/jne-logo-originalbrands.png" alt="jne"
                                 width="75px">
                            <img src="/img/pos-logo-originalbrands.png" alt="pos"
                                 width="75px">
                            <img src="/img/wahana-logo-originalbrands.png" alt="wahana"
                                 width="75px">
                        </p>
                    </li>
                    <li>
                        Internasional
                        <p>
                            <img src="/img/fedex-logo-originalbrands.png"
                                 alt="fedex" width="75px">
                            <img src="/img/rpx-logo-originalbrands.png" alt="rpx"
                                 width="75px">
                        </p>
                    </li>
                </ul>

                <p>
                    Bisa, Klik 'Ubah Alamat' pada menu 'Data Anda​' sebelum klik 'Pesan'
                </p>

                <h2>
                    GANTI ALAMAT
                </h2>

                <p>
                    Pelanggan dapat menghubungi cs@originalbrands.co.id untuk mengganti alamat pengiriman sebelum
                    minimal 5 jam sebelum waktu pengiriman dari pembayaran atau konfirmasi pembayaran kami terima
                </p>

                <h2>
                    APAKAH PELANGGAN DAPAT MENGIRIM KE ALAMAT YANG BERBEDA DARI ALAMAT RUMAH?
                </h2>

                <p>
                    Bisa, pada halaman check out sebelum submit, dengan mengubah Alamat Pengiriman pada table Data Diri
                </p>
            </div>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/js/shop.js"></script>
@endsection