@extends('frontend.frontend')

@section('title')

@endsection

@section('morecss')
@endsection


@section('content')
    <div class="row margin-bottom-10">
        <div class="col-md-12">
            <h1>KEBIJAKAN PRIVASI</h1>

            <div class="content-page">
                <h2>Kami menjaga privasi pelanggan kami dengan serius dan kami hanya akan mengumpulkan, merekam,
                    menahan, menyimpan, mengungkapkan, dan menggunakan informasi pribadi Anda seperti yang diuraikan di
                    bawah.</h2>

                <p>
                    Perlindungan data adalah masalah kepercayaan dan privasi Anda sangat penting bagi kami. Kami hanya
                    akan menggunakan nama Anda dan informasi yang berhubungan dengan Anda sebagaimana yang dinyatakan
                    dalam Kebijakan Privasi berikut. Kami hanya akan mengumpulkan informasi yang kami perlukan dan kami
                    hanya akan mengumpulkan informasi yang relevan dengan transaksi antara kami dengan Anda.
                </p>

                <p>
                    Kami hanya akan menyimpan informasi privasi Anda selama diwajibkan oleh hukum atau selama informasi
                    tersebut masih relevan dengan tujuan pengumpulan informasi.
                </p>

                <p>
                    Anda dapat mengunjungi Platform kami (Seperti yang dijelaskan pada Syarat & Ketentuan) dan
                    menjelajahinya tanpa harus memberikan informasi pribadi. Selama kunjungan Anda ke situs kami,
                    identitas Anda akan tetap terjaga dan kami tidak akan bisa mengidentifikasi Anda kecuali Anda
                    memiliki akun dalam situs kami dan masuk dengan menggunakan username dan passwordAnda.
                </p>

                <p>
                    Jika Anda memiliki komentar, saran atau keluhan, Anda dapat menghubungi kami (dan Data Protection
                    Officer kami) melalui e-mail di support@Originalbrandsbrands.co.id
                </p>

                <h2>
                    Pengumpulan Informasi Pribadi
                </h2>

                <p>
                    Ketika Anda membuat akun Originalbrands, atau memberikan informasi pribadi Anda melalui Platform,
                    informasi pribadi yang kami kumpulkan dapat meliputi:
                </p>
                <ul>
                    <li>
                        Nama
                    </li>
                    <li>
                        Alamat Pengiriman
                    </li>
                    <li>
                        Alamat Email
                    </li>
                    <li>
                        Nomor Telepon
                    </li>
                    <li>
                        Nomor Ponsel
                    </li>
                    <li>
                        Tanggal Lahir
                    </li>
                    <li>
                        Jenis Kelamin
                    </li>
                </ul>
                <p>
                    Anda harus menyerahkan kepada kami, agen resmi kami atau dalam Platform, informasi yang akurat,
                    lengkap dan tidak menyesatkan. Anda harus tetap memperbarui dan menginformasikannya kepada kami
                    apabila ada perubahan (informasi lebih lanjut di bawah). Kami berhak meminta dokumentasi untuk
                    melakukan verifikasi informasi yang Anda berikan.
                </p>

                <p>
                    Kami hanya akan dapat mengumpulkan informasi pribadi Anda jika Anda secara sukarela menyerahkan
                    informasi kepada kami. Jika Anda memilih untuk tidak mengirimkan informasi pribadi Anda kepada kami
                    atau kemudian menarik persetujuan menggunakan informasi pribadi Anda, maka hal itu dapat menyebabkan
                    kami tidak dapat menyediakan layanan kami kepada Anda. Anda dapat mengakses dan memperbarui
                    informasi pribadi yang Anda berikan kepada kami setiap saat seperti yang dijelaskan di bawah.
                </p>

                <p>
                    Jika Anda memberikan informasi pribadi dari pihak ketiga manapun kepada kami, maka kami menganggap
                    bahwa Anda telah memperoleh izin yang diperlukan dari pihak ketiga terkait untuk berbagi dan
                    mentransfer informasi pribadinya kepada kami.
                </p>

                <p>
                    Jika Anda mendaftar menggunakan akun media sosial Anda atau menghubungkan akun Originalbrands Anda
                    ke akun media sosial Anda atau menggunakan fitur media sosial Originalbrands lainnya, kami dapat
                    mengakses informasi tentang Anda yang terdapat dalam media social Anda sesuai dengan kebijakan
                    penyedia social media bersangkutan, dan kami akan mengelola data pribadi Anda yang telah kami
                    kumpulkan sesuai dengan kebijakan privasi Originalbrands.
                </p>

                <h2>Penggunaan dan Pengungkapan Informasi Pribadi</h2>

                <p>
                    Informasi pribadi yang kami kumpulkan dari Anda dapat digunakan, atau dibagikan dengan pihak ketiga
                    (termasuk perusahaan terkait, penyedia jasa layanan pihak ketiga, dan penjual pihak ketiga), untuk
                    beberapa atau semua tujuan berikut:
                </p>
                <ul>
                    <li>
                        Untuk memfasilitasi penggunaan Layanan (sebagaimana didefinisikan dalam Syarat &Ketentuan) dan /
                        atau akses ke Platform;/li>
                    </li>
                    <li>
                        Untuk memproses pesanan yang Anda kirimkan melalui Platform, baik produk yang dijual oleh
                        Originalbrands atau penjual pihak ketiga. Pembayaran produk yang Anda buat melalui Platform,
                        baik yang dijual oleh Originalbrands atau penjual pihak ketiga, akan diproses oleh agen kami;
                    </li>
                    <li>
                        Untuk mengirimkan produk yang telah dibeli melalui Platform, baik yang dijual oleh
                        Originalbrands atau penjual pihak ketiga. Kami dapat menyampaikan informasi pribadi Anda kepada
                        pihak ketiga dalam rangka pengiriman produk kepada Anda (misalnya untuk kurir atau supplier
                        kami), baik produk tersebut dijual melalui Platform oleh Originalbrands atau penjual pihak
                        ketiga;
                    </li>
                    <li>
                        Untuk memberi informasi pengiriman produk pada Anda, baik yang dijual melalui Platformoleh
                        Originalbrands atau penjual pihak ketiga, dan untuk kebutuhan customer support;
                    </li>
                    <li>
                        Untuk membandingkan informasi, dan memverifikasinya dengan pihak ketiga dalam rangka memastikan
                        keakuratan informasi.
                    </li>
                    <li>
                        Selanjutnya, kami akan menggunakan informasi yang Anda berikan untuk mengelola akun Anda;
                        memverifikasi dan melakukan transaksi keuangan dalam kaitannya dengan pembayaran yang Anda buat
                        secara online; mengaudit pengunduhan data dari Platform; memperbarui layout dan / atau konten
                        dari halaman Platform dan menyesuaikannya untuk pengguna; mengidentifikasi pengunjung
                        pada Platform; melakukan penelitian tentang demografi dan perilaku pengguna kami; menyediakan
                        informasi yang kami pikir mungkin berguna bagi Anda atau yang telah Anda minta dari kami,
                        termasuk informasi tentang produk dan layanan kami atau penjual pihak ketiga, dimana jika Anda
                        telah tidak keberatan dihubungi untuk tujuan ini;
                    </li>
                    <li>
                        Ketika Anda mendaftar menggunakan akun Originalbrands atau memberikan kepada kami informasi
                        pribadi Anda melalui Platform, kami juga akan menggunakan informasi pribadi Anda untuk
                        mengirimkan pemasaran dan / atau materi promosi tentang produk dan layanan kami atau penjual
                        pihak ketiga dari waktu ke waktu. Anda dapat berhenti dari berlangganan dan menerima informasi
                        pemasaran setiap saat dengan menggunakan fungsi unsubscribe dalam materi pemasaran elektronik.
                        Kami dapat menggunakan informasi kontak Anda untuk mengirim newsletter dari kami dan dari
                        perusahaan terkait dengan kami;
                    </li>
                    <li>
                        Dalam keadaan tertentu, Originalbrands mungkin perlu untuk mengungkapkan informasi pribadi,
                        seperti ketika ada alasan kuat yang dapat dipercaya bahwa pengungkapan tersebut diperlukan untuk
                        mencegah ancaman terhadap nyawa atau kesehatan, untuk tujuan penegakan hukum, atau untuk
                        permintaan pemenuhan persyaratan hukum dan peraturan terkait.
                    </li>
                </ul>
                <p>
                    Originalbrands dapat menggunakan informasi pribadi Anda dengan pihak ketiga dan afiliasi kami untuk
                    tujuan tersebut di atas, khususnya, menyelesaikan transaksi dengan Anda, mengelola akun Anda dan
                    hubungan kami dengan Anda, dalam rangka pemasaran dan pemenuhan persyaratan hukum atau peraturan dan
                    permintaan yang dianggap perlu oleh Originalbrands. Dalam hal ini, kami berusaha untuk memastikan
                    bahwa pihak ketiga dan afiliasi kami menjaga informasi pribadi Anda aman dari akses yang tidak sah,
                    pengumpulan, penggunaan, pengungkapan, atau risiko sejenis dan menyimpan informasi pribadi Anda
                    selama informasi pribadi Anda dibutuhkan untuk tujuan yang disebutkan di atas.
                </p>
            </div>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/js/shop.js"></script>
@endsection