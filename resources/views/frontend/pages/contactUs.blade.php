@extends('frontend.frontend')

@section('title')
    <title>Contact Us</title>
@endsection

@section('content')
    <div class="row margin-bottom-10">
        <div class="col-md-6">
            <h1>Our Contacts</h1>

            <div class="content-page">
                <h2>PT. Trilogi Cipta Reswara</h2>

                <p>Jalan kusuma pertiwi blok D3 no. 18, Komp. Vijaya Kusuma</p>

                <p>Bandung, Jawa Barat - Indonesia, 40615</p>

                <p>Phone: (022) 7811818</p>

                <p>Email: <a href="mailto:cs@originalbrands.co.id">cs@originalbrands.co.id</a></p>
            </div>
        </div>
        <div class="col-md-6">
            <iframe width="600" height="450" frameborder="0" style="border:0"
                    src="https://www.google.com/maps/embed/v1/place?q=Jalan%20Nusantara%20Pertiwi%20blok%20d3%20no.%2018%2C%20Palasari%2C%20West%20Java%2C%20Indonesia&key=AIzaSyAHFG1L92BcGk4N42oxC5v-2LqJyy-X094"
                    allowfullscreen></iframe>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/js/shop.js"></script>
@endsection