@extends('frontend.frontend')

@section('title')
    <title>General Questions</title>
@endsection

@section('morecss')
    <style>
        .panel {
            margin-bottom: 5px;
        }
    </style>
@endsection


@section('content')
    <div class="row margin-bottom-10">
        <div class="col-md-12">
            <h1>BANTUAN PERTANYAAN UMUM Originalbrands.co.id</h1>

            <div class="faq-page">
                <div class="panel panel-default panel-product">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                               href="#accordion1_1" aria-expanded="false">
                                1. Apakah Originalbrands.co.id memiliki toko retail?
                            </a>
                        </h4>
                    </div>
                    <div id="accordion1_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            Tidak, Originalbrands.co.id adalah sebuah departemen store online di mana Anda dapat
                            berbelanja 24 jam setiap hari. Produk-produk Originalbrands.co.id dapat dipesan melalui
                            website, mobile application dan agen pelayanan pelanggan. Pesanan akan dikirim langsung ke
                            alamat Anda.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-product">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                               href="#accordion1_2" aria-expanded="false">
                                2. Apakah saya harus membuat akun untuk berbelanja di Originalbrands.co.id?
                            </a>
                        </h4>
                    </div>
                    <div id="accordion1_2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            Ya. Memiliki akun akan mempermudah proses belanja karena tidak perlu untuk menulis data
                            pribadi dan alamat setiap kali Anda berbelanja. Anda juga bisa mendapatkan newsletter yang
                            didalamnya terdapat info diskon, penawaran khusus dan manfaat lainnya.

                            Bagaimana Saya bisa mendapatkan akun Originalbrands.co.id?

                            Anda dapat melakukan registrasi dengan mengklik tautan "Log in / Sign up" pada bagian pojok
                            kanan atas halaman website. Lalu isi identitas lengkap Anda pada bagian sebelah kanan login.
                            Mohon pastikan bahwa identitas yang Anda isi adalah benar, kemudian tekan tombol "Create an
                            Account". Setelahnya, Anda memiliki akun Originalbrands.co.id! Mohon untuk mengisi alamat
                            lengkap Anda pada menu “Account - Edit Address Detail”.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-product">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                               href="#accordion1_3" aria-expanded="false">
                                3. Bagaimana cara mengubah data pribadi saya di Originalbrands.co.id?
                            </a>
                        </h4>
                    </div>
                    <div id="accordion1_3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            Anda dapat login dan akses halaman "My Profile" untuk mengubah profil pribadi atau data
                            lainnya.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-product">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                               href="#accordion1_4" aria-expanded="false">
                                4. Apa yang terjadi setelah saya menekan tombol "Check Out"?
                            </a>
                        </h4>
                    </div>
                    <div id="accordion1_4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            Barang hanya akan dikirim setelah kami menerima pembayaran Anda. Saat Anda telah
                            menyelesaikan proses pembayaran, OriginalBrands.id menyarankan agar Anda melakukan
                            konfirmasi
                            dengan meng-klik pilihan "Confirm" pada halaman "My Profile". Saat proses konfirmasi
                            selesai, Originalbrands.co.id akan segera memproses pesanan Anda dan mengirimkannya ke
                            alamat yang telah dicantumkan.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-product">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                               href="#accordion1_5" aria-expanded="false">
                                5. Bagaimana cara saya menggunakan kode diskon?
                            </a>
                        </h4>
                    </div>
                    <div id="accordion1_5" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            Saat akan melakukan Checkout, Anda dapat mengisikan kode tersebut. Setiap transaksi hanya
                            dapat menggunakan satu kode. Mohon perhatikan syarat dan ketentuan yang tercantum pada kode
                            diskon tersebut.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-product">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                               href="#accordion1_6" aria-expanded="false">
                                6. Bagaimana saya bisa menggunakan Promo / Gift Voucher OriginalBrands.co.id?
                            </a>
                        </h4>
                    </div>
                    <div id="accordion1_6" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            Saat Anda akan melakukan SUBMIT ORDER, masukan kode voucher pada kolom COUPON CODE (tulis
                            sesuai dengan penggunaan huruf besar dan huruf kecil). Pastikan Anda telah menuliskannya
                            dengan benar. Setelahnya, klik REDEEM COUPON. Nominal pembayaran Anda akan dikurangi sesuai
                            dengan jumlah voucher secara otomatis. Harap pastikan syarat dan ketentuan yang berlaku.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-product">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                               href="#accordion1_7" aria-expanded="false">
                                7. Apakah yang dimaksud kredit Originalbrands.co.id?
                            </a>
                        </h4>
                    </div>
                    <div id="accordion1_7" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            Kredit adalah sejumlah nominal yang tersimpan dalam akun OriginalBrands.id Anda. Anda bisa
                            menggunakan kredit untuk melakukan pembelian pada OriginalBrands.id.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-product">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                               href="#accordion1_8" aria-expanded="false">
                                8. Bagaimana saya mengkonfirmasi pembayaran secara manual?
                            </a>
                        </h4>
                    </div>
                    <div id="accordion1_8" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            Setelah anda melakukan pembayaran sesuai petunjuk yang kami berikan, anda dapat mengisikan
                            informasi pembayaran pada halaman store.originalbrands.co.id/confirm-order. Isikan sesuai
                            dengan form yang "Confirm Your Order"
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-product">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                               href="#accordion1_8" aria-expanded="false">
                                9. Bagaimana saya dapat menggunakan Kredit?
                            </a>
                        </h4>
                    </div>
                    <div id="accordion1_8" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            Kredit dapat digunakan secara langsung saat Anda ingin berbelanja di Originalbrands.co.id.
                            Jumlah kredit akan mengurangi total pembayaran Anda.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-product">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                               href="#accordion1_9" aria-expanded="false">
                                10. Apakah yang di maksud dengan handling charges?
                            </a>
                        </h4>
                    </div>
                    <div id="accordion1_9" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            Ini adalah biaya tambahan yang dibutuhkan untuk memproses sebuah barang termasuk inspeksi,
                            pengemasan, pengangkutan, transportasi darat dan dokumentasi. Biaya ini akan dimasukkan ke
                            dalam biaya kirim..
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-product">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                               href="#accordion1_10" aria-expanded="false">
                                11. Apakah proses pembayaran di Originalbrands.co.id aman?
                            </a>
                        </h4>
                    </div>
                    <div id="accordion1_10" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            Proses pembayaran kami aman dan kami berkomitmen untuk selalu menjaga data pribadi Anda.
                            Data Anda aman dan tidak akan digunakan untuk tujuan penggunaan lainnya selain
                            Originalbrands.co.id.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-product">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                               href="#accordion1_11" aria-expanded="false">
                                12. Apakah saya bisa mengganti atau membatalkan pesanan saya?
                            </a>
                        </h4>
                    </div>
                    <div id="accordion1_11" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            Jika Anda belum melakukan pembayaran ke rekening kami (via transfer bank ke rekening
                            OriginalBrands.id atau Cash-On-Delivery), Anda masih bisa membatalkan pesanan dengan
                            menginformasikan ke pelayanan pelanggan. Namun, setelah Anda melakukan pembayaran, pesanan
                            Anda tidak dapat dibatalkan atau diganti.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-product">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                               href="#accordion1_12" aria-expanded="false">
                                13. Bagaimana jika saya menerima barang yang berbeda dengan pesanan?
                            </a>
                        </h4>
                    </div>
                    <div id="accordion1_12" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            Jika hal ini terjadi, Anda dapat menghubungi pelayanan pelanggan kami untuk mengatur
                            pergantian barang atau pengembalian uang.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-product">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                               href="#accordion1_13" aria-expanded="false">
                                14. Apakah produk yang telah dibeli bisa dikembalikan?
                            </a>
                        </h4>
                    </div>
                    <div id="accordion1_13" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            Produk yang telah dibeli dapat dikembalikan dalam jangka waktu maksimal 30 hari setelah Anda
                            menerima produk tersebut. Barang yang dikembalikan harus dalam kondisi yang utuh dan belum
                            terpakai. Untuk penjelasan lebih lengkap, lihat halaman return policy.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-product">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                               href="#accordion1_14" aria-expanded="false">
                                14. Darimana saya bisa memperoleh informasi / promo terbaru dari OriginalBrands.co.id?
                            </a>
                        </h4>
                    </div>
                    <div id="accordion1_14" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            Anda bisa mengetahui promo terbaru dengan melakukan dengan menginput alamat email Anda pada
                            kolom "Subscribe Newsletter". Kolom tersebut dalam Anda temukan pada bagian sudut kiri
                            bawah dari halaman website. Selain itu, Anda juga bisa mendapatkan informasi terbaru dengan
                            mengikuti akun Originalbrands.co.id di media social (Twitter, Facebook, Instagram dan
                            Youtube).
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-product">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                               href="#accordion1_15" aria-expanded="false">
                                15. Bagaimana saya dapat menghubungi pelayanan pelanggan?
                            </a>
                        </h4>
                    </div>
                    <div id="accordion1_15" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            Untuk informasi, saran dan kritik, Anda dapat menghubungi petugas pelayanan pelanggan
                            melalui jalur yang paling nyaman menurut Anda. <br>
                            Email : cs@originalbrands.co.id <br>
                            Waktu kerja : <br>
                            Senin – Jumat (9 pagi – 6 sore) <br>
                            Sabtu – Minggu (8 pagi – 5 sore) <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/js/shop.js"></script>
@endsection