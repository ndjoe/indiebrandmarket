@extends('frontend.frontend')

@section('title')

@endsection

@section('morecss')
@endsection


@section('content')
    <div class="row margin-bottom-10">
        <div class="col-md-12">
            <h1>SYARAT DAN KETENTUAN PENGEMBALIAN BARANG</h1>

            <div class="content-page">
                <h2>
                    SYARAT DAN KETENTUAN PENGEMBALIAN
                </h2>

                <p>
                    Anda dapat melakukan pengembalian barang dalam jangka waktu 30 hari (termasuk hari libur) terhitung
                    sejak barang Anda terima. Saat Anda menerima barang sudah terhitung sebagai 1 hari.
                </p>

                <p>
                    Barang harus dikirimkan dalam kondisi asli dan berada dalam kotak kemasan lengkap dengan aksesoris
                    terkait dan "hang tags". Harap tidak mengisolasi kotak secara berlebihan tetapi cukup membungkusnya
                    untuk mencegah kerusakan. Barang tersebut tetap menjadi tanggung jawab Anda sampai Originalbrands
                    menerimanya. Mohon bantuan Anda untuk mengembalikan paket dengan hati-hati.
                </p>

                <h2>
                    LAMA PENGEMBALIAN BARANG
                </h2>

                <p>
                    Pengembalian barang dari alamat Anda akan memakan waktu 1-3 hari kerja (untuk Bandung) atau 2-6 hari
                    kerja (untuk luar Bandung) sampai di gudang Originalbrands. Lamanya waktu juga dipengaruhi oleh
                    jasa pengiriman yang Anda pilih.
                </p>

                <h2>
                    BIAYA PENGEMBALIAN BARANG
                </h2>

                <p>
                    Tergantung dari asal wilayah pelanggan. Originalbrands tidak menanggung biaya pengiriman
                    pengembalian barang. Originalbrands menyarankan agar menggunakan jasa pengiriman yang terpercaya
                    untuk
                    mengantisipasi kemungkinan kehilangan barang selama pengiriman.
                </p>

                <h2>
                    CARA MENGEMBALIKAN BARANG
                </h2>

                <p>
                    Siapkan barang yang akan dikembalikan dengan membungkus kembali barang tersebut ke kotak kemasannya
                </p>

                <p>
                    Sertakan formulir pengembalian pada saat melakukan pengiriman barang. Apabila tidak,
                    Originalbrands berhak untuk menolak proses pengembalian lebih lanjut (Form pengembalian dapat
                    ditemukan di dalam kotak kemasan paket pengiriman Anda).
                </p>

                <h2>
                    ONLINE (ONLINE RETURN FORM)
                </h2>

                <p>
                    Berikut adalah panduan pengisian online return form:
                </p>

                <p>
                    Masuk ke menu akun Anda di originalbrands.co.id
                </p>

                <p>
                    Klik Menu ‘Form Retur’
                </p>

                <p>
                    Pilih nama barang yang akan dikembalikan, klik ‘Retur’
                </p>

                <p>
                    Isi tujuan Anda melakukan pengembalian :
                </p>

                <p>
                    Tukar dengan barang yang sama
                </p>

                <p>
                    Tukar dengan barang yang beda
                </p>

                <p>
                    Ganti ukuran
                </p>

                <p>
                    Ganti warna
                </p>

                <p>
                    Kredit Originalbrands
                </p>

                <p>
                    Pengembalian pembayaran
                </p>

                <p>
                    Lainnya
                </p>

                <p>
                    Isi alasan Anda melakukan pengembalian :
                </p>

                <p>
                    Berbeda dengan di website
                </p>

                <p>
                    Kualitas tidak bagus
                </p>

                <p>
                    Ukuran tidak sesuai
                </p>

                <p>
                    Tidak sesuai pesanan
                </p>

                <p>
                    Rusak/ Tumpah/ Pecah
                </p>

                <p>
                    Lainnya
                </p>

                <p>
                    Lalu klik ‘Proses’
                </p>

                <p>
                    Tunggu notifikasi melalui email atau informasi dari tim Customer Service originalbrands.co.id
                </p>

                <p>
                    cs@originalbrands.co.id
                </p>

                <h2>
                    MENGKREDITKAN UANG DARI BARANG YANG DIKEMBALIKAN
                </h2>

                <p>
                    Kredit hanya akan diberikan apabila barang yang dikembalikan masih dalam kondisi belum terpakai dan
                    tidak rusak dengan tags masih terpasang.. Kredit yang diberikan dapat digunakan untuk pembelian
                    berikutnya di Originalbrands.
                </p>
            </div>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/js/shop.js"></script>
@endsection