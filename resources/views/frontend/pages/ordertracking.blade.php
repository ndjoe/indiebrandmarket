@extends('frontend.frontend')

@section('title')

@endsection

@section('morecss')
    <link rel="stylesheet" href="/css/invoice-2.min.css">
@endsection


@section('content')
    <div class="row margin-bottom-10">
        <h1>Order Tracking</h1>

        <div class="col-md-12">
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-md-4">
                    <div class="well well-lg">
                        <form id="forgot" action="/{{ LaravelLocalization::getCurrentLocale() }}/tracking"
                              method="post">
                            {{ csrf_field() }}
                            <div class="form-group"
                                 :class="!validator.nomororder ? 'has-error':''">
                                <label for="">Nomor Order:</label>
                                <input type="text" v-model="nomororder" class="form-control" name="nomororder">
                        <span class="help-block" v-show="!validator.nomororder">
                            Harus diisi
                        </span>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-8">
                    @if(isset($order))
                        <div class="invoice-content-2 bordered">
                            <div class="row invoice-cust-add">
                                <div class="col-xs-3">
                                    <h2 class="invoice-title uppercase">Customer</h2>

                                    <p class="invoice-desc">
                                        Nama: {{ $order->address->nama }}<br>
                                        Handphone: {{ $order->address->nohp }}
                                    </p>
                                </div>
                                <div class="col-xs-3">
                                    <h2 class="invoice-title uppercase">Date</h2>

                                    <p class="invoice-desc">{{ $order->created_at }}</p>
                                </div>
                                <div class="col-xs-6">
                                    <h2 class="invoice-title uppercase">Address</h2>

                                    <p class="invoice-desc inv-address">
                                        {{ $order->address->alamat }}
                                        , {{ $order->address->kota }} {{ $order->address->kodepos }} {{ $order->address->provinsi }} {{ $order->address->negara }}
                                        <br>
                                    </p>
                                </div>
                            </div>
                            <div class="row invoice-body">
                                <div class="col-xs-12 table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th class="invoice-title uppercase text-center">Nama Produk</th>
                                            <th class="invoice-title uppercase text-center">Brand</th>
                                            <th class="invoice-title uppercase text-center">Size</th>
                                            <th class="invoice-title uppercase text-center">Qty</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($order->items as $item)
                                            <tr>
                                                <td class="text-center sbold">
                                                    {{ $item->productItem->product->name }}
                                                </td>
                                                <td class="text-center sbold">
                                                    {{ $item->productItem->product->author->nama }}
                                                </td>
                                                <td class="text-center sbold">
                                                    {{ $item->productItem->size->text }}
                                                </td>
                                                <td class="text-center sbold">
                                                    {{ $item->qty }}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row invoice-subtotal">
                                <div class="col-xs-4">
                                    <h2 class="invoice-title uppercase">#Order</h2>

                                    <p class="invoice-desc">{{ $order->order_code }}</p>
                                </div>
                                <div class="col-xs-4">
                                    <h2 class="invoice-title uppercase">Status</h2>
                                    @if(!is_null($order->confirmed_at) && is_null($order->shipping))
                                        <p class="invoice-desc">Confirmed</p>
                                    @elseif(!is_null($order->confirmed_at) && !is_null($order->shipping))
                                        <p class="invoice-desc">Shipped; No resi: {{ $order->shipping->no_resi }}</p>
                                    @else
                                        <p class="invoice-desc">Belum dikonfirmasi</p>
                                    @endif
                                </div>
                                <div class="col-xs-4">
                                    <h2 class="invoice-title uppercase">Total</h2>

                                    <p class="invoice-desc grand-total">IDR {{ $order->grossTotal }}</p>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row margin-bottom-10">
        <div class="col-md-12">
            <div class="content-page">
                <h2>
                    Bagaimana saya mengecek status pesanan saya?
                </h2>

                <p>
                    Anda dapat melacak pesanan Anda selama 24 jam/7 hari dengan mengikuti langkah-langkah berikut:
                </p>

                <p>
                    Buka store.originalbrands.co.id/order-tracking
                </p>

                <p>
                    Masukkan nomor order anda
                </p>

                <p>
                    Tekan "Submit" untuk memproses
                </p>

                <p>
                    Atau Anda cukup klik Order Tracking di bagian bawah halaman store.originalbrands.co.id, masukkan
                    nomor order anda
                    Anda, kemudian tekan "Submit" untuk memeriksa status pesanan Anda.
                </p>

                <h2>
                    Nomor resi yang diberikan ke saya tidak valid, apa yang harus saya lakukan?
                </h2>

                <p>
                    Informasi pelacakan bisa saja belum tersedia untuk Anda; Informasi tersebut akan tersedia pada H+1
                    hari kerja ketika rekan logistik kami telah melakukan pembaharuan data mereka.
                </p>

                <p>
                    Andapun dapat memeriksa status order dengan sarana Cek Status Pesanan kami
                    store.originalbrands.co.id,
                    Sistem Originalbrands.co.id akan diperbaharui setiap harinya untuk menyediakan pelayanan terbaik
                    untuk cek status
                    pesanan Anda.
                </p>

                <p>
                    Jika setelah 1 hari kerja Anda masih bermasalah dengan pengecekan status pesanan Anda, silahkan
                    hubungi kami di cs@originalbrands.co.id. Kami akan membantu Anda segera.
                </p>
            </div>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script>
        var vm = new Vue({
            el: '#shop',
            data: {
                cart: [],
                nomororder: '',
                dataOrder: null,
                currency: obid.locale === 'id' ? 'IDR' : 'USD',
                locale: obid.locale,
                newsletter: {
                    email: ''
                }
            },
            computed: {
                validator: function () {
                    return {
                        nomororder: validator.isNumeric(this.nomororder),
                    }
                }
            },
            methods: {
                getCart: function () {
                    var self = this;
                    $.get('/api/cart')
                            .done(function (resp) {
                                self.cart = resp.items;
                            });
                },
                delItem: function (data) {
                    var self = this;
                    var formData = new FormData;

                    formData.append('rowid', data['rowid']);

                    $.ajax({
                        url: '/api/cart/delete',
                        data: formData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: true,
                        type: 'POST'
                    }).done(function (data) {
                        self.cart = data.items;
                    });
                },
                subscribesNewsLetter: function (e) {
                    e.preventDefault();
                    var self = this;
                    var formData = new FormData;
                    formData.append('email', this.newsletter.email);

                    $.ajax({
                        url: '/' + obid.locale + '/api/newsletter',
                        data: formData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: true,
                        type: 'POST',
                        statusCode: {
                            422: function (e) {
                                toastr.error("email invalid");
                            }
                        }
                    }).done(function (data) {
                        if (data.subscribed) {
                            swal(
                                    "You have been subscribed",
                                    "Please save following code and use it for your next purchase: "
                                    + data.couponCode,
                                    "success"
                            );
                            self.newsletter.email = "";
                        } else {
                            swal(
                                    "Ooops...",
                                    "Your have been subscribed to our news letter.... sorry for that",
                                    "error"
                            );
                            self.newsletter.email = "";
                        }
                    });
                }

            },
            ready: function () {
                Layout.init();
                Layout.initTouchspin();
                Layout.initTwitter();
                Layout.initFixHeaderWithPreHeader();
                Layout.initNavScrolling();
                this.getCart();
            },
            components: {}
        });
    </script>
@endsection