@extends('frontend.frontend')

@section('title')
    <title>Order Paid</title>
@endsection

@section('morecss')
@endsection


@section('content')
    <div class="row margin-bottom-10">
        <div class="col-md-12">
            <h1>Terima Kasih Sudah Melakukan Order di originalbrands.id</h1>

            <div class="content-page">
                <h2>
                    Order anda nomor # {{ $order->order_code }} dengan total pesanan sebesar
                    USD {{ number_format($order->grossTotal/$order->currency_value,2,'.',',') }}
                </h2>

                <p>
                    We have accepted your payment via paypal, thank you
                </p>
            </div>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/js/shop.js"></script>
@endsection