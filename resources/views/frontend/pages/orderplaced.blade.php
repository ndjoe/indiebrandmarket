@extends('frontend.frontend')

@section('title')
    <title>Order Placed</title>
@endsection

@section('morecss')
@endsection


@section('content')
    <div class="row margin-bottom-10">
        <div class="col-md-12">
            <h1>Terima Kasih Sudah Melakukan Order di originalbrands.id</h1>

            <div class="content-page">
                <h2>
                    Order anda nomor # {{ $order->order_code }} dengan harga yang harus dilunasi sebesar
                    IDR {{ number_format($order->grossTotal - $order->uniqueCost, 0, ',', '.') }}
                </h2>

                <p>
                    Kami sudah menerima pesanan anda, silahkan transfer
                    melalui bank kami berikut ini:
                </p>
                <p>
                    <span>
                        <img src="/img/bca-logo-originalbrands.png"
                             width="100" alt=""><br>
                        Bank: BCA <br>
                        Atas nama: originalbrands.id <br>
                        Nomor rekening: 1234578 <br>
                    </span>

                    <span>
                        <img src="/img/mandiri-logo-originalbrands.png"
                             width="100" alt=""><br>
                        Bank: Mandiri <br>
                        Atas nama: originalbrands.id <br>
                        Nomor rekening: 12345678 <br>
                    </span>
                </p>
                <p>
                    Transaksi dianggap batal jika sampai dengan
                    tanggal
                    {{ $order->expired_at }}
                    (1×24 jam) pembayaran
                    belum
                    dilunasi.
                </p>
                <p>Jika telah membayar, silahkan melakukan konfirmasi pembayaran melalui halaman confirm order atau
                    melalui link berikut ini
                    <a href="/confirm-order">Confirm Order</a>
                </p>
            </div>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/js/shop.js"></script>
@endsection