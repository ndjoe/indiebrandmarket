@extends('frontend.frontend')

@section('title')

@endsection

@section('morecss')
@endsection


@section('content')
    <div class="row margin-bottom-10">
        <div class="col-md-12">
            <h1>
                Syarat & Ketentuan
                WEBSITE ORIGINALBRANDS.CO.ID
            </h1>

            <div class="content-page">
                <p>
                    Berikut adalah syarat-syarat penggunaan ("Term of Use"), yang mungkin dapat direvisi oleh PT Trilogi
                    Cipta Reswara ("TRILOGI DIGITAL") selaku pemilik Situs www.originalbrands.co.id ("Situs
                    ORIGINALBRANDS.CO.ID") dan/atau PT TRILOGI CIPTA RESWARA ("TRILOGI DIGITAL") selaku pengelola Situs
                    ORIGINALBRANDS.CO.ID, dari waktu ke waktu tanpa pemberitahuan terlebih dahulu, dan berlaku untuk
                    Anda dalam melakukan akses ke, dan penggunaan Situs ORIGINALBRANDS.CO.ID ini. Bacalah semua
                    syarat-syarat dan ketentuan ini dengan seksama. Dengan mengakses Situs ORIGINALBRANDS.CO.ID ini dan
                    salah satu halaman yang terdapat didalamnya, Anda setuju untuk terikat dengan syarat-syarat dan
                    ketentuan di bawah ini. Jika Anda tidak menyetujui syarat-syarat dan ketentuan yang ditetapkan di
                    bawah ini, mohon untuk tidak mengakses Situs ORIGINALBRANDS.CO.ID ini dan/atau salah satu halaman
                    yang terdapat didalamnya.
                </p>

                <h2>
                    Kepemilikan dan Pengelolaan Situs ORIGINALBRANDS.CO.ID
                </h2>

                <p>
                    Bahwa Situs ORIGINALBRANDS.CO.ID ini dimiliki oleh PT TRILOGI CIPTA RESWARA dan pengelolaannya
                    dilakukan oleh TRILOGI DIGITAL. Selaku pengelola Situs ORIGINALBRANDS.CO.ID, TRILOGI DIGITAL
                    bertanggung jawab sepenuhnya atas: (i) Pengelolaan dan manajemen hosting dan domain Situs
                    ORIGINALBRANDS.CO.ID; dan (ii) Penyediaan, pengelolaan serta pemeliharaan content (isi) Situs
                    ORIGINALBRANDS.CO.ID termasuk tetapi tidak terbatas pada penyelenggaraan kuis, permainan, polling,
                    lomba, dan/atau sayembara/undian berhadiah yang diselenggarakan oleh Situs ORIGINALBRANDS.CO.ID.
                    Bahwa seluruh hadiah-hadiah yang diberikan dalam rangka penyelenggaraan kuis, permainan, polling,
                    lomba, dan/atau sayembara/undian berhadiah tersebut, disediakan dan diberikan oleh TRILOGI DIGITAL
                    kepada Anda/pengunjung situs yang beruntung ataupun telah memenuhi syarat-syarat yang TRILOGI
                    DIGITAL tetapkan.
                </p>

                <h2>
                    Pengunjung Yang Terdaftar
                </h2>

                <p>
                    Beberapa layanan pada situs ini, hanya tersedia untuk pengunjung yang telah mendaftar dan
                    mengharuskan pengunjung untuk mendaftar dengan user ID untuk menggunakannya. Sebagai pengunjung yang
                    terdaftar, Anda diwajibkan untuk mengikuti segala peraturan penggunaan layanan tersebut.
                </p>

                <h2>
                    Pemilik Hak Atas Konten
                </h2>

                <p>
                    Bahwa konten, layanan dan software Situs ORIGINALBRANDS.CO.ID (termasuk tetapi tidak terbatas kepada
                    teks, suara, foto grafis, dan materi lain yang terkandung di dalamnya) dilindungi oleh hak cipta,
                    hak merek, hak paten, dan/atau Hak Atas Kekayaan Inteletual lainnya, dimana setiap pelanggaran
                    terhadap hal-hal tersebut diatas adalah suatu tindakan melawan hukum. Konten dalam situs ini bisa
                    digunakan oleh Anda hanya untuk kebutuhan Anda sendiri, penggunaan non komersil, namun tidak boleh
                    disalahartikan dan disalahgunakan. Segala hak yang dijelaskan tidak secara langsung kepada Anda
                    adalah hak dari TRILOGI DIGITAL.
                </p>

                <p>
                    Ketika Anda menambah foto dan/atau materi-materi lainnya, secara otomatis setuju bahwa Situs
                    ORIGINALBRANDS.CO.ID bisa mempertunjukan foto dan/atau materi-materi lainnya yang Anda tambahkan
                    tersebut, dimana memungkinkan Anda bisa mendapatkan beberapa respon.
                </p>

                <h2>
                    Penggunaan Fasilitas Korespondensi
                </h2>

                <p>
                    Situs ini berisi news/artikel, band pages, studio pages, channel/gallery, dan/atau fasilitas lainnya
                    termasuk fasilitas korespondensi, Anda setuju untuk menggunakan fasilitas-fasilitas tersebut hanya
                    untuk mengirim dan menerima pesan materi yang sesuai dan berkaitan dengan fasilitas yang
                    bersangkutan. Anda setuju bahwa ketika berinteraksi atau berkorespondensi Anda tidak boleh melakukan
                    hal-hal berikut ini :
                </p>
                <ul>
                    <li>Mencemarkan nama baik, mengganggu, bertindak sewenang-wenang, mengusik, mengancam, atau
                        melanggar hak-hak legal (seperti hak privasi dan publisitas) member yang lain.
                    </li>
                    <li>Mendistribusikan atau menyebarkan fitnah, menyalahi SARA (suku, agama, ras, adat), cabul, tidak
                        senonoh, atau materi dan informasi yang tidak sah.
                    </li>
                    <li>Upload file yang berisi software atau materi lain yang dilindungi oleh hak properti intelektual
                        (atau hak privasi publisitas) kecuali Anda memiliki atau mempunyai kontrol dari materi atau
                        software tersebut, atau telah memiliki cukup ijin untuk menerbitkannya. Kami berhak untuk
                        menunda penayangan materi file yang di-upload pada situs Kami terkait dengan pelaksanaan
                        verifikasi data untuk membuktikan bahwa Anda adalah pihak yang memiliki hak atas materi
                        sebagaimana dimaksud. Kami berhak pula untuk menurunkan materi yang telah Anda upload pada Situs
                        ORIGINALBRANDS.CO.ID apabila ternyata Anda di kemudian hari terbukti bukan sebagai pemilik hak
                        properti intelektual yang sah atas materi tersebut.
                    </li>
                    <li>Upload file yang berisi virus, file rusak, atau software dan program serupa yang mengakibatkan
                        kerusakan operasi dari komputer member lain.
                    </li>
                    <li>Mem-forward survey, kontes, atau surat berantai.</li>
                    <li>Download file yang di-posting oleh member lain dari forum yang Anda tahu tidak bisa
                        didistribusikan secara legal dengan cara tersebut.
                    </li>
                </ul>
                <p>
                    Semua forum adalah milik dan bersifat publik dan bukan merupakan komunikasi pribadi. Chat, posting,
                    conference, dan komunikasi lain oleh member tidak disahkan oleh Situs ORIGINALBRANDS.CO.ID, dan
                    komunikasi seperti yang disebutkan di atas tidak akan ditanggapi, dipantau, atau disetujui oleh
                    Situs ORIGINALBRANDS.CO.ID. Situs ORIGINALBRANDS.CO.ID berhak untuk alasan apapun menghapus,
                    menyembunyikan, mengubah konten dari forum tersebut tanpa peringatan sebelumnya kepada member.
                </p>

                <h2>
                    Menghubungkan ke Situs Pihak Ketiga
                </h2>

                <p>Situs ORIGINALBRANDS.CO.ID ini mungkin berisi hyperlink ke situs lain yang dimiliki oleh selain Situs
                    ORIGINALBRANDS.CO.ID. Hyperlink serupa telah disediakan hanya untuk referensi dan kenyamanan Anda.
                    Baik Situs ORIGINALBRANDS.CO.ID ataupun pemilik mengatur situs-situs tersebut, dan tidak bertanggung
                    jawab atas konten mereka. Pencantuman hyperlink situs tersebut pada Situs ORIGINALBRANDS.CO.ID bukan
                    merupakan tanda pengabsahan materi yang ada pada situs atau link yang terkait dengan operator
                    mereka.</p>

                <p>
                    Pencantuman Hyperlink oleh member dilayanan Situs ORIGINALBRANDS.CO.ID adalah hak member dan
                    tanggung jawab langsung oleh member. Entitas apapun yang Anda pilih sebagai kontak atau berinteraksi
                    baik yang terdaftar dalam direktori atau di tempat lain pada situs ini adalah sepenuhnya tanggung
                    jawab layanan tersebut pada Anda, dan Anda setuju bahwa Situs ORIGINALBRANDS.CO.ID tidak dapat
                    dikenakan tindakan apapun untuk kerusakan atau biaya yang keluar atas interaksi yang terjadi antara
                    Anda dengan layanan yang bersangkutan.
                </p>

                <h2>
                    Daftar Direktori dan Layanan
                </h2>

                <p>
                    Anda akan mendapatkan akses dan menggunakan informasi, fitur dan layanan yang merupakan resiko Anda
                    sendiri. Situs ORIGINALBRANDS.CO.ID tidak merepresentasikan atau mengesahkan keakurasian atau
                    reliabilitas dari informasi, konten atau layanan yang tersedia, pada daftar direktori tersebut. Anda
                    di sini mengakui bahwa mempercayai terhadap informasi, konten, feature atau layanan yang tersedia
                    pada daftar direktori tersebut merupakan resiko Anda sendiri.
                </p>

                <h2>
                    Penolakan Pertanggungjawaban
                </h2>

                <p>
                    Anda dengan jelas menyetujui bahwa penggunaan Situs ORIGINALBRANDS.CO.ID ini merupakan resiko anda
                    sendiri.
                </p>

                <p>
                    Semua konten, informasi, software, product, feature dan layanan yang diterbitkan pada Situs
                    ORIGINALBRANDS.CO.ID ini kemungkinan mencakup ketidakakuratan atau kesalahan tipografikal. Perubahan
                    akan ditambahkan secara periodik pada konten yang ada pada situs ini. Situs ORIGINALBRANDS.CO.ID
                    mungkin akan mengadakan perbaikan dan / atau perubahan sewaktu-waktu. Situs ORIGINALBRANDS.CO.ID ini
                    mungkin untuk sementara waktu tidak bisa diakses sewaktu-waktu untuk keperluan proses maintenance,
                    interupsi telekomunikasi, atau gangguan lainnya. Situs ORIGINALBRANDS.CO.ID (beserta pemilik,
                    supplier, konsultan, pengiklan, affiliasi, rekanan, pekerja, atau entitas terkait, secara bersama
                    disebut entitas terkait setelah ini) tidak akan dapat dikenakan tuntutan oleh user atau member atau
                    pihak ketiga manapun untuk mengubah atau menghentikan beberapa atau semua konten, informasi,
                    software, product, feature dan layanan yang diterbitkan pada situs ini, kecuali dalam hal konten,
                    informasi, software, product, feature dan layanan sebagaimana dimaksud dapat dibuktikan sebagai hak
                    atas kekayaan intelektual milik dari user atau member atau pihak ketiga tersebut.
                </p>

                <p>Semua konten, informasi, software, product, feature dan layanan disediakan “apa adanya” tanpa adanya
                    jaminan apapun. Situs ORIGINALBRANDS.CO.ID dan / atau entitas terkaitnya dengan ini menolak segala
                    macam bentuk jaminan dan syarat-syarat terhadap konten, informasi, software, product, feature dan
                    layanan termasuk dan kondisi dari barang / jasa yang dapat diperjualbelikan.</p>

                <p>
                    Kami tidak bertanggung jawab atas setiap masalah atau gangguan teknis dari jaringan atau jalur
                    telepon, sistem online komputer, server, penyedia jasa internet, peralatan komputer, piranti lunak,
                    atau apapun termasuk kerusakan pada komputer Anda atau komputer siapapun sebagai akibat dari
                    penggunaan situs ini.
                </p>

                <p>
                    Sebagai pengunjung Situs ORIGINALBRANDS.CO.ID, Anda mengetahui dan setuju bahwa tiap kepercayaan
                    terhadap atau penggunaan informasi dan atau konten yang tersedia dalam situs ini seluruhnya adalah
                    resiko Anda sendiri. Dalam keadaan apapun, TRILOGI DIGITAL tidak bertanggung jawab atas kerugian
                    secara langsung, tak langsung, atau ganti rugi sepenuhya yang muncul sebagai akibat dari penggunaan
                    atau performa situs ini.
                </p>

                <h2>
                    Penolakan Jaminan
                </h2>

                <p>
                    Anda dengan jelas setuju bahwa penggunaan layanan Situs ORIGINALBRANDS.CO.ID adalah sepenuhnya
                    resiko atau tanggung jawab member.
                </p>

                <p>
                    Situs ORIGINALBRANDS.CO.ID dengan jelas menolak apapun dan segala yang termasuk garansi, termasuk,
                    tetapi tidak terbatas pada, jaminan barang atau jasa yang diperdagangkan dan kecocokan untuk tujuan
                    tertentu. Situs ORIGINALBRANDS.CO.ID tidak dapat dikenakan tindakan apapun atau bertanggung jawab
                    atas garansi, jaminan, dan representasi yang ditawarkan oleh pengiklan, rekanan, pabrikan Situs
                    ORIGINALBRANDS.CO.ID, dari pedagang atau supplier layanan. Tidak ada saran atau informasi, baik
                    lisan maupun tertulis, yang dijumpai oleh member dari Situs ORIGINALBRANDS.CO.ID atau melalui sumber
                    lain yang membuat garansi apapun di sini.
                </p>

                <h2>
                    Partisipasi dalam Promosi oleh Pengiklan dan Rekanan
                </h2>

                <p>
                    Korespondensi antara member dengan pihak pengiklan dan rekanannya adalah sepenuhnya dilakukan antara
                    member yang berkorespondensi dengan pengiklan atau rekanannya tersebut. Situs Originalbrands tidak
                    memiliki kewajiban atau tanggung jawab terhadap korespondensi tersebut.
                </p>

                <h2>
                    Ganti Rugi
                </h2>

                <p>
                    Anda menyetujui untuk mengganti rugi, membela dan menjaga TRILOGI DIGITAL dan afiliasinya, mitra
                    bisnis, petugas, direktur, karyawan dan agen-agen agar tetap aman dari tiap kerugian, tanggung
                    jawab, tuntutan, permintaan, ganti rugi, atau biaya (termasuk biaya legal) yang diajukan oleh pihak
                    ketiga manapun sebagai akibat dari penggunaan Anda terhadap situs ini atau pelanggaran atas
                    ketentuan ini. TRILOGI DIGITAL berhak untuk melakukan tindakan pertahanan ekslusif dan kontrol atas
                    setiap hal yang berhubungan dengan tuntutan ganti rugi yang disebabkan oleh Anda, yang tidak
                    memaklumkan kewajiban ganti rugi Anda.
                </p>

                <h2>
                    Penggunaan untuk tujuan tidak sah dan terlarang tidak diperbolehkan.
                </h2>

                <p>
                    Sesuai dengan kondisi penggunaan Anda ke situs ini, Anda menjamin kepada Situs ORIGINALBRANDS.CO.ID
                    bahwa anda tidak akan menggunakan situs ini untuk tujuan apapun yang tidak sah dan dilarang oleh
                    beberapa dan keseluruhan persyaratan, ketentuan, dan peringatan yang ada di situs ini.
                </p>

                <h2>
                    Peringatan dan Penalti
                </h2>

                <p>
                    Peringatan yang diberikan oleh Situs ORIGINALBRANDS.CO.ID kepada Anda akan diberikan lewat e-mail
                    atau private message atau telepon atau surat biasa ataupun posting umum pada Situs
                    ORIGINALBRANDS.CO.ID.
                </p>

                <p>
                    Dalam hal peringatan sudah diberikan, namun tidak terdapat usaha perbaikan dan atau koreksi dari
                    member yang dianggap melakukan pelanggaran, maka Situs ORIGINALBRANDS.CO.ID berhak memberikan
                    penalti kepada member dalam berbagai bentuk, termasuk tetapi tidak terbatas pada non-aktivasi
                    member.
                </p>

                <h2>
                    Penambahan Syarat-Syarat Dan Ketentuan Penggunaan
                </h2>

                <p>
                    Beberapa area atau layanan dari Situs ORIGINALBRANDS.CO.ID, seperti halaman di mana Anda dapat
                    memuat (upload) atau mengunduh (download) dokumen atau berkas, dapat memiliki panduan dan peraturan
                    penggunaan yang akan menambah Syarat-syarat dan Ketentuan Penggunaan ini. Dengan menggunakan
                    layanan-layanan tersebut, Anda setuju untuk terikat dengan petunjuk dan peraturan penggunaan yang
                    berlaku tersebut.
                </p>

                <h2>
                    Hukum
                </h2>

                <p>
                    Syarat-syarat dan ketentuan dalam Syarat-syarat dan Ketentuan Penggunaan Situs ORIGINALBRANDS.CO.ID
                    ini tunduk kepada hukum di wilayah Negara Kesatuan Republik Indonesia.
                </p>
            </div>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/js/shop.js"></script>
@endsection