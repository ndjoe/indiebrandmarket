<div class="newsletter">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="form-newsletter">
                    <h4>GET IDR 20000 VOUCHER IF YOU SUBSCRIBES</h4>

                    <div class="row">
                        <form v-on:submit="subscribesNewsLetter">
                            <div class="col-md-8 col-xs-12 no-padding-right input-newsletter">
                                <input v-model="newsletter.email" type="text" class="form-control border-black"
                                       placeholder="Signup to our newsletter">
                            </div>
                            <div class="col-md-4 col-xs-12 no-padding-left btn-newletter">
                                <button type="submit" class="btn btn-black">Submit</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="sosmed">
                    <h4>Follow Our Social Media</h4>

                    <ul class="social-icons">
                        <li>
                            <a class="facebook" data-original-title="facebook" href="javascript:;"></a>
                        </li>
                        <li>
                            <a class="twitter" data-original-title="twitter" href="javascript:;"></a>
                        </li>
                        <li>
                            <a class="youtube" data-original-title="youtube" href="javascript:;"></a>
                        </li>
                        <li>
                            <a class="instagram" data-original-title="instagram" href="javascript:;"></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="secured">
                    <h4>Secured By:</h4>
                    <img src="/img/logo-comodo.png">
                </div>
            </div>
        </div>
    </div>
</div>