<div class="hidden-xs">
    <div class="col-xs-3">
        <ul class="nav nav-tabs tabs-left" role="tablist">
            <li role="tab" class="active">
                <label data-target="#tbca">
                    <input type="radio" name="payment" value="tbca"
                           v-model="payment" checked>
                    Manual Transfer
                </label>
            </li>
            <li role="tab">
                <label data-target="#vmandiriclickpay">
                    <input type="radio" name="payment"
                           value="vmandiriclickpay"
                           v-model="payment">
                    Mandiri Clickpay
                </label>
            </li>
            <li role="tab">
                <label data-target="#vmandiriecash">
                    <input type="radio" name="payment" value="vmandiriecash"
                           v-model="payment">
                    Mandiri E-cash
                </label>
            </li>
            <li role="tab">
                <label data-target="#vbcaklikpay">
                    <input type="radio" name="payment" value="vbcaklikpay"
                           v-model="payment">
                    BCA klikpay
                </label>
            </li>
            <li role="tab">
                <label data-target="#vcimbclick">
                    <input type="radio" name="payment" value="vcimbclick"
                           v-model="payment">
                    CIMBclick
                </label>
            </li>
            <li role="tab">
                <label data-target="#vcreditcard">
                    <input type="radio" name="payment" value="vcreditcard"
                           v-model="payment">
                    Creditcard
                </label>
            </li>
            <li role="tab">
                <label data-target="#vindomaret">
                    <input type="radio" name="payment" value="vindomaret"
                           v-model="payment">
                    Indomaret
                </label>
            </li>
        </ul>
    </div>
    <div class="col-xs-9">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="tbca">
                <div class="payment-info">
                    <div class="heading">
                        <h6 class="title">
                            Ketentuan pembayaran dengan transfer bank:
                        </h6>
                    </div>

                    <div class="body">
                        <p>
                            <img src="/img/bca-logo-originalbrands.png"
                                 width="100" alt="">
                            <img src="/img/mandiri-logo-originalbrands.png"
                                 width="100" alt="">
                        </p>
                        <p>
                            Transaksi dengan menggunakan metode pembayaran
                            ATM
                            akan
                            ditambahkan <strong class="text-uppercase">harga unik</strong>, <strong
                                    class="text-uppercase">harga unik</strong>
                            dimaksudkan untuk
                            mempermudah proses verifikasi.
                        </p>
                        <p>
                            Harga unik anda adalah: <strong>
                                @{{ currency }} @{{ uniqueCost | formatNumber currency }}
                            </strong>
                        </p>
                        <p>
                            Pembayaran yang wajib dibayarkan sesuai dengan jumlah <strong class="text-uppercase">harga
                                unik</strong>, bukan <strong class="text-uppercase" style="color: red">jumlah
                                total</strong>.
                        </p>
                        <p>
                            Transaksi dianggap batal jika sampai dengan
                            tanggal
                            {{ \Carbon\Carbon::now("Asia/Jakarta")->addDay(1)->toDateTimeString() }}
                            (1×24 jam) pembayaran
                            belum
                            dilunasi.
                        </p>
                        <p>
                            Klik tombol <strong class="text-uppercase">Continue</strong> jika anda telah memahami
                            dan
                            menyetujui
                            ketentuan transaksi di atas.
                        </p>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="vmandiriclickpay">
                <div class="payment-info">
                    <div class="heading">
                        <h6 class="title">
                            Anda tidak dikenakan biaya pelayanan.
                        </h6>
                    </div>

                    <div class="body">
                        <p>
                            <img src="/img/mandiriclickpay-logo-originalbrands.png"
                                 width="100" alt="">
                        </p>
                        <p>
                            Klik tombol Continue untuk melanjutkan
                            transaksi.
                            Anda
                            akan
                            diarahkan pada halaman pembayaran dengan
                            menggunakan
                            Mandiri
                            Clickpay.
                        </p>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="vbcaklikpay">
                <div class="payment-info">
                    <div class="heading">
                        <h6 class="title">
                            Anda tidak dikenakan biaya pelayanan.
                        </h6>
                    </div>

                    <div class="body">
                        <p>
                            <img src="/img/bcaklikpay-logo-originalbrands.png"
                                 width="100" alt="">
                        </p>
                        <p>Klik tombol Continue dan ikuti petunjuk di
                            halaman
                            berikutnya.</p>

                        <p>Anda akan diarahkan untuk melakukan pembayaran
                            melalui
                            BCA
                            KlikPay.</p>
                        <p>Untuk pembayaran tunai, Anda bisa memilih untuk
                            menggunakan
                            KlikBCA Individu atau BCA Card.</p>
                        <p>Nilai minimum per transaksi yang diperbolehkan
                            adalah
                            Rp10.000.</p>
                        <p>Nilai maksimum per transaksi yang diperbolehkan
                            adalah
                            Rp100.000.000.</p>
                        <p>Setelah melakukan pembayaran, klik 'Kembali ke
                            situs
                            Toko'
                            untuk melanjutkan transaksi.</p>
                        <p>* Informasi cara pendaftaran, aktivasi, dan
                            pembayaran
                            dengan menggunakan BCA KlikPay dapat diakses
                            melalui
                            www.klikbca.com/klikpay dan Halo BCA 500888 atau
                            (021)
                            500888 dari ponsel.</p>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="vmandiriecash">
                <div class="payment-info">
                    <div class="heading">
                        <h6 class="title">
                            Anda tidak dikenakan biaya pelayanan.
                        </h6>
                    </div>
                    <div class="body">
                        <p>
                            <img src="/img/mandiri-logo-originalbrands.png"
                                 width="100" alt="">
                        </p>
                        <p>Klik tombol Continue dan ikuti petunjuk di
                            halaman
                            berikutnya.
                        </p>
                        <p>
                        <ol>
                            <li>
                                Anda akan diarahkan untuk melakukan
                                pembayaran
                                melalui Mandiri E-Cash.
                            </li>
                            <li>
                                Nilai maksimum per transaksi yang
                                diperbolehkan
                                adalah Rp1.000.000 untuk pemegang
                                unregistered
                                Mandiri E-Cash dan Rp5.000.000 untuk
                                pengguna
                                registered Mandiri E-Cash
                            </li>
                            <li>
                                Pastikan saldo Anda mencukupi untuk
                                melakukan
                                pembayaran.
                            </li>
                            <li>
                                Transaksi ini menggunakan OTP (one time
                                password)
                                SMS seharga Rp550 per SMS. Pastikan pulsa
                                anda
                                mencukupi untuk menerima OTP SMS dari Bank
                                Mandiri
                            </li>
                        </ol>
                        </p>
                        <p>
                            * Informasi cara pendaftaran, aktivasi, dan
                            detil
                            pembayaran dengan menggunakan Mandiri E-Cash
                            dapat
                            diakses melalui Mandiri E-Cash atau Mandiri Call
                            14000
                        </p>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="vcimbclick">
                <div class="payment-info">
                    <div class="heading">
                        <h6 class="title">
                            Anda tidak dikenakan biaya pelayanan.
                        </h6>
                    </div>

                    <div class="body">
                        <p>
                            <img src="/img/cimbclick-logo-originalbrands.png"
                                 width="100" alt="">
                        </p>
                        <p>Pembayaran menggunakan CIMB Clicks atau Rekening
                            Ponsel
                            dilakukan secara online. Saldo Anda akan didebet
                            secara
                            online sesuai total yang harus dibayar.</p>
                        <p>Nilai minimum per transaksi untuk pembayaran
                            dengan
                            CIMB
                            Clicks adalah Rp 10.000.</p>
                        <p>Nilai maksimum per transaksi untuk pembayaran
                            dengan
                            Rekening Ponsel adalah Rp 5.000.000.</p>
                        <p>Klik tombol Continue dan ikuti petunjuk di
                            halaman
                            berikutnya.
                            *Untuk informasi lebih Continue hubungi Call
                            Center
                            CIMB
                            Niaga
                            di 14041.</p>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="vcreditcard">
                <div class="payment-info">
                    <div class="heading">
                        <h6 class="title">
                            Anda tidak dikenakan biaya pelayanan.
                        </h6>
                    </div>

                    <div class="body">
                        <p>
                            <img src="/img/visa-logo-originalbrands.png"
                                 width="100" alt="">
                            <img src="/img/mastercard-logo-originalbrands.png"
                                 width="100" alt="">
                        </p>
                        <p>Pembayaran menggunakan Kartu Kredit dilakukan
                            secara
                            online.
                            Nilai minimum per transaksi untuk pembayaran
                            dengan
                            kartu
                            kredit adalah Rp10.000</p>
                        <p>Klik tombol Continue untuk melanjutkan transaksi.
                            Anda
                            akan
                            diarahkan pada halaman pembayaran dengan
                            menggunakan
                            kartu
                            kredit.</p>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="vindomaret">
                <div class="payment-info">
                    <div class="heading">
                        <h6 class="title">
                            Anda tidak dikenakan biaya pelayanan.
                        </h6>
                    </div>

                    <div class="body">
                        <p>
                            <img src="/img/indomaret-logo-originalbrands.png"
                                 width="100" alt="">
                        </p>
                        <p>Ketentuan pembayaran dengan Indomaret:</p>

                        <p>Nilai maksimum per transaksi yang diperbolehkan
                            adalah
                            Rp5.000.000.</p>
                        <p>Anda akan dikenakan biaya pembayaran per
                            transaksi
                            sebesar
                            Rp7.500 (di luar total belanja) yang dibayarkan
                            langsung
                            saat pembayaran melalui gerai Indomaret. Ini
                            merupakan
                            ketentuan sepenuhnya dari Indomaret dan dapat
                            berubah
                            sewaktu-waktu tanpa pemberitahuan
                            sebelumnya.</p>
                        <p>Metode pembayaran ini bergantung kepada jam
                            operasional
                            cabang gerai Indomaret pilihan Anda. Hal
                            tersebut
                            sepenuhnya
                            berada di luar kuasa dan tanggung jawab
                            Bukalapak.com.
                            Transaksi dianggap batal jika sampai dengan
                            pukul
                            14:22
                            WIB
                            hari Kamis, 17 Desember 2015 (1×12 jam)
                            pembayaran
                            belum
                            dilunasi.</p>
                        <p>Klik tombol Continue dan ikuti petunjuk di
                            halaman
                            berikutnya.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>