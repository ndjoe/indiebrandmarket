<div class="home-banner">
    @foreach($sliders as $s)
        @if($s->image !== null)
            <img src="/images/sliders/{{ $s->image }}" class="img-responsive">
        @endif
    @endforeach
</div>