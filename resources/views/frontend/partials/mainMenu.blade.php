<div class="header-navigation">
    <ul>
        <li><a href="/">Magazine</a></li>
        <li><a href="/new-arrival">What's New</a></li>
        <li><a href="/brands">Brands</a></li>
        <li><a href="/category">Categories</a></li>
        <li><a href="/sale">Sale</a></li>
        @if(Auth::check())
            <li><a href="/confirm-order">Confirm Order</a></li>
            <li><a href="/account">My Profile</a></li>
        @else
            <li><a href="/login">Login/Register</a></li>
        @endif
    </ul>
</div>