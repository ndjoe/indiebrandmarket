<div class="row margin-bottom-10 sale-product">
    <div class="col-md-12">
        <h5 class="text-center title-section"><span>Another Products</span></h5>

        <div class="row margin-bottom-10">
            @foreach($anotherProducts as $product)
                <div class="col-md-2 col-xs-4 margin-bottom-10">
                    <div class="product clearfix">
                        <div class="product-image">
                            <a href="/brand/{{ $product['author']['username'] }}/{{ $product['slug'] }}">
                                <img src="/images/catalog/{{ $product['images'][0]['name'] }}">
                            </a>
                            @if($product['images'][1]['name'] !== '')
                                <a href="/brand/{{ $product['author']['username'] }}/{{ $product['slug'] }}">
                                    <img src="/images/catalog/{{ $product['images'][1]['name'] }}">
                                </a>
                            @endif
                            @if(\Carbon\Carbon::parse($product['discount']['expired_at'])->isFuture() && $product['discount']['value'] > 0)
                                <div class="sale-flash">
                                    {{ $product['discount']['value'] }}% off
                                </div>
                            @endif
                        </div>
                        <div class="product-desc">
                            <div class="product-brand">
                                <h3>
                                    <a href="/brand/{{ $product['author']['username'] }}">{{ $product['author']['nama'] }}
                                    </a>
                                </h3>
                            </div>
                            <div class="product-title">
                                <h3>
                                    <a href="/brand/{{ $product['author']['username'] }}">{{ $product['name'] }}</a>
                                </h3>
                            </div>
                            <div class="product-price">
                                @if($product['discount']['expired_at'] > \Carbon\Carbon::now() && $product['discount']['value'] > 0)
                                    @if(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getCurrentLocale() === 'id')

                                        <del>
                                            IDR {{ number_format($product['price']*1, 0, ',', '.') }}
                                        </del>
                                        <ins>
                                            IDR {{ number_format((100 - $product['discount']['value'])*$product['price']/100, 0, ',', '.') }}
                                        </ins>
                                    @else
                                        <del>
                                            USD {{ number_format($product['price']*1,2, ',', '.') }}
                                        </del>
                                        <ins>
                                            USD {{ number_format((100 - $product['discount']['value'])*$product['price']/100,2, ',', '.') }}
                                        </ins>
                                    @endif
                                @else
                                    @if(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getCurrentLocale() === 'id')

                                        <ins>
                                            IDR {{ number_format($product['price']*1, 0, ',', '.') }}
                                        </ins>
                                    @else
                                        <ins>
                                            USD {{ number_format($product['price']*1,2, ',', '.') }}
                                        </ins>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
