<template id="cart">
    <div class="top-cart-block" v-cloak>
        <div class="top-cart-info">
            <a href="/cart" class="top-cart-info-count" v-cloak>@{{ cart | sumArray }} items</a>
        </div>
        <i class="fa fa-shopping-cart"></i>

        <div class="top-cart-content-wrapper">
            <div class="top-cart-content" v-if="cart.length > 0">
                <ul class="scroller" style="height: 250px;">
                    <li v-for="c in cart">
                        <a v-bind:href="'/'+locale+'/brand/'+c['options']['brand']+'/'+c['options']['slug']">
                            <img v-bind:src="'/images/catalog/'+c['options']['image']" width="37" height="34">
                        </a>
                        <span class="cart-content-count" v-cloak>x @{{ c['qty'] }}</span>
                        <strong>
                            <a v-bind:href="'/'+locale+'/brand/'+c['options']['brand']+'/'+c['options']['slug']"
                               v-cloak>
                                @{{ c['name'] }}
                                - Size: @{{ c['options']['sizeText'] }}
                            </a>
                        </strong>
                        <em v-cloak>@{{ currency }} @{{ (100 - c['options']['discount'])*c['price']/100 | formatNumber }}</em>
                        <a v-on:click="delItem(c)" class="del-goods">&nbsp;</a>
                    </li>
                </ul>
                <div class="text-right">
                    <a href="/cart" class="btn btn-primary">Checkout</a>
                </div>
            </div>
        </div>
    </div>
</template>