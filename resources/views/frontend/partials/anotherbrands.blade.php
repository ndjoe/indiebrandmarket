<div class="row margin-bottom-10 brand-section">
    <h5 class="text-center title-section"><span>Another Brands</span></h5>
    <div class="brand-container">
        @foreach($anotherBrands as $brand)
            <div>
                <a href="/brand/{{ $brand['username'] }}">
                    <img src="/images/logo/{{ $brand['profileable']['logo'] }}" alt=""
                         class="img-thumbnail brand-logo">
                </a>
            </div>
        @endforeach
    </div>
</div>