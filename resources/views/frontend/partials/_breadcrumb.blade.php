<ul class="breadcrumb">
    <li>
        <a href="/">home</a>
    </li>
    <li>
        <a href="#">{{ $subcategory }}</a>
    </li>
    <li>
        <a href="/product/{{ $subcategory }}/{{ $product->slug }}">{{ $product->name }}</a>
    </li>
</ul>