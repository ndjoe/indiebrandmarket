<div class="brand-banner col-xs-12">
    <div class="image clearfix">
        <img src="/images/banner/{{ $brand->profileable->banner_image }}">
    </div>
    <div class="description">
        <h1 class="name">{{ $brand->nama }}</h1>

        <p>
            {{ $brand->profileable->about }}
        </p>
    </div>
    <div class="logo">
        <img src="/images/logo/{{ $brand->profileable->logo }}" alt="{{ $brand->nama }}">
    </div>
</div>
