<script type="text/x-template" id="modal">
    <div class="modal fade in" tabindex="-1" role="basic" aria-hidden="true"
         v-show="show" v-transition="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <content select=".modal-header">
                    <div class="modal-header">
                        default header
                    </div>
                </content>
                <content select=".modal-body">
                    <div class="modal-body">
                        default body
                    </div>
                </content>
                <content select=".modal-footer">
                    <div class="modal-footer">
                        default footer
                        <button class="modal-default-button"
                                v-on="click: show = false">
                            OK
                        </button>
                    </div>
                </content>
            </div>
        </div>
    </div>
</script>