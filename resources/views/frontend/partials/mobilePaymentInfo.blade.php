<div class="mobile-only">
    <div class="panel-group accordion" id="mobile-info-payment"
         role="tablist" aria-multiselectable="true">
        <div class="panel panel-default panel-info-payment">
            <div class="panel-heading" role="tab">
                <h4 class="panel-title">
                    <div class="checkbox">
                        <label data-toggle="collapse"
                               data-target="#manualtrans">
                            <input type="radio" name="payment"
                                   value="manualtrans"/>
                            Manual Transfer
                        </label>
                    </div>
                </h4>
            </div>
            <div id="manualtrans" class="panel-collapse collapse"
                 role="tabpanel">
                <div class="payment-info">
                    <div class="body clearfix">
                        <h6>
                            Ketentuan pembayaran dengan transfer bank:
                        </h6>
                        <p>
                            <img src="/img/bca-logo-originalbrands.png"
                                 width="100" alt="">
                            <img src="/img/mandiri-logo-originalbrands.png"
                                 width="100" alt="">
                        </p>
                        <p>
                            Transaksi dengan menggunakan metode pembayaran
                            ATM
                            akan
                            ditambahkan <strong class="text-uppercase">harga unik</strong>, <strong
                                    class="text-uppercase">harga unik</strong>
                            dimaksudkan untuk
                            mempermudah proses verifikasi.
                        </p>
                        <p>
                            Harga unik anda adalah: <strong>
                                @{{ currency }} @{{ uniqueCost | formatNumber currency }}
                            </strong>
                        </p>
                        <p>
                            Pembayaran yang wajib dibayarkan sesuai dengan jumlah <strong class="text-uppercase">harga
                                unik</strong>, bukan <strong class="text-uppercase" style="color: red">jumlah
                                total</strong>.
                        </p>
                        <p>
                            Transaksi dianggap batal jika sampai dengan
                            tanggal
                            {{ \Carbon\Carbon::now("Asia/Jakarta")->addDay(1)->toDateTimeString() }}
                            (1×24 jam) pembayaran
                            belum
                            dilunasi.
                        </p>
                        <p>
                            Klik tombol <strong class="text-uppercase">Continue</strong> jika anda telah memahami
                            dan
                            menyetujui
                            ketentuan transaksi di atas.
                        </p>
                        <button class="btn btn-primary btn-circle pull-right hidden-xs" type="submit"
                                v-on:click="submitCheckout" v-if="checkoutValid && !submitting" v-cloak>
                            Continue
                        </button>
                        <button class="btn btn-primary btn-circle pull-right hidden-xs" type="button"
                                disabled v-if="!checkoutValid && !submitting" v-cloak>
                            Continue
                        </button>
                        <button class="btn btn-primary btn-circle pull-right hidden-xs" type="button"
                                disabled v-if="submitting" v-cloak>
                            <i class="fa fa-spinner fa-spin"></i> Saving
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default panel-info-payment">
            <div class="panel-heading" role="tab">
                <h4 class="panel-title">
                    <div class="checkbox">
                        <label data-toggle="collapse"
                               data-target="#clickpaymandiri">
                            <input type="radio" name="payment"
                                   value="mandiriclickpay"/>
                            Mandiri Clickpay
                        </label>
                    </div>
                </h4>
            </div>
            <div id="clickpaymandiri" class="panel-collapse collapse"
                 role="tabpanel">
                <div class="payment-info">
                    <div class="body clearfix">
                        <h6>
                            Anda tidak dikenakan biaya pelayanan.
                        </h6>
                        <p>
                            <img src="/img/mandiriclickpay-logo-originalbrands.png"
                                 width="100" alt="">
                        </p>
                        <p>
                            Klik tombol Continue untuk melanjutkan
                            transaksi.
                            Anda
                            akan
                            diarahkan pada halaman pembayaran dengan
                            menggunakan
                            Mandiri
                            Clickpay.
                        </p>
                        <button class="btn btn-primary btn-circle pull-right"
                                type="button">
                            Continue
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default panel-info-payment">
            <div class="panel-heading" role="tab">
                <h4 class="panel-title">
                    <div class="checkbox">
                        <label data-toggle="collapse"
                               data-target="#klikpaybca">
                            <input type="radio" name="payment"
                                   value="bcaklikpay"
                                   v-model="payment">
                            BCA klikpay
                        </label>
                    </div>
                </h4>
            </div>
            <div id="klikpaybca" class="panel-collapse collapse"
                 role="tabpanel">
                <div class="payment-info">
                    <div class="body clearfix">
                        <h6>
                            Anda tidak dikenakan biaya pelayanan.
                        </h6>
                        <p>
                            <img src="/img/bcaklikpay-logo-originalbrands.png"
                                 width="100" alt="">
                        </p>
                        <p>Klik tombol Continue dan ikuti petunjuk di
                            halaman
                            berikutnya.</p>

                        <p>Anda akan diarahkan untuk melakukan pembayaran
                            melalui
                            BCA
                            KlikPay.</p>
                        <p>Untuk pembayaran tunai, Anda bisa memilih untuk
                            menggunakan
                            KlikBCA Individu atau BCA Card.</p>
                        <p>Nilai minimum per transaksi yang diperbolehkan
                            adalah
                            Rp10.000.</p>
                        <p>Nilai maksimum per transaksi yang diperbolehkan
                            adalah
                            Rp100.000.000.</p>
                        <p>Setelah melakukan pembayaran, klik 'Kembali ke
                            situs
                            Toko'
                            untuk melanjutkan transaksi.</p>
                        <p>* Informasi cara pendaftaran, aktivasi, dan
                            pembayaran
                            dengan menggunakan BCA KlikPay dapat diakses
                            melalui
                            www.klikbca.com/klikpay dan Halo BCA 500888 atau
                            (021)
                            500888 dari ponsel.</p>
                        <button class="btn btn-primary btn-circle pull-right"
                                type="button">
                            Continue
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default panel-info-payment">
            <div class="panel-heading" role="tab">
                <h4 class="panel-title">
                    <div class="checkbox">
                        <label data-toggle="collapse"
                               data-target="#ecashmandiri">
                            <input type="radio" name="payment"
                                   value="mandiriecash"
                                   v-model="payment">
                            Mandiri E-cash
                        </label>
                    </div>
                </h4>
            </div>
            <div id="ecashmandiri" class="panel-collapse collapse"
                 role="tabpanel">
                <div class="payment-info">
                    <div class="body clearfix">
                        <h6>
                            Anda tidak dikenakan biaya pelayanan.
                        </h6>
                        <p>
                            <img src="/img/mandiri-logo-originalbrands.png"
                                 width="100" alt="">
                        </p>
                        <p>Klik tombol Continue dan ikuti petunjuk di
                            halaman
                            berikutnya.
                        </p>
                        <p>
                        <ol>
                            <li>
                                Anda akan diarahkan untuk melakukan
                                pembayaran
                                melalui Mandiri E-Cash.
                            </li>
                            <li>
                                Nilai maksimum per transaksi yang
                                diperbolehkan
                                adalah Rp1.000.000 untuk pemegang
                                unregistered
                                Mandiri E-Cash dan Rp5.000.000 untuk
                                pengguna
                                registered Mandiri E-Cash
                            </li>
                            <li>
                                Pastikan saldo Anda mencukupi untuk
                                melakukan
                                pembayaran.
                            </li>
                            <li>
                                Transaksi ini menggunakan OTP (one time
                                password)
                                SMS seharga Rp550 per SMS. Pastikan pulsa
                                anda
                                mencukupi untuk menerima OTP SMS dari Bank
                                Mandiri
                            </li>
                        </ol>
                        </p>
                        <p>
                            * Informasi cara pendaftaran, aktivasi, dan
                            detil
                            pembayaran dengan menggunakan Mandiri E-Cash
                            dapat
                            diakses melalui Mandiri E-Cash atau Mandiri Call
                            14000
                        </p>
                        <button class="btn btn-primary btn-circle pull-right"
                                type="button">
                            Continue
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default panel-info-payment">
            <div class="panel-heading" role="tab">
                <h4 class="panel-title">
                    <div class="checkbox">
                        <label data-toggle="collapse"
                               data-target="#clickcimb">
                            <input type="radio" name="payment"
                                   value="cimbclick"
                                   v-model="payment">
                            CIMBclick
                        </label>
                    </div>
                </h4>
            </div>
            <div id="clickcimb" class="panel-collapse collapse"
                 role="tabpanel">
                <div class="payment-info">
                    <div class="body clearfix">
                        <h6>
                            Anda tidak dikenakan biaya pelayanan.
                        </h6>
                        <p>
                            <img src="/img/cimbclick-logo-originalbrands.png"
                                 width="100" alt="">
                        </p>
                        <p>Pembayaran menggunakan CIMB Clicks atau Rekening
                            Ponsel
                            dilakukan secara online. Saldo Anda akan didebet
                            secara
                            online sesuai total yang harus dibayar.</p>
                        <p>Nilai minimum per transaksi untuk pembayaran
                            dengan
                            CIMB
                            Clicks adalah Rp 10.000.</p>
                        <p>Nilai maksimum per transaksi untuk pembayaran
                            dengan
                            Rekening Ponsel adalah Rp 5.000.000.</p>
                        <p>Klik tombol Continue dan ikuti petunjuk di
                            halaman
                            berikutnya.
                            *Untuk informasi lebih Continue hubungi Call
                            Center
                            CIMB
                            Niaga
                            di 14041.</p>
                        <button class="btn btn-primary btn-circle pull-right"
                                type="button">
                            Continue
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default panel-info-payment">
            <div class="panel-heading" role="tab">
                <h4 class="panel-title">
                    <div class="checkbox">
                        <label data-toggle="collapse"
                               data-target="#cardcredit">
                            <input type="radio" name="payment"
                                   value="creditcard"
                                   v-model="payment">
                            Creditcard
                        </label>
                    </div>
                </h4>
            </div>
            <div id="cardcredit" class="panel-collapse collapse"
                 role="tabpanel">
                <div class="payment-info">
                    <div class="body clearfix">
                        <h6>
                            Anda tidak dikenakan biaya pelayanan.
                        </h6>
                        <p>
                            <img src="/img/visa-logo-originalbrands.png"
                                 width="100" alt="">
                            <img src="/img/mastercard-logo-originalbrands.png"
                                 width="100" alt="">
                        </p>
                        <p>Pembayaran menggunakan Kartu Kredit dilakukan
                            secara
                            online.
                            Nilai minimum per transaksi untuk pembayaran
                            dengan
                            kartu
                            kredit adalah Rp10.000</p>
                        <p>Klik tombol Continue untuk melanjutkan transaksi.
                            Anda
                            akan
                            diarahkan pada halaman pembayaran dengan
                            menggunakan
                            kartu
                            kredit.</p>
                        <button class="btn btn-primary btn-circle pull-right"
                                type="button">
                            Continue
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default panel-info-payment">
            <div class="panel-heading" role="tab">
                <h4 class="panel-title">
                    <div class="checkbox">
                        <label data-toggle="collapse"
                               data-target="#maretindo">
                            <input type="radio" name="payment"
                                   value="indomaret"
                                   v-model="payment">
                            Indomaret
                        </label>
                    </div>
                </h4>
            </div>
            <div id="maretindo" class="panel-collapse collapse"
                 role="tabpanel">
                <div class="payment-info">
                    <div class="body clearfix">
                        <h6>
                            Anda tidak dikenakan biaya pelayanan.
                        </h6>
                        <p>
                            <img src="/img/indomaret-logo-originalbrands.png"
                                 width="100" alt="">
                        </p>
                        <p>Ketentuan pembayaran dengan Indomaret:</p>

                        <p>Nilai maksimum per transaksi yang diperbolehkan
                            adalah
                            Rp5.000.000.</p>
                        <p>Anda akan dikenakan biaya pembayaran per
                            transaksi
                            sebesar
                            Rp7.500 (di luar total belanja) yang dibayarkan
                            langsung
                            saat pembayaran melalui gerai Indomaret. Ini
                            merupakan
                            ketentuan sepenuhnya dari Indomaret dan dapat
                            berubah
                            sewaktu-waktu tanpa pemberitahuan
                            sebelumnya.</p>
                        <p>Metode pembayaran ini bergantung kepada jam
                            operasional
                            cabang gerai Indomaret pilihan Anda. Hal
                            tersebut
                            sepenuhnya
                            berada di luar kuasa dan tanggung jawab
                            Bukalapak.com.
                            Transaksi dianggap batal jika sampai dengan
                            pukul
                            14:22
                            WIB
                            hari Kamis, 17 Desember 2015 (1×12 jam)
                            pembayaran
                            belum
                            dilunasi.</p>
                        <p>Klik tombol Continue dan ikuti petunjuk di
                            halaman
                            berikutnya.</p>
                        <button class="btn btn-primary btn-circle pull-right"
                                type="button">
                            Continue
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
