@extends('frontend.frontend')

@section('title')
    <title>Checkout Order</title>
@endsection

@section('morecss')
@endsection

@section('content')
    <div class="row margin-bottom-40" v-cloak>
        <div class="col-md-12 col-sm-12">
            <h1>Checkout</h1>

            <div class="panel-group checkout-page accordion scrollable" id="checkout-page">
                <form v-on:submit="submitCheckout">
                    <div id="confirm" class="panel panel-default panel-product">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#checkout-page" href="#confirm-content"
                                   class="accordion-toggle">
                                    Step 1: Review Cart
                                </a>
                            </h2>
                        </div>
                        <div id="confirm-content" class="panel-collapse collapse in">
                            <div class="panel-body row">
                                <div class="col-md-12 clearfix checkout-section">
                                    <div class="table-wrapper-responsive">
                                        <table>
                                            <tr>
                                                <th class="checkout-image hidden-xs">Image</th>
                                                <th class="checkout-description">Description</th>
                                                <th class="checkout-quantity">Quantity</th>
                                                <th class="checkout-price hidden-xs">Price</th>
                                                <th class="checkout-total">Total</th>
                                            </tr>
                                            <tr v-for="c in cart">
                                                <td class="goods-page-image hidden-xs">
                                                    <a :href="'brand/'+c.options.brand+'/'+c.options.slug">
                                                        <img :src="'/images/catalog/' + c['options']['image']"
                                                             alt="@{{ c['name'] }}"></a>
                                                </td>
                                                <td class="goods-page-description">
                                                    <h3>
                                                        <a :href="'brand/'+c.options.brand+'/'+c.options.slug">
                                                            @{{ c['name'] }}
                                                        </a>
                                                    </h3>

                                                    <p>
                                                        Size: @{{ c['options']['sizeText'] }}
                                                    </p>
                                                </td>
                                                <td class="goods-page-quantity">
                                                    <div class="product-quantity">
                                                        <input id="product-quantity" type="text" value="@{{ c['qty'] }}"
                                                               readonly disabled
                                                               class="form-control input-sm">
                                                    </div>
                                                </td>
                                                <td class="goods-page-price hidden-xs">
                                                    <strong>
                                                        <span>
                                                            @{{ currency }}
                                                        </span> @{{ (100 - c['options']['discount'])*c['price']/100 | formatNumber currency }}
                                                    </strong><br>
                                                    <template v-if="c['options']['discount'] > 0">
                                                        <s><span>@{{ currency }}</span> @{{ c['price'] | formatNumber currency }}
                                                        </s>
                                                    </template>
                                                </td>
                                                <td class="goods-page-total">
                                                    <strong>
                                                        <span>
                                                            @{{ currency }}
                                                        </span>
                                                        @{{ ((100 - c['options']['discount'])*c['price']/100)*c['qty'] | formatNumber currency }}
                                                    </strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="checkout-total-block">
                                        <ul>
                                            <li class="checkout-total-price">
                                                <em>Total</em>
                                                <strong class="price">
                                                    <span>@{{ currency }}</span>
                                                    @{{ subTotalCart | formatNumber currency }}
                                                </strong>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-primary btn-circle  pull-right" type="button"
                                            id="button-review-cart"
                                            data-toggle="collapse" data-parent="#checkout-page"
                                            data-target="#shipping-address-content">Continue
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="checkout" class="panel panel-default panel-product">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#checkout-page" href="#shipping-address-content"
                                   class="accordion-toggle">
                                    Step 2: Detail Pengiriman
                                </a>
                            </h2>
                        </div>
                        <div id="shipping-address-content" class="panel-collapse collapse">
                            <div class="panel-body row">
                                <div class="col-md-12 checkout-section clearfix">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group"
                                                 :class="!validator.nama && address.nama.length>0 ? 'has-error' : ''">
                                                <label for="nama">
                                                    Nama Lengkap <span class="require">*</span>
                                                </label>
                                                <input type="text" v-model="address.nama" class="form-control">
                                        <span class="help-block"
                                              v-show="!validator.nama && address.nama.length>0">
                                            Nama Harus Terisi
                                        </span>
                                            </div>
                                            <div class="form-group"
                                                 :class="!validator.nohp && address.nohp.length>0 ? 'has-error' : ''">
                                                <label for="telephone">
                                                    Telephone <span class="require">*</span>
                                                </label>
                                                <input type="text" v-model="address.nohp" class="form-control">
                                        <span class="help-block"
                                              v-show="!validator.nohp && address.nohp.length>0">
                                            Harus sesuai format 08xxxxx
                                        </span>
                                            </div>
                                            <div class="form-group"
                                                 :class="!validator.alamat && address.alamat.length>0 ? 'has-error' : ''">
                                                <label for="address">
                                                    Address <span class="require">*</span>
                                                </label>
                                                <input type="text" v-model="address.alamat" class="form-control">
                                            <span class="help-block"
                                                  v-show="!validator.alamat && address.alamat.length>0">
                                                Harus terisi
                                            </span>
                                            </div>
                                            <div class="form-group"
                                                 :class="!validator.kodepos && address.kodepos.length>0 ? 'has-error' : ''">
                                                <label for="post-code">
                                                    Post Code <span class="require">*</span>
                                                </label>
                                                <input type="text" v-model="address.kodepos" class="form-control">
                                        <span class="help-block"
                                              v-show="!validator.kodepos && address.kodepos.length>0">
                                            harus terisi
                                        </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="country">
                                                    Country <span class="require">*</span>
                                                </label>
                                                <select class="form-control input-sm" v-model="address.negara">
                                                    <option value="indonesia" selected>Indonesia</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="region-state">
                                                    Provinsi <span class="require">*</span>
                                                </label>
                                                <select v-selectize="address.provinsi" class="form-control"
                                                        options="listProvince"
                                                        placeholder="Pilih Provinsi"
                                                        settings="{create: false, selectOnTab: false}"></select>
                                            </div>
                                            <div class="form-group">
                                                <label for="city-dd">
                                                    City <span class="require">*</span>
                                                </label>
                                                <select v-selectize="address.kota" class="form-control"
                                                        options="listCity"
                                                        placeholder="Pilih Kota"
                                                        settings="{create: false, selectOnTab: false}"></select>
                                            </div>
                                            <div class="form-group">
                                                <label>Biaya pengiriman</label>
                                                <select class="form-control" v-model="shippingCost" disabled>
                                                    <option v-for="l in shippingList" :value="l.value">
                                                        @{{ l.text }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="well profile-overview">
                                                <p v-cloak class="nama">@{{ address.nama }}</p>
                                                <p v-cloak class="alamat">
                                                    @{{ address.alamat }},
                                                    @{{ address.kota | getText listCity }}
                                                </p>
                                                <p v-cloak class="alamat">
                                                    @{{ address.provinsi | getText listProvince }}
                                                    Indonesia @{{ address.kodepos }}
                                                </p>
                                                <p v-cloak class="nohp">Tel: @{{ address.nohp }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary btn-circle pull-right" type="button"
                                            id="button-shipping-address"
                                            data-toggle="collapse" data-parent="#checkout-page"
                                            data-target="#shipping-method-content">Continue
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="shipping-method" class="panel panel-default panel-product">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#checkout-page" href="#shipping-method-content"
                                   class="accordion-toggle">
                                    Step 3: Order Total Cost
                                </a>
                            </h2>
                        </div>
                        <div id="shipping-method-content" class="panel-collapse collapse">
                            <div class="panel-body row">
                                <div class="col-md-12 col-xs-12 grand-total clearfix">
                                    <div class="checkout-total-block">
                                        <ul>
                                            <li class="checkout-total-price">
                                                <em>Total Biaya Order
                                                    <br>
                                                    <small>termasuk diskon, voucher, biaya administrasi, dan biaya
                                                        pengiriman
                                                    </small>
                                                </em>
                                                <strong class="price">
                                                    <span>@{{ currency }}</span>
                                                    @{{ totalCost | formatNumber currency }}
                                                </strong>
                                            </li>
                                        </ul>
                                    </div>
                                    <button class="btn btn-primary btn-circle pull-right" type="button"
                                            id="button-payment-method"
                                            data-toggle="collapse" data-parent="#checkout-page"
                                            data-target="#payment-method-content">
                                        Continue
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="payment-method" class="panel panel-default panel-product">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#checkout-page" href="#payment-method-content"
                                   class="accordion-toggle">
                                    Step 4: Payment Method
                                </a>
                            </h2>
                        </div>
                        <div id="payment-method-content" class="panel-collapse collapse">
                            <div class="panel-body row">
                                <div class="col-md-12 checkout-section clearfix">
                                    <h4>Please select the preferred payment method to use on this order.</h4>

                                    <div class="payments">
                                        @if(LaravelLocalization::getCurrentLocale() === 'id')
                                            @include('frontend.partials.desktopPaymentInfo')
                                            @include('frontend.partials.mobilePaymentInfo')
                                        @endif
                                        @if(LaravelLocalization::getCurrentLocale() === 'en')
                                            <label>
                                                <img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/pp-acceptance-medium.png"
                                                     alt="Paypal" width="50px">
                                                Paypal
                                                <input type="radio" value="paypal" v-model="payment">
                                            </label>
                                        @endif
                                    </div>
                                    <button class="btn btn-primary btn-circle pull-right hidden-xs" type="submit"
                                            v-on:click="submitCheckout" v-if="checkoutValid && !submitting" v-cloak>
                                        Continue
                                    </button>
                                    <button class="btn btn-primary btn-circle pull-right hidden-xs" type="button"
                                            disabled v-if="!checkoutValid && !submitting" v-cloak>
                                        Continue
                                    </button>
                                    <button class="btn btn-primary btn-circle pull-right hidden-xs" type="button"
                                            disabled v-if="submitting" v-cloak>
                                        <i class="fa fa-spinner fa-spin"></i> Saving
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/js/shop-checkout.js"></script>
@endsection