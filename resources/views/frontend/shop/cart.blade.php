@extends('frontend.frontend')

@section('title')
    <title>Cart</title>
@endsection

@section('content')
    <div class="row margin-bottom-40" v-cloak>
        <div class="col-md-12 col-sm-12">
            <h1>Shopping cart</h1>
            <template v-if="!cart.length" v-cloak>
                <div class="alert alert-warning">
                    Anda tidak mempunyai item di keranjang anda silahkan <a href="/">kembali berbelanja</a>
                </div>
            </template>
            <template v-if="birthdayDiscount && cart.length > 0" v-cloak>
                <div class="alert alert-info">
                    Selamat ulang tahun anda berhak mendapatkan discount 5% untuk pembelian ini
                </div>
            </template>

            <div class="goods-page">
                <div class="goods-data clearfix">
                    <div class="table-wrapper-responsive">
                        <table summary="Shopping cart">
                            <tr>
                                <th class="goods-page-image hidden-xs">Image</th>
                                <th class="goods-page-description">Description</th>
                                <th class="goods-page-quantity hidden-xs">Quantity</th>
                                <th class="goods-page-price hidden-xs">Unit price</th>
                                <th class="goods-page-total">Subtotal</th>
                                <th>Delete</th>
                            </tr>
                            <tr v-for="c in cart">
                                <td class="goods-page-image hidden-xs">
                                    <a :href="'brand/'+c.options.brand+'/'+c.options.slug">
                                        <img :src="'/images/catalog/'+c['options']['image']">
                                    </a>
                                </td>
                                <td class="goods-page-description">
                                    <h3>
                                        <a :href="'brand/'+c.options.brand+'/'+c.options.slug" v-cloak>
                                            @{{ c['name'] }}
                                        </a>
                                    </h3>

                                    <p v-cloak>Size: @{{ c['options']['sizeText'] | uppercase }}</p>
                                    <template v-if="c['options']['discount']>0" v-cloak>
                                        <p>Discount: @{{ c['options']['discount'] }} %</p>
                                    </template>
                                    <div class="hidden-sm">
                                        <p class="hidden-md" v-cloak>Qty: @{{ c['qty'] }}</p>
                                    </div>
                                </td>
                                <td class="goods-page-quantity hidden-xs">
                                    <div class="product-quantity">
                                        <input id="product-quantity" type="text" value="@{{ c['qty'] }}"
                                               readonly
                                               class="form-control input-sm">
                                    </div>
                                </td>
                                <td class="goods-page-price hidden-xs">
                                    <strong v-cloak>
                                        <span>
                                            @{{ currency }}
                                        </span>
                                        @{{ (100 - c['options']['discount'])*c['price']/100 | formatNumber currency }}
                                    </strong>
                                    <br>
                                    <template v-if="c['options']['discount'] > 0" v-cloak>
                                        <s><span>@{{ currency }}</span> @{{ c['price'] | formatNumber currency }}</s>
                                    </template>
                                </td>
                                <td class="goods-page-total" v-cloak>
                                    <strong><span>@{{ currency }}</span>@{{ c | sumPrice | formatNumber currency }}
                                    </strong>
                                </td>
                                <td class="del-goods-col">
                                    <a class="del-goods" href="javascript:;" v-on:click="delItem(c)">&nbsp;</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="pull-left margin-top-10 coupon-section">
                        @if(Auth::check())
                            <form class="form-inline form-coupon" v-on:submit="addCoupon">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Redeem Voucher" v-model="coupon">
                                </div>
                                <button class="btn btn-primary" type="submit">Redeem Voucher</button>
                            </form>
                        @endif
                    </div>
                    <div class="shopping-total">
                        <ul>
                            <li class="shopping-total-price">
                                <template v-if="voucher.value > 0" v-cloak>
                                    <em>SubTotal</em>
                                    <strong class="price"><span>@{{ currency }}</span>@{{ cart | total | formatNumber currency }}
                                    </strong>
                                </template>
                                <template v-if="!voucher.value">
                                    <em>Total</em>
                                    <strong class="price"><span>@{{ currency }}</span>@{{ cart | total | formatNumber currency }}
                                    </strong>
                                </template>
                            </li>
                            <template v-if="voucher.value > 0 || birthdayDiscount" v-cloak>
                                <li v-if="voucher.value > 0">
                                    <em>Voucher</em>
                                    <a class="del-goods" href="javascript:;" v-on:click="delCoupon()">&nbsp;</a>
                                    <strong class="price">-<span>@{{ currency }}</span>@{{ voucher.value*1 | formatNumber currency }}
                                    </strong>
                                </li>
                                <li v-if="birthdayDiscount">
                                    <em>BirthdayDiscount</em>
                                    <strong class="price">-
                                        <span>@{{ currency }}</span>@{{ birthdayValue | formatNumber currency }}
                                    </strong>
                                </li>
                                <li class="shopping-total-price">
                                    <em>Total</em>
                                    <strong class="price"><span>@{{ currency }}</span>@{{ grandTotal | formatNumber currency }}
                                    </strong>
                                </li>
                            </template>
                        </ul>
                    </div>
                </div>
                <a href="/" class="btn btn-default">Back shopping <i class="fa fa-shopping-cart"></i>
                </a>
                <template v-if="!cart.length" v-cloak>
                    <button class="btn btn-primary hidden-xs" disabled>
                        Anda Tidak Mempunyai item dikeranjang anda silahkan
                        kembali
                    </button>
                </template>
                <template v-if="cart.length>0" v-cloak>
                    <a class="btn btn-primary" href="/checkout">Checkout <i class="fa fa-check"></i></a>
                </template>
            </div>
        </div>
        <!-- END CONTENT -->
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/js/shop-cart.js"></script>
@endsection