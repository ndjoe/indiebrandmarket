@extends('frontend.frontend')

@section('title')
    <title>new arrival</title>
@endsection

@section('morecss')
@endsection

@section('content')
    <div class="row margin-bottom-10 hidden-xs">
        <div class="col-md-3">
            <div class="form-group">
                <select class="form-control" v-model="size">
                    <option value="0" disabled selected>Size</option>
                    <option value="0">All</option>
                    <option v-for="o in optionSize" :value="o.value">
                        @{{ o.text }}
                    </option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <select class="form-control" v-model="color">
                    <option value="0" disabled selected>Color</option>
                    <option value="0">All</option>
                    <option v-for="c in colours" :value="c.value">
                        @{{ c.text }}
                    </option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <select class="form-control" v-model="price">
                    <option value="0" disabled selected>Price</option>
                    <option value="0">All</option>
                    <option value="1">< 50k</option>
                    <option value="2">50k - 100k</option>
                    <option value="3">100k - 300k</option>
                    <option value="4">300k - 600k</option>
                    <option value="5"> > 600k</option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <select class="form-control" v-model="gender">
                    <option value="all" disabled selected>Gender</option>
                    <option value="all">All</option>
                    <option value="male">Man</option>
                    <option value="female">Woman</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row margin-bottom-10">
        <div class="col-md-12">
            <h2 class="text-center title-section">New Arrivals</h2>

            <div class="row margin-bottom-10 sale-product" v-for="d in data" v-cloak>
                <div class="col-md-2 col-xs-4" v-for="b in d">
                    <div class="product clearfix">
                        <div class="product-image">
                            <a :href="'/brand/'+b.author.username+'/'+b.slug">
                                <img :src="'/images/catalog/'+b.images[0].name">
                            </a>
                            <a :href="'/brand/'+b.author.username+'/'+b.slug">
                                <img :src="'/images/catalog/'+b.images[1].name" v-if="b.images[1].name.trim()">
                            </a>
                            <template v-if="isDiscounted(b.discount)">
                                <div class="sale-flash">
                                    @{{ b.discount.value }}% off
                                </div>
                            </template>
                        </div>
                        <div class="product-desc">
                            <div class="product-brand">
                                <h3>
                                    <a :href="'/brand/'+b.author.username">@{{ b.author.nama }}
                                    </a>
                                </h3>
                            </div>
                            <div class="product-title">
                                <h3><a :href="'/brand/'+b.author.username+'/'+b.slug">@{{ b.name }}</a></h3>
                            </div>
                            <div class="product-price">
                                <template v-if="isDiscounted(b.discount)">
                                    <del>@{{ currency }} @{{ b.price*1 | formatNumber currency }}</del>
                                    <ins>@{{ currency }} @{{ (100-b.discount.value)*b.price/100 | formatNumber currency }}</ins>
                                </template>
                                <template v-if="!isDiscounted(b.discount)">
                                    <ins>@{{ currency }} @{{ b.price*1 | formatNumber currency }}</ins>
                                </template>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ul class="pager">
                <li>
                    <a v-on:click="prev()" :class="{'disabled': isFirst}">
                        Previous
                    </a>
                </li>
                <li>
                    <a v-on:click="next()"
                       :class="{'disabled': isLast}">
                        Next
                    </a>
                </li>
            </ul>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/assets/plugins/moment.min.js"></script>
    <script>
        var vm = new Vue({
            el: '#shop',
            data: {
                data: [],
                currentPage: 1,
                totalPage: 1,
                brands: [],
                cart: [],
                currency: obid.locale === 'id' ? 'IDR' : 'USD',
                locale: obid.locale,
                newsletter: {
                    email: ''
                },
                size: 0,
                color: 0,
                category: 0,
                gender: 'all',
                price: 0,
                optionSize: [],
                colours: []
            },
            computed: {
                isLast: function () {
                    return this.currentPage === this.totalPage;
                },
                isFirst: function () {
                    return this.currentPage === 1;
                }
            },
            watch: {
                'size': function (nv, ov) {
                    if (nv === ov) {
                        return;
                    }
                    this.currentPage = 1;
                    this.getData();
                },
                'price': function (nv, ov) {
                    if (nv === ov) {
                        return;
                    }
                    this.currentPage = 1;
                    this.getData();
                },
                'color': function (nv, ov) {
                    if (nv === ov) {
                        return;
                    }
                    this.currentPage = 1;
                    this.getData();
                },
                'gender': function (nv, ov) {
                    if (nv === ov) {
                        return;
                    }

                    this.currentPage = 1;
                    this.getData();
                }
            },
            methods: {
                getData: function (page) {
                    if (!page) {
                        page = 1;
                    }
                    var self = this;
                    $.get('/' + obid.locale + '/api/products', {
                        query: this.query,
                        category: this.category,
                        size: this.size,
                        color: this.color,
                        price: this.price,
                        gender: this.gender,
                        page: page
                    }).done(function (data) {
                        self.data = data.data;
                        self.currentPage = data.current_page;
                        self.totalPage = data.last_page;
                    });
                },
                prev: function () {
                    if (this.currentPage === 1) {
                        return;
                    }
                    this.getData(this.currentPage - 1);
                },
                next: function () {
                    if (this.currentPage === this.totalPage) {
                        return;
                    }
                    this.getData(this.currentPage + 1);
                },
                getCart: function () {
                    var self = this;
                    $.get('/' + obid.locale + '/api/cart')
                            .done(function (resp) {
                                self.cart = resp.items;
                            });
                },
                delItem: function (data) {
                    var self = this;
                    var formData = new FormData;

                    formData.append('rowid', data['rowid']);

                    $.ajax({
                        url: '/' + obid.locale + '/api/cart/delete',
                        data: formData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: true,
                        type: 'POST'
                    }).done(function (data) {
                        self.cart = data.items;
                    });
                },
                getBrands: function () {
                    var self = this;
                    $.get('/' + obid.locale + '/api/brands')
                            .done(function (resp) {
                                self.brands = resp;
                            });
                },
                isDiscounted: function (discount) {
                    return moment().isBefore(discount.expired_at) && discount.value > 0;
                },
                subscribesNewsLetter: function (e) {
                    e.preventDefault();
                    var self = this;
                    var formData = new FormData;
                    formData.append('email', this.newsletter.email);

                    $.ajax({
                        url: '/' + obid.locale + '/api/newsletter',
                        data: formData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: true,
                        type: 'POST',
                        statusCode: {
                            422: function (e) {
                                toastr.error("email invalid");
                            }
                        }
                    }).done(function (data) {
                        if (data.subscribed) {
                            swal(
                                    "You have been subscribed",
                                    "Please save following code and use it for your next purchase: "
                                    + data.couponCode,
                                    "success"
                            );
                            self.newsletter.email = "";
                        } else {
                            swal(
                                    "Ooops...",
                                    "Your have been subscribed to our news letter.... sorry for that",
                                    "error"
                            );
                            self.newsletter.email = "";
                        }
                    });
                },
                getOptions: function () {
                    var self = this;
                    $.get('/' + obid.locale + '/api/options')
                            .done(function (resp) {
                                self.optionSize = resp.size;
                                self.colours = resp.colours;
                            });
                },
            },
            ready: function () {
                Layout.init();
                this.getOptions();
                this.getData();
                this.getCart();
            }
        });
    </script>
@endsection