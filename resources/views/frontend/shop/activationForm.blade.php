@extends('frontend.frontend')

@section('title')
    <title>Account Activation</title>
@endsection

@section('content')
    <div class="row margin-bottom-40">
        <div class="col-md-4">
            <div class="well well-lg">
                <h3>Activate Your Account</h3>

                <form action="/{{ LaravelLocalization::getCurrentLocale() }}/activate" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="username">Activation Code:</label>
                        <input type="text" class="form-control" id="username" name="code">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary">Activate</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-8">
            @if(Session::has('register_success'))
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    {{ Session::get('register_success') }}
                </div>
            @endif
            <div class="row" v-if="counter > 0" v-cloak>
                <div class="col-md-12" id="clockdiv">
                    <div v-cloak>
                        @{{ counter }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-primary" v-if="counter < 1 && !sent" v-on:click="resendCode" v-cloak>
                        Resend Code
                    </button>
                    <div class="alert alert-success alert-dismissible" role="alert" v-if="sent" v-cloak>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        Kode aktifasi anda sudah kami kirim kan kembali
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/js/shop-account-activation.js"></script>
@endsection