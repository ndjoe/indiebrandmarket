@extends('frontend.frontend')

@section('title')
@endsection

@section('morecss')
    <link rel="stylesheet" href="/shop-assets/plugins/uniform/css/uniform.default.css">
@endsection

@section('content')
    <div class="row margin-bottom-40">
        @if(count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="col-md-4">
            <div class="well well-lg bg-black well-login">
                <div class="well-heading">
                    <h3 class="well-title text-center">Forgot Password</h3>
                </div>

                <form id="forgot" class="bg-grey"
                      action="/{{ LaravelLocalization::getCurrentLocale() }}/forgot-password" method="post">
                    {{ csrf_field() }}
                    <div class="form-group"
                         v-class="has-error: !validator.email.isEmail, has-success: validator.email.isHp">
                        <label>Email:</label>
                        <input type="text" v-model="email" class="form-control" name="email">
                        <span class="help-block" v-show="!validator.email.isEmail">
                            Harus dalam format: blabla@blabla.com
                        </span>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-8">
            @if(Session::has('notice.error'))
                <div class="alert alert-danger" role="alert">
                    {{ Session::pull('notice.error') }}
                </div>
            @endif
            @if(Session::has('notice.info'))
                <div class="alert alert-info" role="alert">
                    {{ Session::pull('notice.info') }}
                </div>
            @endif
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/validator.min.js"></script>
    <script>
        var vm = new Vue({
            el: '#shop',
            data: {
                cart: [],
                email: '',
                currency: obid.locale === 'id' ? 'IDR' : 'USD',
                locale: obid.locale,
                newsletter: {
                    email: ''
                }
            },
            computed: {
                validator: function () {
                    return {
                        email: {
                            isEmail: validator.isLength(this.email, 1) && validator.isEmail(this.email)
                        }
                    }
                }
            },
            methods: {
                getCart: function () {
                    var self = this;
                    $.get('/' + obid.locale + '/api/cart')
                            .done(function (resp) {
                                self.cart = resp.items;
                            });
                },
                delItem: function (data) {
                    var self = this;
                    var formData = new FormData;

                    formData.append('rowid', data['rowid']);

                    $.ajax({
                        url: '/' + obid.locale + '/api/cart/delete',
                        data: formData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: true,
                        type: 'POST'
                    }).done(function (data) {
                        self.cart = data.items;
                    });
                },
                subscribesNewsLetter: function (e) {
                    e.preventDefault();
                    var self = this;
                    var formData = new FormData;
                    formData.append('email', this.newsletter.email);

                    $.ajax({
                        url: '/' + obid.locale + '/api/newsletter',
                        data: formData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: true,
                        type: 'POST',
                        statusCode: {
                            422: function (e) {
                                toastr.error("email invalid");
                            }
                        }
                    }).done(function (data) {
                        if (data.subscribed) {
                            swal(
                                    "You have been subscribed",
                                    "Please save following code and use it for your next purchase: "
                                    + data.couponCode,
                                    "success"
                            );
                            self.newsletter.email = "";
                        } else {
                            swal(
                                    "Ooops...",
                                    "Your have been subscribed to our news letter.... sorry for that",
                                    "error"
                            );
                            self.newsletter.email = "";
                        }
                    });
                }

            },
            ready: function () {
                Layout.init();
                Layout.initTouchspin();
                Layout.initTwitter();
//                Layout.initFixHeaderWithPreHeader();
                Layout.initNavScrolling();
                this.getCart();
            }
        });
    </script>
@endsection