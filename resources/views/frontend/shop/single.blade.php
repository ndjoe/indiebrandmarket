@extends('frontend.frontend')

@section('title')
    <title>{{ $product['author']['nama']. ' - '. $product['name'] }}</title>
    {!! SEO::generate() !!}
@endsection

@section('content')
    <div class="row margin-bottom-40">
        <div id="gallery" class="col-md-6">
            <div class="row">
                <div class="col-xs-3 slider-nav">
                    @foreach($product['images'] as $image)
                        @if($image['name'] !== '')
                            <div>
                                <img src="/images/catalog/{{ $image['name'] }}" alt="" class="img-responsive">
                            </div>
                        @endif
                    @endforeach
                </div>
                <div class="col-xs-9 holder-image">
                    <div class="holder">
                        @foreach($product['images'] as $image)
                            @if($image['name'] !== '')
                                <div class="zoomit">
                                    <img src="/images/catalog/{{ $image['name'] }}" alt=""
                                         class="img-responsive">
                                </div>
                            @endif
                        @endforeach
                    </div>
                    @if($product['discount']['expired_at'] > \Carbon\Carbon::now() && $product['discount']['value'] > 0)
                        <div class="sale-flash-holder">
                            {{ $product['discount']['value'] }}% off
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div id="product-summary" class="col-md-6">
            <a href="/brand/{{ $product['author']['username'] }}">
                <h1 class="brand">{{ $product['author']['nama'] }}</h1>
            </a>

            <h1 class="name">{{ $product['name'] }}</h1>

            <div class="info">
                @if($product['discount']['expired_at'] > \Carbon\Carbon::now() && $product['discount']['value'] > 0)
                    @if(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getCurrentLocale() === 'id')
                        <span class="price on-sale">
                            <span class="sale-price">
                               IDR {{ number_format((100 - $product['discount']['value'])*$product['price']/100, 0, ',', '.') }}
                            </span>
                            <span class="regular-price">
                               IDR {{ number_format($product['price']*1, 0, ',', '.') }}
                            </span>
                        </span>
                    @else
                        <span class="price on-sale">
                            <span class="sale-price">
                               USD {{ number_format((100 - $product['discount']['value'])*$product['price']/100,2, ',', '.') }}
                            </span>
                            <span class="regular-price">
                               USD {{ number_format($product['price']*1,2, ',', '.') }}
                            </span>
                        </span>
                    @endif
                @else
                    <span class="price">
                        <span class="regular-price">
                            @if(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getCurrentLocale() === 'id')
                                IDR {{ number_format($product['price']*1, 0, ',', '.') }}
                            @else
                                USD {{ number_format($product['price']*1,2, ',', '.') }}
                            @endif
                        </span>
                    </span>
                @endif

            </div>
            <h2 class="category">Category: <span>{{ $product['category']['text'] }}</span></h2>
            <div class="row">
                <div class="col-md-12 social-share">
                    <span>Share:</span>
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::getUri()) }}"
                       onclick="window.open(
    'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href),
    'facebook-share-dialog',
    'width=626,height=436'
    );
    return false;" target="_blank">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="https://twitter.com/share?url={{ urlencode(Request::getUri()) }}&via=originalbrands.id&related=twitterapi%2Ctwitter&hashtags=example%2Cdemo&text={{ $product['name'].' by '.$product['author']['nama'] }}"
                       target="_blank">
                        <i class="fa fa-twitter"></i>
                    </a>
                </div>
            </div>
            <form class="product-form">
                <div class="row controls">
                    <div class="col-xs-6 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Sizes</label>

                            <div class="color">
                                @if(array_sum(array_column($options, 'qty')) > 0)
                                    <select class="form-control" v-model="size">
                                        <option value="">Pick a size</option>
                                        <option v-for="o in optionSize" v-if="o.value.qty > 0" :value="o.value">
                                            @{{ o.text }}
                                        </option>
                                    </select>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="quantity clearfix">
                            <div class="product-quantity">
                                <input type="number" v-el:qty id="product-quantity" value="1" readonly
                                       class="form-control input-sm">
                                <input type="hidden" v-el:id value="{{ $product['id'] }}">
                            </div>
                            @if(array_sum(array_column($options, 'qty')) > 0)
                                <button class="btn btn-primary" v-if="size && size.id > 0" v-on:click="addCart" v-cloak>
                                    Add to cart
                                </button>
                                <button class="btn btn-primary" v-else disabled v-cloak>
                                    Add to cart
                                </button>
                            @else
                                <button class="btn btn-primary" disabled>
                                    Sold Out
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
            </form>
            <div class="tab-panel">
                <ul class="nav nav-tab">
                    <li class="active">
                        <a href="#description" data-toggle="tab" aria-expanded="true">Description</a>
                    </li>
                    <li class="">
                        <a href="#sizing" data-toggle="tab" aria-expanded="true">Sizing Info</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="description">
                        <pre>{{ $product['description'] }}</pre>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="sizing">
                        @foreach($product['items'] as $item)
                            @if(!isEmptyString($item['sizing_info']))
                                <p>{{ \Illuminate\Support\Str::upper($item['size']['text']) }}</p>
                                <pre>{{ $item['sizing_info'] }}</pre>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('frontend.partials.anotherProducts')
    @include('frontend.partials.anotherbrands')
@endsection

@section('pagescripts')
    <script src="/shop-assets/js/shop-single.js"></script>
@endsection