@extends('frontend.frontend')

@section('title')
@endsection

@section('morecss')
    <link rel="stylesheet" href="/shop-assets/plugins/uniform/css/uniform.default.css">
    <link rel="stylesheet" href="/assets/plugins/bootstrap-datepicker.standalone.min.css">
@endsection

@section('content')
    <div class="row margin-bottom-40">
        @if(count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Session::has('notice.info'))
            <div class="alert alert-info">
                {{ Session::pull('notice.info') }}
            </div>
        @endif
        @if(Session::has('notice.error'))
            <div class="alert alert-danger">
                {{ Session::pull('notice.error') }}
            </div>
        @endif
        <div class="col-md-4">
            <div class="well well-lg">
                <h3>Confirm Your Order</h3>

                <form id="forgot" action="/{{ LaravelLocalization::getCurrentLocale() }}/confirm-order" method="post">
                    {{ csrf_field() }}
                    <div class="form-group"
                         :class="!validator.nomororder? 'has-error' : ''">
                        <label for="">Nomor Order:</label>
                        <input type="text" v-model="nomororder" class="form-control" name="nomororder">
                        <span class="help-block" v-show="!validator.nomororder">
                            Harus diisi
                        </span>
                    </div>
                    <div class="form-group"
                         :class="!validator.nama ? 'has-error':''">
                        <label for="">Atas Nama:</label>
                        <input type="text" v-model="nama" class="form-control"
                               name="nama">
                        <span class="help-block" v-show="!validator.nama">
                            Harus Diisi
                        </span>
                    </div>
                    <div class="form-group">
                        <label for="">Transfer Ke:</label>
                        <select name="bank" class="form-control">
                            <option value="bca" selected>BCA</option>
                            <option value="mandiri">Mandiri</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Transfer:</label>
                        <input type="text" class="form-control" v-datepicker="tgl" name="tgl">
                    </div>
                    <div class="form-group" :class="!validator.amount ? 'has-error':''">
                        <label for="">Sejumlah:</label>
                        <input type="text" class="form-control" name="amount" v-model="amount">
                        <span class="help-block" v-show="!validator.amount">
                            Harus Diisi
                        </span>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/validator.min.js"></script>
    <script src="/assets/plugins/bootstrap-datepicker.min.js"></script>
    <script>
        Vue.directive('datepicker', {
            twoWay: true,
            bind: function () {
                var self = this;
                $(this.el).datepicker({
                    format: "yyyy-mm-dd",
                    startView: 2,
                    immediateUpdates: true
                }).on('change', function () {
                    self.set(this.value);
                });
            },
            update: function (value) {
                $(this.el).val(value).trigger('change');
            }
        });
        var vm = new Vue({
            el: '#shop',
            data: {
                cart: [],
                nomororder: '',
                nama: '',
                amount: '',
                tgl: '',
                currency: obid.locale === 'id' ? 'IDR' : 'USD',
                locale: obid.locale,
                newsletter: {
                    email: ''
                }
            },
            computed: {
                validator: function () {
                    return {
                        nama: validator.isLength(this.nama, 1) && validator.isAlphanumeric(this.nama),
                        nomororder: validator.isNumeric(this.nomororder),
                        amount: validator.isNumeric(this.amount)
                    }
                }
            },
            methods: {
                getCart: function () {
                    var self = this;
                    $.get('/' + obid.locale + '/api/cart')
                            .done(function (resp) {
                                self.cart = resp.items;
                            });
                },
                delItem: function (data) {
                    var self = this;
                    var formData = new FormData;

                    formData.append('rowid', data['rowid']);

                    $.ajax({
                        url: '/' + obid.locale + '/api/cart/delete',
                        data: formData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: true,
                        type: 'POST'
                    }).done(function (data) {
                        self.cart = data.items;
                    });
                },
                subscribesNewsLetter: function (e) {
                    e.preventDefault();
                    var self = this;
                    var formData = new FormData;
                    formData.append('email', this.newsletter.email);

                    $.ajax({
                        url: '/' + obid.locale + '/api/newsletter',
                        data: formData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: true,
                        type: 'POST',
                        statusCode: {
                            422: function (e) {
                                toastr.error("email invalid");
                            }
                        }
                    }).done(function (data) {
                        if (data.subscribed) {
                            swal(
                                    "You have been subscribed",
                                    "Please save following code and use it for your next purchase: "
                                    + data.couponCode,
                                    "success"
                            );
                            self.newsletter.email = "";
                        } else {
                            swal(
                                    "Ooops...",
                                    "Your have been subscribed to our news letter.... sorry for that",
                                    "error"
                            );
                            self.newsletter.email = "";
                        }
                    });
                }

            },
            ready: function () {
                Layout.init();
                Layout.initTouchspin();
                Layout.initTwitter();
//                Layout.initFixHeaderWithPreHeader();
                Layout.initNavScrolling();
                this.getCart();
            },
            components: {}
        });
    </script>
@endsection