@extends('frontend.frontend')

@section('title')
    <title>Originalbrands.id</title>
@endsection


@section('content')
    <div class="row">
        @include('frontend.partials.sliderBrands')
    </div>
    <div class="row margin-bottom-10" v-cloak>
        <div class="col-md-12 sale-product">
            <h5 class="text-center title-section">
                <span>New Arrivals</span>
            </h5>

            <div class="row margin-bottom-10" v-for="d in data">
                <div class="col-md-2 col-xs-4 margin-bottom-10" v-for="item in d">
                    <div class="product clearfix">
                        <div class="product-image">
                            <a v-bind:href="'brand/'+item.author.username+'/'+item.slug">
                                <img :src="'/images/catalog/'+item.images[0].name">
                            </a>
                            <a :href="'brand/'+item.author.username+'/'+item.slug">
                                <img :src="'/images/catalog/'+item.images[1].name" v-if="item.images[1].name.trim()">
                            </a>
                            <template v-if="isDiscounted(item.discount)">
                                <div class="sale-flash" v-cloak>
                                    @{{ item.discount.value }}% off
                                </div>
                            </template>
                        </div>
                        <div class="product-desc">
                            <div class="product-brand">
                                <h3>
                                    <a :href="'brand/'+item.author.username" v-cloak>@{{ item.author.nama }}
                                    </a>
                                </h3>
                            </div>
                            <div class="product-title">
                                <h3>
                                    <a :href="'brand/'+item.author.username+'/'+item.slug" v-cloak>
                                        @{{ item.name }}
                                    </a>
                                </h3>
                            </div>
                            <div class="product-price">
                                <template v-if="isDiscounted(item.discount)">
                                    <del v-cloak>
                                        @{{ currency }} @{{ item.price*1 | formatNumber currency }}
                                    </del>
                                    <ins v-cloak>
                                        @{{ currency }} @{{ (100-item.discount.value)*item.price/100 | formatNumber currency }}
                                    </ins>
                                </template>
                                <template v-else>
                                    <ins v-cloak>
                                        @{{ currency }} @{{ item.price*1 | formatNumber currency }}
                                    </ins>
                                </template>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row margin-bottom-40 brand-section">
        <h5 class="text-center title-section"><span>Brands</span></h5>
        <span class="pull-right morebrand" style="margin-top: -38px">
                <button class="btn btn-circle btn-white" v-on:click="getBrands">more...</button>
            </span>
        <div class="brand-container">
            <div v-for="entry in brands">
                <a :href="'/'+locale+'/brand/'+entry['username']">
                    <img :src="'/images/logo/'+entry['profileable']['logo']" alt=""
                         class="img-thumbnail brand-logo">
                </a>
            </div>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/js/shop-front.js"></script>
@endsection