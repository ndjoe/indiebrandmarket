@extends('frontend.frontend')

@section('title')
    <title>Login/Register</title>
@endsection

@section('content')
    <div class="row margin-bottom-40">
        @if(count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Session::has('checkout'))
            <div class="alert alert-info">
                <p>
                    Anda harus login untuk melakukan order, silahkan register terlebih dahulu atau klik link berikut
                    untuk
                </p>
                <a href="/checkout?guest=true" class="btn btn-primary guest-btn">
                    GUEST CHECKOUT
                </a>
            </div>
        @endif
        <div class="col-md-4">
            @if(Session::has('notice.login.info'))
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    {{ Session::get('notice.login.info') }}
                </div>
            @endif
            @if(Session::has('notice.login.error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    {{ Session::get('notice.login.error') }}
                </div>
            @endif
            <div class="well well-lg bg-black well-login">
                <div class="well-heading">
                    <h3 class="well-title text-center">Login To Your Account</h3>
                </div>

                <form action="/{{ LaravelLocalization::getCurrentLocale() }}/login" class="bg-grey" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="" for="username">Username:</label>
                        <input type="text" class="form-control" id="username" name="username">
                    </div>
                    <div class="form-group">
                        <label class="" for="password">Password:</label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary">Login</button>
                        </div>
                        <div class="col-md-6">
                            <a href="/forgot-password" class="pull-right">Forgot Password?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-8">
            <h3>Don't Have Account? Register New Account</h3>
            @if(Session::has('notice.register.info'))
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    {{ Session::get('notice.register.info') }}
                </div>
            @endif
            @if(Session::has('notice.register.error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    {{ Session::get('notice.register.error') }}
                </div>
            @endif
            <form>
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group"
                             :class="validator.formRegister.username && usernameCheck ? '':'has-error'">
                            <label for="username">Username:</label>
                            <input type="text" v-model="formRegister.username" class="form-control" id="username"
                                   name="username" debounce="500">
                            <span class="help-block"
                                  v-show="!validator.formRegister.username && (formRegister.username.length > 0)"
                                  v-cloak>
                                username harus terisi dan Hanya boleh alpha numerictrue
                            </span>
                            <span class="help-block" v-show="!usernameCheck" v-cloak>
                                Username has been taken
                            </span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group"
                             :class="validator.formRegister.email && emailCheck ? '':'has-error'">
                            <label for="email">Email:</label>
                            <input type="text" v-model="formRegister.email" class="form-control" id="email"
                                   name="email" debounce="500">
                            <span class="help-block"
                                  v-show="!validator.formRegister.email && formRegister.email.length > 0" v-cloak>
                                Harus dalam format email blabla@blabla.com
                            </span>
                            <span class="help-block" v-show="!emailCheck" v-cloak>
                                Email has been taken
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group"
                             :class="validator.formRegister.password ? '':'has-error'">
                            <label for="password">Password:</label>
                            <input type="password" v-model="formRegister.password" class="form-control" id="password"
                                   name="password">
                            <span class="help-block"
                                  v-show="!validator.formRegister.password && formRegister.password.length > 0" v-cloak>
                                minimal 6 karakter
                            </span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group"
                             :class="validator.formRegister.password_confirm ? '':'has-error'">
                            <label for="password_confirmation">Confirm Your Password:</label>
                            <input type="password" v-model="formRegister.password_confirm" class="form-control"
                                   id="password_confirmation"
                                   name="password_confirmation">
                            <span class="help-block"
                                  v-show="!validator.formRegister.password_confirm && formRegister.password_confirm.length > 0"
                                  v-cloak>
                                harus sama dengan password
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group"
                             :class="validator.formRegister.nama ? '':'has-error'">
                            <label for="nama">Nama:</label>
                            <input type="text" v-model="formRegister.nama" class="form-control" name="nama" id="nama">
                            <span class="help-block"
                                  v-show="!validator.formRegister.nama && formRegister.nama.length>0" v-cloak>
                            harus terisi
                            </span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group"
                             :class="validator.formRegister.nohp && nohpCheck ? '':'has-error'">
                            <label for="nohp">Phone Number:</label>
                            <input type="text" v-model="formRegister.nohp" class="form-control" name="nohp" id="nohp"
                                   debounce="500">
                            <span class="help-block"
                                  v-show="!validator.formRegister.nohp && formRegister.nohp.length>0" v-cloak>
                                harus dalam format 08xxxx
                            </span>
                            <span class="help-block" v-show="!nohpCheck" v-cloak>
                                phone number has been taken
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <button type="button" class="btn btn-primary" v-on:click="showProfileModal = true">
                            Register
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <modal :show.sync="showProfileModal" v-cloak>
        <div class="modal-header">
            <button type="button" v-on:click="showProfileModal = false" class="close" data-dismiss="modal"
                    aria-hidden="true">X
            </button>
            <h4 class="modal-title">Isi info pengiriman kamu</h4>
        </div>
        <form v-on:submit="submitNewAccount">
            <div class="modal-body">
                <div class="form-group">
                    <label>Birthday</label>
                    <fieldset class="birthday-picker">
                        <select name="year" id="year" class="year_picker" v-model="profileRegister.year">
                            <option value="0">Tahun</option>
                            <option v-for="d in listYear" class="form-control" :value="d.value">@{{ d.text }}</option>
                        </select>
                        <select name="month" id="month" class="month_picker" v-model="profileRegister.month">
                            <option value="0">Bulan</option>
                            <option v-for="d in listMonth" class="form-control" :value="d.value">@{{ d.text }}</option>
                        </select>
                        <select name="date" id="date" class="date_picker" v-model="profileRegister.date">
                            <option value="0">Tgl</option>
                            <option v-for="d in listDate" class="form-control" :value="d.value">@{{ d.text }}</option>
                        </select>
                    </fieldset>
                </div>
                <div class="form-group">
                    <label>Gender</label>
                    <select name="gender" id="gender" class="form-control" v-model="profileRegister.gender">
                        <option value="unknown">Saya tidak tahu</option>
                        <option value="male">Man</option>
                        <option value="female">Woman</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Provinsi</label>
                    <select v-selectize="profileRegister.provinsi" class="form-control" options="listProvince"
                            placeholder="Pilih Provinsi" settings="{create: false, selectOnTab: false}"></select>
                </div>
                <div class="form-group">
                    <label>Kota</label>
                    <select v-selectize="profileRegister.kota" class="form-control" options="listCity"
                            placeholder="Pilih Kota" settings="{create: false, selectOnTab: false}"></select>
                </div>
                <div class="form-group"
                     :class="validator.formRegister.kodepos ? '':'has-error'">
                    <label>Kode Pos</label>
                    <input type="text" class="form-control" id="kodepos" v-model="profileRegister.kodepos"
                           placeholder="harus angka 5 huruf cth: 20132">
                    <span class="help-block" v-show="!validator.formRegister.kodepos">
                        Harus 5 angka huruf
                    </span>
                </div>
                <div class="form-group">
                    <label>Alamat</label>
                    <input type="text" class="form-control" id="alamat" v-model="profileRegister.alamat"
                           placeholder="masukkan alamat anda">
                </div>
                <div class="alert alert-info">
                    <p>
                        Pastikan data anda sudah lengkap dan benar
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" v-if="formRegisterValid && !submitting">
                    Create My Account
                </button>
                <button type="button" class="btn btn-primary" disabled v-if="!formRegisterValid && !submitting" v-cloak>
                    Create My Account
                </button>
                <button type="button" class="btn btn-primary" disabled v-if="submitting" v-cloak>
                    <i class="fa fa-spinner fa-spin"></i> Saving
                </button>
            </div>
        </form>
    </modal>
    @include('backend.merchant.partials.modal')
@endsection

@section('pagescripts')
    <script src="/shop-assets/js/shop-login.js"></script>
@endsection