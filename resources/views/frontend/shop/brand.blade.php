@extends('frontend.frontend')

@section('content')
    <div class="row margin-bottom-10">
        @include('frontend.partials.banner')
    </div>
    <div class="row margin-bottom-10 hidden-xs">
        <div class="col-md-3">
            <div class="form-group">
                <select class="form-control" v-model="size">
                    <option value="0" disabled selected>Size</option>
                    <option value="0">All</option>
                    <option v-for="o in optionSize" :value="o.value">
                        @{{ o.text }}
                    </option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <select class="form-control" v-model="color">
                    <option value="0" disabled selected>Color</option>
                    <option value="0">All</option>
                    <option v-for="c in colours" :value="c.value">
                        @{{ c.text }}
                    </option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <select class="form-control" v-model="price">
                    <option value="0" disabled selected>Price</option>
                    <option value="0">All</option>
                    <option value="1">< 50k</option>
                    <option value="2">50k - 100k</option>
                    <option value="3">100k - 300k</option>
                    <option value="4">300k - 600k</option>
                    <option value="5"> > 600k</option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <select class="form-control" v-model="gender">
                    <option value="all" disabled selected>Gender</option>
                    <option value="all">All</option>
                    <option value="male">Man</option>
                    <option value="female">Woman</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row margin-bottom-10">
        <div class="col-md-12">
            <div class="panel-group accordion scrollable" id="brand-products" role="tablist"
                 aria-multiselectable="true">
                <div class="panel panel-default panel-product" id="new-arrival">
                    <div class="panel-heading text-center" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#brand-products"
                               href="#new-arrival-content" aria-expanded="true" aria-controls="new-arrival-content"
                               v-on:click="setCategory(0, 'new-arrival')" class="btn-block">
                                New Arrival
                            </a>
                        </h4>
                    </div>
                    <div id="new-arrival-content" class="panel-collapse collapse in" role="tabpanel"
                         aria-labelledby="new-arrival">
                        <div class="panel-body">
                            <div class="row margin-bottom-10 sale-product" v-for="d in data">
                                <div class="col-md-2 col-xs-4" v-for="p in d">
                                    <div class="product clearfix">
                                        <div class="product-image">
                                            <a :href="'/brand/'+p.author.username+'/'+p.slug">
                                                <img :src="'/images/catalog/'+p.images[0].name">
                                            </a>
                                            <a :href="'/brand/'+p.author.username+'/'+p.slug">
                                                <img :src="'/images/catalog/'+p.images[1].name"
                                                     v-if="p.images[1].name.trim()">
                                            </a>
                                            <template v-if="isDiscounted(p.discount)">
                                                <div class="sale-flash" v-cloak>
                                                    @{{ p.discount.value }}% off
                                                </div>
                                            </template>
                                        </div>
                                        <div class="product-desc">
                                            <div class="product-brand">
                                                <h3>
                                                    <a :href="'/brand/'+p.author.username" v-cloak>
                                                        @{{ p.author.nama }}
                                                    </a>
                                                </h3>
                                            </div>
                                            <div class="product-title">
                                                <h3>
                                                    <a :href="'/brand/'+p.author.username+'/'+p.slug" v-cloak>
                                                        @{{ p.name }}
                                                    </a>
                                                </h3>
                                            </div>
                                            <div class="product-price">
                                                <template v-if="isDiscounted(p.discount)">
                                                    <del v-cloak>
                                                        @{{ currency }} @{{ p.price*1 | formatNumber currency }}
                                                    </del>
                                                    <ins v-cloak>
                                                        @{{ currency }} @{{ (100-p.discount.value)*p.price/100 | formatNumber currency }}
                                                    </ins>
                                                </template>
                                                <template v-else>
                                                    <ins v-cloak>
                                                        @{{ currency }} @{{ p.price*1 | formatNumber currency }}
                                                    </ins>
                                                </template>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="pager">
                                <li>
                                    <a v-on:click="prev('new-arrival')" href="javascript:;">
                                        Previous
                                    </a>
                                </li>
                                <li>
                                    <a v-on:click="next('new-arrival')"
                                       href="javascript:;">
                                        Next
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-product" v-for="cat in optionCategory" id="@{{ cat['text'] }}">
                    <div class="panel-heading text-center" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#brand-products"
                               href="#@{{ cat['text'] }}-content" aria-expanded="true"
                               aria-controls="@{{ cat['text'] }}-content"
                               v-on:click="setCategory(cat, cat.text)" class="btn-block" v-cloak>
                                @{{ cat['text'] }}
                            </a>
                        </h4>
                    </div>
                    <div id="@{{ cat['text'] }}-content" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="@{{ cat.text }}">
                        <div class="panel-body">
                            <div class="row margin-bottom-10 sale-product" v-for="d in data">
                                <div class="col-md-2 col-xs-4" v-for="p in d">
                                    <div class="product clearfix">
                                        <div class="product-image">
                                            <a :href="'/brand/'+p.author.username+'/'+p.slug">
                                                <img :src="'/images/catalog/'+p.images[0].name">
                                            </a>
                                            <a :href="'/brand/'+p.author.username+'/'+p.slug">
                                                <img :src="'/images/catalog/'+p.images[1].name"
                                                     v-if="p.images[1].name.trim()">
                                            </a>
                                            <template v-if="isDiscounted(p.discount)">
                                                <div class="sale-flash" v-cloak>
                                                    @{{ p.discount.value }}% off
                                                </div>
                                            </template>
                                        </div>
                                        <div class="product-desc">
                                            <div class="product-brand">
                                                <h3>
                                                    <a :href="'/brand/'+p.author.username">
                                                        @{{ p.author.nama }}
                                                    </a>
                                                </h3>
                                            </div>
                                            <div class="product-title">
                                                <h3>
                                                    <a :href="'/brand/'+p.author.username+'/'+p.slug" v-cloak>
                                                        @{{ p.name }}
                                                    </a>
                                                </h3>
                                            </div>
                                            <div class="product-price">
                                                <template v-if="isDiscounted(p.discount)">
                                                    <del v-cloak>
                                                        @{{ currency }} @{{ p.price*1 | formatNumber currency }}
                                                    </del>
                                                    <ins v-cloak>
                                                        @{{ currency }} @{{ (100-p.discount.value)*p.price/100 | formatNumber currency }}
                                                    </ins>
                                                </template>
                                                <template v-else>
                                                    <ins v-cloak>
                                                        @{{ currency }} @{{ p.price*1 | formatNumber currency }}
                                                    </ins>
                                                </template>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <ul class="pager">
                                <li>
                                    <a v-on:click="prev(cat.text+'-content')" href="javascript:;">
                                        Previous
                                    </a>
                                </li>
                                <li>
                                    <a v-on:click="next(cat.text+'-content')"
                                       href="javascript:;">
                                        Next
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('frontend.partials.anotherbrands')
@endsection

@section('pagescripts')
    <script src="/shop-assets/js/shop-brand-page.js"></script>
@endsection