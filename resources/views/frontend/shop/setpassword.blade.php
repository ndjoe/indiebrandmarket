@extends('frontend.frontend')

@section('title')
@endsection

@section('morecss')
    <link rel="stylesheet" href="/shop-assets/plugins/uniform/css/uniform.default.css">
@endsection

@section('content')
    <div class="row margin-bottom-40">
        @if(count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="col-md-4">
            <div class="well well-lg">
                <h3>Forgot Password</h3>

                <form id="forgot" action="/{{ LaravelLocalization::getCurrentLocale() }}/set-password" method="post">
                    {{ csrf_field() }}
                    <div class="form-group"
                         v-class="has-error: !validator.password.required, has-success: validator.password.required">
                        <label for="">New Password:</label>
                        <input type="password" v-model="password" class="form-control" name="password">
                        <span class="help-block" v-show="!validator.password.required">
                            Minimal 6 karakter angka dan huruf
                        </span>
                    </div>
                    <div class="form-group"
                         v-class="has-error: !validator.password_confirmation.required, has-success: validator.password_confirmation.required">
                        <label for="">Confirm Password:</label>
                        <input type="password" v-model="password_confirmation" class="form-control"
                               name="password_confirmation">
                        <span class="help-block" v-show="!validator.password_confirmation.required">
                            Harus sama denga password yang anda masukkan
                        </span>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/validator.min.js"></script>
    <script>
        var vm = new Vue({
            el: '#shop',
            data: {
                cart: [],
                password: '',
                password_confirmation: '',
                currency: obid.locale === 'id' ? 'IDR' : 'USD',
                locale: obid.locale,
                newsletter: {
                    email: ''
                }
            },
            computed: {
                validator: function () {
                    return {
                        password: {
                            required: validator.isLength(this.password, 6) && validator.isAlphanumeric(this.password)
                        },
                        password_confirmation: {
                            required: validator.equals(this.password_confirmation, this.password)
                        }
                    }
                }
            },
            methods: {
                getCart: function () {
                    var self = this;
                    $.get('/' + obid.locale + '/api/cart')
                            .done(function (resp) {
                                self.cart = resp.items;
                            });
                },
                delItem: function (data) {
                    var self = this;
                    var formData = new FormData;

                    formData.append('rowid', data['rowid']);

                    $.ajax({
                        url: '/' + obid.locale + '/api/cart/delete',
                        data: formData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: true,
                        type: 'POST'
                    }).done(function (data) {
                        self.cart = data.items;
                    });
                },
                subscribesNewsLetter: function (e) {
                    e.preventDefault();
                    var self = this;
                    var formData = new FormData;
                    formData.append('email', this.newsletter.email);

                    $.ajax({
                        url: '/' + obid.locale + '/api/newsletter',
                        data: formData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: true,
                        type: 'POST',
                        statusCode: {
                            422: function (e) {
                                toastr.error("email invalid");
                            }
                        }
                    }).done(function (data) {
                        if (data.subscribed) {
                            swal(
                                    "You have been subscribed",
                                    "Please save following code and use it for your next purchase: "
                                    + data.couponCode,
                                    "success"
                            );
                            self.newsletter.email = "";
                        } else {
                            swal(
                                    "Ooops...",
                                    "Your have been subscribed to our news letter.... sorry for that",
                                    "error"
                            );
                            self.newsletter.email = "";
                        }
                    });
                }
            },
            ready: function () {
                Layout.init();
                Layout.initTouchspin();
                Layout.initTwitter();
//                Layout.initFixHeaderWithPreHeader();
                Layout.initNavScrolling();
                this.getCart();
            }
        });
    </script>
@endsection