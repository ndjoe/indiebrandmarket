@extends('frontend.frontend')

@section('content')
    <div class="row margin-bottom-10 hidden-xs">
        <div class="col-md-3">
            <div class="form-group">
                <select class="form-control" v-model="size">
                    <option value="0" disabled selected>Size</option>
                    <option value="0">All</option>
                    <option v-for="o in optionSize" :value="o.value">
                        @{{ o.text }}
                    </option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <select class="form-control" v-model="color">
                    <option value="0" disabled selected>Color</option>
                    <option value="0">All</option>
                    <option v-for="c in colours" :value="c.value">
                        @{{ c.text }}
                    </option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <select class="form-control" v-model="price">
                    <option value="0" disabled selected>Price</option>
                    <option value="0">All</option>
                    <option value="1">< 50k</option>
                    <option value="2">50k - 100k</option>
                    <option value="3">100k - 300k</option>
                    <option value="4">300k - 600k</option>
                    <option value="5"> > 600k</option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <select class="form-control" v-model="gender">
                    <option value="all" disabled selected>Gender</option>
                    <option value="all">All</option>
                    <option value="male">Man</option>
                    <option value="female">Woman</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row margin-bottom-10">
        <div class="col-md-12">
            <div class="panel-group accordion scrollable" id="brand-products" role="tablist"
                 aria-multiselectable="true">
                <div class="panel panel-default panel-product" id="new-arrival">
                    <div class="panel-heading text-center" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#brand-products"
                               href="#new-arrival-content" aria-expanded="true" aria-controls="new-arrival-content"
                               v-on:click="setCategory(0, 'new-arrival')" class="btn-block">
                                All
                            </a>
                        </h4>
                    </div>
                    <div id="new-arrival-content" class="panel-collapse collapse in" role="tabpanel"
                         aria-labelledby="new-arrival">
                        <div class="panel-body">
                            <div class="row margin-bottom-10 sale-product" v-for="d in data">
                                <div class="col-md-2 col-xs-4" v-for="p in d">
                                    <div class="product clearfix">
                                        <div class="product-image">
                                            <a :href="'/brand/'+p.author.username+'/'+p.slug">
                                                <img :src="'/images/catalog/'+p.images[0].name">
                                            </a>
                                            <a :href="'/brand/'+p.author.username+'/'+p.slug">
                                                <img :src="'/images/catalog/'+p.images[1].name"
                                                     v-if="p.images[1].name.trim()">
                                            </a>
                                            <template v-if="isDiscounted(p.discount)">
                                                <div class="sale-flash" v-cloak>
                                                    @{{ p.discount.value }}% off
                                                </div>
                                            </template>
                                        </div>
                                        <div class="product-desc">
                                            <div class="product-brand">
                                                <h3>
                                                    <a :href="'/brand/'+p.author.username"
                                                       v-cloak>@{{ p.author.nama }}
                                                    </a>
                                                </h3>
                                            </div>
                                            <div class="product-title">
                                                <h3><a :href="'/brand/'+p.author.username+'/'+p.slug"
                                                       v-cloak>@{{ p.name }}</a>
                                                </h3>
                                            </div>
                                            <div class="product-price">
                                                <template v-if="isDiscounted(p.discount)" v-cloak>
                                                    <del>@{{ currency }} @{{ p.price*1 | formatNumber currency }}</del>
                                                    <br>
                                                    <ins>@{{ currency }} @{{ (100-p.discount.value)*p.price/100 | formatNumber currency }}</ins>
                                                </template>
                                                <template v-if="!isDiscounted(p.discount)" v-cloak>
                                                    <ins>@{{ currency }} @{{ p.price*1 | formatNumber currency }}</ins>
                                                </template>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="pager">
                                <li>
                                    <a v-on:click="prev('new-arrival')" href="javascript:;">
                                        Previous
                                    </a>
                                </li>
                                <li>
                                    <a v-on:click="next('new-arrival')"
                                       href="javascript:;">
                                        Next
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-product" v-for="cat in optionCategory" id="@{{ cat['text'] }}">
                    <div class="panel-heading text-center" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#brand-products"
                               href="#@{{ cat['text'] }}-content" aria-expanded="true"
                               aria-controls="@{{ cat['text'] }}-content"
                               v-on:click="setCategory(cat, cat.text)" class="btn-block" v-cloak>
                                @{{ cat['text'] }}
                            </a>
                        </h4>
                    </div>
                    <div id="@{{ cat['text'] }}-content" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="@{{ cat.text }}">
                        <div class="panel-body">
                            <div class="row margin-bottom-10 sale-product" v-for="d in data">
                                <div class="col-md-2 col-xs-4   " v-for="p in d">
                                    <div class="product clearfix">
                                        <div class="product-image">
                                            <a :href="'/brand/'+p.author.username+'/'+p.slug">
                                                <img :src="'/images/catalog/'+p.images[0].name">
                                            </a>
                                            <a :href="'/brand/'+p.author.username+'/'+p.slug">
                                                <img :src="'/images/catalog/'+p.images[1].name"
                                                     v-if="p.images[1].name.trim()">
                                            </a>
                                            <template v-if="isDiscounted(p.discount)">
                                                <div class="sale-flash" v-cloak>
                                                    @{{ p.discount.value }}% off
                                                </div>
                                            </template>
                                        </div>
                                        <div class="product-desc">
                                            <div class="product-brand">
                                                <h3>
                                                    <a :href="'/brand/'+p.author.username"
                                                       v-cloak>@{{ p.author.nama }}
                                                    </a>
                                                </h3>
                                            </div>
                                            <div class="product-title">
                                                <h3><a :href="'/brand/'+p.author.username+'/'+p.slug"
                                                       v-cloak>@{{ p.name }}</a>
                                                </h3>
                                            </div>
                                            <div class="product-price">
                                                <template v-if="isDiscounted(p.discount)" v-cloak>
                                                    <del>@{{ currency }} @{{ p.price*1 | formatNumber currency }}</del>
                                                    <br>
                                                    <ins>@{{ currency }} @{{ (100-p.discount.value)*p.price/100 | formatNumber currency }}</ins>
                                                </template>
                                                <template v-if="!isDiscounted(p.discount)" v-cloak>
                                                    <ins>@{{ currency }} @{{ p.price*1 | formatNumber currency }}</ins>
                                                </template>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <ul class="pager">
                                <li>
                                    <a v-on:click="prev(cat.text+'-content')" href="javascript:;">
                                        Previous
                                    </a>
                                </li>
                                <li>
                                    <a v-on:click="next(cat.text+'-content')"
                                       href="javascript:;">
                                        Next
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="/shop-assets/pages/scripts/checkout.js"></script>
    <script src="/assets/plugins/moment.min.js"></script>
    <script>
        var vm = new Vue({
            el: '#shop',
            data: {
                data: [],
                currentPage: 1,
                totalPage: 1,
                cart: [],
                optionCategory: [],
                currency: obid.locale === 'id' ? 'IDR' : 'USD',
                locale: obid.locale,
                newsletter: {
                    email: ''
                },
                size: 0,
                color: 0,
                category: 0,
                gender: 'all',
                price: 0,
                optionSize: [],
                colours: []
            },
            computed: {
                isLast: function () {
                    return this.currentPage === this.totalPage;
                },
                isFirst: function () {
                    return this.currentPage === 1;
                }
            },
            watch: {
                'size': function (nv, ov) {
                    if (nv === ov) {
                        return;
                    }
                    this.currentPage = 1;
                    this.getData();
                },
                'price': function (nv, ov) {
                    if (nv === ov) {
                        return;
                    }
                    this.currentPage = 1;
                    this.getData();
                },
                'color': function (nv, ov) {
                    if (nv === ov) {
                        return;
                    }
                    this.currentPage = 1;
                    this.getData();
                },
                'gender': function (nv, ov) {
                    if (nv === ov) {
                        return;
                    }

                    this.currentPage = 1;
                    this.getData();
                }
            },
            methods: {
                getData: function (page) {
                    if (!page) {
                        page = 1;
                    }
                    var self = this;
                    $.get('/' + obid.locale + '/api/products/sale', {
                        category: this.category,
                        size: this.size,
                        color: this.color,
                        price: this.price,
                        gender: this.gender,
                        page: page
                    }).done(function (data) {
                        self.data = data.data;
                        self.currentPage = data.current_page;
                        self.totalPage = data.last_page;
                    });
                },
                prev: function (el) {
                    if (this.currentPage === 1) {
                        return;
                    }

                    this.getData(this.currentPage - 1);
//                    Layout.scrollTo($('#' + el), 0);
                },
                next: function (el) {
                    if (this.currentPage === this.totalPage) {
                        return;
                    }

                    this.getData(this.currentPage + 1);
//                    Layout.scrollTo($('#' + el), );
                },
                getCart: function () {
                    var self = this;
                    $.get('/' + obid.locale + '/api/cart')
                            .done(function (resp) {
                                self.cart = resp.items;
                            });
                },
                delItem: function (data) {
                    var self = this;
                    var formData = new FormData;

                    formData.append('rowid', data['rowid']);

                    $.ajax({
                        url: '/' + obid.locale + '/api/cart/delete',
                        data: formData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: true,
                        type: 'POST'
                    }).done(function (data) {
                        self.cart = data.items;
                    });
                },
                getOptions: function () {
                    var self = this;
                    $.get('/' + obid.locale + '/api/options')
                            .done(function (resp) {
                                self.optionSize = resp.size;
                                self.colours = resp.colours;
                                self.optionCategory = resp.categories;
                            });
                },
                setCategory: function (id, el) {
                    this.category = !isNaN(id) ? id : id.value;
                    console.log(this.category);
                    this.currentPage = 1;
                    this.getData();
//                    Layout.scrollTo($('#' + el), -500);
                },
                isDiscounted: function (discount) {
                    return moment().isBefore(discount.expired_at) && discount.value > 0;
                },
                subscribesNewsLetter: function (e) {
                    e.preventDefault();
                    var self = this;
                    var formData = new FormData;
                    formData.append('email', this.newsletter.email);

                    $.ajax({
                        url: '/' + obid.locale + '/api/newsletter',
                        data: formData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: true,
                        type: 'POST',
                        statusCode: {
                            422: function (e) {
                                toastr.error("email invalid");
                            }
                        }
                    }).done(function (data) {
                        if (data.subscribed) {
                            swal(
                                    "You have been subscribed",
                                    "Please save following code and use it for your next purchase: "
                                    + data.couponCode,
                                    "success"
                            );
                            self.newsletter.email = "";
                        } else {
                            swal(
                                    "Ooops...",
                                    "Your have been subscribed to our news letter.... sorry for that",
                                    "error"
                            );
                            self.newsletter.email = "";
                        }
                    });
                }

            },
            ready: function () {
                Layout.init();
                this.getOptions();
                this.getCart();
                this.getData();
            },
            components: {}
        });
    </script>
@endsection