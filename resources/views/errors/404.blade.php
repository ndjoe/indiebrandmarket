<!DOCTYPE html>
<html>
<head>
    @yield('title')
    <meta name="_token" content="{{ csrf_token() }}">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet"
          type="text/css">
    <link rel="stylesheet" href="/css/error.css">
</head>
<body class="page-404-3">
<div class="page-inner">
    <img src="/assets/img/earth.jpg" class="img-responsive" alt=""></div>
<div class="container error-404">
    <h1>404</h1>

    <h2>Houston, we have a problem.</h2>

    <p> Actually, the page you are looking for does not exist. </p>

    <p>
        <a href="//originalbrands.localapp" class="btn red btn-outline"> Return home </a>
        <br></p>
</div>
<script src="/js/error.js"></script>
<script>
    $(function () {
        App.init();
        Layout.init();
    })
</script>
</body>
</html>