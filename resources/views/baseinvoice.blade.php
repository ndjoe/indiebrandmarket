<html>
<head>
    <meta charset="utf-8">
    @yield('title')
    @yield('morecss')
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/invoice.css') }}">
    <style>
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body class="page-container-bg-solid" id="app">
<div class="page-container page-container-bg-solid">
    <div class="container-fluid container-lf-space page-content">
        @yield('content')
    </div>
</div>
@include('js')
<script>
    $(function () {
        App.init();
        Layout.init();
    });
</script>
@yield('scripts')
</body>
</html>