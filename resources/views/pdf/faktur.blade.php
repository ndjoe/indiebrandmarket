<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/layout.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/invoice.css') }}">
    <style>
        table, tr, td, th, tbody, thead, tfoot {
            page-break-inside: avoid !important;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="invoice">
                <div class="row invoice-logo">
                    <div class="col-xs-6 invoice-logo-space">
                        <img src="{{ asset('assets/img/logo-original-brands.png') }}" class="img-responsive"
                             alt="" width="300px"/>
                    </div>
                    <div class="col-xs-6">
                        <p>
                            #{{ $data['orderId'] }} / {{ $data['date'] }}
                        </p>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Item
                                </th>
                                <th>
                                    Quantity
                                </th>
                                <th>
                                    Unit Cost
                                </th>
                                <th>
                                    Total
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data['items'] as $key => $entry)
                                <tr>
                                    <td>
                                        {{ $key+1 }}
                                    </td>
                                    <td>
                                        Nama: {{ $entry['nama'] }} <br>
                                        Size: {{ $entry['size'] }}
                                    </td>
                                    <td>
                                        {{ $entry['qty'] }}
                                    </td>
                                    <td>
                                        {{ $entry['price'] }}
                                    </td>
                                    <td>
                                        {{ $entry['subTotal']  }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <div class="well">
                            <address>
                                <strong>Trilogi Cipta Reswara, Inc.</strong><br/>
                                ORIGINALBRANDS.ID<br/>
                                Jln. Anyer No. 48, Bandung - Indonesia 40272,<br/>
                                <abbr title="Phone">P:</abbr> (234) 145-1810
                            </address>
                        </div>
                    </div>
                    <div class="col-xs-8 invoice-block">
                        <ul class="list-unstyled amounts">
                            <li>
                                <strong>Total amount: {{ $data['total'] }}</strong>
                            </li>
                        </ul>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>