<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/layout.css') }}">
    <link rel="stylesheet" href="{{ asset('css/invoice-2.min.css') }}">
    <style>
        table, tr, td, th, tbody, thead, tfoot {
            page-break-inside: avoid !important;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="invoice-content-2 bordered">
        <div class="row invoice-head">
            <div class="col-md-7 col-xs-6">
                <div class="invoice-logo">
                    <img src="{{ asset('assets/img/logo-original-brands.png') }}" class="img-responsive" alt="">
                </div>
            </div>
            <div class="col-md-5 col-xs-6">
                <div class="company-address">
                    <span class="bold uppercase">Originalbrands.id</span>
                    <br> 25, Lorem Lis Street, Orange C
                    <br> California, US
                    <br>
                    <span class="bold">T</span> 1800 123 456
                    <br>
                    <span class="bold">E</span> cs@originalbrands.id
                    <br>
                    <span class="bold">W</span> store.originalbrands.id
                </div>
            </div>
        </div>
        <div class="row invoice-cust-add">
            <div class="col-xs-3">
                <h2 class="invoice-title uppercase">Customer</h2>

                <p class="invoice-desc">
                    Nama: {{ $order['address']['nama'] }}<br>
                    Handphone: {{ $order['address']['nohp'] }}
                </p>
            </div>
            <div class="col-xs-3">
                <h2 class="invoice-title uppercase">Date</h2>

                <p class="invoice-desc">{{ $order['created_at'] }}</p>
            </div>
            <div class="col-xs-6">
                <h2 class="invoice-title uppercase">Address</h2>

                <p class="invoice-desc inv-address">
                    {{ $order['address']['alamat'] }}
                    , {{ $order['address']['kota'] }} {{ $order['address']['provinsi'] }}
                    {{ $order['address']['negara'] }} {{ $order['address']['kodepos'] }}
                    <br>
                </p>
            </div>
        </div>
        <div class="row invoice-body">
            <div class="col-xs-12 table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="invoice-title uppercase text-center">Nama Produk</th>
                        <th class="invoice-title uppercase text-center">Brand</th>
                        <th class="invoice-title uppercase text-center">Size</th>
                        <th class="invoice-title uppercase text-center">Qty</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($order['items'] as $i)
                        <tr>
                            <td class="text-center sbold">
                                {{ $i['product_item']['product']['name'] }}
                            </td>
                            <td class="text-center sbold">
                                {{ $i['product_item']['product']['author']['nama']}}
                            </td>
                            <td class="text-center sbold">
                                {{ \Illuminate\Support\Str::upper( $i['product_item']['size']['text']) }}
                            </td>
                            <td class="text-center sbold">{{ $i['qty'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row invoice-subtotal">
            <div class="col-xs-6">
                <h2 class="invoice-title uppercase">#Order</h2>

                <p class="invoice-desc">{{ $order['order_code'] }}</p>
            </div>
            <div class="col-xs-6">
                <h2 class="invoice-title uppercase">Total</h2>
                @if($order['currency'] === 'USD')
                    <p class="invoice-desc grand-total">
                        USD {{ number_format($order['grossTotal']/$order['currency_value'], 2, ',','.') }}
                    </p>
                @else
                    <p class="invoice-desc grand-total">
                        IDR {{ number_format($order['grossTotal'], 2, ',','.') }}
                    </p>
                @endif
            </div>
        </div>
    </div>
</div>
</body>
</html>