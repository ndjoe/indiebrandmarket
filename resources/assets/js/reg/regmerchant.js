Vue.use(window['vue-selectize']);
new Vue({
    el: '#app',
    data: {
        password: '',
        passwordConfirm: '',
        nama: '',
        owner: '',
        nohp: '',
        bank: '',
        norek: '',
        about: '',
        kota: '',
        kodepos: '',
        alamat: '',
        provinsi: '',
        ownerRek: '',
        username: '',
        email: '',
        submitted: false,
        selectizeOpts: {
            valueField: 'text'
        },
        listProvinsi: [],
        listCity: [],
        usernameCheck: true,
        nohpCheck: true,
        emailCheck: true
    },
    computed: {
        validation: function () {
            var nohpReg = /^(0+8*\d)\d+/g;
            var self = this;
            return {
                nama: validator.isLength(this.nama, 1),
                owner: validator.isLength(this.owner, 1),
                nohp: nohpReg.test(this.nohp),
                bank: validator.isLength(this.bank, 1),
                norek: validator.isNumeric(this.norek),
                kota: validator.isLength(this.kota, 1),
                kodepos: validator.isLength(this.kodepos, 1, 5) && validator.isNumeric(this.kodepos),
                alamat: validator.isLength(this.alamat, 1),
                provinsi: validator.isLength(this.provinsi, 1),
                ownerRek: validator.isLength(this.ownerRek, 1),
                password: validator.isLength(this.password, 6),
                passwordConfirm: validator.equals(this.password, this.passwordConfirm),
                username: validator.isLength(this.username, 3) && validator.isAlphanumeric(this.username),
                email: validator.isEmail(this.email),
                about: validator.isLength(this.about, 0, 600),
                checkUsername: this.usernameCheck,
                checkEmail: this.emailCheck,
                checkNohp: this.nohpCheck
            };
        },
        formValid: function () {
            var validator = this.validation;
            return Object.keys(validator).every(function (k) {
                return validator[k];
            });
        }
    },
    methods: {
        submitAccount: function (e) {
            e.preventDefault();
            var formData = new FormData;
            if ((this.$els.logo.files.length < 1) || (this.$els.banner.files.length < 1)) {
                toastr.error('banner dan logo tidak boleh kosong');
                return;
            }
            if (this.$els.logo.files.length > 0) {
                formData.append('logo', this.$els.logo.files[0]);
            }
            if (this.$els.banner.files.length > 0) {
                formData.append('banner', this.$els.banner.files[0]);
            }
            formData.append('username', this.username);
            formData.append('email', this.email);
            formData.append('nama', this.nama);
            formData.append('kota', this.kota);
            formData.append('owner', this.owner);
            formData.append('bank', this.bank);
            formData.append('provinsi', this.provinsi);
            formData.append('norek', this.norek);
            formData.append('kodepos', this.kodepos);
            formData.append('nohp', this.nohp);
            formData.append('about', this.about);
            formData.append('alamat', this.alamat);
            formData.append('ownerRek', this.ownerRek);
            formData.append('password', this.password);
            formData.append('password_confirmation', this.paswordConfirm);
            this.submitted = true;
            var self = this;
            $.ajax({
                url: '/reg',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: false,
                type: 'POST'
            }).done(function (data) {
                if (data.created) {
                    toastr.success('Akun berhasil disimpan');
                    setTimeout(function () {
                        window.location.href = data.redirect;
                    }, 1000);
                } else {
                    toastr.error(data.reason);
                    self.submitted = false;
                }
            });
        },
        getProvinces: function () {
            var self = this;
            $.get('/provinces')
                .done(function (data) {
                    self.listProvinsi = data;
                });
        },
        getCity: function (provinceId) {
            var self = this;
            $.get('/provinces/' + provinceId + '/city')
                .done(function (data) {
                    self.listCity = data;
                });
        },
        check: function (type, data, cb) {
            if (!data.trim()) {
                return true;
            }
            var ajax = $.ajax({
                url: '/check/' + type + '/' + data,
                type: 'GET'
            });

            ajax.done(function (resp) {
                cb(resp);
            });
        },
    },
    watch: {
        provinsi: function (nv, ov) {
            if (nv === ov) {
                return;
            }
            var province = $.grep(this.listProvinsi, function (e) {
                return e.text === nv;
            });
            if (province) {
                this.getCity(province[0].value.id);
            }
        },
        username: function (nv, ov) {
            var self = this;
            this.check('username', nv, function (data) {
                self.usernameCheck = !data.exists;
            });
        },
        email: function (nv, ov) {
            var self = this;
            this.check('email', nv, function (data) {
                self.emailCheck = !data.exists;
            });
        },
        nohp: function (nv, ov) {
            var self = this;
            this.check('nohp', nv, function (data) {
                self.nohpCheck = !data.exists;
            });
        }
    },
    ready: function () {
        App.init();
        Layout.init();
        this.getProvinces();
    }
});