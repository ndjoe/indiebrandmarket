Vue.filter('formatNumber', function (data, currency) {
    if (currency === 'IDR') {
        return accounting.formatNumber(data, 0, '.', ',');
    }

    return accounting.formatNumber(data, 2, '.', ',');
});
