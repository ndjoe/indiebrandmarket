Vue.use(window['vue-selectize']);
Vue.directive('datepicker', {
    twoWay: true,
    bind: function () {
        var self = this;
        $(this.el).datepicker({
            format: "yyyy-mm-dd",
            startDate: moment().format('YYYY-MM-DD'),
            todayHighlight: true
        }).on('change', function () {
            self.set(this.value);
        });
    },
    update: function (value) {
        $(this.el).val(value).trigger('change');
    }
});

Vue.filter('sumArray', function (data) {
    var qty = 0;
    data.forEach(function (d) {
        qty += d.qty * 1;
    });

    return qty;
});

Vue.filter('formatDate', function (data) {
    return moment(data).format('D MMM YYYY');
});

Vue.filter('formatMoney', function (data) {
    return accounting.formatMoney(data, 'IDR ', 0, '.', ',');
});

Vue.component('modal', {
    template: '#modal',
    props: ['show']
});
Vue.directive('numberpicker', {
    twoWay: true,
    bind: function () {
        var postfix = this.el.getAttribute('postfix');
        var max = this.el.getAttribute('max');
        var self = this;
        $(this.el)
            .TouchSpin({
                buttondown_class: 'btn blue',
                buttonup_class: 'btn blue',
                min: 0,
                max: max,
                step: 1,
                decimals: 0,
                boostat: 5,
                maxboostedstep: 10,
                postfix: postfix
            }).on('change', function () {
            self.set(this.value);
        });
    },
    update: function (value) {
        $(this.el).val(value).trigger('change');
    }
});

new Vue({
    el: '#app',
    data: {
        notification: 0,
        selected: 0,
        options: [],
        query: '',
        data: [],
        currentPage: 1,
        totalPage: 1,
        showModalItemForm: false,
        formItem: {
            productId: '',
            sizeId: '',
            qty: '',
            desc: ''
        },
        formDiscount: {
            percentDiscount: 0,
            expiredDiscount: '',
            discountId: ''
        },
        formDecrement: {
            productId: '',
            listSize: [],
            qty: '',
            sizeId: ''
        },
        showDecrementItemForm: false,
        showModalDiscountForm: false,
        showViewItemModal: false,
        detailProduct: {
            name: '',
            category: {
                text: ''
            },
            subcategory: {
                text: ''
            },
            price: 0,
            description: '',
            sizing: '',
            items: [],
            images: [],
            color: {
                text: ''
            },
            discount: {
                value: 0,
                expired_at: ''
            }
        },
        productItem: {},
        listSize: []
    },
    computed: {
        isLast: function () {
            return this.currentPage === this.totalPage;
        },
        isFirst: function () {
            return this.currentPage === 1;
        }
    },
    watch: {
        'selected': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        },
        'query': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        },
        'formItem.sizeId': function (nv, ov) {
            if (nv === ov) {
                return;
            }

            if (!isNaN(this.formItem.sizeId * 1)) {
                this.getSizeInfo(this.formItem.sizeId, this.formItem.productId);
            } else {
                this.formItem.desc = '';
            }
        }
    },
    methods: {
        getData: function (page) {
            if (!page) {
                page = 1;
            }
            var self = this;
            $.get('/api/products', {
                query: this.query,
                filter: this.selected,
                page: page
            }).done(function (data) {
                self.data = data.data;
                self.currentPage = data.current_page;
                self.totalPage = data.last_page;
            });
        },
        getCategories: function () {
            var self = this;
            $.get('/api/categories')
                .done(function (data) {
                    self.options = data;
                });
        },
        prev: function () {
            if (this.currentPage === 1) {
                return;
            }
            this.getData(this.currentPage - 1);
        },
        next: function () {
            if (this.currentPage === this.totalPage) {
                return;
            }
            this.getData(this.currentPage + 1);
        },
        showMItem: function (data) {
            console.log(this.showModalItemForm);
            this.formItem.productId = data.id;
            this.formItem.sizeId = '';
            this.formItem.qty = '';
            this.showModalItemForm = true;
            console.log(this.showModalItemForm);
        },
        showMDiscount: function (data) {
            this.formDiscount.percentDiscount = data.discount.value;
            this.formDiscount.expiredDiscount = moment(data.discount.expired_at).format('YYYY-MM-DD');
            this.formDiscount.discountId = data.discount.id;
            this.showModalDiscountForm = true;
        },
        submitDiscount: function (e) {
            e.preventDefault();
            var formData = new FormData;

            formData.append('discountId', this.formDiscount.discountId * 1);
            formData.append('expired', this.formDiscount.expiredDiscount);
            formData.append('percent', this.formDiscount.percentDiscount * 1);
            var self = this;
            $.ajax({
                url: '/api/discount',
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false
            }).done(function (data) {
                self.showModalDiscountForm = false;
                self.formDiscount.discountId = '';
                self.formDiscount.expiredDiscount = '';
                self.formDiscount.percentDiscount = 0;
                toastr.success("Diskon produk berhasil diset");
                self.getData();
            });

        },
        getSize: function () {
            var self = this;
            $.get('/api/sizes')
                .done(function (result) {
                    self.listSize = result;
                });
        },
        submitItem: function (e) {
            e.preventDefault();
            var formData = new FormData();

            formData.append('product', this.formItem.productId);
            formData.append('size', this.formItem.sizeId);
            formData.append('qty', this.formItem.qty);
            formData.append('desc', this.formItem.desc);
            var self = this;
            $.ajax({
                url: '/api/items',
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false
            }).done(function (data) {
                self.showModalItemForm = false;
                self.formItem.productId = '';
                self.formItem.sizeId = '';
                self.formItem.desc = '';
                toastr.success("Item berhasil ditambahkan");
                self.getData();
            });
        },
        deleteItem: function (data) {
            var self = this;
            bootbox.confirm('Yakin Delete Produk ' + data.name, function (result) {
                if (result) {
                    $.get('/api/product/delete/' + data.id)
                        .done(function (resp) {
                            if (resp.deleted) {
                                toastr.success("Produk Berhasil Didelete");
                                self.getData();
                            } else {
                                toastr.error("Produk Gagal Delete");
                            }
                        });
                }
            });
        },
        isDiscounted: function (data) {
            var now = moment();
            return ((data.discount.value > 0) && now.isBefore(data.discount.expired_at));
        },
        showDItemForm: function (data) {
            this.formDecrement.productId = data.id;
            this.formDecrement.listSize = data.items;
            this.formDecrement.qty = 0;
            this.formDecrement.sizeId = '';
            this.showDecrementItemForm = true;
        },
        decrementItem: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;
            formData.append('product', this.formDecrement.productId);
            formData.append('size', this.formDecrement.sizeId);
            formData.append('qty', this.formDecrement.qty);
            $.ajax({
                url: '/api/items/decrement',
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false
            }).done(function (data) {
                self.showDecrementItemForm = false;
                toastr.success("Item berhasil dikurangi");
                self.getData();
            });
        },
        publish: function (data) {
            var self = this;
            if (this.showViewItemModal) {
                this.showViewItemModal = false;
            }
            bootbox.confirm('Yakin Publish Produk ' + data.name, function (result) {
                if (result) {
                    $.get('/api/product/publish/' + data.id)
                        .done(function (resp) {
                            if (resp.published) {
                                toastr.success("Produk Berhasil Dipublish");
                                self.getData();
                            } else {
                                toastr.error("Produk Gagal Dipublish");
                            }
                        });
                }
            });
        },
        unpublish: function (data) {
            var self = this;
            if (this.showViewItemModal) {
                this.showViewItemModal = false;
            }
            bootbox.confirm('Yakin Tarik Produk ' + data.name, function (result) {
                if (result) {
                    $.get('/api/product/unpublish/' + data.id)
                        .done(function (resp) {
                            if (resp.unpublished) {
                                toastr.success("Produk Berhasil Ditarik");
                                self.getData();
                            } else {
                                toastr.error("Produk Gagal Ditarik");
                            }
                        });
                }
            });
        },
        viewItem: function (data) {
            var self = this;
            $.get('/api/product/detail/' + data.id)
                .done(function (data) {
                    self.detailProduct = data;
                    self.showViewItemModal = true;
                });
        },
        getSizeInfo: function (sizeId, productId) {
            var self = this;
            var formData = new FormData;
            formData.append('productId', productId * 1);
            formData.append('sizeId', sizeId * 1);
            $.ajax({
                url: '/api/items/detail',
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false
            }).done(function (data) {
                self.formItem.desc = data.sizing_info;
            });
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        App.init();
        Layout.init();
        this.getCategories();
        this.getSize();
        this.getData();
    },
    components: {}
});