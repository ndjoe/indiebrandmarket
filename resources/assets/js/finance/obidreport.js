Vue.filter('length', function (data) {
    return data.length;
});
Vue.filter('komisiMerchant', function (data) {
    return data['adjusment'] * 0.9;
});
Vue.directive('datepicker', {
    twoWay: true,
    bind: function () {
        var self = this;
        $(this.el).datepicker({
            format: "yyyy-mm-dd",
            todayHighlight: true
        }).on('change', function () {
            self.set(this.value);
        });
    },
    update: function (value) {
        $(this.el).val(value).trigger('change');
    }
});
Vue.filter('formatMoney', function (data) {
    return accounting.formatMoney(data, 'IDR ', 0, '.', ',');
});
var today = new Date;
new Vue({
    el: '#app',
    data: {
        notification: 0,
        profitMerchant: obid.listMerchantProfit,
        dataKupon: obid.listUsedCoupon,
        dataOrder: obid.listMerchantOrderCount,
        dataShipping: obid.listShipping,
        options: 'month',
        date: moment().format('YYYY-MM-DD'),
        monthName: moment().format('MMMM YYYY'),
        graph: null
    },
    computed: {
        totalProfitMerchant: function () {
            var total = 0;
            this.profitMerchant.forEach(function (p) {
                total = total + p.profit * 1;
            });

            return total;
        },
        totalKuponSum: function () {
            var total = 0;
            this.dataKupon.forEach(function (d) {
                total = total + d.value * 1;
            });

            return total;
        },
        totalOrderSum: function () {
            var total = 0;
            this.dataOrder.forEach(function (o) {
                total = total + o.count * 5000;
            });

            return total;
        },
        totalShipping: function () {
            return this.dataShipping.length * 2000;
        },
        pemasukan: function () {
            return this.totalProfitMerchant * 1 + this.totalOrderSum * 1 + this.totalShipping * 1;
        },
        pengeluaran: function () {
            return this.totalKuponSum * 1;
        },
        grandTotal: function () {
            return this.pemasukan * 1 - this.pengeluaran * 1;
        }
    },
    watch: {},
    methods: {
        getData: function () {
            if (this.merchant === 0) {
                toastr.error('tolong pilih merchant terlebih dahulu');
                return;
            }
            var self = this;
            $.get('/api/report/financial', {
                type: this.options,
                date: this.date
            }).done(function (data) {
                self.profitMerchant = data.listMerchantProfit;
                self.dataKupon = data.listUsedCoupon;
                self.dataShipping = data.listShipping;
                self.dataOrder = data.listMerchantOrderCount;
                self.graph.setData(self.formatChartMerchant(data.listMerchantProfit, data.listMerchantOrderCount));
                self.monthName = moment(self.date).format('MMMM YYYY');
            });
        },
        setToday: function () {
            this.date = (new Date()).toISOString().slice(0, 10);
        },
        getMerchants: function () {
            var self = this;
            $.get('/api/users/merchants/select')
                .done(function (data) {
                    self.merchants = data;
                });
        },
        formatChartMerchant: function (listProfit, listOrder) {
            var combined = [];
            var findUsername = function (username, arr) {
                return arr.filter(function (o) {
                    return o.username == username;
                });
            }

            listProfit.forEach(function (l) {
                var secondData = findUsername(l.username, listOrder)[0];

                combined.push({
                    username: l.username,
                    value: l.profit * 1 + secondData.count * 5000
                });
            });

            return combined;
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        var self = this;
        App.init();
        Layout.init();
        this.graph = Morris.Bar({
            element: 'chart-merchant',
            data: self.formatChartMerchant(self.profitMerchant, self.dataOrder),
            xkey: 'username',
            ykeys: ['value'],
            labels: ['Total Kontribusi'],
            parseTime: false,
            xLableAngle: 90
        });
    },
    components: {
        'order-stat': {
            template: '#dashboardstat',
            props: {
                color: String,
                icon: String,
                value: String,
                title: String,
                link: String
            }
        }
    }
});

