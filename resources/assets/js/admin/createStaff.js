var socket = io(window.location.hostname + ':6001');
var vm = new Vue({
    el: '#app',
    data: {
        notification: 0,
        password: '',
        passwordConfirm: '',
        nama: '',
        nohp: '',
        username: '',
        email: '',
        role: ''
    },
    computed: {
        validation: function () {
            var nohpReg = /^(0+8*\d)\d+/g;
            return {
                nama: validator.isLength(this.nama, 1),
                nohp: nohpReg.test(this.nohp),
                password: (this.password.trim()) ? validator.isLength(this.password, 6) : true,
                passwordConfirm: validator.equals(this.password, this.passwordConfirm),
                username: validator.isAlphanumeric(this.username) && validator.isLength(this.username, 3),
                email: validator.isEmail(this.email),
                role: validator.isLength(this.role, 1)
            };
        },
        formValid: function () {
            var validator = this.validation;
            return Object.keys(validator).every(function (k) {
                return validator[k];
            });
        }
    },
    methods: {
        submitAdmin: function (e) {
            e.preventDefault();
            if (!this.formValid) {
                toastr.error('please check your inputs');
                return;
            }

            var formData = new FormData;

            formData.append('password', this.password);
            formData.append('password_confirmation', this.passwordConfirm);
            formData.append('nama', this.nama);
            formData.append('nohp', this.nohp);
            formData.append('role', this.role);
            formData.append('email', this.email);
            formData.append('username', this.username);

            $.ajax({
                url: '/api/users/staffs/',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                aync: false,
                type: 'POST'
            }).done(function (data) {
                if (data.created) {
                    toastr.success("Profile created");
                    setTimeout(function () {
                        window.location.href = data.redirect;
                    }, 1000);
                } else {
                    toastr.error("Failed to create profile");
                }
            });
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        App.init();
        Layout.init();
        var self = this;
        socket.on('orders:created', function (message) {
            self.notification += 1;
        });
        this.getNotif();
    }
});