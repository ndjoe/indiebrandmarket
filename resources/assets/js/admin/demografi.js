var graphAge = Morris.Bar({
    element: 'brand-chart-age',
    data: [],
    xkey: 'label',
    ykeys: ['value'],
    labels: ['Banyak']
});
var graphGender = Morris.Bar({
    element: 'brand-chart-gender',
    data: [],
    xkey: 'label',
    ykeys: ['value'],
    labels: ['Banyak']
});
var socket = io(window.location.hostname + ':6001');
new Vue({
    el: '#app',
    data: {
        notification: 0,
        listmerchant: [],
        merchant: 0
    },
    methods: {
        getMerchants: function () {
            var self = this;
            $.get('/api/users/merchants/select')
                .done(function (resp) {
                    self.listmerchant = resp;
                });
        },
        getBrandStat: function () {
            $.get('/api/demografi/brand/' + this.merchant)
                .done(function (resp) {
                    graphAge.setData(resp.age);
                    graphGender.setData(resp.gender);
                });
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        App.init();
        Layout.init();
        this.getMerchants();
        var self = this;
        socket.on('orders:created', function (message) {
            self.notification += 1;
        });
        this.getNotif();
        $.get('/api/demografi/users')
            .done(function (resp) {
                var donutNewUserGender = Morris.Donut({
                    element: 'chart-new-user-gender',
                    data: resp.newGender
                });
                var donutTotalUserAge = Morris.Donut({
                    element: 'chart-total-user-age',
                    data: resp.totalAge
                });
                var donutTotalUserGender = Morris.Donut({
                    element: 'chart-total-user-gender',
                    data: resp.totalGender
                });
                var donutNewUserAge = Morris.Donut({
                    element: 'chart-new-user-age',
                    data: resp.newAge
                });
            });
    }
});