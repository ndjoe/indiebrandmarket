var socket = io(window.location.hostname + ':6001');
new Vue({
    el: '#app',
    data: {
        notification: 0,
        query: '',
        data: [],
        currentPage: 1,
        totalPage: 1
    },
    computed: {
        isLast: function () {
            return this.currentPage === this.totalPage;
        },
        isFirst: function () {
            return this.currentPage === 1;
        }
    },
    watch: {
        'selected': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        },
        'query': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        }
    },
    methods: {
        getData: function (page) {
            if (!page) {
                page = 1;
            }
            var self = this;
            $.get('/api/users', {
                query: this.query,
                filter: 'customer',
                page: page
            }).done(function (data) {
                self.data = data.data;
                self.currentPage = data.current_page;
                self.totalPage = data.last_page;
            });
        },
        prev: function () {
            if (this.currentPage === 1) {
                return;
            }
            this.getData(this.currentPage - 1);
        },
        next: function () {
            if (this.currentPage === this.totalPage) {
                return;
            }
            this.getData(this.currentPage + 1);
        },
        activate: function (data) {
            var self = this;
            bootbox.confirm('Are you sure to active ' + data.username + '?', function (result) {
                if (result) {
                    $.get('/api/user/' + data.id + '/activate')
                        .done(function () {
                            toastr.success('customer ' + data.username + ' activated');
                            self.getData();
                        });
                }
            });
        },
        deactivate: function (data) {
            var self = this;
            bootbox.confirm('Are you sure to deactivate ' + data.username + '?', function (result) {
                if (result) {
                    $.get('/api/user/' + data.id + '/deactivate')
                        .done(function () {
                            toastr.success('customer ' + data.username + ' deactivated');
                            self.getData();
                        });
                }
            });
        },
        deleteAccount: function (data) {
            var self = this;
            bootbox.confirm('Are you sure to delete ' + data.username + '?', function (result) {
                if (result) {
                    $.get('/api/user/' + data.id + '/delete')
                        .done(function () {
                            toastr.success('customer ' + data.username + ' deleted');
                            self.getData();
                        });
                }
            });
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        App.init();
        Layout.init();
        this.getData();
        var self = this;
        socket.on('orders:created', function (message) {
            self.notification += 1;
        });
        this.getNotif();
    }
});