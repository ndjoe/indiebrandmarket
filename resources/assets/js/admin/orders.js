Vue.component('modal', {
    template: '#modal',
    props: ['show']
});

Vue.filter('length', function (data) {
    return data.length;
});
var socket = io(window.location.hostname + ':6001');
var vm = new Vue({
    el: '#app',
    data: {
        notification: 0,
        data: [],
        options: [],
        selected: 0,
        currentPage: 1,
        totalPage: 1,
        query: '',
        newData: [],
        formItem: {
            orderId: '',
            bank: 'mandiri'
        },
        showModalPaymentForm: false
    },
    computed: {
        isLast: function () {
            return this.currentPage === this.totalPage;
        },
        isFirst: function () {
            return this.currentPage === 1;
        }
    },
    watch: {
        'selected': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        },
        'query': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        }
    },
    methods: {
        getData: function (page) {
            if (!page) {
                page = 1;
            }

            var self = this;
            $.get('/api/orders', {
                query: this.query,
                filter: this.selected,
                page: page
            }).done(function (data) {
                self.data = data.data;
                self.currentPage = data.current_page;
                self.totalPage = data.last_page;
            });
        },
        prev: function () {
            if (this.currentPage === 1) {
                return;
            }
            this.getData(this.currentPage - 1);
        },
        next: function () {
            if (this.currentPage === this.totalPage) {
                return;
            }
            this.getData(this.currentPage + 1);
        },
        getFilter: function () {
            var self = this;
            $.get('/api/order/statuses')
                .done(function (value) {
                    self.options = value;
                });
        },
        showMItem: function (data) {
            var self = this;
            var accepted = data.items.every(function (value) {
                return value.accepted_at;
            });
            console.log(accepted);
            if (accepted) {
                this.formItem.orderId = data['id'];
                this.formItem.bank = data.payment_method === 'tbca' ? 'bca' : 'mandiri';
                this.formItem.norek = '';
                this.showModalPaymentForm = true;
            } else {
                bootbox.confirm('Ada barang yang belum diaccept affiliate, apakah anda yakin tetap ingin confirm order ini?',
                    function (result) {
                        if (result) {
                            self.formItem.orderId = data['id'];
                            self.formItem.bank = data.payment_method === 'tbca' ? 'bca' : 'mandiri';
                            self.formItem.norek = '';
                            self.showModalPaymentForm = true;
                        }
                    });
            }
        },
        submitPayment: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData();

            formData.append('orderId', this.formItem.orderId);
            formData.append('bank', this.formItem.bank);
            $.ajax({
                url: '/api/orders/confirmpayment',
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false
            }).done(function (data) {
                if (data.created) {
                    toastr.success("Order Berhasil dikonfirm");
                    self.showModalPaymentForm = false;
                    self.getData();
                    self.getNotif();
                } else {
                    toastr.error("Order gagal dikonfirm");
                }
            });
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        App.init();
        Layout.init();
        this.getFilter();
        this.getData();
        var self = this;
        socket.on('orders:created', function (message) {
            self.notification += 1;
        });
        this.getNotif();
    }
});