Vue.component('modal', {
    template: '#modal',
    props: ['show']
});
var socket = io(window.location.hostname + ':6001');
var vm = new Vue({
    el: '#app',
    data: {
        notification: 0,
        query: '',
        data: [],
        currentPage: 1,
        totalPage: 1,
        currency: 0,
        showModalCashForm: false,
        formCash: {
            now: 0,
            new: 0
        }
    },
    computed: {},
    watch: {},
    methods: {
        getCurrency: function () {
            var self = this;
            $.get('/api/currency')
                .done(function (data) {
                    self.currency = data.value;
                });
        },
        showModal: function () {
            this.formCash.now = this.currency;
            this.formCash.new = 0;
            this.showModalCashForm = true;
        },
        submitCash: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;

            formData.append('new', this.formCash.new * 1);

            $.ajax({
                url: '/api/currency',
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false
            }).done(function (data) {
                toastr.success("rate dollar berhasil diupdate");
                self.showModalCashForm = false;
                self.formCash.now = 0;
                self.formCash.new = 0;
                self.currency = data.newValue;
            });
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        var self = this;
        App.init();
        Layout.init();
        socket.on('orders:created', function (message) {
            self.notification += 1;
        });
        this.getNotif();
    },
    components: {}
});
