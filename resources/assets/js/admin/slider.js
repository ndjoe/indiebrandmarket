function readImageURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#preview').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

Vue.filter('length', function (data) {
    return data.length;
});
Vue.component('modal', {
    template: '#modal',
    props: ['show']
});
var socket = io(window.location.hostname + ':6001');
var vm = new Vue({
    el: '#app',
    data: {
        notification: 0,
        data: [],
        formItem: {
            slideId: '',
            image: ''
        },
        showChangeImageForm: false
    },
    computed: {},
    watch: {},
    methods: {
        getData: function () {
            var self = this;
            $.get('/api/sliders').done(function (data) {
                self.data = data;
            });
        },
        showMItem: function (data) {
            this.formItem.slideId = data['id'];
            this.formItem.image = data['image'];
            this.$els.fileinput.value = '';
            this.showChangeImageForm = true;
        },
        submitImage: function (e) {
            e.preventDefault();
            var formData = new FormData();

            formData.append('slideId', this.formItem.slideId);
            if (this.$els.fileinput.files.length === 0) {
                toastr.error('plis pilih banner buat submit');
                return;
            }
            formData.append('image', this.$els.fileinput.files[0]);
            var self = this;
            $.ajax({
                url: '/api/sliders',
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false
            }).done(function () {
                self.showChangeImageForm = false;
                self.formItem.slideId = '';
                self.formItem.image = '';
                self.getData();
            });
        },
        clearImage: function (data) {
            var self = this;
            $.get('/api/sliders/' + data.id + '/clear')
                .done(function (res) {
                    if (res.cleared) {
                        self.getData();
                    }
                });
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        App.init();
        Layout.init();
        this.getData();
        $('#image').change(function () {
            readImageURL(this);
        });
        var self = this;
        socket.on('orders:created', function (message) {
            self.notification += 1;
        });
        this.getNotif();
    },
    components: {}
});