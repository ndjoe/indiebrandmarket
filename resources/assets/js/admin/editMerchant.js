function previewBanner(input, data) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            data.banner = e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }
}
var socket = io(window.location.hostname + ':6001');
function previewLogo(input, data) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            data.logo = e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }
}

new Vue({
    el: '#app',
    data: {
        notification: 0,
        password: '',
        passwordConfirm: '',
        nama: obid.user.nama,
        owner: obid.user.profileable.owner_name,
        nohp: obid.user.nohp,
        bank: obid.user.profileable.bank,
        norek: obid.user.profileable.norek,
        about: obid.user.profileable.about,
        kota: obid.user.profileable.kota,
        kodepos: obid.user.profileable.kodepos,
        alamat: obid.user.profileable.alamat,
        provinsi: obid.user.profileable.provinsi,
        ownerRek: obid.user.profileable.bank_owner,
        logo: (obid.user.profileable.logo.trim()) ? '/images/logo/' + obid.user.profileable.logo : '',
        banner: (obid.user.profileable.banner_image.trim()) ? '/images/banner/' + obid.user.profileable.banner_image : '',
        username: obid.user.username,
        email: obid.user.email
    },
    computed: {
        validation: function () {
            var nohpReg = /^(0+8*\d)\d+/g;
            return {
                nama: validator.isLength(this.nama, 1),
                owner: validator.isLength(this.owner, 1),
                nohp: nohpReg.test(this.nohp),
                bank: validator.isLength(this.bank, 1),
                norek: validator.isNumeric(this.norek),
                kota: validator.isLength(this.kota, 1),
                kodepos: validator.isLength(this.kodepos, 1, 5) && validator.isNumeric(this.kodepos),
                alamat: validator.isLength(this.alamat, 1),
                provinsi: validator.isLength(this.provinsi, 1),
                ownerRek: validator.isLength(this.ownerRek, 1),
                password: (this.password.trim()) ? validator.isLength(this.password, 6) : true,
                passwordConfirm: validator.equals(this.password, this.passwordConfirm),
                about: validator.isLength(this.about, 0, 600)
            };
        },
        formValid: function () {
            var validator = this.validation;
            return Object.keys(validator).every(function (k) {
                return validator[k];
            });
        }
    },
    methods: {
        submitAccount: function (e) {
            e.preventDefault();
            var formData = new FormData;
            if (this.$els.logo.files.length > 0) {
                formData.append('logo', this.$els.logo.files[0]);
            }
            if (this.$els.banner.files.length > 0) {
                formData.append('banner', this.$els.banner.files[0]);
            }
            formData.append('nama', this.nama);
            formData.append('kota', this.kota);
            formData.append('owner', this.owner);
            formData.append('bank', this.bank);
            formData.append('provinsi', this.provinsi);
            formData.append('norek', this.norek);
            formData.append('kodepos', this.kodepos);
            formData.append('nohp', this.nohp);
            formData.append('about', this.about);
            formData.append('alamat', this.alamat);
            formData.append('ownerRek', this.ownerRek);
            if (this.password.trim()) {
                formData.append('password', this.password);
                formData.append('password_confirmation', this.paswordConfirm);
            }
            $.ajax({
                url: '/api/users/merchants/' + obid.user.id + '/edit',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: false,
                type: 'POST'
            }).done(function (data) {
                if (data.updated) {
                    toastr.success('Profile updated');
                    setTimeout(function () {
                        window.location.href = data.redirect;
                    }, 1000);
                } else {
                    toastr.error(data.reason);
                }
            });
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        var self = this;
        App.init();
        Layout.init();
        $('#bannerimage').change(function () {
            previewBanner(this, self);
        });
        $('#logoimage').change(function () {
            previewLogo(this, self);
        });
        socket.on('orders:created', function (message) {
            self.notification += 1;
        });
        this.getNotif();
    }
});