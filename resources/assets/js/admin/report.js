Vue.filter('length', function (data) {
    return data.length;
});
Vue.filter('komisiMerchant', function (data) {
    return data['adjusment'] * 0.9;
});
Vue.filter('formatMoney', function (data) {
    return accounting.formatMoney(data, 'IDR ', 0, '.', ',');
});
Vue.directive('datepicker', {
    twoWay: true,
    bind: function () {
        var self = this;
        $(this.el).datepicker({
            format: "yyyy-mm-dd",
            todayHighlight: true
        }).on('change', function () {
            self.set(this.value);
        });
    },
    update: function (value) {
        $(this.el).val(value).trigger('change');
    }
});
var today = new Date;
var socket = io(window.location.hostname + ':6001');
new Vue({
    el: '#app',
    data: {
        notification: 0,
        data: obid.itemList,
        dataKupon: obid.couponList,
        dataOrder: obid.orderList,
        options: 'month',
        date: obid.tgl,
        monthName: moment().format('MMMM YYYY'),
        merchant: obid.user,
        merchants: [],
        graph: null
    },
    computed: {
        pendapatan: function () {
            var total = 0;
            this.data.forEach(function (d) {
                total += d.adjusment * 1;
            });

            return total * 0.9;
        },
        totalKuponSum: function () {
            var total = 0;
            this.dataKupon.forEach(function (d) {
                total += d.value;
            });

            return total;
        },
        totalAdminSum: function () {
            return this.dataOrder.length * 5000;
        }
    },
    watch: {},
    methods: {
        getData: function () {
            if (this.merchant === 0) {
                toastr.error('tolong pilih merchant terlebih dahulu');
                return;
            }
            var self = this;
            $.get('/api/report/sales', {
                type: this.options,
                date: this.date,
                user: this.merchant
            }).done(function (data) {
                self.graph.setData(data.chartSales);
                self.data = data.itemList;
                self.dataKupon = data.couponList;
                self.dataOrder = data.orderList;
                self.monthName = moment(self.date).format('MMMM YYYY');
            });
        },
        setToday: function () {
            this.date = (new Date()).toISOString().slice(0, 10);
        },
        getMerchants: function () {
            var self = this;
            $.get('/api/users/merchants/select')
                .done(function (data) {
                    self.merchants = data;
                });
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        var self = this;
        App.init();
        Layout.init();
        this.getMerchants();
        this.graph = Morris.Line({
            element: 'chart',
            data: obid.chartSales,
            xkey: 'time',
            ykeys: ['value'],
            labels: ['Total Penjualan'],
            parseTime: false,
            xLableAngle: 90
        });
        socket.on('orders:created', function (message) {
            self.notification += 1;
        });
        this.getNotif();
    },
    components: {
        'order-stat': {
            template: '#dashboardstat',
            props: {
                color: String,
                icon: String,
                value: String,
                title: String,
                link: String
            }
        }
    }
});