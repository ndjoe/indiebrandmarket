var socket = io(window.location.hostname + ':6001');
var vm = new Vue({
    el: '#app',
    data: {
        notification: 0,
        password: '',
        passwordConfirm: '',
        nama: obid.cashier.user.nama,
        nohp: obid.cashier.user.nohp,
        username: obid.cashier.user.username,
        email: obid.cashier.user.email,
        merchant: obid.cashier.merchant.username
    },
    computed: {
        validation: function () {
            var nohpReg = /^(0+8*\d)\d+/g;
            return {
                nama: validator.isLength(this.nama, 1),
                nohp: nohpReg.test(this.nohp),
                password: (this.password.trim()) ? validator.isLength(this.password, 6) : true,
                passwordConfirm: validator.equals(this.password, this.passwordConfirm)
            };
        },
        formValid: function () {
            var validator = this.validation;
            return Object.keys(validator).every(function (k) {
                return validator[k];
            });
        }
    },
    methods: {
        submitAdmin: function (e) {
            e.preventDefault();
            var formData = new FormData;
            if (!validator.isNull(this.password)) {
                formData.append('password', this.password);
                formData.append('password_confirmation', this.passwordConfirm);
            }
            formData.append('nama', this.nama);
            formData.append('nohp', this.nohp);

            $.ajax({
                url: '/api/users/cashiers/' + obid.cashier.user.id + '/edit',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                aync: false,
                type: 'POST'
            }).done(function (data) {
                if (data.updated) {
                    toastr.success("profile updated");
                    setTimeout(function () {
                        window.location.href = data.redirect;
                    }, 1000);
                } else {
                    toastr.error("failed to update profile");
                }
            });
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        App.init();
        Layout.init();
        var self = this;
        socket.on('orders:created', function (message) {
            self.notification += 1;
        });
        this.getNotif();
    }
});