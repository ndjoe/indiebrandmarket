Vue.component('modal', {
    template: '#modal',
    props: ['show']
});
var socket = io(window.location.hostname + ':6001');
Vue.directive('datepicker', {
    twoWay: true,
    bind: function () {
        var self = this;
        $(this.el).datepicker({
            format: "yyyy-mm-dd",
            startDate: moment().format('YYYY-MM-DD'),
            todayHighlight: true
        }).on('change', function () {
            self.set(this.value);
        });
    },
    update: function (value) {
        $(this.el).val(value).trigger('change');
    }
});

var vm = new Vue({
    el: '#app',
    data: {
        notification: 0,
        selected: 0,
        options: [],
        query: '',
        data: [],
        currentPage: 1,
        totalPage: 1,
        showModalAddForm: false,
        formAdd: {
            kode: '',
            expired: moment().format("YYYY-MM-DD"),
        }
    },
    computed: {
        isLast: function () {
            return this.currentPage === this.totalPage;
        },
        isFirst: function () {
            return this.currentPage === 1;
        }
    },
    watch: {
        'selected': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        },
        'query': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        }
    },
    methods: {
        getData: function (page) {
            if (!page) {
                page = 1;
            }
            var self = this;
            $.get('/api/token', {
                page: page
            }).done(function (data) {
                self.data = data.data;
                self.currentPage = data.current_page;
                self.totalPage = data.last_page;
            });
        },
        showModalAdd: function () {
            this.showModalAddForm = true;
        },
        getCategories: function () {
            var self = this;
            $.get('/api/categories')
                .done(function (data) {
                    self.options = data;
                });
        },
        prev: function () {
            if (this.currentPage === 1) {
                return;
            }
            this.getData(this.currentPage - 1);
        },
        next: function () {
            if (this.currentPage === this.totalPage) {
                return;
            }
            this.getData(this.currentPage + 1);
        },
        showMItem: function (data) {
            console.log(this.showModalItemForm);
            this.formItem.productId = data.id;
            this.formItem.sizeId = '';
            this.formItem.qty = '';
            this.showModalItemForm = true;
            console.log(this.showModalItemForm);
        },
        getSize: function () {
            var self = this;
            $.get('/api/sizes')
                .done(function (result) {
                    self.listSize = result;
                });
        },
        submitAddToken: function (e) {
            e.preventDefault();
            var formData = new FormData();
            formData.append('token', this.formAdd.kode);
            formData.append('expired', this.formAdd.expired);
            var self = this;
            $.ajax({
                url: '/api/token',
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false
            }).done(function () {
                self.showModalAddForm = false;
                self.formAdd.kode = '';
                self.formAdd.expired = moment().format('YYYY-MM-DD');
                self.getData();
            });
        },
        getMerchants: function () {
            var self = this;
            $.get('/api/users/merchants/select')
                .done(function (resp) {
                    self.listmerchant = resp;
                });
        },
        isValid: function (data) {
            return moment().isBefore(moment(data.expired_at).format("YYYY-MM-DD")) && (data.qty > 0);
        },
        deleteToken: function (coupon) {
            var self = this;
            bootbox.confirm('Are you sure to delete a token ?', function (result) {
                if (result) {
                    $.get('/api/token/delete/' + coupon.id)
                        .done(function () {
                            toastr.success('Token berhasil didelete');
                            self.getData();
                        });
                }
            });
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        App.init();
        Layout.init();
        this.getData();
        var self = this;
        socket.on('orders:created', function (message) {
            self.notification += 1;
        });
        this.getNotif();
    },
    components: {}
});
