$(document).ajaxError(function (e) {
    if (e.responseJSON.redirect) {
        window.location.href = e.responseJSON.redirect;
    }
});
