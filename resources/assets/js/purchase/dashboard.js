var socket = io(window.location.hostname + ':6001');
var orderCountChart = Morris.Area({
    element: 'chartOrderCount',
    padding: 10,
    fillOpacity: 1,
    data: obid.todayOrderCountChart,
    xkey: 'time',
    ykeys: ['value'],
    labels: ['Jumlah'],
    parseTime: false,
    lineColors: ['#399a8c', '#92e9dc'],
    hideHover: 'auto',
    resize: true
});
new Vue({
    el: '#app',
    data: {
        notification: 0,
        totalOrderCount: obid.totalOrderCount,
        todayOrderCount: obid.todayOrderCount,
        optionOrderCount: 'day',
        optionOrderSum: 'day',
        confirmedOrderCount: obid.confirmedOrderCount,
        unconfirmedOrderCount: obid.unconfirmedOrderCount
    },
    watch: {
        'optionOrderCount': function (nv, ov) {
            if (nv == 'month') {
                orderCountChart.setData(obid.totalOrderCountChart);
            } else {
                orderCountChart.setData(obid.todayOrderCountChart);
            }
        },
        'optionOrderSum': function (nv, ov) {
            if (nv == 'month') {
                orderSumChart.setData(obid.totalOrderSumChart);
            } else {
                orderSumChart.setData(obid.todayOrderSumChart);
            }
        }
    },
    methods: {
        setOptionCount: function (data) {
            this.optionOrderCount = data;
        },
        setOptionSum: function (data) {
            this.optionOrderSum = data;
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    components: {
        'order-stat': {
            template: '#dashboardstat',
            props: {
                color: String,
                icon: String,
                value: Number,
                title: String
            }
        }
    },
    ready: function () {
        var self = this;
        socket.on('orders:created', function (message) {
            self.notification += 1;
        });
        this.getNotif();
        App.init();
        Layout.init();
    }
});
