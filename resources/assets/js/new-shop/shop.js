import Vue from 'vue';
import VueResource from 'vue-resource';
import Cart from './components/cart.vue';
import BlockFloor from './components/block-floor.vue';
import LazyLoad from 'vue-lazyload';

Vue.use(VueResource);
Vue.use(LazyLoad);

new Vue({
    el: 'body',
    data: {
        cart: []
    },
    components: {
        Cart,
        BlockFloor
    },
    methods: {
        delItem: function (item) {
            this.$http.delete('/cart/', item['rowId'])
                .then((res) => {
                    this.cart.splice(this.cart.indexOf(item), 1);
                });
        }
    }
});