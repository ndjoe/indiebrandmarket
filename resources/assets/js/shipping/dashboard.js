new Vue({
    el: '#app',
    data: {
        notification: 0,
        tobeshipped: obid.tobeshipped,
        totalShipping: obid.totalShipping
    },
    watch: {
        'optionOrderCount': function (nv, ov) {
            if (nv == 'month') {
                orderCountChart.setData(obid.totalOrderCountChart);
            } else {
                orderCountChart.setData(obid.todayOrderCountChart);
            }
        },
        'optionOrderSum': function (nv, ov) {
            if (nv == 'month') {
                orderSumChart.setData(obid.totalOrderSumChart);
            } else {
                orderSumChart.setData(obid.todayOrderSumChart);
            }
        }
    },
    methods: {
        setOptionCount: function (data) {
            this.optionOrderCount = data;
        },
        setOptionSum: function (data) {
            this.optionOrderSum = data;
        },
        getNotif: function () {
            var self = this;
            $.get('/user/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    components: {
        'order-stat': {
            template: '#dashboardstat',
            props: {
                color: String,
                icon: String,
                value: Number,
                title: String
            }
        }
    },
    ready: function () {
        var self = this;
        App.init();
        Layout.init();
    }
});


