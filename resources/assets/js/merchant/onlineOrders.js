Vue.filter('length', function (data) {
    return data.length;
});
Vue.filter('komisiIbm', function (data) {
    return data.adjusment * 0.1;
});
Vue.filter('komisiMerchant', function (data) {
    return data.adjusment * 0.9;
});
var socket = io(window.location.hostname + ':6001');

var vm = new Vue({
    el: '#app',
    data: {
        notification: 0,
        data: [],
        options: [],
        selected: 0,
        currentPage: 1,
        totalPage: 1,
    },
    computed: {
        isLast: function () {
            return this.currentPage === this.totalPage;
        },
        isFirst: function () {
            return this.currentPage === 1;
        }
    },
    watch: {
        'selected': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        },
        'query': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        }
    },
    methods: {
        getData: function (page) {
            if (!page) {
                page = 1;
            }

            var self = this;
            $.get('/api/orders', {
                query: this.query,
                filter: this.selected,
                page: page
            }).done(function (data) {
                self.data = data.data;
                self.currentPage = data.current_page;
                self.totalPage = data.last_page;
            });
        },
        prev: function () {
            if (this.currentPage === 1) {
                return;
            }
            this.getData(this.currentPage - 1);
        },
        next: function () {
            if (this.currentPage === this.totalPage) {
                return;
            }
            this.getData(this.currentPage + 1);
        },
        acceptItem: function (data) {
            var self = this;
            bootbox.confirm('Accpet order item # ' + data.id, function (result) {
                if (result) {
                    $.get('/api/orders/accept/' + data.id)
                        .done(function (resp) {
                            if (resp.accepted) {
                                toastr.success("Order Item Accepted");
                                self.getData();
                                self.getNotif();
                            } else {
                                toastr.error("Failed to accept order item");
                            }
                        });
                }
            });
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        App.init();
        Layout.init();
        var self = this;
        this.getData();
        this.getNotif();
        socket.on('merchant.' + obid.authId + ':orders.confirmed', function (message) {
            self.notification += 1;
            self.getData();
        });
    },
    components: {}
});