var socket = io(window.location.hostname + ':6001');

Vue.filter('sumArray', function (data) {
    var qty = 0;
    data.forEach(function (d) {
        qty += d.qty;
    });

    return qty;
});
Vue.component('modal', {
    template: '#modal',
    props: ['show']
});

Vue.directive('datepicker', {
    twoWay: true,
    bind: function () {
        var self = this;
        $(this.el).datepicker({
            format: "yyyy-mm-dd",
            startDate: moment().format('YYYY-MM-DD'),
            todayHighlight: true
        }).on('change', function () {
            self.set(this.value);
        });
    },
    update: function (value) {
        $(this.el).val(value).trigger('change');
    }
});

var vm = new Vue({
    el: '#app',
    data: {
        notification: 0,
        selected: 0,
        options: [],
        query: '',
        data: [],
        currentPage: 1,
        totalPage: 1,
        showModalAddForm: false,
        showModalAddCustomCodeForm: false,
        formCustom: {
            value: 0,
            brand: 0,
            minimum: 0,
            expired: moment().format("YYYY-MM-DD"),
            setting: 'none',
            qty: 0,
            kode: ''
        },
        formAdd: {
            value: 0,
            brand: 0,
            minimum: 0,
            expired: moment().format("YYYY-MM-DD"),
            setting: 'none',
            qty: 0
        },
        listmerchant: []
    },
    computed: {
        isLast: function () {
            return this.currentPage === this.totalPage;
        },
        isFirst: function () {
            return this.currentPage === 1;
        }
    },
    watch: {
        'selected': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        },
        'query': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        }
    },
    methods: {
        getData: function (page) {
            if (!page) {
                page = 1;
            }
            var self = this;
            $.get('/api/coupons', {
                page: page
            }).done(function (data) {
                self.data = data.data;
                self.currentPage = data.current_page;
                self.totalPage = data.last_page;
            });
        },
        showModalAdd: function () {
            this.showModalAddForm = true;
        },
        getCategories: function () {
            var self = this;
            $.get('/api/categories')
                .done(function (data) {
                    self.options = data;
                });
        },
        prev: function () {
            if (this.currentPage === 1) {
                return;
            }
            this.getData(this.currentPage - 1);
        },
        next: function () {
            if (this.currentPage === this.totalPage) {
                return;
            }
            this.getData(this.currentPage + 1);
        },
        showMItem: function (data) {
            console.log(this.showModalItemForm);
            this.formItem.productId = data.id;
            this.formItem.sizeId = '';
            this.formItem.qty = '';
            this.showModalItemForm = true;
            console.log(this.showModalItemForm);
        },
        getSize: function () {
            var self = this;
            $.get('/api/sizes')
                .done(function (result) {
                    self.listSize = result;
                });
        },
        submitAddCoupon: function (e) {
            e.preventDefault();
            var formData = new FormData();
            formData.append('value', this.formAdd.value * 1);
            formData.append('minimum', this.formAdd.minimum * 1);
            formData.append('expired', this.formAdd.expired);
            formData.append('setting', this.formAdd.setting);
            formData.append('qty', this.formAdd.qty * 1);
            var self = this;
            $.ajax({
                url: '/api/coupons',
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false
            }).done(function () {
                self.showModalAddForm = false;
                self.formAdd.value = 0;
                self.formAdd.minimum = 0;
                self.formAdd.expired = moment().format('YYYY-MM-DD');
                self.formAdd.setting = 'none';
                self.formAdd.qty = 0;
                self.getData();
            });
        },
        submitAddCustomCoupon: function (e) {
            e.preventDefault();
            var formData = new FormData();
            formData.append('value', this.formCustom.value * 1);
            formData.append('brand', this.formCustom.brand * 1);
            formData.append('minimum', this.formCustom.minimum * 1);
            formData.append('expired', this.formCustom.expired);
            formData.append('setting', this.formCustom.setting);
            formData.append('kode', this.formCustom.kode);
            formData.append('qty', this.formCustom.qty * 1);
            var self = this;
            $.ajax({
                url: '/api/coupons/custom',
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false
            }).done(function () {
                self.showModalAddCustomCodeForm = false;
                self.formCustom.value = 0;
                self.formCustom.brand = 0;
                self.formCustom.minimum = 0;
                self.formCustom.expired = moment().format('YYYY-MM-DD');
                self.formCustom.setting = 'none';
                self.formCustom.qty = 0;
                self.formCustom.kode = '';
                self.getData();
            });
        },
        getMerchants: function () {
            var self = this;
            $.get('/api/users/merchants/select')
                .done(function (resp) {
                    self.listmerchant = resp;
                });
        },
        isValid: function (data) {
            return moment().isBefore(moment(data.expired_at).format("YYYY-MM-DD")) && (data.qty > 0);
        },
        deleteCoupon: function (coupon) {
            var self = this;
            bootbox.confirm('Are you sure to delete a Coupon ?', function (result) {
                if (result) {
                    $.get('/api/coupons/delete/' + coupon.id)
                        .done(function () {
                            toastr.success('Coupon berhasil didelete');
                            self.getData();
                        });
                }
            });
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        var self = this;
        App.init();
        Layout.init();
        socket.on('merchant.' + obid.authId + ':orders.confirmed', function (message) {
            self.notification += 1;
        });
        this.getNotif();
        this.getData();
    },
    components: {}
});
