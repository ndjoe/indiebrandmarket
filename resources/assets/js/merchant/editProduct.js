Vue.directive('select', {
    twoWay: true,
    selectizeSettings: {},
    defaultValue: "",
    bind: function () {
        var optionsExpression = this.el.getAttribute('options'),
            settingsExpression = this.el.getAttribute('settings'),
            self = this,
            optionsData;
        this.defaultValue = this.el.getAttribute('value');
        if (optionsExpression) {
            optionsData = this.vm.$eval(optionsExpression);
            this.vm.$watch(optionsExpression, this.optionsChange.bind(this));
        }

        this.selectizeSettings = {
            options: optionsData,
            onChange: function (value) {
                self.set(value);
                self.nativeEvent('change').call();
            },
            onFocus: this.nativeEvent('focus').bind(this),
            onBlur: this.nativeEvent('blur').bind(this)
        };

        if (settingsExpression) {
            var userSettings = this.vm.$eval(settingsExpression);
            this.selectizeSettings = $.extend({}, this.selectizeSettings, userSettings);
            this.vm.$watch(settingsExpression, this.settingsChange.bind(this), {
                deep: true
            });
        }

        $(this.el).selectize(this.selectizeSettings);
    },

    nativeEvent: function (eventName) {
        var self = this;
        return function () {
            var event = new Event(eventName);
            self.el.dispatchEvent(event);
        };
    },

    optionsChange: function (options) {
        var selectize = this.el.selectize,
            value = this.el.selectize.getValue();

        selectize.clearOptions();
        selectize.addOption(options);
        selectize.refreshOptions(false);
        console.log(value);
        console.log(this.el.getAttribute('value'));
        if (!value) {
            selectize.setValue(this.el.getAttribute('value'));
        } else {
            selectize.setValue(value);
        }
        console.log(selectize.getValue());
    },

    settingsChange: function (settings) {
        var value = this.el.selectize.getValue();

        this.selectizeSettings = $.extend({}, this.selectizeSettings, settings);

        this.el.selectize.destroy();
        $(this.el).selectize(this.selectizeSettings);
        this.el.selectize.setValue(value);
    },

    update: function (value) {
        this.el.selectize.setValue(value);
    },

    unbind: function () {
        this.el.selectize.destroy();
    }
});

Vue.directive('datepicker', {
    twoWay: true,
    bind: function () {
        var self = this;
        $(this.el).datepicker({
            format: "yyyy-mm-dd",
            startDate: moment().format('YYYY-MM-DD'),
            todayHighlight: true
        }).on('change', function () {
            self.set(this.value);
        });
    },
    update: function (value) {
        $(this.el).val(value).trigger('change');
    }
});

Vue.directive('numberpicker', {
    twoWay: true,
    bind: function () {
        var postfix = this.el.getAttribute('postfix');
        var max = this.el.getAttribute('max');
        var self = this;
        $(this.el)
            .TouchSpin({
                buttondown_class: 'btn blue',
                buttonup_class: 'btn blue',
                min: 0,
                max: max,
                step: 1,
                decimals: 0,
                boostat: 5,
                maxboostedstep: 10,
                postfix: postfix
            }).on('change', function () {
            self.set(this.value);
        });
    },
    update: function (value) {
        $(this.el).val(value).trigger('change');
    }
});
function preview1(input, data) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            data.photo.photo1 = e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function preview2(input, data) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            data.photo.photo2 = e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function preview3(input, data) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            data.photo.photo3 = e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function preview4(input, data) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            data.photo.photo4 = e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function preview5(input, data) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            data.photo.photo5 = e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function preview6(input, data) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            data.photo.photo6 = e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function preview7(input, data) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            data.photo.photo7 = e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function preview8(input, data) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            data.photo.photo8 = e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }
}

var socket = io(window.location.hostname + ':6001');
Vue.filter('size', function (value, list) {
    return !isNaN(value * 1) ? list.filter(function (obj) {
        return obj.value == value;
    })[0].text : value;
});
Vue.component('modal', {
    template: '#modal',
    props: ['show']
});
new Vue({
    el: '#app',
    data: {
        notification: 0,
        nama: obid.product.name,
        kategori: obid.product.category_id,
        harga: obid.product.price * 1,
        color: obid.product.color_id,
        subKategori: obid.product.subcategory_id,
        berat: obid.product.weight * 1,
        percentDiscount: obid.product.discount.value * 1,
        expiredDiscount: moment(obid.product.discount.expired_at).format('YYYY-MM-DD'),
        description: obid.product.description,
        gender: obid.product.gender,
        listKategori: [],
        colorOption: [],
        listSubkategori: [],
        selectizeOpts: {
            create: true,
            selectOnTab: true
        },
        photo: {
            photo1: '/images/catalog/' + obid.product.images[0].name,
            photo2: (obid.product.images[1].name.trim()) ? '/images/catalog/' + obid.product.images[1].name : '',
            photo3: (obid.product.images[2].name.trim()) ? '/images/catalog/' + obid.product.images[2].name : '',
            photo4: (obid.product.images[3].name.trim()) ? '/images/catalog/' + obid.product.images[3].name : '',
            photo5: (obid.product.images[4].name.trim()) ? '/images/catalog/' + obid.product.images[4].name : '',
            photo6: (obid.product.images[5].name.trim()) ? '/images/catalog/' + obid.product.images[5].name : '',
            photo7: (obid.product.images[6].name.trim()) ? '/images/catalog/' + obid.product.images[6].name : '',
            photo8: (obid.product.images[7].name.trim()) ? '/images/catalog/' + obid.product.images[7].name : ''
        },
        items: obid.product.items,
        formItem: {
            productId: obid.product.id,
            sizeId: '',
            qty: '',
            desc: ''
        },
        modalItemForm: false,
        listSize: obid.resp
    },
    computed: {
        validation: function () {
            return {
                nama: validator.isLength(this.nama, 1),
                kategori: validator.isLength(this.kategori, 1),
                harga: validator.isNumeric(this.harga),
                color: validator.isLength(this.color, 1),
                subKategori: validator.isLength(this.subKategori, 1),
                berat: validator.isNumeric(this.berat) && this.berat > 0,
                percentDiscount: validator.isNumeric(this.percentDiscount),
                expiredDiscount: validator.isLength(this.expiredDiscount, 1),
                description: validator.isLength(this.description, 1, 600),
            };
        },
        formValid: function () {
            var validator = this.validation;
            return Object.keys(validator).every(function (k) {
                return validator[k];
            });
        }
    },
    methods: {
        getCategories: function () {
            var self = this;
            $.get('/api/categories')
                .done(function (data) {
                    self.listKategori = data;
                });
        },
        submitProduct: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;
            [1, 2, 3, 4, 5, 6, 7, 8].forEach(function (v) {
                if (self.$els['fileinput' + v].files.length > 0) {
                    formData.append('photo-' + v, self.$els['fileinput' + v].files[0]);
                } else {
                    if (!(self.photo['photo' + v].trim()) && (v !== 1)) {
                        formData.append('delphoto-' + v, 'delete');
                    }
                }
            });
            formData.append('nama', this.nama);
            formData.append('kategori', this.kategori);
            formData.append('harga', this.harga);
            formData.append('color', this.color);
            formData.append('subkategori', this.subKategori);
            formData.append('berat', this.berat);
            formData.append('percent', this.percentDiscount * 1);
            formData.append('expired', this.expiredDiscount);
            formData.append('description', this.description);
            formData.append('sizing', this.sizing);
            formData.append('gender', this.gender);
            $.ajax({
                url: '/api/product/' + obid.product.id,
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                type: 'POST',
                statusCode: {
                    422: function (e) {
                        Object.keys(e.responseJSON).forEach(function (k) {
                            e.responseJSON[k].forEach(function (m) {
                                toastr.error(m);
                            });
                        });
                    }
                }
            }).done(function (data) {
                if (data.updated) {
                    toastr.success('Product ' + obid.product.name + ' berhasil di update');
                    setTimeout(function () {
                        window.location.href = data.redirect;
                    }, 1000);
                } else {
                    toastr.error(data.reason);
                }
            });
        },
        getColors: function () {
            var self = this;
            $.get('/api/colors/option')
                .done(function (resp) {
                    if (resp.length > 0)
                        self.colorOption = resp;
                });
        },
        getSubcategories: function () {
            var self = this;
            $.get('/api/subcategories')
                .done(function (resp) {
                    if (resp.length > 0)
                        self.listSubkategori = resp;
                });
        },
        removeImage: function (id) {
            if (id === 1) {
                toastr.error('gambar utama tidak boleh didelete');
                return;
            }
            console.log(this.$els['fileinput' + id].files[0]);
            this.$els['fileinput' + id].files = null;
            this.photo['photo' + id] = '';
            $('#photo-' + id).val('');
            console.log(this.$els['fileinput' + id].files[0]);
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        },
        getSize: function () {
            var self = this;
            $.get('/api/sizes')
                .done(function (result) {
                    self.listSize = result;
                });
        },
        submitItem: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;
            formData.append('product', obid.product.id);
            formData.append('size', this.formItem.sizeId);
            formData.append('desc', this.formItem.desc);
            formData.append('qty', this.formItem.qty);
            $.ajax({
                url: '/api/items/set',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                type: 'POST'
            }).done(function (data) {
                if (data.created) {
                    toastr.success('item berhasil diupdate');
                } else {
                    toastr.error('item gagal update');
                }
                self.modalItemForm = false;
                self.getSize();
                self.formItem.sizeId = '';
                self.formItem.qty = '';
                self.formItem.desc = '';
                self.getItem(obid.product.id);
            });
        },
        getItem: function (id) {
            var self = this;
            $.get('/api/product/item/' + id)
                .done(function (data) {
                    self.items = data;
                });
        }
    },
    ready: function () {
        var self = this;
        App.init();
        Layout.init();
        this.getColors();
        this.getCategories();
        this.getSubcategories();
        $('#photo-1').change(function () {
            preview1(this, self);
        });
        $('#photo-2').change(function () {
            preview2(this, self);
        });
        $('#photo-3').change(function () {
            preview3(this, self);
        });
        $('#photo-4').change(function () {
            preview4(this, self);
        });
        $('#photo-5').change(function () {
            preview5(this, self);
        });
        $('#photo-6').change(function () {
            preview6(this, self);
        });
        $('#photo-7').change(function () {
            preview7(this, self);
        });
        $('#photo-8').change(function () {
            preview8(this, self);
        });
        socket.on('merchant.' + obid.authId + ':orders.confirmed', function (message) {
            self.notification += 1;
        });
    }
});