var socket = io(window.location.hostname + ':6001');

var vm = new Vue({
    el: '#app',
    data: {
        notification: 0,
        username: '',
        email: '',
        password: '',
        passwordConfirm: '',
        nama: '',
        nohp: ''
    },
    computed: {
        'validates': function () {
            var nohpReg = /^(0+8*\d)\d+/g;
            return {
                username: validator.isAlphanumeric(this.username) && validator.isLength(this.username, 3),
                email: validator.isEmail(this.email),
                nama: validator.isLength(this.nama, 1),
                nohp: nohpReg.test(this.nohp),
                password: (this.password.trim()) ? validator.isLength(this.password, 6) : true,
                passwordConfirm: validator.equals(this.password, this.passwordConfirm)
            }
        },
        'isValid': function () {
            var validator = this.validates;
            return Object.keys(validator).every(function (k) {
                return validator[k];
            });
        }
    },
    methods: {
        submitAdmin: function (e) {
            e.preventDefault();
            var formData = new FormData;
            formData.append('username', this.username);
            formData.append('password', this.password);
            formData.append('email', this.email);
            formData.append('password_confirmation', this.passwordConfirm);
            formData.append('nama', this.nama);
            formData.append('nohp', this.nohp);

            $.ajax({
                url: '/api/pos/accounts',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                type: 'POST',
                statusCode: {
                    401: function (e) {
                        window.location.href = e.responseJSON.redirect;
                    }
                }
            }).done(function (data) {
                if (data.created) {
                    window.location.href = data.redirect;
                } else {
                    console.log(data);
                }
            });
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    components: {},
    ready: function () {
        var self = this;
        App.init();
        Layout.init();
        this.getNotif();
        socket.on('merchant.' + obid.authId + ':orders.confirmed', function (message) {
            self.notification += 1;
        });
    }
});