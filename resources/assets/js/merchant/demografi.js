var graphAge = Morris.Bar({
    element: 'brand-chart-age',
    data: [],
    xkey: 'label',
    ykeys: ['value'],
    labels: ['Banyak']
});
var graphGender = Morris.Bar({
    element: 'brand-chart-gender',
    data: [],
    xkey: 'label',
    ykeys: ['value'],
    labels: ['Banyak']
});

new Vue({
    el: '#app',
    data: {
        merchant: 0
    },
    methods: {
        getBrandStat: function () {
            $.get('/api/demografi/brand/' + obid.authId)
                .done(function (resp) {
                    graphAge.setData(resp.age);
                    graphGender.setData(resp.gender);
                });
        }
    },
    ready: function () {
        App.init();
        Layout.init();
        this.getBrandStat();
    }
});
