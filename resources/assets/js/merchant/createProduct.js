Vue.use(window['vue-selectize']);
Vue.filter('size', function (value, list) {
    return !isNaN(value * 1) ? list.filter(function (obj) {
        return obj.value == value;
    })[0].text : value;
});
Vue.directive('datepicker', {
    twoWay: true,
    bind: function () {
        var self = this;
        $(this.el).datepicker({
            format: "yyyy-mm-dd",
            startDate: moment().format('YYYY-MM-DD'),
            todayHighlight: true
        }).on('change', function () {
            self.set(this.value);
        });
    },
    update: function (value) {
        $(this.el).val(value).trigger('change');
    }
});

Vue.component('modal', {
    template: '#modal',
    props: ['show']
});
Vue.directive('numberpicker', {
    twoWay: true,
    bind: function () {
        var postfix = this.el.getAttribute('postfix');
        var max = this.el.getAttribute('max');
        var self = this;
        $(this.el)
            .TouchSpin({
                buttondown_class: 'btn blue',
                buttonup_class: 'btn blue',
                min: 0,
                max: max,
                step: 1,
                decimals: 0,
                boostat: 5,
                maxboostedstep: 10,
                postfix: postfix
            }).on('change', function () {
            self.set(this.value);
        });
    },
    update: function (value) {
        $(this.el).val(value).trigger('change');
    }
});
var socket = io(window.location.hostname + ':6001');
new Vue({
    el: '#app',
    data: {
        notification: 0,
        nama: "",
        kategori: "",
        harga: "",
        color: "",
        subKategori: "",
        berat: 0,
        percentDiscount: 0,
        expiredDiscount: moment().format('YYYY-MM-DD'),
        listKategori: [],
        colorOption: [],
        listSubkategori: [],
        gender: 'all',
        description: '',
        submitted: false,
        selectizeOpts: {
            create: true,
            selectOnTab: true,
            maxItems: 1
        },
        sizing: '',
        items: [],
        modalItemForm: false,
        formItem: {
            sizeId: '',
            qty: '',
            desc: ''
        },
        listSize: []
    },
    computed: {
        validation: function () {
            return {
                nama: validator.isLength(this.nama, 1),
                kategori: validator.isLength(this.kategori, 1),
                harga: validator.isNumeric(this.harga),
                color: validator.isLength(this.color, 1),
                subKategori: validator.isLength(this.subKategori, 1),
                berat: validator.isNumeric(this.berat) && this.berat > 0,
                percentDiscount: validator.isNumeric(this.percentDiscount),
                expiredDiscount: validator.isLength(this.expiredDiscount, 1),
                description: validator.isLength(this.description, 1, 600)
            };
        },
        formValid: function () {
            var validator = this.validation;
            return Object.keys(validator).every(function (k) {
                return validator[k];
            });
        }
    },
    methods: {
        getCategories: function () {
            var self = this;
            $.get('/api/categories')
                .done(function (data) {
                    self.listKategori = data;
                });
        },
        submitProduct: function (e) {
            e.preventDefault();
            if (this.$els.fileinput1.files.length < 1) {
                toastr.error("Gambar Utama tidak boleh kosong");
                return false;
            }

            var formData = new FormData;
            formData.append('photo-1', this.$els.fileinput1.files[0]);
            if (this.$els.fileinput2.files.length > 0) {
                formData.append('photo-2', this.$els.fileinput2.files[0]);
            }
            if (this.$els.fileinput3.files.length > 0) {
                formData.append('photo-3', this.$els.fileinput3.files[0]);
            }
            if (this.$els.fileinput4.files.length > 0) {
                formData.append('photo-4', this.$els.fileinput4.files[0]);
            }
            if (this.$els.fileinput5.files.length > 0) {
                formData.append('photo-5', this.$els.fileinput5.files[0]);
            }
            if (this.$els.fileinput6.files.length > 0) {
                formData.append('photo-6', this.$els.fileinput6.files[0]);
            }
            if (this.$els.fileinput7.files.length > 0) {
                formData.append('photo-7', this.$els.fileinput7.files[0]);
            }
            if (this.$els.fileinput8.files.length > 0) {
                formData.append('photo-8', this.$els.fileinput8.files[0]);
            }
            formData.append('nama', this.nama);
            formData.append('kategori', this.kategori);
            formData.append('harga', this.harga);
            formData.append('color', this.color);
            formData.append('subkategori', this.subKategori);
            formData.append('berat', this.berat);
            formData.append('percent', this.percentDiscount * 1);
            formData.append('expired', this.expiredDiscount);
            formData.append('description', this.description);
            formData.append('sizing', this.sizing);
            formData.append('gender', this.gender);
            this.items.forEach(function (i) {
                formData.append('size[]', i.size);
                formData.append('qty[]', i.qty);
                formData.append('desc[]', i.desc);
            });
            this.submitted = true;
            var self = this;
            $.ajax({
                url: '/api/products',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                type: 'POST',
                statusCode: {
                    422: function (e) {
                        Object.keys(e.responseJSON).forEach(function (k) {
                            e.responseJSON[k].forEach(function (m) {
                                toastr.error(m);
                            });
                        });

                        self.submitted = false;
                    },
                    401: function (e) {
                        window.location.href = e.responseJSON.redirect;
                    }
                }
            }).done(function (data) {
                if (data.created) {
                    toastr.success('Produk Berhasil Ditambahkan');
                    setTimeout(function () {
                        window.location.href = data.redirect;
                    }, 1000);
                } else {
                    toastr.error(data.reason);
                    self.submitted = false;
                }
            });
        },
        getColors: function () {
            var self = this;
            $.get('/api/colors/option')
                .done(function (resp) {
                    if (resp.length > 0)
                        self.colorOption = resp;
                });
        },
        getSubcategories: function () {
            var self = this;
            $.get('/api/subcategories')
                .done(function (resp) {
                    if (resp.length > 0)
                        self.listSubkategori = resp;
                });
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        },
        submitItem: function (e) {
            e.preventDefault();
            var newItem = {
                size: this.formItem.sizeId,
                qty: this.formItem.qty,
                desc: this.formItem.desc
            };

            this.items.push(newItem);
            this.formItem.sizeId = '';
            this.formItem.qty = '';
            this.formItem.desc = '';
            this.modalItemForm = false;
        },
        getSize: function () {
            var self = this;
            $.get('/api/sizes')
                .done(function (result) {
                    self.listSize = result;
                });
        },
    },
    ready: function () {
        var self = this;
        App.init();
        Layout.init();
        this.getColors();
        this.getCategories();
        this.getSubcategories();
        this.getNotif();
        this.getSize();
        socket.on('merchant.' + obid.authId + ':orders.confirmed', function (message) {
            self.notification += 1;
        });
    }
});