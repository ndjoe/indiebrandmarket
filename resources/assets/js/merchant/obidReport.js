Vue.filter('length', function (data) {
    return data.length;
});
Vue.filter('komisiMerchant', function (data) {
    return data['adjusment'] * 0.9;
});
Vue.filter('formatMoney', function (data) {
    return accounting.formatMoney(data, 'IDR ', 0, '.', ',');
});
Vue.directive('datepicker', {
    twoWay: true,
    bind: function () {
        var self = this;
        $(this.el).datepicker({
            format: "yyyy-mm-dd",
            todayHighlight: true
        }).on('change', function () {
            self.set(this.value);
        });
    },
    update: function (value) {
        $(this.el).val(value).trigger('change');
    }
});
var chartData = [];
var graph = Morris.Line({
    element: 'chart',
    data: chartData,
    xkey: 'time',
    ykeys: ['value'],
    labels: ['Total Penjualan'],
    parseTime: false,
    xLableAngle: 90
});
var today = new Date;
var socket = io(window.location.hostname + ':6001');
var vm = new Vue({
    el: '#app',
    data: {
        notification: 0,
        data: [],
        dataKupon: [],
        dataOrder: [],
        options: 'month',
        date: moment().format('YYYY-MM-DD'),
        monthName: moment().format('MMMM YYYY')
    },
    computed: {
        pendapatan: function () {
            var total = 0;
            this.data.forEach(function (d) {
                total += d.adjusment * 1;
            });

            return total * 0.9;
        },
        totalKuponSum: function () {
            var total = 0;
            this.dataKupon.forEach(function (d) {
                total += d.value;
            });

            return total;
        },
        totalAdminSum: function () {
            return this.dataOrder.length * 5000;
        }
    },
    watch: {},
    methods: {
        getData: function () {
            var self = this;
            $.get('/api/report', {
                type: this.options,
                date: this.date
            }).done(function (data) {
                graph.setData(data.chartSales);
                self.data = data.itemList;
                self.dataKupon = data.couponList;
                self.dataOrder = data.orderList;
                self.monthName = moment(self.date).format('MMMM YYYY');
            });
        },
        setToday: function () {
            this.date = (new Date()).toISOString().slice(0, 10);
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        var self = this;
        App.init();
        Layout.init();
        this.getData();
        this.getNotif();
        socket.on('merchant.' + obid.authId + ':orders.confirmed', function (message) {
            self.notification += 1;
        });
    },
    components: {
        'order-stat': {
            template: '#dashboardstat',
            props: {
                color: String,
                icon: String,
                value: String,
                title: String,
                link: String
            }
        }
    }
});