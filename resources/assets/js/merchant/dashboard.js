var orderCountChart = Morris.Area({
    element: 'chartOrderCount',
    padding: 10,
    fillOpacity: 1,
    data: obid.todayOrderCountChart,
    xkey: 'time',
    ykeys: ['value'],
    labels: ['Jumlah'],
    parseTime: false,
    lineColors: ['#399a8c', '#92e9dc'],
    hideHover: 'auto',
    resize: true
});
var orderSumChart = Morris.Area({
    element: 'chartOrderSum',
    padding: 10,
    behaveLikeLine: false,
    fillOpacity: 1,
    data: obid.todayOrderSumChart,
    xkey: 'time',
    ykeys: ['value'],
    labels: ['Jumlah'],
    parseTime: false,
    lineColors: ['#399a8c', '#92e9dc'],
    hideHover: 'auto',
    resize: true
});
var socket = io(window.location.hostname + ':6001');
new Vue({
    el: '#app',
    data: {
        notification: 0,
        totalOrderCount: obid.totalOrderCount,
        totalOrderSum: obid.totalOrderSum,
        todayOrderCount: obid.todayOrderCount,
        todayOrderSum: obid.todayOrderSum,
        optionOrderCount: 'day',
        optionOrderSum: 'day',
        topProducts: obid.topSaleProducts
    },
    watch: {
        'optionOrderCount': function (nv, ov) {
            if (nv == 'month') {
                orderCountChart.setData(obid.totalOrderCountChart);
            } else {
                orderCountChart.setData(obid.todayOrderCountChart);
            }
        },
        'optionOrderSum': function (nv, ov) {
            if (nv == 'month') {
                orderSumChart.setData(obid.totalOrderSumChart);
            } else {
                orderSumChart.setData(obid.todayOrderSumChart);
            }
        }
    },
    methods: {
        setOptionCount: function (data) {
            this.optionOrderCount = data;
        },
        setOptionSum: function (data) {
            this.optionOrderSum = data;
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    components: {
        'order-stat': {
            template: '#dashboardstat',
            props: {
                color: String,
                icon: String,
                value: Number,
                title: String,
                link: String
            }
        }
    },
    ready: function () {
        var self = this;
        App.init();
        Layout.init();
        this.getNotif();
        socket.on('merchant.' + obid.authId + ':orders.confirmed', function (message) {
            self.notification += 1;
        });
    }
});