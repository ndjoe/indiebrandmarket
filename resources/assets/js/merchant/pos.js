Vue.component('modal', {
    template: '#modal',
    props: ['show']
});
var socket = io(window.location.hostname + ':6001');
var vm = new Vue({
    el: '#app',
    data: {
        notification: 0,
        selected: 'all',
        query: '',
        data: [],
        currentPage: 1,
        totalPage: 1,
        posCash: 0,
        showModalCashForm: false,
        formCash: {
            now: 0,
            new: 0
        }
    },
    computed: {
        isLast: function () {
            return this.currentPage === this.totalPage;
        },
        isFirst: function () {
            return this.currentPage === 1;
        }
    },
    watch: {
        'selected': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        },
        'query': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        }
    },
    methods: {
        getData: function (page) {
            if (!page) {
                page = 1;
            }
            var self = this;
            $.get('/api/pos/accounts', {
                query: this.query,
                page: page
            }).done(function (data) {
                self.data = data.data;
                self.currentPage = data.current_page;
                self.totalPage = data.last_page;
            });
        },
        prev: function () {
            if (this.currentPage === 1) {
                return;
            }
            this.getData(this.currentPage - 1);
        },
        next: function () {
            if (this.currentPage === this.totalPage) {
                return;
            }
            this.getData(this.currentPage + 1);
        },
        getPosCash: function () {
            var self = this;
            $.get('/api/pos/' + obid.merchantId + '/cash')
                .done(function (data) {
                    self.posCash = data;
                });
        },
        showModal: function () {
            this.formCash.now = this.posCash;
            this.formCash.new = 0;
            this.showModalCashForm = true;
        },
        submitCash: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;

            formData.append('new', this.formCash.new * 1);

            $.ajax({
                url: '/api/pos/cash',
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false
            }).done(function (data) {
                self.showModalCashForm = false;
                self.formCash.now = 0;
                self.formCash.new = 0;
                self.posCash = data.newValue;
            });
        },
        activate: function (data) {
            var self = this;
            $.get('/api/pos/account/' + data.id + '/activate')
                .done(function () {
                    self.getData();
                });
        },
        deactivate: function (data) {
            var self = this;
            $.get('/api/pos/account/' + data.id + '/deactivate')
                .done(function () {
                    self.getData();
                });
        },
        deleteUser: function (data) {
            var self = this;
            $.get('/api/pos/account/' + data.id + '/delete')
                .done(function () {
                    self.getData();
                });
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        var self = this;
        App.init();
        Layout.init();
        this.getData();
        this.getPosCash();
        this.getNotif();
        socket.on('merchant.' + obid.authId + ':orders.confirmed', function (message) {
            self.notification += 1;
        });
    },
    components: {}
});