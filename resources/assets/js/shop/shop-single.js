var vm = new Vue({
    el: '#shop',
    data: {
        cart: [],
        locale: obid.locale,
        currency: obid.locale === 'id' ? 'IDR' : 'USD',
        newsletter: {
            email: ''
        },
        optionSize: obid.options.map(function (obj) {
            return {
                value: {
                    id: obj.value,
                    qty: obj.qty
                },
                text: obj.text
            };
        }),
        size: ''
    },
    computed: {},
    watch: {
        'size': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            if (nv.id > 0) {
                $(".product-quantity .form-control").trigger("touchspin.updatesettings", {max: nv.qty, min: 1})
            } else {
                $(".product-quantity .form-control").trigger("touchspin.updatesettings", {max: 0, min: 0})
            }
        }
    },
    methods: {
        getCart: function () {
            var self = this;
            $.get('/' + obid.locale + '/api/cart')
                .done(function (resp) {
                    self.cart = resp.items;
                });
        },
        addCart: function (e) {
            e.preventDefault();
            var qty = this.$els.qty.value;
            var id = this.$els.id.value;
            var self = this;
            var formData = new FormData;
            formData.append('productId', id);
            formData.append('sizeId', this.size.id);
            formData.append('qty', qty);
            $.ajax({
                url: '/' + obid.locale + '/api/cart',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                aync: true,
                type: 'POST'
            }).done(function (data) {
                self.cart = data.items;
                toastr.success('Produk ditambahkan ke keranjang belanja');
            });
        },
        delItem: function (data) {
            var self = this;
            var formData = new FormData;

            formData.append('rowid', data['rowid']);

            $.ajax({
                url: '/' + obid.locale + '/api/cart/delete',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                type: 'POST'
            }).done(function (data) {
                self.cart = data.items;
            });
        },
        subscribesNewsLetter: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;
            formData.append('email', this.newsletter.email);

            $.ajax({
                url: '/' + obid.locale + '/api/newsletter',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST',
                statusCode: {
                    422: function (e) {
                        toastr.error("email invalid");
                    }
                }
            }).done(function (data) {
                if (data.subscribed) {
                    swal(
                        "You have been subscribed",
                        "Please save following code and use it for your next purchase: "
                        + data.couponCode,
                        "success"
                    );
                    self.newsletter.email = "";
                } else {
                    swal(
                        "Ooops...",
                        "Your have been subscribed to our news letter.... sorry for that",
                        "error"
                    );
                    self.newsletter.email = "";
                }
            });
        }
    },
    ready: function () {
        $('.holder').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true
        });
        $('.slider-nav').slick({
            asNavFor: '.holder',
            dots: false,
            vertical: true,
            focusOnSelect: true,
            arrows: true,
            slidesToShow: 4
        });

        Layout.init();
        Layout.initTwitter();
        //Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();
        $('div.zoomit').each(function () {
            var url = $(this).find('img')[0].src;
            $(this).zoom({
                url: url
            });
        });
        $(".product-quantity .form-control").TouchSpin({
            buttondown_class: "btn quantity-down",
            buttonup_class: "btn quantity-up",
            max: 0,
            value: 0
        });
        $(".quantity-down").html("<i class='fa fa-angle-down'></i>");
        $(".quantity-up").html("<i class='fa fa-angle-up'></i>");
        this.getCart();
    }
});
