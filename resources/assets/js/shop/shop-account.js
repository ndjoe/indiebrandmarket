Vue.use(window['vue-selectize']);

new Vue({
    el: '#shop',
    data: {
        data: [],
        currentPage: 1,
        totalPage: 1,
        brands: [],
        cart: [],
        currency: obid.locale === 'id' ? 'IDR' : 'USD',
        locale: obid.locale,
        newsletter: {
            email: ''
        },
        month: moment(obid.user.profileable.birthday).format("MM"),
        year: moment(obid.user.profileable.birthday).format("YYYY"),
        date: moment(obid.user.profileable.birthday).format("DD"),
        alamat: obid.user.profileable.alamat,
        kodepos: obid.user.profileable.kodepos,
        provinsi: obid.user.profileable.ongkir_province_id,
        kota: obid.user.profileable.ongkir_city_id,
        gender: obid.user.profileable.gender,
        username: obid.user.username,
        email: obid.user.email,
        nama: obid.user.nama,
        nohp: obid.user.nohp,
        profile: obid.user,
        submitting: false,
        listProvince: obid.provinces.map(function (p) {
            return {
                value: p.value.id,
                text: p.text
            };
        }),
        listCity: obid.cities.map(function (p) {
            return {
                value: p.value.id,
                text: p.text
            }
        })
    },
    computed: {
        listYear: function () {
            var years = [];
            for (var i = new Date().getFullYear() - 10; i > 1950; i--) {
                years.push({
                    value: i,
                    text: i
                });
            }

            return years;
        },
        listMonth: function () {
            var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var list = [];
            for (var i = 1; i < months.length + 1; i++) {
                list.push({
                    value: i,
                    text: months[i - 1]
                });
            }

            return list;
        },
        listDate: function () {
            var month = this.month;
            var year = this.year;
            if (month === 0 || year === 0) {
                return [];
            }
            var date = new Date(year, month + 1, 0).getDate();
            var list = [];

            for (var i = 1; i < date; i++) {
                list.push({
                    value: i,
                    text: i
                });
            }

            return list;
        }
    },
    methods: {
        getData: function (page) {
            if (!page) {
                page = 1;
            }
            var self = this;
            $.get('/' + obid.locale + '/api/products', {
                query: this.query,
                filter: this.selected,
                page: page
            }).done(function (data) {
                self.data = data.data;
                self.currentPage = data.current_page;
                self.totalPage = data.last_page;
            });
        },
        prev: function () {
            if (this.currentPage === 1) {
                return;
            }
            this.getData(this.currentPage - 1);
        },
        next: function () {
            if (this.currentPage === this.totalPage) {
                return;
            }
            this.getData(this.currentPage + 1);
        },
        getCart: function () {
            var self = this;
            $.get('/' + obid.locale + '/api/cart')
                .done(function (resp) {
                    self.cart = resp.items;
                });
        },
        delItem: function (data) {
            var self = this;
            var formData = new FormData;

            formData.append('rowid', data['rowid']);

            $.ajax({
                url: '/' + obid.locale + '/api/cart/delete',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST'
            }).done(function (data) {
                self.cart = data.items;
            });
        },
        getBrands: function () {
            var self = this;
            $.get('/' + obid.locale + '/api/brands')
                .done(function (resp) {
                    self.brands = resp;
                });
        },
        subscribesNewsLetter: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;
            formData.append('email', this.newsletter.email);

            $.ajax({
                url: '/' + obid.locale + '/api/newsletter',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                type: 'POST',
                statusCode: {
                    422: function (e) {
                        toastr.error("email invalid");
                    }
                }
            }).done(function (data) {
                if (data.subscribed) {
                    swal(
                        "You have been subscribed",
                        "Please save following code and use it for your next purchase: "
                        + data.couponCode,
                        "success"
                    );
                    self.newsletter.email = "";
                } else {
                    swal(
                        "Ooops...",
                        "Your have been subscribed to our news letter.... sorry for that",
                        "error"
                    );
                    self.newsletter.email = "";
                }
            });
        },
        getCityList: function (provinceId) {
            var self = this;
            $.get('/api/provinces/' + provinceId + '/city')
                .done(function (data) {
                    self.listCity = data.map(function (d) {
                        return {
                            value: d.value.id,
                            text: d.text
                        };
                    });
                });
        },
        saveProfile: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;
            formData.append('nama', this.nama);
            formData.append('nohp', this.nohp);
            formData.append('birthday', this.date + '-' + this.month + '-' + this.year);
            formData.append('alamat', this.alamat);
            formData.append('kodepos', this.kodepos);
            formData.append('kota', $.grep(this.listCity, function (v, i) {
                return v.value === self.kota;
            })[0].text);
            formData.append('provinsi', $.grep(this.listProvince, function (v, i) {
                return v.value === self.provinsi;
            })[0].text);
            formData.append('ongkir_province_id', this.provinsi);
            formData.append('ongkir_city_id', this.kota);
            formData.append('gender', this.gender);
            this.submitting = true;
            $.ajax({
                url: '/' + obid.locale + '/api/account/profile/edit',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                type: 'POST'
            }).done(function (data) {
                if (data.updated) {
                    toastr.success("Profil anda berhasi diupdate");
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
            });
        }
    },
    watch: {
        'provinsi': function (nv, ov) {
            if (nv === ov) {
                return;
            }

            this.getCityList(nv);
        }
    },
    ready: function () {
        Layout.init();
        Layout.initTouchspin();
        Layout.initTwitter();
        this.getCart();
    }
});
