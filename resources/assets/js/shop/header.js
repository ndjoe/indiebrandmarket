$(function () {
    Origami.fastclick(document.body);
    var overlay = $('#body-overlay');
    var html = $('html');
    var closebtn = $('.close-btn');
    var wrap = $("body");
    $('.menu-toggler').click(function () {
        if (html.hasClass('open-slide-menu')) {
            html.removeClass('open-slide-menu');
            overlay.css({display: 'none'});
        } else {
            html.addClass('open-slide-menu');
            overlay.css({display: 'block'});
        }
    });

    overlay.click(function () {
        if (html.hasClass('open-slide-menu')) {
            html.removeClass('open-slide-menu');
            overlay.css({display: 'none'});
        }
    });

    closebtn.click(function () {
        if (html.hasClass('open-slide-menu')) {
            html.removeClass('open-slide-menu');
            overlay.css({display: 'none'});
        }
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() > 37) {
            wrap.addClass("header-fixed");
        }
        else {
            wrap.removeClass("header-fixed");
        }
    });
});
