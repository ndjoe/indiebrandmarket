new Vue({
    el: '#shop',
    data: {
        data: [],
        currentPage: 1,
        totalPage: 1,
        cart: [],
        size: 0,
        color: 0,
        category: 0,
        gender: 'all',
        optionSize: [],
        optionCategory: [],
        price: 0,
        colours: [],
        currency: obid.locale === 'id' ? 'IDR' : 'USD',
        locale: obid.locale,
        newsletter: {
            email: ''
        }
    },
    computed: {
        isLast: function () {
            return this.currentPage === this.totalPage;
        },
        isFirst: function () {
            return this.currentPage === 1;
        }
    },
    watch: {
        'size': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        },
        'price': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        },
        'color': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        },
        'gender': function (nv, ov) {
            if (nv === ov) {
                return;
            }

            this.currentPage = 1;
            this.getData();
        }
    },
    methods: {
        getData: function (page) {
            if (!page) {
                page = 1;
            }
            var self = this;
            $.get('/' + obid.locale + '/api/brand/' + obid.brand.username + '/products', {
                category: this.category,
                size: this.size,
                color: this.color,
                price: this.price,
                gender: this.gender,
                page: page
            }).done(function (data) {
                self.data = data.data;
                self.currentPage = data.current_page;
                self.totalPage = data.last_page;
            });
        },
        prev: function (el) {
            if (this.currentPage === 1) {
                return;
            }

            this.getData(this.currentPage - 1);
//                    Layout.scrollTo($('#' + el), 0);
        },
        next: function (el) {
            if (this.currentPage === this.totalPage) {
                return;
            }

            this.getData(this.currentPage + 1);
//                    Layout.scrollTo($('#' + el), );
        },
        getCart: function () {
            var self = this;
            $.get('/' + obid.locale + '/api/cart')
                .done(function (resp) {
                    self.cart = resp.items;
                });
        },
        delItem: function (data) {
            var self = this;
            var formData = new FormData;

            formData.append('rowid', data['rowid']);

            $.ajax({
                url: '/' + obid.locale + '/api/cart/delete',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST'
            }).done(function (data) {
                self.cart = data.items;
            });
        },
        getOptions: function () {
            var self = this;
            $.get('/' + obid.locale + '/api/brand/' + obid.brand.id + '/options')
                .done(function (resp) {
                    self.optionSize = resp.size;
                    self.optionCategory = resp.categories;
                    self.colours = resp.colours;
                });
        },
        setCategory: function (id, el) {
            this.category = !isNaN(id) ? id : id.value;
            console.log(this.category);
            this.currentPage = 1;
            this.getData();
//                    Layout.scrollTo($('#' + el), -500);
        },
        isDiscounted: function (discount) {
            return moment().isBefore(discount.expired_at) && discount.value > 0;
        },
        subscribesNewsLetter: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;
            formData.append('email', this.newsletter.email);

            $.ajax({
                url: '/' + obid.locale + '/api/newsletter',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST',
                statusCode: {
                    422: function (e) {
                        toastr.error("email invalid");
                    }
                }
            }).done(function (data) {
                if (data.subscribed) {
                    swal(
                        "You have been subscribed",
                        "Please save following code and use it for your next purchase: "
                        + data.couponCode,
                        "success"
                    );
                    self.newsletter.email = "";
                } else {
                    swal(
                        "Ooops...",
                        "Your have been subscribed to our news letter.... sorry for that",
                        "error"
                    );
                    self.newsletter.email = "";
                }
            });
        }

    },
    ready: function () {
        Layout.init();
        Layout.initTouchspin();
        Layout.initTwitter();
        //Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();
        this.getOptions();
        this.getCart();
        this.getData();
    },
    components: {}
});
