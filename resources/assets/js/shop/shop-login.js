Vue.use(window['vue-selectize']);

Vue.component('modal', {
    template: '#modal',
    props: ['show']
});

new Vue({
    el: '#shop',
    data: {
        cart: [],
        formLogin: {
            username: '',
            password: ''
        },
        formRegister: {
            nama: '',
            username: '',
            password: '',
            password_confirm: '',
            email: '',
            nohp: ''
        },
        profileRegister: {
            month: 0,
            year: 0,
            date: 0,
            alamat: '',
            kodepos: '',
            provinsi: '',
            kota: '',
            gender: 'unknown'
        },
        usernameCheck: true,
        nohpCheck: true,
        emailCheck: true,
        currency: obid.locale === 'id' ? 'IDR' : 'USD',
        locale: obid.locale,
        newsletter: {
            email: ''
        },
        showProfileModal: false,
        submitting: false
    },
    computed: {
        validator: function () {
            var self = this;
            var nohpReg = /^(0+8*\d)\d+/g;
            return {
                formLogin: {
                    username: {
                        required: validator.isLength(this.formLogin.username, 3)
                    },
                    password: {
                        required: validator.isLength(this.formLogin.password, 3)
                    }
                },
                formRegister: {
                    username: this.formRegister.username.trim() ? validator.isLength(this.formRegister.username, 3) && validator.isAlphanumeric(this.formRegister.username) : true,
                    email: this.formRegister.email.trim() ? validator.isEmail(this.formRegister.email) : true,
                    password: this.formRegister.password.trim() ? validator.isLength(this.formRegister.password, 6) : true,
                    password_confirm: this.formRegister.password_confirm.trim() ? validator.equals(this.formRegister.password_confirm, this.formRegister.password) : true,
                    nama: this.formRegister.nama.trim() ? validator.isLength(this.formRegister.nama, 2) : true,
                    nohp: this.formRegister.nohp.trim() ? nohpReg.test(this.formRegister.nohp) : true,
                    mustBeFilled: (
                        this.formRegister.username.trim()
                        && this.formRegister.email.trim()
                        && this.formRegister.password.trim()
                        && this.formRegister.password_confirm.trim()
                        && this.formRegister.nama.trim()
                        && this.formRegister.nohp.trim()
                    ),
                    checkUsername: this.usernameCheck,
                    checkEmail: this.emailCheck,
                    checkNohp: this.nohpCheck,
                    kodepos: this.profileRegister.kodepos.trim() ? validator.isNumeric(this.profileRegister.kodepos)
                    && validator.isLength(this.profileRegister.kodepos, 5, 5) : true
                }
            }
        },
        formRegisterValid: function () {
            var validator = this.validator;
            return this.usernameCheck
                && this.emailCheck
                && this.nohpCheck
                && Object.keys(validator.formRegister).every(function (k) {
                    return validator.formRegister[k];
                });
        },
        listYear: function () {
            var years = [];
            for (var i = new Date().getFullYear() - 10; i > 1950; i--) {
                years.push({
                    value: i,
                    text: i
                });
            }

            return years;
        },
        listMonth: function () {
            var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var list = [];
            for (var i = 1; i < months.length + 1; i++) {
                list.push({
                    value: i,
                    text: months[i - 1]
                });
            }

            return list;
        },
        listDate: function () {
            var month = this.profileRegister.month;
            var year = this.profileRegister.year;
            if (month === 0 || year === 0) {
                return [];
            }
            var date = new Date(year, month + 1, 0).getDate();
            var list = [];

            for (var i = 1; i < date; i++) {
                list.push({
                    value: i,
                    text: i
                });
            }

            return list;
        }
    },
    watch: {
        'formRegister.username': function (nv, ov) {
            var self = this;
            this.check('username', nv, function (data) {
                self.usernameCheck = !data.exists;
            });
        },
        'formRegister.email': function (nv, ov) {
            var self = this;
            this.check('email', nv, function (data) {
                self.emailCheck = !data.exists;
            });
        },
        'formRegister.nohp': function (nv, ov) {
            var self = this;
            this.check('nohp', nv, function (data) {
                self.nohpCheck = !data.exists;
            });
        }
    },
    methods: {
        getCart: function () {
            var self = this;
            $.get('/' + obid.locale + '/api/cart')
                .done(function (resp) {
                    self.cart = resp.items;
                });
        }
        ,
        delItem: function (data) {
            var self = this;
            var formData = new FormData;

            formData.append('rowid', data['rowid']);

            $.ajax({
                url: '/' + obid.locale + '/api/cart/delete',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST'
            }).done(function (data) {
                self.cart = data.items;
            });
        },
        check: function (type, data, cb) {
            if (!data.trim()) {
                return true;
            }
            var ajax = $.ajax({
                url: '/' + obid.locale + '/api/check/' + type + '/' + data,
                type: 'GET'
            });

            ajax.done(function (resp) {
                cb(resp);
            });
        },
        subscribesNewsLetter: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;
            formData.append('email', this.newsletter.email);

            $.ajax({
                url: '/' + obid.locale + '/api/newsletter',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST',
                statusCode: {
                    422: function (e) {
                        toastr.error("email invalid");
                    }
                }
            }).done(function (data) {
                if (data.subscribed) {
                    swal(
                        "You have been subscribed",
                        "Please save following code and use it for your next purchase: "
                        + data.couponCode,
                        "success"
                    );
                    self.newsletter.email = "";
                } else {
                    swal(
                        "Ooops...",
                        "Your have been subscribed to our news letter.... sorry for that",
                        "error"
                    );
                    self.newsletter.email = "";
                }
            });
        },
        submitNewAccount: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;
            var formRegister = this.formRegister;
            var profile = this.profileRegister;
            formData.append('username', formRegister.username);
            formData.append('email', formRegister.email);
            formData.append('password', formRegister.password);
            formData.append('password_confirmation', formRegister.password_confirm);
            formData.append('nama', formRegister.nama);
            formData.append('nohp', formRegister.nohp);
            formData.append('birthday', profile.date + '-' + profile.month + '-' + profile.year);
            formData.append('alamat', profile.alamat);
            formData.append('kodepos', profile.kodepos);
            formData.append('gender', profile.gender);
            this.submitting = true;
            $.ajax({
                url: '/' + obid.locale + '/register',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                type: 'POST'
            }).done(function (data) {
                if (data.created) {
                    window.location.href = data.redirect;
                }
            });
        }

    },
    ready: function () {
        Layout.init();
        this.getCart();
    }
});
