Vue.use(window['vue-selectize']);

Vue.filter('getText', function (v, i) {
    var result = '';
    i.forEach(function (o) {
        if (o.value == v) {
            result = o.text;
            return false;
        }
    });

    return result;
});

Vue.filter('subTotal', function (data) {
    var total = 0;
    data.forEach(function (d) {
        var discount = 100 - d.options.discount;
        discount = discount / 100;
        var qty = d.qty * 1;
        var price = d.price * 1;
        total += qty * price * discount;
    });

    return total;
});

Vue.filter('applyDiscount', function (d) {
    var discount = 100 - d.options.discount;
    discount = discount / 100;

    return d.price * discount;
});
Vue.filter('sumPrice', function (data) {
    var discount = 100 - data.options.discount;
    discount = discount / 100;

    return data.price * data.qty * discount;
});
var vm = new Vue({
    el: '#shop',
    data: {
        cart: [],
        address: {
            nama: '',
            kota: 0,
            kodepos: '',
            nohp: '',
            alamat: '',
            provinsi: 0,
            negara: 'indonesia'
        },
        newsletter: {
            email: ''
        },
        shippingCost: {service: '', cost: 0},
        shippingList: [],
        voucher: {value: 0},
        submitting: false,
        guest: obid.guest ? 'false' : 'true',
        isGuest: obid.guest,
        payment: '',
        birthdayDiscount: obid.birthday,
        locale: obid.locale,
        currency: obid.locale === 'id' ? 'IDR' : 'USD',
        randTrans: obid.uniqueTrans
    },
    computed: {
        'totalCost': function () {
            var total = 0;
            this.cart.forEach(function (d) {
                var discount = 100 - d.options.discount;
                discount = discount / 100;
                var qty = d.qty * 1;
                var price = d.price * 1;
                total += qty * price * discount;
            });

            if (this.birthdayDiscount) {
                total = total * (100 - 5) / 100;
            }
            total = total + this.shippingCost.cost * 1 - this.voucher.value * 1;
            return total;
        },
        subTotalCart: function () {
            var total = 0;
            this.cart.forEach(function (d) {
                var discount = 100 - d.options.discount;
                discount = discount / 100;
                var qty = d.qty * 1;
                var price = d.price * 1;
                total += qty * price * discount;
            });

            return total;
        },
        validator: function () {
            var nohpReg = /^(0+8*\d)\d+/g;
            return {
                nama: validator.isLength(this.address.nama, 2),
                nohp: nohpReg.test(this.address.nohp),
                kodepos: validator.isNumeric(this.address.kodepos),
                alamat: validator.isLength(this.address.alamat, 4)
            }
        },
        checkoutValid: function () {
            var validator = this.validator;
            return Object.keys(validator).every(function (k) {
                return validator[k];
            });
        },
        'birthdayValue': function () {
            var total = 0;
            this.cart.forEach(function (d) {
                var discount = 100 - d.options.discount;
                discount = discount / 100;
                var qty = d.qty * 1;
                var price = d.price * 1;
                total += qty * price * discount;
            });

            return total * 5 / 100;
        },
        uniqueCost: function () {
            return this.totalCost - this.randTrans;
        }
    },
    methods: {
        getCart: function () {
            var self = this;
            $.get('/' + obid.locale + '/api/cart')
                .done(function (resp) {
                    self.cart = resp.items;
                    self.voucher = resp.coupon;
                });
        },
        addCart: function (e) {
            e.preventDefault();
            var qty = this.$$.qty.value;
            var size = this.$$.size.value;
            var id = this.$$.id.value;
            var self = this;
            var formData = new FormData;
            formData.append('productId', id);
            formData.append('sizeId', size);
            formData.append('qty', qty);
            $.ajax({
                url: '/' + obid.locale + '/api/cart',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST'
            }).done(function (data) {
                self.cart = data.items;
            });
        },
        delItem: function (data) {
            var self = this;
            var formData = new FormData;

            formData.append('rowid', data['rowid']);

            $.ajax({
                url: '/' + obid.locale + '/api/cart/delete',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST'
            }).done(function (data) {
                self.cart = data.items;
            });
        },
        getProvinces: function () {
            var self = this;
            $.get('/' + obid.locale + '/api/provinces')
                .done(function (data) {
                    self.listProvince = data;
                });
        },
        getCities: function (id) {
            var self = this;
            $.get('/' + obid.locale + '/api/provinces/' + id + '/city')
                .done(function (data) {
                    self.listCity = data.map(function (d) {
                        return {
                            value: d.value.id,
                            text: d.text
                        };
                    });
                });
        },
        getOngkir: function (id) {
            var self = this;
            $.get('/' + obid.locale + '/api/ongkir/' + id)
                .done(function (data) {
                    self.shippingList = data;
                    if (data.length > 0) {
                        self.shippingCost = data[0].value;
                    } else {
                        self.shippingCost = {
                            cost: 0,
                            service: "REG"
                        }
                    }
                });
        },
        submitCheckout: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;
            formData.append('nama', this.address.nama);
            formData.append('kota', this.address.kota);
            formData.append('nohp', this.address.nohp * 1);
            formData.append('kodepos', this.address.kodepos * 1);
            formData.append('alamat', this.address.alamat);
            formData.append('negara', this.address.negara);
            formData.append('provinsi', this.address.provinsi);
            formData.append('shippingCost', this.shippingCost.cost * 1);
            formData.append('shippingName', this.shippingCost.service);
            formData.append('payment', this.payment);
            this.submitting = true;

            $.ajax({
                url: '/' + obid.locale + '/checkout',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                type: 'POST'
            }).done(function (data) {
                if (data.created) {
                    window.location.href = data.redirect;
                } else {
                    console.log(data);
                }
            });
        },
        setProfile: function () {
            var self = this;
            $.get('/' + obid.locale + '/api/profile')
                .done(function (data) {
                    self.address.nama = data.nama;
                    self.address.kodepos = data.kodepos;
                    self.address.nohp = data.nohp;
                    self.address.alamat = data.alamat;
                    self.address.kota = data.kota;
                    self.address.provinsi = data.provinsi;
                });
        },
        clearProfile: function () {
            this.address.nama = '';
            this.address.kodepos = '';
            this.address.nohp = '';
            this.address.alamat = '';
        },
        subscribesNewsLetter: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;
            formData.append('email', this.newsletter.email);

            $.ajax({
                url: '/' + obid.locale + '/api/newsletter',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST',
                statusCode: {
                    422: function (e) {
                        toastr.error("email invalid");
                    }
                }
            }).done(function (data) {
                if (data.subscribed) {
                    swal(
                        "You have been subscribed",
                        "Please save following code and use it for your next purchase: "
                        + data.couponCode,
                        "success"
                    );
                    self.newsletter.email = "";
                } else {
                    swal(
                        "Ooops...",
                        "Your have been subscribed to our news letter.... sorry for that",
                        "error"
                    );
                    self.newsletter.email = "";
                }
            });
        }

    },
    ready: function () {
        Layout.init();
        Layout.initNavScrolling();
        if (!this.isGuest) {
            this.setProfile();
        }
        this.getCart();
        $('input[name="payment"]').click(function () {
            $(this).closest('label').tab('show');
        });
    },
    watch: {
        'address.provinsi': function (nv, ov) {
            this.getCities(nv);
        },
        'address.kota': function (nv, ov) {
            this.getOngkir(nv);
        },
        'guest': function (nv, ov) {
            if (nv === ov) {
                return;
            }

            nv === "true" ? this.setProfile() : this.clearProfile();
        },
        'cart': function (nv, ov) {
            if (nv === ov) {
                return;
            }

            if (nv.length < 1) {
                window.location.href = 'http://originalbrands.localapp/cart';
            }
        }
    }
});
