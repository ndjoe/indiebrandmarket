<?php


use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    public function run()
    {
        $categories = [
            'top',
            'bottom',
            'outerwear',
            'shoe',
            'hat',
            'bag',
            'accesory',
            'other'
        ];

        foreach ($categories as $cat) {
            $category = new \OBID\Models\Category;
            $category->text = $cat;

            $category->save();
        }
    }
}