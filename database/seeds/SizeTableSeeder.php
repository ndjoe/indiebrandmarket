<?php


use Illuminate\Database\Seeder;

class SizeTableSeeder extends Seeder
{

    public function run()
    {
        $sizeArray = [
            's',
            'm',
            'l',
            'xl',
            'xxl',
            'xxxl',
            '6',
            '7',
            '7.5',
            '8',
            '8.5',
            '9',
            '9.5',
            '10',
            '10.5',
            '11',
            '11.5',
            '12',
            'allsize'
        ];

        foreach ($sizeArray as $s) {
            $size = new \OBID\Models\Size;

            $size->text = $s;
            $size->save();
        }
    }

}