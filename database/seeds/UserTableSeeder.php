<?php


use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{

    public function run()
    {
        $admin = new \OBID\Models\Role;
        $admin->display_name = 'Admin Master';
        $admin->name = 'admin';
        $admin->description = 'masterweb';
        $admin->save();

        $merchant = new \OBID\Models\Role;
        $merchant->name = 'affiliate';
        $merchant->display_name = 'Affiliate';
        $admin->description = 'Affiliate';
        $merchant->save();

        $member = new \OBID\Models\Role;
        $member->name = 'customer';
        $member->display_name = 'Customer';
        $member->description = 'customer';
        $member->save();

        $cashier = new \OBID\Models\Role;
        $cashier->name = 'cashier';
        $cashier->display_name = 'Cashier';
        $cashier->description = 'cashier';
        $cashier->save();

        $shipper = new \OBID\Models\Role;
        $shipper->name = 'shipper';
        $shipper->display_name = 'Shipper';
        $shipper->description = 'staff';
        $shipper->save();

        $finance = new \OBID\Models\Role;
        $finance->name = 'finance';
        $finance->display_name = 'Finance';
        $finance->description = 'staff';
        $finance->save();

        $inventory = new \OBID\Models\Role;
        $inventory->name = 'inventory';
        $inventory->display_name = 'Inventory';
        $inventory->description = 'staff';
        $inventory->save();

        $purchase = new \OBID\Models\Role;
        $purchase->name = 'purchase';
        $purchase->display_name = 'Purchase';
        $purchase->description = 'staff';
        $purchase->save();

        $stafmerchant = new \OBID\Models\Role;
        $stafmerchant->name = 'staffmerchant';
        $stafmerchant->display_name = 'Staff Merchant';
        $stafmerchant->description = 'staffnya Merchant';
        $stafmerchant->save();

        $userAdmin = new \OBID\Models\User;
        $userAdmin->nama = 'admin';
        $userAdmin->password = bcrypt('admin');
        $userAdmin->username = 'admin';
        $userAdmin->is_active = true;
        $userAdmin->email = 'originalbrands.id@gmail.com';
        $userAdmin->nohp = '0834567';
        $userAdmin->save();
        $userAdmin->attachRole($admin);

        $userInventory = new \OBID\Models\User;
        $userInventory->nama = 'inventory';
        $userInventory->username = 'inventory';
        $userInventory->password = bcrypt('inventory');
        $userInventory->email = 'inventory@inventory.com';
        $userInventory->is_active = true;
        $userInventory->nohp = '08345678';
        $userInventory->save();
        $userInventory->attachRole($inventory);

        $userFinance = new \OBID\Models\User;
        $userFinance->nama = 'finance';
        $userFinance->username = 'finance';
        $userFinance->password = bcrypt('finance');
        $userFinance->email = 'finance.originalbrands@gmail.com';
        $userFinance->is_active = true;
        $userFinance->nohp = '08345679';
        $userFinance->save();
        $userFinance->attachRole($finance);

        $userShipper = new \OBID\Models\User;
        $userShipper->nama = 'shipper';
        $userShipper->username = 'shipper';
        $userShipper->password = bcrypt('shipper');
        $userShipper->email = 'shipper.originalbrands@gmail.com';
        $userShipper->is_active = true;
        $userShipper->nohp = '08345672';
        $userShipper->save();
        $userShipper->attachRole($shipper);

        $userPurchase = new \OBID\Models\User;
        $userPurchase->nama = 'purchase';
        $userPurchase->username = 'purchase';
        $userPurchase->password = bcrypt('purchase');
        $userPurchase->email = 'purchase.originalbrands@gmail.com';
        $userPurchase->is_active = true;
        $userPurchase->nohp = '123456722';
        $userPurchase->save();
        $userPurchase->attachRole($purchase);

        $array = ['stevemeier', 'ordinaryuser'];

        foreach ($array as $name) {
            $user = new \OBID\Models\User;

            $user->username = $name;
            $user->nama = $name;
            $user->password = bcrypt($name);
            $user->activation_code = null;
            $user->is_active = true;
            $user->nohp = '08' . mt_rand(1, 9) . mt_rand(1, 9) . mt_rand(1, 9);
            $user->email = $name . '@' . $name . '.com';

            switch ($name) {
                case 'admin':
                    $profile = new \OBID\Models\UserProfile;
                    $profile->alamat = 'sadasd';
                    $profile->gender = 'male';
                    $profile->ongkir_city_id = 1;
                    $profile->ongkir_province_id = 1;
                    $profile->save();
                    $profile->user()->save($user);
                    $user->attachRole($admin);
                    break;
                case 'stevemeier':
                    $profile = new \OBID\Models\MerchantProfile;
                    $profile->alamat = 'dasdasdas';
                    $profile->bank = 'bca';
                    $profile->kota = 'bandung';
                    $profile->norek = '2131231';
                    $profile->owner_name = 'kampret';
                    $profile->bank_owner = 'kampret';
                    $profile->provinsi = 'jawa barat';
                    $profile->banner_image = 'default-banner.jpg';
                    $profile->logo = 'default-logo.png';
                    $profile->kodepos = '123123';
                    $profile->save();
                    $profile->user()->save($user);
                    $user->attachRole($merchant);
                    $cashierProfile = new \OBID\Models\CashierProfile;
                    $cashierProfile->merchant_id = $user->id;
                    $cashierProfile->save();
                    $cashierUser = new \OBID\Models\User;
                    $cashierUser->nama = 'cashiersteve';
                    $cashierUser->nohp = '0865332';
                    $cashierUser->username = 'cashiersteve';
                    $cashierUser->email = 'steve@steve.com';
                    $cashierUser->password = bcrypt('cashiersteve');
                    $cashierProfile->user()->save($cashierUser);
                    $cashierUser->attachRole($cashier);
                    $posCash = new \OBID\Models\PosCash;
                    $posCash->cash = 0;
                    $posCash->merchant_id = $user->id;
                    $posCash->save();
                    $salesNotif = new \OBID\Models\SalesNotification;
                    $salesNotif->user_id = $user->id;
                    $salesNotif->notif_count = 0;
                    $salesNotif->save();
                    break;
                default:
                    $profile = new \OBID\Models\UserProfile;
                    $user->email = 'tanjung.perdana@outlook.com';
                    $profile->save();
                    $profile->user()->save($user);
                    $user->attachRole($member);
            }
        }
    }

}