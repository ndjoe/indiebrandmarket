<?php


use Illuminate\Database\Seeder;

class ColorTableSeeder extends Seeder
{
    public function run()
    {
        $colors = ['red', 'yellow', 'blue', 'grey', 'navy', 'black'];

        foreach ($colors as $c) {
            $color = new \OBID\Models\Color;
            $color->text = $c;
            $color->save();
        }
    }
}