<?php

use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    public function run()
    {
        $merchant = \OBID\Models\Role::whereName('affiliate')->first();
        $cashier = \OBID\Models\Role::whereName('cashier')->first();
        $products = \OBID\Models\Product::all();
        for ($i = 1; $i < 30; $i++) {
            $user = new \OBID\Models\User;
            $user->username = 'brand' . str_random(3) . $i;
            $user->nama = 'brand' . $i;
            $user->password = bcrypt('brand' . $i);
            $user->activation_code = null;
            $user->is_active = true;
            $user->nohp = '08' . mt_rand(1, 9) . mt_rand(1, 9) . mt_rand(1, 9) . $i;
            $user->email = 'brand' . $i . '@' . 'brand' . str_random(3) . $i . '.com';
            $profile = new \OBID\Models\MerchantProfile;
            $profile->alamat = 'dasdasdas';
            $profile->bank = 'bca';
            $profile->kota = 'bandung';
            $profile->norek = '2131231';
            $profile->owner_name = 'kampret';
            $profile->bank_owner = 'kampret';
            $profile->provinsi = 'jawa barat';
            $profile->banner_image = 'default-banner.jpg';
            $profile->logo = 'default-logo.png';
            $profile->kodepos = '123123';
            $profile->save();
            $profile->user()->save($user);
            $user->attachRole($merchant);
            $cashierProfile = new \OBID\Models\CashierProfile;
            $cashierProfile->merchant_id = $user->id;
            $cashierProfile->save();
            $cashierUser = new \OBID\Models\User;
            $cashierUser->nama = 'cashierbrand' . $i;
            $cashierUser->nohp = '0865332' . mt_rand(1, 9999);
            $cashierUser->username = 'cashierbrand' . $i;
            $cashierUser->email = 'steve@steve' . str_random(16) . 'com';
            $cashierUser->password = bcrypt('cashierbrand' . $i);
            $cashierProfile->user()->save($cashierUser);
            $cashierUser->attachRole($cashier);
            $posCash = new \OBID\Models\PosCash;
            $posCash->cash = 0;
            $posCash->merchant_id = $user->id;
            $posCash->save();
            $salesNotif = new \OBID\Models\SalesNotification;
            $salesNotif->user_id = $user->id;
            $salesNotif->notif_count = 0;
            $salesNotif->save();

            for ($k = 1; $k < 3; $k++) {
                $product = $products->shuffle()->first();

                $newProduct = new \OBID\Models\Product;

                $newProduct->category_id = $product->category_id;
                $newProduct->subcategory_id = $product->subcategory_id;
                $newProduct->slug = $product->slug . $k . $user->id;
                $newProduct->price = $product->price;
                $newProduct->name = $product->name;
                $newProduct->published_at = $product->published_at;
                $newProduct->color_id = $product->color_id;
                $newProduct->description = $product->description;
                $newProduct->weight = $product->weight;
                $newProduct->author_id = $user->id;
                $newProduct->save();

                $newDiscount = new \OBID\Models\Discount;
                $newDiscount->product_id = $newProduct->id;
                $newDiscount->value = 0;
                $newDiscount->expired_at = \Carbon\Carbon::now();

                $product->images->each(function (\OBID\Models\ProductImage $i) use ($newProduct) {
                    $image = new \OBID\Models\ProductImage;
                    $image->product_id = $newProduct->id;
                    $image->name = $i->name;
                    $image->type = $i->type;
                    $image->save();
                });
            }
        }
    }
}