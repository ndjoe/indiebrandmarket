<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);

        $this->call(UserTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(SizeTableSeeder::class);
        $this->call(ColorTableSeeder::class);
        $this->call(SliderTableSeeder::class);
//        $this->call(ProductTableSeeder::class);
        $curreny = new \OBID\Models\Currency;
        $curreny->value = 12000;
        $curreny->save();
//        $this->call(BrandSeeder::class);
        Model::reguard();
    }
}
