<?php


use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{

    public function run()
    {
        $user = \OBID\Models\User::whereUsername('stevemeier')->first();

        $subcategories = \OBID\Models\Subcategory::all();
        $colors = \OBID\Models\Color::all();
        $products = factory(\OBID\Models\Product::class, 10)->make();

        $products->each(function (\OBID\Models\Product $p) use ($user, $subcategories, $colors) {
            $p->author()->associate($user);
            $p->subcategory()->associate($subcategories->random());
            $p->color()->associate($colors->random());
            $p->save();
        });
    }
}