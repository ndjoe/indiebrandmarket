<?php


use Illuminate\Database\Seeder;

class SliderTableSeeder extends Seeder
{
    public function run()
    {
        $slide = new \OBID\Models\Slider;
        $slide->image = 'slide1.jpg';
        $slide->save();

        $slide1 = new \OBID\Models\Slider;
        $slide1->image = 'slide2.jpg';
        $slide1->save();

        $slide2 = new \OBID\Models\Slider;
        $slide2->save();

        $slide3 = new \OBID\Models\Slider;
        $slide3->save();

        $slide4 = new \OBID\Models\Slider;
        $slide4->save();
    }
}