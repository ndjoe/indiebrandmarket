<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(OBID\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(\OBID\Models\Product::class, function (\Faker\Generator $faker) {
    $name = $faker->sentence(2);
    return [
        'name' => $name,
        'price' => $faker->numberBetween(100000, 1000000),
        'desc' => $faker->paragraph,
        'slug' => \Illuminate\Support\Str::slug($name),
        'image' => $faker->image(base_path() . '/public/images/catalog', 300, 400, 'fashion', false)
    ];
});
