<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('author_id');
            $table->string('name');
            $table->decimal('price', 50, 2);
            $table->unsignedInteger('subcategory_id');
            $table->unsignedInteger('category_id');
            $table->dateTime('published_at')->nullable();
            $table->integer('discount')->default(0);
            $table->float('weight');
            $table->unsignedInteger('color_id');
            $table->text('description')->nullable();
            $table->string('gender')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
