<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kodepos')->nullable();
            $table->string('kota')->nullable();
            $table->string('gender')->nullable();
            $table->date('birthday')->nullable();
            $table->string('provinsi')->nullable();
            $table->text('alamat')->nullable();
            $table->unsignedInteger('ongkir_province_id')->nullable();
            $table->unsignedInteger('ongkir_city_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_profiles');
    }
}
