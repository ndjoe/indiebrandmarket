<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('alamat');
            $table->string('kota');
            $table->string('provinsi');
            $table->string('owner_name');
            $table->string('kodepos');
            $table->string('bank');
            $table->string('bank_owner');
            $table->string('norek');
            $table->string('banner_image');
            $table->string('logo');
            $table->text('about')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('merchant_profiles');
    }
}
