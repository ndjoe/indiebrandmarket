<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->enum('type', ['offline', 'online'])->default('online');
            $table->decimal('grossTotal', 50, 2);
            $table->decimal('taxTotal', 50, 2)->default(0);
            $table->decimal('birthdayAdjusment', 50, 2)->default(0);
            $table->decimal('adjustmentCoupon', 50, 2)->default(0);
            $table->decimal('shippingCost', 50, 2)->default(0);
            $table->decimal('netTotal', 50, 2);
            $table->decimal('uniqueCost', 5, 2);
            $table->dateTime('confirmed_at')->nullable();
            $table->dateTime('expired_at')->nullable();
            $table->dateTime('cancelled_at')->nullable();
            $table->string('payment_method')->nullable();
            $table->float('total_weight');
            $table->string('currency')->default('IDR');
            $table->decimal('currency_value', 50, 2)->default(0);
            $table->string('shippingType')->nullable();
            $table->string('order_code');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
