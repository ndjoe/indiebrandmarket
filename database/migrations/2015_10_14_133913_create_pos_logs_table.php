<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('event');
            $table->decimal('transaksi', 10, 4)->nullable();
            $table->decimal('pos_cash', 10, 4);
            $table->unsignedInteger('merchant_id');
            $table->unsignedInteger('cashier_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pos_logs');
    }
}
