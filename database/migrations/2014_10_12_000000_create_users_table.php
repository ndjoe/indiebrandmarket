<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('nohp')->unique();
            $table->unsignedBigInteger('profileable_id')->nullable();
            $table->string('profileable_type')->nullable();
            $table->boolean('is_cashier')->default(false);
            $table->unsignedBigInteger('merchant_id')->nullable();
            $table->boolean('is_active')->default(false);
            $table->string('activation_code')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
