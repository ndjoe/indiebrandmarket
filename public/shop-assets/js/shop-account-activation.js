new Vue({
    el: '#shop',
    data: {
        cart: [],
        counter: obid.counter ? obid.counter : 0,
        sent: false,
        currency: obid.locale === 'id' ? 'IDR' : 'USD',
        locale: obid.locale,
        newsletter: {
            email: ''
        }
    },
    computed: {},
    methods: {
        getCart: function () {
            var self = this;
            $.get('/' + obid.locale + '/api/cart')
                .done(function (resp) {
                    self.cart = resp.items;
                });
        },
        addCart: function (e) {
            e.preventDefault();
            var qty = this.$$.qty.value;
            var size = this.$$.size.value;
            var id = this.$$.id.value;
            var self = this;
            var formData = new FormData;
            formData.append('productId', id);
            formData.append('sizeId', size);
            formData.append('qty', qty);
            $.ajax({
                url: '/' + obid.locale + '/api/cart',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                aync: true,
                type: 'POST'
            }).done(function (data) {
                self.cart = data;
            });
        },
        delItem: function (data) {
            var self = this;
            var formData = new FormData;

            formData.append('rowid', data['rowid']);


            $.ajax({
                url: '/' + obid.locale + '/api/cart/delete',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST'
            }).done(function (data) {
                self.cart = data.items;
            });
        },
        resendCode: function () {
            var self = this;
            $.get('/' + obid.locale + '/api/resend-code')
                .done(function () {
                    self.sent = true;
                });
        },
        subscribesNewsLetter: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;
            formData.append('email', this.newsletter.email);

            $.ajax({
                url: '/' + obid.locale + '/api/newsletter',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST',
                statusCode: {
                    422: function (e) {
                        toastr.error("email invalid");
                    }
                }
            }).done(function (data) {
                if (data.subscribed) {
                    swal(
                        "You have been subscribed",
                        "Please save following code and use it for your next purchase: "
                        + data.couponCode,
                        "success"
                    );
                    self.newsletter.email = "";
                } else {
                    swal(
                        "Ooops...",
                        "Your have been subscribed to our news letter.... sorry for that",
                        "error"
                    );
                    self.newsletter.email = "";
                }
            });
        }

    },
    ready: function () {
        var self = this;
        Layout.init();
        Layout.initTouchspin();
        Layout.initTwitter();
        //Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();
        this.getCart();
        if (this.counter > 0) {
            var count = setInterval(function () {
                self.counter--;
                if (self.counter < 0) {
                    clearInterval(count);
                }
            }, 1000);
        }
    }
});

//# sourceMappingURL=shop-account-activation.js.map
