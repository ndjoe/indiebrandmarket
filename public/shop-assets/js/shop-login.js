/*!
 * Copyright (c) 2015 Chris O'Hara <cohara87@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
!function (t, e) {
    "undefined" != typeof exports && "undefined" != typeof module ? module.exports = e() : "function" == typeof define && "object" == typeof define.amd ? define(e) : this[t] = e()
}("validator", function (t) {
    "use strict";
    function e(t, e) {
        t = t || {};
        for (var r in e)"undefined" == typeof t[r] && (t[r] = e[r]);
        return t
    }

    function r(t) {
        var e = "(\\" + t.symbol.replace(/\./g, "\\.") + ")" + (t.require_symbol ? "" : "?"), r = "-?", n = "[1-9]\\d*", i = "[1-9]\\d{0,2}(\\" + t.thousands_separator + "\\d{3})*", o = ["0", n, i], u = "(" + o.join("|") + ")?", s = "(\\" + t.decimal_separator + "\\d{2})?", a = u + s;
        return t.allow_negatives && !t.parens_for_negatives && (t.negative_sign_after_digits ? a += r : t.negative_sign_before_digits && (a = r + a)), t.allow_negative_sign_placeholder ? a = "( (?!\\-))?" + a : t.allow_space_after_symbol ? a = " ?" + a : t.allow_space_after_digits && (a += "( (?!$))?"), t.symbol_after_digits ? a += e : a = e + a, t.allow_negatives && (t.parens_for_negatives ? a = "(\\(" + a + "\\)|" + a + ")" : t.negative_sign_before_digits || t.negative_sign_after_digits || (a = r + a)), new RegExp("^(?!-? )(?=.*\\d)" + a + "$")
    }

    t = {version: "4.0.5"};
    var n = /^[a-z\d!#\$%&'\*\+\-\/=\?\^_`{\|}~]+$/i, i = /^([\s\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e]|(\\[\x01-\x09\x0b\x0c\x0d-\x7f]))*$/i, o = /^[a-z\d!#\$%&'\*\+\-\/=\?\^_`{\|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+$/i, u = /^([\s\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|(\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*$/i, s = /^[a-z\d!#\$%&'\*\+\-\/=\?\^_`{\|}~\.\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+[a-z\d!#\$%&'\*\+\-\/=\?\^_`{\|}~\.\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF\s]*<(.+)>$/i, a = /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/, l = /^[A-Z]{2}[0-9A-Z]{9}[0-9]$/, f = /^(?:[0-9]{9}X|[0-9]{10})$/, c = /^(?:[0-9]{13})$/, p = /^(\d+)\.(\d+)\.(\d+)\.(\d+)$/, g = /^[0-9A-F]{1,4}$/i, F = {
        3: /^[0-9A-F]{8}-[0-9A-F]{4}-3[0-9A-F]{3}-[0-9A-F]{4}-[0-9A-F]{12}$/i,
        4: /^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i,
        5: /^[0-9A-F]{8}-[0-9A-F]{4}-5[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i,
        all: /^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$/i
    }, d = /^[A-Z]+$/i, _ = /^[0-9A-Z]+$/i, x = /^[-+]?[0-9]+$/, h = /^(?:[-+]?(?:0|[1-9][0-9]*))$/, v = /^(?:[-+]?(?:[0-9]+))?(?:\.[0-9]*)?(?:[eE][\+\-]?(?:[0-9]+))?$/, A = /^[0-9A-F]+$/i, m = /^[-+]?([0-9]+|\.[0-9]+|[0-9]+\.[0-9]+)$/, $ = /^#?([0-9A-F]{3}|[0-9A-F]{6})$/i, w = /^[\x00-\x7F]+$/, b = /[^\x00-\x7F]/, D = /[^\u0020-\u007E\uFF61-\uFF9F\uFFA0-\uFFDC\uFFE8-\uFFEE0-9a-zA-Z]/, y = /[\u0020-\u007E\uFF61-\uFF9F\uFFA0-\uFFDC\uFFE8-\uFFEE0-9a-zA-Z]/, E = /[\uD800-\uDBFF][\uDC00-\uDFFF]/, I = /^(?:[A-Z0-9+\/]{4})*(?:[A-Z0-9+\/]{2}==|[A-Z0-9+\/]{3}=|[A-Z0-9+\/]{4})$/i, O = {
        "zh-CN": /^(\+?0?86\-?)?1[345789]\d{9}$/,
        "en-ZA": /^(\+?27|0)\d{9}$/,
        "en-AU": /^(\+?61|0)4\d{8}$/,
        "en-HK": /^(\+?852\-?)?[569]\d{3}\-?\d{4}$/,
        "fr-FR": /^(\+?33|0)[67]\d{8}$/,
        "pt-PT": /^(\+351)?9[1236]\d{7}$/,
        "el-GR": /^(\+30)?((2\d{9})|(69\d{8}))$/,
        "en-GB": /^(\+?44|0)7\d{9}$/,
        "en-US": /^(\+?1)?[2-9]\d{2}[2-9](?!11)\d{6}$/,
        "en-ZM": /^(\+26)?09[567]\d{7}$/,
        "ru-RU": /^(\+?7|8)?9\d{9}$/
    }, C = /^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/;
    t.extend = function (e, r) {
        t[e] = function () {
            var e = Array.prototype.slice.call(arguments);
            return e[0] = t.toString(e[0]), r.apply(t, e)
        }
    }, t.init = function () {
        for (var e in t)"function" == typeof t[e] && "toString" !== e && "toDate" !== e && "extend" !== e && "init" !== e && t.extend(e, t[e])
    }, t.toString = function (t) {
        return "object" == typeof t && null !== t && t.toString ? t = t.toString() : null === t || "undefined" == typeof t || isNaN(t) && !t.length ? t = "" : "string" != typeof t && (t += ""), t
    }, t.toDate = function (t) {
        return "[object Date]" === Object.prototype.toString.call(t) ? t : (t = Date.parse(t), isNaN(t) ? null : new Date(t))
    }, t.toFloat = function (t) {
        return parseFloat(t)
    }, t.toInt = function (t, e) {
        return parseInt(t, e || 10)
    }, t.toBoolean = function (t, e) {
        return e ? "1" === t || "true" === t : "0" !== t && "false" !== t && "" !== t
    }, t.equals = function (e, r) {
        return e === t.toString(r)
    }, t.contains = function (e, r) {
        return e.indexOf(t.toString(r)) >= 0
    }, t.matches = function (t, e, r) {
        return "[object RegExp]" !== Object.prototype.toString.call(e) && (e = new RegExp(e, r)), e.test(t)
    };
    var S = {allow_display_name: !1, allow_utf8_local_part: !0, require_tld: !0};
    t.isEmail = function (r, a) {
        if (a = e(a, S), a.allow_display_name) {
            var l = r.match(s);
            l && (r = l[1])
        }
        var f = r.split("@"), c = f.pop(), p = f.join("@"), g = c.toLowerCase();
        if (("gmail.com" === g || "googlemail.com" === g) && (p = p.replace(/\./g, "").toLowerCase()), !t.isByteLength(p, 0, 64) || !t.isByteLength(c, 0, 256))return !1;
        if (!t.isFQDN(c, {require_tld: a.require_tld}))return !1;
        if ('"' === p[0])return p = p.slice(1, p.length - 1), a.allow_utf8_local_part ? u.test(p) : i.test(p);
        for (var F = a.allow_utf8_local_part ? o : n, d = p.split("."), _ = 0; _ < d.length; _++)if (!F.test(d[_]))return !1;
        return !0
    };
    var N = {
        protocols: ["http", "https", "ftp"],
        require_tld: !0,
        require_protocol: !1,
        require_valid_protocol: !0,
        allow_underscores: !1,
        allow_trailing_dot: !1,
        allow_protocol_relative_urls: !1
    };
    t.isURL = function (r, n) {
        if (!r || r.length >= 2083 || /\s/.test(r))return !1;
        if (0 === r.indexOf("mailto:"))return !1;
        n = e(n, N);
        var i, o, u, s, a, l, f;
        if (f = r.split("://"), f.length > 1) {
            if (i = f.shift(), n.require_valid_protocol && -1 === n.protocols.indexOf(i))return !1
        } else {
            if (n.require_protocol)return !1;
            n.allow_protocol_relative_urls && "//" === r.substr(0, 2) && (f[0] = r.substr(2))
        }
        return r = f.join("://"), f = r.split("#"), r = f.shift(), f = r.split("?"), r = f.shift(), f = r.split("/"), r = f.shift(), f = r.split("@"), f.length > 1 && (o = f.shift(), o.indexOf(":") >= 0 && o.split(":").length > 2) ? !1 : (s = f.join("@"), f = s.split(":"), u = f.shift(), f.length && (l = f.join(":"), a = parseInt(l, 10), !/^[0-9]+$/.test(l) || 0 >= a || a > 65535) ? !1 : t.isIP(u) || t.isFQDN(u, n) || "localhost" === u ? n.host_whitelist && -1 === n.host_whitelist.indexOf(u) ? !1 : n.host_blacklist && -1 !== n.host_blacklist.indexOf(u) ? !1 : !0 : !1)
    }, t.isIP = function (e, r) {
        if (r = t.toString(r), !r)return t.isIP(e, 4) || t.isIP(e, 6);
        if ("4" === r) {
            if (!p.test(e))return !1;
            var n = e.split(".").sort(function (t, e) {
                return t - e
            });
            return n[3] <= 255
        }
        if ("6" === r) {
            var i = e.split(":"), o = !1, u = t.isIP(i[i.length - 1], 4), s = u ? 7 : 8;
            if (i.length > s)return !1;
            if ("::" === e)return !0;
            "::" === e.substr(0, 2) ? (i.shift(), i.shift(), o = !0) : "::" === e.substr(e.length - 2) && (i.pop(), i.pop(), o = !0);
            for (var a = 0; a < i.length; ++a)if ("" === i[a] && a > 0 && a < i.length - 1) {
                if (o)return !1;
                o = !0
            } else if (u && a == i.length - 1); else if (!g.test(i[a]))return !1;
            return o ? i.length >= 1 : i.length === s
        }
        return !1
    };
    var B = {require_tld: !0, allow_underscores: !1, allow_trailing_dot: !1};
    t.isFQDN = function (t, r) {
        r = e(r, B), r.allow_trailing_dot && "." === t[t.length - 1] && (t = t.substring(0, t.length - 1));
        var n = t.split(".");
        if (r.require_tld) {
            var i = n.pop();
            if (!n.length || !/^([a-z\u00a1-\uffff]{2,}|xn[a-z0-9-]{2,})$/i.test(i))return !1
        }
        for (var o, u = 0; u < n.length; u++) {
            if (o = n[u], r.allow_underscores) {
                if (o.indexOf("__") >= 0)return !1;
                o = o.replace(/_/g, "")
            }
            if (!/^[a-z\u00a1-\uffff0-9-]+$/i.test(o))return !1;
            if (/[\uff01-\uff5e]/.test(o))return !1;
            if ("-" === o[0] || "-" === o[o.length - 1] || o.indexOf("---") >= 0)return !1
        }
        return !0
    }, t.isBoolean = function (t) {
        return ["true", "false", "1", "0"].indexOf(t) >= 0
    }, t.isAlpha = function (t) {
        return d.test(t)
    }, t.isAlphanumeric = function (t) {
        return _.test(t)
    }, t.isNumeric = function (t) {
        return x.test(t)
    }, t.isDecimal = function (t) {
        return "" !== t && m.test(t)
    }, t.isHexadecimal = function (t) {
        return A.test(t)
    }, t.isHexColor = function (t) {
        return $.test(t)
    }, t.isLowercase = function (t) {
        return t === t.toLowerCase()
    }, t.isUppercase = function (t) {
        return t === t.toUpperCase()
    }, t.isInt = function (t, e) {
        return e = e || {}, h.test(t) && (!e.hasOwnProperty("min") || t >= e.min) && (!e.hasOwnProperty("max") || t <= e.max)
    }, t.isFloat = function (t, e) {
        return e = e || {}, "" !== t && v.test(t) && (!e.hasOwnProperty("min") || t >= e.min) && (!e.hasOwnProperty("max") || t <= e.max)
    }, t.isDivisibleBy = function (e, r) {
        return t.toFloat(e) % t.toInt(r) === 0
    }, t.isNull = function (t) {
        return 0 === t.length
    }, t.isLength = function (t, e, r) {
        var n = t.match(/[\uD800-\uDBFF][\uDC00-\uDFFF]/g) || [], i = t.length - n.length;
        return i >= e && ("undefined" == typeof r || r >= i)
    }, t.isByteLength = function (t, e, r) {
        var n = encodeURI(t).split(/%..|./).length - 1;
        return n >= e && ("undefined" == typeof r || r >= n)
    }, t.isUUID = function (t, e) {
        var r = F[e ? e : "all"];
        return r && r.test(t)
    }, t.isDate = function (t) {
        return !isNaN(Date.parse(t))
    }, t.isAfter = function (e, r) {
        var n = t.toDate(r || new Date), i = t.toDate(e);
        return !!(i && n && i > n)
    }, t.isBefore = function (e, r) {
        var n = t.toDate(r || new Date), i = t.toDate(e);
        return i && n && n > i
    }, t.isIn = function (e, r) {
        var n;
        if ("[object Array]" === Object.prototype.toString.call(r)) {
            var i = [];
            for (n in r)i[n] = t.toString(r[n]);
            return i.indexOf(e) >= 0
        }
        return "object" == typeof r ? r.hasOwnProperty(e) : r && "function" == typeof r.indexOf ? r.indexOf(e) >= 0 : !1
    }, t.isCreditCard = function (t) {
        var e = t.replace(/[^0-9]+/g, "");
        if (!a.test(e))return !1;
        for (var r, n, i, o = 0, u = e.length - 1; u >= 0; u--)r = e.substring(u, u + 1), n = parseInt(r, 10), i ? (n *= 2, o += n >= 10 ? n % 10 + 1 : n) : o += n, i = !i;
        return !!(o % 10 === 0 ? e : !1)
    }, t.isISIN = function (t) {
        if (!l.test(t))return !1;
        for (var e, r, n = t.replace(/[A-Z]/g, function (t) {
            return parseInt(t, 36)
        }), i = 0, o = !0, u = n.length - 2; u >= 0; u--)e = n.substring(u, u + 1), r = parseInt(e, 10), o ? (r *= 2, i += r >= 10 ? r + 1 : r) : i += r, o = !o;
        return parseInt(t.substr(t.length - 1), 10) === (1e4 - i) % 10
    }, t.isISBN = function (e, r) {
        if (r = t.toString(r), !r)return t.isISBN(e, 10) || t.isISBN(e, 13);
        var n, i = e.replace(/[\s-]+/g, ""), o = 0;
        if ("10" === r) {
            if (!f.test(i))return !1;
            for (n = 0; 9 > n; n++)o += (n + 1) * i.charAt(n);
            if (o += "X" === i.charAt(9) ? 100 : 10 * i.charAt(9), o % 11 === 0)return !!i
        } else if ("13" === r) {
            if (!c.test(i))return !1;
            var u = [1, 3];
            for (n = 0; 12 > n; n++)o += u[n % 2] * i.charAt(n);
            if (i.charAt(12) - (10 - o % 10) % 10 === 0)return !!i
        }
        return !1
    }, t.isMobilePhone = function (t, e) {
        return e in O ? O[e].test(t) : !1
    };
    var j = {
        symbol: "$",
        require_symbol: !1,
        allow_space_after_symbol: !1,
        symbol_after_digits: !1,
        allow_negatives: !0,
        parens_for_negatives: !1,
        negative_sign_before_digits: !1,
        negative_sign_after_digits: !1,
        allow_negative_sign_placeholder: !1,
        thousands_separator: ",",
        decimal_separator: ".",
        allow_space_after_digits: !1
    };
    t.isCurrency = function (t, n) {
        return n = e(n, j), r(n).test(t)
    }, t.isJSON = function (t) {
        try {
            var e = JSON.parse(t);
            return !!e && "object" == typeof e
        } catch (r) {
        }
        return !1
    }, t.isMultibyte = function (t) {
        return b.test(t)
    }, t.isAscii = function (t) {
        return w.test(t)
    }, t.isFullWidth = function (t) {
        return D.test(t)
    }, t.isHalfWidth = function (t) {
        return y.test(t)
    }, t.isVariableWidth = function (t) {
        return D.test(t) && y.test(t)
    }, t.isSurrogatePair = function (t) {
        return E.test(t)
    }, t.isBase64 = function (t) {
        return I.test(t)
    }, t.isMongoId = function (e) {
        return t.isHexadecimal(e) && 24 === e.length
    }, t.isISO8601 = function (t) {
        return C.test(t)
    }, t.ltrim = function (t, e) {
        var r = e ? new RegExp("^[" + e + "]+", "g") : /^\s+/g;
        return t.replace(r, "")
    }, t.rtrim = function (t, e) {
        var r = e ? new RegExp("[" + e + "]+$", "g") : /\s+$/g;
        return t.replace(r, "")
    }, t.trim = function (t, e) {
        var r = e ? new RegExp("^[" + e + "]+|[" + e + "]+$", "g") : /^\s+|\s+$/g;
        return t.replace(r, "")
    }, t.escape = function (t) {
        return t.replace(/&/g, "&amp;").replace(/"/g, "&quot;").replace(/'/g, "&#x27;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/\//g, "&#x2F;").replace(/\`/g, "&#96;")
    }, t.stripLow = function (e, r) {
        var n = r ? "\\x00-\\x09\\x0B\\x0C\\x0E-\\x1F\\x7F" : "\\x00-\\x1F\\x7F";
        return t.blacklist(e, n)
    }, t.whitelist = function (t, e) {
        return t.replace(new RegExp("[^" + e + "]+", "g"), "")
    }, t.blacklist = function (t, e) {
        return t.replace(new RegExp("[" + e + "]+", "g"), "")
    };
    var q = {lowercase: !0};
    return t.normalizeEmail = function (r, n) {
        if (n = e(n, q), !t.isEmail(r))return !1;
        var i = r.split("@", 2);
        if (i[1] = i[1].toLowerCase(), "gmail.com" === i[1] || "googlemail.com" === i[1]) {
            if (i[0] = i[0].toLowerCase().replace(/\./g, ""), "+" === i[0][0])return !1;
            i[0] = i[0].split("+")[0], i[1] = "gmail.com"
        } else n.lowercase && (i[0] = i[0].toLowerCase());
        return i.join("@")
    }, t.init(), t
});

Vue.use(window['vue-selectize']);

Vue.component('modal', {
    template: '#modal',
    props: ['show']
});

new Vue({
    el: '#shop',
    data: {
        cart: [],
        formLogin: {
            username: '',
            password: ''
        },
        formRegister: {
            nama: '',
            username: '',
            password: '',
            password_confirm: '',
            email: '',
            nohp: ''
        },
        profileRegister: {
            month: 0,
            year: 0,
            date: 0,
            alamat: '',
            kodepos: '',
            provinsi: '',
            kota: '',
            gender: 'unknown'
        },
        usernameCheck: true,
        nohpCheck: true,
        emailCheck: true,
        currency: obid.locale === 'id' ? 'IDR' : 'USD',
        locale: obid.locale,
        newsletter: {
            email: ''
        },
        showProfileModal: false,
        submitting: false
    },
    computed: {
        validator: function () {
            var self = this;
            var nohpReg = /^(0+8*\d)\d+/g;
            return {
                formLogin: {
                    username: {
                        required: validator.isLength(this.formLogin.username, 3)
                    },
                    password: {
                        required: validator.isLength(this.formLogin.password, 3)
                    }
                },
                formRegister: {
                    username: this.formRegister.username.trim() ? validator.isLength(this.formRegister.username, 3) && validator.isAlphanumeric(this.formRegister.username) : true,
                    email: this.formRegister.email.trim() ? validator.isEmail(this.formRegister.email) : true,
                    password: this.formRegister.password.trim() ? validator.isLength(this.formRegister.password, 6) : true,
                    password_confirm: this.formRegister.password_confirm.trim() ? validator.equals(this.formRegister.password_confirm, this.formRegister.password) : true,
                    nama: this.formRegister.nama.trim() ? validator.isLength(this.formRegister.nama, 2) : true,
                    nohp: this.formRegister.nohp.trim() ? nohpReg.test(this.formRegister.nohp) : true,
                    mustBeFilled: (
                        this.formRegister.username.trim()
                        && this.formRegister.email.trim()
                        && this.formRegister.password.trim()
                        && this.formRegister.password_confirm.trim()
                        && this.formRegister.nama.trim()
                        && this.formRegister.nohp.trim()
                    ),
                    checkUsername: this.usernameCheck,
                    checkEmail: this.emailCheck,
                    checkNohp: this.nohpCheck,
                    kodepos: this.profileRegister.kodepos.trim() ? validator.isNumeric(this.profileRegister.kodepos)
                    && validator.isLength(this.profileRegister.kodepos, 5, 5) : true
                }
            }
        },
        formRegisterValid: function () {
            var validator = this.validator;
            return this.usernameCheck
                && this.emailCheck
                && this.nohpCheck
                && Object.keys(validator.formRegister).every(function (k) {
                    return validator.formRegister[k];
                });
        },
        listYear: function () {
            var years = [];
            for (var i = new Date().getFullYear() - 10; i > 1950; i--) {
                years.push({
                    value: i,
                    text: i
                });
            }

            return years;
        },
        listMonth: function () {
            var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var list = [];
            for (var i = 1; i < months.length + 1; i++) {
                list.push({
                    value: i,
                    text: months[i - 1]
                });
            }

            return list;
        },
        listDate: function () {
            var month = this.profileRegister.month;
            var year = this.profileRegister.year;
            if (month === 0 || year === 0) {
                return [];
            }
            var date = new Date(year, month + 1, 0).getDate();
            var list = [];

            for (var i = 1; i < date; i++) {
                list.push({
                    value: i,
                    text: i
                });
            }

            return list;
        }
    },
    watch: {
        'formRegister.username': function (nv, ov) {
            var self = this;
            this.check('username', nv, function (data) {
                self.usernameCheck = !data.exists;
            });
        },
        'formRegister.email': function (nv, ov) {
            var self = this;
            this.check('email', nv, function (data) {
                self.emailCheck = !data.exists;
            });
        },
        'formRegister.nohp': function (nv, ov) {
            var self = this;
            this.check('nohp', nv, function (data) {
                self.nohpCheck = !data.exists;
            });
        }
    },
    methods: {
        getCart: function () {
            var self = this;
            $.get('/' + obid.locale + '/api/cart')
                .done(function (resp) {
                    self.cart = resp.items;
                });
        }
        ,
        delItem: function (data) {
            var self = this;
            var formData = new FormData;

            formData.append('rowid', data['rowid']);

            $.ajax({
                url: '/' + obid.locale + '/api/cart/delete',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST'
            }).done(function (data) {
                self.cart = data.items;
            });
        },
        check: function (type, data, cb) {
            if (!data.trim()) {
                return true;
            }
            var ajax = $.ajax({
                url: '/' + obid.locale + '/api/check/' + type + '/' + data,
                type: 'GET'
            });

            ajax.done(function (resp) {
                cb(resp);
            });
        },
        subscribesNewsLetter: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;
            formData.append('email', this.newsletter.email);

            $.ajax({
                url: '/' + obid.locale + '/api/newsletter',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST',
                statusCode: {
                    422: function (e) {
                        toastr.error("email invalid");
                    }
                }
            }).done(function (data) {
                if (data.subscribed) {
                    swal(
                        "You have been subscribed",
                        "Please save following code and use it for your next purchase: "
                        + data.couponCode,
                        "success"
                    );
                    self.newsletter.email = "";
                } else {
                    swal(
                        "Ooops...",
                        "Your have been subscribed to our news letter.... sorry for that",
                        "error"
                    );
                    self.newsletter.email = "";
                }
            });
        },
        submitNewAccount: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;
            var formRegister = this.formRegister;
            var profile = this.profileRegister;
            formData.append('username', formRegister.username);
            formData.append('email', formRegister.email);
            formData.append('password', formRegister.password);
            formData.append('password_confirmation', formRegister.password_confirm);
            formData.append('nama', formRegister.nama);
            formData.append('nohp', formRegister.nohp);
            formData.append('birthday', profile.date + '-' + profile.month + '-' + profile.year);
            formData.append('alamat', profile.alamat);
            formData.append('kodepos', profile.kodepos);
            formData.append('gender', profile.gender);
            this.submitting = true;
            $.ajax({
                url: '/' + obid.locale + '/register',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                type: 'POST'
            }).done(function (data) {
                if (data.created) {
                    window.location.href = data.redirect;
                }
            });
        }

    },
    ready: function () {
        Layout.init();
        this.getCart();
    }
});

//# sourceMappingURL=shop-login.js.map
