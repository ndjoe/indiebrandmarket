Vue.filter('subTotal', function (data) {
    var total = 0;
    data.forEach(function (d) {
        var discount = 100 - d.options.discount;
        discount = discount / 100;
        var qty = d.qty * 1;
        var price = d.price * 1;
        total += qty * price * discount;
    });

    return total;
});

Vue.filter('applyDiscount', function (d) {
    var discount = 100 - d.options.discount;
    discount = discount / 100;

    return d.price * discount;
});
Vue.filter('total', function (data) {
    var total = 0;
    data.forEach(function (d) {
        var discount = 100 - d.options.discount;
        discount = discount / 100;
        var qty = d.qty * 1;
        var price = d.price * 1;
        total += qty * price * discount;
    });

    return total;
});
Vue.filter('sumPrice', function (data) {
    var discount = 100 - data.options.discount;
    discount = discount / 100;

    return data.price * data.qty * discount;
});

var vm = new Vue({
    el: '#shop',
    data: {
        cart: [],
        coupon: '',
        voucher: 0,
        birthdayDiscount: obid.birthday ? true : false,
        locale: obid.locale,
        currency: obid.locale === 'id' ? 'IDR' : 'USD',
        newsletter: {
            email: ''
        }
    },
    computed: {
        'grandTotal': function () {
            var total = 0;
            this.cart.forEach(function (d) {
                var discount = 100 - d.options.discount;
                discount = discount / 100;
                var qty = d.qty * 1;
                var price = d.price * 1;
                total += qty * price * discount;
            });

            if (this.birthdayDiscount) {
                total = total * (100 - 5) / 100;
            }

            return total - this.voucher.value;
        },
        'birthdayValue': function () {
            var total = 0;
            this.cart.forEach(function (d) {
                var discount = 100 - d.options.discount;
                discount = discount / 100;
                var qty = d.qty * 1;
                var price = d.price * 1;
                total += qty * price * discount;
            });

            return total * 5 / 100;
        }
    }
    ,
    methods: {
        getCart: function () {
            var self = this;
            $.get('/' + obid.locale + '/api/cart')
                .done(function (resp) {
                    self.cart = resp.items;
                    self.voucher = resp.coupon;
                });
        },
        addCart: function (e) {
            e.preventDefault();
            var qty = this.$els.qty.value;
            var size = this.$els.size.value;
            var id = this.$els.id.value;
            var self = this;
            var formData = new FormData;
            formData.append('productId', id);
            formData.append('sizeId', size);
            formData.append('qty', qty);
            $.ajax({
                url: '/' + obid.locale + '/api/cart',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST'
            }).done(function (data) {
                self.cart = data.items;
            });
        },
        delItem: function (data) {
            var self = this;
            var formData = new FormData;

            formData.append('rowid', data['rowid']);

            $.ajax({
                url: '/' + obid.locale + '/api/cart/delete',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST'
            }).done(function (data) {
                self.cart = data.items;
            });
        },
        addCoupon: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;
            formData.append('code', this.coupon);
            $.ajax({
                url: '/' + obid.locale + '/api/coupon',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                aync: true,
                type: 'POST'
            }).done(function (data) {
                if (data.added === false) {
                    toastr.error(data.reason);
                    return false;
                }
                toastr.success('voucher berhasil ditambahkan');
                self.cart = data.items;
                self.coupon = '';
                self.voucher = data.coupon;
            });
        },
        delCoupon: function () {
            var self = this;
            $.get('/' + obid.locale + '/api/coupon/delete')
                .done(function (data) {
                    toastr.success('voucher berhasil dihapus');
                    self.cart = data.items;
                    self.coupon = '';
                    self.voucher = 0;
                });
        },
        subscribesNewsLetter: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;
            formData.append('email', this.newsletter.email);

            $.ajax({
                url: '/' + obid.locale + '/api/newsletter',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST',
                statusCode: {
                    422: function (e) {
                        toastr.error("email invalid");
                    }
                }
            }).done(function (data) {
                if (data.subscribed) {
                    swal(
                        "You have been subscribed",
                        "Please save following code and use it for your next purchase: "
                        + data.couponCode,
                        "success"
                    );
                    self.newsletter.email = "";
                } else {
                    swal(
                        "Ooops...",
                        "Your have been subscribed to our news letter.... sorry for that",
                        "error"
                    );
                    self.newsletter.email = "";
                }
            });
        }

    },
    ready: function () {
        Layout.init();
        Layout.initTouchspin();
        Layout.initTwitter();
        //Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();
        this.getCart();
    }
});

//# sourceMappingURL=shop-cart.js.map
