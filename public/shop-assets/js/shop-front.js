new Vue({
    el: '#shop',
    data: {
        data: [],
        currentPage: 1,
        totalPage: 1,
        brands: [],
        cart: [],
        currency: obid.locale === 'id' ? 'IDR' : 'USD',
        locale: obid.locale,
        newsletter: {
            email: ''
        }
    },
    computed: {
        isLast: function () {
            return this.currentPage === this.totalPage;
        },
        isFirst: function () {
            return this.currentPage === 1;
        }
    },
    methods: {
        getData: function (page) {
            if (!page) {
                page = 1;
            }
            var self = this;
            $.get('/' + obid.locale + '/api/products', {
                query: this.query,
                filter: this.selected,
                page: page
            }).done(function (data) {
                self.data = data.data;
                self.currentPage = data.current_page;
                self.totalPage = data.last_page;
            });
        },
        prev: function () {
            if (this.currentPage === 1) {
                return;
            }
            this.getData(this.currentPage - 1);
        },
        next: function () {
            if (this.currentPage === this.totalPage) {
                return;
            }
            this.getData(this.currentPage + 1);
        },
        getCart: function () {
            var self = this;
            $.get('/' + obid.locale + '/api/cart')
                .done(function (resp) {
                    self.cart = resp.items;
                });
        },
        delItem: function (data) {
            var self = this;
            var formData = new FormData;

            formData.append('rowid', data['rowid']);

            $.ajax({
                url: '/' + obid.locale + '/api/cart/delete',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST'
            }).done(function (data) {
                self.cart = data.items;
            });
        },
        getBrands: function () {
            var self = this;
            $.get('/' + obid.locale + '/api/brands')
                .done(function (resp) {
                    self.brands = resp;
                });
        },
        isDiscounted: function (discount) {
            return moment().isBefore(discount.expired_at) && discount.value > 0;
        },
        subscribesNewsLetter: function (e) {
            e.preventDefault();
            var self = this;
            var formData = new FormData;
            formData.append('email', this.newsletter.email);

            $.ajax({
                url: '/' + obid.locale + '/api/newsletter',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                type: 'POST',
                statusCode: {
                    422: function (e) {
                        toastr.error("email invalid");
                    }
                }
            }).done(function (data) {
                if (data.subscribed) {
                    swal(
                        "You have been subscribed",
                        "Please save following code and use it for your next purchase: "
                        + data.couponCode,
                        "success"
                    );
                    self.newsletter.email = "";
                } else {
                    swal(
                        "Ooops...",
                        "Your have been subscribed to our news letter.... sorry for that",
                        "error"
                    );
                    self.newsletter.email = "";
                }
            });
        }
    },
    ready: function () {
        Layout.init();
        $('.home-banner').slick({
            infinite: true,
            speed: 800,
            accesibility: true,
            autoplay: true,
            arrows: false,
            autoplaySpeed: 2500
        });
        this.getData();
        this.getCart();
        this.getBrands();
    }
});

//# sourceMappingURL=shop-front.js.map
