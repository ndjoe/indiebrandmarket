/*! pace 1.0.0 */
(function(){var a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X=[].slice,Y={}.hasOwnProperty,Z=function(a,b){function c(){this.constructor=a}for(var d in b)Y.call(b,d)&&(a[d]=b[d]);return c.prototype=b.prototype,a.prototype=new c,a.__super__=b.prototype,a},$=[].indexOf||function(a){for(var b=0,c=this.length;c>b;b++)if(b in this&&this[b]===a)return b;return-1};for(u={catchupTime:100,initialRate:.03,minTime:250,ghostTime:100,maxProgressPerFrame:20,easeFactor:1.25,startOnPageLoad:!0,restartOnPushState:!0,restartOnRequestAfter:500,target:"body",elements:{checkInterval:100,selectors:["body"]},eventLag:{minSamples:10,sampleCount:3,lagThreshold:3},ajax:{trackMethods:["GET"],trackWebSockets:!0,ignoreURLs:[]}},C=function(){var a;return null!=(a="undefined"!=typeof performance&&null!==performance&&"function"==typeof performance.now?performance.now():void 0)?a:+new Date},E=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||window.msRequestAnimationFrame,t=window.cancelAnimationFrame||window.mozCancelAnimationFrame,null==E&&(E=function(a){return setTimeout(a,50)},t=function(a){return clearTimeout(a)}),G=function(a){var b,c;return b=C(),(c=function(){var d;return d=C()-b,d>=33?(b=C(),a(d,function(){return E(c)})):setTimeout(c,33-d)})()},F=function(){var a,b,c;return c=arguments[0],b=arguments[1],a=3<=arguments.length?X.call(arguments,2):[],"function"==typeof c[b]?c[b].apply(c,a):c[b]},v=function(){var a,b,c,d,e,f,g;for(b=arguments[0],d=2<=arguments.length?X.call(arguments,1):[],f=0,g=d.length;g>f;f++)if(c=d[f])for(a in c)Y.call(c,a)&&(e=c[a],null!=b[a]&&"object"==typeof b[a]&&null!=e&&"object"==typeof e?v(b[a],e):b[a]=e);return b},q=function(a){var b,c,d,e,f;for(c=b=0,e=0,f=a.length;f>e;e++)d=a[e],c+=Math.abs(d),b++;return c/b},x=function(a,b){var c,d,e;if(null==a&&(a="options"),null==b&&(b=!0),e=document.querySelector("[data-pace-"+a+"]")){if(c=e.getAttribute("data-pace-"+a),!b)return c;try{return JSON.parse(c)}catch(f){return d=f,"undefined"!=typeof console&&null!==console?console.error("Error parsing inline pace options",d):void 0}}},g=function(){function a(){}return a.prototype.on=function(a,b,c,d){var e;return null==d&&(d=!1),null==this.bindings&&(this.bindings={}),null==(e=this.bindings)[a]&&(e[a]=[]),this.bindings[a].push({handler:b,ctx:c,once:d})},a.prototype.once=function(a,b,c){return this.on(a,b,c,!0)},a.prototype.off=function(a,b){var c,d,e;if(null!=(null!=(d=this.bindings)?d[a]:void 0)){if(null==b)return delete this.bindings[a];for(c=0,e=[];c<this.bindings[a].length;)e.push(this.bindings[a][c].handler===b?this.bindings[a].splice(c,1):c++);return e}},a.prototype.trigger=function(){var a,b,c,d,e,f,g,h,i;if(c=arguments[0],a=2<=arguments.length?X.call(arguments,1):[],null!=(g=this.bindings)?g[c]:void 0){for(e=0,i=[];e<this.bindings[c].length;)h=this.bindings[c][e],d=h.handler,b=h.ctx,f=h.once,d.apply(null!=b?b:this,a),i.push(f?this.bindings[c].splice(e,1):e++);return i}},a}(),j=window.Pace||{},window.Pace=j,v(j,g.prototype),D=j.options=v({},u,window.paceOptions,x()),U=["ajax","document","eventLag","elements"],Q=0,S=U.length;S>Q;Q++)K=U[Q],D[K]===!0&&(D[K]=u[K]);i=function(a){function b(){return V=b.__super__.constructor.apply(this,arguments)}return Z(b,a),b}(Error),b=function(){function a(){this.progress=0}return a.prototype.getElement=function(){var a;if(null==this.el){if(a=document.querySelector(D.target),!a)throw new i;this.el=document.createElement("div"),this.el.className="pace pace-active",document.body.className=document.body.className.replace(/pace-done/g,""),document.body.className+=" pace-running",this.el.innerHTML='<div class="pace-progress">\n  <div class="pace-progress-inner"></div>\n</div>\n<div class="pace-activity"></div>',null!=a.firstChild?a.insertBefore(this.el,a.firstChild):a.appendChild(this.el)}return this.el},a.prototype.finish=function(){var a;return a=this.getElement(),a.className=a.className.replace("pace-active",""),a.className+=" pace-inactive",document.body.className=document.body.className.replace("pace-running",""),document.body.className+=" pace-done"},a.prototype.update=function(a){return this.progress=a,this.render()},a.prototype.destroy=function(){try{this.getElement().parentNode.removeChild(this.getElement())}catch(a){i=a}return this.el=void 0},a.prototype.render=function(){var a,b,c,d,e,f,g;if(null==document.querySelector(D.target))return!1;for(a=this.getElement(),d="translate3d("+this.progress+"%, 0, 0)",g=["webkitTransform","msTransform","transform"],e=0,f=g.length;f>e;e++)b=g[e],a.children[0].style[b]=d;return(!this.lastRenderedProgress||this.lastRenderedProgress|0!==this.progress|0)&&(a.children[0].setAttribute("data-progress-text",""+(0|this.progress)+"%"),this.progress>=100?c="99":(c=this.progress<10?"0":"",c+=0|this.progress),a.children[0].setAttribute("data-progress",""+c)),this.lastRenderedProgress=this.progress},a.prototype.done=function(){return this.progress>=100},a}(),h=function(){function a(){this.bindings={}}return a.prototype.trigger=function(a,b){var c,d,e,f,g;if(null!=this.bindings[a]){for(f=this.bindings[a],g=[],d=0,e=f.length;e>d;d++)c=f[d],g.push(c.call(this,b));return g}},a.prototype.on=function(a,b){var c;return null==(c=this.bindings)[a]&&(c[a]=[]),this.bindings[a].push(b)},a}(),P=window.XMLHttpRequest,O=window.XDomainRequest,N=window.WebSocket,w=function(a,b){var c,d,e,f;f=[];for(d in b.prototype)try{e=b.prototype[d],f.push(null==a[d]&&"function"!=typeof e?a[d]=e:void 0)}catch(g){c=g}return f},A=[],j.ignore=function(){var a,b,c;return b=arguments[0],a=2<=arguments.length?X.call(arguments,1):[],A.unshift("ignore"),c=b.apply(null,a),A.shift(),c},j.track=function(){var a,b,c;return b=arguments[0],a=2<=arguments.length?X.call(arguments,1):[],A.unshift("track"),c=b.apply(null,a),A.shift(),c},J=function(a){var b;if(null==a&&(a="GET"),"track"===A[0])return"force";if(!A.length&&D.ajax){if("socket"===a&&D.ajax.trackWebSockets)return!0;if(b=a.toUpperCase(),$.call(D.ajax.trackMethods,b)>=0)return!0}return!1},k=function(a){function b(){var a,c=this;b.__super__.constructor.apply(this,arguments),a=function(a){var b;return b=a.open,a.open=function(d,e){return J(d)&&c.trigger("request",{type:d,url:e,request:a}),b.apply(a,arguments)}},window.XMLHttpRequest=function(b){var c;return c=new P(b),a(c),c};try{w(window.XMLHttpRequest,P)}catch(d){}if(null!=O){window.XDomainRequest=function(){var b;return b=new O,a(b),b};try{w(window.XDomainRequest,O)}catch(d){}}if(null!=N&&D.ajax.trackWebSockets){window.WebSocket=function(a,b){var d;return d=null!=b?new N(a,b):new N(a),J("socket")&&c.trigger("request",{type:"socket",url:a,protocols:b,request:d}),d};try{w(window.WebSocket,N)}catch(d){}}}return Z(b,a),b}(h),R=null,y=function(){return null==R&&(R=new k),R},I=function(a){var b,c,d,e;for(e=D.ajax.ignoreURLs,c=0,d=e.length;d>c;c++)if(b=e[c],"string"==typeof b){if(-1!==a.indexOf(b))return!0}else if(b.test(a))return!0;return!1},y().on("request",function(b){var c,d,e,f,g;return f=b.type,e=b.request,g=b.url,I(g)?void 0:j.running||D.restartOnRequestAfter===!1&&"force"!==J(f)?void 0:(d=arguments,c=D.restartOnRequestAfter||0,"boolean"==typeof c&&(c=0),setTimeout(function(){var b,c,g,h,i,k;if(b="socket"===f?e.readyState<2:0<(h=e.readyState)&&4>h){for(j.restart(),i=j.sources,k=[],c=0,g=i.length;g>c;c++){if(K=i[c],K instanceof a){K.watch.apply(K,d);break}k.push(void 0)}return k}},c))}),a=function(){function a(){var a=this;this.elements=[],y().on("request",function(){return a.watch.apply(a,arguments)})}return a.prototype.watch=function(a){var b,c,d,e;return d=a.type,b=a.request,e=a.url,I(e)?void 0:(c="socket"===d?new n(b):new o(b),this.elements.push(c))},a}(),o=function(){function a(a){var b,c,d,e,f,g,h=this;if(this.progress=0,null!=window.ProgressEvent)for(c=null,a.addEventListener("progress",function(a){return h.progress=a.lengthComputable?100*a.loaded/a.total:h.progress+(100-h.progress)/2},!1),g=["load","abort","timeout","error"],d=0,e=g.length;e>d;d++)b=g[d],a.addEventListener(b,function(){return h.progress=100},!1);else f=a.onreadystatechange,a.onreadystatechange=function(){var b;return 0===(b=a.readyState)||4===b?h.progress=100:3===a.readyState&&(h.progress=50),"function"==typeof f?f.apply(null,arguments):void 0}}return a}(),n=function(){function a(a){var b,c,d,e,f=this;for(this.progress=0,e=["error","open"],c=0,d=e.length;d>c;c++)b=e[c],a.addEventListener(b,function(){return f.progress=100},!1)}return a}(),d=function(){function a(a){var b,c,d,f;for(null==a&&(a={}),this.elements=[],null==a.selectors&&(a.selectors=[]),f=a.selectors,c=0,d=f.length;d>c;c++)b=f[c],this.elements.push(new e(b))}return a}(),e=function(){function a(a){this.selector=a,this.progress=0,this.check()}return a.prototype.check=function(){var a=this;return document.querySelector(this.selector)?this.done():setTimeout(function(){return a.check()},D.elements.checkInterval)},a.prototype.done=function(){return this.progress=100},a}(),c=function(){function a(){var a,b,c=this;this.progress=null!=(b=this.states[document.readyState])?b:100,a=document.onreadystatechange,document.onreadystatechange=function(){return null!=c.states[document.readyState]&&(c.progress=c.states[document.readyState]),"function"==typeof a?a.apply(null,arguments):void 0}}return a.prototype.states={loading:0,interactive:50,complete:100},a}(),f=function(){function a(){var a,b,c,d,e,f=this;this.progress=0,a=0,e=[],d=0,c=C(),b=setInterval(function(){var g;return g=C()-c-50,c=C(),e.push(g),e.length>D.eventLag.sampleCount&&e.shift(),a=q(e),++d>=D.eventLag.minSamples&&a<D.eventLag.lagThreshold?(f.progress=100,clearInterval(b)):f.progress=100*(3/(a+3))},50)}return a}(),m=function(){function a(a){this.source=a,this.last=this.sinceLastUpdate=0,this.rate=D.initialRate,this.catchup=0,this.progress=this.lastProgress=0,null!=this.source&&(this.progress=F(this.source,"progress"))}return a.prototype.tick=function(a,b){var c;return null==b&&(b=F(this.source,"progress")),b>=100&&(this.done=!0),b===this.last?this.sinceLastUpdate+=a:(this.sinceLastUpdate&&(this.rate=(b-this.last)/this.sinceLastUpdate),this.catchup=(b-this.progress)/D.catchupTime,this.sinceLastUpdate=0,this.last=b),b>this.progress&&(this.progress+=this.catchup*a),c=1-Math.pow(this.progress/100,D.easeFactor),this.progress+=c*this.rate*a,this.progress=Math.min(this.lastProgress+D.maxProgressPerFrame,this.progress),this.progress=Math.max(0,this.progress),this.progress=Math.min(100,this.progress),this.lastProgress=this.progress,this.progress},a}(),L=null,H=null,r=null,M=null,p=null,s=null,j.running=!1,z=function(){return D.restartOnPushState?j.restart():void 0},null!=window.history.pushState&&(T=window.history.pushState,window.history.pushState=function(){return z(),T.apply(window.history,arguments)}),null!=window.history.replaceState&&(W=window.history.replaceState,window.history.replaceState=function(){return z(),W.apply(window.history,arguments)}),l={ajax:a,elements:d,document:c,eventLag:f},(B=function(){var a,c,d,e,f,g,h,i;for(j.sources=L=[],g=["ajax","elements","document","eventLag"],c=0,e=g.length;e>c;c++)a=g[c],D[a]!==!1&&L.push(new l[a](D[a]));for(i=null!=(h=D.extraSources)?h:[],d=0,f=i.length;f>d;d++)K=i[d],L.push(new K(D));return j.bar=r=new b,H=[],M=new m})(),j.stop=function(){return j.trigger("stop"),j.running=!1,r.destroy(),s=!0,null!=p&&("function"==typeof t&&t(p),p=null),B()},j.restart=function(){return j.trigger("restart"),j.stop(),j.start()},j.go=function(){var a;return j.running=!0,r.render(),a=C(),s=!1,p=G(function(b,c){var d,e,f,g,h,i,k,l,n,o,p,q,t,u,v,w;for(l=100-r.progress,e=p=0,f=!0,i=q=0,u=L.length;u>q;i=++q)for(K=L[i],o=null!=H[i]?H[i]:H[i]=[],h=null!=(w=K.elements)?w:[K],k=t=0,v=h.length;v>t;k=++t)g=h[k],n=null!=o[k]?o[k]:o[k]=new m(g),f&=n.done,n.done||(e++,p+=n.tick(b));return d=p/e,r.update(M.tick(b,d)),r.done()||f||s?(r.update(100),j.trigger("done"),setTimeout(function(){return r.finish(),j.running=!1,j.trigger("hide")},Math.max(D.ghostTime,Math.max(D.minTime-(C()-a),0)))):c()})},j.start=function(a){v(D,a),j.running=!0;try{r.render()}catch(b){i=b}return document.querySelector(".pace")?(j.trigger("start"),j.go()):setTimeout(j.start,50)},"function"==typeof define&&define.amd?define(function(){return j}):"object"==typeof exports?module.exports=j:D.startOnPageLoad&&j.start()}).call(this);
/*! jQuery v2.1.4 | (c) 2005, 2015 jQuery Foundation, Inc. | jquery.org/license */
!function(a,b){"object"==typeof module&&"object"==typeof module.exports?module.exports=a.document?b(a,!0):function(a){if(!a.document)throw new Error("jQuery requires a window with a document");return b(a)}:b(a)}("undefined"!=typeof window?window:this,function(a,b){var c=[],d=c.slice,e=c.concat,f=c.push,g=c.indexOf,h={},i=h.toString,j=h.hasOwnProperty,k={},l=a.document,m="2.1.4",n=function(a,b){return new n.fn.init(a,b)},o=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,p=/^-ms-/,q=/-([\da-z])/gi,r=function(a,b){return b.toUpperCase()};n.fn=n.prototype={jquery:m,constructor:n,selector:"",length:0,toArray:function(){return d.call(this)},get:function(a){return null!=a?0>a?this[a+this.length]:this[a]:d.call(this)},pushStack:function(a){var b=n.merge(this.constructor(),a);return b.prevObject=this,b.context=this.context,b},each:function(a,b){return n.each(this,a,b)},map:function(a){return this.pushStack(n.map(this,function(b,c){return a.call(b,c,b)}))},slice:function(){return this.pushStack(d.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(a){var b=this.length,c=+a+(0>a?b:0);return this.pushStack(c>=0&&b>c?[this[c]]:[])},end:function(){return this.prevObject||this.constructor(null)},push:f,sort:c.sort,splice:c.splice},n.extend=n.fn.extend=function(){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||n.isFunction(g)||(g={}),h===i&&(g=this,h--);i>h;h++)if(null!=(a=arguments[h]))for(b in a)c=g[b],d=a[b],g!==d&&(j&&d&&(n.isPlainObject(d)||(e=n.isArray(d)))?(e?(e=!1,f=c&&n.isArray(c)?c:[]):f=c&&n.isPlainObject(c)?c:{},g[b]=n.extend(j,f,d)):void 0!==d&&(g[b]=d));return g},n.extend({expando:"jQuery"+(m+Math.random()).replace(/\D/g,""),isReady:!0,error:function(a){throw new Error(a)},noop:function(){},isFunction:function(a){return"function"===n.type(a)},isArray:Array.isArray,isWindow:function(a){return null!=a&&a===a.window},isNumeric:function(a){return!n.isArray(a)&&a-parseFloat(a)+1>=0},isPlainObject:function(a){return"object"!==n.type(a)||a.nodeType||n.isWindow(a)?!1:a.constructor&&!j.call(a.constructor.prototype,"isPrototypeOf")?!1:!0},isEmptyObject:function(a){var b;for(b in a)return!1;return!0},type:function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?h[i.call(a)]||"object":typeof a},globalEval:function(a){var b,c=eval;a=n.trim(a),a&&(1===a.indexOf("use strict")?(b=l.createElement("script"),b.text=a,l.head.appendChild(b).parentNode.removeChild(b)):c(a))},camelCase:function(a){return a.replace(p,"ms-").replace(q,r)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()},each:function(a,b,c){var d,e=0,f=a.length,g=s(a);if(c){if(g){for(;f>e;e++)if(d=b.apply(a[e],c),d===!1)break}else for(e in a)if(d=b.apply(a[e],c),d===!1)break}else if(g){for(;f>e;e++)if(d=b.call(a[e],e,a[e]),d===!1)break}else for(e in a)if(d=b.call(a[e],e,a[e]),d===!1)break;return a},trim:function(a){return null==a?"":(a+"").replace(o,"")},makeArray:function(a,b){var c=b||[];return null!=a&&(s(Object(a))?n.merge(c,"string"==typeof a?[a]:a):f.call(c,a)),c},inArray:function(a,b,c){return null==b?-1:g.call(b,a,c)},merge:function(a,b){for(var c=+b.length,d=0,e=a.length;c>d;d++)a[e++]=b[d];return a.length=e,a},grep:function(a,b,c){for(var d,e=[],f=0,g=a.length,h=!c;g>f;f++)d=!b(a[f],f),d!==h&&e.push(a[f]);return e},map:function(a,b,c){var d,f=0,g=a.length,h=s(a),i=[];if(h)for(;g>f;f++)d=b(a[f],f,c),null!=d&&i.push(d);else for(f in a)d=b(a[f],f,c),null!=d&&i.push(d);return e.apply([],i)},guid:1,proxy:function(a,b){var c,e,f;return"string"==typeof b&&(c=a[b],b=a,a=c),n.isFunction(a)?(e=d.call(arguments,2),f=function(){return a.apply(b||this,e.concat(d.call(arguments)))},f.guid=a.guid=a.guid||n.guid++,f):void 0},now:Date.now,support:k}),n.each("Boolean Number String Function Array Date RegExp Object Error".split(" "),function(a,b){h["[object "+b+"]"]=b.toLowerCase()});function s(a){var b="length"in a&&a.length,c=n.type(a);return"function"===c||n.isWindow(a)?!1:1===a.nodeType&&b?!0:"array"===c||0===b||"number"==typeof b&&b>0&&b-1 in a}var t=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u="sizzle"+1*new Date,v=a.document,w=0,x=0,y=ha(),z=ha(),A=ha(),B=function(a,b){return a===b&&(l=!0),0},C=1<<31,D={}.hasOwnProperty,E=[],F=E.pop,G=E.push,H=E.push,I=E.slice,J=function(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return c;return-1},K="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",L="[\\x20\\t\\r\\n\\f]",M="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",N=M.replace("w","w#"),O="\\["+L+"*("+M+")(?:"+L+"*([*^$|!~]?=)"+L+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+N+"))|)"+L+"*\\]",P=":("+M+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+O+")*)|.*)\\)|)",Q=new RegExp(L+"+","g"),R=new RegExp("^"+L+"+|((?:^|[^\\\\])(?:\\\\.)*)"+L+"+$","g"),S=new RegExp("^"+L+"*,"+L+"*"),T=new RegExp("^"+L+"*([>+~]|"+L+")"+L+"*"),U=new RegExp("="+L+"*([^\\]'\"]*?)"+L+"*\\]","g"),V=new RegExp(P),W=new RegExp("^"+N+"$"),X={ID:new RegExp("^#("+M+")"),CLASS:new RegExp("^\\.("+M+")"),TAG:new RegExp("^("+M.replace("w","w*")+")"),ATTR:new RegExp("^"+O),PSEUDO:new RegExp("^"+P),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+L+"*(even|odd|(([+-]|)(\\d*)n|)"+L+"*(?:([+-]|)"+L+"*(\\d+)|))"+L+"*\\)|)","i"),bool:new RegExp("^(?:"+K+")$","i"),needsContext:new RegExp("^"+L+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+L+"*((?:-\\d)?\\d*)"+L+"*\\)|)(?=[^-]|$)","i")},Y=/^(?:input|select|textarea|button)$/i,Z=/^h\d$/i,$=/^[^{]+\{\s*\[native \w/,_=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,aa=/[+~]/,ba=/'|\\/g,ca=new RegExp("\\\\([\\da-f]{1,6}"+L+"?|("+L+")|.)","ig"),da=function(a,b,c){var d="0x"+b-65536;return d!==d||c?b:0>d?String.fromCharCode(d+65536):String.fromCharCode(d>>10|55296,1023&d|56320)},ea=function(){m()};try{H.apply(E=I.call(v.childNodes),v.childNodes),E[v.childNodes.length].nodeType}catch(fa){H={apply:E.length?function(a,b){G.apply(a,I.call(b))}:function(a,b){var c=a.length,d=0;while(a[c++]=b[d++]);a.length=c-1}}}function ga(a,b,d,e){var f,h,j,k,l,o,r,s,w,x;if((b?b.ownerDocument||b:v)!==n&&m(b),b=b||n,d=d||[],k=b.nodeType,"string"!=typeof a||!a||1!==k&&9!==k&&11!==k)return d;if(!e&&p){if(11!==k&&(f=_.exec(a)))if(j=f[1]){if(9===k){if(h=b.getElementById(j),!h||!h.parentNode)return d;if(h.id===j)return d.push(h),d}else if(b.ownerDocument&&(h=b.ownerDocument.getElementById(j))&&t(b,h)&&h.id===j)return d.push(h),d}else{if(f[2])return H.apply(d,b.getElementsByTagName(a)),d;if((j=f[3])&&c.getElementsByClassName)return H.apply(d,b.getElementsByClassName(j)),d}if(c.qsa&&(!q||!q.test(a))){if(s=r=u,w=b,x=1!==k&&a,1===k&&"object"!==b.nodeName.toLowerCase()){o=g(a),(r=b.getAttribute("id"))?s=r.replace(ba,"\\$&"):b.setAttribute("id",s),s="[id='"+s+"'] ",l=o.length;while(l--)o[l]=s+ra(o[l]);w=aa.test(a)&&pa(b.parentNode)||b,x=o.join(",")}if(x)try{return H.apply(d,w.querySelectorAll(x)),d}catch(y){}finally{r||b.removeAttribute("id")}}}return i(a.replace(R,"$1"),b,d,e)}function ha(){var a=[];function b(c,e){return a.push(c+" ")>d.cacheLength&&delete b[a.shift()],b[c+" "]=e}return b}function ia(a){return a[u]=!0,a}function ja(a){var b=n.createElement("div");try{return!!a(b)}catch(c){return!1}finally{b.parentNode&&b.parentNode.removeChild(b),b=null}}function ka(a,b){var c=a.split("|"),e=a.length;while(e--)d.attrHandle[c[e]]=b}function la(a,b){var c=b&&a,d=c&&1===a.nodeType&&1===b.nodeType&&(~b.sourceIndex||C)-(~a.sourceIndex||C);if(d)return d;if(c)while(c=c.nextSibling)if(c===b)return-1;return a?1:-1}function ma(a){return function(b){var c=b.nodeName.toLowerCase();return"input"===c&&b.type===a}}function na(a){return function(b){var c=b.nodeName.toLowerCase();return("input"===c||"button"===c)&&b.type===a}}function oa(a){return ia(function(b){return b=+b,ia(function(c,d){var e,f=a([],c.length,b),g=f.length;while(g--)c[e=f[g]]&&(c[e]=!(d[e]=c[e]))})})}function pa(a){return a&&"undefined"!=typeof a.getElementsByTagName&&a}c=ga.support={},f=ga.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;return b?"HTML"!==b.nodeName:!1},m=ga.setDocument=function(a){var b,e,g=a?a.ownerDocument||a:v;return g!==n&&9===g.nodeType&&g.documentElement?(n=g,o=g.documentElement,e=g.defaultView,e&&e!==e.top&&(e.addEventListener?e.addEventListener("unload",ea,!1):e.attachEvent&&e.attachEvent("onunload",ea)),p=!f(g),c.attributes=ja(function(a){return a.className="i",!a.getAttribute("className")}),c.getElementsByTagName=ja(function(a){return a.appendChild(g.createComment("")),!a.getElementsByTagName("*").length}),c.getElementsByClassName=$.test(g.getElementsByClassName),c.getById=ja(function(a){return o.appendChild(a).id=u,!g.getElementsByName||!g.getElementsByName(u).length}),c.getById?(d.find.ID=function(a,b){if("undefined"!=typeof b.getElementById&&p){var c=b.getElementById(a);return c&&c.parentNode?[c]:[]}},d.filter.ID=function(a){var b=a.replace(ca,da);return function(a){return a.getAttribute("id")===b}}):(delete d.find.ID,d.filter.ID=function(a){var b=a.replace(ca,da);return function(a){var c="undefined"!=typeof a.getAttributeNode&&a.getAttributeNode("id");return c&&c.value===b}}),d.find.TAG=c.getElementsByTagName?function(a,b){return"undefined"!=typeof b.getElementsByTagName?b.getElementsByTagName(a):c.qsa?b.querySelectorAll(a):void 0}:function(a,b){var c,d=[],e=0,f=b.getElementsByTagName(a);if("*"===a){while(c=f[e++])1===c.nodeType&&d.push(c);return d}return f},d.find.CLASS=c.getElementsByClassName&&function(a,b){return p?b.getElementsByClassName(a):void 0},r=[],q=[],(c.qsa=$.test(g.querySelectorAll))&&(ja(function(a){o.appendChild(a).innerHTML="<a id='"+u+"'></a><select id='"+u+"-\f]' msallowcapture=''><option selected=''></option></select>",a.querySelectorAll("[msallowcapture^='']").length&&q.push("[*^$]="+L+"*(?:''|\"\")"),a.querySelectorAll("[selected]").length||q.push("\\["+L+"*(?:value|"+K+")"),a.querySelectorAll("[id~="+u+"-]").length||q.push("~="),a.querySelectorAll(":checked").length||q.push(":checked"),a.querySelectorAll("a#"+u+"+*").length||q.push(".#.+[+~]")}),ja(function(a){var b=g.createElement("input");b.setAttribute("type","hidden"),a.appendChild(b).setAttribute("name","D"),a.querySelectorAll("[name=d]").length&&q.push("name"+L+"*[*^$|!~]?="),a.querySelectorAll(":enabled").length||q.push(":enabled",":disabled"),a.querySelectorAll("*,:x"),q.push(",.*:")})),(c.matchesSelector=$.test(s=o.matches||o.webkitMatchesSelector||o.mozMatchesSelector||o.oMatchesSelector||o.msMatchesSelector))&&ja(function(a){c.disconnectedMatch=s.call(a,"div"),s.call(a,"[s!='']:x"),r.push("!=",P)}),q=q.length&&new RegExp(q.join("|")),r=r.length&&new RegExp(r.join("|")),b=$.test(o.compareDocumentPosition),t=b||$.test(o.contains)?function(a,b){var c=9===a.nodeType?a.documentElement:a,d=b&&b.parentNode;return a===d||!(!d||1!==d.nodeType||!(c.contains?c.contains(d):a.compareDocumentPosition&&16&a.compareDocumentPosition(d)))}:function(a,b){if(b)while(b=b.parentNode)if(b===a)return!0;return!1},B=b?function(a,b){if(a===b)return l=!0,0;var d=!a.compareDocumentPosition-!b.compareDocumentPosition;return d?d:(d=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b):1,1&d||!c.sortDetached&&b.compareDocumentPosition(a)===d?a===g||a.ownerDocument===v&&t(v,a)?-1:b===g||b.ownerDocument===v&&t(v,b)?1:k?J(k,a)-J(k,b):0:4&d?-1:1)}:function(a,b){if(a===b)return l=!0,0;var c,d=0,e=a.parentNode,f=b.parentNode,h=[a],i=[b];if(!e||!f)return a===g?-1:b===g?1:e?-1:f?1:k?J(k,a)-J(k,b):0;if(e===f)return la(a,b);c=a;while(c=c.parentNode)h.unshift(c);c=b;while(c=c.parentNode)i.unshift(c);while(h[d]===i[d])d++;return d?la(h[d],i[d]):h[d]===v?-1:i[d]===v?1:0},g):n},ga.matches=function(a,b){return ga(a,null,null,b)},ga.matchesSelector=function(a,b){if((a.ownerDocument||a)!==n&&m(a),b=b.replace(U,"='$1']"),!(!c.matchesSelector||!p||r&&r.test(b)||q&&q.test(b)))try{var d=s.call(a,b);if(d||c.disconnectedMatch||a.document&&11!==a.document.nodeType)return d}catch(e){}return ga(b,n,null,[a]).length>0},ga.contains=function(a,b){return(a.ownerDocument||a)!==n&&m(a),t(a,b)},ga.attr=function(a,b){(a.ownerDocument||a)!==n&&m(a);var e=d.attrHandle[b.toLowerCase()],f=e&&D.call(d.attrHandle,b.toLowerCase())?e(a,b,!p):void 0;return void 0!==f?f:c.attributes||!p?a.getAttribute(b):(f=a.getAttributeNode(b))&&f.specified?f.value:null},ga.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)},ga.uniqueSort=function(a){var b,d=[],e=0,f=0;if(l=!c.detectDuplicates,k=!c.sortStable&&a.slice(0),a.sort(B),l){while(b=a[f++])b===a[f]&&(e=d.push(f));while(e--)a.splice(d[e],1)}return k=null,a},e=ga.getText=function(a){var b,c="",d=0,f=a.nodeType;if(f){if(1===f||9===f||11===f){if("string"==typeof a.textContent)return a.textContent;for(a=a.firstChild;a;a=a.nextSibling)c+=e(a)}else if(3===f||4===f)return a.nodeValue}else while(b=a[d++])c+=e(b);return c},d=ga.selectors={cacheLength:50,createPseudo:ia,match:X,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){return a[1]=a[1].replace(ca,da),a[3]=(a[3]||a[4]||a[5]||"").replace(ca,da),"~="===a[2]&&(a[3]=" "+a[3]+" "),a.slice(0,4)},CHILD:function(a){return a[1]=a[1].toLowerCase(),"nth"===a[1].slice(0,3)?(a[3]||ga.error(a[0]),a[4]=+(a[4]?a[5]+(a[6]||1):2*("even"===a[3]||"odd"===a[3])),a[5]=+(a[7]+a[8]||"odd"===a[3])):a[3]&&ga.error(a[0]),a},PSEUDO:function(a){var b,c=!a[6]&&a[2];return X.CHILD.test(a[0])?null:(a[3]?a[2]=a[4]||a[5]||"":c&&V.test(c)&&(b=g(c,!0))&&(b=c.indexOf(")",c.length-b)-c.length)&&(a[0]=a[0].slice(0,b),a[2]=c.slice(0,b)),a.slice(0,3))}},filter:{TAG:function(a){var b=a.replace(ca,da).toLowerCase();return"*"===a?function(){return!0}:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b}},CLASS:function(a){var b=y[a+" "];return b||(b=new RegExp("(^|"+L+")"+a+"("+L+"|$)"))&&y(a,function(a){return b.test("string"==typeof a.className&&a.className||"undefined"!=typeof a.getAttribute&&a.getAttribute("class")||"")})},ATTR:function(a,b,c){return function(d){var e=ga.attr(d,a);return null==e?"!="===b:b?(e+="","="===b?e===c:"!="===b?e!==c:"^="===b?c&&0===e.indexOf(c):"*="===b?c&&e.indexOf(c)>-1:"$="===b?c&&e.slice(-c.length)===c:"~="===b?(" "+e.replace(Q," ")+" ").indexOf(c)>-1:"|="===b?e===c||e.slice(0,c.length+1)===c+"-":!1):!0}},CHILD:function(a,b,c,d,e){var f="nth"!==a.slice(0,3),g="last"!==a.slice(-4),h="of-type"===b;return 1===d&&0===e?function(a){return!!a.parentNode}:function(b,c,i){var j,k,l,m,n,o,p=f!==g?"nextSibling":"previousSibling",q=b.parentNode,r=h&&b.nodeName.toLowerCase(),s=!i&&!h;if(q){if(f){while(p){l=b;while(l=l[p])if(h?l.nodeName.toLowerCase()===r:1===l.nodeType)return!1;o=p="only"===a&&!o&&"nextSibling"}return!0}if(o=[g?q.firstChild:q.lastChild],g&&s){k=q[u]||(q[u]={}),j=k[a]||[],n=j[0]===w&&j[1],m=j[0]===w&&j[2],l=n&&q.childNodes[n];while(l=++n&&l&&l[p]||(m=n=0)||o.pop())if(1===l.nodeType&&++m&&l===b){k[a]=[w,n,m];break}}else if(s&&(j=(b[u]||(b[u]={}))[a])&&j[0]===w)m=j[1];else while(l=++n&&l&&l[p]||(m=n=0)||o.pop())if((h?l.nodeName.toLowerCase()===r:1===l.nodeType)&&++m&&(s&&((l[u]||(l[u]={}))[a]=[w,m]),l===b))break;return m-=e,m===d||m%d===0&&m/d>=0}}},PSEUDO:function(a,b){var c,e=d.pseudos[a]||d.setFilters[a.toLowerCase()]||ga.error("unsupported pseudo: "+a);return e[u]?e(b):e.length>1?(c=[a,a,"",b],d.setFilters.hasOwnProperty(a.toLowerCase())?ia(function(a,c){var d,f=e(a,b),g=f.length;while(g--)d=J(a,f[g]),a[d]=!(c[d]=f[g])}):function(a){return e(a,0,c)}):e}},pseudos:{not:ia(function(a){var b=[],c=[],d=h(a.replace(R,"$1"));return d[u]?ia(function(a,b,c,e){var f,g=d(a,null,e,[]),h=a.length;while(h--)(f=g[h])&&(a[h]=!(b[h]=f))}):function(a,e,f){return b[0]=a,d(b,null,f,c),b[0]=null,!c.pop()}}),has:ia(function(a){return function(b){return ga(a,b).length>0}}),contains:ia(function(a){return a=a.replace(ca,da),function(b){return(b.textContent||b.innerText||e(b)).indexOf(a)>-1}}),lang:ia(function(a){return W.test(a||"")||ga.error("unsupported lang: "+a),a=a.replace(ca,da).toLowerCase(),function(b){var c;do if(c=p?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang"))return c=c.toLowerCase(),c===a||0===c.indexOf(a+"-");while((b=b.parentNode)&&1===b.nodeType);return!1}}),target:function(b){var c=a.location&&a.location.hash;return c&&c.slice(1)===b.id},root:function(a){return a===o},focus:function(a){return a===n.activeElement&&(!n.hasFocus||n.hasFocus())&&!!(a.type||a.href||~a.tabIndex)},enabled:function(a){return a.disabled===!1},disabled:function(a){return a.disabled===!0},checked:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&!!a.checked||"option"===b&&!!a.selected},selected:function(a){return a.parentNode&&a.parentNode.selectedIndex,a.selected===!0},empty:function(a){for(a=a.firstChild;a;a=a.nextSibling)if(a.nodeType<6)return!1;return!0},parent:function(a){return!d.pseudos.empty(a)},header:function(a){return Z.test(a.nodeName)},input:function(a){return Y.test(a.nodeName)},button:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&"button"===a.type||"button"===b},text:function(a){var b;return"input"===a.nodeName.toLowerCase()&&"text"===a.type&&(null==(b=a.getAttribute("type"))||"text"===b.toLowerCase())},first:oa(function(){return[0]}),last:oa(function(a,b){return[b-1]}),eq:oa(function(a,b,c){return[0>c?c+b:c]}),even:oa(function(a,b){for(var c=0;b>c;c+=2)a.push(c);return a}),odd:oa(function(a,b){for(var c=1;b>c;c+=2)a.push(c);return a}),lt:oa(function(a,b,c){for(var d=0>c?c+b:c;--d>=0;)a.push(d);return a}),gt:oa(function(a,b,c){for(var d=0>c?c+b:c;++d<b;)a.push(d);return a})}},d.pseudos.nth=d.pseudos.eq;for(b in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})d.pseudos[b]=ma(b);for(b in{submit:!0,reset:!0})d.pseudos[b]=na(b);function qa(){}qa.prototype=d.filters=d.pseudos,d.setFilters=new qa,g=ga.tokenize=function(a,b){var c,e,f,g,h,i,j,k=z[a+" "];if(k)return b?0:k.slice(0);h=a,i=[],j=d.preFilter;while(h){(!c||(e=S.exec(h)))&&(e&&(h=h.slice(e[0].length)||h),i.push(f=[])),c=!1,(e=T.exec(h))&&(c=e.shift(),f.push({value:c,type:e[0].replace(R," ")}),h=h.slice(c.length));for(g in d.filter)!(e=X[g].exec(h))||j[g]&&!(e=j[g](e))||(c=e.shift(),f.push({value:c,type:g,matches:e}),h=h.slice(c.length));if(!c)break}return b?h.length:h?ga.error(a):z(a,i).slice(0)};function ra(a){for(var b=0,c=a.length,d="";c>b;b++)d+=a[b].value;return d}function sa(a,b,c){var d=b.dir,e=c&&"parentNode"===d,f=x++;return b.first?function(b,c,f){while(b=b[d])if(1===b.nodeType||e)return a(b,c,f)}:function(b,c,g){var h,i,j=[w,f];if(g){while(b=b[d])if((1===b.nodeType||e)&&a(b,c,g))return!0}else while(b=b[d])if(1===b.nodeType||e){if(i=b[u]||(b[u]={}),(h=i[d])&&h[0]===w&&h[1]===f)return j[2]=h[2];if(i[d]=j,j[2]=a(b,c,g))return!0}}}function ta(a){return a.length>1?function(b,c,d){var e=a.length;while(e--)if(!a[e](b,c,d))return!1;return!0}:a[0]}function ua(a,b,c){for(var d=0,e=b.length;e>d;d++)ga(a,b[d],c);return c}function va(a,b,c,d,e){for(var f,g=[],h=0,i=a.length,j=null!=b;i>h;h++)(f=a[h])&&(!c||c(f,d,e))&&(g.push(f),j&&b.push(h));return g}function wa(a,b,c,d,e,f){return d&&!d[u]&&(d=wa(d)),e&&!e[u]&&(e=wa(e,f)),ia(function(f,g,h,i){var j,k,l,m=[],n=[],o=g.length,p=f||ua(b||"*",h.nodeType?[h]:h,[]),q=!a||!f&&b?p:va(p,m,a,h,i),r=c?e||(f?a:o||d)?[]:g:q;if(c&&c(q,r,h,i),d){j=va(r,n),d(j,[],h,i),k=j.length;while(k--)(l=j[k])&&(r[n[k]]=!(q[n[k]]=l))}if(f){if(e||a){if(e){j=[],k=r.length;while(k--)(l=r[k])&&j.push(q[k]=l);e(null,r=[],j,i)}k=r.length;while(k--)(l=r[k])&&(j=e?J(f,l):m[k])>-1&&(f[j]=!(g[j]=l))}}else r=va(r===g?r.splice(o,r.length):r),e?e(null,g,r,i):H.apply(g,r)})}function xa(a){for(var b,c,e,f=a.length,g=d.relative[a[0].type],h=g||d.relative[" "],i=g?1:0,k=sa(function(a){return a===b},h,!0),l=sa(function(a){return J(b,a)>-1},h,!0),m=[function(a,c,d){var e=!g&&(d||c!==j)||((b=c).nodeType?k(a,c,d):l(a,c,d));return b=null,e}];f>i;i++)if(c=d.relative[a[i].type])m=[sa(ta(m),c)];else{if(c=d.filter[a[i].type].apply(null,a[i].matches),c[u]){for(e=++i;f>e;e++)if(d.relative[a[e].type])break;return wa(i>1&&ta(m),i>1&&ra(a.slice(0,i-1).concat({value:" "===a[i-2].type?"*":""})).replace(R,"$1"),c,e>i&&xa(a.slice(i,e)),f>e&&xa(a=a.slice(e)),f>e&&ra(a))}m.push(c)}return ta(m)}function ya(a,b){var c=b.length>0,e=a.length>0,f=function(f,g,h,i,k){var l,m,o,p=0,q="0",r=f&&[],s=[],t=j,u=f||e&&d.find.TAG("*",k),v=w+=null==t?1:Math.random()||.1,x=u.length;for(k&&(j=g!==n&&g);q!==x&&null!=(l=u[q]);q++){if(e&&l){m=0;while(o=a[m++])if(o(l,g,h)){i.push(l);break}k&&(w=v)}c&&((l=!o&&l)&&p--,f&&r.push(l))}if(p+=q,c&&q!==p){m=0;while(o=b[m++])o(r,s,g,h);if(f){if(p>0)while(q--)r[q]||s[q]||(s[q]=F.call(i));s=va(s)}H.apply(i,s),k&&!f&&s.length>0&&p+b.length>1&&ga.uniqueSort(i)}return k&&(w=v,j=t),r};return c?ia(f):f}return h=ga.compile=function(a,b){var c,d=[],e=[],f=A[a+" "];if(!f){b||(b=g(a)),c=b.length;while(c--)f=xa(b[c]),f[u]?d.push(f):e.push(f);f=A(a,ya(e,d)),f.selector=a}return f},i=ga.select=function(a,b,e,f){var i,j,k,l,m,n="function"==typeof a&&a,o=!f&&g(a=n.selector||a);if(e=e||[],1===o.length){if(j=o[0]=o[0].slice(0),j.length>2&&"ID"===(k=j[0]).type&&c.getById&&9===b.nodeType&&p&&d.relative[j[1].type]){if(b=(d.find.ID(k.matches[0].replace(ca,da),b)||[])[0],!b)return e;n&&(b=b.parentNode),a=a.slice(j.shift().value.length)}i=X.needsContext.test(a)?0:j.length;while(i--){if(k=j[i],d.relative[l=k.type])break;if((m=d.find[l])&&(f=m(k.matches[0].replace(ca,da),aa.test(j[0].type)&&pa(b.parentNode)||b))){if(j.splice(i,1),a=f.length&&ra(j),!a)return H.apply(e,f),e;break}}}return(n||h(a,o))(f,b,!p,e,aa.test(a)&&pa(b.parentNode)||b),e},c.sortStable=u.split("").sort(B).join("")===u,c.detectDuplicates=!!l,m(),c.sortDetached=ja(function(a){return 1&a.compareDocumentPosition(n.createElement("div"))}),ja(function(a){return a.innerHTML="<a href='#'></a>","#"===a.firstChild.getAttribute("href")})||ka("type|href|height|width",function(a,b,c){return c?void 0:a.getAttribute(b,"type"===b.toLowerCase()?1:2)}),c.attributes&&ja(function(a){return a.innerHTML="<input/>",a.firstChild.setAttribute("value",""),""===a.firstChild.getAttribute("value")})||ka("value",function(a,b,c){return c||"input"!==a.nodeName.toLowerCase()?void 0:a.defaultValue}),ja(function(a){return null==a.getAttribute("disabled")})||ka(K,function(a,b,c){var d;return c?void 0:a[b]===!0?b.toLowerCase():(d=a.getAttributeNode(b))&&d.specified?d.value:null}),ga}(a);n.find=t,n.expr=t.selectors,n.expr[":"]=n.expr.pseudos,n.unique=t.uniqueSort,n.text=t.getText,n.isXMLDoc=t.isXML,n.contains=t.contains;var u=n.expr.match.needsContext,v=/^<(\w+)\s*\/?>(?:<\/\1>|)$/,w=/^.[^:#\[\.,]*$/;function x(a,b,c){if(n.isFunction(b))return n.grep(a,function(a,d){return!!b.call(a,d,a)!==c});if(b.nodeType)return n.grep(a,function(a){return a===b!==c});if("string"==typeof b){if(w.test(b))return n.filter(b,a,c);b=n.filter(b,a)}return n.grep(a,function(a){return g.call(b,a)>=0!==c})}n.filter=function(a,b,c){var d=b[0];return c&&(a=":not("+a+")"),1===b.length&&1===d.nodeType?n.find.matchesSelector(d,a)?[d]:[]:n.find.matches(a,n.grep(b,function(a){return 1===a.nodeType}))},n.fn.extend({find:function(a){var b,c=this.length,d=[],e=this;if("string"!=typeof a)return this.pushStack(n(a).filter(function(){for(b=0;c>b;b++)if(n.contains(e[b],this))return!0}));for(b=0;c>b;b++)n.find(a,e[b],d);return d=this.pushStack(c>1?n.unique(d):d),d.selector=this.selector?this.selector+" "+a:a,d},filter:function(a){return this.pushStack(x(this,a||[],!1))},not:function(a){return this.pushStack(x(this,a||[],!0))},is:function(a){return!!x(this,"string"==typeof a&&u.test(a)?n(a):a||[],!1).length}});var y,z=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,A=n.fn.init=function(a,b){var c,d;if(!a)return this;if("string"==typeof a){if(c="<"===a[0]&&">"===a[a.length-1]&&a.length>=3?[null,a,null]:z.exec(a),!c||!c[1]&&b)return!b||b.jquery?(b||y).find(a):this.constructor(b).find(a);if(c[1]){if(b=b instanceof n?b[0]:b,n.merge(this,n.parseHTML(c[1],b&&b.nodeType?b.ownerDocument||b:l,!0)),v.test(c[1])&&n.isPlainObject(b))for(c in b)n.isFunction(this[c])?this[c](b[c]):this.attr(c,b[c]);return this}return d=l.getElementById(c[2]),d&&d.parentNode&&(this.length=1,this[0]=d),this.context=l,this.selector=a,this}return a.nodeType?(this.context=this[0]=a,this.length=1,this):n.isFunction(a)?"undefined"!=typeof y.ready?y.ready(a):a(n):(void 0!==a.selector&&(this.selector=a.selector,this.context=a.context),n.makeArray(a,this))};A.prototype=n.fn,y=n(l);var B=/^(?:parents|prev(?:Until|All))/,C={children:!0,contents:!0,next:!0,prev:!0};n.extend({dir:function(a,b,c){var d=[],e=void 0!==c;while((a=a[b])&&9!==a.nodeType)if(1===a.nodeType){if(e&&n(a).is(c))break;d.push(a)}return d},sibling:function(a,b){for(var c=[];a;a=a.nextSibling)1===a.nodeType&&a!==b&&c.push(a);return c}}),n.fn.extend({has:function(a){var b=n(a,this),c=b.length;return this.filter(function(){for(var a=0;c>a;a++)if(n.contains(this,b[a]))return!0})},closest:function(a,b){for(var c,d=0,e=this.length,f=[],g=u.test(a)||"string"!=typeof a?n(a,b||this.context):0;e>d;d++)for(c=this[d];c&&c!==b;c=c.parentNode)if(c.nodeType<11&&(g?g.index(c)>-1:1===c.nodeType&&n.find.matchesSelector(c,a))){f.push(c);break}return this.pushStack(f.length>1?n.unique(f):f)},index:function(a){return a?"string"==typeof a?g.call(n(a),this[0]):g.call(this,a.jquery?a[0]:a):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(a,b){return this.pushStack(n.unique(n.merge(this.get(),n(a,b))))},addBack:function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))}});function D(a,b){while((a=a[b])&&1!==a.nodeType);return a}n.each({parent:function(a){var b=a.parentNode;return b&&11!==b.nodeType?b:null},parents:function(a){return n.dir(a,"parentNode")},parentsUntil:function(a,b,c){return n.dir(a,"parentNode",c)},next:function(a){return D(a,"nextSibling")},prev:function(a){return D(a,"previousSibling")},nextAll:function(a){return n.dir(a,"nextSibling")},prevAll:function(a){return n.dir(a,"previousSibling")},nextUntil:function(a,b,c){return n.dir(a,"nextSibling",c)},prevUntil:function(a,b,c){return n.dir(a,"previousSibling",c)},siblings:function(a){return n.sibling((a.parentNode||{}).firstChild,a)},children:function(a){return n.sibling(a.firstChild)},contents:function(a){return a.contentDocument||n.merge([],a.childNodes)}},function(a,b){n.fn[a]=function(c,d){var e=n.map(this,b,c);return"Until"!==a.slice(-5)&&(d=c),d&&"string"==typeof d&&(e=n.filter(d,e)),this.length>1&&(C[a]||n.unique(e),B.test(a)&&e.reverse()),this.pushStack(e)}});var E=/\S+/g,F={};function G(a){var b=F[a]={};return n.each(a.match(E)||[],function(a,c){b[c]=!0}),b}n.Callbacks=function(a){a="string"==typeof a?F[a]||G(a):n.extend({},a);var b,c,d,e,f,g,h=[],i=!a.once&&[],j=function(l){for(b=a.memory&&l,c=!0,g=e||0,e=0,f=h.length,d=!0;h&&f>g;g++)if(h[g].apply(l[0],l[1])===!1&&a.stopOnFalse){b=!1;break}d=!1,h&&(i?i.length&&j(i.shift()):b?h=[]:k.disable())},k={add:function(){if(h){var c=h.length;!function g(b){n.each(b,function(b,c){var d=n.type(c);"function"===d?a.unique&&k.has(c)||h.push(c):c&&c.length&&"string"!==d&&g(c)})}(arguments),d?f=h.length:b&&(e=c,j(b))}return this},remove:function(){return h&&n.each(arguments,function(a,b){var c;while((c=n.inArray(b,h,c))>-1)h.splice(c,1),d&&(f>=c&&f--,g>=c&&g--)}),this},has:function(a){return a?n.inArray(a,h)>-1:!(!h||!h.length)},empty:function(){return h=[],f=0,this},disable:function(){return h=i=b=void 0,this},disabled:function(){return!h},lock:function(){return i=void 0,b||k.disable(),this},locked:function(){return!i},fireWith:function(a,b){return!h||c&&!i||(b=b||[],b=[a,b.slice?b.slice():b],d?i.push(b):j(b)),this},fire:function(){return k.fireWith(this,arguments),this},fired:function(){return!!c}};return k},n.extend({Deferred:function(a){var b=[["resolve","done",n.Callbacks("once memory"),"resolved"],["reject","fail",n.Callbacks("once memory"),"rejected"],["notify","progress",n.Callbacks("memory")]],c="pending",d={state:function(){return c},always:function(){return e.done(arguments).fail(arguments),this},then:function(){var a=arguments;return n.Deferred(function(c){n.each(b,function(b,f){var g=n.isFunction(a[b])&&a[b];e[f[1]](function(){var a=g&&g.apply(this,arguments);a&&n.isFunction(a.promise)?a.promise().done(c.resolve).fail(c.reject).progress(c.notify):c[f[0]+"With"](this===d?c.promise():this,g?[a]:arguments)})}),a=null}).promise()},promise:function(a){return null!=a?n.extend(a,d):d}},e={};return d.pipe=d.then,n.each(b,function(a,f){var g=f[2],h=f[3];d[f[1]]=g.add,h&&g.add(function(){c=h},b[1^a][2].disable,b[2][2].lock),e[f[0]]=function(){return e[f[0]+"With"](this===e?d:this,arguments),this},e[f[0]+"With"]=g.fireWith}),d.promise(e),a&&a.call(e,e),e},when:function(a){var b=0,c=d.call(arguments),e=c.length,f=1!==e||a&&n.isFunction(a.promise)?e:0,g=1===f?a:n.Deferred(),h=function(a,b,c){return function(e){b[a]=this,c[a]=arguments.length>1?d.call(arguments):e,c===i?g.notifyWith(b,c):--f||g.resolveWith(b,c)}},i,j,k;if(e>1)for(i=new Array(e),j=new Array(e),k=new Array(e);e>b;b++)c[b]&&n.isFunction(c[b].promise)?c[b].promise().done(h(b,k,c)).fail(g.reject).progress(h(b,j,i)):--f;return f||g.resolveWith(k,c),g.promise()}});var H;n.fn.ready=function(a){return n.ready.promise().done(a),this},n.extend({isReady:!1,readyWait:1,holdReady:function(a){a?n.readyWait++:n.ready(!0)},ready:function(a){(a===!0?--n.readyWait:n.isReady)||(n.isReady=!0,a!==!0&&--n.readyWait>0||(H.resolveWith(l,[n]),n.fn.triggerHandler&&(n(l).triggerHandler("ready"),n(l).off("ready"))))}});function I(){l.removeEventListener("DOMContentLoaded",I,!1),a.removeEventListener("load",I,!1),n.ready()}n.ready.promise=function(b){return H||(H=n.Deferred(),"complete"===l.readyState?setTimeout(n.ready):(l.addEventListener("DOMContentLoaded",I,!1),a.addEventListener("load",I,!1))),H.promise(b)},n.ready.promise();var J=n.access=function(a,b,c,d,e,f,g){var h=0,i=a.length,j=null==c;if("object"===n.type(c)){e=!0;for(h in c)n.access(a,b,h,c[h],!0,f,g)}else if(void 0!==d&&(e=!0,n.isFunction(d)||(g=!0),j&&(g?(b.call(a,d),b=null):(j=b,b=function(a,b,c){return j.call(n(a),c)})),b))for(;i>h;h++)b(a[h],c,g?d:d.call(a[h],h,b(a[h],c)));return e?a:j?b.call(a):i?b(a[0],c):f};n.acceptData=function(a){return 1===a.nodeType||9===a.nodeType||!+a.nodeType};function K(){Object.defineProperty(this.cache={},0,{get:function(){return{}}}),this.expando=n.expando+K.uid++}K.uid=1,K.accepts=n.acceptData,K.prototype={key:function(a){if(!K.accepts(a))return 0;var b={},c=a[this.expando];if(!c){c=K.uid++;try{b[this.expando]={value:c},Object.defineProperties(a,b)}catch(d){b[this.expando]=c,n.extend(a,b)}}return this.cache[c]||(this.cache[c]={}),c},set:function(a,b,c){var d,e=this.key(a),f=this.cache[e];if("string"==typeof b)f[b]=c;else if(n.isEmptyObject(f))n.extend(this.cache[e],b);else for(d in b)f[d]=b[d];return f},get:function(a,b){var c=this.cache[this.key(a)];return void 0===b?c:c[b]},access:function(a,b,c){var d;return void 0===b||b&&"string"==typeof b&&void 0===c?(d=this.get(a,b),void 0!==d?d:this.get(a,n.camelCase(b))):(this.set(a,b,c),void 0!==c?c:b)},remove:function(a,b){var c,d,e,f=this.key(a),g=this.cache[f];if(void 0===b)this.cache[f]={};else{n.isArray(b)?d=b.concat(b.map(n.camelCase)):(e=n.camelCase(b),b in g?d=[b,e]:(d=e,d=d in g?[d]:d.match(E)||[])),c=d.length;while(c--)delete g[d[c]]}},hasData:function(a){return!n.isEmptyObject(this.cache[a[this.expando]]||{})},discard:function(a){a[this.expando]&&delete this.cache[a[this.expando]]}};var L=new K,M=new K,N=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,O=/([A-Z])/g;function P(a,b,c){var d;if(void 0===c&&1===a.nodeType)if(d="data-"+b.replace(O,"-$1").toLowerCase(),c=a.getAttribute(d),"string"==typeof c){try{c="true"===c?!0:"false"===c?!1:"null"===c?null:+c+""===c?+c:N.test(c)?n.parseJSON(c):c}catch(e){}M.set(a,b,c)}else c=void 0;return c}n.extend({hasData:function(a){return M.hasData(a)||L.hasData(a)},data:function(a,b,c){
    return M.access(a,b,c)},removeData:function(a,b){M.remove(a,b)},_data:function(a,b,c){return L.access(a,b,c)},_removeData:function(a,b){L.remove(a,b)}}),n.fn.extend({data:function(a,b){var c,d,e,f=this[0],g=f&&f.attributes;if(void 0===a){if(this.length&&(e=M.get(f),1===f.nodeType&&!L.get(f,"hasDataAttrs"))){c=g.length;while(c--)g[c]&&(d=g[c].name,0===d.indexOf("data-")&&(d=n.camelCase(d.slice(5)),P(f,d,e[d])));L.set(f,"hasDataAttrs",!0)}return e}return"object"==typeof a?this.each(function(){M.set(this,a)}):J(this,function(b){var c,d=n.camelCase(a);if(f&&void 0===b){if(c=M.get(f,a),void 0!==c)return c;if(c=M.get(f,d),void 0!==c)return c;if(c=P(f,d,void 0),void 0!==c)return c}else this.each(function(){var c=M.get(this,d);M.set(this,d,b),-1!==a.indexOf("-")&&void 0!==c&&M.set(this,a,b)})},null,b,arguments.length>1,null,!0)},removeData:function(a){return this.each(function(){M.remove(this,a)})}}),n.extend({queue:function(a,b,c){var d;return a?(b=(b||"fx")+"queue",d=L.get(a,b),c&&(!d||n.isArray(c)?d=L.access(a,b,n.makeArray(c)):d.push(c)),d||[]):void 0},dequeue:function(a,b){b=b||"fx";var c=n.queue(a,b),d=c.length,e=c.shift(),f=n._queueHooks(a,b),g=function(){n.dequeue(a,b)};"inprogress"===e&&(e=c.shift(),d--),e&&("fx"===b&&c.unshift("inprogress"),delete f.stop,e.call(a,g,f)),!d&&f&&f.empty.fire()},_queueHooks:function(a,b){var c=b+"queueHooks";return L.get(a,c)||L.access(a,c,{empty:n.Callbacks("once memory").add(function(){L.remove(a,[b+"queue",c])})})}}),n.fn.extend({queue:function(a,b){var c=2;return"string"!=typeof a&&(b=a,a="fx",c--),arguments.length<c?n.queue(this[0],a):void 0===b?this:this.each(function(){var c=n.queue(this,a,b);n._queueHooks(this,a),"fx"===a&&"inprogress"!==c[0]&&n.dequeue(this,a)})},dequeue:function(a){return this.each(function(){n.dequeue(this,a)})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,b){var c,d=1,e=n.Deferred(),f=this,g=this.length,h=function(){--d||e.resolveWith(f,[f])};"string"!=typeof a&&(b=a,a=void 0),a=a||"fx";while(g--)c=L.get(f[g],a+"queueHooks"),c&&c.empty&&(d++,c.empty.add(h));return h(),e.promise(b)}});var Q=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,R=["Top","Right","Bottom","Left"],S=function(a,b){return a=b||a,"none"===n.css(a,"display")||!n.contains(a.ownerDocument,a)},T=/^(?:checkbox|radio)$/i;!function(){var a=l.createDocumentFragment(),b=a.appendChild(l.createElement("div")),c=l.createElement("input");c.setAttribute("type","radio"),c.setAttribute("checked","checked"),c.setAttribute("name","t"),b.appendChild(c),k.checkClone=b.cloneNode(!0).cloneNode(!0).lastChild.checked,b.innerHTML="<textarea>x</textarea>",k.noCloneChecked=!!b.cloneNode(!0).lastChild.defaultValue}();var U="undefined";k.focusinBubbles="onfocusin"in a;var V=/^key/,W=/^(?:mouse|pointer|contextmenu)|click/,X=/^(?:focusinfocus|focusoutblur)$/,Y=/^([^.]*)(?:\.(.+)|)$/;function Z(){return!0}function $(){return!1}function _(){try{return l.activeElement}catch(a){}}n.event={global:{},add:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=L.get(a);if(r){c.handler&&(f=c,c=f.handler,e=f.selector),c.guid||(c.guid=n.guid++),(i=r.events)||(i=r.events={}),(g=r.handle)||(g=r.handle=function(b){return typeof n!==U&&n.event.triggered!==b.type?n.event.dispatch.apply(a,arguments):void 0}),b=(b||"").match(E)||[""],j=b.length;while(j--)h=Y.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o&&(l=n.event.special[o]||{},o=(e?l.delegateType:l.bindType)||o,l=n.event.special[o]||{},k=n.extend({type:o,origType:q,data:d,handler:c,guid:c.guid,selector:e,needsContext:e&&n.expr.match.needsContext.test(e),namespace:p.join(".")},f),(m=i[o])||(m=i[o]=[],m.delegateCount=0,l.setup&&l.setup.call(a,d,p,g)!==!1||a.addEventListener&&a.addEventListener(o,g,!1)),l.add&&(l.add.call(a,k),k.handler.guid||(k.handler.guid=c.guid)),e?m.splice(m.delegateCount++,0,k):m.push(k),n.event.global[o]=!0)}},remove:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=L.hasData(a)&&L.get(a);if(r&&(i=r.events)){b=(b||"").match(E)||[""],j=b.length;while(j--)if(h=Y.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o){l=n.event.special[o]||{},o=(d?l.delegateType:l.bindType)||o,m=i[o]||[],h=h[2]&&new RegExp("(^|\\.)"+p.join("\\.(?:.*\\.|)")+"(\\.|$)"),g=f=m.length;while(f--)k=m[f],!e&&q!==k.origType||c&&c.guid!==k.guid||h&&!h.test(k.namespace)||d&&d!==k.selector&&("**"!==d||!k.selector)||(m.splice(f,1),k.selector&&m.delegateCount--,l.remove&&l.remove.call(a,k));g&&!m.length&&(l.teardown&&l.teardown.call(a,p,r.handle)!==!1||n.removeEvent(a,o,r.handle),delete i[o])}else for(o in i)n.event.remove(a,o+b[j],c,d,!0);n.isEmptyObject(i)&&(delete r.handle,L.remove(a,"events"))}},trigger:function(b,c,d,e){var f,g,h,i,k,m,o,p=[d||l],q=j.call(b,"type")?b.type:b,r=j.call(b,"namespace")?b.namespace.split("."):[];if(g=h=d=d||l,3!==d.nodeType&&8!==d.nodeType&&!X.test(q+n.event.triggered)&&(q.indexOf(".")>=0&&(r=q.split("."),q=r.shift(),r.sort()),k=q.indexOf(":")<0&&"on"+q,b=b[n.expando]?b:new n.Event(q,"object"==typeof b&&b),b.isTrigger=e?2:3,b.namespace=r.join("."),b.namespace_re=b.namespace?new RegExp("(^|\\.)"+r.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,b.result=void 0,b.target||(b.target=d),c=null==c?[b]:n.makeArray(c,[b]),o=n.event.special[q]||{},e||!o.trigger||o.trigger.apply(d,c)!==!1)){if(!e&&!o.noBubble&&!n.isWindow(d)){for(i=o.delegateType||q,X.test(i+q)||(g=g.parentNode);g;g=g.parentNode)p.push(g),h=g;h===(d.ownerDocument||l)&&p.push(h.defaultView||h.parentWindow||a)}f=0;while((g=p[f++])&&!b.isPropagationStopped())b.type=f>1?i:o.bindType||q,m=(L.get(g,"events")||{})[b.type]&&L.get(g,"handle"),m&&m.apply(g,c),m=k&&g[k],m&&m.apply&&n.acceptData(g)&&(b.result=m.apply(g,c),b.result===!1&&b.preventDefault());return b.type=q,e||b.isDefaultPrevented()||o._default&&o._default.apply(p.pop(),c)!==!1||!n.acceptData(d)||k&&n.isFunction(d[q])&&!n.isWindow(d)&&(h=d[k],h&&(d[k]=null),n.event.triggered=q,d[q](),n.event.triggered=void 0,h&&(d[k]=h)),b.result}},dispatch:function(a){a=n.event.fix(a);var b,c,e,f,g,h=[],i=d.call(arguments),j=(L.get(this,"events")||{})[a.type]||[],k=n.event.special[a.type]||{};if(i[0]=a,a.delegateTarget=this,!k.preDispatch||k.preDispatch.call(this,a)!==!1){h=n.event.handlers.call(this,a,j),b=0;while((f=h[b++])&&!a.isPropagationStopped()){a.currentTarget=f.elem,c=0;while((g=f.handlers[c++])&&!a.isImmediatePropagationStopped())(!a.namespace_re||a.namespace_re.test(g.namespace))&&(a.handleObj=g,a.data=g.data,e=((n.event.special[g.origType]||{}).handle||g.handler).apply(f.elem,i),void 0!==e&&(a.result=e)===!1&&(a.preventDefault(),a.stopPropagation()))}return k.postDispatch&&k.postDispatch.call(this,a),a.result}},handlers:function(a,b){var c,d,e,f,g=[],h=b.delegateCount,i=a.target;if(h&&i.nodeType&&(!a.button||"click"!==a.type))for(;i!==this;i=i.parentNode||this)if(i.disabled!==!0||"click"!==a.type){for(d=[],c=0;h>c;c++)f=b[c],e=f.selector+" ",void 0===d[e]&&(d[e]=f.needsContext?n(e,this).index(i)>=0:n.find(e,this,null,[i]).length),d[e]&&d.push(f);d.length&&g.push({elem:i,handlers:d})}return h<b.length&&g.push({elem:this,handlers:b.slice(h)}),g},props:"altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){return null==a.which&&(a.which=null!=b.charCode?b.charCode:b.keyCode),a}},mouseHooks:{props:"button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,b){var c,d,e,f=b.button;return null==a.pageX&&null!=b.clientX&&(c=a.target.ownerDocument||l,d=c.documentElement,e=c.body,a.pageX=b.clientX+(d&&d.scrollLeft||e&&e.scrollLeft||0)-(d&&d.clientLeft||e&&e.clientLeft||0),a.pageY=b.clientY+(d&&d.scrollTop||e&&e.scrollTop||0)-(d&&d.clientTop||e&&e.clientTop||0)),a.which||void 0===f||(a.which=1&f?1:2&f?3:4&f?2:0),a}},fix:function(a){if(a[n.expando])return a;var b,c,d,e=a.type,f=a,g=this.fixHooks[e];g||(this.fixHooks[e]=g=W.test(e)?this.mouseHooks:V.test(e)?this.keyHooks:{}),d=g.props?this.props.concat(g.props):this.props,a=new n.Event(f),b=d.length;while(b--)c=d[b],a[c]=f[c];return a.target||(a.target=l),3===a.target.nodeType&&(a.target=a.target.parentNode),g.filter?g.filter(a,f):a},special:{load:{noBubble:!0},focus:{trigger:function(){return this!==_()&&this.focus?(this.focus(),!1):void 0},delegateType:"focusin"},blur:{trigger:function(){return this===_()&&this.blur?(this.blur(),!1):void 0},delegateType:"focusout"},click:{trigger:function(){return"checkbox"===this.type&&this.click&&n.nodeName(this,"input")?(this.click(),!1):void 0},_default:function(a){return n.nodeName(a.target,"a")}},beforeunload:{postDispatch:function(a){void 0!==a.result&&a.originalEvent&&(a.originalEvent.returnValue=a.result)}}},simulate:function(a,b,c,d){var e=n.extend(new n.Event,c,{type:a,isSimulated:!0,originalEvent:{}});d?n.event.trigger(e,null,b):n.event.dispatch.call(b,e),e.isDefaultPrevented()&&c.preventDefault()}},n.removeEvent=function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c,!1)},n.Event=function(a,b){return this instanceof n.Event?(a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||void 0===a.defaultPrevented&&a.returnValue===!1?Z:$):this.type=a,b&&n.extend(this,b),this.timeStamp=a&&a.timeStamp||n.now(),void(this[n.expando]=!0)):new n.Event(a,b)},n.Event.prototype={isDefaultPrevented:$,isPropagationStopped:$,isImmediatePropagationStopped:$,preventDefault:function(){var a=this.originalEvent;this.isDefaultPrevented=Z,a&&a.preventDefault&&a.preventDefault()},stopPropagation:function(){var a=this.originalEvent;this.isPropagationStopped=Z,a&&a.stopPropagation&&a.stopPropagation()},stopImmediatePropagation:function(){var a=this.originalEvent;this.isImmediatePropagationStopped=Z,a&&a.stopImmediatePropagation&&a.stopImmediatePropagation(),this.stopPropagation()}},n.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(a,b){n.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c,d=this,e=a.relatedTarget,f=a.handleObj;return(!e||e!==d&&!n.contains(d,e))&&(a.type=f.origType,c=f.handler.apply(this,arguments),a.type=b),c}}}),k.focusinBubbles||n.each({focus:"focusin",blur:"focusout"},function(a,b){var c=function(a){n.event.simulate(b,a.target,n.event.fix(a),!0)};n.event.special[b]={setup:function(){var d=this.ownerDocument||this,e=L.access(d,b);e||d.addEventListener(a,c,!0),L.access(d,b,(e||0)+1)},teardown:function(){var d=this.ownerDocument||this,e=L.access(d,b)-1;e?L.access(d,b,e):(d.removeEventListener(a,c,!0),L.remove(d,b))}}}),n.fn.extend({on:function(a,b,c,d,e){var f,g;if("object"==typeof a){"string"!=typeof b&&(c=c||b,b=void 0);for(g in a)this.on(g,b,c,a[g],e);return this}if(null==c&&null==d?(d=b,c=b=void 0):null==d&&("string"==typeof b?(d=c,c=void 0):(d=c,c=b,b=void 0)),d===!1)d=$;else if(!d)return this;return 1===e&&(f=d,d=function(a){return n().off(a),f.apply(this,arguments)},d.guid=f.guid||(f.guid=n.guid++)),this.each(function(){n.event.add(this,a,d,c,b)})},one:function(a,b,c,d){return this.on(a,b,c,d,1)},off:function(a,b,c){var d,e;if(a&&a.preventDefault&&a.handleObj)return d=a.handleObj,n(a.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler),this;if("object"==typeof a){for(e in a)this.off(e,b,a[e]);return this}return(b===!1||"function"==typeof b)&&(c=b,b=void 0),c===!1&&(c=$),this.each(function(){n.event.remove(this,a,c,b)})},trigger:function(a,b){return this.each(function(){n.event.trigger(a,b,this)})},triggerHandler:function(a,b){var c=this[0];return c?n.event.trigger(a,b,c,!0):void 0}});var aa=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,ba=/<([\w:]+)/,ca=/<|&#?\w+;/,da=/<(?:script|style|link)/i,ea=/checked\s*(?:[^=]|=\s*.checked.)/i,fa=/^$|\/(?:java|ecma)script/i,ga=/^true\/(.*)/,ha=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,ia={option:[1,"<select multiple='multiple'>","</select>"],thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};ia.optgroup=ia.option,ia.tbody=ia.tfoot=ia.colgroup=ia.caption=ia.thead,ia.th=ia.td;function ja(a,b){return n.nodeName(a,"table")&&n.nodeName(11!==b.nodeType?b:b.firstChild,"tr")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a}function ka(a){return a.type=(null!==a.getAttribute("type"))+"/"+a.type,a}function la(a){var b=ga.exec(a.type);return b?a.type=b[1]:a.removeAttribute("type"),a}function ma(a,b){for(var c=0,d=a.length;d>c;c++)L.set(a[c],"globalEval",!b||L.get(b[c],"globalEval"))}function na(a,b){var c,d,e,f,g,h,i,j;if(1===b.nodeType){if(L.hasData(a)&&(f=L.access(a),g=L.set(b,f),j=f.events)){delete g.handle,g.events={};for(e in j)for(c=0,d=j[e].length;d>c;c++)n.event.add(b,e,j[e][c])}M.hasData(a)&&(h=M.access(a),i=n.extend({},h),M.set(b,i))}}function oa(a,b){var c=a.getElementsByTagName?a.getElementsByTagName(b||"*"):a.querySelectorAll?a.querySelectorAll(b||"*"):[];return void 0===b||b&&n.nodeName(a,b)?n.merge([a],c):c}function pa(a,b){var c=b.nodeName.toLowerCase();"input"===c&&T.test(a.type)?b.checked=a.checked:("input"===c||"textarea"===c)&&(b.defaultValue=a.defaultValue)}n.extend({clone:function(a,b,c){var d,e,f,g,h=a.cloneNode(!0),i=n.contains(a.ownerDocument,a);if(!(k.noCloneChecked||1!==a.nodeType&&11!==a.nodeType||n.isXMLDoc(a)))for(g=oa(h),f=oa(a),d=0,e=f.length;e>d;d++)pa(f[d],g[d]);if(b)if(c)for(f=f||oa(a),g=g||oa(h),d=0,e=f.length;e>d;d++)na(f[d],g[d]);else na(a,h);return g=oa(h,"script"),g.length>0&&ma(g,!i&&oa(a,"script")),h},buildFragment:function(a,b,c,d){for(var e,f,g,h,i,j,k=b.createDocumentFragment(),l=[],m=0,o=a.length;o>m;m++)if(e=a[m],e||0===e)if("object"===n.type(e))n.merge(l,e.nodeType?[e]:e);else if(ca.test(e)){f=f||k.appendChild(b.createElement("div")),g=(ba.exec(e)||["",""])[1].toLowerCase(),h=ia[g]||ia._default,f.innerHTML=h[1]+e.replace(aa,"<$1></$2>")+h[2],j=h[0];while(j--)f=f.lastChild;n.merge(l,f.childNodes),f=k.firstChild,f.textContent=""}else l.push(b.createTextNode(e));k.textContent="",m=0;while(e=l[m++])if((!d||-1===n.inArray(e,d))&&(i=n.contains(e.ownerDocument,e),f=oa(k.appendChild(e),"script"),i&&ma(f),c)){j=0;while(e=f[j++])fa.test(e.type||"")&&c.push(e)}return k},cleanData:function(a){for(var b,c,d,e,f=n.event.special,g=0;void 0!==(c=a[g]);g++){if(n.acceptData(c)&&(e=c[L.expando],e&&(b=L.cache[e]))){if(b.events)for(d in b.events)f[d]?n.event.remove(c,d):n.removeEvent(c,d,b.handle);L.cache[e]&&delete L.cache[e]}delete M.cache[c[M.expando]]}}}),n.fn.extend({text:function(a){return J(this,function(a){return void 0===a?n.text(this):this.empty().each(function(){(1===this.nodeType||11===this.nodeType||9===this.nodeType)&&(this.textContent=a)})},null,a,arguments.length)},append:function(){return this.domManip(arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=ja(this,a);b.appendChild(a)}})},prepend:function(){return this.domManip(arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=ja(this,a);b.insertBefore(a,b.firstChild)}})},before:function(){return this.domManip(arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)})},after:function(){return this.domManip(arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)})},remove:function(a,b){for(var c,d=a?n.filter(a,this):this,e=0;null!=(c=d[e]);e++)b||1!==c.nodeType||n.cleanData(oa(c)),c.parentNode&&(b&&n.contains(c.ownerDocument,c)&&ma(oa(c,"script")),c.parentNode.removeChild(c));return this},empty:function(){for(var a,b=0;null!=(a=this[b]);b++)1===a.nodeType&&(n.cleanData(oa(a,!1)),a.textContent="");return this},clone:function(a,b){return a=null==a?!1:a,b=null==b?a:b,this.map(function(){return n.clone(this,a,b)})},html:function(a){return J(this,function(a){var b=this[0]||{},c=0,d=this.length;if(void 0===a&&1===b.nodeType)return b.innerHTML;if("string"==typeof a&&!da.test(a)&&!ia[(ba.exec(a)||["",""])[1].toLowerCase()]){a=a.replace(aa,"<$1></$2>");try{for(;d>c;c++)b=this[c]||{},1===b.nodeType&&(n.cleanData(oa(b,!1)),b.innerHTML=a);b=0}catch(e){}}b&&this.empty().append(a)},null,a,arguments.length)},replaceWith:function(){var a=arguments[0];return this.domManip(arguments,function(b){a=this.parentNode,n.cleanData(oa(this)),a&&a.replaceChild(b,this)}),a&&(a.length||a.nodeType)?this:this.remove()},detach:function(a){return this.remove(a,!0)},domManip:function(a,b){a=e.apply([],a);var c,d,f,g,h,i,j=0,l=this.length,m=this,o=l-1,p=a[0],q=n.isFunction(p);if(q||l>1&&"string"==typeof p&&!k.checkClone&&ea.test(p))return this.each(function(c){var d=m.eq(c);q&&(a[0]=p.call(this,c,d.html())),d.domManip(a,b)});if(l&&(c=n.buildFragment(a,this[0].ownerDocument,!1,this),d=c.firstChild,1===c.childNodes.length&&(c=d),d)){for(f=n.map(oa(c,"script"),ka),g=f.length;l>j;j++)h=c,j!==o&&(h=n.clone(h,!0,!0),g&&n.merge(f,oa(h,"script"))),b.call(this[j],h,j);if(g)for(i=f[f.length-1].ownerDocument,n.map(f,la),j=0;g>j;j++)h=f[j],fa.test(h.type||"")&&!L.access(h,"globalEval")&&n.contains(i,h)&&(h.src?n._evalUrl&&n._evalUrl(h.src):n.globalEval(h.textContent.replace(ha,"")))}return this}}),n.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){n.fn[a]=function(a){for(var c,d=[],e=n(a),g=e.length-1,h=0;g>=h;h++)c=h===g?this:this.clone(!0),n(e[h])[b](c),f.apply(d,c.get());return this.pushStack(d)}});var qa,ra={};function sa(b,c){var d,e=n(c.createElement(b)).appendTo(c.body),f=a.getDefaultComputedStyle&&(d=a.getDefaultComputedStyle(e[0]))?d.display:n.css(e[0],"display");return e.detach(),f}function ta(a){var b=l,c=ra[a];return c||(c=sa(a,b),"none"!==c&&c||(qa=(qa||n("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement),b=qa[0].contentDocument,b.write(),b.close(),c=sa(a,b),qa.detach()),ra[a]=c),c}var ua=/^margin/,va=new RegExp("^("+Q+")(?!px)[a-z%]+$","i"),wa=function(b){return b.ownerDocument.defaultView.opener?b.ownerDocument.defaultView.getComputedStyle(b,null):a.getComputedStyle(b,null)};function xa(a,b,c){var d,e,f,g,h=a.style;return c=c||wa(a),c&&(g=c.getPropertyValue(b)||c[b]),c&&(""!==g||n.contains(a.ownerDocument,a)||(g=n.style(a,b)),va.test(g)&&ua.test(b)&&(d=h.width,e=h.minWidth,f=h.maxWidth,h.minWidth=h.maxWidth=h.width=g,g=c.width,h.width=d,h.minWidth=e,h.maxWidth=f)),void 0!==g?g+"":g}function ya(a,b){return{get:function(){return a()?void delete this.get:(this.get=b).apply(this,arguments)}}}!function(){var b,c,d=l.documentElement,e=l.createElement("div"),f=l.createElement("div");if(f.style){f.style.backgroundClip="content-box",f.cloneNode(!0).style.backgroundClip="",k.clearCloneStyle="content-box"===f.style.backgroundClip,e.style.cssText="border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;position:absolute",e.appendChild(f);function g(){f.style.cssText="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute",f.innerHTML="",d.appendChild(e);var g=a.getComputedStyle(f,null);b="1%"!==g.top,c="4px"===g.width,d.removeChild(e)}a.getComputedStyle&&n.extend(k,{pixelPosition:function(){return g(),b},boxSizingReliable:function(){return null==c&&g(),c},reliableMarginRight:function(){var b,c=f.appendChild(l.createElement("div"));return c.style.cssText=f.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0",c.style.marginRight=c.style.width="0",f.style.width="1px",d.appendChild(e),b=!parseFloat(a.getComputedStyle(c,null).marginRight),d.removeChild(e),f.removeChild(c),b}})}}(),n.swap=function(a,b,c,d){var e,f,g={};for(f in b)g[f]=a.style[f],a.style[f]=b[f];e=c.apply(a,d||[]);for(f in b)a.style[f]=g[f];return e};var za=/^(none|table(?!-c[ea]).+)/,Aa=new RegExp("^("+Q+")(.*)$","i"),Ba=new RegExp("^([+-])=("+Q+")","i"),Ca={position:"absolute",visibility:"hidden",display:"block"},Da={letterSpacing:"0",fontWeight:"400"},Ea=["Webkit","O","Moz","ms"];function Fa(a,b){if(b in a)return b;var c=b[0].toUpperCase()+b.slice(1),d=b,e=Ea.length;while(e--)if(b=Ea[e]+c,b in a)return b;return d}function Ga(a,b,c){var d=Aa.exec(b);return d?Math.max(0,d[1]-(c||0))+(d[2]||"px"):b}function Ha(a,b,c,d,e){for(var f=c===(d?"border":"content")?4:"width"===b?1:0,g=0;4>f;f+=2)"margin"===c&&(g+=n.css(a,c+R[f],!0,e)),d?("content"===c&&(g-=n.css(a,"padding"+R[f],!0,e)),"margin"!==c&&(g-=n.css(a,"border"+R[f]+"Width",!0,e))):(g+=n.css(a,"padding"+R[f],!0,e),"padding"!==c&&(g+=n.css(a,"border"+R[f]+"Width",!0,e)));return g}function Ia(a,b,c){var d=!0,e="width"===b?a.offsetWidth:a.offsetHeight,f=wa(a),g="border-box"===n.css(a,"boxSizing",!1,f);if(0>=e||null==e){if(e=xa(a,b,f),(0>e||null==e)&&(e=a.style[b]),va.test(e))return e;d=g&&(k.boxSizingReliable()||e===a.style[b]),e=parseFloat(e)||0}return e+Ha(a,b,c||(g?"border":"content"),d,f)+"px"}function Ja(a,b){for(var c,d,e,f=[],g=0,h=a.length;h>g;g++)d=a[g],d.style&&(f[g]=L.get(d,"olddisplay"),c=d.style.display,b?(f[g]||"none"!==c||(d.style.display=""),""===d.style.display&&S(d)&&(f[g]=L.access(d,"olddisplay",ta(d.nodeName)))):(e=S(d),"none"===c&&e||L.set(d,"olddisplay",e?c:n.css(d,"display"))));for(g=0;h>g;g++)d=a[g],d.style&&(b&&"none"!==d.style.display&&""!==d.style.display||(d.style.display=b?f[g]||"":"none"));return a}n.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=xa(a,"opacity");return""===c?"1":c}}}},cssNumber:{columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":"cssFloat"},style:function(a,b,c,d){if(a&&3!==a.nodeType&&8!==a.nodeType&&a.style){var e,f,g,h=n.camelCase(b),i=a.style;return b=n.cssProps[h]||(n.cssProps[h]=Fa(i,h)),g=n.cssHooks[b]||n.cssHooks[h],void 0===c?g&&"get"in g&&void 0!==(e=g.get(a,!1,d))?e:i[b]:(f=typeof c,"string"===f&&(e=Ba.exec(c))&&(c=(e[1]+1)*e[2]+parseFloat(n.css(a,b)),f="number"),null!=c&&c===c&&("number"!==f||n.cssNumber[h]||(c+="px"),k.clearCloneStyle||""!==c||0!==b.indexOf("background")||(i[b]="inherit"),g&&"set"in g&&void 0===(c=g.set(a,c,d))||(i[b]=c)),void 0)}},css:function(a,b,c,d){var e,f,g,h=n.camelCase(b);return b=n.cssProps[h]||(n.cssProps[h]=Fa(a.style,h)),g=n.cssHooks[b]||n.cssHooks[h],g&&"get"in g&&(e=g.get(a,!0,c)),void 0===e&&(e=xa(a,b,d)),"normal"===e&&b in Da&&(e=Da[b]),""===c||c?(f=parseFloat(e),c===!0||n.isNumeric(f)?f||0:e):e}}),n.each(["height","width"],function(a,b){n.cssHooks[b]={get:function(a,c,d){return c?za.test(n.css(a,"display"))&&0===a.offsetWidth?n.swap(a,Ca,function(){return Ia(a,b,d)}):Ia(a,b,d):void 0},set:function(a,c,d){var e=d&&wa(a);return Ga(a,c,d?Ha(a,b,d,"border-box"===n.css(a,"boxSizing",!1,e),e):0)}}}),n.cssHooks.marginRight=ya(k.reliableMarginRight,function(a,b){return b?n.swap(a,{display:"inline-block"},xa,[a,"marginRight"]):void 0}),n.each({margin:"",padding:"",border:"Width"},function(a,b){n.cssHooks[a+b]={expand:function(c){for(var d=0,e={},f="string"==typeof c?c.split(" "):[c];4>d;d++)e[a+R[d]+b]=f[d]||f[d-2]||f[0];return e}},ua.test(a)||(n.cssHooks[a+b].set=Ga)}),n.fn.extend({css:function(a,b){return J(this,function(a,b,c){var d,e,f={},g=0;if(n.isArray(b)){for(d=wa(a),e=b.length;e>g;g++)f[b[g]]=n.css(a,b[g],!1,d);return f}return void 0!==c?n.style(a,b,c):n.css(a,b)},a,b,arguments.length>1)},show:function(){return Ja(this,!0)},hide:function(){return Ja(this)},toggle:function(a){return"boolean"==typeof a?a?this.show():this.hide():this.each(function(){S(this)?n(this).show():n(this).hide()})}});function Ka(a,b,c,d,e){return new Ka.prototype.init(a,b,c,d,e)}n.Tween=Ka,Ka.prototype={constructor:Ka,init:function(a,b,c,d,e,f){this.elem=a,this.prop=c,this.easing=e||"swing",this.options=b,this.start=this.now=this.cur(),this.end=d,this.unit=f||(n.cssNumber[c]?"":"px")},cur:function(){var a=Ka.propHooks[this.prop];return a&&a.get?a.get(this):Ka.propHooks._default.get(this)},run:function(a){var b,c=Ka.propHooks[this.prop];return this.options.duration?this.pos=b=n.easing[this.easing](a,this.options.duration*a,0,1,this.options.duration):this.pos=b=a,this.now=(this.end-this.start)*b+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),c&&c.set?c.set(this):Ka.propHooks._default.set(this),this}},Ka.prototype.init.prototype=Ka.prototype,Ka.propHooks={_default:{get:function(a){var b;return null==a.elem[a.prop]||a.elem.style&&null!=a.elem.style[a.prop]?(b=n.css(a.elem,a.prop,""),b&&"auto"!==b?b:0):a.elem[a.prop]},set:function(a){n.fx.step[a.prop]?n.fx.step[a.prop](a):a.elem.style&&(null!=a.elem.style[n.cssProps[a.prop]]||n.cssHooks[a.prop])?n.style(a.elem,a.prop,a.now+a.unit):a.elem[a.prop]=a.now}}},Ka.propHooks.scrollTop=Ka.propHooks.scrollLeft={set:function(a){a.elem.nodeType&&a.elem.parentNode&&(a.elem[a.prop]=a.now)}},n.easing={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2}},n.fx=Ka.prototype.init,n.fx.step={};var La,Ma,Na=/^(?:toggle|show|hide)$/,Oa=new RegExp("^(?:([+-])=|)("+Q+")([a-z%]*)$","i"),Pa=/queueHooks$/,Qa=[Va],Ra={"*":[function(a,b){var c=this.createTween(a,b),d=c.cur(),e=Oa.exec(b),f=e&&e[3]||(n.cssNumber[a]?"":"px"),g=(n.cssNumber[a]||"px"!==f&&+d)&&Oa.exec(n.css(c.elem,a)),h=1,i=20;if(g&&g[3]!==f){f=f||g[3],e=e||[],g=+d||1;do h=h||".5",g/=h,n.style(c.elem,a,g+f);while(h!==(h=c.cur()/d)&&1!==h&&--i)}return e&&(g=c.start=+g||+d||0,c.unit=f,c.end=e[1]?g+(e[1]+1)*e[2]:+e[2]),c}]};function Sa(){return setTimeout(function(){La=void 0}),La=n.now()}function Ta(a,b){var c,d=0,e={height:a};for(b=b?1:0;4>d;d+=2-b)c=R[d],e["margin"+c]=e["padding"+c]=a;return b&&(e.opacity=e.width=a),e}function Ua(a,b,c){for(var d,e=(Ra[b]||[]).concat(Ra["*"]),f=0,g=e.length;g>f;f++)if(d=e[f].call(c,b,a))return d}function Va(a,b,c){var d,e,f,g,h,i,j,k,l=this,m={},o=a.style,p=a.nodeType&&S(a),q=L.get(a,"fxshow");c.queue||(h=n._queueHooks(a,"fx"),null==h.unqueued&&(h.unqueued=0,i=h.empty.fire,h.empty.fire=function(){h.unqueued||i()}),h.unqueued++,l.always(function(){l.always(function(){h.unqueued--,n.queue(a,"fx").length||h.empty.fire()})})),1===a.nodeType&&("height"in b||"width"in b)&&(c.overflow=[o.overflow,o.overflowX,o.overflowY],j=n.css(a,"display"),k="none"===j?L.get(a,"olddisplay")||ta(a.nodeName):j,"inline"===k&&"none"===n.css(a,"float")&&(o.display="inline-block")),c.overflow&&(o.overflow="hidden",l.always(function(){o.overflow=c.overflow[0],o.overflowX=c.overflow[1],o.overflowY=c.overflow[2]}));for(d in b)if(e=b[d],Na.exec(e)){if(delete b[d],f=f||"toggle"===e,e===(p?"hide":"show")){if("show"!==e||!q||void 0===q[d])continue;p=!0}m[d]=q&&q[d]||n.style(a,d)}else j=void 0;if(n.isEmptyObject(m))"inline"===("none"===j?ta(a.nodeName):j)&&(o.display=j);else{q?"hidden"in q&&(p=q.hidden):q=L.access(a,"fxshow",{}),f&&(q.hidden=!p),p?n(a).show():l.done(function(){n(a).hide()}),l.done(function(){var b;L.remove(a,"fxshow");for(b in m)n.style(a,b,m[b])});for(d in m)g=Ua(p?q[d]:0,d,l),d in q||(q[d]=g.start,p&&(g.end=g.start,g.start="width"===d||"height"===d?1:0))}}function Wa(a,b){var c,d,e,f,g;for(c in a)if(d=n.camelCase(c),e=b[d],f=a[c],n.isArray(f)&&(e=f[1],f=a[c]=f[0]),c!==d&&(a[d]=f,delete a[c]),g=n.cssHooks[d],g&&"expand"in g){f=g.expand(f),delete a[d];for(c in f)c in a||(a[c]=f[c],b[c]=e)}else b[d]=e}function Xa(a,b,c){var d,e,f=0,g=Qa.length,h=n.Deferred().always(function(){delete i.elem}),i=function(){if(e)return!1;for(var b=La||Sa(),c=Math.max(0,j.startTime+j.duration-b),d=c/j.duration||0,f=1-d,g=0,i=j.tweens.length;i>g;g++)j.tweens[g].run(f);return h.notifyWith(a,[j,f,c]),1>f&&i?c:(h.resolveWith(a,[j]),!1)},j=h.promise({elem:a,props:n.extend({},b),opts:n.extend(!0,{specialEasing:{}},c),originalProperties:b,originalOptions:c,startTime:La||Sa(),duration:c.duration,tweens:[],createTween:function(b,c){var d=n.Tween(a,j.opts,b,c,j.opts.specialEasing[b]||j.opts.easing);return j.tweens.push(d),d},stop:function(b){var c=0,d=b?j.tweens.length:0;if(e)return this;for(e=!0;d>c;c++)j.tweens[c].run(1);return b?h.resolveWith(a,[j,b]):h.rejectWith(a,[j,b]),this}}),k=j.props;for(Wa(k,j.opts.specialEasing);g>f;f++)if(d=Qa[f].call(j,a,k,j.opts))return d;return n.map(k,Ua,j),n.isFunction(j.opts.start)&&j.opts.start.call(a,j),n.fx.timer(n.extend(i,{elem:a,anim:j,queue:j.opts.queue})),j.progress(j.opts.progress).done(j.opts.done,j.opts.complete).fail(j.opts.fail).always(j.opts.always)}n.Animation=n.extend(Xa,{tweener:function(a,b){n.isFunction(a)?(b=a,a=["*"]):a=a.split(" ");for(var c,d=0,e=a.length;e>d;d++)c=a[d],Ra[c]=Ra[c]||[],Ra[c].unshift(b)},prefilter:function(a,b){b?Qa.unshift(a):Qa.push(a)}}),n.speed=function(a,b,c){var d=a&&"object"==typeof a?n.extend({},a):{complete:c||!c&&b||n.isFunction(a)&&a,duration:a,easing:c&&b||b&&!n.isFunction(b)&&b};return d.duration=n.fx.off?0:"number"==typeof d.duration?d.duration:d.duration in n.fx.speeds?n.fx.speeds[d.duration]:n.fx.speeds._default,(null==d.queue||d.queue===!0)&&(d.queue="fx"),d.old=d.complete,d.complete=function(){n.isFunction(d.old)&&d.old.call(this),d.queue&&n.dequeue(this,d.queue)},d},n.fn.extend({fadeTo:function(a,b,c,d){return this.filter(S).css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){var e=n.isEmptyObject(a),f=n.speed(b,c,d),g=function(){var b=Xa(this,n.extend({},a),f);(e||L.get(this,"finish"))&&b.stop(!0)};return g.finish=g,e||f.queue===!1?this.each(g):this.queue(f.queue,g)},stop:function(a,b,c){var d=function(a){var b=a.stop;delete a.stop,b(c)};return"string"!=typeof a&&(c=b,b=a,a=void 0),b&&a!==!1&&this.queue(a||"fx",[]),this.each(function(){var b=!0,e=null!=a&&a+"queueHooks",f=n.timers,g=L.get(this);if(e)g[e]&&g[e].stop&&d(g[e]);else for(e in g)g[e]&&g[e].stop&&Pa.test(e)&&d(g[e]);for(e=f.length;e--;)f[e].elem!==this||null!=a&&f[e].queue!==a||(f[e].anim.stop(c),b=!1,f.splice(e,1));(b||!c)&&n.dequeue(this,a)})},finish:function(a){return a!==!1&&(a=a||"fx"),this.each(function(){var b,c=L.get(this),d=c[a+"queue"],e=c[a+"queueHooks"],f=n.timers,g=d?d.length:0;for(c.finish=!0,n.queue(this,a,[]),e&&e.stop&&e.stop.call(this,!0),b=f.length;b--;)f[b].elem===this&&f[b].queue===a&&(f[b].anim.stop(!0),f.splice(b,1));for(b=0;g>b;b++)d[b]&&d[b].finish&&d[b].finish.call(this);delete c.finish})}}),n.each(["toggle","show","hide"],function(a,b){var c=n.fn[b];n.fn[b]=function(a,d,e){return null==a||"boolean"==typeof a?c.apply(this,arguments):this.animate(Ta(b,!0),a,d,e)}}),n.each({slideDown:Ta("show"),slideUp:Ta("hide"),slideToggle:Ta("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){n.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),n.timers=[],n.fx.tick=function(){var a,b=0,c=n.timers;for(La=n.now();b<c.length;b++)a=c[b],a()||c[b]!==a||c.splice(b--,1);c.length||n.fx.stop(),La=void 0},n.fx.timer=function(a){n.timers.push(a),a()?n.fx.start():n.timers.pop()},n.fx.interval=13,n.fx.start=function(){Ma||(Ma=setInterval(n.fx.tick,n.fx.interval))},n.fx.stop=function(){clearInterval(Ma),Ma=null},n.fx.speeds={slow:600,fast:200,_default:400},n.fn.delay=function(a,b){return a=n.fx?n.fx.speeds[a]||a:a,b=b||"fx",this.queue(b,function(b,c){var d=setTimeout(b,a);c.stop=function(){clearTimeout(d)}})},function(){var a=l.createElement("input"),b=l.createElement("select"),c=b.appendChild(l.createElement("option"));a.type="checkbox",k.checkOn=""!==a.value,k.optSelected=c.selected,b.disabled=!0,k.optDisabled=!c.disabled,a=l.createElement("input"),a.value="t",a.type="radio",k.radioValue="t"===a.value}();var Ya,Za,$a=n.expr.attrHandle;n.fn.extend({attr:function(a,b){return J(this,n.attr,a,b,arguments.length>1)},removeAttr:function(a){return this.each(function(){n.removeAttr(this,a)})}}),n.extend({attr:function(a,b,c){var d,e,f=a.nodeType;if(a&&3!==f&&8!==f&&2!==f)return typeof a.getAttribute===U?n.prop(a,b,c):(1===f&&n.isXMLDoc(a)||(b=b.toLowerCase(),d=n.attrHooks[b]||(n.expr.match.bool.test(b)?Za:Ya)),
    void 0===c?d&&"get"in d&&null!==(e=d.get(a,b))?e:(e=n.find.attr(a,b),null==e?void 0:e):null!==c?d&&"set"in d&&void 0!==(e=d.set(a,c,b))?e:(a.setAttribute(b,c+""),c):void n.removeAttr(a,b))},removeAttr:function(a,b){var c,d,e=0,f=b&&b.match(E);if(f&&1===a.nodeType)while(c=f[e++])d=n.propFix[c]||c,n.expr.match.bool.test(c)&&(a[d]=!1),a.removeAttribute(c)},attrHooks:{type:{set:function(a,b){if(!k.radioValue&&"radio"===b&&n.nodeName(a,"input")){var c=a.value;return a.setAttribute("type",b),c&&(a.value=c),b}}}}}),Za={set:function(a,b,c){return b===!1?n.removeAttr(a,c):a.setAttribute(c,c),c}},n.each(n.expr.match.bool.source.match(/\w+/g),function(a,b){var c=$a[b]||n.find.attr;$a[b]=function(a,b,d){var e,f;return d||(f=$a[b],$a[b]=e,e=null!=c(a,b,d)?b.toLowerCase():null,$a[b]=f),e}});var _a=/^(?:input|select|textarea|button)$/i;n.fn.extend({prop:function(a,b){return J(this,n.prop,a,b,arguments.length>1)},removeProp:function(a){return this.each(function(){delete this[n.propFix[a]||a]})}}),n.extend({propFix:{"for":"htmlFor","class":"className"},prop:function(a,b,c){var d,e,f,g=a.nodeType;if(a&&3!==g&&8!==g&&2!==g)return f=1!==g||!n.isXMLDoc(a),f&&(b=n.propFix[b]||b,e=n.propHooks[b]),void 0!==c?e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:a[b]=c:e&&"get"in e&&null!==(d=e.get(a,b))?d:a[b]},propHooks:{tabIndex:{get:function(a){return a.hasAttribute("tabindex")||_a.test(a.nodeName)||a.href?a.tabIndex:-1}}}}),k.optSelected||(n.propHooks.selected={get:function(a){var b=a.parentNode;return b&&b.parentNode&&b.parentNode.selectedIndex,null}}),n.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){n.propFix[this.toLowerCase()]=this});var ab=/[\t\r\n\f]/g;n.fn.extend({addClass:function(a){var b,c,d,e,f,g,h="string"==typeof a&&a,i=0,j=this.length;if(n.isFunction(a))return this.each(function(b){n(this).addClass(a.call(this,b,this.className))});if(h)for(b=(a||"").match(E)||[];j>i;i++)if(c=this[i],d=1===c.nodeType&&(c.className?(" "+c.className+" ").replace(ab," "):" ")){f=0;while(e=b[f++])d.indexOf(" "+e+" ")<0&&(d+=e+" ");g=n.trim(d),c.className!==g&&(c.className=g)}return this},removeClass:function(a){var b,c,d,e,f,g,h=0===arguments.length||"string"==typeof a&&a,i=0,j=this.length;if(n.isFunction(a))return this.each(function(b){n(this).removeClass(a.call(this,b,this.className))});if(h)for(b=(a||"").match(E)||[];j>i;i++)if(c=this[i],d=1===c.nodeType&&(c.className?(" "+c.className+" ").replace(ab," "):"")){f=0;while(e=b[f++])while(d.indexOf(" "+e+" ")>=0)d=d.replace(" "+e+" "," ");g=a?n.trim(d):"",c.className!==g&&(c.className=g)}return this},toggleClass:function(a,b){var c=typeof a;return"boolean"==typeof b&&"string"===c?b?this.addClass(a):this.removeClass(a):this.each(n.isFunction(a)?function(c){n(this).toggleClass(a.call(this,c,this.className,b),b)}:function(){if("string"===c){var b,d=0,e=n(this),f=a.match(E)||[];while(b=f[d++])e.hasClass(b)?e.removeClass(b):e.addClass(b)}else(c===U||"boolean"===c)&&(this.className&&L.set(this,"__className__",this.className),this.className=this.className||a===!1?"":L.get(this,"__className__")||"")})},hasClass:function(a){for(var b=" "+a+" ",c=0,d=this.length;d>c;c++)if(1===this[c].nodeType&&(" "+this[c].className+" ").replace(ab," ").indexOf(b)>=0)return!0;return!1}});var bb=/\r/g;n.fn.extend({val:function(a){var b,c,d,e=this[0];{if(arguments.length)return d=n.isFunction(a),this.each(function(c){var e;1===this.nodeType&&(e=d?a.call(this,c,n(this).val()):a,null==e?e="":"number"==typeof e?e+="":n.isArray(e)&&(e=n.map(e,function(a){return null==a?"":a+""})),b=n.valHooks[this.type]||n.valHooks[this.nodeName.toLowerCase()],b&&"set"in b&&void 0!==b.set(this,e,"value")||(this.value=e))});if(e)return b=n.valHooks[e.type]||n.valHooks[e.nodeName.toLowerCase()],b&&"get"in b&&void 0!==(c=b.get(e,"value"))?c:(c=e.value,"string"==typeof c?c.replace(bb,""):null==c?"":c)}}}),n.extend({valHooks:{option:{get:function(a){var b=n.find.attr(a,"value");return null!=b?b:n.trim(n.text(a))}},select:{get:function(a){for(var b,c,d=a.options,e=a.selectedIndex,f="select-one"===a.type||0>e,g=f?null:[],h=f?e+1:d.length,i=0>e?h:f?e:0;h>i;i++)if(c=d[i],!(!c.selected&&i!==e||(k.optDisabled?c.disabled:null!==c.getAttribute("disabled"))||c.parentNode.disabled&&n.nodeName(c.parentNode,"optgroup"))){if(b=n(c).val(),f)return b;g.push(b)}return g},set:function(a,b){var c,d,e=a.options,f=n.makeArray(b),g=e.length;while(g--)d=e[g],(d.selected=n.inArray(d.value,f)>=0)&&(c=!0);return c||(a.selectedIndex=-1),f}}}}),n.each(["radio","checkbox"],function(){n.valHooks[this]={set:function(a,b){return n.isArray(b)?a.checked=n.inArray(n(a).val(),b)>=0:void 0}},k.checkOn||(n.valHooks[this].get=function(a){return null===a.getAttribute("value")?"on":a.value})}),n.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){n.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)}}),n.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)},bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return 1===arguments.length?this.off(a,"**"):this.off(b,a||"**",c)}});var cb=n.now(),db=/\?/;n.parseJSON=function(a){return JSON.parse(a+"")},n.parseXML=function(a){var b,c;if(!a||"string"!=typeof a)return null;try{c=new DOMParser,b=c.parseFromString(a,"text/xml")}catch(d){b=void 0}return(!b||b.getElementsByTagName("parsererror").length)&&n.error("Invalid XML: "+a),b};var eb=/#.*$/,fb=/([?&])_=[^&]*/,gb=/^(.*?):[ \t]*([^\r\n]*)$/gm,hb=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,ib=/^(?:GET|HEAD)$/,jb=/^\/\//,kb=/^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,lb={},mb={},nb="*/".concat("*"),ob=a.location.href,pb=kb.exec(ob.toLowerCase())||[];function qb(a){return function(b,c){"string"!=typeof b&&(c=b,b="*");var d,e=0,f=b.toLowerCase().match(E)||[];if(n.isFunction(c))while(d=f[e++])"+"===d[0]?(d=d.slice(1)||"*",(a[d]=a[d]||[]).unshift(c)):(a[d]=a[d]||[]).push(c)}}function rb(a,b,c,d){var e={},f=a===mb;function g(h){var i;return e[h]=!0,n.each(a[h]||[],function(a,h){var j=h(b,c,d);return"string"!=typeof j||f||e[j]?f?!(i=j):void 0:(b.dataTypes.unshift(j),g(j),!1)}),i}return g(b.dataTypes[0])||!e["*"]&&g("*")}function sb(a,b){var c,d,e=n.ajaxSettings.flatOptions||{};for(c in b)void 0!==b[c]&&((e[c]?a:d||(d={}))[c]=b[c]);return d&&n.extend(!0,a,d),a}function tb(a,b,c){var d,e,f,g,h=a.contents,i=a.dataTypes;while("*"===i[0])i.shift(),void 0===d&&(d=a.mimeType||b.getResponseHeader("Content-Type"));if(d)for(e in h)if(h[e]&&h[e].test(d)){i.unshift(e);break}if(i[0]in c)f=i[0];else{for(e in c){if(!i[0]||a.converters[e+" "+i[0]]){f=e;break}g||(g=e)}f=f||g}return f?(f!==i[0]&&i.unshift(f),c[f]):void 0}function ub(a,b,c,d){var e,f,g,h,i,j={},k=a.dataTypes.slice();if(k[1])for(g in a.converters)j[g.toLowerCase()]=a.converters[g];f=k.shift();while(f)if(a.responseFields[f]&&(c[a.responseFields[f]]=b),!i&&d&&a.dataFilter&&(b=a.dataFilter(b,a.dataType)),i=f,f=k.shift())if("*"===f)f=i;else if("*"!==i&&i!==f){if(g=j[i+" "+f]||j["* "+f],!g)for(e in j)if(h=e.split(" "),h[1]===f&&(g=j[i+" "+h[0]]||j["* "+h[0]])){g===!0?g=j[e]:j[e]!==!0&&(f=h[0],k.unshift(h[1]));break}if(g!==!0)if(g&&a["throws"])b=g(b);else try{b=g(b)}catch(l){return{state:"parsererror",error:g?l:"No conversion from "+i+" to "+f}}}return{state:"success",data:b}}n.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:ob,type:"GET",isLocal:hb.test(pb[1]),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":nb,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/xml/,html:/html/,json:/json/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":n.parseJSON,"text xml":n.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(a,b){return b?sb(sb(a,n.ajaxSettings),b):sb(n.ajaxSettings,a)},ajaxPrefilter:qb(lb),ajaxTransport:qb(mb),ajax:function(a,b){"object"==typeof a&&(b=a,a=void 0),b=b||{};var c,d,e,f,g,h,i,j,k=n.ajaxSetup({},b),l=k.context||k,m=k.context&&(l.nodeType||l.jquery)?n(l):n.event,o=n.Deferred(),p=n.Callbacks("once memory"),q=k.statusCode||{},r={},s={},t=0,u="canceled",v={readyState:0,getResponseHeader:function(a){var b;if(2===t){if(!f){f={};while(b=gb.exec(e))f[b[1].toLowerCase()]=b[2]}b=f[a.toLowerCase()]}return null==b?null:b},getAllResponseHeaders:function(){return 2===t?e:null},setRequestHeader:function(a,b){var c=a.toLowerCase();return t||(a=s[c]=s[c]||a,r[a]=b),this},overrideMimeType:function(a){return t||(k.mimeType=a),this},statusCode:function(a){var b;if(a)if(2>t)for(b in a)q[b]=[q[b],a[b]];else v.always(a[v.status]);return this},abort:function(a){var b=a||u;return c&&c.abort(b),x(0,b),this}};if(o.promise(v).complete=p.add,v.success=v.done,v.error=v.fail,k.url=((a||k.url||ob)+"").replace(eb,"").replace(jb,pb[1]+"//"),k.type=b.method||b.type||k.method||k.type,k.dataTypes=n.trim(k.dataType||"*").toLowerCase().match(E)||[""],null==k.crossDomain&&(h=kb.exec(k.url.toLowerCase()),k.crossDomain=!(!h||h[1]===pb[1]&&h[2]===pb[2]&&(h[3]||("http:"===h[1]?"80":"443"))===(pb[3]||("http:"===pb[1]?"80":"443")))),k.data&&k.processData&&"string"!=typeof k.data&&(k.data=n.param(k.data,k.traditional)),rb(lb,k,b,v),2===t)return v;i=n.event&&k.global,i&&0===n.active++&&n.event.trigger("ajaxStart"),k.type=k.type.toUpperCase(),k.hasContent=!ib.test(k.type),d=k.url,k.hasContent||(k.data&&(d=k.url+=(db.test(d)?"&":"?")+k.data,delete k.data),k.cache===!1&&(k.url=fb.test(d)?d.replace(fb,"$1_="+cb++):d+(db.test(d)?"&":"?")+"_="+cb++)),k.ifModified&&(n.lastModified[d]&&v.setRequestHeader("If-Modified-Since",n.lastModified[d]),n.etag[d]&&v.setRequestHeader("If-None-Match",n.etag[d])),(k.data&&k.hasContent&&k.contentType!==!1||b.contentType)&&v.setRequestHeader("Content-Type",k.contentType),v.setRequestHeader("Accept",k.dataTypes[0]&&k.accepts[k.dataTypes[0]]?k.accepts[k.dataTypes[0]]+("*"!==k.dataTypes[0]?", "+nb+"; q=0.01":""):k.accepts["*"]);for(j in k.headers)v.setRequestHeader(j,k.headers[j]);if(k.beforeSend&&(k.beforeSend.call(l,v,k)===!1||2===t))return v.abort();u="abort";for(j in{success:1,error:1,complete:1})v[j](k[j]);if(c=rb(mb,k,b,v)){v.readyState=1,i&&m.trigger("ajaxSend",[v,k]),k.async&&k.timeout>0&&(g=setTimeout(function(){v.abort("timeout")},k.timeout));try{t=1,c.send(r,x)}catch(w){if(!(2>t))throw w;x(-1,w)}}else x(-1,"No Transport");function x(a,b,f,h){var j,r,s,u,w,x=b;2!==t&&(t=2,g&&clearTimeout(g),c=void 0,e=h||"",v.readyState=a>0?4:0,j=a>=200&&300>a||304===a,f&&(u=tb(k,v,f)),u=ub(k,u,v,j),j?(k.ifModified&&(w=v.getResponseHeader("Last-Modified"),w&&(n.lastModified[d]=w),w=v.getResponseHeader("etag"),w&&(n.etag[d]=w)),204===a||"HEAD"===k.type?x="nocontent":304===a?x="notmodified":(x=u.state,r=u.data,s=u.error,j=!s)):(s=x,(a||!x)&&(x="error",0>a&&(a=0))),v.status=a,v.statusText=(b||x)+"",j?o.resolveWith(l,[r,x,v]):o.rejectWith(l,[v,x,s]),v.statusCode(q),q=void 0,i&&m.trigger(j?"ajaxSuccess":"ajaxError",[v,k,j?r:s]),p.fireWith(l,[v,x]),i&&(m.trigger("ajaxComplete",[v,k]),--n.active||n.event.trigger("ajaxStop")))}return v},getJSON:function(a,b,c){return n.get(a,b,c,"json")},getScript:function(a,b){return n.get(a,void 0,b,"script")}}),n.each(["get","post"],function(a,b){n[b]=function(a,c,d,e){return n.isFunction(c)&&(e=e||d,d=c,c=void 0),n.ajax({url:a,type:b,dataType:e,data:c,success:d})}}),n._evalUrl=function(a){return n.ajax({url:a,type:"GET",dataType:"script",async:!1,global:!1,"throws":!0})},n.fn.extend({wrapAll:function(a){var b;return n.isFunction(a)?this.each(function(b){n(this).wrapAll(a.call(this,b))}):(this[0]&&(b=n(a,this[0].ownerDocument).eq(0).clone(!0),this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstElementChild)a=a.firstElementChild;return a}).append(this)),this)},wrapInner:function(a){return this.each(n.isFunction(a)?function(b){n(this).wrapInner(a.call(this,b))}:function(){var b=n(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=n.isFunction(a);return this.each(function(c){n(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(){return this.parent().each(function(){n.nodeName(this,"body")||n(this).replaceWith(this.childNodes)}).end()}}),n.expr.filters.hidden=function(a){return a.offsetWidth<=0&&a.offsetHeight<=0},n.expr.filters.visible=function(a){return!n.expr.filters.hidden(a)};var vb=/%20/g,wb=/\[\]$/,xb=/\r?\n/g,yb=/^(?:submit|button|image|reset|file)$/i,zb=/^(?:input|select|textarea|keygen)/i;function Ab(a,b,c,d){var e;if(n.isArray(b))n.each(b,function(b,e){c||wb.test(a)?d(a,e):Ab(a+"["+("object"==typeof e?b:"")+"]",e,c,d)});else if(c||"object"!==n.type(b))d(a,b);else for(e in b)Ab(a+"["+e+"]",b[e],c,d)}n.param=function(a,b){var c,d=[],e=function(a,b){b=n.isFunction(b)?b():null==b?"":b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)};if(void 0===b&&(b=n.ajaxSettings&&n.ajaxSettings.traditional),n.isArray(a)||a.jquery&&!n.isPlainObject(a))n.each(a,function(){e(this.name,this.value)});else for(c in a)Ab(c,a[c],b,e);return d.join("&").replace(vb,"+")},n.fn.extend({serialize:function(){return n.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var a=n.prop(this,"elements");return a?n.makeArray(a):this}).filter(function(){var a=this.type;return this.name&&!n(this).is(":disabled")&&zb.test(this.nodeName)&&!yb.test(a)&&(this.checked||!T.test(a))}).map(function(a,b){var c=n(this).val();return null==c?null:n.isArray(c)?n.map(c,function(a){return{name:b.name,value:a.replace(xb,"\r\n")}}):{name:b.name,value:c.replace(xb,"\r\n")}}).get()}}),n.ajaxSettings.xhr=function(){try{return new XMLHttpRequest}catch(a){}};var Bb=0,Cb={},Db={0:200,1223:204},Eb=n.ajaxSettings.xhr();a.attachEvent&&a.attachEvent("onunload",function(){for(var a in Cb)Cb[a]()}),k.cors=!!Eb&&"withCredentials"in Eb,k.ajax=Eb=!!Eb,n.ajaxTransport(function(a){var b;return k.cors||Eb&&!a.crossDomain?{send:function(c,d){var e,f=a.xhr(),g=++Bb;if(f.open(a.type,a.url,a.async,a.username,a.password),a.xhrFields)for(e in a.xhrFields)f[e]=a.xhrFields[e];a.mimeType&&f.overrideMimeType&&f.overrideMimeType(a.mimeType),a.crossDomain||c["X-Requested-With"]||(c["X-Requested-With"]="XMLHttpRequest");for(e in c)f.setRequestHeader(e,c[e]);b=function(a){return function(){b&&(delete Cb[g],b=f.onload=f.onerror=null,"abort"===a?f.abort():"error"===a?d(f.status,f.statusText):d(Db[f.status]||f.status,f.statusText,"string"==typeof f.responseText?{text:f.responseText}:void 0,f.getAllResponseHeaders()))}},f.onload=b(),f.onerror=b("error"),b=Cb[g]=b("abort");try{f.send(a.hasContent&&a.data||null)}catch(h){if(b)throw h}},abort:function(){b&&b()}}:void 0}),n.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/(?:java|ecma)script/},converters:{"text script":function(a){return n.globalEval(a),a}}}),n.ajaxPrefilter("script",function(a){void 0===a.cache&&(a.cache=!1),a.crossDomain&&(a.type="GET")}),n.ajaxTransport("script",function(a){if(a.crossDomain){var b,c;return{send:function(d,e){b=n("<script>").prop({async:!0,charset:a.scriptCharset,src:a.url}).on("load error",c=function(a){b.remove(),c=null,a&&e("error"===a.type?404:200,a.type)}),l.head.appendChild(b[0])},abort:function(){c&&c()}}}});var Fb=[],Gb=/(=)\?(?=&|$)|\?\?/;n.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var a=Fb.pop()||n.expando+"_"+cb++;return this[a]=!0,a}}),n.ajaxPrefilter("json jsonp",function(b,c,d){var e,f,g,h=b.jsonp!==!1&&(Gb.test(b.url)?"url":"string"==typeof b.data&&!(b.contentType||"").indexOf("application/x-www-form-urlencoded")&&Gb.test(b.data)&&"data");return h||"jsonp"===b.dataTypes[0]?(e=b.jsonpCallback=n.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,h?b[h]=b[h].replace(Gb,"$1"+e):b.jsonp!==!1&&(b.url+=(db.test(b.url)?"&":"?")+b.jsonp+"="+e),b.converters["script json"]=function(){return g||n.error(e+" was not called"),g[0]},b.dataTypes[0]="json",f=a[e],a[e]=function(){g=arguments},d.always(function(){a[e]=f,b[e]&&(b.jsonpCallback=c.jsonpCallback,Fb.push(e)),g&&n.isFunction(f)&&f(g[0]),g=f=void 0}),"script"):void 0}),n.parseHTML=function(a,b,c){if(!a||"string"!=typeof a)return null;"boolean"==typeof b&&(c=b,b=!1),b=b||l;var d=v.exec(a),e=!c&&[];return d?[b.createElement(d[1])]:(d=n.buildFragment([a],b,e),e&&e.length&&n(e).remove(),n.merge([],d.childNodes))};var Hb=n.fn.load;n.fn.load=function(a,b,c){if("string"!=typeof a&&Hb)return Hb.apply(this,arguments);var d,e,f,g=this,h=a.indexOf(" ");return h>=0&&(d=n.trim(a.slice(h)),a=a.slice(0,h)),n.isFunction(b)?(c=b,b=void 0):b&&"object"==typeof b&&(e="POST"),g.length>0&&n.ajax({url:a,type:e,dataType:"html",data:b}).done(function(a){f=arguments,g.html(d?n("<div>").append(n.parseHTML(a)).find(d):a)}).complete(c&&function(a,b){g.each(c,f||[a.responseText,b,a])}),this},n.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(a,b){n.fn[b]=function(a){return this.on(b,a)}}),n.expr.filters.animated=function(a){return n.grep(n.timers,function(b){return a===b.elem}).length};var Ib=a.document.documentElement;function Jb(a){return n.isWindow(a)?a:9===a.nodeType&&a.defaultView}n.offset={setOffset:function(a,b,c){var d,e,f,g,h,i,j,k=n.css(a,"position"),l=n(a),m={};"static"===k&&(a.style.position="relative"),h=l.offset(),f=n.css(a,"top"),i=n.css(a,"left"),j=("absolute"===k||"fixed"===k)&&(f+i).indexOf("auto")>-1,j?(d=l.position(),g=d.top,e=d.left):(g=parseFloat(f)||0,e=parseFloat(i)||0),n.isFunction(b)&&(b=b.call(a,c,h)),null!=b.top&&(m.top=b.top-h.top+g),null!=b.left&&(m.left=b.left-h.left+e),"using"in b?b.using.call(a,m):l.css(m)}},n.fn.extend({offset:function(a){if(arguments.length)return void 0===a?this:this.each(function(b){n.offset.setOffset(this,a,b)});var b,c,d=this[0],e={top:0,left:0},f=d&&d.ownerDocument;if(f)return b=f.documentElement,n.contains(b,d)?(typeof d.getBoundingClientRect!==U&&(e=d.getBoundingClientRect()),c=Jb(f),{top:e.top+c.pageYOffset-b.clientTop,left:e.left+c.pageXOffset-b.clientLeft}):e},position:function(){if(this[0]){var a,b,c=this[0],d={top:0,left:0};return"fixed"===n.css(c,"position")?b=c.getBoundingClientRect():(a=this.offsetParent(),b=this.offset(),n.nodeName(a[0],"html")||(d=a.offset()),d.top+=n.css(a[0],"borderTopWidth",!0),d.left+=n.css(a[0],"borderLeftWidth",!0)),{top:b.top-d.top-n.css(c,"marginTop",!0),left:b.left-d.left-n.css(c,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var a=this.offsetParent||Ib;while(a&&!n.nodeName(a,"html")&&"static"===n.css(a,"position"))a=a.offsetParent;return a||Ib})}}),n.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(b,c){var d="pageYOffset"===c;n.fn[b]=function(e){return J(this,function(b,e,f){var g=Jb(b);return void 0===f?g?g[c]:b[e]:void(g?g.scrollTo(d?a.pageXOffset:f,d?f:a.pageYOffset):b[e]=f)},b,e,arguments.length,null)}}),n.each(["top","left"],function(a,b){n.cssHooks[b]=ya(k.pixelPosition,function(a,c){return c?(c=xa(a,b),va.test(c)?n(a).position()[b]+"px":c):void 0})}),n.each({Height:"height",Width:"width"},function(a,b){n.each({padding:"inner"+a,content:b,"":"outer"+a},function(c,d){n.fn[d]=function(d,e){var f=arguments.length&&(c||"boolean"!=typeof d),g=c||(d===!0||e===!0?"margin":"border");return J(this,function(b,c,d){var e;return n.isWindow(b)?b.document.documentElement["client"+a]:9===b.nodeType?(e=b.documentElement,Math.max(b.body["scroll"+a],e["scroll"+a],b.body["offset"+a],e["offset"+a],e["client"+a])):void 0===d?n.css(b,c,g):n.style(b,c,d,g)},b,f?d:void 0,f,null)}})}),n.fn.size=function(){return this.length},n.fn.andSelf=n.fn.addBack,"function"==typeof define&&define.amd&&define("jquery",[],function(){return n});var Kb=a.jQuery,Lb=a.$;return n.noConflict=function(b){return a.$===n&&(a.$=Lb),b&&a.jQuery===n&&(a.jQuery=Kb),n},typeof b===U&&(a.jQuery=a.$=n),n});
/*! jQuery Migrate v1.2.1 | (c) 2005, 2013 jQuery Foundation, Inc. and other contributors | jquery.org/license */
jQuery.migrateMute === void 0 && (jQuery.migrateMute = !0), function (e, t, n) {
    function r(n) {
        var r = t.console;
        i[n] || (i[n] = !0, e.migrateWarnings.push(n), r && r.warn && !e.migrateMute && (r.warn("JQMIGRATE: " + n), e.migrateTrace && r.trace && r.trace()))
    }

    function a(t, a, i, o) {
        if (Object.defineProperty)try {
            return Object.defineProperty(t, a, {
                configurable: !0, enumerable: !0, get: function () {
                    return r(o), i
                }, set: function (e) {
                    r(o), i = e
                }
            }), n
        } catch (s) {
        }
        e._definePropertyBroken = !0, t[a] = i
    }

    var i = {};
    e.migrateWarnings = [], !e.migrateMute && t.console && t.console.log && t.console.log("JQMIGRATE: Logging is active"), e.migrateTrace === n && (e.migrateTrace = !0), e.migrateReset = function () {
        i = {}, e.migrateWarnings.length = 0
    }, "BackCompat" === document.compatMode && r("jQuery is not compatible with Quirks Mode");
    var o = e("<input/>", {size: 1}).attr("size") && e.attrFn, s = e.attr, u = e.attrHooks.value && e.attrHooks.value.get || function () {
            return null
        }, c = e.attrHooks.value && e.attrHooks.value.set || function () {
            return n
        }, l = /^(?:input|button)$/i, d = /^[238]$/, p = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i, f = /^(?:checked|selected)$/i;
    a(e, "attrFn", o || {}, "jQuery.attrFn is deprecated"), e.attr = function (t, a, i, u) {
        var c = a.toLowerCase(), g = t && t.nodeType;
        return u && (4 > s.length && r("jQuery.fn.attr( props, pass ) is deprecated"), t && !d.test(g) && (o ? a in o : e.isFunction(e.fn[a]))) ? e(t)[a](i) : ("type" === a && i !== n && l.test(t.nodeName) && t.parentNode && r("Can't change the 'type' of an input or button in IE 6/7/8"), !e.attrHooks[c] && p.test(c) && (e.attrHooks[c] = {
            get: function (t, r) {
                var a, i = e.prop(t, r);
                return i === !0 || "boolean" != typeof i && (a = t.getAttributeNode(r)) && a.nodeValue !== !1 ? r.toLowerCase() : n
            }, set: function (t, n, r) {
                var a;
                return n === !1 ? e.removeAttr(t, r) : (a = e.propFix[r] || r, a in t && (t[a] = !0), t.setAttribute(r, r.toLowerCase())), r
            }
        }, f.test(c) && r("jQuery.fn.attr('" + c + "') may use property instead of attribute")), s.call(e, t, a, i))
    }, e.attrHooks.value = {
        get: function (e, t) {
            var n = (e.nodeName || "").toLowerCase();
            return "button" === n ? u.apply(this, arguments) : ("input" !== n && "option" !== n && r("jQuery.fn.attr('value') no longer gets properties"), t in e ? e.value : null)
        }, set: function (e, t) {
            var a = (e.nodeName || "").toLowerCase();
            return "button" === a ? c.apply(this, arguments) : ("input" !== a && "option" !== a && r("jQuery.fn.attr('value', val) no longer sets properties"), e.value = t, n)
        }
    };
    var g, h, v = e.fn.init, m = e.parseJSON, y = /^([^<]*)(<[\w\W]+>)([^>]*)$/;
    e.fn.init = function (t, n, a) {
        var i;
        return t && "string" == typeof t && !e.isPlainObject(n) && (i = y.exec(e.trim(t))) && i[0] && ("<" !== t.charAt(0) && r("$(html) HTML strings must start with '<' character"), i[3] && r("$(html) HTML text after last tag is ignored"), "#" === i[0].charAt(0) && (r("HTML string cannot start with a '#' character"), e.error("JQMIGRATE: Invalid selector string (XSS)")), n && n.context && (n = n.context), e.parseHTML) ? v.call(this, e.parseHTML(i[2], n, !0), n, a) : v.apply(this, arguments)
    }, e.fn.init.prototype = e.fn, e.parseJSON = function (e) {
        return e || null === e ? m.apply(this, arguments) : (r("jQuery.parseJSON requires a valid JSON string"), null)
    }, e.uaMatch = function (e) {
        e = e.toLowerCase();
        var t = /(chrome)[ \/]([\w.]+)/.exec(e) || /(webkit)[ \/]([\w.]+)/.exec(e) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(e) || /(msie) ([\w.]+)/.exec(e) || 0 > e.indexOf("compatible") && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(e) || [];
        return {browser: t[1] || "", version: t[2] || "0"}
    }, e.browser || (g = e.uaMatch(navigator.userAgent), h = {}, g.browser && (h[g.browser] = !0, h.version = g.version), h.chrome ? h.webkit = !0 : h.webkit && (h.safari = !0), e.browser = h), a(e, "browser", e.browser, "jQuery.browser is deprecated"), e.sub = function () {
        function t(e, n) {
            return new t.fn.init(e, n)
        }

        e.extend(!0, t, this), t.superclass = this, t.fn = t.prototype = this(), t.fn.constructor = t, t.sub = this.sub, t.fn.init = function (r, a) {
            return a && a instanceof e && !(a instanceof t) && (a = t(a)), e.fn.init.call(this, r, a, n)
        }, t.fn.init.prototype = t.fn;
        var n = t(document);
        return r("jQuery.sub() is deprecated"), t
    }, e.ajaxSetup({converters: {"text json": e.parseJSON}});
    var b = e.fn.data;
    e.fn.data = function (t) {
        var a, i, o = this[0];
        return !o || "events" !== t || 1 !== arguments.length || (a = e.data(o, t), i = e._data(o, t), a !== n && a !== i || i === n) ? b.apply(this, arguments) : (r("Use of jQuery.fn.data('events') is deprecated"), i)
    };
    var j = /\/(java|ecma)script/i, w = e.fn.andSelf || e.fn.addBack;
    e.fn.andSelf = function () {
        return r("jQuery.fn.andSelf() replaced by jQuery.fn.addBack()"), w.apply(this, arguments)
    }, e.clean || (e.clean = function (t, a, i, o) {
        a = a || document, a = !a.nodeType && a[0] || a, a = a.ownerDocument || a, r("jQuery.clean() is deprecated");
        var s, u, c, l, d = [];
        if (e.merge(d, e.buildFragment(t, a).childNodes), i)for (c = function (e) {
            return !e.type || j.test(e.type) ? o ? o.push(e.parentNode ? e.parentNode.removeChild(e) : e) : i.appendChild(e) : n
        }, s = 0; null != (u = d[s]); s++)e.nodeName(u, "script") && c(u) || (i.appendChild(u), u.getElementsByTagName !== n && (l = e.grep(e.merge([], u.getElementsByTagName("script")), c), d.splice.apply(d, [s + 1, 0].concat(l)), s += l.length));
        return d
    });
    var Q = e.event.add, x = e.event.remove, k = e.event.trigger, N = e.fn.toggle, T = e.fn.live, M = e.fn.die, S = "ajaxStart|ajaxStop|ajaxSend|ajaxComplete|ajaxError|ajaxSuccess", C = RegExp("\\b(?:" + S + ")\\b"), H = /(?:^|\s)hover(\.\S+|)\b/, A = function (t) {
        return "string" != typeof t || e.event.special.hover ? t : (H.test(t) && r("'hover' pseudo-event is deprecated, use 'mouseenter mouseleave'"), t && t.replace(H, "mouseenter$1 mouseleave$1"))
    };
    e.event.props && "attrChange" !== e.event.props[0] && e.event.props.unshift("attrChange", "attrName", "relatedNode", "srcElement"), e.event.dispatch && a(e.event, "handle", e.event.dispatch, "jQuery.event.handle is undocumented and deprecated"), e.event.add = function (e, t, n, a, i) {
        e !== document && C.test(t) && r("AJAX events should be attached to document: " + t), Q.call(this, e, A(t || ""), n, a, i)
    }, e.event.remove = function (e, t, n, r, a) {
        x.call(this, e, A(t) || "", n, r, a)
    }, e.fn.error = function () {
        var e = Array.prototype.slice.call(arguments, 0);
        return r("jQuery.fn.error() is deprecated"), e.splice(0, 0, "error"), arguments.length ? this.bind.apply(this, e) : (this.triggerHandler.apply(this, e), this)
    }, e.fn.toggle = function (t, n) {
        if (!e.isFunction(t) || !e.isFunction(n))return N.apply(this, arguments);
        r("jQuery.fn.toggle(handler, handler...) is deprecated");
        var a = arguments, i = t.guid || e.guid++, o = 0, s = function (n) {
            var r = (e._data(this, "lastToggle" + t.guid) || 0) % o;
            return e._data(this, "lastToggle" + t.guid, r + 1), n.preventDefault(), a[r].apply(this, arguments) || !1
        };
        for (s.guid = i; a.length > o;)a[o++].guid = i;
        return this.click(s)
    }, e.fn.live = function (t, n, a) {
        return r("jQuery.fn.live() is deprecated"), T ? T.apply(this, arguments) : (e(this.context).on(t, this.selector, n, a), this)
    }, e.fn.die = function (t, n) {
        return r("jQuery.fn.die() is deprecated"), M ? M.apply(this, arguments) : (e(this.context).off(t, this.selector || "**", n), this)
    }, e.event.trigger = function (e, t, n, a) {
        return n || C.test(e) || r("Global events are undocumented and deprecated"), k.call(this, e, t, n || document, a)
    }, e.each(S.split("|"), function (t, n) {
        e.event.special[n] = {
            setup: function () {
                var t = this;
                return t !== document && (e.event.add(document, n + "." + e.guid, function () {
                    e.event.trigger(n, null, t, !0)
                }), e._data(this, n, e.guid++)), !1
            }, teardown: function () {
                return this !== document && e.event.remove(document, n + "." + e._data(this, n)), !1
            }
        }
    })
}(jQuery, window);
/*!
 * Bootstrap v3.3.5 (http://getbootstrap.com)
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under the MIT license
 */

if (typeof jQuery === 'undefined') {
  throw new Error('Bootstrap\'s JavaScript requires jQuery')
}

+function ($) {
  'use strict';
  var version = $.fn.jquery.split(' ')[0].split('.')
  if ((version[0] < 2 && version[1] < 9) || (version[0] == 1 && version[1] == 9 && version[2] < 1)) {
    throw new Error('Bootstrap\'s JavaScript requires jQuery version 1.9.1 or higher')
  }
}(jQuery);

/* ========================================================================
 * Bootstrap: transition.js v3.3.5
 * http://getbootstrap.com/javascript/#transitions
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
  // ============================================================

  function transitionEnd() {
    var el = document.createElement('bootstrap')

    var transEndEventNames = {
      WebkitTransition : 'webkitTransitionEnd',
      MozTransition    : 'transitionend',
      OTransition      : 'oTransitionEnd otransitionend',
      transition       : 'transitionend'
    }

    for (var name in transEndEventNames) {
      if (el.style[name] !== undefined) {
        return { end: transEndEventNames[name] }
      }
    }

    return false // explicit for ie8 (  ._.)
  }

  // http://blog.alexmaccaw.com/css-transitions
  $.fn.emulateTransitionEnd = function (duration) {
    var called = false
    var $el = this
    $(this).one('bsTransitionEnd', function () { called = true })
    var callback = function () { if (!called) $($el).trigger($.support.transition.end) }
    setTimeout(callback, duration)
    return this
  }

  $(function () {
    $.support.transition = transitionEnd()

    if (!$.support.transition) return

    $.event.special.bsTransitionEnd = {
      bindType: $.support.transition.end,
      delegateType: $.support.transition.end,
      handle: function (e) {
        if ($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
      }
    }
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: alert.js v3.3.5
 * http://getbootstrap.com/javascript/#alerts
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // ALERT CLASS DEFINITION
  // ======================

  var dismiss = '[data-dismiss="alert"]'
  var Alert   = function (el) {
    $(el).on('click', dismiss, this.close)
  }

  Alert.VERSION = '3.3.5'

  Alert.TRANSITION_DURATION = 150

  Alert.prototype.close = function (e) {
    var $this    = $(this)
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = $(selector)

    if (e) e.preventDefault()

    if (!$parent.length) {
      $parent = $this.closest('.alert')
    }

    $parent.trigger(e = $.Event('close.bs.alert'))

    if (e.isDefaultPrevented()) return

    $parent.removeClass('in')

    function removeElement() {
      // detach from parent, fire event then clean up data
      $parent.detach().trigger('closed.bs.alert').remove()
    }

    $.support.transition && $parent.hasClass('fade') ?
      $parent
        .one('bsTransitionEnd', removeElement)
        .emulateTransitionEnd(Alert.TRANSITION_DURATION) :
      removeElement()
  }


  // ALERT PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.alert')

      if (!data) $this.data('bs.alert', (data = new Alert(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.alert

  $.fn.alert             = Plugin
  $.fn.alert.Constructor = Alert


  // ALERT NO CONFLICT
  // =================

  $.fn.alert.noConflict = function () {
    $.fn.alert = old
    return this
  }


  // ALERT DATA-API
  // ==============

  $(document).on('click.bs.alert.data-api', dismiss, Alert.prototype.close)

}(jQuery);

/* ========================================================================
 * Bootstrap: button.js v3.3.5
 * http://getbootstrap.com/javascript/#buttons
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // BUTTON PUBLIC CLASS DEFINITION
  // ==============================

  var Button = function (element, options) {
    this.$element  = $(element)
    this.options   = $.extend({}, Button.DEFAULTS, options)
    this.isLoading = false
  }

  Button.VERSION  = '3.3.5'

  Button.DEFAULTS = {
    loadingText: 'loading...'
  }

  Button.prototype.setState = function (state) {
    var d    = 'disabled'
    var $el  = this.$element
    var val  = $el.is('input') ? 'val' : 'html'
    var data = $el.data()

    state += 'Text'

    if (data.resetText == null) $el.data('resetText', $el[val]())

    // push to event loop to allow forms to submit
    setTimeout($.proxy(function () {
      $el[val](data[state] == null ? this.options[state] : data[state])

      if (state == 'loadingText') {
        this.isLoading = true
        $el.addClass(d).attr(d, d)
      } else if (this.isLoading) {
        this.isLoading = false
        $el.removeClass(d).removeAttr(d)
      }
    }, this), 0)
  }

  Button.prototype.toggle = function () {
    var changed = true
    var $parent = this.$element.closest('[data-toggle="buttons"]')

    if ($parent.length) {
      var $input = this.$element.find('input')
      if ($input.prop('type') == 'radio') {
        if ($input.prop('checked')) changed = false
        $parent.find('.active').removeClass('active')
        this.$element.addClass('active')
      } else if ($input.prop('type') == 'checkbox') {
        if (($input.prop('checked')) !== this.$element.hasClass('active')) changed = false
        this.$element.toggleClass('active')
      }
      $input.prop('checked', this.$element.hasClass('active'))
      if (changed) $input.trigger('change')
    } else {
      this.$element.attr('aria-pressed', !this.$element.hasClass('active'))
      this.$element.toggleClass('active')
    }
  }


  // BUTTON PLUGIN DEFINITION
  // ========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.button')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.button', (data = new Button(this, options)))

      if (option == 'toggle') data.toggle()
      else if (option) data.setState(option)
    })
  }

  var old = $.fn.button

  $.fn.button             = Plugin
  $.fn.button.Constructor = Button


  // BUTTON NO CONFLICT
  // ==================

  $.fn.button.noConflict = function () {
    $.fn.button = old
    return this
  }


  // BUTTON DATA-API
  // ===============

  $(document)
    .on('click.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      var $btn = $(e.target)
      if (!$btn.hasClass('btn')) $btn = $btn.closest('.btn')
      Plugin.call($btn, 'toggle')
      if (!($(e.target).is('input[type="radio"]') || $(e.target).is('input[type="checkbox"]'))) e.preventDefault()
    })
    .on('focus.bs.button.data-api blur.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      $(e.target).closest('.btn').toggleClass('focus', /^focus(in)?$/.test(e.type))
    })

}(jQuery);

/* ========================================================================
 * Bootstrap: carousel.js v3.3.5
 * http://getbootstrap.com/javascript/#carousel
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CAROUSEL CLASS DEFINITION
  // =========================

  var Carousel = function (element, options) {
    this.$element    = $(element)
    this.$indicators = this.$element.find('.carousel-indicators')
    this.options     = options
    this.paused      = null
    this.sliding     = null
    this.interval    = null
    this.$active     = null
    this.$items      = null

    this.options.keyboard && this.$element.on('keydown.bs.carousel', $.proxy(this.keydown, this))

    this.options.pause == 'hover' && !('ontouchstart' in document.documentElement) && this.$element
      .on('mouseenter.bs.carousel', $.proxy(this.pause, this))
      .on('mouseleave.bs.carousel', $.proxy(this.cycle, this))
  }

  Carousel.VERSION  = '3.3.5'

  Carousel.TRANSITION_DURATION = 600

  Carousel.DEFAULTS = {
    interval: 5000,
    pause: 'hover',
    wrap: true,
    keyboard: true
  }

  Carousel.prototype.keydown = function (e) {
    if (/input|textarea/i.test(e.target.tagName)) return
    switch (e.which) {
      case 37: this.prev(); break
      case 39: this.next(); break
      default: return
    }

    e.preventDefault()
  }

  Carousel.prototype.cycle = function (e) {
    e || (this.paused = false)

    this.interval && clearInterval(this.interval)

    this.options.interval
      && !this.paused
      && (this.interval = setInterval($.proxy(this.next, this), this.options.interval))

    return this
  }

  Carousel.prototype.getItemIndex = function (item) {
    this.$items = item.parent().children('.item')
    return this.$items.index(item || this.$active)
  }

  Carousel.prototype.getItemForDirection = function (direction, active) {
    var activeIndex = this.getItemIndex(active)
    var willWrap = (direction == 'prev' && activeIndex === 0)
                || (direction == 'next' && activeIndex == (this.$items.length - 1))
    if (willWrap && !this.options.wrap) return active
    var delta = direction == 'prev' ? -1 : 1
    var itemIndex = (activeIndex + delta) % this.$items.length
    return this.$items.eq(itemIndex)
  }

  Carousel.prototype.to = function (pos) {
    var that        = this
    var activeIndex = this.getItemIndex(this.$active = this.$element.find('.item.active'))

    if (pos > (this.$items.length - 1) || pos < 0) return

    if (this.sliding)       return this.$element.one('slid.bs.carousel', function () { that.to(pos) }) // yes, "slid"
    if (activeIndex == pos) return this.pause().cycle()

    return this.slide(pos > activeIndex ? 'next' : 'prev', this.$items.eq(pos))
  }

  Carousel.prototype.pause = function (e) {
    e || (this.paused = true)

    if (this.$element.find('.next, .prev').length && $.support.transition) {
      this.$element.trigger($.support.transition.end)
      this.cycle(true)
    }

    this.interval = clearInterval(this.interval)

    return this
  }

  Carousel.prototype.next = function () {
    if (this.sliding) return
    return this.slide('next')
  }

  Carousel.prototype.prev = function () {
    if (this.sliding) return
    return this.slide('prev')
  }

  Carousel.prototype.slide = function (type, next) {
    var $active   = this.$element.find('.item.active')
    var $next     = next || this.getItemForDirection(type, $active)
    var isCycling = this.interval
    var direction = type == 'next' ? 'left' : 'right'
    var that      = this

    if ($next.hasClass('active')) return (this.sliding = false)

    var relatedTarget = $next[0]
    var slideEvent = $.Event('slide.bs.carousel', {
      relatedTarget: relatedTarget,
      direction: direction
    })
    this.$element.trigger(slideEvent)
    if (slideEvent.isDefaultPrevented()) return

    this.sliding = true

    isCycling && this.pause()

    if (this.$indicators.length) {
      this.$indicators.find('.active').removeClass('active')
      var $nextIndicator = $(this.$indicators.children()[this.getItemIndex($next)])
      $nextIndicator && $nextIndicator.addClass('active')
    }

    var slidEvent = $.Event('slid.bs.carousel', { relatedTarget: relatedTarget, direction: direction }) // yes, "slid"
    if ($.support.transition && this.$element.hasClass('slide')) {
      $next.addClass(type)
      $next[0].offsetWidth // force reflow
      $active.addClass(direction)
      $next.addClass(direction)
      $active
        .one('bsTransitionEnd', function () {
          $next.removeClass([type, direction].join(' ')).addClass('active')
          $active.removeClass(['active', direction].join(' '))
          that.sliding = false
          setTimeout(function () {
            that.$element.trigger(slidEvent)
          }, 0)
        })
        .emulateTransitionEnd(Carousel.TRANSITION_DURATION)
    } else {
      $active.removeClass('active')
      $next.addClass('active')
      this.sliding = false
      this.$element.trigger(slidEvent)
    }

    isCycling && this.cycle()

    return this
  }


  // CAROUSEL PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.carousel')
      var options = $.extend({}, Carousel.DEFAULTS, $this.data(), typeof option == 'object' && option)
      var action  = typeof option == 'string' ? option : options.slide

      if (!data) $this.data('bs.carousel', (data = new Carousel(this, options)))
      if (typeof option == 'number') data.to(option)
      else if (action) data[action]()
      else if (options.interval) data.pause().cycle()
    })
  }

  var old = $.fn.carousel

  $.fn.carousel             = Plugin
  $.fn.carousel.Constructor = Carousel


  // CAROUSEL NO CONFLICT
  // ====================

  $.fn.carousel.noConflict = function () {
    $.fn.carousel = old
    return this
  }


  // CAROUSEL DATA-API
  // =================

  var clickHandler = function (e) {
    var href
    var $this   = $(this)
    var $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) // strip for ie7
    if (!$target.hasClass('carousel')) return
    var options = $.extend({}, $target.data(), $this.data())
    var slideIndex = $this.attr('data-slide-to')
    if (slideIndex) options.interval = false

    Plugin.call($target, options)

    if (slideIndex) {
      $target.data('bs.carousel').to(slideIndex)
    }

    e.preventDefault()
  }

  $(document)
    .on('click.bs.carousel.data-api', '[data-slide]', clickHandler)
    .on('click.bs.carousel.data-api', '[data-slide-to]', clickHandler)

  $(window).on('load', function () {
    $('[data-ride="carousel"]').each(function () {
      var $carousel = $(this)
      Plugin.call($carousel, $carousel.data())
    })
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: collapse.js v3.3.5
 * http://getbootstrap.com/javascript/#collapse
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // COLLAPSE PUBLIC CLASS DEFINITION
  // ================================

  var Collapse = function (element, options) {
    this.$element      = $(element)
    this.options       = $.extend({}, Collapse.DEFAULTS, options)
    this.$trigger      = $('[data-toggle="collapse"][href="#' + element.id + '"],' +
                           '[data-toggle="collapse"][data-target="#' + element.id + '"]')
    this.transitioning = null

    if (this.options.parent) {
      this.$parent = this.getParent()
    } else {
      this.addAriaAndCollapsedClass(this.$element, this.$trigger)
    }

    if (this.options.toggle) this.toggle()
  }

  Collapse.VERSION  = '3.3.5'

  Collapse.TRANSITION_DURATION = 350

  Collapse.DEFAULTS = {
    toggle: true
  }

  Collapse.prototype.dimension = function () {
    var hasWidth = this.$element.hasClass('width')
    return hasWidth ? 'width' : 'height'
  }

  Collapse.prototype.show = function () {
    if (this.transitioning || this.$element.hasClass('in')) return

    var activesData
    var actives = this.$parent && this.$parent.children('.panel').children('.in, .collapsing')

    if (actives && actives.length) {
      activesData = actives.data('bs.collapse')
      if (activesData && activesData.transitioning) return
    }

    var startEvent = $.Event('show.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    if (actives && actives.length) {
      Plugin.call(actives, 'hide')
      activesData || actives.data('bs.collapse', null)
    }

    var dimension = this.dimension()

    this.$element
      .removeClass('collapse')
      .addClass('collapsing')[dimension](0)
      .attr('aria-expanded', true)

    this.$trigger
      .removeClass('collapsed')
      .attr('aria-expanded', true)

    this.transitioning = 1

    var complete = function () {
      this.$element
        .removeClass('collapsing')
        .addClass('collapse in')[dimension]('')
      this.transitioning = 0
      this.$element
        .trigger('shown.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    var scrollSize = $.camelCase(['scroll', dimension].join('-'))

    this.$element
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)[dimension](this.$element[0][scrollSize])
  }

  Collapse.prototype.hide = function () {
    if (this.transitioning || !this.$element.hasClass('in')) return

    var startEvent = $.Event('hide.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    var dimension = this.dimension()

    this.$element[dimension](this.$element[dimension]())[0].offsetHeight

    this.$element
      .addClass('collapsing')
      .removeClass('collapse in')
      .attr('aria-expanded', false)

    this.$trigger
      .addClass('collapsed')
      .attr('aria-expanded', false)

    this.transitioning = 1

    var complete = function () {
      this.transitioning = 0
      this.$element
        .removeClass('collapsing')
        .addClass('collapse')
        .trigger('hidden.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    this.$element
      [dimension](0)
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)
  }

  Collapse.prototype.toggle = function () {
    this[this.$element.hasClass('in') ? 'hide' : 'show']()
  }

  Collapse.prototype.getParent = function () {
    return $(this.options.parent)
      .find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]')
      .each($.proxy(function (i, element) {
        var $element = $(element)
        this.addAriaAndCollapsedClass(getTargetFromTrigger($element), $element)
      }, this))
      .end()
  }

  Collapse.prototype.addAriaAndCollapsedClass = function ($element, $trigger) {
    var isOpen = $element.hasClass('in')

    $element.attr('aria-expanded', isOpen)
    $trigger
      .toggleClass('collapsed', !isOpen)
      .attr('aria-expanded', isOpen)
  }

  function getTargetFromTrigger($trigger) {
    var href
    var target = $trigger.attr('data-target')
      || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') // strip for ie7

    return $(target)
  }


  // COLLAPSE PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.collapse')
      var options = $.extend({}, Collapse.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data && options.toggle && /show|hide/.test(option)) options.toggle = false
      if (!data) $this.data('bs.collapse', (data = new Collapse(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.collapse

  $.fn.collapse             = Plugin
  $.fn.collapse.Constructor = Collapse


  // COLLAPSE NO CONFLICT
  // ====================

  $.fn.collapse.noConflict = function () {
    $.fn.collapse = old
    return this
  }


  // COLLAPSE DATA-API
  // =================

  $(document).on('click.bs.collapse.data-api', '[data-toggle="collapse"]', function (e) {
    var $this   = $(this)

    if (!$this.attr('data-target')) e.preventDefault()

    var $target = getTargetFromTrigger($this)
    var data    = $target.data('bs.collapse')
    var option  = data ? 'toggle' : $this.data()

    Plugin.call($target, option)
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: dropdown.js v3.3.5
 * http://getbootstrap.com/javascript/#dropdowns
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // DROPDOWN CLASS DEFINITION
  // =========================

  var backdrop = '.dropdown-backdrop'
  var toggle   = '[data-toggle="dropdown"]'
  var Dropdown = function (element) {
    $(element).on('click.bs.dropdown', this.toggle)
  }

  Dropdown.VERSION = '3.3.5'

  function getParent($this) {
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = selector && $(selector)

    return $parent && $parent.length ? $parent : $this.parent()
  }

  function clearMenus(e) {
    if (e && e.which === 3) return
    $(backdrop).remove()
    $(toggle).each(function () {
      var $this         = $(this)
      var $parent       = getParent($this)
      var relatedTarget = { relatedTarget: this }

      if (!$parent.hasClass('open')) return

      if (e && e.type == 'click' && /input|textarea/i.test(e.target.tagName) && $.contains($parent[0], e.target)) return

      $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this.attr('aria-expanded', 'false')
      $parent.removeClass('open').trigger('hidden.bs.dropdown', relatedTarget)
    })
  }

  Dropdown.prototype.toggle = function (e) {
    var $this = $(this)

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    clearMenus()

    if (!isActive) {
      if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {
        // if mobile we use a backdrop because click events don't delegate
        $(document.createElement('div'))
          .addClass('dropdown-backdrop')
          .insertAfter($(this))
          .on('click', clearMenus)
      }

      var relatedTarget = { relatedTarget: this }
      $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this
        .trigger('focus')
        .attr('aria-expanded', 'true')

      $parent
        .toggleClass('open')
        .trigger('shown.bs.dropdown', relatedTarget)
    }

    return false
  }

  Dropdown.prototype.keydown = function (e) {
    if (!/(38|40|27|32)/.test(e.which) || /input|textarea/i.test(e.target.tagName)) return

    var $this = $(this)

    e.preventDefault()
    e.stopPropagation()

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    if (!isActive && e.which != 27 || isActive && e.which == 27) {
      if (e.which == 27) $parent.find(toggle).trigger('focus')
      return $this.trigger('click')
    }

    var desc = ' li:not(.disabled):visible a'
    var $items = $parent.find('.dropdown-menu' + desc)

    if (!$items.length) return

    var index = $items.index(e.target)

    if (e.which == 38 && index > 0)                 index--         // up
    if (e.which == 40 && index < $items.length - 1) index++         // down
    if (!~index)                                    index = 0

    $items.eq(index).trigger('focus')
  }


  // DROPDOWN PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.dropdown')

      if (!data) $this.data('bs.dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.dropdown

  $.fn.dropdown             = Plugin
  $.fn.dropdown.Constructor = Dropdown


  // DROPDOWN NO CONFLICT
  // ====================

  $.fn.dropdown.noConflict = function () {
    $.fn.dropdown = old
    return this
  }


  // APPLY TO STANDARD DROPDOWN ELEMENTS
  // ===================================

  $(document)
    .on('click.bs.dropdown.data-api', clearMenus)
    .on('click.bs.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    .on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle)
    .on('keydown.bs.dropdown.data-api', toggle, Dropdown.prototype.keydown)
    .on('keydown.bs.dropdown.data-api', '.dropdown-menu', Dropdown.prototype.keydown)

}(jQuery);

/* ========================================================================
 * Bootstrap: modal.js v3.3.5
 * http://getbootstrap.com/javascript/#modals
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // MODAL CLASS DEFINITION
  // ======================

  var Modal = function (element, options) {
    this.options             = options
    this.$body               = $(document.body)
    this.$element            = $(element)
    this.$dialog             = this.$element.find('.modal-dialog')
    this.$backdrop           = null
    this.isShown             = null
    this.originalBodyPad     = null
    this.scrollbarWidth      = 0
    this.ignoreBackdropClick = false

    if (this.options.remote) {
      this.$element
        .find('.modal-content')
        .load(this.options.remote, $.proxy(function () {
          this.$element.trigger('loaded.bs.modal')
        }, this))
    }
  }

  Modal.VERSION  = '3.3.5'

  Modal.TRANSITION_DURATION = 300
  Modal.BACKDROP_TRANSITION_DURATION = 150

  Modal.DEFAULTS = {
    backdrop: true,
    keyboard: true,
    show: true
  }

  Modal.prototype.toggle = function (_relatedTarget) {
    return this.isShown ? this.hide() : this.show(_relatedTarget)
  }

  Modal.prototype.show = function (_relatedTarget) {
    var that = this
    var e    = $.Event('show.bs.modal', { relatedTarget: _relatedTarget })

    this.$element.trigger(e)

    if (this.isShown || e.isDefaultPrevented()) return

    this.isShown = true

    this.checkScrollbar()
    this.setScrollbar()
    this.$body.addClass('modal-open')

    this.escape()
    this.resize()

    this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))

    this.$dialog.on('mousedown.dismiss.bs.modal', function () {
      that.$element.one('mouseup.dismiss.bs.modal', function (e) {
        if ($(e.target).is(that.$element)) that.ignoreBackdropClick = true
      })
    })

    this.backdrop(function () {
      var transition = $.support.transition && that.$element.hasClass('fade')

      if (!that.$element.parent().length) {
        that.$element.appendTo(that.$body) // don't move modals dom position
      }

      that.$element
        .show()
        .scrollTop(0)

      that.adjustDialog()

      if (transition) {
        that.$element[0].offsetWidth // force reflow
      }

      that.$element.addClass('in')

      that.enforceFocus()

      var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget })

      transition ?
        that.$dialog // wait for modal to slide in
          .one('bsTransitionEnd', function () {
            that.$element.trigger('focus').trigger(e)
          })
          .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
        that.$element.trigger('focus').trigger(e)
    })
  }

  Modal.prototype.hide = function (e) {
    if (e) e.preventDefault()

    e = $.Event('hide.bs.modal')

    this.$element.trigger(e)

    if (!this.isShown || e.isDefaultPrevented()) return

    this.isShown = false

    this.escape()
    this.resize()

    $(document).off('focusin.bs.modal')

    this.$element
      .removeClass('in')
      .off('click.dismiss.bs.modal')
      .off('mouseup.dismiss.bs.modal')

    this.$dialog.off('mousedown.dismiss.bs.modal')

    $.support.transition && this.$element.hasClass('fade') ?
      this.$element
        .one('bsTransitionEnd', $.proxy(this.hideModal, this))
        .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
      this.hideModal()
  }

  Modal.prototype.enforceFocus = function () {
    $(document)
      .off('focusin.bs.modal') // guard against infinite focus loop
      .on('focusin.bs.modal', $.proxy(function (e) {
        if (this.$element[0] !== e.target && !this.$element.has(e.target).length) {
          this.$element.trigger('focus')
        }
      }, this))
  }

  Modal.prototype.escape = function () {
    if (this.isShown && this.options.keyboard) {
      this.$element.on('keydown.dismiss.bs.modal', $.proxy(function (e) {
        e.which == 27 && this.hide()
      }, this))
    } else if (!this.isShown) {
      this.$element.off('keydown.dismiss.bs.modal')
    }
  }

  Modal.prototype.resize = function () {
    if (this.isShown) {
      $(window).on('resize.bs.modal', $.proxy(this.handleUpdate, this))
    } else {
      $(window).off('resize.bs.modal')
    }
  }

  Modal.prototype.hideModal = function () {
    var that = this
    this.$element.hide()
    this.backdrop(function () {
      that.$body.removeClass('modal-open')
      that.resetAdjustments()
      that.resetScrollbar()
      that.$element.trigger('hidden.bs.modal')
    })
  }

  Modal.prototype.removeBackdrop = function () {
    this.$backdrop && this.$backdrop.remove()
    this.$backdrop = null
  }

  Modal.prototype.backdrop = function (callback) {
    var that = this
    var animate = this.$element.hasClass('fade') ? 'fade' : ''

    if (this.isShown && this.options.backdrop) {
      var doAnimate = $.support.transition && animate

      this.$backdrop = $(document.createElement('div'))
        .addClass('modal-backdrop ' + animate)
        .appendTo(this.$body)

      this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
        if (this.ignoreBackdropClick) {
          this.ignoreBackdropClick = false
          return
        }
        if (e.target !== e.currentTarget) return
        this.options.backdrop == 'static'
          ? this.$element[0].focus()
          : this.hide()
      }, this))

      if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

      this.$backdrop.addClass('in')

      if (!callback) return

      doAnimate ?
        this.$backdrop
          .one('bsTransitionEnd', callback)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callback()

    } else if (!this.isShown && this.$backdrop) {
      this.$backdrop.removeClass('in')

      var callbackRemove = function () {
        that.removeBackdrop()
        callback && callback()
      }
      $.support.transition && this.$element.hasClass('fade') ?
        this.$backdrop
          .one('bsTransitionEnd', callbackRemove)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callbackRemove()

    } else if (callback) {
      callback()
    }
  }

  // these following methods are used to handle overflowing modals

  Modal.prototype.handleUpdate = function () {
    this.adjustDialog()
  }

  Modal.prototype.adjustDialog = function () {
    var modalIsOverflowing = this.$element[0].scrollHeight > document.documentElement.clientHeight

    this.$element.css({
      paddingLeft:  !this.bodyIsOverflowing && modalIsOverflowing ? this.scrollbarWidth : '',
      paddingRight: this.bodyIsOverflowing && !modalIsOverflowing ? this.scrollbarWidth : ''
    })
  }

  Modal.prototype.resetAdjustments = function () {
    this.$element.css({
      paddingLeft: '',
      paddingRight: ''
    })
  }

  Modal.prototype.checkScrollbar = function () {
    var fullWindowWidth = window.innerWidth
    if (!fullWindowWidth) { // workaround for missing window.innerWidth in IE8
      var documentElementRect = document.documentElement.getBoundingClientRect()
      fullWindowWidth = documentElementRect.right - Math.abs(documentElementRect.left)
    }
    this.bodyIsOverflowing = document.body.clientWidth < fullWindowWidth
    this.scrollbarWidth = this.measureScrollbar()
  }

  Modal.prototype.setScrollbar = function () {
    var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10)
    this.originalBodyPad = document.body.style.paddingRight || ''
    if (this.bodyIsOverflowing) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
  }

  Modal.prototype.resetScrollbar = function () {
    this.$body.css('padding-right', this.originalBodyPad)
  }

  Modal.prototype.measureScrollbar = function () { // thx walsh
    var scrollDiv = document.createElement('div')
    scrollDiv.className = 'modal-scrollbar-measure'
    this.$body.append(scrollDiv)
    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth
    this.$body[0].removeChild(scrollDiv)
    return scrollbarWidth
  }


  // MODAL PLUGIN DEFINITION
  // =======================

  function Plugin(option, _relatedTarget) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.modal')
      var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
      if (typeof option == 'string') data[option](_relatedTarget)
      else if (options.show) data.show(_relatedTarget)
    })
  }

  var old = $.fn.modal

  $.fn.modal             = Plugin
  $.fn.modal.Constructor = Modal


  // MODAL NO CONFLICT
  // =================

  $.fn.modal.noConflict = function () {
    $.fn.modal = old
    return this
  }


  // MODAL DATA-API
  // ==============

  $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
    var $this   = $(this)
    var href    = $this.attr('href')
    var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) // strip for ie7
    var option  = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data())

    if ($this.is('a')) e.preventDefault()

    $target.one('show.bs.modal', function (showEvent) {
      if (showEvent.isDefaultPrevented()) return // only register focus restorer if modal will actually get shown
      $target.one('hidden.bs.modal', function () {
        $this.is(':visible') && $this.trigger('focus')
      })
    })
    Plugin.call($target, option, this)
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: tooltip.js v3.3.5
 * http://getbootstrap.com/javascript/#tooltip
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TOOLTIP PUBLIC CLASS DEFINITION
  // ===============================

  var Tooltip = function (element, options) {
    this.type       = null
    this.options    = null
    this.enabled    = null
    this.timeout    = null
    this.hoverState = null
    this.$element   = null
    this.inState    = null

    this.init('tooltip', element, options)
  }

  Tooltip.VERSION  = '3.3.5'

  Tooltip.TRANSITION_DURATION = 150

  Tooltip.DEFAULTS = {
    animation: true,
    placement: 'top',
    selector: false,
    template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
    trigger: 'hover focus',
    title: '',
    delay: 0,
    html: false,
    container: false,
    viewport: {
      selector: 'body',
      padding: 0
    }
  }

  Tooltip.prototype.init = function (type, element, options) {
    this.enabled   = true
    this.type      = type
    this.$element  = $(element)
    this.options   = this.getOptions(options)
    this.$viewport = this.options.viewport && $($.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : (this.options.viewport.selector || this.options.viewport))
    this.inState   = { click: false, hover: false, focus: false }

    if (this.$element[0] instanceof document.constructor && !this.options.selector) {
      throw new Error('`selector` option must be specified when initializing ' + this.type + ' on the window.document object!')
    }

    var triggers = this.options.trigger.split(' ')

    for (var i = triggers.length; i--;) {
      var trigger = triggers[i]

      if (trigger == 'click') {
        this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
      } else if (trigger != 'manual') {
        var eventIn  = trigger == 'hover' ? 'mouseenter' : 'focusin'
        var eventOut = trigger == 'hover' ? 'mouseleave' : 'focusout'

        this.$element.on(eventIn  + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
        this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
      }
    }

    this.options.selector ?
      (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
      this.fixTitle()
  }

  Tooltip.prototype.getDefaults = function () {
    return Tooltip.DEFAULTS
  }

  Tooltip.prototype.getOptions = function (options) {
    options = $.extend({}, this.getDefaults(), this.$element.data(), options)

    if (options.delay && typeof options.delay == 'number') {
      options.delay = {
        show: options.delay,
        hide: options.delay
      }
    }

    return options
  }

  Tooltip.prototype.getDelegateOptions = function () {
    var options  = {}
    var defaults = this.getDefaults()

    this._options && $.each(this._options, function (key, value) {
      if (defaults[key] != value) options[key] = value
    })

    return options
  }

  Tooltip.prototype.enter = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusin' ? 'focus' : 'hover'] = true
    }

    if (self.tip().hasClass('in') || self.hoverState == 'in') {
      self.hoverState = 'in'
      return
    }

    clearTimeout(self.timeout)

    self.hoverState = 'in'

    if (!self.options.delay || !self.options.delay.show) return self.show()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'in') self.show()
    }, self.options.delay.show)
  }

  Tooltip.prototype.isInStateTrue = function () {
    for (var key in this.inState) {
      if (this.inState[key]) return true
    }

    return false
  }

  Tooltip.prototype.leave = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusout' ? 'focus' : 'hover'] = false
    }

    if (self.isInStateTrue()) return

    clearTimeout(self.timeout)

    self.hoverState = 'out'

    if (!self.options.delay || !self.options.delay.hide) return self.hide()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'out') self.hide()
    }, self.options.delay.hide)
  }

  Tooltip.prototype.show = function () {
    var e = $.Event('show.bs.' + this.type)

    if (this.hasContent() && this.enabled) {
      this.$element.trigger(e)

      var inDom = $.contains(this.$element[0].ownerDocument.documentElement, this.$element[0])
      if (e.isDefaultPrevented() || !inDom) return
      var that = this

      var $tip = this.tip()

      var tipId = this.getUID(this.type)

      this.setContent()
      $tip.attr('id', tipId)
      this.$element.attr('aria-describedby', tipId)

      if (this.options.animation) $tip.addClass('fade')

      var placement = typeof this.options.placement == 'function' ?
        this.options.placement.call(this, $tip[0], this.$element[0]) :
        this.options.placement

      var autoToken = /\s?auto?\s?/i
      var autoPlace = autoToken.test(placement)
      if (autoPlace) placement = placement.replace(autoToken, '') || 'top'

      $tip
        .detach()
        .css({ top: 0, left: 0, display: 'block' })
        .addClass(placement)
        .data('bs.' + this.type, this)

      this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)
      this.$element.trigger('inserted.bs.' + this.type)

      var pos          = this.getPosition()
      var actualWidth  = $tip[0].offsetWidth
      var actualHeight = $tip[0].offsetHeight

      if (autoPlace) {
        var orgPlacement = placement
        var viewportDim = this.getPosition(this.$viewport)

        placement = placement == 'bottom' && pos.bottom + actualHeight > viewportDim.bottom ? 'top'    :
                    placement == 'top'    && pos.top    - actualHeight < viewportDim.top    ? 'bottom' :
                    placement == 'right'  && pos.right  + actualWidth  > viewportDim.width  ? 'left'   :
                    placement == 'left'   && pos.left   - actualWidth  < viewportDim.left   ? 'right'  :
                    placement

        $tip
          .removeClass(orgPlacement)
          .addClass(placement)
      }

      var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight)

      this.applyPlacement(calculatedOffset, placement)

      var complete = function () {
        var prevHoverState = that.hoverState
        that.$element.trigger('shown.bs.' + that.type)
        that.hoverState = null

        if (prevHoverState == 'out') that.leave(that)
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        $tip
          .one('bsTransitionEnd', complete)
          .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
        complete()
    }
  }

  Tooltip.prototype.applyPlacement = function (offset, placement) {
    var $tip   = this.tip()
    var width  = $tip[0].offsetWidth
    var height = $tip[0].offsetHeight

    // manually read margins because getBoundingClientRect includes difference
    var marginTop = parseInt($tip.css('margin-top'), 10)
    var marginLeft = parseInt($tip.css('margin-left'), 10)

    // we must check for NaN for ie 8/9
    if (isNaN(marginTop))  marginTop  = 0
    if (isNaN(marginLeft)) marginLeft = 0

    offset.top  += marginTop
    offset.left += marginLeft

    // $.fn.offset doesn't round pixel values
    // so we use setOffset directly with our own function B-0
    $.offset.setOffset($tip[0], $.extend({
      using: function (props) {
        $tip.css({
          top: Math.round(props.top),
          left: Math.round(props.left)
        })
      }
    }, offset), 0)

    $tip.addClass('in')

    // check to see if placing tip in new offset caused the tip to resize itself
    var actualWidth  = $tip[0].offsetWidth
    var actualHeight = $tip[0].offsetHeight

    if (placement == 'top' && actualHeight != height) {
      offset.top = offset.top + height - actualHeight
    }

    var delta = this.getViewportAdjustedDelta(placement, offset, actualWidth, actualHeight)

    if (delta.left) offset.left += delta.left
    else offset.top += delta.top

    var isVertical          = /top|bottom/.test(placement)
    var arrowDelta          = isVertical ? delta.left * 2 - width + actualWidth : delta.top * 2 - height + actualHeight
    var arrowOffsetPosition = isVertical ? 'offsetWidth' : 'offsetHeight'

    $tip.offset(offset)
    this.replaceArrow(arrowDelta, $tip[0][arrowOffsetPosition], isVertical)
  }

  Tooltip.prototype.replaceArrow = function (delta, dimension, isVertical) {
    this.arrow()
      .css(isVertical ? 'left' : 'top', 50 * (1 - delta / dimension) + '%')
      .css(isVertical ? 'top' : 'left', '')
  }

  Tooltip.prototype.setContent = function () {
    var $tip  = this.tip()
    var title = this.getTitle()

    $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
    $tip.removeClass('fade in top bottom left right')
  }

  Tooltip.prototype.hide = function (callback) {
    var that = this
    var $tip = $(this.$tip)
    var e    = $.Event('hide.bs.' + this.type)

    function complete() {
      if (that.hoverState != 'in') $tip.detach()
      that.$element
        .removeAttr('aria-describedby')
        .trigger('hidden.bs.' + that.type)
      callback && callback()
    }

    this.$element.trigger(e)

    if (e.isDefaultPrevented()) return

    $tip.removeClass('in')

    $.support.transition && $tip.hasClass('fade') ?
      $tip
        .one('bsTransitionEnd', complete)
        .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
      complete()

    this.hoverState = null

    return this
  }

  Tooltip.prototype.fixTitle = function () {
    var $e = this.$element
    if ($e.attr('title') || typeof $e.attr('data-original-title') != 'string') {
      $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
    }
  }

  Tooltip.prototype.hasContent = function () {
    return this.getTitle()
  }

  Tooltip.prototype.getPosition = function ($element) {
    $element   = $element || this.$element

    var el     = $element[0]
    var isBody = el.tagName == 'BODY'

    var elRect    = el.getBoundingClientRect()
    if (elRect.width == null) {
      // width and height are missing in IE8, so compute them manually; see https://github.com/twbs/bootstrap/issues/14093
      elRect = $.extend({}, elRect, { width: elRect.right - elRect.left, height: elRect.bottom - elRect.top })
    }
    var elOffset  = isBody ? { top: 0, left: 0 } : $element.offset()
    var scroll    = { scroll: isBody ? document.documentElement.scrollTop || document.body.scrollTop : $element.scrollTop() }
    var outerDims = isBody ? { width: $(window).width(), height: $(window).height() } : null

    return $.extend({}, elRect, scroll, outerDims, elOffset)
  }

  Tooltip.prototype.getCalculatedOffset = function (placement, pos, actualWidth, actualHeight) {
    return placement == 'bottom' ? { top: pos.top + pos.height,   left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'top'    ? { top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'left'   ? { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth } :
        /* placement == 'right' */ { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width }

  }

  Tooltip.prototype.getViewportAdjustedDelta = function (placement, pos, actualWidth, actualHeight) {
    var delta = { top: 0, left: 0 }
    if (!this.$viewport) return delta

    var viewportPadding = this.options.viewport && this.options.viewport.padding || 0
    var viewportDimensions = this.getPosition(this.$viewport)

    if (/right|left/.test(placement)) {
      var topEdgeOffset    = pos.top - viewportPadding - viewportDimensions.scroll
      var bottomEdgeOffset = pos.top + viewportPadding - viewportDimensions.scroll + actualHeight
      if (topEdgeOffset < viewportDimensions.top) { // top overflow
        delta.top = viewportDimensions.top - topEdgeOffset
      } else if (bottomEdgeOffset > viewportDimensions.top + viewportDimensions.height) { // bottom overflow
        delta.top = viewportDimensions.top + viewportDimensions.height - bottomEdgeOffset
      }
    } else {
      var leftEdgeOffset  = pos.left - viewportPadding
      var rightEdgeOffset = pos.left + viewportPadding + actualWidth
      if (leftEdgeOffset < viewportDimensions.left) { // left overflow
        delta.left = viewportDimensions.left - leftEdgeOffset
      } else if (rightEdgeOffset > viewportDimensions.right) { // right overflow
        delta.left = viewportDimensions.left + viewportDimensions.width - rightEdgeOffset
      }
    }

    return delta
  }

  Tooltip.prototype.getTitle = function () {
    var title
    var $e = this.$element
    var o  = this.options

    title = $e.attr('data-original-title')
      || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)

    return title
  }

  Tooltip.prototype.getUID = function (prefix) {
    do prefix += ~~(Math.random() * 1000000)
    while (document.getElementById(prefix))
    return prefix
  }

  Tooltip.prototype.tip = function () {
    if (!this.$tip) {
      this.$tip = $(this.options.template)
      if (this.$tip.length != 1) {
        throw new Error(this.type + ' `template` option must consist of exactly 1 top-level element!')
      }
    }
    return this.$tip
  }

  Tooltip.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.tooltip-arrow'))
  }

  Tooltip.prototype.enable = function () {
    this.enabled = true
  }

  Tooltip.prototype.disable = function () {
    this.enabled = false
  }

  Tooltip.prototype.toggleEnabled = function () {
    this.enabled = !this.enabled
  }

  Tooltip.prototype.toggle = function (e) {
    var self = this
    if (e) {
      self = $(e.currentTarget).data('bs.' + this.type)
      if (!self) {
        self = new this.constructor(e.currentTarget, this.getDelegateOptions())
        $(e.currentTarget).data('bs.' + this.type, self)
      }
    }

    if (e) {
      self.inState.click = !self.inState.click
      if (self.isInStateTrue()) self.enter(self)
      else self.leave(self)
    } else {
      self.tip().hasClass('in') ? self.leave(self) : self.enter(self)
    }
  }

  Tooltip.prototype.destroy = function () {
    var that = this
    clearTimeout(this.timeout)
    this.hide(function () {
      that.$element.off('.' + that.type).removeData('bs.' + that.type)
      if (that.$tip) {
        that.$tip.detach()
      }
      that.$tip = null
      that.$arrow = null
      that.$viewport = null
    })
  }


  // TOOLTIP PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.tooltip')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.tooltip', (data = new Tooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tooltip

  $.fn.tooltip             = Plugin
  $.fn.tooltip.Constructor = Tooltip


  // TOOLTIP NO CONFLICT
  // ===================

  $.fn.tooltip.noConflict = function () {
    $.fn.tooltip = old
    return this
  }

}(jQuery);

/* ========================================================================
 * Bootstrap: popover.js v3.3.5
 * http://getbootstrap.com/javascript/#popovers
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // POPOVER PUBLIC CLASS DEFINITION
  // ===============================

  var Popover = function (element, options) {
    this.init('popover', element, options)
  }

  if (!$.fn.tooltip) throw new Error('Popover requires tooltip.js')

  Popover.VERSION  = '3.3.5'

  Popover.DEFAULTS = $.extend({}, $.fn.tooltip.Constructor.DEFAULTS, {
    placement: 'right',
    trigger: 'click',
    content: '',
    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
  })


  // NOTE: POPOVER EXTENDS tooltip.js
  // ================================

  Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype)

  Popover.prototype.constructor = Popover

  Popover.prototype.getDefaults = function () {
    return Popover.DEFAULTS
  }

  Popover.prototype.setContent = function () {
    var $tip    = this.tip()
    var title   = this.getTitle()
    var content = this.getContent()

    $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
    $tip.find('.popover-content').children().detach().end()[ // we use append for html objects to maintain js events
      this.options.html ? (typeof content == 'string' ? 'html' : 'append') : 'text'
    ](content)

    $tip.removeClass('fade top bottom left right in')

    // IE8 doesn't accept hiding via the `:empty` pseudo selector, we have to do
    // this manually by checking the contents.
    if (!$tip.find('.popover-title').html()) $tip.find('.popover-title').hide()
  }

  Popover.prototype.hasContent = function () {
    return this.getTitle() || this.getContent()
  }

  Popover.prototype.getContent = function () {
    var $e = this.$element
    var o  = this.options

    return $e.attr('data-content')
      || (typeof o.content == 'function' ?
            o.content.call($e[0]) :
            o.content)
  }

  Popover.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.arrow'))
  }


  // POPOVER PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.popover')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.popover', (data = new Popover(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.popover

  $.fn.popover             = Plugin
  $.fn.popover.Constructor = Popover


  // POPOVER NO CONFLICT
  // ===================

  $.fn.popover.noConflict = function () {
    $.fn.popover = old
    return this
  }

}(jQuery);

/* ========================================================================
 * Bootstrap: scrollspy.js v3.3.5
 * http://getbootstrap.com/javascript/#scrollspy
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // SCROLLSPY CLASS DEFINITION
  // ==========================

  function ScrollSpy(element, options) {
    this.$body          = $(document.body)
    this.$scrollElement = $(element).is(document.body) ? $(window) : $(element)
    this.options        = $.extend({}, ScrollSpy.DEFAULTS, options)
    this.selector       = (this.options.target || '') + ' .nav li > a'
    this.offsets        = []
    this.targets        = []
    this.activeTarget   = null
    this.scrollHeight   = 0

    this.$scrollElement.on('scroll.bs.scrollspy', $.proxy(this.process, this))
    this.refresh()
    this.process()
  }

  ScrollSpy.VERSION  = '3.3.5'

  ScrollSpy.DEFAULTS = {
    offset: 10
  }

  ScrollSpy.prototype.getScrollHeight = function () {
    return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
  }

  ScrollSpy.prototype.refresh = function () {
    var that          = this
    var offsetMethod  = 'offset'
    var offsetBase    = 0

    this.offsets      = []
    this.targets      = []
    this.scrollHeight = this.getScrollHeight()

    if (!$.isWindow(this.$scrollElement[0])) {
      offsetMethod = 'position'
      offsetBase   = this.$scrollElement.scrollTop()
    }

    this.$body
      .find(this.selector)
      .map(function () {
        var $el   = $(this)
        var href  = $el.data('target') || $el.attr('href')
        var $href = /^#./.test(href) && $(href)

        return ($href
          && $href.length
          && $href.is(':visible')
          && [[$href[offsetMethod]().top + offsetBase, href]]) || null
      })
      .sort(function (a, b) { return a[0] - b[0] })
      .each(function () {
        that.offsets.push(this[0])
        that.targets.push(this[1])
      })
  }

  ScrollSpy.prototype.process = function () {
    var scrollTop    = this.$scrollElement.scrollTop() + this.options.offset
    var scrollHeight = this.getScrollHeight()
    var maxScroll    = this.options.offset + scrollHeight - this.$scrollElement.height()
    var offsets      = this.offsets
    var targets      = this.targets
    var activeTarget = this.activeTarget
    var i

    if (this.scrollHeight != scrollHeight) {
      this.refresh()
    }

    if (scrollTop >= maxScroll) {
      return activeTarget != (i = targets[targets.length - 1]) && this.activate(i)
    }

    if (activeTarget && scrollTop < offsets[0]) {
      this.activeTarget = null
      return this.clear()
    }

    for (i = offsets.length; i--;) {
      activeTarget != targets[i]
        && scrollTop >= offsets[i]
        && (offsets[i + 1] === undefined || scrollTop < offsets[i + 1])
        && this.activate(targets[i])
    }
  }

  ScrollSpy.prototype.activate = function (target) {
    this.activeTarget = target

    this.clear()

    var selector = this.selector +
      '[data-target="' + target + '"],' +
      this.selector + '[href="' + target + '"]'

    var active = $(selector)
      .parents('li')
      .addClass('active')

    if (active.parent('.dropdown-menu').length) {
      active = active
        .closest('li.dropdown')
        .addClass('active')
    }

    active.trigger('activate.bs.scrollspy')
  }

  ScrollSpy.prototype.clear = function () {
    $(this.selector)
      .parentsUntil(this.options.target, '.active')
      .removeClass('active')
  }


  // SCROLLSPY PLUGIN DEFINITION
  // ===========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.scrollspy')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.scrollspy', (data = new ScrollSpy(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.scrollspy

  $.fn.scrollspy             = Plugin
  $.fn.scrollspy.Constructor = ScrollSpy


  // SCROLLSPY NO CONFLICT
  // =====================

  $.fn.scrollspy.noConflict = function () {
    $.fn.scrollspy = old
    return this
  }


  // SCROLLSPY DATA-API
  // ==================

  $(window).on('load.bs.scrollspy.data-api', function () {
    $('[data-spy="scroll"]').each(function () {
      var $spy = $(this)
      Plugin.call($spy, $spy.data())
    })
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: tab.js v3.3.5
 * http://getbootstrap.com/javascript/#tabs
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TAB CLASS DEFINITION
  // ====================

  var Tab = function (element) {
    // jscs:disable requireDollarBeforejQueryAssignment
    this.element = $(element)
    // jscs:enable requireDollarBeforejQueryAssignment
  }

  Tab.VERSION = '3.3.5'

  Tab.TRANSITION_DURATION = 150

  Tab.prototype.show = function () {
    var $this    = this.element
    var $ul      = $this.closest('ul:not(.dropdown-menu)')
    var selector = $this.data('target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    if ($this.parent('li').hasClass('active')) return

    var $previous = $ul.find('.active:last a')
    var hideEvent = $.Event('hide.bs.tab', {
      relatedTarget: $this[0]
    })
    var showEvent = $.Event('show.bs.tab', {
      relatedTarget: $previous[0]
    })

    $previous.trigger(hideEvent)
    $this.trigger(showEvent)

    if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) return

    var $target = $(selector)

    this.activate($this.closest('li'), $ul)
    this.activate($target, $target.parent(), function () {
      $previous.trigger({
        type: 'hidden.bs.tab',
        relatedTarget: $this[0]
      })
      $this.trigger({
        type: 'shown.bs.tab',
        relatedTarget: $previous[0]
      })
    })
  }

  Tab.prototype.activate = function (element, container, callback) {
    var $active    = container.find('> .active')
    var transition = callback
      && $.support.transition
      && ($active.length && $active.hasClass('fade') || !!container.find('> .fade').length)

    function next() {
      $active
        .removeClass('active')
        .find('> .dropdown-menu > .active')
          .removeClass('active')
        .end()
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', false)

      element
        .addClass('active')
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', true)

      if (transition) {
        element[0].offsetWidth // reflow for transition
        element.addClass('in')
      } else {
        element.removeClass('fade')
      }

      if (element.parent('.dropdown-menu').length) {
        element
          .closest('li.dropdown')
            .addClass('active')
          .end()
          .find('[data-toggle="tab"]')
            .attr('aria-expanded', true)
      }

      callback && callback()
    }

    $active.length && transition ?
      $active
        .one('bsTransitionEnd', next)
        .emulateTransitionEnd(Tab.TRANSITION_DURATION) :
      next()

    $active.removeClass('in')
  }


  // TAB PLUGIN DEFINITION
  // =====================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.tab')

      if (!data) $this.data('bs.tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tab

  $.fn.tab             = Plugin
  $.fn.tab.Constructor = Tab


  // TAB NO CONFLICT
  // ===============

  $.fn.tab.noConflict = function () {
    $.fn.tab = old
    return this
  }


  // TAB DATA-API
  // ============

  var clickHandler = function (e) {
    e.preventDefault()
    Plugin.call($(this), 'show')
  }

  $(document)
    .on('click.bs.tab.data-api', '[data-toggle="tab"]', clickHandler)
    .on('click.bs.tab.data-api', '[data-toggle="pill"]', clickHandler)

}(jQuery);

/* ========================================================================
 * Bootstrap: affix.js v3.3.5
 * http://getbootstrap.com/javascript/#affix
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // AFFIX CLASS DEFINITION
  // ======================

  var Affix = function (element, options) {
    this.options = $.extend({}, Affix.DEFAULTS, options)

    this.$target = $(this.options.target)
      .on('scroll.bs.affix.data-api', $.proxy(this.checkPosition, this))
      .on('click.bs.affix.data-api',  $.proxy(this.checkPositionWithEventLoop, this))

    this.$element     = $(element)
    this.affixed      = null
    this.unpin        = null
    this.pinnedOffset = null

    this.checkPosition()
  }

  Affix.VERSION  = '3.3.5'

  Affix.RESET    = 'affix affix-top affix-bottom'

  Affix.DEFAULTS = {
    offset: 0,
    target: window
  }

  Affix.prototype.getState = function (scrollHeight, height, offsetTop, offsetBottom) {
    var scrollTop    = this.$target.scrollTop()
    var position     = this.$element.offset()
    var targetHeight = this.$target.height()

    if (offsetTop != null && this.affixed == 'top') return scrollTop < offsetTop ? 'top' : false

    if (this.affixed == 'bottom') {
      if (offsetTop != null) return (scrollTop + this.unpin <= position.top) ? false : 'bottom'
      return (scrollTop + targetHeight <= scrollHeight - offsetBottom) ? false : 'bottom'
    }

    var initializing   = this.affixed == null
    var colliderTop    = initializing ? scrollTop : position.top
    var colliderHeight = initializing ? targetHeight : height

    if (offsetTop != null && scrollTop <= offsetTop) return 'top'
    if (offsetBottom != null && (colliderTop + colliderHeight >= scrollHeight - offsetBottom)) return 'bottom'

    return false
  }

  Affix.prototype.getPinnedOffset = function () {
    if (this.pinnedOffset) return this.pinnedOffset
    this.$element.removeClass(Affix.RESET).addClass('affix')
    var scrollTop = this.$target.scrollTop()
    var position  = this.$element.offset()
    return (this.pinnedOffset = position.top - scrollTop)
  }

  Affix.prototype.checkPositionWithEventLoop = function () {
    setTimeout($.proxy(this.checkPosition, this), 1)
  }

  Affix.prototype.checkPosition = function () {
    if (!this.$element.is(':visible')) return

    var height       = this.$element.height()
    var offset       = this.options.offset
    var offsetTop    = offset.top
    var offsetBottom = offset.bottom
    var scrollHeight = Math.max($(document).height(), $(document.body).height())

    if (typeof offset != 'object')         offsetBottom = offsetTop = offset
    if (typeof offsetTop == 'function')    offsetTop    = offset.top(this.$element)
    if (typeof offsetBottom == 'function') offsetBottom = offset.bottom(this.$element)

    var affix = this.getState(scrollHeight, height, offsetTop, offsetBottom)

    if (this.affixed != affix) {
      if (this.unpin != null) this.$element.css('top', '')

      var affixType = 'affix' + (affix ? '-' + affix : '')
      var e         = $.Event(affixType + '.bs.affix')

      this.$element.trigger(e)

      if (e.isDefaultPrevented()) return

      this.affixed = affix
      this.unpin = affix == 'bottom' ? this.getPinnedOffset() : null

      this.$element
        .removeClass(Affix.RESET)
        .addClass(affixType)
        .trigger(affixType.replace('affix', 'affixed') + '.bs.affix')
    }

    if (affix == 'bottom') {
      this.$element.offset({
        top: scrollHeight - height - offsetBottom
      })
    }
  }


  // AFFIX PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.affix')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.affix', (data = new Affix(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.affix

  $.fn.affix             = Plugin
  $.fn.affix.Constructor = Affix


  // AFFIX NO CONFLICT
  // =================

  $.fn.affix.noConflict = function () {
    $.fn.affix = old
    return this
  }


  // AFFIX DATA-API
  // ==============

  $(window).on('load', function () {
    $('[data-spy="affix"]').each(function () {
      var $spy = $(this)
      var data = $spy.data()

      data.offset = data.offset || {}

      if (data.offsetBottom != null) data.offset.bottom = data.offsetBottom
      if (data.offsetTop    != null) data.offset.top    = data.offsetTop

      Plugin.call($spy, data)
    })
  })

}(jQuery);

(function(e,t){"use strict";function n(e){var t=Array.prototype.slice.call(arguments,1);return e.prop?e.prop.apply(e,t):e.attr.apply(e,t)}function s(e,t,n){var s,a;for(s in n)n.hasOwnProperty(s)&&(a=s.replace(/ |$/g,t.eventNamespace),e.bind(a,n[s]))}function a(e,t,n){s(e,n,{focus:function(){t.addClass(n.focusClass)},blur:function(){t.removeClass(n.focusClass),t.removeClass(n.activeClass)},mouseenter:function(){t.addClass(n.hoverClass)},mouseleave:function(){t.removeClass(n.hoverClass),t.removeClass(n.activeClass)},"mousedown touchbegin":function(){e.is(":disabled")||t.addClass(n.activeClass)},"mouseup touchend":function(){t.removeClass(n.activeClass)}})}function i(e,t){e.removeClass(t.hoverClass+" "+t.focusClass+" "+t.activeClass)}function r(e,t,n){n?e.addClass(t):e.removeClass(t)}function l(e,t,n){var s="checked",a=t.is(":"+s);t.prop?t.prop(s,a):a?t.attr(s,s):t.removeAttr(s),r(e,n.checkedClass,a)}function u(e,t,n){r(e,n.disabledClass,t.is(":disabled"))}function o(e,t,n){switch(n){case"after":return e.after(t),e.next();case"before":return e.before(t),e.prev();case"wrap":return e.wrap(t),e.parent()}return null}function c(t,s,a){var i,r,l;return a||(a={}),a=e.extend({bind:{},divClass:null,divWrap:"wrap",spanClass:null,spanHtml:null,spanWrap:"wrap"},a),i=e("<div />"),r=e("<span />"),s.autoHide&&t.is(":hidden")&&"none"===t.css("display")&&i.hide(),a.divClass&&i.addClass(a.divClass),s.wrapperClass&&i.addClass(s.wrapperClass),a.spanClass&&r.addClass(a.spanClass),l=n(t,"id"),s.useID&&l&&n(i,"id",s.idPrefix+"-"+l),a.spanHtml&&r.html(a.spanHtml),i=o(t,i,a.divWrap),r=o(t,r,a.spanWrap),u(i,t,s),{div:i,span:r}}function d(t,n){var s;return n.wrapperClass?(s=e("<span />").addClass(n.wrapperClass),s=o(t,s,"wrap")):null}function f(){var t,n,s,a;return a="rgb(120,2,153)",n=e('<div style="width:0;height:0;color:'+a+'">'),e("body").append(n),s=n.get(0),t=window.getComputedStyle?window.getComputedStyle(s,"").color:(s.currentStyle||s.style||{}).color,n.remove(),t.replace(/ /g,"")!==a}function p(t){return t?e("<span />").text(t).html():""}function m(){return navigator.cpuClass&&!navigator.product}function v(){return window.XMLHttpRequest!==void 0?!0:!1}function h(e){var t;return e[0].multiple?!0:(t=n(e,"size"),!t||1>=t?!1:!0)}function C(){return!1}function w(e,t){var n="none";s(e,t,{"selectstart dragstart mousedown":C}),e.css({MozUserSelect:n,msUserSelect:n,webkitUserSelect:n,userSelect:n})}function b(e,t,n){var s=e.val();""===s?s=n.fileDefaultHtml:(s=s.split(/[\/\\]+/),s=s[s.length-1]),t.text(s)}function y(e,t,n){var s,a;for(s=[],e.each(function(){var e;for(e in t)Object.prototype.hasOwnProperty.call(t,e)&&(s.push({el:this,name:e,old:this.style[e]}),this.style[e]=t[e])}),n();s.length;)a=s.pop(),a.el.style[a.name]=a.old}function g(e,t){var n;n=e.parents(),n.push(e[0]),n=n.not(":visible"),y(n,{visibility:"hidden",display:"block",position:"absolute"},t)}function k(e,t){return function(){e.unwrap().unwrap().unbind(t.eventNamespace)}}var H=!0,x=!1,A=[{match:function(e){return e.is("a, button, :submit, :reset, input[type='button']")},apply:function(e,t){var r,l,o,d,f;return l=t.submitDefaultHtml,e.is(":reset")&&(l=t.resetDefaultHtml),d=e.is("a, button")?function(){return e.html()||l}:function(){return p(n(e,"value"))||l},o=c(e,t,{divClass:t.buttonClass,spanHtml:d()}),r=o.div,a(e,r,t),f=!1,s(r,t,{"click touchend":function(){var t,s,a,i;f||e.is(":disabled")||(f=!0,e[0].dispatchEvent?(t=document.createEvent("MouseEvents"),t.initEvent("click",!0,!0),s=e[0].dispatchEvent(t),e.is("a")&&s&&(a=n(e,"target"),i=n(e,"href"),a&&"_self"!==a?window.open(i,a):document.location.href=i)):e.click(),f=!1)}}),w(r,t),{remove:function(){return r.after(e),r.remove(),e.unbind(t.eventNamespace),e},update:function(){i(r,t),u(r,e,t),e.detach(),o.span.html(d()).append(e)}}}},{match:function(e){return e.is(":checkbox")},apply:function(e,t){var n,r,o;return n=c(e,t,{divClass:t.checkboxClass}),r=n.div,o=n.span,a(e,r,t),s(e,t,{"click touchend":function(){l(o,e,t)}}),l(o,e,t),{remove:k(e,t),update:function(){i(r,t),o.removeClass(t.checkedClass),l(o,e,t),u(r,e,t)}}}},{match:function(e){return e.is(":file")},apply:function(t,r){function l(){b(t,p,r)}var d,f,p,v;return d=c(t,r,{divClass:r.fileClass,spanClass:r.fileButtonClass,spanHtml:r.fileButtonHtml,spanWrap:"after"}),f=d.div,v=d.span,p=e("<span />").html(r.fileDefaultHtml),p.addClass(r.filenameClass),p=o(t,p,"after"),n(t,"size")||n(t,"size",f.width()/10),a(t,f,r),l(),m()?s(t,r,{click:function(){t.trigger("change"),setTimeout(l,0)}}):s(t,r,{change:l}),w(p,r),w(v,r),{remove:function(){return p.remove(),v.remove(),t.unwrap().unbind(r.eventNamespace)},update:function(){i(f,r),b(t,p,r),u(f,t,r)}}}},{match:function(e){if(e.is("input")){var t=(" "+n(e,"type")+" ").toLowerCase(),s=" color date datetime datetime-local email month number password search tel text time url week ";return s.indexOf(t)>=0}return!1},apply:function(e,t){var s,i;return s=n(e,"type"),e.addClass(t.inputClass),i=d(e,t),a(e,e,t),t.inputAddTypeAsClass&&e.addClass(s),{remove:function(){e.removeClass(t.inputClass),t.inputAddTypeAsClass&&e.removeClass(s),i&&e.unwrap()},update:C}}},{match:function(e){return e.is(":radio")},apply:function(t,r){var o,d,f;return o=c(t,r,{divClass:r.radioClass}),d=o.div,f=o.span,a(t,d,r),s(t,r,{"click touchend":function(){e.uniform.update(e(':radio[name="'+n(t,"name")+'"]'))}}),l(f,t,r),{remove:k(t,r),update:function(){i(d,r),l(f,t,r),u(d,t,r)}}}},{match:function(e){return e.is("select")&&!h(e)?!0:!1},apply:function(t,n){var r,l,o,d;return n.selectAutoWidth&&g(t,function(){d=t.width()}),r=c(t,n,{divClass:n.selectClass,spanHtml:(t.find(":selected:first")||t.find("option:first")).html(),spanWrap:"before"}),l=r.div,o=r.span,n.selectAutoWidth?g(t,function(){y(e([o[0],l[0]]),{display:"block"},function(){var e;e=o.outerWidth()-o.width(),l.width(d+e),o.width(d)})}):l.addClass("fixedWidth"),a(t,l,n),s(t,n,{change:function(){o.html(t.find(":selected").html()),l.removeClass(n.activeClass)},"click touchend":function(){var e=t.find(":selected").html();o.html()!==e&&t.trigger("change")},keyup:function(){o.html(t.find(":selected").html())}}),w(o,n),{remove:function(){return o.remove(),t.unwrap().unbind(n.eventNamespace),t},update:function(){n.selectAutoWidth?(e.uniform.restore(t),t.uniform(n)):(i(l,n),o.html(t.find(":selected").html()),u(l,t,n))}}}},{match:function(e){return e.is("select")&&h(e)?!0:!1},apply:function(e,t){var n;return e.addClass(t.selectMultiClass),n=d(e,t),a(e,e,t),{remove:function(){e.removeClass(t.selectMultiClass),n&&e.unwrap()},update:C}}},{match:function(e){return e.is("textarea")},apply:function(e,t){var n;return e.addClass(t.textareaClass),n=d(e,t),a(e,e,t),{remove:function(){e.removeClass(t.textareaClass),n&&e.unwrap()},update:C}}}];m()&&!v()&&(H=!1),e.uniform={defaults:{activeClass:"active",autoHide:!0,buttonClass:"button",checkboxClass:"checker",checkedClass:"checked",disabledClass:"disabled",eventNamespace:".uniform",fileButtonClass:"action",fileButtonHtml:"Choose File",fileClass:"uploader",fileDefaultHtml:"No file selected",filenameClass:"filename",focusClass:"focus",hoverClass:"hover",idPrefix:"uniform",inputAddTypeAsClass:!0,inputClass:"uniform-input",radioClass:"radio",resetDefaultHtml:"Reset",resetSelector:!1,selectAutoWidth:!0,selectClass:"selector",selectMultiClass:"uniform-multiselect",submitDefaultHtml:"Submit",textareaClass:"uniform",useID:!0,wrapperClass:null},elements:[]},e.fn.uniform=function(t){var n=this;return t=e.extend({},e.uniform.defaults,t),x||(x=!0,f()&&(H=!1)),H?(t.resetSelector&&e(t.resetSelector).mouseup(function(){window.setTimeout(function(){e.uniform.update(n)},10)}),this.each(function(){var n,s,a,i=e(this);if(i.data("uniformed"))return e.uniform.update(i),void 0;for(n=0;A.length>n;n+=1)if(s=A[n],s.match(i,t))return a=s.apply(i,t),i.data("uniformed",a),e.uniform.elements.push(i.get(0)),void 0})):this},e.uniform.restore=e.fn.uniform.restore=function(n){n===t&&(n=e.uniform.elements),e(n).each(function(){var t,n,s=e(this);n=s.data("uniformed"),n&&(n.remove(),t=e.inArray(this,e.uniform.elements),t>=0&&e.uniform.elements.splice(t,1),s.removeData("uniformed"))})},e.uniform.update=e.fn.uniform.update=function(n){n===t&&(n=e.uniform.elements),e(n).each(function(){var t,n=e(this);t=n.data("uniformed"),t&&t.update(n,t.options)})}})(jQuery);
/*
 * Toastr
 * Copyright 2012-2014 John Papa and Hans Fjällemark.
 * All Rights Reserved.
 * Use, reproduction, distribution, and modification of this code is subject to the terms and
 * conditions of the MIT license, available at http://www.opensource.org/licenses/mit-license.php
 *
 * Author: John Papa and Hans Fjällemark
 * ARIA Support: Greta Krafsig
 * Project: https://github.com/CodeSeven/toastr
 */
; (function (define) {
    define(['jquery'], function ($) {
        return (function () {
            var $container;
            var listener;
            var toastId = 0;
            var toastType = {
                error: 'error',
                info: 'info',
                success: 'success',
                warning: 'warning'
            };

            var toastr = {
                clear: clear,
                remove: remove,
                error: error,
                getContainer: getContainer,
                info: info,
                options: {},
                subscribe: subscribe,
                success: success,
                version: '2.1.0',
                warning: warning
            };

            var previousToast;

            return toastr;

            //#region Accessible Methods
            function error(message, title, optionsOverride) {
                return notify({
                    type: toastType.error,
                    iconClass: getOptions().iconClasses.error,
                    message: message,
                    optionsOverride: optionsOverride,
                    title: title
                });
            }

            function getContainer(options, create) {
                if (!options) { options = getOptions(); }
                $container = $('#' + options.containerId);
                if ($container.length) {
                    return $container;
                }
                if(create) {
                    $container = createContainer(options);
                }
                return $container;
            }

            function info(message, title, optionsOverride) {
                return notify({
                    type: toastType.info,
                    iconClass: getOptions().iconClasses.info,
                    message: message,
                    optionsOverride: optionsOverride,
                    title: title
                });
            }

            function subscribe(callback) {
                listener = callback;
            }

            function success(message, title, optionsOverride) {
                return notify({
                    type: toastType.success,
                    iconClass: getOptions().iconClasses.success,
                    message: message,
                    optionsOverride: optionsOverride,
                    title: title
                });
            }

            function warning(message, title, optionsOverride) {
                return notify({
                    type: toastType.warning,
                    iconClass: getOptions().iconClasses.warning,
                    message: message,
                    optionsOverride: optionsOverride,
                    title: title
                });
            }

            function clear($toastElement) {
                var options = getOptions();
                if (!$container) { getContainer(options); }
                if (!clearToast($toastElement, options)) {
                    clearContainer(options);
                }
            }

            function remove($toastElement) {
                var options = getOptions();
                if (!$container) { getContainer(options); }
                if ($toastElement && $(':focus', $toastElement).length === 0) {
                    removeToast($toastElement);
                    return;
                }
                if ($container.children().length) {
                    $container.remove();
                }
            }
            //#endregion

            //#region Internal Methods

            function clearContainer(options){
                var toastsToClear = $container.children();
                for (var i = toastsToClear.length - 1; i >= 0; i--) {
                    clearToast($(toastsToClear[i]), options);
                };
            }

            function clearToast($toastElement, options){
                if ($toastElement && $(':focus', $toastElement).length === 0) {
                    $toastElement[options.hideMethod]({
                        duration: options.hideDuration,
                        easing: options.hideEasing,
                        complete: function () { removeToast($toastElement); }
                    });
                    return true;
                }
                return false;
            }

            function createContainer(options) {
                $container = $('<div/>')
                    .attr('id', options.containerId)
                    .addClass(options.positionClass)
                    .attr('aria-live', 'polite')
                    .attr('role', 'alert');

                $container.appendTo($(options.target));
                return $container;
            }

            function getDefaults() {
                return {
                    tapToDismiss: true,
                    toastClass: 'toast',
                    containerId: 'toast-container',
                    debug: false,

                    showMethod: 'fadeIn', //fadeIn, slideDown, and show are built into jQuery
                    showDuration: 300,
                    showEasing: 'swing', //swing and linear are built into jQuery
                    onShown: undefined,
                    hideMethod: 'fadeOut',
                    hideDuration: 1000,
                    hideEasing: 'swing',
                    onHidden: undefined,

                    extendedTimeOut: 1000,
                    iconClasses: {
                        error: 'toast-error',
                        info: 'toast-info',
                        success: 'toast-success',
                        warning: 'toast-warning'
                    },
                    iconClass: 'toast-info',
                    positionClass: 'toast-top-right',
                    timeOut: 5000, // Set timeOut and extendedTimeOut to 0 to make it sticky
                    titleClass: 'toast-title',
                    messageClass: 'toast-message',
                    target: 'body',
                    closeHtml: '<button>&times;</button>',
                    newestOnTop: true,
                    preventDuplicates: false
                };
            }

            function publish(args) {
                if (!listener) { return; }
                listener(args);
            }

            function notify(map) {
                var options = getOptions(),
                    iconClass = map.iconClass || options.iconClass;

                if(options.preventDuplicates){
                    if(map.message === previousToast){
                        return;
                    }
                    else{
                        previousToast = map.message;
                    }
                }

                if (typeof (map.optionsOverride) !== 'undefined') {
                    options = $.extend(options, map.optionsOverride);
                    iconClass = map.optionsOverride.iconClass || iconClass;
                }

                toastId++;

                $container = getContainer(options, true);
                var intervalId = null,
                    $toastElement = $('<div/>'),
                    $titleElement = $('<div/>'),
                    $messageElement = $('<div/>'),
                    $closeElement = $(options.closeHtml),
                    response = {
                        toastId: toastId,
                        state: 'visible',
                        startTime: new Date(),
                        options: options,
                        map: map
                    };

                if (map.iconClass) {
                    $toastElement.addClass(options.toastClass).addClass(iconClass);
                }

                if (map.title) {
                    $titleElement.append(map.title).addClass(options.titleClass);
                    $toastElement.append($titleElement);
                }

                if (map.message) {
                    $messageElement.append(map.message).addClass(options.messageClass);
                    $toastElement.append($messageElement);
                }

                if (options.closeButton) {
                    $closeElement.addClass('toast-close-button').attr("role", "button");
                    $toastElement.prepend($closeElement);
                }

                $toastElement.hide();
                if (options.newestOnTop) {
                    $container.prepend($toastElement);
                } else {
                    $container.append($toastElement);
                }


                $toastElement[options.showMethod](
                    { duration: options.showDuration, easing: options.showEasing, complete: options.onShown }
                );

                if (options.timeOut > 0) {
                    intervalId = setTimeout(hideToast, options.timeOut);
                }

                $toastElement.hover(stickAround, delayedHideToast);
                if (!options.onclick && options.tapToDismiss) {
                    $toastElement.click(hideToast);
                }

                if (options.closeButton && $closeElement) {
                    $closeElement.click(function (event) {
                        if( event.stopPropagation ) {
                            event.stopPropagation();
                        } else if( event.cancelBubble !== undefined && event.cancelBubble !== true ) {
                            event.cancelBubble = true;
                        }
                        hideToast(true);
                    });
                }

                if (options.onclick) {
                    $toastElement.click(function () {
                        options.onclick();
                        hideToast();
                    });
                }

                publish(response);

                if (options.debug && console) {
                    console.log(response);
                }

                return $toastElement;

                function hideToast(override) {
                    if ($(':focus', $toastElement).length && !override) {
                        return;
                    }
                    return $toastElement[options.hideMethod]({
                        duration: options.hideDuration,
                        easing: options.hideEasing,
                        complete: function () {
                            removeToast($toastElement);
                            if (options.onHidden && response.state !== 'hidden') {
                                options.onHidden();
                            }
                            response.state = 'hidden';
                            response.endTime = new Date();
                            publish(response);
                        }
                    });
                }

                function delayedHideToast() {
                    if (options.timeOut > 0 || options.extendedTimeOut > 0) {
                        intervalId = setTimeout(hideToast, options.extendedTimeOut);
                    }
                }

                function stickAround() {
                    clearTimeout(intervalId);
                    $toastElement.stop(true, true)[options.showMethod](
                        { duration: options.showDuration, easing: options.showEasing }
                    );
                }
            }

            function getOptions() {
                return $.extend({}, getDefaults(), toastr.options);
            }

            function removeToast($toastElement) {
                if (!$container) { $container = getContainer(); }
                if ($toastElement.is(':visible')) {
                    return;
                }
                $toastElement.remove();
                $toastElement = null;
                if ($container.children().length === 0) {
                    $container.remove();
                }
            }
            //#endregion

        })();
    });
}(typeof define === 'function' && define.amd ? define : function (deps, factory) {
    if (typeof module !== 'undefined' && module.exports) { //Node
        module.exports = factory(require('jquery'));
    } else {
        window['toastr'] = factory(window['jQuery']);
    }
}));

/*! Copyright (c) 2011 Piotr Rochala (http://rocha.la)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Improved by keenthemes for Metronic Theme
 * Version: 1.3.2
 *
 */
!function(e){jQuery.fn.extend({slimScroll:function(i){var o={width:"auto",height:"250px",size:"7px",color:"#000",position:"right",distance:"1px",start:"top",opacity:.4,alwaysVisible:!1,disableFadeOut:!1,railVisible:!1,railColor:"#333",railOpacity:.2,railDraggable:!0,railClass:"slimScrollRail",barClass:"slimScrollBar",wrapperClass:"slimScrollDiv",allowPageScroll:!1,wheelStep:20,touchScrollStep:200,borderRadius:"7px",railBorderRadius:"7px",animate:!0},a=e.extend(o,i);return this.each(function(){function o(t){if(u){var t=t||window.event,i=0;t.wheelDelta&&(i=-t.wheelDelta/120),t.detail&&(i=t.detail/3);var o=t.target||t.srcTarget||t.srcElement;e(o).closest("."+a.wrapperClass).is(S.parent())&&r(i,!0),t.preventDefault&&!y&&t.preventDefault(),y||(t.returnValue=!1)}}function r(e,t,i){y=!1;var o=e,r=S.outerHeight()-M.outerHeight();if(t&&(o=parseInt(M.css("top"))+e*parseInt(a.wheelStep)/100*M.outerHeight(),o=Math.min(Math.max(o,0),r),o=e>0?Math.ceil(o):Math.floor(o),M.css({top:o+"px"})),v=parseInt(M.css("top"))/(S.outerHeight()-M.outerHeight()),o=v*(S[0].scrollHeight-S.outerHeight()),i){o=e;var s=o/S[0].scrollHeight*S.outerHeight();s=Math.min(Math.max(s,0),r),M.css({top:s+"px"})}"scrollTo"in a&&a.animate?S.animate({scrollTop:o}):S.scrollTop(o),S.trigger("slimscrolling",~~o),l(),c()}function s(){window.addEventListener?(this.addEventListener("DOMMouseScroll",o,!1),this.addEventListener("mousewheel",o,!1)):document.attachEvent("onmousewheel",o)}function n(){f=Math.max(S.outerHeight()/S[0].scrollHeight*S.outerHeight(),m),M.css({height:f+"px"});var e=f==S.outerHeight()?"none":"block";M.css({display:e})}function l(){if(n(),clearTimeout(p),v==~~v){if(y=a.allowPageScroll,b!=v){var e=0==~~v?"top":"bottom";S.trigger("slimscroll",e)}}else y=!1;return b=v,f>=S.outerHeight()?void(y=!0):(M.stop(!0,!0).fadeIn("fast"),void(a.railVisible&&H.stop(!0,!0).fadeIn("fast")))}function c(){a.alwaysVisible||(p=setTimeout(function(){a.disableFadeOut&&u||h||d||(M.fadeOut("slow"),H.fadeOut("slow"))},1e3))}var u,h,d,p,g,f,v,b,w="<div></div>",m=30,y=!1,S=e(this);if("ontouchstart"in window&&window.navigator.msPointerEnabled&&S.css("-ms-touch-action","none"),S.parent().hasClass(a.wrapperClass)){var E=S.scrollTop();if(M=S.parent().find("."+a.barClass),H=S.parent().find("."+a.railClass),n(),e.isPlainObject(i)){if("height"in i&&"auto"==i.height){S.parent().css("height","auto"),S.css("height","auto");var x=S.parent().parent().height();S.parent().css("height",x),S.css("height",x)}if("scrollTo"in i)E=parseInt(a.scrollTo);else if("scrollBy"in i)E+=parseInt(a.scrollBy);else if("destroy"in i)return M.remove(),H.remove(),void S.unwrap();r(E,!1,!0)}}else{a.height="auto"==i.height?S.parent().height():i.height;var C=e(w).addClass(a.wrapperClass).css({position:"relative",overflow:"hidden",width:a.width,height:a.height});S.css({overflow:"hidden",width:a.width,height:a.height});var H=e(w).addClass(a.railClass).css({width:a.size,height:"100%",position:"absolute",top:0,display:a.alwaysVisible&&a.railVisible?"block":"none","border-radius":a.railBorderRadius,background:a.railColor,opacity:a.railOpacity,zIndex:90}),M=e(w).addClass(a.barClass).css({background:a.color,width:a.size,position:"absolute",top:0,opacity:a.opacity,display:a.alwaysVisible?"block":"none","border-radius":a.borderRadius,BorderRadius:a.borderRadius,MozBorderRadius:a.borderRadius,WebkitBorderRadius:a.borderRadius,zIndex:99}),D="right"==a.position?{right:a.distance}:{left:a.distance};H.css(D),M.css(D),S.wrap(C),S.parent().append(M),S.parent().append(H),a.railDraggable&&M.bind("mousedown",function(i){var o=e(document);return d=!0,t=parseFloat(M.css("top")),pageY=i.pageY,o.bind("mousemove.slimscroll",function(e){currTop=t+e.pageY-pageY,M.css("top",currTop),r(0,M.position().top,!1)}),o.bind("mouseup.slimscroll",function(){d=!1,c(),o.unbind(".slimscroll")}),!1}).bind("selectstart.slimscroll",function(e){return e.stopPropagation(),e.preventDefault(),!1}),"ontouchstart"in window&&window.navigator.msPointerEnabled&&(S.bind("MSPointerDown",function(e){g=e.originalEvent.pageY}),S.bind("MSPointerMove",function(e){e.originalEvent.preventDefault();var t=(g-e.originalEvent.pageY)/a.touchScrollStep;r(t,!0),g=e.originalEvent.pageY})),H.hover(function(){l()},function(){c()}),M.hover(function(){h=!0},function(){h=!1}),S.hover(function(){u=!0,l(),c()},function(){u=!1,c()}),S.bind("touchstart",function(e){e.originalEvent.touches.length&&(g=e.originalEvent.touches[0].pageY)}),S.bind("touchmove",function(e){if(y||e.originalEvent.preventDefault(),e.originalEvent.touches.length){var t=(g-e.originalEvent.touches[0].pageY)/a.touchScrollStep;r(t,!0),g=e.originalEvent.touches[0].pageY}}),n(),"bottom"===a.start?(M.css({top:S.outerHeight()-M.outerHeight()}),r(0,!0)):"top"!==a.start&&(r(e(a.start).position().top,null,!0),a.alwaysVisible||M.hide()),s()}}),this}}),jQuery.fn.extend({slimscroll:jQuery.fn.slimScroll})}(jQuery);
/*!
 * Vue.js v1.0.4
 * (c) 2015 Evan You
 * Released under the MIT License.
 */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["Vue"] = factory();
	else
		root["Vue"] = factory();
})(this, function() {
	return /******/ (function(modules) { // webpackBootstrap
		/******/ 	// The module cache
		/******/ 	var installedModules = {};

		/******/ 	// The require function
		/******/ 	function __webpack_require__(moduleId) {

			/******/ 		// Check if module is in cache
			/******/ 		if(installedModules[moduleId])
			/******/ 			return installedModules[moduleId].exports;

			/******/ 		// Create a new module (and put it into the cache)
			/******/ 		var module = installedModules[moduleId] = {
				/******/ 			exports: {},
				/******/ 			id: moduleId,
				/******/ 			loaded: false
				/******/ 		};

			/******/ 		// Execute the module function
			/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

			/******/ 		// Flag the module as loaded
			/******/ 		module.loaded = true;

			/******/ 		// Return the exports of the module
			/******/ 		return module.exports;
			/******/ 	}


		/******/ 	// expose the modules object (__webpack_modules__)
		/******/ 	__webpack_require__.m = modules;

		/******/ 	// expose the module cache
		/******/ 	__webpack_require__.c = installedModules;

		/******/ 	// __webpack_public_path__
		/******/ 	__webpack_require__.p = "";

		/******/ 	// Load entry module and return exports
		/******/ 	return __webpack_require__(0);
		/******/ })
		/************************************************************************/
		/******/ ([
		/* 0 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var extend = _.extend

			/**
			 * The exposed Vue constructor.
			 *
			 * API conventions:
			 * - public API methods/properties are prefiexed with `$`
			 * - internal methods/properties are prefixed with `_`
			 * - non-prefixed properties are assumed to be proxied user
			 *   data.
			 *
			 * @constructor
			 * @param {Object} [options]
			 * @public
			 */

			function Vue (options) {
				this._init(options)
			}

			/**
			 * Mixin global API
			 */

			extend(Vue, __webpack_require__(13))

			/**
			 * Vue and every constructor that extends Vue has an
			 * associated options object, which can be accessed during
			 * compilation steps as `this.constructor.options`.
			 *
			 * These can be seen as the default options of every
			 * Vue instance.
			 */

			Vue.options = {
				replace: true,
				directives: __webpack_require__(16),
				elementDirectives: __webpack_require__(50),
				filters: __webpack_require__(53),
				transitions: {},
				components: {},
				partials: {}
			}

			/**
			 * Build up the prototype
			 */

			var p = Vue.prototype

			/**
			 * $data has a setter which does a bunch of
			 * teardown/setup work
			 */

			Object.defineProperty(p, '$data', {
				get: function () {
					return this._data
				},
				set: function (newData) {
					if (newData !== this._data) {
						this._setData(newData)
					}
				}
			})

			/**
			 * Mixin internal instance methods
			 */

			extend(p, __webpack_require__(55))
			extend(p, __webpack_require__(56))
			extend(p, __webpack_require__(57))
			extend(p, __webpack_require__(60))
			extend(p, __webpack_require__(62))

			/**
			 * Mixin public API methods
			 */

			extend(p, __webpack_require__(63))
			extend(p, __webpack_require__(64))
			extend(p, __webpack_require__(65))
			extend(p, __webpack_require__(66))

			Vue.version = '1.0.4'
			module.exports = _.Vue = Vue

			/* istanbul ignore if */
			if (true) {
				if (_.inBrowser && window.__VUE_DEVTOOLS_GLOBAL_HOOK__) {
					window.__VUE_DEVTOOLS_GLOBAL_HOOK__.emit('init', Vue)
				}
			}


			/***/ },
		/* 1 */
		/***/ function(module, exports, __webpack_require__) {

			var lang = __webpack_require__(2)
			var extend = lang.extend

			extend(exports, lang)
			extend(exports, __webpack_require__(3))
			extend(exports, __webpack_require__(4))
			extend(exports, __webpack_require__(10))
			extend(exports, __webpack_require__(11))
			extend(exports, __webpack_require__(12))


			/***/ },
		/* 2 */
		/***/ function(module, exports) {

			/**
			 * Set a property on an object. Adds the new property and
			 * triggers change notification if the property doesn't
			 * already exist.
			 *
			 * @param {Object} obj
			 * @param {String} key
			 * @param {*} val
			 * @public
			 */

			exports.set = function set (obj, key, val) {
				if (obj.hasOwnProperty(key)) {
					obj[key] = val
					return
				}
				if (obj._isVue) {
					set(obj._data, key, val)
					return
				}
				var ob = obj.__ob__
				if (!ob) {
					obj[key] = val
					return
				}
				ob.convert(key, val)
				ob.dep.notify()
				if (ob.vms) {
					var i = ob.vms.length
					while (i--) {
						var vm = ob.vms[i]
						vm._proxy(key)
						vm._digest()
					}
				}
			}

			/**
			 * Delete a property and trigger change if necessary.
			 *
			 * @param {Object} obj
			 * @param {String} key
			 */

			exports.delete = function (obj, key) {
				if (!obj.hasOwnProperty(key)) {
					return
				}
				delete obj[key]
				var ob = obj.__ob__
				if (!ob) {
					return
				}
				ob.dep.notify()
				if (ob.vms) {
					var i = ob.vms.length
					while (i--) {
						var vm = ob.vms[i]
						vm._unproxy(key)
						vm._digest()
					}
				}
			}

			/**
			 * Check if an expression is a literal value.
			 *
			 * @param {String} exp
			 * @return {Boolean}
			 */

			var literalValueRE = /^\s?(true|false|[\d\.]+|'[^']*'|"[^"]*")\s?$/
			exports.isLiteral = function (exp) {
				return literalValueRE.test(exp)
			}

			/**
			 * Check if a string starts with $ or _
			 *
			 * @param {String} str
			 * @return {Boolean}
			 */

			exports.isReserved = function (str) {
				var c = (str + '').charCodeAt(0)
				return c === 0x24 || c === 0x5F
			}

			/**
			 * Guard text output, make sure undefined outputs
			 * empty string
			 *
			 * @param {*} value
			 * @return {String}
			 */

			exports.toString = function (value) {
				return value == null
					? ''
					: value.toString()
			}

			/**
			 * Check and convert possible numeric strings to numbers
			 * before setting back to data
			 *
			 * @param {*} value
			 * @return {*|Number}
			 */

			exports.toNumber = function (value) {
				if (typeof value !== 'string') {
					return value
				} else {
					var parsed = Number(value)
					return isNaN(parsed)
						? value
						: parsed
				}
			}

			/**
			 * Convert string boolean literals into real booleans.
			 *
			 * @param {*} value
			 * @return {*|Boolean}
			 */

			exports.toBoolean = function (value) {
				return value === 'true'
					? true
					: value === 'false'
					? false
					: value
			}

			/**
			 * Strip quotes from a string
			 *
			 * @param {String} str
			 * @return {String | false}
			 */

			exports.stripQuotes = function (str) {
				var a = str.charCodeAt(0)
				var b = str.charCodeAt(str.length - 1)
				return a === b && (a === 0x22 || a === 0x27)
					? str.slice(1, -1)
					: str
			}

			/**
			 * Camelize a hyphen-delmited string.
			 *
			 * @param {String} str
			 * @return {String}
			 */

			var camelizeRE = /-(\w)/g
			exports.camelize = function (str) {
				return str.replace(camelizeRE, toUpper)
			}

			function toUpper (_, c) {
				return c ? c.toUpperCase() : ''
			}

			/**
			 * Hyphenate a camelCase string.
			 *
			 * @param {String} str
			 * @return {String}
			 */

			var hyphenateRE = /([a-z\d])([A-Z])/g
			exports.hyphenate = function (str) {
				return str
					.replace(hyphenateRE, '$1-$2')
					.toLowerCase()
			}

			/**
			 * Converts hyphen/underscore/slash delimitered names into
			 * camelized classNames.
			 *
			 * e.g. my-component => MyComponent
			 *      some_else    => SomeElse
			 *      some/comp    => SomeComp
			 *
			 * @param {String} str
			 * @return {String}
			 */

			var classifyRE = /(?:^|[-_\/])(\w)/g
			exports.classify = function (str) {
				return str.replace(classifyRE, toUpper)
			}

			/**
			 * Simple bind, faster than native
			 *
			 * @param {Function} fn
			 * @param {Object} ctx
			 * @return {Function}
			 */

			exports.bind = function (fn, ctx) {
				return function (a) {
					var l = arguments.length
					return l
						? l > 1
						? fn.apply(ctx, arguments)
						: fn.call(ctx, a)
						: fn.call(ctx)
				}
			}

			/**
			 * Convert an Array-like object to a real Array.
			 *
			 * @param {Array-like} list
			 * @param {Number} [start] - start index
			 * @return {Array}
			 */

			exports.toArray = function (list, start) {
				start = start || 0
				var i = list.length - start
				var ret = new Array(i)
				while (i--) {
					ret[i] = list[i + start]
				}
				return ret
			}

			/**
			 * Mix properties into target object.
			 *
			 * @param {Object} to
			 * @param {Object} from
			 */

			exports.extend = function (to, from) {
				var keys = Object.keys(from)
				var i = keys.length
				while (i--) {
					to[keys[i]] = from[keys[i]]
				}
				return to
			}

			/**
			 * Quick object check - this is primarily used to tell
			 * Objects from primitive values when we know the value
			 * is a JSON-compliant type.
			 *
			 * @param {*} obj
			 * @return {Boolean}
			 */

			exports.isObject = function (obj) {
				return obj !== null && typeof obj === 'object'
			}

			/**
			 * Strict object type check. Only returns true
			 * for plain JavaScript objects.
			 *
			 * @param {*} obj
			 * @return {Boolean}
			 */

			var toString = Object.prototype.toString
			var OBJECT_STRING = '[object Object]'
			exports.isPlainObject = function (obj) {
				return toString.call(obj) === OBJECT_STRING
			}

			/**
			 * Array type check.
			 *
			 * @param {*} obj
			 * @return {Boolean}
			 */

			exports.isArray = Array.isArray

			/**
			 * Define a non-enumerable property
			 *
			 * @param {Object} obj
			 * @param {String} key
			 * @param {*} val
			 * @param {Boolean} [enumerable]
			 */

			exports.define = function (obj, key, val, enumerable) {
				Object.defineProperty(obj, key, {
					value: val,
					enumerable: !!enumerable,
					writable: true,
					configurable: true
				})
			}

			/**
			 * Debounce a function so it only gets called after the
			 * input stops arriving after the given wait period.
			 *
			 * @param {Function} func
			 * @param {Number} wait
			 * @return {Function} - the debounced function
			 */

			exports.debounce = function (func, wait) {
				var timeout, args, context, timestamp, result
				var later = function () {
					var last = Date.now() - timestamp
					if (last < wait && last >= 0) {
						timeout = setTimeout(later, wait - last)
					} else {
						timeout = null
						result = func.apply(context, args)
						if (!timeout) context = args = null
					}
				}
				return function () {
					context = this
					args = arguments
					timestamp = Date.now()
					if (!timeout) {
						timeout = setTimeout(later, wait)
					}
					return result
				}
			}

			/**
			 * Manual indexOf because it's slightly faster than
			 * native.
			 *
			 * @param {Array} arr
			 * @param {*} obj
			 */

			exports.indexOf = function (arr, obj) {
				var i = arr.length
				while (i--) {
					if (arr[i] === obj) return i
				}
				return -1
			}

			/**
			 * Make a cancellable version of an async callback.
			 *
			 * @param {Function} fn
			 * @return {Function}
			 */

			exports.cancellable = function (fn) {
				var cb = function () {
					if (!cb.cancelled) {
						return fn.apply(this, arguments)
					}
				}
				cb.cancel = function () {
					cb.cancelled = true
				}
				return cb
			}

			/**
			 * Check if two values are loosely equal - that is,
			 * if they are plain objects, do they have the same shape?
			 *
			 * @param {*} a
			 * @param {*} b
			 * @return {Boolean}
			 */

			exports.looseEqual = function (a, b) {
				/* eslint-disable eqeqeq */
				return a == b || (
						exports.isObject(a) && exports.isObject(b)
							? JSON.stringify(a) === JSON.stringify(b)
							: false
					)
				/* eslint-enable eqeqeq */
			}


			/***/ },
		/* 3 */
		/***/ function(module, exports) {

			// can we use __proto__?
			exports.hasProto = '__proto__' in {}

			// Browser environment sniffing
			var inBrowser = exports.inBrowser =
				typeof window !== 'undefined' &&
				Object.prototype.toString.call(window) !== '[object Object]'

			exports.isIE9 =
				inBrowser &&
				navigator.userAgent.toLowerCase().indexOf('msie 9.0') > 0

			exports.isAndroid =
				inBrowser &&
				navigator.userAgent.toLowerCase().indexOf('android') > 0

			// Transition property/event sniffing
			if (inBrowser && !exports.isIE9) {
				var isWebkitTrans =
					window.ontransitionend === undefined &&
					window.onwebkittransitionend !== undefined
				var isWebkitAnim =
					window.onanimationend === undefined &&
					window.onwebkitanimationend !== undefined
				exports.transitionProp = isWebkitTrans
					? 'WebkitTransition'
					: 'transition'
				exports.transitionEndEvent = isWebkitTrans
					? 'webkitTransitionEnd'
					: 'transitionend'
				exports.animationProp = isWebkitAnim
					? 'WebkitAnimation'
					: 'animation'
				exports.animationEndEvent = isWebkitAnim
					? 'webkitAnimationEnd'
					: 'animationend'
			}

			/**
			 * Defer a task to execute it asynchronously. Ideally this
			 * should be executed as a microtask, so we leverage
			 * MutationObserver if it's available, and fallback to
			 * setTimeout(0).
			 *
			 * @param {Function} cb
			 * @param {Object} ctx
			 */

			exports.nextTick = (function () {
				var callbacks = []
				var pending = false
				var timerFunc
				function nextTickHandler () {
					pending = false
					var copies = callbacks.slice(0)
					callbacks = []
					for (var i = 0; i < copies.length; i++) {
						copies[i]()
					}
				}
				/* istanbul ignore if */
				if (typeof MutationObserver !== 'undefined') {
					var counter = 1
					var observer = new MutationObserver(nextTickHandler)
					var textNode = document.createTextNode(counter)
					observer.observe(textNode, {
						characterData: true
					})
					timerFunc = function () {
						counter = (counter + 1) % 2
						textNode.data = counter
					}
				} else {
					timerFunc = setTimeout
				}
				return function (cb, ctx) {
					var func = ctx
						? function () { cb.call(ctx) }
						: cb
					callbacks.push(func)
					if (pending) return
					pending = true
					timerFunc(nextTickHandler, 0)
				}
			})()


			/***/ },
		/* 4 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var config = __webpack_require__(5)
			var transition = __webpack_require__(9)

			/**
			 * Query an element selector if it's not an element already.
			 *
			 * @param {String|Element} el
			 * @return {Element}
			 */

			exports.query = function (el) {
				if (typeof el === 'string') {
					var selector = el
					el = document.querySelector(el)
					if (!el) {
						("development") !== 'production' && _.warn(
							'Cannot find element: ' + selector
						)
					}
				}
				return el
			}

			/**
			 * Check if a node is in the document.
			 * Note: document.documentElement.contains should work here
			 * but always returns false for comment nodes in phantomjs,
			 * making unit tests difficult. This is fixed by doing the
			 * contains() check on the node's parentNode instead of
			 * the node itself.
			 *
			 * @param {Node} node
			 * @return {Boolean}
			 */

			exports.inDoc = function (node) {
				var doc = document.documentElement
				var parent = node && node.parentNode
				return doc === node ||
					doc === parent ||
					!!(parent && parent.nodeType === 1 && (doc.contains(parent)))
			}

			/**
			 * Get and remove an attribute from a node.
			 *
			 * @param {Node} node
			 * @param {String} attr
			 */

			exports.attr = function (node, attr) {
				var val = node.getAttribute(attr)
				if (val !== null) {
					node.removeAttribute(attr)
				}
				return val
			}

			/**
			 * Get an attribute with colon or v-bind: prefix.
			 *
			 * @param {Node} node
			 * @param {String} name
			 * @return {String|null}
			 */

			exports.getBindAttr = function (node, name) {
				var val = exports.attr(node, ':' + name)
				if (val === null) {
					val = exports.attr(node, 'v-bind:' + name)
				}
				return val
			}

			/**
			 * Insert el before target
			 *
			 * @param {Element} el
			 * @param {Element} target
			 */

			exports.before = function (el, target) {
				target.parentNode.insertBefore(el, target)
			}

			/**
			 * Insert el after target
			 *
			 * @param {Element} el
			 * @param {Element} target
			 */

			exports.after = function (el, target) {
				if (target.nextSibling) {
					exports.before(el, target.nextSibling)
				} else {
					target.parentNode.appendChild(el)
				}
			}

			/**
			 * Remove el from DOM
			 *
			 * @param {Element} el
			 */

			exports.remove = function (el) {
				el.parentNode.removeChild(el)
			}

			/**
			 * Prepend el to target
			 *
			 * @param {Element} el
			 * @param {Element} target
			 */

			exports.prepend = function (el, target) {
				if (target.firstChild) {
					exports.before(el, target.firstChild)
				} else {
					target.appendChild(el)
				}
			}

			/**
			 * Replace target with el
			 *
			 * @param {Element} target
			 * @param {Element} el
			 */

			exports.replace = function (target, el) {
				var parent = target.parentNode
				if (parent) {
					parent.replaceChild(el, target)
				}
			}

			/**
			 * Add event listener shorthand.
			 *
			 * @param {Element} el
			 * @param {String} event
			 * @param {Function} cb
			 */

			exports.on = function (el, event, cb) {
				el.addEventListener(event, cb)
			}

			/**
			 * Remove event listener shorthand.
			 *
			 * @param {Element} el
			 * @param {String} event
			 * @param {Function} cb
			 */

			exports.off = function (el, event, cb) {
				el.removeEventListener(event, cb)
			}

			/**
			 * Add class with compatibility for IE & SVG
			 *
			 * @param {Element} el
			 * @param {Strong} cls
			 */

			exports.addClass = function (el, cls) {
				if (el.classList) {
					el.classList.add(cls)
				} else {
					var cur = ' ' + (el.getAttribute('class') || '') + ' '
					if (cur.indexOf(' ' + cls + ' ') < 0) {
						el.setAttribute('class', (cur + cls).trim())
					}
				}
			}

			/**
			 * Remove class with compatibility for IE & SVG
			 *
			 * @param {Element} el
			 * @param {Strong} cls
			 */

			exports.removeClass = function (el, cls) {
				if (el.classList) {
					el.classList.remove(cls)
				} else {
					var cur = ' ' + (el.getAttribute('class') || '') + ' '
					var tar = ' ' + cls + ' '
					while (cur.indexOf(tar) >= 0) {
						cur = cur.replace(tar, ' ')
					}
					el.setAttribute('class', cur.trim())
				}
				if (!el.className) {
					el.removeAttribute('class')
				}
			}

			/**
			 * Extract raw content inside an element into a temporary
			 * container div
			 *
			 * @param {Element} el
			 * @param {Boolean} asFragment
			 * @return {Element}
			 */

			exports.extractContent = function (el, asFragment) {
				var child
				var rawContent
				/* istanbul ignore if */
				if (
					exports.isTemplate(el) &&
					el.content instanceof DocumentFragment
				) {
					el = el.content
				}
				if (el.hasChildNodes()) {
					exports.trimNode(el)
					rawContent = asFragment
						? document.createDocumentFragment()
						: document.createElement('div')
					/* eslint-disable no-cond-assign */
					while (child = el.firstChild) {
						/* eslint-enable no-cond-assign */
						rawContent.appendChild(child)
					}
				}
				return rawContent
			}

			/**
			 * Trim possible empty head/tail textNodes inside a parent.
			 *
			 * @param {Node} node
			 */

			exports.trimNode = function (node) {
				trim(node, node.firstChild)
				trim(node, node.lastChild)
			}

			function trim (parent, node) {
				if (node && node.nodeType === 3 && !node.data.trim()) {
					parent.removeChild(node)
				}
			}

			/**
			 * Check if an element is a template tag.
			 * Note if the template appears inside an SVG its tagName
			 * will be in lowercase.
			 *
			 * @param {Element} el
			 */

			exports.isTemplate = function (el) {
				return el.tagName &&
					el.tagName.toLowerCase() === 'template'
			}

			/**
			 * Create an "anchor" for performing dom insertion/removals.
			 * This is used in a number of scenarios:
			 * - fragment instance
			 * - v-html
			 * - v-if
			 * - v-for
			 * - component
			 *
			 * @param {String} content
			 * @param {Boolean} persist - IE trashes empty textNodes on
			 *                            cloneNode(true), so in certain
			 *                            cases the anchor needs to be
			 *                            non-empty to be persisted in
			 *                            templates.
			 * @return {Comment|Text}
			 */

			exports.createAnchor = function (content, persist) {
				return config.debug
					? document.createComment(content)
					: document.createTextNode(persist ? ' ' : '')
			}

			/**
			 * Find a component ref attribute that starts with $.
			 *
			 * @param {Element} node
			 * @return {String|undefined}
			 */

			var refRE = /^v-ref:/
			exports.findRef = function (node) {
				if (node.hasAttributes()) {
					var attrs = node.attributes
					for (var i = 0, l = attrs.length; i < l; i++) {
						var name = attrs[i].name
						if (refRE.test(name)) {
							node.removeAttribute(name)
							return _.camelize(name.replace(refRE, ''))
						}
					}
				}
			}

			/**
			 * Map a function to a range of nodes .
			 *
			 * @param {Node} node
			 * @param {Node} end
			 * @param {Function} op
			 */

			exports.mapNodeRange = function (node, end, op) {
				var next
				while (node !== end) {
					next = node.nextSibling
					op(node)
					node = next
				}
				op(end)
			}

			/**
			 * Remove a range of nodes with transition, store
			 * the nodes in a fragment with correct ordering,
			 * and call callback when done.
			 *
			 * @param {Node} start
			 * @param {Node} end
			 * @param {Vue} vm
			 * @param {DocumentFragment} frag
			 * @param {Function} cb
			 */

			exports.removeNodeRange = function (start, end, vm, frag, cb) {
				var done = false
				var removed = 0
				var nodes = []
				exports.mapNodeRange(start, end, function (node) {
					if (node === end) done = true
					nodes.push(node)
					transition.remove(node, vm, onRemoved)
				})
				function onRemoved () {
					removed++
					if (done && removed >= nodes.length) {
						for (var i = 0; i < nodes.length; i++) {
							frag.appendChild(nodes[i])
						}
						cb && cb()
					}
				}
			}


			/***/ },
		/* 5 */
		/***/ function(module, exports, __webpack_require__) {

			module.exports = {

				/**
				 * Whether to print debug messages.
				 * Also enables stack trace for warnings.
				 *
				 * @type {Boolean}
				 */

				debug: false,

				/**
				 * Whether to suppress warnings.
				 *
				 * @type {Boolean}
				 */

				silent: false,

				/**
				 * Whether to use async rendering.
				 */

				async: true,

				/**
				 * Whether to warn against errors caught when evaluating
				 * expressions.
				 */

				warnExpressionErrors: true,

				/**
				 * Internal flag to indicate the delimiters have been
				 * changed.
				 *
				 * @type {Boolean}
				 */

				_delimitersChanged: true,

				/**
				 * List of asset types that a component can own.
				 *
				 * @type {Array}
				 */

				_assetTypes: [
					'component',
					'directive',
					'elementDirective',
					'filter',
					'transition',
					'partial'
				],

				/**
				 * prop binding modes
				 */

				_propBindingModes: {
					ONE_WAY: 0,
					TWO_WAY: 1,
					ONE_TIME: 2
				},

				/**
				 * Max circular updates allowed in a batcher flush cycle.
				 */

				_maxUpdateCount: 100

			}

			/**
			 * Interpolation delimiters. Changing these would trigger
			 * the text parser to re-compile the regular expressions.
			 *
			 * @type {Array<String>}
			 */

			var delimiters = ['{{', '}}']
			var unsafeDelimiters = ['{{{', '}}}']
			var textParser = __webpack_require__(6)

			Object.defineProperty(module.exports, 'delimiters', {
				get: function () {
					return delimiters
				},
				set: function (val) {
					delimiters = val
					textParser.compileRegex()
				}
			})

			Object.defineProperty(module.exports, 'unsafeDelimiters', {
				get: function () {
					return unsafeDelimiters
				},
				set: function (val) {
					unsafeDelimiters = val
					textParser.compileRegex()
				}
			})


			/***/ },
		/* 6 */
		/***/ function(module, exports, __webpack_require__) {

			var Cache = __webpack_require__(7)
			var config = __webpack_require__(5)
			var dirParser = __webpack_require__(8)
			var regexEscapeRE = /[-.*+?^${}()|[\]\/\\]/g
			var cache, tagRE, htmlRE

			/**
			 * Escape a string so it can be used in a RegExp
			 * constructor.
			 *
			 * @param {String} str
			 */

			function escapeRegex (str) {
				return str.replace(regexEscapeRE, '\\$&')
			}

			exports.compileRegex = function () {
				var open = escapeRegex(config.delimiters[0])
				var close = escapeRegex(config.delimiters[1])
				var unsafeOpen = escapeRegex(config.unsafeDelimiters[0])
				var unsafeClose = escapeRegex(config.unsafeDelimiters[1])
				tagRE = new RegExp(
					unsafeOpen + '(.+?)' + unsafeClose + '|' +
					open + '(.+?)' + close,
					'g'
				)
				htmlRE = new RegExp(
					'^' + unsafeOpen + '.*' + unsafeClose + '$'
				)
				// reset cache
				cache = new Cache(1000)
			}

			/**
			 * Parse a template text string into an array of tokens.
			 *
			 * @param {String} text
			 * @return {Array<Object> | null}
			 *               - {String} type
			 *               - {String} value
			 *               - {Boolean} [html]
			 *               - {Boolean} [oneTime]
			 */

			exports.parse = function (text) {
				if (!cache) {
					exports.compileRegex()
				}
				var hit = cache.get(text)
				if (hit) {
					return hit
				}
				text = text.replace(/\n/g, '')
				if (!tagRE.test(text)) {
					return null
				}
				var tokens = []
				var lastIndex = tagRE.lastIndex = 0
				var match, index, html, value, first, oneTime
				/* eslint-disable no-cond-assign */
				while (match = tagRE.exec(text)) {
					/* eslint-enable no-cond-assign */
					index = match.index
					// push text token
					if (index > lastIndex) {
						tokens.push({
							value: text.slice(lastIndex, index)
						})
					}
					// tag token
					html = htmlRE.test(match[0])
					value = html ? match[1] : match[2]
					first = value.charCodeAt(0)
					oneTime = first === 42 // *
					value = oneTime
						? value.slice(1)
						: value
					tokens.push({
						tag: true,
						value: value.trim(),
						html: html,
						oneTime: oneTime
					})
					lastIndex = index + match[0].length
				}
				if (lastIndex < text.length) {
					tokens.push({
						value: text.slice(lastIndex)
					})
				}
				cache.put(text, tokens)
				return tokens
			}

			/**
			 * Format a list of tokens into an expression.
			 * e.g. tokens parsed from 'a {{b}} c' can be serialized
			 * into one single expression as '"a " + b + " c"'.
			 *
			 * @param {Array} tokens
			 * @return {String}
			 */

			exports.tokensToExp = function (tokens) {
				if (tokens.length > 1) {
					return tokens.map(function (token) {
						return formatToken(token)
					}).join('+')
				} else {
					return formatToken(tokens[0], true)
				}
			}

			/**
			 * Format a single token.
			 *
			 * @param {Object} token
			 * @param {Boolean} single
			 * @return {String}
			 */

			function formatToken (token, single) {
				return token.tag
					? inlineFilters(token.value, single)
					: '"' + token.value + '"'
			}

			/**
			 * For an attribute with multiple interpolation tags,
			 * e.g. attr="some-{{thing | filter}}", in order to combine
			 * the whole thing into a single watchable expression, we
			 * have to inline those filters. This function does exactly
			 * that. This is a bit hacky but it avoids heavy changes
			 * to directive parser and watcher mechanism.
			 *
			 * @param {String} exp
			 * @param {Boolean} single
			 * @return {String}
			 */

			var filterRE = /[^|]\|[^|]/
			function inlineFilters (exp, single) {
				if (!filterRE.test(exp)) {
					return single
						? exp
						: '(' + exp + ')'
				} else {
					var dir = dirParser.parse(exp)
					if (!dir.filters) {
						return '(' + exp + ')'
					} else {
						return 'this._applyFilters(' +
							dir.expression + // value
							',null,' +       // oldValue (null for read)
							JSON.stringify(dir.filters) + // filter descriptors
							',false)'        // write?
					}
				}
			}


			/***/ },
		/* 7 */
		/***/ function(module, exports) {

			/**
			 * A doubly linked list-based Least Recently Used (LRU)
			 * cache. Will keep most recently used items while
			 * discarding least recently used items when its limit is
			 * reached. This is a bare-bone version of
			 * Rasmus Andersson's js-lru:
			 *
			 *   https://github.com/rsms/js-lru
			 *
			 * @param {Number} limit
			 * @constructor
			 */

			function Cache (limit) {
				this.size = 0
				this.limit = limit
				this.head = this.tail = undefined
				this._keymap = Object.create(null)
			}

			var p = Cache.prototype

			/**
			 * Put <value> into the cache associated with <key>.
			 * Returns the entry which was removed to make room for
			 * the new entry. Otherwise undefined is returned.
			 * (i.e. if there was enough room already).
			 *
			 * @param {String} key
			 * @param {*} value
			 * @return {Entry|undefined}
			 */

			p.put = function (key, value) {
				var entry = {
					key: key,
					value: value
				}
				this._keymap[key] = entry
				if (this.tail) {
					this.tail.newer = entry
					entry.older = this.tail
				} else {
					this.head = entry
				}
				this.tail = entry
				if (this.size === this.limit) {
					return this.shift()
				} else {
					this.size++
				}
			}

			/**
			 * Purge the least recently used (oldest) entry from the
			 * cache. Returns the removed entry or undefined if the
			 * cache was empty.
			 */

			p.shift = function () {
				var entry = this.head
				if (entry) {
					this.head = this.head.newer
					this.head.older = undefined
					entry.newer = entry.older = undefined
					this._keymap[entry.key] = undefined
				}
				return entry
			}

			/**
			 * Get and register recent use of <key>. Returns the value
			 * associated with <key> or undefined if not in cache.
			 *
			 * @param {String} key
			 * @param {Boolean} returnEntry
			 * @return {Entry|*}
			 */

			p.get = function (key, returnEntry) {
				var entry = this._keymap[key]
				if (entry === undefined) return
				if (entry === this.tail) {
					return returnEntry
						? entry
						: entry.value
				}
				// HEAD--------------TAIL
				//   <.older   .newer>
				//  <--- add direction --
				//   A  B  C  <D>  E
				if (entry.newer) {
					if (entry === this.head) {
						this.head = entry.newer
					}
					entry.newer.older = entry.older // C <-- E.
				}
				if (entry.older) {
					entry.older.newer = entry.newer // C. --> E
				}
				entry.newer = undefined // D --x
				entry.older = this.tail // D. --> E
				if (this.tail) {
					this.tail.newer = entry // E. <-- D
				}
				this.tail = entry
				return returnEntry
					? entry
					: entry.value
			}

			module.exports = Cache


			/***/ },
		/* 8 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var Cache = __webpack_require__(7)
			var cache = new Cache(1000)
			var filterTokenRE = /[^\s'"]+|'[^']*'|"[^"]*"/g
			var reservedArgRE = /^in$|^-?\d+/

			/**
			 * Parser state
			 */

			var str, dir
			var c, i, l, lastFilterIndex
			var inSingle, inDouble, curly, square, paren

			/**
			 * Push a filter to the current directive object
			 */

			function pushFilter () {
				var exp = str.slice(lastFilterIndex, i).trim()
				var filter
				if (exp) {
					filter = {}
					var tokens = exp.match(filterTokenRE)
					filter.name = tokens[0]
					if (tokens.length > 1) {
						filter.args = tokens.slice(1).map(processFilterArg)
					}
				}
				if (filter) {
					(dir.filters = dir.filters || []).push(filter)
				}
				lastFilterIndex = i + 1
			}

			/**
			 * Check if an argument is dynamic and strip quotes.
			 *
			 * @param {String} arg
			 * @return {Object}
			 */

			function processFilterArg (arg) {
				if (reservedArgRE.test(arg)) {
					return {
						value: _.toNumber(arg),
						dynamic: false
					}
				} else {
					var stripped = _.stripQuotes(arg)
					var dynamic = stripped === arg
					return {
						value: dynamic ? arg : stripped,
						dynamic: dynamic
					}
				}
			}

			/**
			 * Parse a directive value and extract the expression
			 * and its filters into a descriptor.
			 *
			 * Example:
			 *
			 * "a + 1 | uppercase" will yield:
			 * {
	 *   expression: 'a + 1',
	 *   filters: [
	 *     { name: 'uppercase', args: null }
	 *   ]
	 * }
			 *
			 * @param {String} str
			 * @return {Object}
			 */

			exports.parse = function (s) {

				var hit = cache.get(s)
				if (hit) {
					return hit
				}

				// reset parser state
				str = s
				inSingle = inDouble = false
				curly = square = paren = 0
				lastFilterIndex = 0
				dir = {}

				for (i = 0, l = str.length; i < l; i++) {
					c = str.charCodeAt(i)
					if (inSingle) {
						// check single quote
						if (c === 0x27) inSingle = !inSingle
					} else if (inDouble) {
						// check double quote
						if (c === 0x22) inDouble = !inDouble
					} else if (
						c === 0x7C && // pipe
						str.charCodeAt(i + 1) !== 0x7C &&
						str.charCodeAt(i - 1) !== 0x7C
					) {
						if (dir.expression == null) {
							// first filter, end of expression
							lastFilterIndex = i + 1
							dir.expression = str.slice(0, i).trim()
						} else {
							// already has filter
							pushFilter()
						}
					} else {
						switch (c) {
							case 0x22: inDouble = true; break // "
							case 0x27: inSingle = true; break // '
							case 0x28: paren++; break         // (
							case 0x29: paren--; break         // )
							case 0x5B: square++; break        // [
							case 0x5D: square--; break        // ]
							case 0x7B: curly++; break         // {
							case 0x7D: curly--; break         // }
						}
					}
				}

				if (dir.expression == null) {
					dir.expression = str.slice(0, i).trim()
				} else if (lastFilterIndex !== 0) {
					pushFilter()
				}

				cache.put(s, dir)
				return dir
			}


			/***/ },
		/* 9 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)

			/**
			 * Append with transition.
			 *
			 * @param {Element} el
			 * @param {Element} target
			 * @param {Vue} vm
			 * @param {Function} [cb]
			 */

			exports.append = function (el, target, vm, cb) {
				apply(el, 1, function () {
					target.appendChild(el)
				}, vm, cb)
			}

			/**
			 * InsertBefore with transition.
			 *
			 * @param {Element} el
			 * @param {Element} target
			 * @param {Vue} vm
			 * @param {Function} [cb]
			 */

			exports.before = function (el, target, vm, cb) {
				apply(el, 1, function () {
					_.before(el, target)
				}, vm, cb)
			}

			/**
			 * Remove with transition.
			 *
			 * @param {Element} el
			 * @param {Vue} vm
			 * @param {Function} [cb]
			 */

			exports.remove = function (el, vm, cb) {
				apply(el, -1, function () {
					_.remove(el)
				}, vm, cb)
			}

			/**
			 * Apply transitions with an operation callback.
			 *
			 * @param {Element} el
			 * @param {Number} direction
			 *                  1: enter
			 *                 -1: leave
			 * @param {Function} op - the actual DOM operation
			 * @param {Vue} vm
			 * @param {Function} [cb]
			 */

			var apply = exports.apply = function (el, direction, op, vm, cb) {
				var transition = el.__v_trans
				if (
					!transition ||
						// skip if there are no js hooks and CSS transition is
						// not supported
					(!transition.hooks && !_.transitionEndEvent) ||
						// skip transitions for initial compile
					!vm._isCompiled ||
						// if the vm is being manipulated by a parent directive
						// during the parent's compilation phase, skip the
						// animation.
					(vm.$parent && !vm.$parent._isCompiled)
				) {
					op()
					if (cb) cb()
					return
				}
				var action = direction > 0 ? 'enter' : 'leave'
				transition[action](op, cb)
			}


			/***/ },
		/* 10 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var config = __webpack_require__(5)
			var extend = _.extend

			/**
			 * Option overwriting strategies are functions that handle
			 * how to merge a parent option value and a child option
			 * value into the final value.
			 *
			 * All strategy functions follow the same signature:
			 *
			 * @param {*} parentVal
			 * @param {*} childVal
			 * @param {Vue} [vm]
			 */

			var strats = config.optionMergeStrategies = Object.create(null)

			/**
			 * Helper that recursively merges two data objects together.
			 */

			function mergeData (to, from) {
				var key, toVal, fromVal
				for (key in from) {
					toVal = to[key]
					fromVal = from[key]
					if (!to.hasOwnProperty(key)) {
						_.set(to, key, fromVal)
					} else if (_.isObject(toVal) && _.isObject(fromVal)) {
						mergeData(toVal, fromVal)
					}
				}
				return to
			}

			/**
			 * Data
			 */

			strats.data = function (parentVal, childVal, vm) {
				if (!vm) {
					// in a Vue.extend merge, both should be functions
					if (!childVal) {
						return parentVal
					}
					if (typeof childVal !== 'function') {
						("development") !== 'production' && _.warn(
							'The "data" option should be a function ' +
							'that returns a per-instance value in component ' +
							'definitions.'
						)
						return parentVal
					}
					if (!parentVal) {
						return childVal
					}
					// when parentVal & childVal are both present,
					// we need to return a function that returns the
					// merged result of both functions... no need to
					// check if parentVal is a function here because
					// it has to be a function to pass previous merges.
					return function mergedDataFn () {
						return mergeData(
							childVal.call(this),
							parentVal.call(this)
						)
					}
				} else if (parentVal || childVal) {
					return function mergedInstanceDataFn () {
						// instance merge
						var instanceData = typeof childVal === 'function'
							? childVal.call(vm)
							: childVal
						var defaultData = typeof parentVal === 'function'
							? parentVal.call(vm)
							: undefined
						if (instanceData) {
							return mergeData(instanceData, defaultData)
						} else {
							return defaultData
						}
					}
				}
			}

			/**
			 * El
			 */

			strats.el = function (parentVal, childVal, vm) {
				if (!vm && childVal && typeof childVal !== 'function') {
					("development") !== 'production' && _.warn(
						'The "el" option should be a function ' +
						'that returns a per-instance value in component ' +
						'definitions.'
					)
					return
				}
				var ret = childVal || parentVal
				// invoke the element factory if this is instance merge
				return vm && typeof ret === 'function'
					? ret.call(vm)
					: ret
			}

			/**
			 * Hooks and param attributes are merged as arrays.
			 */

			strats.init =
				strats.created =
					strats.ready =
						strats.attached =
							strats.detached =
								strats.beforeCompile =
									strats.compiled =
										strats.beforeDestroy =
											strats.destroyed = function (parentVal, childVal) {
												return childVal
													? parentVal
													? parentVal.concat(childVal)
													: _.isArray(childVal)
													? childVal
													: [childVal]
													: parentVal
											}

			/**
			 * 0.11 deprecation warning
			 */

			strats.paramAttributes = function () {
				/* istanbul ignore next */
				("development") !== 'production' && _.warn(
					'"paramAttributes" option has been deprecated in 0.12. ' +
					'Use "props" instead.'
				)
			}

			/**
			 * Assets
			 *
			 * When a vm is present (instance creation), we need to do
			 * a three-way merge between constructor options, instance
			 * options and parent options.
			 */

			function mergeAssets (parentVal, childVal) {
				var res = Object.create(parentVal)
				return childVal
					? extend(res, guardArrayAssets(childVal))
					: res
			}

			config._assetTypes.forEach(function (type) {
				strats[type + 's'] = mergeAssets
			})

			/**
			 * Events & Watchers.
			 *
			 * Events & watchers hashes should not overwrite one
			 * another, so we merge them as arrays.
			 */

			strats.watch =
				strats.events = function (parentVal, childVal) {
					if (!childVal) return parentVal
					if (!parentVal) return childVal
					var ret = {}
					extend(ret, parentVal)
					for (var key in childVal) {
						var parent = ret[key]
						var child = childVal[key]
						if (parent && !_.isArray(parent)) {
							parent = [parent]
						}
						ret[key] = parent
							? parent.concat(child)
							: [child]
					}
					return ret
				}

			/**
			 * Other object hashes.
			 */

			strats.props =
				strats.methods =
					strats.computed = function (parentVal, childVal) {
						if (!childVal) return parentVal
						if (!parentVal) return childVal
						var ret = Object.create(null)
						extend(ret, parentVal)
						extend(ret, childVal)
						return ret
					}

			/**
			 * Default strategy.
			 */

			var defaultStrat = function (parentVal, childVal) {
				return childVal === undefined
					? parentVal
					: childVal
			}

			/**
			 * Make sure component options get converted to actual
			 * constructors.
			 *
			 * @param {Object} options
			 */

			function guardComponents (options) {
				if (options.components) {
					var components = options.components =
						guardArrayAssets(options.components)
					var def
					var ids = Object.keys(components)
					for (var i = 0, l = ids.length; i < l; i++) {
						var key = ids[i]
						if (_.commonTagRE.test(key)) {
							("development") !== 'production' && _.warn(
								'Do not use built-in HTML elements as component ' +
								'id: ' + key
							)
							continue
						}
						def = components[key]
						if (_.isPlainObject(def)) {
							components[key] = _.Vue.extend(def)
						}
					}
				}
			}

			/**
			 * Ensure all props option syntax are normalized into the
			 * Object-based format.
			 *
			 * @param {Object} options
			 */

			function guardProps (options) {
				var props = options.props
				var i
				if (_.isArray(props)) {
					options.props = {}
					i = props.length
					while (i--) {
						options.props[props[i]] = null
					}
				} else if (_.isPlainObject(props)) {
					var keys = Object.keys(props)
					i = keys.length
					while (i--) {
						var val = props[keys[i]]
						if (typeof val === 'function') {
							props[keys[i]] = { type: val }
						}
					}
				}
			}

			/**
			 * Guard an Array-format assets option and converted it
			 * into the key-value Object format.
			 *
			 * @param {Object|Array} assets
			 * @return {Object}
			 */

			function guardArrayAssets (assets) {
				if (_.isArray(assets)) {
					var res = {}
					var i = assets.length
					var asset
					while (i--) {
						asset = assets[i]
						var id = typeof asset === 'function'
							? ((asset.options && asset.options.name) || asset.id)
							: (asset.name || asset.id)
						if (!id) {
							("development") !== 'production' && _.warn(
								'Array-syntax assets must provide a "name" or "id" field.'
							)
						} else {
							res[id] = asset
						}
					}
					return res
				}
				return assets
			}

			/**
			 * Merge two option objects into a new one.
			 * Core utility used in both instantiation and inheritance.
			 *
			 * @param {Object} parent
			 * @param {Object} child
			 * @param {Vue} [vm] - if vm is present, indicates this is
			 *                     an instantiation merge.
			 */

			exports.mergeOptions = function merge (parent, child, vm) {
				guardComponents(child)
				guardProps(child)
				var options = {}
				var key
				if (child.mixins) {
					for (var i = 0, l = child.mixins.length; i < l; i++) {
						parent = merge(parent, child.mixins[i], vm)
					}
				}
				for (key in parent) {
					mergeField(key)
				}
				for (key in child) {
					if (!(parent.hasOwnProperty(key))) {
						mergeField(key)
					}
				}
				function mergeField (key) {
					var strat = strats[key] || defaultStrat
					options[key] = strat(parent[key], child[key], vm, key)
				}
				return options
			}

			/**
			 * Resolve an asset.
			 * This function is used because child instances need access
			 * to assets defined in its ancestor chain.
			 *
			 * @param {Object} options
			 * @param {String} type
			 * @param {String} id
			 * @return {Object|Function}
			 */

			exports.resolveAsset = function resolve (options, type, id) {
				var assets = options[type]
				var camelizedId
				return assets[id] ||
						// camelCase ID
					assets[camelizedId = _.camelize(id)] ||
						// Pascal Case ID
					assets[camelizedId.charAt(0).toUpperCase() + camelizedId.slice(1)]
			}


			/***/ },
		/* 11 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)

			/**
			 * Check if an element is a component, if yes return its
			 * component id.
			 *
			 * @param {Element} el
			 * @param {Object} options
			 * @return {Object|undefined}
			 */

			exports.commonTagRE = /^(div|p|span|img|a|b|i|br|ul|ol|li|h1|h2|h3|h4|h5|h6|code|pre|table|th|td|tr|form|label|input|select|option|nav|article|section|header|footer)$/
			exports.checkComponent = function (el, options) {
				var tag = el.tagName.toLowerCase()
				var hasAttrs = el.hasAttributes()
				if (!exports.commonTagRE.test(tag) && tag !== 'component') {
					if (_.resolveAsset(options, 'components', tag)) {
						return { id: tag }
					} else {
						var is = hasAttrs && getIsBinding(el)
						if (is) {
							return is
						} else if (true) {
							if (
								tag.indexOf('-') > -1 ||
								(
									/HTMLUnknownElement/.test(el.toString()) &&
										// Chrome returns unknown for several HTML5 elements.
										// https://code.google.com/p/chromium/issues/detail?id=540526
									!/^(data|time|rtc|rb)$/.test(tag)
								)
							) {
								_.warn(
									'Unknown custom element: <' + tag + '> - did you ' +
									'register the component correctly?'
								)
							}
						}
					}
				} else if (hasAttrs) {
					return getIsBinding(el)
				}
			}

			/**
			 * Get "is" binding from an element.
			 *
			 * @param {Element} el
			 * @return {Object|undefined}
			 */

			function getIsBinding (el) {
				// dynamic syntax
				var exp = _.attr(el, 'is')
				if (exp != null) {
					return { id: exp }
				} else {
					exp = _.getBindAttr(el, 'is')
					if (exp != null) {
						return { id: exp, dynamic: true }
					}
				}
			}

			/**
			 * Set a prop's initial value on a vm and its data object.
			 *
			 * @param {Vue} vm
			 * @param {Object} prop
			 * @param {*} value
			 */

			exports.initProp = function (vm, prop, value) {
				if (exports.assertProp(prop, value)) {
					var key = prop.path
					vm[key] = vm._data[key] = value
				}
			}

			/**
			 * Assert whether a prop is valid.
			 *
			 * @param {Object} prop
			 * @param {*} value
			 */

			exports.assertProp = function (prop, value) {
				// if a prop is not provided and is not required,
				// skip the check.
				if (prop.raw === null && !prop.required) {
					return true
				}
				var options = prop.options
				var type = options.type
				var valid = true
				var expectedType
				if (type) {
					if (type === String) {
						expectedType = 'string'
						valid = typeof value === expectedType
					} else if (type === Number) {
						expectedType = 'number'
						valid = typeof value === 'number'
					} else if (type === Boolean) {
						expectedType = 'boolean'
						valid = typeof value === 'boolean'
					} else if (type === Function) {
						expectedType = 'function'
						valid = typeof value === 'function'
					} else if (type === Object) {
						expectedType = 'object'
						valid = _.isPlainObject(value)
					} else if (type === Array) {
						expectedType = 'array'
						valid = _.isArray(value)
					} else {
						valid = value instanceof type
					}
				}
				if (!valid) {
					("development") !== 'production' && _.warn(
						'Invalid prop: type check failed for ' +
						prop.path + '="' + prop.raw + '".' +
						' Expected ' + formatType(expectedType) +
						', got ' + formatValue(value) + '.'
					)
					return false
				}
				var validator = options.validator
				if (validator) {
					if (!validator.call(null, value)) {
						("development") !== 'production' && _.warn(
							'Invalid prop: custom validator check failed for ' +
							prop.path + '="' + prop.raw + '"'
						)
						return false
					}
				}
				return true
			}

			function formatType (val) {
				return val
					? val.charAt(0).toUpperCase() + val.slice(1)
					: 'custom type'
			}

			function formatValue (val) {
				return Object.prototype.toString.call(val).slice(8, -1)
			}


			/***/ },
		/* 12 */
		/***/ function(module, exports, __webpack_require__) {

			/**
			 * Enable debug utilities.
			 */

			if (true) {

				var config = __webpack_require__(5)
				var hasConsole = typeof console !== 'undefined'

				/**
				 * Log a message.
				 *
				 * @param {String} msg
				 */

				exports.log = function (msg) {
					if (hasConsole && config.debug) {
						console.log('[Vue info]: ' + msg)
					}
				}

				/**
				 * We've got a problem here.
				 *
				 * @param {String} msg
				 */

				exports.warn = function (msg, e) {
					if (hasConsole && (!config.silent || config.debug)) {
						console.warn('[Vue warn]: ' + msg)
						/* istanbul ignore if */
						if (config.debug) {
							console.warn((e || new Error('Warning Stack Trace')).stack)
						}
					}
				}

				/**
				 * Assert asset exists
				 */

				exports.assertAsset = function (val, type, id) {
					if (!val) {
						exports.warn('Failed to resolve ' + type + ': ' + id)
					}
				}
			}


			/***/ },
		/* 13 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var config = __webpack_require__(5)

			/**
			 * Expose useful internals
			 */

			exports.util = _
			exports.config = config
			exports.set = _.set
			exports.delete = _.delete
			exports.nextTick = _.nextTick

			/**
			 * The following are exposed for advanced usage / plugins
			 */

			exports.compiler = __webpack_require__(14)
			exports.FragmentFactory = __webpack_require__(21)
			exports.internalDirectives = __webpack_require__(36)
			exports.parsers = {
				path: __webpack_require__(43),
				text: __webpack_require__(6),
				template: __webpack_require__(19),
				directive: __webpack_require__(8),
				expression: __webpack_require__(42)
			}

			/**
			 * Each instance constructor, including Vue, has a unique
			 * cid. This enables us to create wrapped "child
			 * constructors" for prototypal inheritance and cache them.
			 */

			exports.cid = 0
			var cid = 1

			/**
			 * Class inheritance
			 *
			 * @param {Object} extendOptions
			 */

			exports.extend = function (extendOptions) {
				extendOptions = extendOptions || {}
				var Super = this
				var isFirstExtend = Super.cid === 0
				if (isFirstExtend && extendOptions._Ctor) {
					return extendOptions._Ctor
				}
				var name = extendOptions.name || Super.options.name
				var Sub = createClass(name || 'VueComponent')
				Sub.prototype = Object.create(Super.prototype)
				Sub.prototype.constructor = Sub
				Sub.cid = cid++
				Sub.options = _.mergeOptions(
					Super.options,
					extendOptions
				)
				Sub['super'] = Super
				// allow further extension
				Sub.extend = Super.extend
				// create asset registers, so extended classes
				// can have their private assets too.
				config._assetTypes.forEach(function (type) {
					Sub[type] = Super[type]
				})
				// enable recursive self-lookup
				if (name) {
					Sub.options.components[name] = Sub
				}
				// cache constructor
				if (isFirstExtend) {
					extendOptions._Ctor = Sub
				}
				return Sub
			}

			/**
			 * A function that returns a sub-class constructor with the
			 * given name. This gives us much nicer output when
			 * logging instances in the console.
			 *
			 * @param {String} name
			 * @return {Function}
			 */

			function createClass (name) {
				return new Function(
					'return function ' + _.classify(name) +
					' (options) { this._init(options) }'
				)()
			}

			/**
			 * Plugin system
			 *
			 * @param {Object} plugin
			 */

			exports.use = function (plugin) {
				/* istanbul ignore if */
				if (plugin.installed) {
					return
				}
				// additional parameters
				var args = _.toArray(arguments, 1)
				args.unshift(this)
				if (typeof plugin.install === 'function') {
					plugin.install.apply(plugin, args)
				} else {
					plugin.apply(null, args)
				}
				plugin.installed = true
				return this
			}

			/**
			 * Apply a global mixin by merging it into the default
			 * options.
			 */

			exports.mixin = function (mixin) {
				var Vue = _.Vue
				Vue.options = _.mergeOptions(Vue.options, mixin)
			}

			/**
			 * Create asset registration methods with the following
			 * signature:
			 *
			 * @param {String} id
			 * @param {*} definition
			 */

			config._assetTypes.forEach(function (type) {
				exports[type] = function (id, definition) {
					if (!definition) {
						return this.options[type + 's'][id]
					} else {
						if (
							type === 'component' &&
							_.isPlainObject(definition)
						) {
							definition.name = id
							definition = _.Vue.extend(definition)
						}
						this.options[type + 's'][id] = definition
						return definition
					}
				}
			})


			/***/ },
		/* 14 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)

			_.extend(exports, __webpack_require__(15))
			_.extend(exports, __webpack_require__(49))


			/***/ },
		/* 15 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var publicDirectives = __webpack_require__(16)
			var internalDirectives = __webpack_require__(36)
			var compileProps = __webpack_require__(48)
			var textParser = __webpack_require__(6)
			var dirParser = __webpack_require__(8)
			var templateParser = __webpack_require__(19)
			var resolveAsset = _.resolveAsset

			// special binding prefixes
			var bindRE = /^v-bind:|^:/
			var onRE = /^v-on:|^@/
			var argRE = /:(.*)$/
			var modifierRE = /\.[^\.]+/g
			var transitionRE = /^(v-bind:|:)?transition$/

			// terminal directives
			var terminalDirectives = [
				'for',
				'if'
			]

			// default directive priority
			var DEFAULT_PRIORITY = 1000

			/**
			 * Compile a template and return a reusable composite link
			 * function, which recursively contains more link functions
			 * inside. This top level compile function would normally
			 * be called on instance root nodes, but can also be used
			 * for partial compilation if the partial argument is true.
			 *
			 * The returned composite link function, when called, will
			 * return an unlink function that tearsdown all directives
			 * created during the linking phase.
			 *
			 * @param {Element|DocumentFragment} el
			 * @param {Object} options
			 * @param {Boolean} partial
			 * @return {Function}
			 */

			exports.compile = function (el, options, partial) {
				// link function for the node itself.
				var nodeLinkFn = partial || !options._asComponent
					? compileNode(el, options)
					: null
				// link function for the childNodes
				var childLinkFn =
					!(nodeLinkFn && nodeLinkFn.terminal) &&
					el.tagName !== 'SCRIPT' &&
					el.hasChildNodes()
						? compileNodeList(el.childNodes, options)
						: null

				/**
				 * A composite linker function to be called on a already
				 * compiled piece of DOM, which instantiates all directive
				 * instances.
				 *
				 * @param {Vue} vm
				 * @param {Element|DocumentFragment} el
				 * @param {Vue} [host] - host vm of transcluded content
				 * @param {Object} [scope] - v-for scope
				 * @param {Fragment} [frag] - link context fragment
				 * @return {Function|undefined}
				 */

				return function compositeLinkFn (vm, el, host, scope, frag) {
					// cache childNodes before linking parent, fix #657
					var childNodes = _.toArray(el.childNodes)
					// link
					var dirs = linkAndCapture(function compositeLinkCapturer () {
						if (nodeLinkFn) nodeLinkFn(vm, el, host, scope, frag)
						if (childLinkFn) childLinkFn(vm, childNodes, host, scope, frag)
					}, vm)
					return makeUnlinkFn(vm, dirs)
				}
			}

			/**
			 * Apply a linker to a vm/element pair and capture the
			 * directives created during the process.
			 *
			 * @param {Function} linker
			 * @param {Vue} vm
			 */

			function linkAndCapture (linker, vm) {
				var originalDirCount = vm._directives.length
				linker()
				var dirs = vm._directives.slice(originalDirCount)
				dirs.sort(directiveComparator)
				for (var i = 0, l = dirs.length; i < l; i++) {
					dirs[i]._bind()
				}
				return dirs
			}

			/**
			 * Directive priority sort comparator
			 *
			 * @param {Object} a
			 * @param {Object} b
			 */

			function directiveComparator (a, b) {
				a = a.descriptor.def.priority || DEFAULT_PRIORITY
				b = b.descriptor.def.priority || DEFAULT_PRIORITY
				return a > b ? -1 : a === b ? 0 : 1
			}

			/**
			 * Linker functions return an unlink function that
			 * tearsdown all directives instances generated during
			 * the process.
			 *
			 * We create unlink functions with only the necessary
			 * information to avoid retaining additional closures.
			 *
			 * @param {Vue} vm
			 * @param {Array} dirs
			 * @param {Vue} [context]
			 * @param {Array} [contextDirs]
			 * @return {Function}
			 */

			function makeUnlinkFn (vm, dirs, context, contextDirs) {
				return function unlink (destroying) {
					teardownDirs(vm, dirs, destroying)
					if (context && contextDirs) {
						teardownDirs(context, contextDirs)
					}
				}
			}

			/**
			 * Teardown partial linked directives.
			 *
			 * @param {Vue} vm
			 * @param {Array} dirs
			 * @param {Boolean} destroying
			 */

			function teardownDirs (vm, dirs, destroying) {
				var i = dirs.length
				while (i--) {
					dirs[i]._teardown()
					if (!destroying) {
						vm._directives.$remove(dirs[i])
					}
				}
			}

			/**
			 * Compile link props on an instance.
			 *
			 * @param {Vue} vm
			 * @param {Element} el
			 * @param {Object} props
			 * @param {Object} [scope]
			 * @return {Function}
			 */

			exports.compileAndLinkProps = function (vm, el, props, scope) {
				var propsLinkFn = compileProps(el, props)
				var propDirs = linkAndCapture(function () {
					propsLinkFn(vm, scope)
				}, vm)
				return makeUnlinkFn(vm, propDirs)
			}

			/**
			 * Compile the root element of an instance.
			 *
			 * 1. attrs on context container (context scope)
			 * 2. attrs on the component template root node, if
			 *    replace:true (child scope)
			 *
			 * If this is a fragment instance, we only need to compile 1.
			 *
			 * @param {Vue} vm
			 * @param {Element} el
			 * @param {Object} options
			 * @param {Object} contextOptions
			 * @return {Function}
			 */

			exports.compileRoot = function (el, options, contextOptions) {
				var containerAttrs = options._containerAttrs
				var replacerAttrs = options._replacerAttrs
				var contextLinkFn, replacerLinkFn

				// only need to compile other attributes for
				// non-fragment instances
				if (el.nodeType !== 11) {
					// for components, container and replacer need to be
					// compiled separately and linked in different scopes.
					if (options._asComponent) {
						// 2. container attributes
						if (containerAttrs && contextOptions) {
							contextLinkFn = compileDirectives(containerAttrs, contextOptions)
						}
						if (replacerAttrs) {
							// 3. replacer attributes
							replacerLinkFn = compileDirectives(replacerAttrs, options)
						}
					} else {
						// non-component, just compile as a normal element.
						replacerLinkFn = compileDirectives(el.attributes, options)
					}
				} else if (("development") !== 'production' && containerAttrs) {
					// warn container directives for fragment instances
					var names = containerAttrs.map(function (attr) {
						return '"' + attr.name + '"'
					}).join(', ')
					var plural = containerAttrs.length > 1
					_.warn(
						'Attribute' + (plural ? 's ' : ' ') + names +
						(plural ? ' are' : ' is') + ' ignored on component ' +
						'<' + options.el.tagName.toLowerCase() + '> because ' +
						'the component is a fragment instance: ' +
						'http://vuejs.org/guide/components.html#Fragment_Instance'
					)
				}

				return function rootLinkFn (vm, el, scope) {
					// link context scope dirs
					var context = vm._context
					var contextDirs
					if (context && contextLinkFn) {
						contextDirs = linkAndCapture(function () {
							contextLinkFn(context, el, null, scope)
						}, context)
					}

					// link self
					var selfDirs = linkAndCapture(function () {
						if (replacerLinkFn) replacerLinkFn(vm, el)
					}, vm)

					// return the unlink function that tearsdown context
					// container directives.
					return makeUnlinkFn(vm, selfDirs, context, contextDirs)
				}
			}

			/**
			 * Compile a node and return a nodeLinkFn based on the
			 * node type.
			 *
			 * @param {Node} node
			 * @param {Object} options
			 * @return {Function|null}
			 */

			function compileNode (node, options) {
				var type = node.nodeType
				if (type === 1 && node.tagName !== 'SCRIPT') {
					return compileElement(node, options)
				} else if (type === 3 && node.data.trim()) {
					return compileTextNode(node, options)
				} else {
					return null
				}
			}

			/**
			 * Compile an element and return a nodeLinkFn.
			 *
			 * @param {Element} el
			 * @param {Object} options
			 * @return {Function|null}
			 */

			function compileElement (el, options) {
				// preprocess textareas.
				// textarea treats its text content as the initial value.
				// just bind it as an attr directive for value.
				if (el.tagName === 'TEXTAREA') {
					var tokens = textParser.parse(el.value)
					if (tokens) {
						el.setAttribute(':value', textParser.tokensToExp(tokens))
						el.value = ''
					}
				}
				var linkFn
				var hasAttrs = el.hasAttributes()
				// check terminal directives (for & if)
				if (hasAttrs) {
					linkFn = checkTerminalDirectives(el, options)
				}
				// check element directives
				if (!linkFn) {
					linkFn = checkElementDirectives(el, options)
				}
				// check component
				if (!linkFn) {
					linkFn = checkComponent(el, options)
				}
				// normal directives
				if (!linkFn && hasAttrs) {
					linkFn = compileDirectives(el.attributes, options)
				}
				return linkFn
			}

			/**
			 * Compile a textNode and return a nodeLinkFn.
			 *
			 * @param {TextNode} node
			 * @param {Object} options
			 * @return {Function|null} textNodeLinkFn
			 */

			function compileTextNode (node, options) {
				var tokens = textParser.parse(node.data)
				if (!tokens) {
					return null
				}
				var frag = document.createDocumentFragment()
				var el, token
				for (var i = 0, l = tokens.length; i < l; i++) {
					token = tokens[i]
					el = token.tag
						? processTextToken(token, options)
						: document.createTextNode(token.value)
					frag.appendChild(el)
				}
				return makeTextNodeLinkFn(tokens, frag, options)
			}

			/**
			 * Process a single text token.
			 *
			 * @param {Object} token
			 * @param {Object} options
			 * @return {Node}
			 */

			function processTextToken (token, options) {
				var el
				if (token.oneTime) {
					el = document.createTextNode(token.value)
				} else {
					if (token.html) {
						el = document.createComment('v-html')
						setTokenType('html')
					} else {
						// IE will clean up empty textNodes during
						// frag.cloneNode(true), so we have to give it
						// something here...
						el = document.createTextNode(' ')
						setTokenType('text')
					}
				}
				function setTokenType (type) {
					if (token.descriptor) return
					var parsed = dirParser.parse(token.value)
					token.descriptor = {
						name: type,
						def: publicDirectives[type],
						expression: parsed.expression,
						filters: parsed.filters
					}
				}
				return el
			}

			/**
			 * Build a function that processes a textNode.
			 *
			 * @param {Array<Object>} tokens
			 * @param {DocumentFragment} frag
			 */

			function makeTextNodeLinkFn (tokens, frag) {
				return function textNodeLinkFn (vm, el, host, scope) {
					var fragClone = frag.cloneNode(true)
					var childNodes = _.toArray(fragClone.childNodes)
					var token, value, node
					for (var i = 0, l = tokens.length; i < l; i++) {
						token = tokens[i]
						value = token.value
						if (token.tag) {
							node = childNodes[i]
							if (token.oneTime) {
								value = (scope || vm).$eval(value)
								if (token.html) {
									_.replace(node, templateParser.parse(value, true))
								} else {
									node.data = value
								}
							} else {
								vm._bindDir(token.descriptor, node, host, scope)
							}
						}
					}
					_.replace(el, fragClone)
				}
			}

			/**
			 * Compile a node list and return a childLinkFn.
			 *
			 * @param {NodeList} nodeList
			 * @param {Object} options
			 * @return {Function|undefined}
			 */

			function compileNodeList (nodeList, options) {
				var linkFns = []
				var nodeLinkFn, childLinkFn, node
				for (var i = 0, l = nodeList.length; i < l; i++) {
					node = nodeList[i]
					nodeLinkFn = compileNode(node, options)
					childLinkFn =
						!(nodeLinkFn && nodeLinkFn.terminal) &&
						node.tagName !== 'SCRIPT' &&
						node.hasChildNodes()
							? compileNodeList(node.childNodes, options)
							: null
					linkFns.push(nodeLinkFn, childLinkFn)
				}
				return linkFns.length
					? makeChildLinkFn(linkFns)
					: null
			}

			/**
			 * Make a child link function for a node's childNodes.
			 *
			 * @param {Array<Function>} linkFns
			 * @return {Function} childLinkFn
			 */

			function makeChildLinkFn (linkFns) {
				return function childLinkFn (vm, nodes, host, scope, frag) {
					var node, nodeLinkFn, childrenLinkFn
					for (var i = 0, n = 0, l = linkFns.length; i < l; n++) {
						node = nodes[n]
						nodeLinkFn = linkFns[i++]
						childrenLinkFn = linkFns[i++]
						// cache childNodes before linking parent, fix #657
						var childNodes = _.toArray(node.childNodes)
						if (nodeLinkFn) {
							nodeLinkFn(vm, node, host, scope, frag)
						}
						if (childrenLinkFn) {
							childrenLinkFn(vm, childNodes, host, scope, frag)
						}
					}
				}
			}

			/**
			 * Check for element directives (custom elements that should
			 * be resovled as terminal directives).
			 *
			 * @param {Element} el
			 * @param {Object} options
			 */

			function checkElementDirectives (el, options) {
				var tag = el.tagName.toLowerCase()
				if (_.commonTagRE.test(tag)) return
				var def = resolveAsset(options, 'elementDirectives', tag)
				if (def) {
					return makeTerminalNodeLinkFn(el, tag, '', options, def)
				}
			}

			/**
			 * Check if an element is a component. If yes, return
			 * a component link function.
			 *
			 * @param {Element} el
			 * @param {Object} options
			 * @return {Function|undefined}
			 */

			function checkComponent (el, options) {
				var component = _.checkComponent(el, options)
				if (component) {
					var descriptor = {
						name: 'component',
						expression: component.id,
						def: internalDirectives.component,
						modifiers: {
							literal: !component.dynamic
						}
					}
					var componentLinkFn = function (vm, el, host, scope, frag) {
						vm._bindDir(descriptor, el, host, scope, frag)
					}
					componentLinkFn.terminal = true
					return componentLinkFn
				}
			}

			/**
			 * Check an element for terminal directives in fixed order.
			 * If it finds one, return a terminal link function.
			 *
			 * @param {Element} el
			 * @param {Object} options
			 * @return {Function} terminalLinkFn
			 */

			function checkTerminalDirectives (el, options) {
				// skip v-pre
				if (_.attr(el, 'v-pre') !== null) {
					return skip
				}
				// skip v-else block, but only if following v-if
				if (el.hasAttribute('v-else')) {
					var prev = el.previousElementSibling
					if (prev && prev.hasAttribute('v-if')) {
						return skip
					}
				}
				var value, dirName
				for (var i = 0, l = terminalDirectives.length; i < l; i++) {
					dirName = terminalDirectives[i]
					/* eslint-disable no-cond-assign */
					if (value = el.getAttribute('v-' + dirName)) {
						return makeTerminalNodeLinkFn(el, dirName, value, options)
					}
					/* eslint-enable no-cond-assign */
				}
			}

			function skip () {}
			skip.terminal = true

			/**
			 * Build a node link function for a terminal directive.
			 * A terminal link function terminates the current
			 * compilation recursion and handles compilation of the
			 * subtree in the directive.
			 *
			 * @param {Element} el
			 * @param {String} dirName
			 * @param {String} value
			 * @param {Object} options
			 * @param {Object} [def]
			 * @return {Function} terminalLinkFn
			 */

			function makeTerminalNodeLinkFn (el, dirName, value, options, def) {
				var parsed = dirParser.parse(value)
				var descriptor = {
					name: dirName,
					expression: parsed.expression,
					filters: parsed.filters,
					raw: value,
					// either an element directive, or if/for
					def: def || publicDirectives[dirName]
				}
				var fn = function terminalNodeLinkFn (vm, el, host, scope, frag) {
					vm._bindDir(descriptor, el, host, scope, frag)
				}
				fn.terminal = true
				return fn
			}

			/**
			 * Compile the directives on an element and return a linker.
			 *
			 * @param {Array|NamedNodeMap} attrs
			 * @param {Object} options
			 * @return {Function}
			 */

			function compileDirectives (attrs, options) {
				var i = attrs.length
				var dirs = []
				var attr, name, value, rawName, rawValue, dirName, arg, modifiers, dirDef, tokens
				while (i--) {
					attr = attrs[i]
					name = rawName = attr.name
					value = rawValue = attr.value
					tokens = textParser.parse(value)
					// reset arg
					arg = null
					// check modifiers
					modifiers = parseModifiers(name)
					name = name.replace(modifierRE, '')

					// attribute interpolations
					if (tokens) {
						value = textParser.tokensToExp(tokens)
						arg = name
						pushDir('bind', publicDirectives.bind, true)
					} else

					// special attribute: transition
					if (transitionRE.test(name)) {
						modifiers.literal = !bindRE.test(name)
						pushDir('transition', internalDirectives.transition)
					} else

					// event handlers
					if (onRE.test(name)) {
						arg = name.replace(onRE, '')
						pushDir('on', publicDirectives.on)
					} else

					// attribute bindings
					if (bindRE.test(name)) {
						dirName = name.replace(bindRE, '')
						if (dirName === 'style' || dirName === 'class') {
							pushDir(dirName, internalDirectives[dirName])
						} else {
							arg = dirName
							pushDir('bind', publicDirectives.bind)
						}
					} else

					// normal directives
					if (name.indexOf('v-') === 0) {
						// check arg
						arg = (arg = name.match(argRE)) && arg[1]
						if (arg) {
							name = name.replace(argRE, '')
						}
						// extract directive name
						dirName = name.slice(2)

						// skip v-else (when used with v-show)
						if (dirName === 'else') {
							continue
						}

						dirDef = resolveAsset(options, 'directives', dirName)

						if (true) {
							_.assertAsset(dirDef, 'directive', dirName)
						}

						if (dirDef) {
							if (_.isLiteral(value)) {
								value = _.stripQuotes(value)
								modifiers.literal = true
							}
							pushDir(dirName, dirDef)
						}
					}
				}

				/**
				 * Push a directive.
				 *
				 * @param {String} dirName
				 * @param {Object|Function} def
				 * @param {Boolean} [interp]
				 */

				function pushDir (dirName, def, interp) {
					var parsed = dirParser.parse(value)
					dirs.push({
						name: dirName,
						attr: rawName,
						raw: rawValue,
						def: def,
						arg: arg,
						modifiers: modifiers,
						expression: parsed.expression,
						filters: parsed.filters,
						interp: interp
					})
				}

				if (dirs.length) {
					return makeNodeLinkFn(dirs)
				}
			}

			/**
			 * Parse modifiers from directive attribute name.
			 *
			 * @param {String} name
			 * @return {Object}
			 */

			function parseModifiers (name) {
				var res = Object.create(null)
				var match = name.match(modifierRE)
				if (match) {
					var i = match.length
					while (i--) {
						res[match[i].slice(1)] = true
					}
				}
				return res
			}

			/**
			 * Build a link function for all directives on a single node.
			 *
			 * @param {Array} directives
			 * @return {Function} directivesLinkFn
			 */

			function makeNodeLinkFn (directives) {
				return function nodeLinkFn (vm, el, host, scope, frag) {
					// reverse apply because it's sorted low to high
					var i = directives.length
					while (i--) {
						vm._bindDir(directives[i], el, host, scope, frag)
					}
				}
			}


			/***/ },
		/* 16 */
		/***/ function(module, exports, __webpack_require__) {

			// text & html
			exports.text = __webpack_require__(17)
			exports.html = __webpack_require__(18)

			// logic control
			exports['for'] = __webpack_require__(20)
			exports['if'] = __webpack_require__(23)
			exports.show = __webpack_require__(24)

			// two-way binding
			exports.model = __webpack_require__(25)

			// event handling
			exports.on = __webpack_require__(30)

			// attributes
			exports.bind = __webpack_require__(31)

			// ref & el
			exports.el = __webpack_require__(33)
			exports.ref = __webpack_require__(34)

			// cloak
			exports.cloak = __webpack_require__(35)


			/***/ },
		/* 17 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)

			module.exports = {

				bind: function () {
					this.attr = this.el.nodeType === 3
						? 'data'
						: 'textContent'
				},

				update: function (value) {
					this.el[this.attr] = _.toString(value)
				}
			}


			/***/ },
		/* 18 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var templateParser = __webpack_require__(19)

			module.exports = {

				bind: function () {
					// a comment node means this is a binding for
					// {{{ inline unescaped html }}}
					if (this.el.nodeType === 8) {
						// hold nodes
						this.nodes = []
						// replace the placeholder with proper anchor
						this.anchor = _.createAnchor('v-html')
						_.replace(this.el, this.anchor)
					}
				},

				update: function (value) {
					value = _.toString(value)
					if (this.nodes) {
						this.swap(value)
					} else {
						this.el.innerHTML = value
					}
				},

				swap: function (value) {
					// remove old nodes
					var i = this.nodes.length
					while (i--) {
						_.remove(this.nodes[i])
					}
					// convert new value to a fragment
					// do not attempt to retrieve from id selector
					var frag = templateParser.parse(value, true, true)
					// save a reference to these nodes so we can remove later
					this.nodes = _.toArray(frag.childNodes)
					_.before(frag, this.anchor)
				}
			}


			/***/ },
		/* 19 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var Cache = __webpack_require__(7)
			var templateCache = new Cache(1000)
			var idSelectorCache = new Cache(1000)

			var map = {
				_default: [0, '', ''],
				legend: [1, '<fieldset>', '</fieldset>'],
				tr: [2, '<table><tbody>', '</tbody></table>'],
				col: [
					2,
					'<table><tbody></tbody><colgroup>',
					'</colgroup></table>'
				]
			}

			map.td =
				map.th = [
					3,
					'<table><tbody><tr>',
					'</tr></tbody></table>'
				]

			map.option =
				map.optgroup = [
					1,
					'<select multiple="multiple">',
					'</select>'
				]

			map.thead =
				map.tbody =
					map.colgroup =
						map.caption =
							map.tfoot = [1, '<table>', '</table>']

			map.g =
				map.defs =
					map.symbol =
						map.use =
							map.image =
								map.text =
									map.circle =
										map.ellipse =
											map.line =
												map.path =
													map.polygon =
														map.polyline =
															map.rect = [
																1,
																'<svg ' +
																'xmlns="http://www.w3.org/2000/svg" ' +
																'xmlns:xlink="http://www.w3.org/1999/xlink" ' +
																'xmlns:ev="http://www.w3.org/2001/xml-events"' +
																'version="1.1">',
																'</svg>'
															]

			/**
			 * Check if a node is a supported template node with a
			 * DocumentFragment content.
			 *
			 * @param {Node} node
			 * @return {Boolean}
			 */

			function isRealTemplate (node) {
				return _.isTemplate(node) &&
					node.content instanceof DocumentFragment
			}

			var tagRE = /<([\w:]+)/
			var entityRE = /&\w+;|&#\d+;|&#x[\dA-F]+;/

			/**
			 * Convert a string template to a DocumentFragment.
			 * Determines correct wrapping by tag types. Wrapping
			 * strategy found in jQuery & component/domify.
			 *
			 * @param {String} templateString
			 * @return {DocumentFragment}
			 */

			function stringToFragment (templateString) {
				// try a cache hit first
				var hit = templateCache.get(templateString)
				if (hit) {
					return hit
				}

				var frag = document.createDocumentFragment()
				var tagMatch = templateString.match(tagRE)
				var entityMatch = entityRE.test(templateString)

				if (!tagMatch && !entityMatch) {
					// text only, return a single text node.
					frag.appendChild(
						document.createTextNode(templateString)
					)
				} else {

					var tag = tagMatch && tagMatch[1]
					var wrap = map[tag] || map._default
					var depth = wrap[0]
					var prefix = wrap[1]
					var suffix = wrap[2]
					var node = document.createElement('div')

					node.innerHTML = prefix + templateString.trim() + suffix
					while (depth--) {
						node = node.lastChild
					}

					var child
					/* eslint-disable no-cond-assign */
					while (child = node.firstChild) {
						/* eslint-enable no-cond-assign */
						frag.appendChild(child)
					}
				}

				templateCache.put(templateString, frag)
				return frag
			}

			/**
			 * Convert a template node to a DocumentFragment.
			 *
			 * @param {Node} node
			 * @return {DocumentFragment}
			 */

			function nodeToFragment (node) {
				// if its a template tag and the browser supports it,
				// its content is already a document fragment.
				if (isRealTemplate(node)) {
					_.trimNode(node.content)
					return node.content
				}
				// script template
				if (node.tagName === 'SCRIPT') {
					return stringToFragment(node.textContent)
				}
				// normal node, clone it to avoid mutating the original
				var clone = exports.clone(node)
				var frag = document.createDocumentFragment()
				var child
				/* eslint-disable no-cond-assign */
				while (child = clone.firstChild) {
					/* eslint-enable no-cond-assign */
					frag.appendChild(child)
				}
				_.trimNode(frag)
				return frag
			}

			// Test for the presence of the Safari template cloning bug
			// https://bugs.webkit.org/show_bug.cgi?id=137755
			var hasBrokenTemplate = (function () {
				/* istanbul ignore else */
				if (_.inBrowser) {
					var a = document.createElement('div')
					a.innerHTML = '<template>1</template>'
					return !a.cloneNode(true).firstChild.innerHTML
				} else {
					return false
				}
			})()

			// Test for IE10/11 textarea placeholder clone bug
			var hasTextareaCloneBug = (function () {
				/* istanbul ignore else */
				if (_.inBrowser) {
					var t = document.createElement('textarea')
					t.placeholder = 't'
					return t.cloneNode(true).value === 't'
				} else {
					return false
				}
			})()

			/**
			 * 1. Deal with Safari cloning nested <template> bug by
			 *    manually cloning all template instances.
			 * 2. Deal with IE10/11 textarea placeholder bug by setting
			 *    the correct value after cloning.
			 *
			 * @param {Element|DocumentFragment} node
			 * @return {Element|DocumentFragment}
			 */

			exports.clone = function (node) {
				if (!node.querySelectorAll) {
					return node.cloneNode()
				}
				var res = node.cloneNode(true)
				var i, original, cloned
				/* istanbul ignore if */
				if (hasBrokenTemplate) {
					var clone = res
					if (isRealTemplate(node)) {
						node = node.content
						clone = res.content
					}
					original = node.querySelectorAll('template')
					if (original.length) {
						cloned = clone.querySelectorAll('template')
						i = cloned.length
						while (i--) {
							cloned[i].parentNode.replaceChild(
								exports.clone(original[i]),
								cloned[i]
							)
						}
					}
				}
				/* istanbul ignore if */
				if (hasTextareaCloneBug) {
					if (node.tagName === 'TEXTAREA') {
						res.value = node.value
					} else {
						original = node.querySelectorAll('textarea')
						if (original.length) {
							cloned = res.querySelectorAll('textarea')
							i = cloned.length
							while (i--) {
								cloned[i].value = original[i].value
							}
						}
					}
				}
				return res
			}

			/**
			 * Process the template option and normalizes it into a
			 * a DocumentFragment that can be used as a partial or a
			 * instance template.
			 *
			 * @param {*} template
			 *    Possible values include:
			 *    - DocumentFragment object
			 *    - Node object of type Template
			 *    - id selector: '#some-template-id'
			 *    - template string: '<div><span>{{msg}}</span></div>'
			 * @param {Boolean} clone
			 * @param {Boolean} noSelector
			 * @return {DocumentFragment|undefined}
			 */

			exports.parse = function (template, clone, noSelector) {
				var node, frag

				// if the template is already a document fragment,
				// do nothing
				if (template instanceof DocumentFragment) {
					_.trimNode(template)
					return clone
						? exports.clone(template)
						: template
				}

				if (typeof template === 'string') {
					// id selector
					if (!noSelector && template.charAt(0) === '#') {
						// id selector can be cached too
						frag = idSelectorCache.get(template)
						if (!frag) {
							node = document.getElementById(template.slice(1))
							if (node) {
								frag = nodeToFragment(node)
								// save selector to cache
								idSelectorCache.put(template, frag)
							}
						}
					} else {
						// normal string template
						frag = stringToFragment(template)
					}
				} else if (template.nodeType) {
					// a direct node
					frag = nodeToFragment(template)
				}

				return frag && clone
					? exports.clone(frag)
					: frag
			}


			/***/ },
		/* 20 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var FragmentFactory = __webpack_require__(21)
			var isObject = _.isObject
			var uid = 0

			module.exports = {

				priority: 2000,

				params: [
					'track-by',
					'stagger',
					'enter-stagger',
					'leave-stagger'
				],

				bind: function () {
					// support "item in items" syntax
					var inMatch = this.expression.match(/(.*) in (.*)/)
					if (inMatch) {
						var itMatch = inMatch[1].match(/\((.*),(.*)\)/)
						if (itMatch) {
							this.iterator = itMatch[1].trim()
							this.alias = itMatch[2].trim()
						} else {
							this.alias = inMatch[1].trim()
						}
						this.expression = inMatch[2]
					}

					if (!this.alias) {
						("development") !== 'production' && _.warn(
							'Alias is required in v-for.'
						)
						return
					}

					// uid as a cache identifier
					this.id = '__v-for__' + (++uid)

					// check if this is an option list,
					// so that we know if we need to update the <select>'s
					// v-model when the option list has changed.
					// because v-model has a lower priority than v-for,
					// the v-model is not bound here yet, so we have to
					// retrive it in the actual updateModel() function.
					var tag = this.el.tagName
					this.isOption =
						(tag === 'OPTION' || tag === 'OPTGROUP') &&
						this.el.parentNode.tagName === 'SELECT'

					// setup anchor nodes
					this.start = _.createAnchor('v-for-start')
					this.end = _.createAnchor('v-for-end')
					_.replace(this.el, this.end)
					_.before(this.start, this.end)

					// check ref
					this.ref = _.findRef(this.el)

					// cache
					this.cache = Object.create(null)

					// fragment factory
					this.factory = new FragmentFactory(this.vm, this.el)
				},

				update: function (data) {
					this.diff(data)
					this.updateRef()
					this.updateModel()
				},

				/**
				 * Diff, based on new data and old data, determine the
				 * minimum amount of DOM manipulations needed to make the
				 * DOM reflect the new data Array.
				 *
				 * The algorithm diffs the new data Array by storing a
				 * hidden reference to an owner vm instance on previously
				 * seen data. This allows us to achieve O(n) which is
				 * better than a levenshtein distance based algorithm,
				 * which is O(m * n).
				 *
				 * @param {Array} data
				 */

				diff: function (data) {
					// check if the Array was converted from an Object
					var item = data[0]
					var convertedFromObject = this.fromObject =
						isObject(item) &&
						item.hasOwnProperty('$key') &&
						item.hasOwnProperty('$value')

					var trackByKey = this.params.trackBy
					var oldFrags = this.frags
					var frags = this.frags = new Array(data.length)
					var alias = this.alias
					var iterator = this.iterator
					var start = this.start
					var end = this.end
					var inDoc = _.inDoc(start)
					var init = !oldFrags
					var i, l, frag, key, value, primitive

					// First pass, go through the new Array and fill up
					// the new frags array. If a piece of data has a cached
					// instance for it, we reuse it. Otherwise build a new
					// instance.
					for (i = 0, l = data.length; i < l; i++) {
						item = data[i]
						key = convertedFromObject ? item.$key : null
						value = convertedFromObject ? item.$value : item
						primitive = !isObject(value)
						frag = !init && this.getCachedFrag(value, i, key)
						if (frag) { // reusable fragment
							frag.reused = true
							// update $index
							frag.scope.$index = i
							// update $key
							if (key) {
								frag.scope.$key = key
							}
							// update iterator
							if (iterator) {
								frag.scope[iterator] = key !== null ? key : i
							}
							// update data for track-by, object repeat &
							// primitive values.
							if (trackByKey || convertedFromObject || primitive) {
								frag.scope[alias] = value
							}
						} else { // new isntance
							frag = this.create(value, alias, i, key)
							frag.fresh = !init
						}
						frags[i] = frag
						if (init) {
							frag.before(end)
						}
					}

					// we're done for the initial render.
					if (init) {
						return
					}

					// Second pass, go through the old fragments and
					// destroy those who are not reused (and remove them
					// from cache)
					var removalIndex = 0
					var totalRemoved = oldFrags.length - frags.length
					for (i = 0, l = oldFrags.length; i < l; i++) {
						frag = oldFrags[i]
						if (!frag.reused) {
							this.deleteCachedFrag(frag)
							this.remove(frag, removalIndex++, totalRemoved, inDoc)
						}
					}

					// Final pass, move/insert new fragments into the
					// right place.
					var targetPrev, prevEl, currentPrev
					var insertionIndex = 0
					for (i = 0, l = frags.length; i < l; i++) {
						frag = frags[i]
						// this is the frag that we should be after
						targetPrev = frags[i - 1]
						prevEl = targetPrev
							? targetPrev.staggerCb
							? targetPrev.staggerAnchor
							: targetPrev.end || targetPrev.node
							: start
						if (frag.reused && !frag.staggerCb) {
							currentPrev = findPrevFrag(frag, start, this.id)
							if (currentPrev !== targetPrev) {
								this.move(frag, prevEl)
							}
						} else {
							// new instance, or still in stagger.
							// insert with updated stagger index.
							this.insert(frag, insertionIndex++, prevEl, inDoc)
						}
						frag.reused = frag.fresh = false
					}
				},

				/**
				 * Create a new fragment instance.
				 *
				 * @param {*} value
				 * @param {String} alias
				 * @param {Number} index
				 * @param {String} [key]
				 * @return {Fragment}
				 */

				create: function (value, alias, index, key) {
					var host = this._host
					// create iteration scope
					var parentScope = this._scope || this.vm
					var scope = Object.create(parentScope)
					// ref holder for the scope
					scope.$refs = Object.create(parentScope.$refs)
					scope.$els = Object.create(parentScope.$els)
					// make sure point $parent to parent scope
					scope.$parent = parentScope
					// for two-way binding on alias
					scope.$forContext = this
					// define scope properties
					_.defineReactive(scope, alias, value)
					_.defineReactive(scope, '$index', index)
					if (key) {
						_.defineReactive(scope, '$key', key)
					} else if (scope.$key) {
						// avoid accidental fallback
						_.define(scope, '$key', null)
					}
					if (this.iterator) {
						_.defineReactive(scope, this.iterator, key !== null ? key : index)
					}
					var frag = this.factory.create(host, scope, this._frag)
					frag.forId = this.id
					this.cacheFrag(value, frag, index, key)
					return frag
				},

				/**
				 * Update the v-ref on owner vm.
				 */

				updateRef: function () {
					var ref = this.ref
					if (!ref) return
					var hash = (this._scope || this.vm).$refs
					var refs
					if (!this.fromObject) {
						refs = this.frags.map(findVmFromFrag)
					} else {
						refs = {}
						this.frags.forEach(function (frag) {
							refs[frag.scope.$key] = findVmFromFrag(frag)
						})
					}
					if (!hash.hasOwnProperty(ref)) {
						_.defineReactive(hash, ref, refs)
					} else {
						hash[ref] = refs
					}
				},

				/**
				 * For option lists, update the containing v-model on
				 * parent <select>.
				 */

				updateModel: function () {
					if (this.isOption) {
						var parent = this.start.parentNode
						var model = parent && parent.__v_model
						if (model) {
							model.forceUpdate()
						}
					}
				},

				/**
				 * Insert a fragment. Handles staggering.
				 *
				 * @param {Fragment} frag
				 * @param {Number} index
				 * @param {Node} prevEl
				 * @param {Boolean} inDoc
				 */

				insert: function (frag, index, prevEl, inDoc) {
					if (frag.staggerCb) {
						frag.staggerCb.cancel()
						frag.staggerCb = null
					}
					var staggerAmount = this.getStagger(frag, index, null, 'enter')
					if (inDoc && staggerAmount) {
						// create an anchor and insert it synchronously,
						// so that we can resolve the correct order without
						// worrying about some elements not inserted yet
						var anchor = frag.staggerAnchor
						if (!anchor) {
							anchor = frag.staggerAnchor = _.createAnchor('stagger-anchor')
							anchor.__vfrag__ = frag
						}
						_.after(anchor, prevEl)
						var op = frag.staggerCb = _.cancellable(function () {
							frag.staggerCb = null
							frag.before(anchor)
							_.remove(anchor)
						})
						setTimeout(op, staggerAmount)
					} else {
						frag.before(prevEl.nextSibling)
					}
				},

				/**
				 * Remove a fragment. Handles staggering.
				 *
				 * @param {Fragment} frag
				 * @param {Number} index
				 * @param {Number} total
				 * @param {Boolean} inDoc
				 */

				remove: function (frag, index, total, inDoc) {
					if (frag.staggerCb) {
						frag.staggerCb.cancel()
						frag.staggerCb = null
						// it's not possible for the same frag to be removed
						// twice, so if we have a pending stagger callback,
						// it means this frag is queued for enter but removed
						// before its transition started. Since it is already
						// destroyed, we can just leave it in detached state.
						return
					}
					var staggerAmount = this.getStagger(frag, index, total, 'leave')
					if (inDoc && staggerAmount) {
						var op = frag.staggerCb = _.cancellable(function () {
							frag.staggerCb = null
							frag.remove(true)
						})
						setTimeout(op, staggerAmount)
					} else {
						frag.remove(true)
					}
				},

				/**
				 * Move a fragment to a new position.
				 * Force no transition.
				 *
				 * @param {Fragment} frag
				 * @param {Node} prevEl
				 */

				move: function (frag, prevEl) {
					frag.before(prevEl.nextSibling, false)
				},

				/**
				 * Cache a fragment using track-by or the object key.
				 *
				 * @param {*} value
				 * @param {Fragment} frag
				 * @param {Number} index
				 * @param {String} [key]
				 */

				cacheFrag: function (value, frag, index, key) {
					var trackByKey = this.params.trackBy
					var cache = this.cache
					var primitive = !isObject(value)
					var id
					if (key || trackByKey || primitive) {
						id = trackByKey
							? trackByKey === '$index'
							? index
							: value[trackByKey]
							: (key || value)
						if (!cache[id]) {
							cache[id] = frag
						} else if (trackByKey !== '$index') {
							("development") !== 'production' &&
							this.warnDuplicate(value)
						}
					} else {
						id = this.id
						if (value.hasOwnProperty(id)) {
							if (value[id] === null) {
								value[id] = frag
							} else {
								("development") !== 'production' &&
								this.warnDuplicate(value)
							}
						} else {
							_.define(value, id, frag)
						}
					}
					frag.raw = value
				},

				/**
				 * Get a cached fragment from the value/index/key
				 *
				 * @param {*} value
				 * @param {Number} index
				 * @param {String} key
				 * @return {Fragment}
				 */

				getCachedFrag: function (value, index, key) {
					var trackByKey = this.params.trackBy
					var primitive = !isObject(value)
					var frag
					if (key || trackByKey || primitive) {
						var id = trackByKey
							? trackByKey === '$index'
							? index
							: value[trackByKey]
							: (key || value)
						frag = this.cache[id]
					} else {
						frag = value[this.id]
					}
					if (frag && (frag.reused || frag.fresh)) {
						("development") !== 'production' &&
						this.warnDuplicate(value)
					}
					return frag
				},

				/**
				 * Delete a fragment from cache.
				 *
				 * @param {Fragment} frag
				 */

				deleteCachedFrag: function (frag) {
					var value = frag.raw
					var trackByKey = this.params.trackBy
					var scope = frag.scope
					var index = scope.$index
					// fix #948: avoid accidentally fall through to
					// a parent repeater which happens to have $key.
					var key = scope.hasOwnProperty('$key') && scope.$key
					var primitive = !isObject(value)
					if (trackByKey || key || primitive) {
						var id = trackByKey
							? trackByKey === '$index'
							? index
							: value[trackByKey]
							: (key || value)
						this.cache[id] = null
					} else {
						value[this.id] = null
						frag.raw = null
					}
				},

				/**
				 * Get the stagger amount for an insertion/removal.
				 *
				 * @param {Fragment} frag
				 * @param {Number} index
				 * @param {Number} total
				 * @param {String} type
				 */

				getStagger: function (frag, index, total, type) {
					type = type + 'Stagger'
					var trans = frag.node.__v_trans
					var hooks = trans && trans.hooks
					var hook = hooks && (hooks[type] || hooks.stagger)
					return hook
						? hook.call(frag, index, total)
						: index * parseInt(this.params[type] || this.params.stagger, 10)
				},

				/**
				 * Pre-process the value before piping it through the
				 * filters. This is passed to and called by the watcher.
				 */

				_preProcess: function (value) {
					// regardless of type, store the un-filtered raw value.
					this.rawValue = value
					return value
				},

				/**
				 * Post-process the value after it has been piped through
				 * the filters. This is passed to and called by the watcher.
				 *
				 * It is necessary for this to be called during the
				 * wathcer's dependency collection phase because we want
				 * the v-for to update when the source Object is mutated.
				 */

				_postProcess: function (value) {
					if (_.isArray(value)) {
						return value
					} else if (_.isPlainObject(value)) {
						// convert plain object to array.
						var keys = Object.keys(value)
						var i = keys.length
						var res = new Array(i)
						var key
						while (i--) {
							key = keys[i]
							res[i] = {
								$key: key,
								$value: value[key]
							}
						}
						return res
					} else {
						var type = typeof value
						if (type === 'number') {
							value = range(value)
						} else if (type === 'string') {
							value = _.toArray(value)
						}
						return value || []
					}
				},

				unbind: function () {
					if (this.ref) {
						(this._scope || this.vm).$refs[this.ref] = null
					}
					if (this.frags) {
						var i = this.frags.length
						var frag
						while (i--) {
							frag = this.frags[i]
							this.deleteCachedFrag(frag)
							frag.destroy()
						}
					}
				}
			}

			/**
			 * Helper to find the previous element that is a fragment
			 * anchor. This is necessary because a destroyed frag's
			 * element could still be lingering in the DOM before its
			 * leaving transition finishes, but its inserted flag
			 * should have been set to false so we can skip them.
			 *
			 * If this is a block repeat, we want to make sure we only
			 * return frag that is bound to this v-for. (see #929)
			 *
			 * @param {Fragment} frag
			 * @param {Comment|Text} anchor
			 * @param {String} id
			 * @return {Fragment}
			 */

			function findPrevFrag (frag, anchor, id) {
				var el = frag.node.previousSibling
				/* istanbul ignore if */
				if (!el) return
				frag = el.__vfrag__
				while (
				(!frag || frag.forId !== id || !frag.inserted) &&
				el !== anchor
					) {
					el = el.previousSibling
					/* istanbul ignore if */
					if (!el) return
					frag = el.__vfrag__
				}
				return frag
			}

			/**
			 * Find a vm from a fragment.
			 *
			 * @param {Fragment} frag
			 * @return {Vue|undefined}
			 */

			function findVmFromFrag (frag) {
				return frag.node.__vue__ || frag.node.nextSibling.__vue__
			}

			/**
			 * Create a range array from given number.
			 *
			 * @param {Number} n
			 * @return {Array}
			 */

			function range (n) {
				var i = -1
				var ret = new Array(n)
				while (++i < n) {
					ret[i] = i
				}
				return ret
			}

			if (true) {
				module.exports.warnDuplicate = function (value) {
					_.warn(
						'Duplicate value found in v-for="' + this.descriptor.raw + '": ' +
						JSON.stringify(value) + '. Use track-by="$index" if ' +
						'you are expecting duplicate values.'
					)
				}
			}


			/***/ },
		/* 21 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var compiler = __webpack_require__(14)
			var templateParser = __webpack_require__(19)
			var Fragment = __webpack_require__(22)
			var Cache = __webpack_require__(7)
			var linkerCache = new Cache(5000)

			/**
			 * A factory that can be used to create instances of a
			 * fragment. Caches the compiled linker if possible.
			 *
			 * @param {Vue} vm
			 * @param {Element|String} el
			 */

			function FragmentFactory (vm, el) {
				this.vm = vm
				var template
				var isString = typeof el === 'string'
				if (isString || _.isTemplate(el)) {
					template = templateParser.parse(el, true)
				} else {
					template = document.createDocumentFragment()
					template.appendChild(el)
				}
				this.template = template
				// linker can be cached, but only for components
				var linker
				var cid = vm.constructor.cid
				if (cid > 0) {
					var cacheId = cid + (isString ? el : el.outerHTML)
					linker = linkerCache.get(cacheId)
					if (!linker) {
						linker = compiler.compile(template, vm.$options, true)
						linkerCache.put(cacheId, linker)
					}
				} else {
					linker = compiler.compile(template, vm.$options, true)
				}
				this.linker = linker
			}

			/**
			 * Create a fragment instance with given host and scope.
			 *
			 * @param {Vue} host
			 * @param {Object} scope
			 * @param {Fragment} parentFrag
			 */

			FragmentFactory.prototype.create = function (host, scope, parentFrag) {
				var frag = templateParser.clone(this.template)
				return new Fragment(this.linker, this.vm, frag, host, scope, parentFrag)
			}

			module.exports = FragmentFactory


			/***/ },
		/* 22 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var transition = __webpack_require__(9)

			/**
			 * Abstraction for a partially-compiled fragment.
			 * Can optionally compile content with a child scope.
			 *
			 * @param {Function} linker
			 * @param {Vue} vm
			 * @param {DocumentFragment} frag
			 * @param {Vue} [host]
			 * @param {Object} [scope]
			 */

			function Fragment (linker, vm, frag, host, scope, parentFrag) {
				this.children = []
				this.childFrags = []
				this.vm = vm
				this.scope = scope
				this.inserted = false
				this.parentFrag = parentFrag
				if (parentFrag) {
					parentFrag.childFrags.push(this)
				}
				this.unlink = linker(vm, frag, host, scope, this)
				var single = this.single = frag.childNodes.length === 1
				if (single) {
					this.node = frag.childNodes[0]
					this.before = singleBefore
					this.remove = singleRemove
				} else {
					this.node = _.createAnchor('fragment-start')
					this.end = _.createAnchor('fragment-end')
					this.frag = frag
					_.prepend(this.node, frag)
					frag.appendChild(this.end)
					this.before = multiBefore
					this.remove = multiRemove
				}
				this.node.__vfrag__ = this
			}

			/**
			 * Call attach/detach for all components contained within
			 * this fragment. Also do so recursively for all child
			 * fragments.
			 *
			 * @param {Function} hook
			 */

			Fragment.prototype.callHook = function (hook) {
				var i, l
				for (i = 0, l = this.children.length; i < l; i++) {
					hook(this.children[i])
				}
				for (i = 0, l = this.childFrags.length; i < l; i++) {
					this.childFrags[i].callHook(hook)
				}
			}

			/**
			 * Destroy the fragment.
			 */

			Fragment.prototype.destroy = function () {
				if (this.parentFrag) {
					this.parentFrag.childFrags.$remove(this)
				}
				this.unlink()
			}

			/**
			 * Insert fragment before target, single node version
			 *
			 * @param {Node} target
			 * @param {Boolean} withTransition
			 */

			function singleBefore (target, withTransition) {
				this.inserted = true
				var method = withTransition !== false
					? transition.before
					: _.before
				method(this.node, target, this.vm)
				if (_.inDoc(this.node)) {
					this.callHook(attach)
				}
			}

			/**
			 * Remove fragment, single node version
			 *
			 * @param {Boolean} [destroy]
			 */

			function singleRemove (destroy) {
				this.inserted = false
				var shouldCallRemove = _.inDoc(this.node)
				var self = this
				transition.remove(this.node, this.vm, function () {
					if (shouldCallRemove) {
						self.callHook(detach)
					}
					if (destroy) {
						self.destroy()
					}
				})
			}

			/**
			 * Insert fragment before target, multi-nodes version
			 *
			 * @param {Node} target
			 * @param {Boolean} withTransition
			 */

			function multiBefore (target, withTransition) {
				this.inserted = true
				var vm = this.vm
				var method = withTransition !== false
					? transition.before
					: _.before
				_.mapNodeRange(this.node, this.end, function (node) {
					method(node, target, vm)
				})
				if (_.inDoc(this.node)) {
					this.callHook(attach)
				}
			}

			/**
			 * Remove fragment, multi-nodes version
			 *
			 * @param {Boolean} [destroy]
			 */

			function multiRemove (destroy) {
				this.inserted = false
				var self = this
				var shouldCallRemove = _.inDoc(this.node)
				_.removeNodeRange(this.node, this.end, this.vm, this.frag, function () {
					if (shouldCallRemove) {
						self.callHook(detach)
					}
					if (destroy) {
						self.destroy()
					}
				})
			}

			/**
			 * Call attach hook for a Vue instance.
			 *
			 * @param {Vue} child
			 */

			function attach (child) {
				if (!child._isAttached) {
					child._callHook('attached')
				}
			}

			/**
			 * Call detach hook for a Vue instance.
			 *
			 * @param {Vue} child
			 */

			function detach (child) {
				if (child._isAttached) {
					child._callHook('detached')
				}
			}

			module.exports = Fragment


			/***/ },
		/* 23 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var FragmentFactory = __webpack_require__(21)

			module.exports = {

				priority: 2000,

				bind: function () {
					var el = this.el
					if (!el.__vue__) {
						// check else block
						var next = el.nextElementSibling
						if (next && _.attr(next, 'v-else') !== null) {
							_.remove(next)
							this.elseFactory = new FragmentFactory(this.vm, next)
						}
						// check main block
						this.anchor = _.createAnchor('v-if')
						_.replace(el, this.anchor)
						this.factory = new FragmentFactory(this.vm, el)
					} else {
						("development") !== 'production' && _.warn(
							'v-if="' + this.expression + '" cannot be ' +
							'used on an instance root element.'
						)
						this.invalid = true
					}
				},

				update: function (value) {
					if (this.invalid) return
					if (value) {
						if (!this.frag) {
							this.insert()
						}
					} else {
						this.remove()
					}
				},

				insert: function () {
					if (this.elseFrag) {
						this.elseFrag.remove(true)
						this.elseFrag = null
					}
					this.frag = this.factory.create(this._host, this._scope, this._frag)
					this.frag.before(this.anchor)
				},

				remove: function () {
					if (this.frag) {
						this.frag.remove(true)
						this.frag = null
					}
					if (this.elseFactory) {
						this.elseFrag = this.elseFactory.create(this._host, this._scope, this._frag)
						this.elseFrag.before(this.anchor)
					}
				},

				unbind: function () {
					if (this.frag) {
						this.frag.destroy()
					}
				}
			}


			/***/ },
		/* 24 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var transition = __webpack_require__(9)

			module.exports = {

				bind: function () {
					// check else block
					var next = this.el.nextElementSibling
					if (next && _.attr(next, 'v-else') !== null) {
						this.elseEl = next
					}
				},

				update: function (value) {
					var el = this.el
					transition.apply(el, value ? 1 : -1, function () {
						el.style.display = value ? '' : 'none'
					}, this.vm)
					var elseEl = this.elseEl
					if (elseEl) {
						transition.apply(elseEl, value ? -1 : 1, function () {
							elseEl.style.display = value ? 'none' : ''
						}, this.vm)
					}
				}
			}


			/***/ },
		/* 25 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)

			var handlers = {
				text: __webpack_require__(26),
				radio: __webpack_require__(27),
				select: __webpack_require__(28),
				checkbox: __webpack_require__(29)
			}

			module.exports = {

				priority: 800,
				twoWay: true,
				handlers: handlers,
				params: ['lazy', 'number', 'debounce'],

				/**
				 * Possible elements:
				 *   <select>
				 *   <textarea>
				 *   <input type="*">
				 *     - text
				 *     - checkbox
				 *     - radio
				 *     - number
				 */

				bind: function () {
					// friendly warning...
					this.checkFilters()
					if (this.hasRead && !this.hasWrite) {
						("development") !== 'production' && _.warn(
							'It seems you are using a read-only filter with ' +
							'v-model. You might want to use a two-way filter ' +
							'to ensure correct behavior.'
						)
					}
					var el = this.el
					var tag = el.tagName
					var handler
					if (tag === 'INPUT') {
						handler = handlers[el.type] || handlers.text
					} else if (tag === 'SELECT') {
						handler = handlers.select
					} else if (tag === 'TEXTAREA') {
						handler = handlers.text
					} else {
						("development") !== 'production' && _.warn(
							'v-model does not support element type: ' + tag
						)
						return
					}
					el.__v_model = this
					handler.bind.call(this)
					this.update = handler.update
					this._unbind = handler.unbind
				},

				/**
				 * Check read/write filter stats.
				 */

				checkFilters: function () {
					var filters = this.filters
					if (!filters) return
					var i = filters.length
					while (i--) {
						var filter = _.resolveAsset(this.vm.$options, 'filters', filters[i].name)
						if (typeof filter === 'function' || filter.read) {
							this.hasRead = true
						}
						if (filter.write) {
							this.hasWrite = true
						}
					}
				},

				unbind: function () {
					this.el.__v_model = null
					this._unbind && this._unbind()
				}
			}


			/***/ },
		/* 26 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)

			module.exports = {

				bind: function () {
					var self = this
					var el = this.el
					var isRange = el.type === 'range'
					var lazy = this.params.lazy
					var number = this.params.number
					var debounce = this.params.debounce

					// handle composition events.
					//   http://blog.evanyou.me/2014/01/03/composition-event/
					// skip this for Android because it handles composition
					// events quite differently. Android doesn't trigger
					// composition events for language input methods e.g.
					// Chinese, but instead triggers them for spelling
					// suggestions... (see Discussion/#162)
					var composing = false
					if (!_.isAndroid && !isRange) {
						this.on('compositionstart', function () {
							composing = true
						})
						this.on('compositionend', function () {
							composing = false
							// in IE11 the "compositionend" event fires AFTER
							// the "input" event, so the input handler is blocked
							// at the end... have to call it here.
							//
							// #1327: in lazy mode this is unecessary.
							if (!lazy) {
								self.listener()
							}
						})
					}

					// prevent messing with the input when user is typing,
					// and force update on blur.
					this.focused = false
					if (!isRange) {
						this.on('focus', function () {
							self.focused = true
						})
						this.on('blur', function () {
							self.focused = false
							self.listener()
						})
					}

					// Now attach the main listener
					this.listener = function () {
						if (composing) return
						var val = number || isRange
							? _.toNumber(el.value)
							: el.value
						self.set(val)
						// force update on next tick to avoid lock & same value
						// also only update when user is not typing
						_.nextTick(function () {
							if (self._bound && !self.focused) {
								self.update(self._watcher.value)
							}
						})
					}

					// apply debounce
					if (debounce) {
						this.listener = _.debounce(this.listener, debounce)
					}

					// Support jQuery events, since jQuery.trigger() doesn't
					// trigger native events in some cases and some plugins
					// rely on $.trigger()
					//
					// We want to make sure if a listener is attached using
					// jQuery, it is also removed with jQuery, that's why
					// we do the check for each directive instance and
					// store that check result on itself. This also allows
					// easier test coverage control by unsetting the global
					// jQuery variable in tests.
					this.hasjQuery = typeof jQuery === 'function'
					if (this.hasjQuery) {
						jQuery(el).on('change', this.listener)
						if (!lazy) {
							jQuery(el).on('input', this.listener)
						}
					} else {
						this.on('change', this.listener)
						if (!lazy) {
							this.on('input', this.listener)
						}
					}

					// IE9 doesn't fire input event on backspace/del/cut
					if (!lazy && _.isIE9) {
						this.on('cut', function () {
							_.nextTick(self.listener)
						})
						this.on('keyup', function (e) {
							if (e.keyCode === 46 || e.keyCode === 8) {
								self.listener()
							}
						})
					}

					// set initial value if present
					if (
						el.hasAttribute('value') ||
						(el.tagName === 'TEXTAREA' && el.value.trim())
					) {
						this.afterBind = this.listener
					}
				},

				update: function (value) {
					this.el.value = _.toString(value)
				},

				unbind: function () {
					var el = this.el
					if (this.hasjQuery) {
						jQuery(el).off('change', this.listener)
						jQuery(el).off('input', this.listener)
					}
				}
			}


			/***/ },
		/* 27 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)

			module.exports = {

				bind: function () {
					var self = this
					var el = this.el

					this.getValue = function () {
						// value overwrite via v-bind:value
						if (el.hasOwnProperty('_value')) {
							return el._value
						}
						var val = el.value
						if (self.params.number) {
							val = _.toNumber(val)
						}
						return val
					}

					this.listener = function () {
						self.set(self.getValue())
					}
					this.on('change', this.listener)

					if (el.checked) {
						this.afterBind = this.listener
					}
				},

				update: function (value) {
					this.el.checked = _.looseEqual(value, this.getValue())
				}
			}


			/***/ },
		/* 28 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)

			module.exports = {

				bind: function () {
					var self = this
					var el = this.el

					// method to force update DOM using latest value.
					this.forceUpdate = function () {
						if (self._watcher) {
							self.update(self._watcher.get())
						}
					}

					// check if this is a multiple select
					var multiple = this.multiple = el.hasAttribute('multiple')

					// attach listener
					this.listener = function () {
						var value = getValue(el, multiple)
						value = self.params.number
							? _.isArray(value)
							? value.map(_.toNumber)
							: _.toNumber(value)
							: value
						self.set(value)
					}
					this.on('change', this.listener)

					// if has initial value, set afterBind
					var initValue = getValue(el, multiple, true)
					if ((multiple && initValue.length) ||
						(!multiple && initValue !== null)) {
						this.afterBind = this.listener
					}

					// All major browsers except Firefox resets
					// selectedIndex with value -1 to 0 when the element
					// is appended to a new parent, therefore we have to
					// force a DOM update whenever that happens...
					this.vm.$on('hook:attached', this.forceUpdate)
				},

				update: function (value) {
					var el = this.el
					el.selectedIndex = -1
					var multi = this.multiple && _.isArray(value)
					var options = el.options
					var i = options.length
					var op, val
					while (i--) {
						op = options[i]
						val = op.hasOwnProperty('_value')
							? op._value
							: op.value
						/* eslint-disable eqeqeq */
						op.selected = multi
							? indexOf(value, val) > -1
							: _.looseEqual(value, val)
						/* eslint-enable eqeqeq */
					}
				},

				unbind: function () {
					/* istanbul ignore next */
					this.vm.$off('hook:attached', this.forceUpdate)
				}
			}

			/**
			 * Get select value
			 *
			 * @param {SelectElement} el
			 * @param {Boolean} multi
			 * @param {Boolean} init
			 * @return {Array|*}
			 */

			function getValue (el, multi, init) {
				var res = multi ? [] : null
				var op, val, selected
				for (var i = 0, l = el.options.length; i < l; i++) {
					op = el.options[i]
					selected = init
						? op.hasAttribute('selected')
						: op.selected
					if (selected) {
						val = op.hasOwnProperty('_value')
							? op._value
							: op.value
						if (multi) {
							res.push(val)
						} else {
							return val
						}
					}
				}
				return res
			}

			/**
			 * Native Array.indexOf uses strict equal, but in this
			 * case we need to match string/numbers with custom equal.
			 *
			 * @param {Array} arr
			 * @param {*} val
			 */

			function indexOf (arr, val) {
				var i = arr.length
				while (i--) {
					if (_.looseEqual(arr[i], val)) {
						return i
					}
				}
				return -1
			}


			/***/ },
		/* 29 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)

			module.exports = {

				bind: function () {
					var self = this
					var el = this.el

					this.getValue = function () {
						return el.hasOwnProperty('_value')
							? el._value
							: self.params.number
							? _.toNumber(el.value)
							: el.value
					}

					function getBooleanValue () {
						var val = el.checked
						if (val && el.hasOwnProperty('_trueValue')) {
							return el._trueValue
						}
						if (!val && el.hasOwnProperty('_falseValue')) {
							return el._falseValue
						}
						return val
					}

					this.listener = function () {
						var model = self._watcher.value
						if (_.isArray(model)) {
							var val = self.getValue()
							if (el.checked) {
								if (_.indexOf(model, val) < 0) {
									model.push(val)
								}
							} else {
								model.$remove(val)
							}
						} else {
							self.set(getBooleanValue())
						}
					}

					this.on('change', this.listener)
					if (el.checked) {
						this.afterBind = this.listener
					}
				},

				update: function (value) {
					var el = this.el
					if (_.isArray(value)) {
						el.checked = _.indexOf(value, this.getValue()) > -1
					} else {
						if (el.hasOwnProperty('_trueValue')) {
							el.checked = _.looseEqual(value, el._trueValue)
						} else {
							el.checked = !!value
						}
					}
				}
			}


			/***/ },
		/* 30 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)

			// keyCode aliases
			var keyCodes = {
				esc: 27,
				tab: 9,
				enter: 13,
				space: 32,
				'delete': 46,
				up: 38,
				left: 37,
				right: 39,
				down: 40
			}

			function keyFilter (handler, keys) {
				var codes = keys.map(function (key) {
					var code = keyCodes[key]
					if (!code) {
						code = parseInt(key, 10)
					}
					return code
				})
				return function keyHandler (e) {
					if (codes.indexOf(e.keyCode) > -1) {
						return handler.call(this, e)
					}
				}
			}

			function stopFilter (handler) {
				return function stopHandler (e) {
					e.stopPropagation()
					return handler.call(this, e)
				}
			}

			function preventFilter (handler) {
				return function preventHandler (e) {
					e.preventDefault()
					return handler.call(this, e)
				}
			}

			module.exports = {

				acceptStatement: true,
				priority: 700,

				bind: function () {
					// deal with iframes
					if (
						this.el.tagName === 'IFRAME' &&
						this.arg !== 'load'
					) {
						var self = this
						this.iframeBind = function () {
							_.on(self.el.contentWindow, self.arg, self.handler)
						}
						this.on('load', this.iframeBind)
					}
				},

				update: function (handler) {
					// stub a noop for v-on with no value,
					// e.g. @mousedown.prevent
					if (!this.descriptor.raw) {
						handler = function () {}
					}

					if (typeof handler !== 'function') {
						("development") !== 'production' && _.warn(
							'v-on:' + this.arg + '="' +
							this.expression + '" expects a function value, ' +
							'got ' + handler
						)
						return
					}

					// apply modifiers
					if (this.modifiers.stop) {
						handler = stopFilter(handler)
					}
					if (this.modifiers.prevent) {
						handler = preventFilter(handler)
					}
					// key filter
					var keys = Object.keys(this.modifiers)
						.filter(function (key) {
							return key !== 'stop' && key !== 'prevent'
						})
					if (keys.length) {
						handler = keyFilter(handler, keys)
					}

					this.reset()
					var scope = this._scope || this.vm
					this.handler = function (e) {
						scope.$event = e
						var res = handler(e)
						scope.$event = null
						return res
					}
					if (this.iframeBind) {
						this.iframeBind()
					} else {
						_.on(this.el, this.arg, this.handler)
					}
				},

				reset: function () {
					var el = this.iframeBind
						? this.el.contentWindow
						: this.el
					if (this.handler) {
						_.off(el, this.arg, this.handler)
					}
				},

				unbind: function () {
					this.reset()
				}
			}


			/***/ },
		/* 31 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)

			// xlink
			var xlinkNS = 'http://www.w3.org/1999/xlink'
			var xlinkRE = /^xlink:/

			// these input element attributes should also set their
			// corresponding properties
			var inputProps = {
				value: 1,
				checked: 1,
				selected: 1
			}

			// these attributes should set a hidden property for
			// binding v-model to object values
			var modelProps = {
				value: '_value',
				'true-value': '_trueValue',
				'false-value': '_falseValue'
			}

			// check for attributes that prohibit interpolations
			var disallowedInterpAttrRE = /^v-|^:|^@|^(is|transition|transition-mode|debounce|track-by|stagger|enter-stagger|leave-stagger)$/

			module.exports = {

				priority: 850,

				bind: function () {
					var attr = this.arg
					var tag = this.el.tagName
					// should be deep watch on object mode
					if (!attr) {
						this.deep = true
					}
					// handle interpolation bindings
					if (this.descriptor.interp) {
						// only allow binding on native attributes
						if (
							disallowedInterpAttrRE.test(attr) ||
							(attr === 'name' && (tag === 'PARTIAL' || tag === 'SLOT'))
						) {
							("development") !== 'production' && _.warn(
								attr + '="' + this.descriptor.raw + '": ' +
								'attribute interpolation is not allowed in Vue.js ' +
								'directives and special attributes.'
							)
							this.el.removeAttribute(attr)
							this.invalid = true
						}

						/* istanbul ignore if */
						if (true) {
							var raw = attr + '="' + this.descriptor.raw + '": '
							// warn src
							if (attr === 'src') {
								_.warn(
									raw + 'interpolation in "src" attribute will cause ' +
									'a 404 request. Use v-bind:src instead.'
								)
							}

							// warn style
							if (attr === 'style') {
								_.warn(
									raw + 'interpolation in "style" attribute will cause ' +
									'the attribute to be discarded in Internet Explorer. ' +
									'Use v-bind:style instead.'
								)
							}
						}
					}
				},

				update: function (value) {
					if (this.invalid) {
						return
					}
					var attr = this.arg
					if (this.arg) {
						this.handleSingle(attr, value)
					} else {
						this.handleObject(value || {})
					}
				},

				// share object handler with v-bind:class
				handleObject: __webpack_require__(32).handleObject,

				handleSingle: function (attr, value) {
					if (inputProps[attr] && attr in this.el) {
						this.el[attr] = attr === 'value'
							? (value || '') // IE9 will set input.value to "null" for null...
							: value
					}
					// set model props
					var modelProp = modelProps[attr]
					if (modelProp) {
						this.el[modelProp] = value
						// update v-model if present
						var model = this.el.__v_model
						if (model) {
							model.listener()
						}
					}
					// do not set value attribute for textarea
					if (attr === 'value' && this.el.tagName === 'TEXTAREA') {
						this.el.removeAttribute(attr)
						return
					}
					// update attribute
					if (value != null && value !== false) {
						if (xlinkRE.test(attr)) {
							this.el.setAttributeNS(xlinkNS, attr, value)
						} else {
							this.el.setAttribute(attr, value)
						}
					} else {
						this.el.removeAttribute(attr)
					}
				}
			}


			/***/ },
		/* 32 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var prefixes = ['-webkit-', '-moz-', '-ms-']
			var camelPrefixes = ['Webkit', 'Moz', 'ms']
			var importantRE = /!important;?$/
			var testEl = null
			var propCache = {}

			module.exports = {

				deep: true,

				update: function (value) {
					if (typeof value === 'string') {
						this.el.style.cssText = value
					} else if (_.isArray(value)) {
						this.handleObject(value.reduce(_.extend, {}))
					} else {
						this.handleObject(value || {})
					}
				},

				handleObject: function (value) {
					// cache object styles so that only changed props
					// are actually updated.
					var cache = this.cache || (this.cache = {})
					var name, val
					for (name in cache) {
						if (!(name in value)) {
							this.handleSingle(name, null)
							delete cache[name]
						}
					}
					for (name in value) {
						val = value[name]
						if (val !== cache[name]) {
							cache[name] = val
							this.handleSingle(name, val)
						}
					}
				},

				handleSingle: function (prop, value) {
					prop = normalize(prop)
					if (!prop) return // unsupported prop
					// cast possible numbers/booleans into strings
					if (value != null) value += ''
					if (value) {
						var isImportant = importantRE.test(value)
							? 'important'
							: ''
						if (isImportant) {
							value = value.replace(importantRE, '').trim()
						}
						this.el.style.setProperty(prop, value, isImportant)
					} else {
						this.el.style.removeProperty(prop)
					}
				}

			}

			/**
			 * Normalize a CSS property name.
			 * - cache result
			 * - auto prefix
			 * - camelCase -> dash-case
			 *
			 * @param {String} prop
			 * @return {String}
			 */

			function normalize (prop) {
				if (propCache[prop]) {
					return propCache[prop]
				}
				var res = prefix(prop)
				propCache[prop] = propCache[res] = res
				return res
			}

			/**
			 * Auto detect the appropriate prefix for a CSS property.
			 * https://gist.github.com/paulirish/523692
			 *
			 * @param {String} prop
			 * @return {String}
			 */

			function prefix (prop) {
				prop = _.hyphenate(prop)
				var camel = _.camelize(prop)
				var upper = camel.charAt(0).toUpperCase() + camel.slice(1)
				if (!testEl) {
					testEl = document.createElement('div')
				}
				if (camel in testEl.style) {
					return prop
				}
				var i = prefixes.length
				var prefixed
				while (i--) {
					prefixed = camelPrefixes[i] + upper
					if (prefixed in testEl.style) {
						return prefixes[i] + prop
					}
				}
			}


			/***/ },
		/* 33 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)

			module.exports = {

				priority: 1500,

				bind: function () {
					/* istanbul ignore if */
					if (!this.arg) {
						return
					}
					var id = this.id = _.camelize(this.arg)
					var refs = (this._scope || this.vm).$els
					if (refs.hasOwnProperty(id)) {
						refs[id] = this.el
					} else {
						_.defineReactive(refs, id, this.el)
					}
				},

				unbind: function () {
					var refs = (this._scope || this.vm).$els
					if (refs[this.id] === this.el) {
						refs[this.id] = null
					}
				}
			}


			/***/ },
		/* 34 */
		/***/ function(module, exports, __webpack_require__) {

			if (true) {
				module.exports = {
					bind: function () {
						__webpack_require__(1).warn(
							'v-ref:' + this.arg + ' must be used on a child ' +
							'component. Found on <' + this.el.tagName.toLowerCase() + '>.'
						)
					}
				}
			}


			/***/ },
		/* 35 */
		/***/ function(module, exports) {

			module.exports = {
				bind: function () {
					var el = this.el
					this.vm.$once('hook:compiled', function () {
						el.removeAttribute('v-cloak')
					})
				}
			}


			/***/ },
		/* 36 */
		/***/ function(module, exports, __webpack_require__) {

			exports.style = __webpack_require__(32)
			exports['class'] = __webpack_require__(37)
			exports.component = __webpack_require__(38)
			exports.prop = __webpack_require__(39)
			exports.transition = __webpack_require__(45)


			/***/ },
		/* 37 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var addClass = _.addClass
			var removeClass = _.removeClass

			module.exports = {

				deep: true,

				update: function (value) {
					if (value && typeof value === 'string') {
						this.handleObject(stringToObject(value))
					} else if (_.isPlainObject(value)) {
						this.handleObject(value)
					} else if (_.isArray(value)) {
						this.handleArray(value)
					} else {
						this.cleanup()
					}
				},

				handleObject: function (value) {
					this.cleanup(value)
					var keys = this.prevKeys = Object.keys(value)
					for (var i = 0, l = keys.length; i < l; i++) {
						var key = keys[i]
						if (value[key]) {
							addClass(this.el, key)
						} else {
							removeClass(this.el, key)
						}
					}
				},

				handleArray: function (value) {
					this.cleanup(value)
					for (var i = 0, l = value.length; i < l; i++) {
						if (value[i]) {
							addClass(this.el, value[i])
						}
					}
					this.prevKeys = value.slice()
				},

				cleanup: function (value) {
					if (this.prevKeys) {
						var i = this.prevKeys.length
						while (i--) {
							var key = this.prevKeys[i]
							if (key && (!value || !contains(value, key))) {
								removeClass(this.el, key)
							}
						}
					}
				}
			}

			function stringToObject (value) {
				var res = {}
				var keys = value.trim().split(/\s+/)
				var i = keys.length
				while (i--) {
					res[keys[i]] = true
				}
				return res
			}

			function contains (value, key) {
				return _.isArray(value)
					? value.indexOf(key) > -1
					: value.hasOwnProperty(key)
			}


			/***/ },
		/* 38 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var templateParser = __webpack_require__(19)

			module.exports = {

				priority: 1500,

				params: [
					'keep-alive',
					'transition-mode',
					'inline-template'
				],

				/**
				 * Setup. Two possible usages:
				 *
				 * - static:
				 *   <comp> or <div v-component="comp">
				 *
				 * - dynamic:
				 *   <component :is="view">
				 */

				bind: function () {
					if (!this.el.__vue__) {
						// check ref
						this.ref = _.findRef(this.el)
						var refs = (this._scope || this.vm).$refs
						if (this.ref && !refs.hasOwnProperty(this.ref)) {
							_.defineReactive(refs, this.ref, null)
						}
						// keep-alive cache
						this.keepAlive = this.params.keepAlive
						if (this.keepAlive) {
							this.cache = {}
						}
						// check inline-template
						if (this.params.inlineTemplate) {
							// extract inline template as a DocumentFragment
							this.inlineTemplate = _.extractContent(this.el, true)
						}
						// component resolution related state
						this.pendingComponentCb =
							this.Component = null
						// transition related state
						this.pendingRemovals = 0
						this.pendingRemovalCb = null
						// check dynamic component params
						// create a ref anchor
						this.anchor = _.createAnchor('v-component')
						_.replace(this.el, this.anchor)
						// if static, build right now.
						if (this.literal) {
							this.setComponent(this.expression)
						}
					} else {
						("development") !== 'production' && _.warn(
							'cannot mount component "' + this.expression + '" ' +
							'on already mounted element: ' + this.el
						)
					}
				},

				/**
				 * Public update, called by the watcher in the dynamic
				 * literal scenario, e.g. <component :is="view">
				 */

				update: function (value) {
					if (!this.literal) {
						this.setComponent(value)
					}
				},

				/**
				 * Switch dynamic components. May resolve the component
				 * asynchronously, and perform transition based on
				 * specified transition mode. Accepts a few additional
				 * arguments specifically for vue-router.
				 *
				 * The callback is called when the full transition is
				 * finished.
				 *
				 * @param {String} value
				 * @param {Function} [cb]
				 */

				setComponent: function (value, cb) {
					this.invalidatePending()
					if (!value) {
						// just remove current
						this.unbuild(true)
						this.remove(this.childVM, cb)
						this.childVM = null
					} else {
						var self = this
						this.resolveComponent(value, function () {
							self.mountComponent(cb)
						})
					}
				},

				/**
				 * Resolve the component constructor to use when creating
				 * the child vm.
				 */

				resolveComponent: function (id, cb) {
					var self = this
					this.pendingComponentCb = _.cancellable(function (Component) {
						self.ComponentName = Component.options.name || id
						self.Component = Component
						cb()
					})
					this.vm._resolveComponent(id, this.pendingComponentCb)
				},

				/**
				 * Create a new instance using the current constructor and
				 * replace the existing instance. This method doesn't care
				 * whether the new component and the old one are actually
				 * the same.
				 *
				 * @param {Function} [cb]
				 */

				mountComponent: function (cb) {
					// actual mount
					this.unbuild(true)
					var self = this
					var activateHook = this.Component.options.activate
					var cached = this.getCached()
					var newComponent = this.build()
					if (activateHook && !cached) {
						this.waitingFor = newComponent
						activateHook.call(newComponent, function () {
							self.waitingFor = null
							self.transition(newComponent, cb)
						})
					} else {
						this.transition(newComponent, cb)
					}
				},

				/**
				 * When the component changes or unbinds before an async
				 * constructor is resolved, we need to invalidate its
				 * pending callback.
				 */

				invalidatePending: function () {
					if (this.pendingComponentCb) {
						this.pendingComponentCb.cancel()
						this.pendingComponentCb = null
					}
				},

				/**
				 * Instantiate/insert a new child vm.
				 * If keep alive and has cached instance, insert that
				 * instance; otherwise build a new one and cache it.
				 *
				 * @param {Object} [extraOptions]
				 * @return {Vue} - the created instance
				 */

				build: function (extraOptions) {
					var cached = this.getCached()
					if (cached) {
						return cached
					}
					if (this.Component) {
						// default options
						var options = {
							name: this.ComponentName,
							el: templateParser.clone(this.el),
							template: this.inlineTemplate,
							// make sure to add the child with correct parent
							// if this is a transcluded component, its parent
							// should be the transclusion host.
							parent: this._host || this.vm,
							// if no inline-template, then the compiled
							// linker can be cached for better performance.
							_linkerCachable: !this.inlineTemplate,
							_ref: this.ref,
							_asComponent: true,
							_isRouterView: this._isRouterView,
							// if this is a transcluded component, context
							// will be the common parent vm of this instance
							// and its host.
							_context: this.vm,
							// if this is inside an inline v-for, the scope
							// will be the intermediate scope created for this
							// repeat fragment. this is used for linking props
							// and container directives.
							_scope: this._scope,
							// pass in the owner fragment of this component.
							// this is necessary so that the fragment can keep
							// track of its contained components in order to
							// call attach/detach hooks for them.
							_frag: this._frag
						}
						// extra options
						// in 1.0.0 this is used by vue-router only
						/* istanbul ignore if */
						if (extraOptions) {
							_.extend(options, extraOptions)
						}
						var child = new this.Component(options)
						if (this.keepAlive) {
							this.cache[this.Component.cid] = child
						}
						/* istanbul ignore if */
						if (("development") !== 'production' &&
							this.el.hasAttribute('transition') &&
							child._isFragment) {
							_.warn(
								'Transitions will not work on a fragment instance. ' +
								'Template: ' + child.$options.template
							)
						}
						return child
					}
				},

				/**
				 * Try to get a cached instance of the current component.
				 *
				 * @return {Vue|undefined}
				 */

				getCached: function () {
					return this.keepAlive && this.cache[this.Component.cid]
				},

				/**
				 * Teardown the current child, but defers cleanup so
				 * that we can separate the destroy and removal steps.
				 *
				 * @param {Boolean} defer
				 */

				unbuild: function (defer) {
					if (this.waitingFor) {
						this.waitingFor.$destroy()
						this.waitingFor = null
					}
					var child = this.childVM
					if (!child || this.keepAlive) {
						return
					}
					// the sole purpose of `deferCleanup` is so that we can
					// "deactivate" the vm right now and perform DOM removal
					// later.
					child.$destroy(false, defer)
				},

				/**
				 * Remove current destroyed child and manually do
				 * the cleanup after removal.
				 *
				 * @param {Function} cb
				 */

				remove: function (child, cb) {
					var keepAlive = this.keepAlive
					if (child) {
						// we may have a component switch when a previous
						// component is still being transitioned out.
						// we want to trigger only one lastest insertion cb
						// when the existing transition finishes. (#1119)
						this.pendingRemovals++
						this.pendingRemovalCb = cb
						var self = this
						child.$remove(function () {
							self.pendingRemovals--
							if (!keepAlive) child._cleanup()
							if (!self.pendingRemovals && self.pendingRemovalCb) {
								self.pendingRemovalCb()
								self.pendingRemovalCb = null
							}
						})
					} else if (cb) {
						cb()
					}
				},

				/**
				 * Actually swap the components, depending on the
				 * transition mode. Defaults to simultaneous.
				 *
				 * @param {Vue} target
				 * @param {Function} [cb]
				 */

				transition: function (target, cb) {
					var self = this
					var current = this.childVM
					// for devtool inspection
					if (true) {
						if (current) current._inactive = true
						target._inactive = false
					}
					this.childVM = target
					switch (self.params.transitionMode) {
						case 'in-out':
							target.$before(self.anchor, function () {
								self.remove(current, cb)
							})
							break
						case 'out-in':
							self.remove(current, function () {
								target.$before(self.anchor, cb)
							})
							break
						default:
							self.remove(current)
							target.$before(self.anchor, cb)
					}
				},

				/**
				 * Unbind.
				 */

				unbind: function () {
					this.invalidatePending()
					// Do not defer cleanup when unbinding
					this.unbuild()
					// destroy all keep-alive cached instances
					if (this.cache) {
						for (var key in this.cache) {
							this.cache[key].$destroy()
						}
						this.cache = null
					}
				}
			}


			/***/ },
		/* 39 */
		/***/ function(module, exports, __webpack_require__) {

			// NOTE: the prop internal directive is compiled and linked
			// during _initScope(), before the created hook is called.
			// The purpose is to make the initial prop values available
			// inside `created` hooks and `data` functions.

			var _ = __webpack_require__(1)
			var Watcher = __webpack_require__(40)
			var bindingModes = __webpack_require__(5)._propBindingModes

			module.exports = {

				bind: function () {

					var child = this.vm
					var parent = child._context
					// passed in from compiler directly
					var prop = this.descriptor.prop
					var childKey = prop.path
					var parentKey = prop.parentPath
					var twoWay = prop.mode === bindingModes.TWO_WAY

					var parentWatcher = this.parentWatcher = new Watcher(
						parent,
						parentKey,
						function (val) {
							if (_.assertProp(prop, val)) {
								child[childKey] = val
							}
						}, {
							twoWay: twoWay,
							filters: prop.filters,
							// important: props need to be observed on the
							// v-for scope if present
							scope: this._scope
						}
					)

					// set the child initial value.
					_.initProp(child, prop, parentWatcher.value)

					// setup two-way binding
					if (twoWay) {
						// important: defer the child watcher creation until
						// the created hook (after data observation)
						var self = this
						child.$once('hook:created', function () {
							self.childWatcher = new Watcher(
								child,
								childKey,
								function (val) {
									parentWatcher.set(val)
								}
							)
						})
					}
				},

				unbind: function () {
					this.parentWatcher.teardown()
					if (this.childWatcher) {
						this.childWatcher.teardown()
					}
				}
			}


			/***/ },
		/* 40 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var config = __webpack_require__(5)
			var Dep = __webpack_require__(41)
			var expParser = __webpack_require__(42)
			var batcher = __webpack_require__(44)
			var uid = 0

			/**
			 * A watcher parses an expression, collects dependencies,
			 * and fires callback when the expression value changes.
			 * This is used for both the $watch() api and directives.
			 *
			 * @param {Vue} vm
			 * @param {String} expression
			 * @param {Function} cb
			 * @param {Object} options
			 *                 - {Array} filters
			 *                 - {Boolean} twoWay
			 *                 - {Boolean} deep
			 *                 - {Boolean} user
			 *                 - {Boolean} sync
			 *                 - {Boolean} lazy
			 *                 - {Function} [preProcess]
			 *                 - {Function} [postProcess]
			 * @constructor
			 */

			function Watcher (vm, expOrFn, cb, options) {
				// mix in options
				if (options) {
					_.extend(this, options)
				}
				var isFn = typeof expOrFn === 'function'
				this.vm = vm
				vm._watchers.push(this)
				this.expression = isFn ? expOrFn.toString() : expOrFn
				this.cb = cb
				this.id = ++uid // uid for batching
				this.active = true
				this.dirty = this.lazy // for lazy watchers
				this.deps = Object.create(null)
				this.newDeps = null
				this.prevError = null // for async error stacks
				// parse expression for getter/setter
				if (isFn) {
					this.getter = expOrFn
					this.setter = undefined
				} else {
					var res = expParser.parse(expOrFn, this.twoWay)
					this.getter = res.get
					this.setter = res.set
				}
				this.value = this.lazy
					? undefined
					: this.get()
				// state for avoiding false triggers for deep and Array
				// watchers during vm._digest()
				this.queued = this.shallow = false
			}

			/**
			 * Add a dependency to this directive.
			 *
			 * @param {Dep} dep
			 */

			Watcher.prototype.addDep = function (dep) {
				var id = dep.id
				if (!this.newDeps[id]) {
					this.newDeps[id] = dep
					if (!this.deps[id]) {
						this.deps[id] = dep
						dep.addSub(this)
					}
				}
			}

			/**
			 * Evaluate the getter, and re-collect dependencies.
			 */

			Watcher.prototype.get = function () {
				this.beforeGet()
				var scope = this.scope || this.vm
				var value
				try {
					value = this.getter.call(scope, scope)
				} catch (e) {
					if (
						("development") !== 'production' &&
						config.warnExpressionErrors
					) {
						_.warn(
							'Error when evaluating expression "' +
							this.expression + '". ' +
							(config.debug
									? ''
									: 'Turn on debug mode to see stack trace.'
							), e
						)
					}
				}
				// "touch" every property so they are all tracked as
				// dependencies for deep watching
				if (this.deep) {
					traverse(value)
				}
				if (this.preProcess) {
					value = this.preProcess(value)
				}
				if (this.filters) {
					value = scope._applyFilters(value, null, this.filters, false)
				}
				if (this.postProcess) {
					value = this.postProcess(value)
				}
				this.afterGet()
				return value
			}

			/**
			 * Set the corresponding value with the setter.
			 *
			 * @param {*} value
			 */

			Watcher.prototype.set = function (value) {
				var scope = this.scope || this.vm
				if (this.filters) {
					value = scope._applyFilters(
						value, this.value, this.filters, true)
				}
				try {
					this.setter.call(scope, scope, value)
				} catch (e) {
					if (
						("development") !== 'production' &&
						config.warnExpressionErrors
					) {
						_.warn(
							'Error when evaluating setter "' +
							this.expression + '"', e
						)
					}
				}
				// two-way sync for v-for alias
				var forContext = scope.$forContext
				if (true) {
					if (
						forContext &&
						forContext.filters &&
						(new RegExp(forContext.alias + '\\b')).test(this.expression)
					) {
						_.warn(
							'It seems you are using two-way binding on ' +
							'a v-for alias (' + this.expression + '), and the ' +
							'v-for has filters. This will not work properly. ' +
							'Either remove the filters or use an array of ' +
							'objects and bind to object properties instead.'
						)
					}
				}
				if (
					forContext &&
					forContext.alias === this.expression &&
					!forContext.filters
				) {
					if (scope.$key) { // original is an object
						forContext.rawValue[scope.$key] = value
					} else {
						forContext.rawValue.$set(scope.$index, value)
					}
				}
			}

			/**
			 * Prepare for dependency collection.
			 */

			Watcher.prototype.beforeGet = function () {
				Dep.target = this
				this.newDeps = Object.create(null)
			}

			/**
			 * Clean up for dependency collection.
			 */

			Watcher.prototype.afterGet = function () {
				Dep.target = null
				var ids = Object.keys(this.deps)
				var i = ids.length
				while (i--) {
					var id = ids[i]
					if (!this.newDeps[id]) {
						this.deps[id].removeSub(this)
					}
				}
				this.deps = this.newDeps
			}

			/**
			 * Subscriber interface.
			 * Will be called when a dependency changes.
			 *
			 * @param {Boolean} shallow
			 */

			Watcher.prototype.update = function (shallow) {
				if (this.lazy) {
					this.dirty = true
				} else if (this.sync || !config.async) {
					this.run()
				} else {
					// if queued, only overwrite shallow with non-shallow,
					// but not the other way around.
					this.shallow = this.queued
						? shallow
						? this.shallow
						: false
						: !!shallow
					this.queued = true
					// record before-push error stack in debug mode
					/* istanbul ignore if */
					if (("development") !== 'production' && config.debug) {
						this.prevError = new Error('[vue] async stack trace')
					}
					batcher.push(this)
				}
			}

			/**
			 * Batcher job interface.
			 * Will be called by the batcher.
			 */

			Watcher.prototype.run = function () {
				if (this.active) {
					var value = this.get()
					if (
						value !== this.value ||
							// Deep watchers and Array watchers should fire even
							// when the value is the same, because the value may
							// have mutated; but only do so if this is a
							// non-shallow update (caused by a vm digest).
						((_.isArray(value) || this.deep) && !this.shallow)
					) {
						// set new value
						var oldValue = this.value
						this.value = value
						// in debug + async mode, when a watcher callbacks
						// throws, we also throw the saved before-push error
						// so the full cross-tick stack trace is available.
						var prevError = this.prevError
						/* istanbul ignore if */
						if (("development") !== 'production' &&
							config.debug && prevError) {
							this.prevError = null
							try {
								this.cb.call(this.vm, value, oldValue)
							} catch (e) {
								_.nextTick(function () {
									throw prevError
								}, 0)
								throw e
							}
						} else {
							this.cb.call(this.vm, value, oldValue)
						}
					}
					this.queued = this.shallow = false
				}
			}

			/**
			 * Evaluate the value of the watcher.
			 * This only gets called for lazy watchers.
			 */

			Watcher.prototype.evaluate = function () {
				// avoid overwriting another watcher that is being
				// collected.
				var current = Dep.target
				this.value = this.get()
				this.dirty = false
				Dep.target = current
			}

			/**
			 * Depend on all deps collected by this watcher.
			 */

			Watcher.prototype.depend = function () {
				var depIds = Object.keys(this.deps)
				var i = depIds.length
				while (i--) {
					this.deps[depIds[i]].depend()
				}
			}

			/**
			 * Remove self from all dependencies' subcriber list.
			 */

			Watcher.prototype.teardown = function () {
				if (this.active) {
					// remove self from vm's watcher list
					// we can skip this if the vm if being destroyed
					// which can improve teardown performance.
					if (!this.vm._isBeingDestroyed) {
						this.vm._watchers.$remove(this)
					}
					var depIds = Object.keys(this.deps)
					var i = depIds.length
					while (i--) {
						this.deps[depIds[i]].removeSub(this)
					}
					this.active = false
					this.vm = this.cb = this.value = null
				}
			}

			/**
			 * Recrusively traverse an object to evoke all converted
			 * getters, so that every nested property inside the object
			 * is collected as a "deep" dependency.
			 *
			 * @param {*} val
			 */

			function traverse (val) {
				var i, keys
				if (_.isArray(val)) {
					i = val.length
					while (i--) traverse(val[i])
				} else if (_.isObject(val)) {
					keys = Object.keys(val)
					i = keys.length
					while (i--) traverse(val[keys[i]])
				}
			}

			module.exports = Watcher


			/***/ },
		/* 41 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var uid = 0

			/**
			 * A dep is an observable that can have multiple
			 * directives subscribing to it.
			 *
			 * @constructor
			 */

			function Dep () {
				this.id = uid++
				this.subs = []
			}

			// the current target watcher being evaluated.
			// this is globally unique because there could be only one
			// watcher being evaluated at any time.
			Dep.target = null

			/**
			 * Add a directive subscriber.
			 *
			 * @param {Directive} sub
			 */

			Dep.prototype.addSub = function (sub) {
				this.subs.push(sub)
			}

			/**
			 * Remove a directive subscriber.
			 *
			 * @param {Directive} sub
			 */

			Dep.prototype.removeSub = function (sub) {
				this.subs.$remove(sub)
			}

			/**
			 * Add self as a dependency to the target watcher.
			 */

			Dep.prototype.depend = function () {
				Dep.target.addDep(this)
			}

			/**
			 * Notify all subscribers of a new value.
			 */

			Dep.prototype.notify = function () {
				// stablize the subscriber list first
				var subs = _.toArray(this.subs)
				for (var i = 0, l = subs.length; i < l; i++) {
					subs[i].update()
				}
			}

			module.exports = Dep


			/***/ },
		/* 42 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var Path = __webpack_require__(43)
			var Cache = __webpack_require__(7)
			var expressionCache = new Cache(1000)

			var allowedKeywords =
				'Math,Date,this,true,false,null,undefined,Infinity,NaN,' +
				'isNaN,isFinite,decodeURI,decodeURIComponent,encodeURI,' +
				'encodeURIComponent,parseInt,parseFloat'
			var allowedKeywordsRE =
				new RegExp('^(' + allowedKeywords.replace(/,/g, '\\b|') + '\\b)')

			// keywords that don't make sense inside expressions
			var improperKeywords =
				'break,case,class,catch,const,continue,debugger,default,' +
				'delete,do,else,export,extends,finally,for,function,if,' +
				'import,in,instanceof,let,return,super,switch,throw,try,' +
				'var,while,with,yield,enum,await,implements,package,' +
				'proctected,static,interface,private,public'
			var improperKeywordsRE =
				new RegExp('^(' + improperKeywords.replace(/,/g, '\\b|') + '\\b)')

			var wsRE = /\s/g
			var newlineRE = /\n/g
			var saveRE = /[\{,]\s*[\w\$_]+\s*:|('[^']*'|"[^"]*")|new |typeof |void /g
			var restoreRE = /"(\d+)"/g
			var pathTestRE = /^[A-Za-z_$][\w$]*(\.[A-Za-z_$][\w$]*|\['.*?'\]|\[".*?"\]|\[\d+\]|\[[A-Za-z_$][\w$]*\])*$/
			var pathReplaceRE = /[^\w$\.]([A-Za-z_$][\w$]*(\.[A-Za-z_$][\w$]*|\['.*?'\]|\[".*?"\])*)/g
			var booleanLiteralRE = /^(true|false)$/

			/**
			 * Save / Rewrite / Restore
			 *
			 * When rewriting paths found in an expression, it is
			 * possible for the same letter sequences to be found in
			 * strings and Object literal property keys. Therefore we
			 * remove and store these parts in a temporary array, and
			 * restore them after the path rewrite.
			 */

			var saved = []

			/**
			 * Save replacer
			 *
			 * The save regex can match two possible cases:
			 * 1. An opening object literal
			 * 2. A string
			 * If matched as a plain string, we need to escape its
			 * newlines, since the string needs to be preserved when
			 * generating the function body.
			 *
			 * @param {String} str
			 * @param {String} isString - str if matched as a string
			 * @return {String} - placeholder with index
			 */

			function save (str, isString) {
				var i = saved.length
				saved[i] = isString
					? str.replace(newlineRE, '\\n')
					: str
				return '"' + i + '"'
			}

			/**
			 * Path rewrite replacer
			 *
			 * @param {String} raw
			 * @return {String}
			 */

			function rewrite (raw) {
				var c = raw.charAt(0)
				var path = raw.slice(1)
				if (allowedKeywordsRE.test(path)) {
					return raw
				} else {
					path = path.indexOf('"') > -1
						? path.replace(restoreRE, restore)
						: path
					return c + 'scope.' + path
				}
			}

			/**
			 * Restore replacer
			 *
			 * @param {String} str
			 * @param {String} i - matched save index
			 * @return {String}
			 */

			function restore (str, i) {
				return saved[i]
			}

			/**
			 * Rewrite an expression, prefixing all path accessors with
			 * `scope.` and generate getter/setter functions.
			 *
			 * @param {String} exp
			 * @param {Boolean} needSet
			 * @return {Function}
			 */

			function compileExpFns (exp, needSet) {
				if (improperKeywordsRE.test(exp)) {
					("development") !== 'production' && _.warn(
						'Avoid using reserved keywords in expression: ' + exp
					)
				}
				// reset state
				saved.length = 0
				// save strings and object literal keys
				var body = exp
					.replace(saveRE, save)
					.replace(wsRE, '')
				// rewrite all paths
				// pad 1 space here becaue the regex matches 1 extra char
				body = (' ' + body)
					.replace(pathReplaceRE, rewrite)
					.replace(restoreRE, restore)
				var getter = makeGetter(body)
				if (getter) {
					return {
						get: getter,
						body: body,
						set: needSet
							? makeSetter(body)
							: null
					}
				}
			}

			/**
			 * Compile getter setters for a simple path.
			 *
			 * @param {String} exp
			 * @return {Function}
			 */

			function compilePathFns (exp) {
				var getter, path
				if (exp.indexOf('[') < 0) {
					// really simple path
					path = exp.split('.')
					path.raw = exp
					getter = Path.compileGetter(path)
				} else {
					// do the real parsing
					path = Path.parse(exp)
					getter = path.get
				}
				return {
					get: getter,
					// always generate setter for simple paths
					set: function (obj, val) {
						Path.set(obj, path, val)
					}
				}
			}

			/**
			 * Build a getter function. Requires eval.
			 *
			 * We isolate the try/catch so it doesn't affect the
			 * optimization of the parse function when it is not called.
			 *
			 * @param {String} body
			 * @return {Function|undefined}
			 */

			function makeGetter (body) {
				try {
					return new Function('scope', 'return ' + body + ';')
				} catch (e) {
					("development") !== 'production' && _.warn(
						'Invalid expression. ' +
						'Generated function body: ' + body
					)
				}
			}

			/**
			 * Build a setter function.
			 *
			 * This is only needed in rare situations like "a[b]" where
			 * a settable path requires dynamic evaluation.
			 *
			 * This setter function may throw error when called if the
			 * expression body is not a valid left-hand expression in
			 * assignment.
			 *
			 * @param {String} body
			 * @return {Function|undefined}
			 */

			function makeSetter (body) {
				try {
					return new Function('scope', 'value', body + '=value;')
				} catch (e) {
					("development") !== 'production' && _.warn(
						'Invalid setter function body: ' + body
					)
				}
			}

			/**
			 * Check for setter existence on a cache hit.
			 *
			 * @param {Function} hit
			 */

			function checkSetter (hit) {
				if (!hit.set) {
					hit.set = makeSetter(hit.body)
				}
			}

			/**
			 * Parse an expression into re-written getter/setters.
			 *
			 * @param {String} exp
			 * @param {Boolean} needSet
			 * @return {Function}
			 */

			exports.parse = function (exp, needSet) {
				exp = exp.trim()
				// try cache
				var hit = expressionCache.get(exp)
				if (hit) {
					if (needSet) {
						checkSetter(hit)
					}
					return hit
				}
				// we do a simple path check to optimize for them.
				// the check fails valid paths with unusal whitespaces,
				// but that's too rare and we don't care.
				// also skip boolean literals and paths that start with
				// global "Math"
				var res = exports.isSimplePath(exp)
					? compilePathFns(exp)
					: compileExpFns(exp, needSet)
				expressionCache.put(exp, res)
				return res
			}

			/**
			 * Check if an expression is a simple path.
			 *
			 * @param {String} exp
			 * @return {Boolean}
			 */

			exports.isSimplePath = function (exp) {
				return pathTestRE.test(exp) &&
						// don't treat true/false as paths
					!booleanLiteralRE.test(exp) &&
						// Math constants e.g. Math.PI, Math.E etc.
					exp.slice(0, 5) !== 'Math.'
			}


			/***/ },
		/* 43 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var Cache = __webpack_require__(7)
			var pathCache = new Cache(1000)
			var identRE = exports.identRE = /^[$_a-zA-Z]+[\w$]*$/

			// actions
			var APPEND = 0
			var PUSH = 1

			// states
			var BEFORE_PATH = 0
			var IN_PATH = 1
			var BEFORE_IDENT = 2
			var IN_IDENT = 3
			var BEFORE_ELEMENT = 4
			var AFTER_ZERO = 5
			var IN_INDEX = 6
			var IN_SINGLE_QUOTE = 7
			var IN_DOUBLE_QUOTE = 8
			var IN_SUB_PATH = 9
			var AFTER_ELEMENT = 10
			var AFTER_PATH = 11
			var ERROR = 12

			var pathStateMachine = []

			pathStateMachine[BEFORE_PATH] = {
				'ws': [BEFORE_PATH],
				'ident': [IN_IDENT, APPEND],
				'[': [BEFORE_ELEMENT],
				'eof': [AFTER_PATH]
			}

			pathStateMachine[IN_PATH] = {
				'ws': [IN_PATH],
				'.': [BEFORE_IDENT],
				'[': [BEFORE_ELEMENT],
				'eof': [AFTER_PATH]
			}

			pathStateMachine[BEFORE_IDENT] = {
				'ws': [BEFORE_IDENT],
				'ident': [IN_IDENT, APPEND]
			}

			pathStateMachine[IN_IDENT] = {
				'ident': [IN_IDENT, APPEND],
				'0': [IN_IDENT, APPEND],
				'number': [IN_IDENT, APPEND],
				'ws': [IN_PATH, PUSH],
				'.': [BEFORE_IDENT, PUSH],
				'[': [BEFORE_ELEMENT, PUSH],
				'eof': [AFTER_PATH, PUSH]
			}

			pathStateMachine[BEFORE_ELEMENT] = {
				'ws': [BEFORE_ELEMENT],
				'0': [AFTER_ZERO, APPEND],
				'number': [IN_INDEX, APPEND],
				"'": [IN_SINGLE_QUOTE, APPEND, ''],
				'"': [IN_DOUBLE_QUOTE, APPEND, ''],
				'ident': [IN_SUB_PATH, APPEND, '*']
			}

			pathStateMachine[AFTER_ZERO] = {
				'ws': [AFTER_ELEMENT, PUSH],
				']': [IN_PATH, PUSH]
			}

			pathStateMachine[IN_INDEX] = {
				'0': [IN_INDEX, APPEND],
				'number': [IN_INDEX, APPEND],
				'ws': [AFTER_ELEMENT],
				']': [IN_PATH, PUSH]
			}

			pathStateMachine[IN_SINGLE_QUOTE] = {
				"'": [AFTER_ELEMENT],
				'eof': ERROR,
				'else': [IN_SINGLE_QUOTE, APPEND]
			}

			pathStateMachine[IN_DOUBLE_QUOTE] = {
				'"': [AFTER_ELEMENT],
				'eof': ERROR,
				'else': [IN_DOUBLE_QUOTE, APPEND]
			}

			pathStateMachine[IN_SUB_PATH] = {
				'ident': [IN_SUB_PATH, APPEND],
				'0': [IN_SUB_PATH, APPEND],
				'number': [IN_SUB_PATH, APPEND],
				'ws': [AFTER_ELEMENT],
				']': [IN_PATH, PUSH]
			}

			pathStateMachine[AFTER_ELEMENT] = {
				'ws': [AFTER_ELEMENT],
				']': [IN_PATH, PUSH]
			}

			/**
			 * Determine the type of a character in a keypath.
			 *
			 * @param {Char} ch
			 * @return {String} type
			 */

			function getPathCharType (ch) {
				if (ch === undefined) {
					return 'eof'
				}

				var code = ch.charCodeAt(0)

				switch (code) {
					case 0x5B: // [
					case 0x5D: // ]
					case 0x2E: // .
					case 0x22: // "
					case 0x27: // '
					case 0x30: // 0
						return ch

					case 0x5F: // _
					case 0x24: // $
						return 'ident'

					case 0x20: // Space
					case 0x09: // Tab
					case 0x0A: // Newline
					case 0x0D: // Return
					case 0xA0:  // No-break space
					case 0xFEFF:  // Byte Order Mark
					case 0x2028:  // Line Separator
					case 0x2029:  // Paragraph Separator
						return 'ws'
				}

				// a-z, A-Z
				if (
					(code >= 0x61 && code <= 0x7A) ||
					(code >= 0x41 && code <= 0x5A)
				) {
					return 'ident'
				}

				// 1-9
				if (code >= 0x31 && code <= 0x39) {
					return 'number'
				}

				return 'else'
			}

			/**
			 * Parse a string path into an array of segments
			 *
			 * @param {String} path
			 * @return {Array|undefined}
			 */

			function parsePath (path) {
				var keys = []
				var index = -1
				var mode = BEFORE_PATH
				var c, newChar, key, type, transition, action, typeMap

				var actions = []
				actions[PUSH] = function () {
					if (key === undefined) {
						return
					}
					keys.push(key)
					key = undefined
				}
				actions[APPEND] = function () {
					if (key === undefined) {
						key = newChar
					} else {
						key += newChar
					}
				}

				function maybeUnescapeQuote () {
					var nextChar = path[index + 1]
					if ((mode === IN_SINGLE_QUOTE && nextChar === "'") ||
						(mode === IN_DOUBLE_QUOTE && nextChar === '"')) {
						index++
						newChar = nextChar
						actions[APPEND]()
						return true
					}
				}

				while (mode != null) {
					index++
					c = path[index]

					if (c === '\\' && maybeUnescapeQuote()) {
						continue
					}

					type = getPathCharType(c)
					typeMap = pathStateMachine[mode]
					transition = typeMap[type] || typeMap['else'] || ERROR

					if (transition === ERROR) {
						return // parse error
					}

					mode = transition[0]
					action = actions[transition[1]]
					if (action) {
						newChar = transition[2]
						newChar = newChar === undefined
							? c
							: newChar === '*'
							? newChar + c
							: newChar
						action()
					}

					if (mode === AFTER_PATH) {
						keys.raw = path
						return keys
					}
				}
			}

			/**
			 * Format a accessor segment based on its type.
			 *
			 * @param {String} key
			 * @return {Boolean}
			 */

			function formatAccessor (key) {
				if (identRE.test(key)) { // identifier
					return '.' + key
				} else if (+key === key >>> 0) { // bracket index
					return '[' + key + ']'
				} else if (key.charAt(0) === '*') {
					return '[o' + formatAccessor(key.slice(1)) + ']'
				} else { // bracket string
					return '["' + key.replace(/"/g, '\\"') + '"]'
				}
			}

			/**
			 * Compiles a getter function with a fixed path.
			 * The fixed path getter supresses errors.
			 *
			 * @param {Array} path
			 * @return {Function}
			 */

			exports.compileGetter = function (path) {
				var body = 'return o' + path.map(formatAccessor).join('')
				return new Function('o', body)
			}

			/**
			 * External parse that check for a cache hit first
			 *
			 * @param {String} path
			 * @return {Array|undefined}
			 */

			exports.parse = function (path) {
				var hit = pathCache.get(path)
				if (!hit) {
					hit = parsePath(path)
					if (hit) {
						hit.get = exports.compileGetter(hit)
						pathCache.put(path, hit)
					}
				}
				return hit
			}

			/**
			 * Get from an object from a path string
			 *
			 * @param {Object} obj
			 * @param {String} path
			 */

			exports.get = function (obj, path) {
				path = exports.parse(path)
				if (path) {
					return path.get(obj)
				}
			}

			/**
			 * Warn against setting non-existent root path on a vm.
			 */

			var warnNonExistent
			if (true) {
				warnNonExistent = function (path) {
					_.warn(
						'You are setting a non-existent path "' + path.raw + '" ' +
						'on a vm instance. Consider pre-initializing the property ' +
						'with the "data" option for more reliable reactivity ' +
						'and better performance.'
					)
				}
			}

			/**
			 * Set on an object from a path
			 *
			 * @param {Object} obj
			 * @param {String | Array} path
			 * @param {*} val
			 */

			exports.set = function (obj, path, val) {
				var original = obj
				if (typeof path === 'string') {
					path = exports.parse(path)
				}
				if (!path || !_.isObject(obj)) {
					return false
				}
				var last, key
				for (var i = 0, l = path.length; i < l; i++) {
					last = obj
					key = path[i]
					if (key.charAt(0) === '*') {
						key = original[key.slice(1)]
					}
					if (i < l - 1) {
						obj = obj[key]
						if (!_.isObject(obj)) {
							obj = {}
							if (("development") !== 'production' && last._isVue) {
								warnNonExistent(path)
							}
							_.set(last, key, obj)
						}
					} else {
						if (_.isArray(obj)) {
							obj.$set(key, val)
						} else if (key in obj) {
							obj[key] = val
						} else {
							if (("development") !== 'production' && obj._isVue) {
								warnNonExistent(path)
							}
							_.set(obj, key, val)
						}
					}
				}
				return true
			}


			/***/ },
		/* 44 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var config = __webpack_require__(5)

			// we have two separate queues: one for directive updates
			// and one for user watcher registered via $watch().
			// we want to guarantee directive updates to be called
			// before user watchers so that when user watchers are
			// triggered, the DOM would have already been in updated
			// state.
			var queue = []
			var userQueue = []
			var has = {}
			var circular = {}
			var waiting = false
			var internalQueueDepleted = false

			/**
			 * Reset the batcher's state.
			 */

			function resetBatcherState () {
				queue = []
				userQueue = []
				has = {}
				circular = {}
				waiting = internalQueueDepleted = false
			}

			/**
			 * Flush both queues and run the watchers.
			 */

			function flushBatcherQueue () {
				runBatcherQueue(queue)
				internalQueueDepleted = true
				runBatcherQueue(userQueue)
				// dev tool hook
				/* istanbul ignore if */
				if (true) {
					if (_.inBrowser && window.__VUE_DEVTOOLS_GLOBAL_HOOK__) {
						window.__VUE_DEVTOOLS_GLOBAL_HOOK__.emit('flush')
					}
				}
				resetBatcherState()
			}

			/**
			 * Run the watchers in a single queue.
			 *
			 * @param {Array} queue
			 */

			function runBatcherQueue (queue) {
				// do not cache length because more watchers might be pushed
				// as we run existing watchers
				for (var i = 0; i < queue.length; i++) {
					var watcher = queue[i]
					var id = watcher.id
					has[id] = null
					watcher.run()
					// in dev build, check and stop circular updates.
					if (("development") !== 'production' && has[id] != null) {
						circular[id] = (circular[id] || 0) + 1
						if (circular[id] > config._maxUpdateCount) {
							queue.splice(has[id], 1)
							_.warn(
								'You may have an infinite update loop for watcher ' +
								'with expression: ' + watcher.expression
							)
						}
					}
				}
			}

			/**
			 * Push a watcher into the watcher queue.
			 * Jobs with duplicate IDs will be skipped unless it's
			 * pushed when the queue is being flushed.
			 *
			 * @param {Watcher} watcher
			 *   properties:
			 *   - {Number} id
			 *   - {Function} run
			 */

			exports.push = function (watcher) {
				var id = watcher.id
				if (has[id] == null) {
					// if an internal watcher is pushed, but the internal
					// queue is already depleted, we run it immediately.
					if (internalQueueDepleted && !watcher.user) {
						watcher.run()
						return
					}
					// push watcher into appropriate queue
					var q = watcher.user ? userQueue : queue
					has[id] = q.length
					q.push(watcher)
					// queue the flush
					if (!waiting) {
						waiting = true
						_.nextTick(flushBatcherQueue)
					}
				}
			}


			/***/ },
		/* 45 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var Transition = __webpack_require__(46)

			module.exports = {

				priority: 1000,

				update: function (id, oldId) {
					var el = this.el
					// resolve on owner vm
					var hooks = _.resolveAsset(this.vm.$options, 'transitions', id)
					id = id || 'v'
					// apply on closest vm
					el.__v_trans = new Transition(el, id, hooks, this.el.__vue__ || this.vm)
					if (oldId) {
						_.removeClass(el, oldId + '-transition')
					}
					_.addClass(el, id + '-transition')
				}
			}


			/***/ },
		/* 46 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var queue = __webpack_require__(47)
			var addClass = _.addClass
			var removeClass = _.removeClass
			var transitionEndEvent = _.transitionEndEvent
			var animationEndEvent = _.animationEndEvent
			var transDurationProp = _.transitionProp + 'Duration'
			var animDurationProp = _.animationProp + 'Duration'

			var TYPE_TRANSITION = 1
			var TYPE_ANIMATION = 2

			/**
			 * A Transition object that encapsulates the state and logic
			 * of the transition.
			 *
			 * @param {Element} el
			 * @param {String} id
			 * @param {Object} hooks
			 * @param {Vue} vm
			 */

			function Transition (el, id, hooks, vm) {
				this.id = id
				this.el = el
				this.enterClass = id + '-enter'
				this.leaveClass = id + '-leave'
				this.hooks = hooks
				this.vm = vm
				// async state
				this.pendingCssEvent =
					this.pendingCssCb =
						this.cancel =
							this.pendingJsCb =
								this.op =
									this.cb = null
				this.justEntered = false
				this.entered = this.left = false
				this.typeCache = {}
				// bind
				var self = this
					;['enterNextTick', 'enterDone', 'leaveNextTick', 'leaveDone']
					.forEach(function (m) {
						self[m] = _.bind(self[m], self)
					})
			}

			var p = Transition.prototype

			/**
			 * Start an entering transition.
			 *
			 * 1. enter transition triggered
			 * 2. call beforeEnter hook
			 * 3. add enter class
			 * 4. insert/show element
			 * 5. call enter hook (with possible explicit js callback)
			 * 6. reflow
			 * 7. based on transition type:
			 *    - transition:
			 *        remove class now, wait for transitionend,
			 *        then done if there's no explicit js callback.
			 *    - animation:
			 *        wait for animationend, remove class,
			 *        then done if there's no explicit js callback.
			 *    - no css transition:
			 *        done now if there's no explicit js callback.
			 * 8. wait for either done or js callback, then call
			 *    afterEnter hook.
			 *
			 * @param {Function} op - insert/show the element
			 * @param {Function} [cb]
			 */

			p.enter = function (op, cb) {
				this.cancelPending()
				this.callHook('beforeEnter')
				this.cb = cb
				addClass(this.el, this.enterClass)
				op()
				this.entered = false
				this.callHookWithCb('enter')
				if (this.entered) {
					return // user called done synchronously.
				}
				this.cancel = this.hooks && this.hooks.enterCancelled
				queue.push(this.enterNextTick)
			}

			/**
			 * The "nextTick" phase of an entering transition, which is
			 * to be pushed into a queue and executed after a reflow so
			 * that removing the class can trigger a CSS transition.
			 */

			p.enterNextTick = function () {

				// Important hack:
				// in Chrome, if a just-entered element is applied the
				// leave class while its interpolated property still has
				// a very small value (within one frame), Chrome will
				// skip the leave transition entirely and not firing the
				// transtionend event. Therefore we need to protected
				// against such cases using a one-frame timeout.
				this.justEntered = true
				var self = this
				setTimeout(function () {
					self.justEntered = false
				}, 17)

				var enterDone = this.enterDone
				var type = this.getCssTransitionType(this.enterClass)
				if (!this.pendingJsCb) {
					if (type === TYPE_TRANSITION) {
						// trigger transition by removing enter class now
						removeClass(this.el, this.enterClass)
						this.setupCssCb(transitionEndEvent, enterDone)
					} else if (type === TYPE_ANIMATION) {
						this.setupCssCb(animationEndEvent, enterDone)
					} else {
						enterDone()
					}
				} else if (type === TYPE_TRANSITION) {
					removeClass(this.el, this.enterClass)
				}
			}

			/**
			 * The "cleanup" phase of an entering transition.
			 */

			p.enterDone = function () {
				this.entered = true
				this.cancel = this.pendingJsCb = null
				removeClass(this.el, this.enterClass)
				this.callHook('afterEnter')
				if (this.cb) this.cb()
			}

			/**
			 * Start a leaving transition.
			 *
			 * 1. leave transition triggered.
			 * 2. call beforeLeave hook
			 * 3. add leave class (trigger css transition)
			 * 4. call leave hook (with possible explicit js callback)
			 * 5. reflow if no explicit js callback is provided
			 * 6. based on transition type:
			 *    - transition or animation:
			 *        wait for end event, remove class, then done if
			 *        there's no explicit js callback.
			 *    - no css transition:
			 *        done if there's no explicit js callback.
			 * 7. wait for either done or js callback, then call
			 *    afterLeave hook.
			 *
			 * @param {Function} op - remove/hide the element
			 * @param {Function} [cb]
			 */

			p.leave = function (op, cb) {
				this.cancelPending()
				this.callHook('beforeLeave')
				this.op = op
				this.cb = cb
				addClass(this.el, this.leaveClass)
				this.left = false
				this.callHookWithCb('leave')
				if (this.left) {
					return // user called done synchronously.
				}
				this.cancel = this.hooks && this.hooks.leaveCancelled
				// only need to handle leaveDone if
				// 1. the transition is already done (synchronously called
				//    by the user, which causes this.op set to null)
				// 2. there's no explicit js callback
				if (this.op && !this.pendingJsCb) {
					// if a CSS transition leaves immediately after enter,
					// the transitionend event never fires. therefore we
					// detect such cases and end the leave immediately.
					if (this.justEntered) {
						this.leaveDone()
					} else {
						queue.push(this.leaveNextTick)
					}
				}
			}

			/**
			 * The "nextTick" phase of a leaving transition.
			 */

			p.leaveNextTick = function () {
				var type = this.getCssTransitionType(this.leaveClass)
				if (type) {
					var event = type === TYPE_TRANSITION
						? transitionEndEvent
						: animationEndEvent
					this.setupCssCb(event, this.leaveDone)
				} else {
					this.leaveDone()
				}
			}

			/**
			 * The "cleanup" phase of a leaving transition.
			 */

			p.leaveDone = function () {
				this.left = true
				this.cancel = this.pendingJsCb = null
				this.op()
				removeClass(this.el, this.leaveClass)
				this.callHook('afterLeave')
				if (this.cb) this.cb()
				this.op = null
			}

			/**
			 * Cancel any pending callbacks from a previously running
			 * but not finished transition.
			 */

			p.cancelPending = function () {
				this.op = this.cb = null
				var hasPending = false
				if (this.pendingCssCb) {
					hasPending = true
					_.off(this.el, this.pendingCssEvent, this.pendingCssCb)
					this.pendingCssEvent = this.pendingCssCb = null
				}
				if (this.pendingJsCb) {
					hasPending = true
					this.pendingJsCb.cancel()
					this.pendingJsCb = null
				}
				if (hasPending) {
					removeClass(this.el, this.enterClass)
					removeClass(this.el, this.leaveClass)
				}
				if (this.cancel) {
					this.cancel.call(this.vm, this.el)
					this.cancel = null
				}
			}

			/**
			 * Call a user-provided synchronous hook function.
			 *
			 * @param {String} type
			 */

			p.callHook = function (type) {
				if (this.hooks && this.hooks[type]) {
					this.hooks[type].call(this.vm, this.el)
				}
			}

			/**
			 * Call a user-provided, potentially-async hook function.
			 * We check for the length of arguments to see if the hook
			 * expects a `done` callback. If true, the transition's end
			 * will be determined by when the user calls that callback;
			 * otherwise, the end is determined by the CSS transition or
			 * animation.
			 *
			 * @param {String} type
			 */

			p.callHookWithCb = function (type) {
				var hook = this.hooks && this.hooks[type]
				if (hook) {
					if (hook.length > 1) {
						this.pendingJsCb = _.cancellable(this[type + 'Done'])
					}
					hook.call(this.vm, this.el, this.pendingJsCb)
				}
			}

			/**
			 * Get an element's transition type based on the
			 * calculated styles.
			 *
			 * @param {String} className
			 * @return {Number}
			 */

			p.getCssTransitionType = function (className) {
				/* istanbul ignore if */
				if (
					!transitionEndEvent ||
						// skip CSS transitions if page is not visible -
						// this solves the issue of transitionend events not
						// firing until the page is visible again.
						// pageVisibility API is supported in IE10+, same as
						// CSS transitions.
					document.hidden ||
						// explicit js-only transition
					(this.hooks && this.hooks.css === false) ||
						// element is hidden
					isHidden(this.el)
				) {
					return
				}
				var type = this.typeCache[className]
				if (type) return type
				var inlineStyles = this.el.style
				var computedStyles = window.getComputedStyle(this.el)
				var transDuration =
					inlineStyles[transDurationProp] ||
					computedStyles[transDurationProp]
				if (transDuration && transDuration !== '0s') {
					type = TYPE_TRANSITION
				} else {
					var animDuration =
						inlineStyles[animDurationProp] ||
						computedStyles[animDurationProp]
					if (animDuration && animDuration !== '0s') {
						type = TYPE_ANIMATION
					}
				}
				if (type) {
					this.typeCache[className] = type
				}
				return type
			}

			/**
			 * Setup a CSS transitionend/animationend callback.
			 *
			 * @param {String} event
			 * @param {Function} cb
			 */

			p.setupCssCb = function (event, cb) {
				this.pendingCssEvent = event
				var self = this
				var el = this.el
				var onEnd = this.pendingCssCb = function (e) {
					if (e.target === el) {
						_.off(el, event, onEnd)
						self.pendingCssEvent = self.pendingCssCb = null
						if (!self.pendingJsCb && cb) {
							cb()
						}
					}
				}
				_.on(el, event, onEnd)
			}

			/**
			 * Check if an element is hidden - in that case we can just
			 * skip the transition alltogether.
			 *
			 * @param {Element} el
			 * @return {Boolean}
			 */

			function isHidden (el) {
				return !(
					el.offsetWidth &&
					el.offsetHeight &&
					el.getClientRects().length
				)
			}

			module.exports = Transition


			/***/ },
		/* 47 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var queue = []
			var queued = false

			/**
			 * Push a job into the queue.
			 *
			 * @param {Function} job
			 */

			exports.push = function (job) {
				queue.push(job)
				if (!queued) {
					queued = true
					_.nextTick(flush)
				}
			}

			/**
			 * Flush the queue, and do one forced reflow before
			 * triggering transitions.
			 */

			function flush () {
				// Force layout
				var f = document.documentElement.offsetHeight
				for (var i = 0; i < queue.length; i++) {
					queue[i]()
				}
				queue = []
				queued = false
				// dummy return, so js linters don't complain about
				// unused variable f
				return f
			}


			/***/ },
		/* 48 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var dirParser = __webpack_require__(8)
			var propDef = __webpack_require__(39)
			var propBindingModes = __webpack_require__(5)._propBindingModes
			var empty = {}

			// regexes
			var identRE = __webpack_require__(43).identRE
			var settablePathRE = /^[A-Za-z_$][\w$]*(\.[A-Za-z_$][\w$]*|\[[^\[\]]+\])*$/

			/**
			 * Compile props on a root element and return
			 * a props link function.
			 *
			 * @param {Element|DocumentFragment} el
			 * @param {Array} propOptions
			 * @return {Function} propsLinkFn
			 */

			module.exports = function compileProps (el, propOptions) {
				var props = []
				var names = Object.keys(propOptions)
				var i = names.length
				var options, name, attr, value, path, parsed, prop, isTitleBinding
				while (i--) {
					name = names[i]
					options = propOptions[name] || empty

					if (("development") !== 'production' && name === '$data') {
						_.warn('Do not use $data as prop.')
						continue
					}

					// props could contain dashes, which will be
					// interpreted as minus calculations by the parser
					// so we need to camelize the path here
					path = _.camelize(name)
					if (!identRE.test(path)) {
						("development") !== 'production' && _.warn(
							'Invalid prop key: "' + name + '". Prop keys ' +
							'must be valid identifiers.'
						)
						continue
					}

					prop = {
						name: name,
						path: path,
						options: options,
						mode: propBindingModes.ONE_WAY
					}

					// IE title issues
					isTitleBinding = false
					if (name === 'title' && (el.getAttribute(':title') || el.getAttribute('v-bind:title'))) {
						isTitleBinding = true
					}

					// first check literal version
					attr = _.hyphenate(name)
					value = prop.raw = _.attr(el, attr)
					if (value === null || isTitleBinding) {
						// then check dynamic version
						if ((value = _.getBindAttr(el, attr)) === null) {
							if ((value = _.getBindAttr(el, attr + '.sync')) !== null) {
								prop.mode = propBindingModes.TWO_WAY
							} else if ((value = _.getBindAttr(el, attr + '.once')) !== null) {
								prop.mode = propBindingModes.ONE_TIME
							}
						}
						prop.raw = value
						if (value !== null) {
							parsed = dirParser.parse(value)
							value = parsed.expression
							prop.filters = parsed.filters
							// check binding type
							if (_.isLiteral(value)) {
								// for expressions containing literal numbers and
								// booleans, there's no need to setup a prop binding,
								// so we can optimize them as a one-time set.
								prop.optimizedLiteral = true
							} else {
								prop.dynamic = true
								// check non-settable path for two-way bindings
								if (("development") !== 'production' &&
									prop.mode === propBindingModes.TWO_WAY &&
									!settablePathRE.test(value)) {
									prop.mode = propBindingModes.ONE_WAY
									_.warn(
										'Cannot bind two-way prop with non-settable ' +
										'parent path: ' + value
									)
								}
							}
							prop.parentPath = value

							// warn required two-way
							if (
								("development") !== 'production' &&
								options.twoWay &&
								prop.mode !== propBindingModes.TWO_WAY
							) {
								_.warn(
									'Prop "' + name + '" expects a two-way binding type.'
								)
							}

						} else if (options.required) {
							// warn missing required
							("development") !== 'production' && _.warn(
								'Missing required prop: ' + name
							)
						}
					}

					// push prop
					props.push(prop)
				}
				return makePropsLinkFn(props)
			}

			/**
			 * Build a function that applies props to a vm.
			 *
			 * @param {Array} props
			 * @return {Function} propsLinkFn
			 */

			function makePropsLinkFn (props) {
				return function propsLinkFn (vm, scope) {
					// store resolved props info
					vm._props = {}
					var i = props.length
					var prop, path, options, value, raw
					while (i--) {
						prop = props[i]
						raw = prop.raw
						path = prop.path
						options = prop.options
						vm._props[path] = prop
						if (raw === null) {
							// initialize absent prop
							_.initProp(vm, prop, getDefault(vm, options))
						} else if (prop.dynamic) {
							// dynamic prop
							if (vm._context) {
								if (prop.mode === propBindingModes.ONE_TIME) {
									// one time binding
									value = (scope || vm._context).$get(prop.parentPath)
									_.initProp(vm, prop, value)
								} else {
									// dynamic binding
									vm._bindDir({
										name: 'prop',
										def: propDef,
										prop: prop
									}, null, null, scope) // el, host, scope
								}
							} else {
								("development") !== 'production' && _.warn(
									'Cannot bind dynamic prop on a root instance' +
									' with no parent: ' + prop.name + '="' +
									raw + '"'
								)
							}
						} else if (prop.optimizedLiteral) {
							// optimized literal, cast it and just set once
							raw = _.stripQuotes(raw)
							value = _.toBoolean(_.toNumber(raw))
							_.initProp(vm, prop, value)
						} else {
							// string literal, but we need to cater for
							// Boolean props with no value
							value = options.type === Boolean && raw === ''
								? true
								: raw
							_.initProp(vm, prop, value)
						}
					}
				}
			}

			/**
			 * Get the default value of a prop.
			 *
			 * @param {Vue} vm
			 * @param {Object} options
			 * @return {*}
			 */

			function getDefault (vm, options) {
				// no default, return undefined
				if (!options.hasOwnProperty('default')) {
					// absent boolean value defaults to false
					return options.type === Boolean
						? false
						: undefined
				}
				var def = options.default
				// warn against non-factory defaults for Object & Array
				if (_.isObject(def)) {
					("development") !== 'production' && _.warn(
						'Object/Array as default prop values will be shared ' +
						'across multiple instances. Use a factory function ' +
						'to return the default value instead.'
					)
				}
				// call factory function for non-Function types
				return typeof def === 'function' && options.type !== Function
					? def.call(vm)
					: def
			}


			/***/ },
		/* 49 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var templateParser = __webpack_require__(19)
			var specialCharRE = /[^\w\-:\.]/

			/**
			 * Process an element or a DocumentFragment based on a
			 * instance option object. This allows us to transclude
			 * a template node/fragment before the instance is created,
			 * so the processed fragment can then be cloned and reused
			 * in v-for.
			 *
			 * @param {Element} el
			 * @param {Object} options
			 * @return {Element|DocumentFragment}
			 */

			exports.transclude = function (el, options) {
				// extract container attributes to pass them down
				// to compiler, because they need to be compiled in
				// parent scope. we are mutating the options object here
				// assuming the same object will be used for compile
				// right after this.
				if (options) {
					options._containerAttrs = extractAttrs(el)
				}
				// for template tags, what we want is its content as
				// a documentFragment (for fragment instances)
				if (_.isTemplate(el)) {
					el = templateParser.parse(el)
				}
				if (options) {
					if (options._asComponent && !options.template) {
						options.template = '<slot></slot>'
					}
					if (options.template) {
						options._content = _.extractContent(el)
						el = transcludeTemplate(el, options)
					}
				}
				if (el instanceof DocumentFragment) {
					// anchors for fragment instance
					// passing in `persist: true` to avoid them being
					// discarded by IE during template cloning
					_.prepend(_.createAnchor('v-start', true), el)
					el.appendChild(_.createAnchor('v-end', true))
				}
				return el
			}

			/**
			 * Process the template option.
			 * If the replace option is true this will swap the $el.
			 *
			 * @param {Element} el
			 * @param {Object} options
			 * @return {Element|DocumentFragment}
			 */

			function transcludeTemplate (el, options) {
				var template = options.template
				var frag = templateParser.parse(template, true)
				if (frag) {
					var replacer = frag.firstChild
					var tag = replacer.tagName && replacer.tagName.toLowerCase()
					if (options.replace) {
						/* istanbul ignore if */
						if (el === document.body) {
							("development") !== 'production' && _.warn(
								'You are mounting an instance with a template to ' +
								'<body>. This will replace <body> entirely. You ' +
								'should probably use `replace: false` here.'
							)
						}
						// there are many cases where the instance must
						// become a fragment instance: basically anything that
						// can create more than 1 root nodes.
						if (
							// multi-children template
						frag.childNodes.length > 1 ||
							// non-element template
						replacer.nodeType !== 1 ||
							// single nested component
						tag === 'component' ||
						_.resolveAsset(options, 'components', tag) ||
						replacer.hasAttribute('is') ||
						replacer.hasAttribute(':is') ||
						replacer.hasAttribute('v-bind:is') ||
							// element directive
						_.resolveAsset(options, 'elementDirectives', tag) ||
							// for block
						replacer.hasAttribute('v-for') ||
							// if block
						replacer.hasAttribute('v-if')
						) {
							return frag
						} else {
							options._replacerAttrs = extractAttrs(replacer)
							mergeAttrs(el, replacer)
							return replacer
						}
					} else {
						el.appendChild(frag)
						return el
					}
				} else {
					("development") !== 'production' && _.warn(
						'Invalid template option: ' + template
					)
				}
			}

			/**
			 * Helper to extract a component container's attributes
			 * into a plain object array.
			 *
			 * @param {Element} el
			 * @return {Array}
			 */

			function extractAttrs (el) {
				if (el.nodeType === 1 && el.hasAttributes()) {
					return _.toArray(el.attributes)
				}
			}

			/**
			 * Merge the attributes of two elements, and make sure
			 * the class names are merged properly.
			 *
			 * @param {Element} from
			 * @param {Element} to
			 */

			function mergeAttrs (from, to) {
				var attrs = from.attributes
				var i = attrs.length
				var name, value
				while (i--) {
					name = attrs[i].name
					value = attrs[i].value
					if (!to.hasAttribute(name) && !specialCharRE.test(name)) {
						to.setAttribute(name, value)
					} else if (name === 'class') {
						value = to.getAttribute(name) + ' ' + value
						to.setAttribute(name, value)
					}
				}
			}


			/***/ },
		/* 50 */
		/***/ function(module, exports, __webpack_require__) {

			exports.slot = __webpack_require__(51)
			exports.partial = __webpack_require__(52)


			/***/ },
		/* 51 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var templateParser = __webpack_require__(19)

			// This is the elementDirective that handles <content>
			// transclusions. It relies on the raw content of an
			// instance being stored as `$options._content` during
			// the transclude phase.

			module.exports = {

				priority: 1750,

				params: ['name'],

				bind: function () {
					var host = this.vm
					var raw = host.$options._content
					var content
					if (!raw) {
						this.fallback()
						return
					}
					var context = host._context
					var slotName = this.params.name
					if (!slotName) {
						// Default content
						var self = this
						var compileDefaultContent = function () {
							self.compile(
								extractFragment(raw.childNodes, raw, true),
								context,
								host
							)
						}
						if (!host._isCompiled) {
							// defer until the end of instance compilation,
							// because the default outlet must wait until all
							// other possible outlets with selectors have picked
							// out their contents.
							host.$once('hook:compiled', compileDefaultContent)
						} else {
							compileDefaultContent()
						}
					} else {
						var selector = '[slot="' + slotName + '"]'
						var nodes = raw.querySelectorAll(selector)
						if (nodes.length) {
							content = extractFragment(nodes, raw)
							if (content.hasChildNodes()) {
								this.compile(content, context, host)
							} else {
								this.fallback()
							}
						} else {
							this.fallback()
						}
					}
				},

				fallback: function () {
					this.compile(_.extractContent(this.el, true), this.vm)
				},

				compile: function (content, context, host) {
					if (content && context) {
						var scope = host
							? host._scope
							: this._scope
						this.unlink = context.$compile(
							content, host, scope, this._frag
						)
					}
					if (content) {
						_.replace(this.el, content)
					} else {
						_.remove(this.el)
					}
				},

				unbind: function () {
					if (this.unlink) {
						this.unlink()
					}
				}
			}

			/**
			 * Extract qualified content nodes from a node list.
			 *
			 * @param {NodeList} nodes
			 * @param {Element} parent
			 * @param {Boolean} main
			 * @return {DocumentFragment}
			 */

			function extractFragment (nodes, parent, main) {
				var frag = document.createDocumentFragment()
				for (var i = 0, l = nodes.length; i < l; i++) {
					var node = nodes[i]
					// if this is the main outlet, we want to skip all
					// previously selected nodes;
					// otherwise, we want to mark the node as selected.
					// clone the node so the original raw content remains
					// intact. this ensures proper re-compilation in cases
					// where the outlet is inside a conditional block
					if (main && !node.__v_selected) {
						append(node)
					} else if (!main && node.parentNode === parent) {
						node.__v_selected = true
						append(node)
					}
				}
				return frag

				function append (node) {
					if (_.isTemplate(node) &&
						!node.hasAttribute('v-if') &&
						!node.hasAttribute('v-for')) {
						node = templateParser.parse(node)
					}
					node = templateParser.clone(node)
					frag.appendChild(node)
				}
			}


			/***/ },
		/* 52 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var vIf = __webpack_require__(23)
			var FragmentFactory = __webpack_require__(21)

			module.exports = {

				priority: 1750,

				params: ['name'],

				// watch changes to name for dynamic partials
				paramWatchers: {
					name: function (value) {
						vIf.remove.call(this)
						if (value) {
							this.insert(value)
						}
					}
				},

				bind: function () {
					this.anchor = _.createAnchor('v-partial')
					_.replace(this.el, this.anchor)
					this.insert(this.params.name)
				},

				insert: function (id) {
					var partial = _.resolveAsset(this.vm.$options, 'partials', id)
					if (true) {
						_.assertAsset(partial, 'partial', id)
					}
					if (partial) {
						this.factory = new FragmentFactory(this.vm, partial)
						vIf.insert.call(this)
					}
				},

				unbind: function () {
					if (this.frag) {
						this.frag.destroy()
					}
				}
			}


			/***/ },
		/* 53 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)

			/**
			 * Stringify value.
			 *
			 * @param {Number} indent
			 */

			exports.json = {
				read: function (value, indent) {
					return typeof value === 'string'
						? value
						: JSON.stringify(value, null, Number(indent) || 2)
				},
				write: function (value) {
					try {
						return JSON.parse(value)
					} catch (e) {
						return value
					}
				}
			}

			/**
			 * 'abc' => 'Abc'
			 */

			exports.capitalize = function (value) {
				if (!value && value !== 0) return ''
				value = value.toString()
				return value.charAt(0).toUpperCase() + value.slice(1)
			}

			/**
			 * 'abc' => 'ABC'
			 */

			exports.uppercase = function (value) {
				return (value || value === 0)
					? value.toString().toUpperCase()
					: ''
			}

			/**
			 * 'AbC' => 'abc'
			 */

			exports.lowercase = function (value) {
				return (value || value === 0)
					? value.toString().toLowerCase()
					: ''
			}

			/**
			 * 12345 => $12,345.00
			 *
			 * @param {String} sign
			 */

			var digitsRE = /(\d{3})(?=\d)/g
			exports.currency = function (value, currency) {
				value = parseFloat(value)
				if (!isFinite(value) || (!value && value !== 0)) return ''
				currency = currency != null ? currency : '$'
				var stringified = Math.abs(value).toFixed(2)
				var _int = stringified.slice(0, -3)
				var i = _int.length % 3
				var head = i > 0
					? (_int.slice(0, i) + (_int.length > 3 ? ',' : ''))
					: ''
				var _float = stringified.slice(-3)
				var sign = value < 0 ? '-' : ''
				return currency + sign + head +
					_int.slice(i).replace(digitsRE, '$1,') +
					_float
			}

			/**
			 * 'item' => 'items'
			 *
			 * @params
			 *  an array of strings corresponding to
			 *  the single, double, triple ... forms of the word to
			 *  be pluralized. When the number to be pluralized
			 *  exceeds the length of the args, it will use the last
			 *  entry in the array.
			 *
			 *  e.g. ['single', 'double', 'triple', 'multiple']
			 */

			exports.pluralize = function (value) {
				var args = _.toArray(arguments, 1)
				return args.length > 1
					? (args[value % 10 - 1] || args[args.length - 1])
					: (args[0] + (value === 1 ? '' : 's'))
			}

			/**
			 * Debounce a handler function.
			 *
			 * @param {Function} handler
			 * @param {Number} delay = 300
			 * @return {Function}
			 */

			exports.debounce = function (handler, delay) {
				if (!handler) return
				if (!delay) {
					delay = 300
				}
				return _.debounce(handler, delay)
			}

			/**
			 * Install special array filters
			 */

			_.extend(exports, __webpack_require__(54))


			/***/ },
		/* 54 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var Path = __webpack_require__(43)
			var toArray = __webpack_require__(20)._postProcess

			/**
			 * Limit filter for arrays
			 *
			 * @param {Number} n
			 */

			exports.limitBy = function (arr, n) {
				return typeof n === 'number'
					? arr.slice(0, n)
					: arr
			}

			/**
			 * Filter filter for arrays
			 *
			 * @param {String} searchKey
			 * @param {String} [delimiter]
			 * @param {String} dataKey
			 */

			exports.filterBy = function (arr, search, delimiter /* ...dataKeys */) {
				arr = toArray(arr)
				if (search == null) {
					return arr
				}
				if (typeof search === 'function') {
					return arr.filter(search)
				}
				// cast to lowercase string
				search = ('' + search).toLowerCase()
				// allow optional `in` delimiter
				// because why not
				var n = delimiter === 'in' ? 3 : 2
				// extract and flatten keys
				var keys = _.toArray(arguments, n).reduce(function (prev, cur) {
					return prev.concat(cur)
				}, [])
				var res = []
				var item, key, val, j
				for (var i = 0, l = arr.length; i < l; i++) {
					item = arr[i]
					val = (item && item.$value) || item
					j = keys.length
					if (j) {
						while (j--) {
							key = keys[j]
							if ((key === '$key' && contains(item.$key, search)) ||
								contains(Path.get(val, key), search)) {
								res.push(item)
								break
							}
						}
					} else if (contains(item, search)) {
						res.push(item)
					}
				}
				return res
			}

			/**
			 * Filter filter for arrays
			 *
			 * @param {String} sortKey
			 * @param {String} reverse
			 */

			exports.orderBy = function (arr, sortKey, reverse) {
				arr = toArray(arr)
				if (!sortKey) {
					return arr
				}
				var order = (reverse && reverse < 0) ? -1 : 1
				// sort on a copy to avoid mutating original array
				return arr.slice().sort(function (a, b) {
					if (sortKey !== '$key') {
						if (_.isObject(a) && '$value' in a) a = a.$value
						if (_.isObject(b) && '$value' in b) b = b.$value
					}
					a = _.isObject(a) ? Path.get(a, sortKey) : a
					b = _.isObject(b) ? Path.get(b, sortKey) : b
					return a === b ? 0 : a > b ? order : -order
				})
			}

			/**
			 * String contain helper
			 *
			 * @param {*} val
			 * @param {String} search
			 */

			function contains (val, search) {
				var i
				if (_.isPlainObject(val)) {
					var keys = Object.keys(val)
					i = keys.length
					while (i--) {
						if (contains(val[keys[i]], search)) {
							return true
						}
					}
				} else if (_.isArray(val)) {
					i = val.length
					while (i--) {
						if (contains(val[i], search)) {
							return true
						}
					}
				} else if (val != null) {
					return val.toString().toLowerCase().indexOf(search) > -1
				}
			}


			/***/ },
		/* 55 */
		/***/ function(module, exports, __webpack_require__) {

			var mergeOptions = __webpack_require__(1).mergeOptions
			var uid = 0

			/**
			 * The main init sequence. This is called for every
			 * instance, including ones that are created from extended
			 * constructors.
			 *
			 * @param {Object} options - this options object should be
			 *                           the result of merging class
			 *                           options and the options passed
			 *                           in to the constructor.
			 */

			exports._init = function (options) {

				options = options || {}

				this.$el = null
				this.$parent = options.parent
				this.$root = this.$parent
					? this.$parent.$root
					: this
				this.$children = []
				this.$refs = {}       // child vm references
				this.$els = {}        // element references
				this._watchers = []   // all watchers as an array
				this._directives = [] // all directives

				// a uid
				this._uid = uid++

				// a flag to avoid this being observed
				this._isVue = true

				// events bookkeeping
				this._events = {}            // registered callbacks
				this._eventsCount = {}       // for $broadcast optimization
				this._shouldPropagate = false // for event propagation

				// fragment instance properties
				this._isFragment = false
				this._fragment =         // @type {DocumentFragment}
					this._fragmentStart =    // @type {Text|Comment}
						this._fragmentEnd = null // @type {Text|Comment}

				// lifecycle state
				this._isCompiled =
					this._isDestroyed =
						this._isReady =
							this._isAttached =
								this._isBeingDestroyed = false
				this._unlinkFn = null

				// context:
				// if this is a transcluded component, context
				// will be the common parent vm of this instance
				// and its host.
				this._context = options._context || this.$parent

				// scope:
				// if this is inside an inline v-for, the scope
				// will be the intermediate scope created for this
				// repeat fragment. this is used for linking props
				// and container directives.
				this._scope = options._scope

				// fragment:
				// if this instance is compiled inside a Fragment, it
				// needs to reigster itself as a child of that fragment
				// for attach/detach to work properly.
				this._frag = options._frag
				if (this._frag) {
					this._frag.children.push(this)
				}

				// push self into parent / transclusion host
				if (this.$parent) {
					this.$parent.$children.push(this)
				}

				// set ref
				if (options._ref) {
					(this._scope || this._context).$refs[options._ref] = this
				}

				// merge options.
				options = this.$options = mergeOptions(
					this.constructor.options,
					options,
					this
				)

				// initialize data as empty object.
				// it will be filled up in _initScope().
				this._data = {}

				// call init hook
				this._callHook('init')

				// initialize data observation and scope inheritance.
				this._initState()

				// setup event system and option events.
				this._initEvents()

				// call created hook
				this._callHook('created')

				// if `el` option is passed, start compilation.
				if (options.el) {
					this.$mount(options.el)
				}
			}


			/***/ },
		/* 56 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var inDoc = _.inDoc
			var eventRE = /^v-on:|^@/

			/**
			 * Setup the instance's option events & watchers.
			 * If the value is a string, we pull it from the
			 * instance's methods by name.
			 */

			exports._initEvents = function () {
				var options = this.$options
				if (options._asComponent) {
					registerComponentEvents(this, options.el)
				}
				registerCallbacks(this, '$on', options.events)
				registerCallbacks(this, '$watch', options.watch)
			}

			/**
			 * Register v-on events on a child component
			 *
			 * @param {Vue} vm
			 * @param {Element} el
			 */

			function registerComponentEvents (vm, el) {
				var attrs = el.attributes
				var name, handler
				for (var i = 0, l = attrs.length; i < l; i++) {
					name = attrs[i].name
					if (eventRE.test(name)) {
						name = name.replace(eventRE, '')
						handler = (vm._scope || vm._context).$eval(attrs[i].value, true)
						vm.$on(name.replace(eventRE), handler)
					}
				}
			}

			/**
			 * Register callbacks for option events and watchers.
			 *
			 * @param {Vue} vm
			 * @param {String} action
			 * @param {Object} hash
			 */

			function registerCallbacks (vm, action, hash) {
				if (!hash) return
				var handlers, key, i, j
				for (key in hash) {
					handlers = hash[key]
					if (_.isArray(handlers)) {
						for (i = 0, j = handlers.length; i < j; i++) {
							register(vm, action, key, handlers[i])
						}
					} else {
						register(vm, action, key, handlers)
					}
				}
			}

			/**
			 * Helper to register an event/watch callback.
			 *
			 * @param {Vue} vm
			 * @param {String} action
			 * @param {String} key
			 * @param {Function|String|Object} handler
			 * @param {Object} [options]
			 */

			function register (vm, action, key, handler, options) {
				var type = typeof handler
				if (type === 'function') {
					vm[action](key, handler, options)
				} else if (type === 'string') {
					var methods = vm.$options.methods
					var method = methods && methods[handler]
					if (method) {
						vm[action](key, method, options)
					} else {
						("development") !== 'production' && _.warn(
							'Unknown method: "' + handler + '" when ' +
							'registering callback for ' + action +
							': "' + key + '".'
						)
					}
				} else if (handler && type === 'object') {
					register(vm, action, key, handler.handler, handler)
				}
			}

			/**
			 * Setup recursive attached/detached calls
			 */

			exports._initDOMHooks = function () {
				this.$on('hook:attached', onAttached)
				this.$on('hook:detached', onDetached)
			}

			/**
			 * Callback to recursively call attached hook on children
			 */

			function onAttached () {
				if (!this._isAttached) {
					this._isAttached = true
					this.$children.forEach(callAttach)
				}
			}

			/**
			 * Iterator to call attached hook
			 *
			 * @param {Vue} child
			 */

			function callAttach (child) {
				if (!child._isAttached && inDoc(child.$el)) {
					child._callHook('attached')
				}
			}

			/**
			 * Callback to recursively call detached hook on children
			 */

			function onDetached () {
				if (this._isAttached) {
					this._isAttached = false
					this.$children.forEach(callDetach)
				}
			}

			/**
			 * Iterator to call detached hook
			 *
			 * @param {Vue} child
			 */

			function callDetach (child) {
				if (child._isAttached && !inDoc(child.$el)) {
					child._callHook('detached')
				}
			}

			/**
			 * Trigger all handlers for a hook
			 *
			 * @param {String} hook
			 */

			exports._callHook = function (hook) {
				var handlers = this.$options[hook]
				if (handlers) {
					for (var i = 0, j = handlers.length; i < j; i++) {
						handlers[i].call(this)
					}
				}
				this.$emit('hook:' + hook)
			}


			/***/ },
		/* 57 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var compiler = __webpack_require__(14)
			var Observer = __webpack_require__(58)
			var Dep = __webpack_require__(41)
			var Watcher = __webpack_require__(40)

			/**
			 * Setup the scope of an instance, which contains:
			 * - observed data
			 * - computed properties
			 * - user methods
			 * - meta properties
			 */

			exports._initState = function () {
				this._initProps()
				this._initMeta()
				this._initMethods()
				this._initData()
				this._initComputed()
			}

			/**
			 * Initialize props.
			 */

			exports._initProps = function () {
				var options = this.$options
				var el = options.el
				var props = options.props
				if (props && !el) {
					("development") !== 'production' && _.warn(
						'Props will not be compiled if no `el` option is ' +
						'provided at instantiation.'
					)
				}
				// make sure to convert string selectors into element now
				el = options.el = _.query(el)
				this._propsUnlinkFn = el && el.nodeType === 1 && props
					// props must be linked in proper scope if inside v-for
					? compiler.compileAndLinkProps(this, el, props, this._scope)
					: null
			}

			/**
			 * Initialize the data.
			 */

			exports._initData = function () {
				var propsData = this._data
				var optionsDataFn = this.$options.data
				var optionsData = optionsDataFn && optionsDataFn()
				if (optionsData) {
					this._data = optionsData
					for (var prop in propsData) {
						if (("development") !== 'production' &&
							optionsData.hasOwnProperty(prop)) {
							_.warn(
								'Data field "' + prop + '" is already defined ' +
								'as a prop. Use prop default value instead.'
							)
						}
						if (this._props[prop].raw !== null ||
							!optionsData.hasOwnProperty(prop)) {
							_.set(optionsData, prop, propsData[prop])
						}
					}
				}
				var data = this._data
				// proxy data on instance
				var keys = Object.keys(data)
				var i, key
				i = keys.length
				while (i--) {
					key = keys[i]
					this._proxy(key)
				}
				// observe data
				Observer.create(data, this)
			}

			/**
			 * Swap the isntance's $data. Called in $data's setter.
			 *
			 * @param {Object} newData
			 */

			exports._setData = function (newData) {
				newData = newData || {}
				var oldData = this._data
				this._data = newData
				var keys, key, i
				// unproxy keys not present in new data
				keys = Object.keys(oldData)
				i = keys.length
				while (i--) {
					key = keys[i]
					if (!(key in newData)) {
						this._unproxy(key)
					}
				}
				// proxy keys not already proxied,
				// and trigger change for changed values
				keys = Object.keys(newData)
				i = keys.length
				while (i--) {
					key = keys[i]
					if (!this.hasOwnProperty(key)) {
						// new property
						this._proxy(key)
					}
				}
				oldData.__ob__.removeVm(this)
				Observer.create(newData, this)
				this._digest()
			}

			/**
			 * Proxy a property, so that
			 * vm.prop === vm._data.prop
			 *
			 * @param {String} key
			 */

			exports._proxy = function (key) {
				if (!_.isReserved(key)) {
					// need to store ref to self here
					// because these getter/setters might
					// be called by child scopes via
					// prototype inheritance.
					var self = this
					Object.defineProperty(self, key, {
						configurable: true,
						enumerable: true,
						get: function proxyGetter () {
							return self._data[key]
						},
						set: function proxySetter (val) {
							self._data[key] = val
						}
					})
				}
			}

			/**
			 * Unproxy a property.
			 *
			 * @param {String} key
			 */

			exports._unproxy = function (key) {
				if (!_.isReserved(key)) {
					delete this[key]
				}
			}

			/**
			 * Force update on every watcher in scope.
			 */

			exports._digest = function () {
				for (var i = 0, l = this._watchers.length; i < l; i++) {
					this._watchers[i].update(true) // shallow updates
				}
			}

			/**
			 * Setup computed properties. They are essentially
			 * special getter/setters
			 */

			function noop () {}
			exports._initComputed = function () {
				var computed = this.$options.computed
				if (computed) {
					for (var key in computed) {
						var userDef = computed[key]
						var def = {
							enumerable: true,
							configurable: true
						}
						if (typeof userDef === 'function') {
							def.get = makeComputedGetter(userDef, this)
							def.set = noop
						} else {
							def.get = userDef.get
								? userDef.cache !== false
								? makeComputedGetter(userDef.get, this)
								: _.bind(userDef.get, this)
								: noop
							def.set = userDef.set
								? _.bind(userDef.set, this)
								: noop
						}
						Object.defineProperty(this, key, def)
					}
				}
			}

			function makeComputedGetter (getter, owner) {
				var watcher = new Watcher(owner, getter, null, {
					lazy: true
				})
				return function computedGetter () {
					if (watcher.dirty) {
						watcher.evaluate()
					}
					if (Dep.target) {
						watcher.depend()
					}
					return watcher.value
				}
			}

			/**
			 * Setup instance methods. Methods must be bound to the
			 * instance since they might be passed down as a prop to
			 * child components.
			 */

			exports._initMethods = function () {
				var methods = this.$options.methods
				if (methods) {
					for (var key in methods) {
						this[key] = _.bind(methods[key], this)
					}
				}
			}

			/**
			 * Initialize meta information like $index, $key & $value.
			 */

			exports._initMeta = function () {
				var metas = this.$options._meta
				if (metas) {
					for (var key in metas) {
						_.defineReactive(this, key, metas[key])
					}
				}
			}


			/***/ },
		/* 58 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var Dep = __webpack_require__(41)
			var arrayMethods = __webpack_require__(59)
			var arrayKeys = Object.getOwnPropertyNames(arrayMethods)

			/**
			 * Observer class that are attached to each observed
			 * object. Once attached, the observer converts target
			 * object's property keys into getter/setters that
			 * collect dependencies and dispatches updates.
			 *
			 * @param {Array|Object} value
			 * @constructor
			 */

			function Observer (value) {
				this.value = value
				this.dep = new Dep()
				_.define(value, '__ob__', this)
				if (_.isArray(value)) {
					var augment = _.hasProto
						? protoAugment
						: copyAugment
					augment(value, arrayMethods, arrayKeys)
					this.observeArray(value)
				} else {
					this.walk(value)
				}
			}

			// Static methods

			/**
			 * Attempt to create an observer instance for a value,
			 * returns the new observer if successfully observed,
			 * or the existing observer if the value already has one.
			 *
			 * @param {*} value
			 * @param {Vue} [vm]
			 * @return {Observer|undefined}
			 * @static
			 */

			Observer.create = function (value, vm) {
				if (!value || typeof value !== 'object') {
					return
				}
				var ob
				if (
					value.hasOwnProperty('__ob__') &&
					value.__ob__ instanceof Observer
				) {
					ob = value.__ob__
				} else if (
					(_.isArray(value) || _.isPlainObject(value)) &&
					!Object.isFrozen(value) &&
					!value._isVue
				) {
					ob = new Observer(value)
				}
				if (ob && vm) {
					ob.addVm(vm)
				}
				return ob
			}

			// Instance methods

			/**
			 * Walk through each property and convert them into
			 * getter/setters. This method should only be called when
			 * value type is Object.
			 *
			 * @param {Object} obj
			 */

			Observer.prototype.walk = function (obj) {
				var keys = Object.keys(obj)
				var i = keys.length
				while (i--) {
					this.convert(keys[i], obj[keys[i]])
				}
			}

			/**
			 * Observe a list of Array items.
			 *
			 * @param {Array} items
			 */

			Observer.prototype.observeArray = function (items) {
				var i = items.length
				while (i--) {
					Observer.create(items[i])
				}
			}

			/**
			 * Convert a property into getter/setter so we can emit
			 * the events when the property is accessed/changed.
			 *
			 * @param {String} key
			 * @param {*} val
			 */

			Observer.prototype.convert = function (key, val) {
				defineReactive(this.value, key, val)
			}

			/**
			 * Add an owner vm, so that when $set/$delete mutations
			 * happen we can notify owner vms to proxy the keys and
			 * digest the watchers. This is only called when the object
			 * is observed as an instance's root $data.
			 *
			 * @param {Vue} vm
			 */

			Observer.prototype.addVm = function (vm) {
				(this.vms || (this.vms = [])).push(vm)
			}

			/**
			 * Remove an owner vm. This is called when the object is
			 * swapped out as an instance's $data object.
			 *
			 * @param {Vue} vm
			 */

			Observer.prototype.removeVm = function (vm) {
				this.vms.$remove(vm)
			}

			// helpers

			/**
			 * Augment an target Object or Array by intercepting
			 * the prototype chain using __proto__
			 *
			 * @param {Object|Array} target
			 * @param {Object} proto
			 */

			function protoAugment (target, src) {
				target.__proto__ = src
			}

			/**
			 * Augment an target Object or Array by defining
			 * hidden properties.
			 *
			 * @param {Object|Array} target
			 * @param {Object} proto
			 */

			function copyAugment (target, src, keys) {
				var i = keys.length
				var key
				while (i--) {
					key = keys[i]
					_.define(target, key, src[key])
				}
			}

			/**
			 * Define a reactive property on an Object.
			 *
			 * @param {Object} obj
			 * @param {String} key
			 * @param {*} val
			 */

			function defineReactive (obj, key, val) {
				var dep = new Dep()
				var childOb = Observer.create(val)
				Object.defineProperty(obj, key, {
					enumerable: true,
					configurable: true,
					get: function metaGetter () {
						if (Dep.target) {
							dep.depend()
							if (childOb) {
								childOb.dep.depend()
							}
							if (_.isArray(val)) {
								for (var e, i = 0, l = val.length; i < l; i++) {
									e = val[i]
									e && e.__ob__ && e.__ob__.dep.depend()
								}
							}
						}
						return val
					},
					set: function metaSetter (newVal) {
						if (newVal === val) return
						val = newVal
						childOb = Observer.create(newVal)
						dep.notify()
					}
				})
			}

			// Attach to the util object so it can be used elsewhere.
			_.defineReactive = defineReactive

			module.exports = Observer


			/***/ },
		/* 59 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var arrayProto = Array.prototype
			var arrayMethods = Object.create(arrayProto)

			/**
			 * Intercept mutating methods and emit events
			 */

				;[
				'push',
				'pop',
				'shift',
				'unshift',
				'splice',
				'sort',
				'reverse'
			]
				.forEach(function (method) {
					// cache original method
					var original = arrayProto[method]
					_.define(arrayMethods, method, function mutator () {
						// avoid leaking arguments:
						// http://jsperf.com/closure-with-arguments
						var i = arguments.length
						var args = new Array(i)
						while (i--) {
							args[i] = arguments[i]
						}
						var result = original.apply(this, args)
						var ob = this.__ob__
						var inserted
						switch (method) {
							case 'push':
								inserted = args
								break
							case 'unshift':
								inserted = args
								break
							case 'splice':
								inserted = args.slice(2)
								break
						}
						if (inserted) ob.observeArray(inserted)
						// notify change
						ob.dep.notify()
						return result
					})
				})

			/**
			 * Swap the element at the given index with a new value
			 * and emits corresponding event.
			 *
			 * @param {Number} index
			 * @param {*} val
			 * @return {*} - replaced element
			 */

			_.define(
				arrayProto,
				'$set',
				function $set (index, val) {
					if (index >= this.length) {
						this.length = index + 1
					}
					return this.splice(index, 1, val)[0]
				}
			)

			/**
			 * Convenience method to remove the element at given index.
			 *
			 * @param {Number} index
			 * @param {*} val
			 */

			_.define(
				arrayProto,
				'$remove',
				function $remove (item) {
					/* istanbul ignore if */
					if (!this.length) return
					var index = _.indexOf(this, item)
					if (index > -1) {
						return this.splice(index, 1)
					}
				}
			)

			module.exports = arrayMethods


			/***/ },
		/* 60 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var Directive = __webpack_require__(61)
			var compiler = __webpack_require__(14)

			/**
			 * Transclude, compile and link element.
			 *
			 * If a pre-compiled linker is available, that means the
			 * passed in element will be pre-transcluded and compiled
			 * as well - all we need to do is to call the linker.
			 *
			 * Otherwise we need to call transclude/compile/link here.
			 *
			 * @param {Element} el
			 * @return {Element}
			 */

			exports._compile = function (el) {
				var options = this.$options

				// transclude and init element
				// transclude can potentially replace original
				// so we need to keep reference; this step also injects
				// the template and caches the original attributes
				// on the container node and replacer node.
				var original = el
				el = compiler.transclude(el, options)
				this._initElement(el)

				// root is always compiled per-instance, because
				// container attrs and props can be different every time.
				var contextOptions = this._context && this._context.$options
				var rootLinker = compiler.compileRoot(el, options, contextOptions)

				// compile and link the rest
				var contentLinkFn
				var ctor = this.constructor
				// component compilation can be cached
				// as long as it's not using inline-template
				if (options._linkerCachable) {
					contentLinkFn = ctor.linker
					if (!contentLinkFn) {
						contentLinkFn = ctor.linker = compiler.compile(el, options)
					}
				}

				// link phase
				// make sure to link root with prop scope!
				var rootUnlinkFn = rootLinker(this, el, this._scope)
				var contentUnlinkFn = contentLinkFn
					? contentLinkFn(this, el)
					: compiler.compile(el, options)(this, el)

				// register composite unlink function
				// to be called during instance destruction
				this._unlinkFn = function () {
					rootUnlinkFn()
					// passing destroying: true to avoid searching and
					// splicing the directives
					contentUnlinkFn(true)
				}

				// finally replace original
				if (options.replace) {
					_.replace(original, el)
				}

				this._isCompiled = true
				this._callHook('compiled')
				return el
			}

			/**
			 * Initialize instance element. Called in the public
			 * $mount() method.
			 *
			 * @param {Element} el
			 */

			exports._initElement = function (el) {
				if (el instanceof DocumentFragment) {
					this._isFragment = true
					this.$el = this._fragmentStart = el.firstChild
					this._fragmentEnd = el.lastChild
					// set persisted text anchors to empty
					if (this._fragmentStart.nodeType === 3) {
						this._fragmentStart.data = this._fragmentEnd.data = ''
					}
					this._fragment = el
				} else {
					this.$el = el
				}
				this.$el.__vue__ = this
				this._callHook('beforeCompile')
			}

			/**
			 * Create and bind a directive to an element.
			 *
			 * @param {String} name - directive name
			 * @param {Node} node   - target node
			 * @param {Object} desc - parsed directive descriptor
			 * @param {Object} def  - directive definition object
			 * @param {Vue} [host] - transclusion host component
			 * @param {Object} [scope] - v-for scope
			 * @param {Fragment} [frag] - owner fragment
			 */

			exports._bindDir = function (descriptor, node, host, scope, frag) {
				this._directives.push(
					new Directive(descriptor, this, node, host, scope, frag)
				)
			}

			/**
			 * Teardown an instance, unobserves the data, unbind all the
			 * directives, turn off all the event listeners, etc.
			 *
			 * @param {Boolean} remove - whether to remove the DOM node.
			 * @param {Boolean} deferCleanup - if true, defer cleanup to
			 *                                 be called later
			 */

			exports._destroy = function (remove, deferCleanup) {
				if (this._isBeingDestroyed) {
					return
				}
				this._callHook('beforeDestroy')
				this._isBeingDestroyed = true
				var i
				// remove self from parent. only necessary
				// if parent is not being destroyed as well.
				var parent = this.$parent
				if (parent && !parent._isBeingDestroyed) {
					parent.$children.$remove(this)
					// unregister ref
					var ref = this.$options._ref
					if (ref) {
						var scope = this._scope || this._context
						if (scope.$refs[ref] === this) {
							scope.$refs[ref] = null
						}
					}
				}
				// remove self from owner fragment
				if (this._frag) {
					this._frag.children.$remove(this)
				}
				// destroy all children.
				i = this.$children.length
				while (i--) {
					this.$children[i].$destroy()
				}
				// teardown props
				if (this._propsUnlinkFn) {
					this._propsUnlinkFn()
				}
				// teardown all directives. this also tearsdown all
				// directive-owned watchers.
				if (this._unlinkFn) {
					this._unlinkFn()
				}
				i = this._watchers.length
				while (i--) {
					this._watchers[i].teardown()
				}
				// remove reference to self on $el
				if (this.$el) {
					this.$el.__vue__ = null
				}
				// remove DOM element
				var self = this
				if (remove && this.$el) {
					this.$remove(function () {
						self._cleanup()
					})
				} else if (!deferCleanup) {
					this._cleanup()
				}
			}

			/**
			 * Clean up to ensure garbage collection.
			 * This is called after the leave transition if there
			 * is any.
			 */

			exports._cleanup = function () {
				// remove reference from data ob
				// frozen object may not have observer.
				if (this._data.__ob__) {
					this._data.__ob__.removeVm(this)
				}
				// Clean up references to private properties and other
				// instances. preserve reference to _data so that proxy
				// accessors still work. The only potential side effect
				// here is that mutating the instance after it's destroyed
				// may affect the state of other components that are still
				// observing the same object, but that seems to be a
				// reasonable responsibility for the user rather than
				// always throwing an error on them.
				this.$el =
					this.$parent =
						this.$root =
							this.$children =
								this._watchers =
									this._context =
										this._scope =
											this._directives = null
				// call the last hook...
				this._isDestroyed = true
				this._callHook('destroyed')
				// turn off all instance listeners.
				this.$off()
			}


			/***/ },
		/* 61 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var Watcher = __webpack_require__(40)
			var expParser = __webpack_require__(42)
			function noop () {}

			/**
			 * A directive links a DOM element with a piece of data,
			 * which is the result of evaluating an expression.
			 * It registers a watcher with the expression and calls
			 * the DOM update function when a change is triggered.
			 *
			 * @param {String} name
			 * @param {Node} el
			 * @param {Vue} vm
			 * @param {Object} descriptor
			 *                 - {String} name
			 *                 - {Object} def
			 *                 - {String} expression
			 *                 - {Array<Object>} [filters]
			 *                 - {Boolean} literal
			 *                 - {String} attr
			 *                 - {String} raw
			 * @param {Object} def - directive definition object
			 * @param {Vue} [host] - transclusion host component
			 * @param {Object} [scope] - v-for scope
			 * @param {Fragment} [frag] - owner fragment
			 * @constructor
			 */

			function Directive (descriptor, vm, el, host, scope, frag) {
				this.vm = vm
				this.el = el
				// copy descriptor properties
				this.descriptor = descriptor
				this.name = descriptor.name
				this.expression = descriptor.expression
				this.arg = descriptor.arg
				this.modifiers = descriptor.modifiers
				this.filters = descriptor.filters
				this.literal = this.modifiers && this.modifiers.literal
				// private
				this._locked = false
				this._bound = false
				this._listeners = null
				// link context
				this._host = host
				this._scope = scope
				this._frag = frag
				// store directives on node in dev mode
				if (("development") !== 'production' && this.el) {
					this.el._vue_directives = this.el._vue_directives || []
					this.el._vue_directives.push(this)
				}
			}

			/**
			 * Initialize the directive, mixin definition properties,
			 * setup the watcher, call definition bind() and update()
			 * if present.
			 *
			 * @param {Object} def
			 */

			Directive.prototype._bind = function () {
				var name = this.name
				var descriptor = this.descriptor

				// remove attribute
				if (
					(name !== 'cloak' || this.vm._isCompiled) &&
					this.el && this.el.removeAttribute
				) {
					var attr = descriptor.attr || ('v-' + name)
					this.el.removeAttribute(attr)
				}

				// copy def properties
				var def = descriptor.def
				if (typeof def === 'function') {
					this.update = def
				} else {
					_.extend(this, def)
				}

				// setup directive params
				this._setupParams()

				// initial bind
				if (this.bind) {
					this.bind()
				}

				if (this.literal) {
					this.update && this.update(descriptor.raw)
				} else if (
					(this.expression || this.modifiers) &&
					(this.update || this.twoWay) &&
					!this._checkStatement()
				) {
					// wrapped updater for context
					var dir = this
					if (this.update) {
						this._update = function (val, oldVal) {
							if (!dir._locked) {
								dir.update(val, oldVal)
							}
						}
					} else {
						this._update = noop
					}
					var preProcess = this._preProcess
						? _.bind(this._preProcess, this)
						: null
					var postProcess = this._postProcess
						? _.bind(this._postProcess, this)
						: null
					var watcher = this._watcher = new Watcher(
						this.vm,
						this.expression,
						this._update, // callback
						{
							filters: this.filters,
							twoWay: this.twoWay,
							deep: this.deep,
							preProcess: preProcess,
							postProcess: postProcess,
							scope: this._scope
						}
					)
					// v-model with inital inline value need to sync back to
					// model instead of update to DOM on init. They would
					// set the afterBind hook to indicate that.
					if (this.afterBind) {
						this.afterBind()
					} else if (this.update) {
						this.update(watcher.value)
					}
				}
				this._bound = true
			}

			/**
			 * Setup all param attributes, e.g. track-by,
			 * transition-mode, etc...
			 */

			Directive.prototype._setupParams = function () {
				if (!this.params) {
					return
				}
				var params = this.params
				// swap the params array with a fresh object.
				this.params = Object.create(null)
				var i = params.length
				var key, val, mappedKey
				while (i--) {
					key = params[i]
					mappedKey = _.camelize(key)
					val = _.getBindAttr(this.el, key)
					if (val != null) {
						// dynamic
						this._setupParamWatcher(mappedKey, val)
					} else {
						// static
						val = _.attr(this.el, key)
						if (val != null) {
							this.params[mappedKey] = val === '' ? true : val
						}
					}
				}
			}

			/**
			 * Setup a watcher for a dynamic param.
			 *
			 * @param {String} key
			 * @param {String} expression
			 */

			Directive.prototype._setupParamWatcher = function (key, expression) {
				var self = this
				var called = false
				var unwatch = (this._scope || this.vm).$watch(expression, function (val, oldVal) {
						self.params[key] = val
						// since we are in immediate mode,
						// only call the param change callbacks if this is not the first update.
						if (called) {
							var cb = self.paramWatchers && self.paramWatchers[key]
							if (cb) {
								cb.call(self, val, oldVal)
							}
						} else {
							called = true
						}
					}, {
						immediate: true
					})
					;(this._paramUnwatchFns || (this._paramUnwatchFns = [])).push(unwatch)
			}

			/**
			 * Check if the directive is a function caller
			 * and if the expression is a callable one. If both true,
			 * we wrap up the expression and use it as the event
			 * handler.
			 *
			 * e.g. on-click="a++"
			 *
			 * @return {Boolean}
			 */

			Directive.prototype._checkStatement = function () {
				var expression = this.expression
				if (
					expression && this.acceptStatement &&
					!expParser.isSimplePath(expression)
				) {
					var fn = expParser.parse(expression).get
					var scope = this._scope || this.vm
					var handler = function () {
						fn.call(scope, scope)
					}
					if (this.filters) {
						handler = scope._applyFilters(handler, null, this.filters)
					}
					this.update(handler)
					return true
				}
			}

			/**
			 * Set the corresponding value with the setter.
			 * This should only be used in two-way directives
			 * e.g. v-model.
			 *
			 * @param {*} value
			 * @public
			 */

			Directive.prototype.set = function (value) {
				/* istanbul ignore else */
				if (this.twoWay) {
					this._withLock(function () {
						this._watcher.set(value)
					})
				} else if (true) {
					_.warn(
						'Directive.set() can only be used inside twoWay' +
						'directives.'
					)
				}
			}

			/**
			 * Execute a function while preventing that function from
			 * triggering updates on this directive instance.
			 *
			 * @param {Function} fn
			 */

			Directive.prototype._withLock = function (fn) {
				var self = this
				self._locked = true
				fn.call(self)
				_.nextTick(function () {
					self._locked = false
				})
			}

			/**
			 * Convenience method that attaches a DOM event listener
			 * to the directive element and autometically tears it down
			 * during unbind.
			 *
			 * @param {String} event
			 * @param {Function} handler
			 */

			Directive.prototype.on = function (event, handler) {
				_.on(this.el, event, handler)
				;(this._listeners || (this._listeners = []))
					.push([event, handler])
			}

			/**
			 * Teardown the watcher and call unbind.
			 */

			Directive.prototype._teardown = function () {
				if (this._bound) {
					this._bound = false
					if (this.unbind) {
						this.unbind()
					}
					if (this._watcher) {
						this._watcher.teardown()
					}
					var listeners = this._listeners
					var i
					if (listeners) {
						i = listeners.length
						while (i--) {
							_.off(this.el, listeners[i][0], listeners[i][1])
						}
					}
					var unwatchFns = this._paramUnwatchFns
					if (unwatchFns) {
						i = unwatchFns.length
						while (i--) {
							unwatchFns[i]()
						}
					}
					if (("development") !== 'production' && this.el) {
						this.el._vue_directives.$remove(this)
					}
					this.vm = this.el = this._watcher = this._listeners = null
				}
			}

			module.exports = Directive


			/***/ },
		/* 62 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)

			/**
			 * Apply a list of filter (descriptors) to a value.
			 * Using plain for loops here because this will be called in
			 * the getter of any watcher with filters so it is very
			 * performance sensitive.
			 *
			 * @param {*} value
			 * @param {*} [oldValue]
			 * @param {Array} filters
			 * @param {Boolean} write
			 * @return {*}
			 */

			exports._applyFilters = function (value, oldValue, filters, write) {
				var filter, fn, args, arg, offset, i, l, j, k
				for (i = 0, l = filters.length; i < l; i++) {
					filter = filters[i]
					fn = _.resolveAsset(this.$options, 'filters', filter.name)
					if (true) {
						_.assertAsset(fn, 'filter', filter.name)
					}
					if (!fn) continue
					fn = write ? fn.write : (fn.read || fn)
					if (typeof fn !== 'function') continue
					args = write ? [value, oldValue] : [value]
					offset = write ? 2 : 1
					if (filter.args) {
						for (j = 0, k = filter.args.length; j < k; j++) {
							arg = filter.args[j]
							args[j + offset] = arg.dynamic
								? this.$get(arg.value)
								: arg.value
						}
					}
					value = fn.apply(this, args)
				}
				return value
			}

			/**
			 * Resolve a component, depending on whether the component
			 * is defined normally or using an async factory function.
			 * Resolves synchronously if already resolved, otherwise
			 * resolves asynchronously and caches the resolved
			 * constructor on the factory.
			 *
			 * @param {String} id
			 * @param {Function} cb
			 */

			exports._resolveComponent = function (id, cb) {
				var factory = _.resolveAsset(this.$options, 'components', id)
				if (true) {
					_.assertAsset(factory, 'component', id)
				}
				if (!factory) {
					return
				}
				// async component factory
				if (!factory.options) {
					if (factory.resolved) {
						// cached
						cb(factory.resolved)
					} else if (factory.requested) {
						// pool callbacks
						factory.pendingCallbacks.push(cb)
					} else {
						factory.requested = true
						var cbs = factory.pendingCallbacks = [cb]
						factory(function resolve (res) {
							if (_.isPlainObject(res)) {
								res = _.Vue.extend(res)
							}
							// cache resolved
							factory.resolved = res
							// invoke callbacks
							for (var i = 0, l = cbs.length; i < l; i++) {
								cbs[i](res)
							}
						}, function reject (reason) {
							("development") !== 'production' && _.warn(
								'Failed to resolve async component: ' + id + '. ' +
								(reason ? '\nReason: ' + reason : '')
							)
						})
					}
				} else {
					// normal component
					cb(factory)
				}
			}


			/***/ },
		/* 63 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var Watcher = __webpack_require__(40)
			var Path = __webpack_require__(43)
			var textParser = __webpack_require__(6)
			var dirParser = __webpack_require__(8)
			var expParser = __webpack_require__(42)
			var filterRE = /[^|]\|[^|]/

			/**
			 * Get the value from an expression on this vm.
			 *
			 * @param {String} exp
			 * @param {Boolean} [asStatement]
			 * @return {*}
			 */

			exports.$get = function (exp, asStatement) {
				var res = expParser.parse(exp)
				if (res) {
					if (asStatement && !expParser.isSimplePath(exp)) {
						var self = this
						return function statementHandler () {
							res.get.call(self, self)
						}
					} else {
						try {
							return res.get.call(this, this)
						} catch (e) {}
					}
				}
			}

			/**
			 * Set the value from an expression on this vm.
			 * The expression must be a valid left-hand
			 * expression in an assignment.
			 *
			 * @param {String} exp
			 * @param {*} val
			 */

			exports.$set = function (exp, val) {
				var res = expParser.parse(exp, true)
				if (res && res.set) {
					res.set.call(this, this, val)
				}
			}

			/**
			 * Delete a property on the VM
			 *
			 * @param {String} key
			 */

			exports.$delete = function (key) {
				_.delete(this._data, key)
			}

			/**
			 * Watch an expression, trigger callback when its
			 * value changes.
			 *
			 * @param {String|Function} expOrFn
			 * @param {Function} cb
			 * @param {Object} [options]
			 *                 - {Boolean} deep
			 *                 - {Boolean} immediate
			 * @return {Function} - unwatchFn
			 */

			exports.$watch = function (expOrFn, cb, options) {
				var vm = this
				var parsed
				if (typeof expOrFn === 'string') {
					parsed = dirParser.parse(expOrFn)
					expOrFn = parsed.expression
				}
				var watcher = new Watcher(vm, expOrFn, cb, {
					deep: options && options.deep,
					filters: parsed && parsed.filters
				})
				if (options && options.immediate) {
					cb.call(vm, watcher.value)
				}
				return function unwatchFn () {
					watcher.teardown()
				}
			}

			/**
			 * Evaluate a text directive, including filters.
			 *
			 * @param {String} text
			 * @param {Boolean} [asStatement]
			 * @return {String}
			 */

			exports.$eval = function (text, asStatement) {
				// check for filters.
				if (filterRE.test(text)) {
					var dir = dirParser.parse(text)
					// the filter regex check might give false positive
					// for pipes inside strings, so it's possible that
					// we don't get any filters here
					var val = this.$get(dir.expression, asStatement)
					return dir.filters
						? this._applyFilters(val, null, dir.filters)
						: val
				} else {
					// no filter
					return this.$get(text, asStatement)
				}
			}

			/**
			 * Interpolate a piece of template text.
			 *
			 * @param {String} text
			 * @return {String}
			 */

			exports.$interpolate = function (text) {
				var tokens = textParser.parse(text)
				var vm = this
				if (tokens) {
					if (tokens.length === 1) {
						return vm.$eval(tokens[0].value) + ''
					} else {
						return tokens.map(function (token) {
							return token.tag
								? vm.$eval(token.value)
								: token.value
						}).join('')
					}
				} else {
					return text
				}
			}

			/**
			 * Log instance data as a plain JS object
			 * so that it is easier to inspect in console.
			 * This method assumes console is available.
			 *
			 * @param {String} [path]
			 */

			exports.$log = function (path) {
				var data = path
					? Path.get(this._data, path)
					: this._data
				if (data) {
					data = clean(data)
				}
				// include computed fields
				if (!path) {
					for (var key in this.$options.computed) {
						data[key] = clean(this[key])
					}
				}
				console.log(data)
			}

			/**
			 * "clean" a getter/setter converted object into a plain
			 * object copy.
			 *
			 * @param {Object} - obj
			 * @return {Object}
			 */

			function clean (obj) {
				return JSON.parse(JSON.stringify(obj))
			}


			/***/ },
		/* 64 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var transition = __webpack_require__(9)

			/**
			 * Convenience on-instance nextTick. The callback is
			 * auto-bound to the instance, and this avoids component
			 * modules having to rely on the global Vue.
			 *
			 * @param {Function} fn
			 */

			exports.$nextTick = function (fn) {
				_.nextTick(fn, this)
			}

			/**
			 * Append instance to target
			 *
			 * @param {Node} target
			 * @param {Function} [cb]
			 * @param {Boolean} [withTransition] - defaults to true
			 */

			exports.$appendTo = function (target, cb, withTransition) {
				return insert(
					this, target, cb, withTransition,
					append, transition.append
				)
			}

			/**
			 * Prepend instance to target
			 *
			 * @param {Node} target
			 * @param {Function} [cb]
			 * @param {Boolean} [withTransition] - defaults to true
			 */

			exports.$prependTo = function (target, cb, withTransition) {
				target = query(target)
				if (target.hasChildNodes()) {
					this.$before(target.firstChild, cb, withTransition)
				} else {
					this.$appendTo(target, cb, withTransition)
				}
				return this
			}

			/**
			 * Insert instance before target
			 *
			 * @param {Node} target
			 * @param {Function} [cb]
			 * @param {Boolean} [withTransition] - defaults to true
			 */

			exports.$before = function (target, cb, withTransition) {
				return insert(
					this, target, cb, withTransition,
					before, transition.before
				)
			}

			/**
			 * Insert instance after target
			 *
			 * @param {Node} target
			 * @param {Function} [cb]
			 * @param {Boolean} [withTransition] - defaults to true
			 */

			exports.$after = function (target, cb, withTransition) {
				target = query(target)
				if (target.nextSibling) {
					this.$before(target.nextSibling, cb, withTransition)
				} else {
					this.$appendTo(target.parentNode, cb, withTransition)
				}
				return this
			}

			/**
			 * Remove instance from DOM
			 *
			 * @param {Function} [cb]
			 * @param {Boolean} [withTransition] - defaults to true
			 */

			exports.$remove = function (cb, withTransition) {
				if (!this.$el.parentNode) {
					return cb && cb()
				}
				var inDoc = this._isAttached && _.inDoc(this.$el)
				// if we are not in document, no need to check
				// for transitions
				if (!inDoc) withTransition = false
				var self = this
				var realCb = function () {
					if (inDoc) self._callHook('detached')
					if (cb) cb()
				}
				if (this._isFragment) {
					_.removeNodeRange(
						this._fragmentStart,
						this._fragmentEnd,
						this, this._fragment, realCb
					)
				} else {
					var op = withTransition === false
						? remove
						: transition.remove
					op(this.$el, this, realCb)
				}
				return this
			}

			/**
			 * Shared DOM insertion function.
			 *
			 * @param {Vue} vm
			 * @param {Element} target
			 * @param {Function} [cb]
			 * @param {Boolean} [withTransition]
			 * @param {Function} op1 - op for non-transition insert
			 * @param {Function} op2 - op for transition insert
			 * @return vm
			 */

			function insert (vm, target, cb, withTransition, op1, op2) {
				target = query(target)
				var targetIsDetached = !_.inDoc(target)
				var op = withTransition === false || targetIsDetached
					? op1
					: op2
				var shouldCallHook =
					!targetIsDetached &&
					!vm._isAttached &&
					!_.inDoc(vm.$el)
				if (vm._isFragment) {
					_.mapNodeRange(vm._fragmentStart, vm._fragmentEnd, function (node) {
						op(node, target, vm)
					})
					cb && cb()
				} else {
					op(vm.$el, target, vm, cb)
				}
				if (shouldCallHook) {
					vm._callHook('attached')
				}
				return vm
			}

			/**
			 * Check for selectors
			 *
			 * @param {String|Element} el
			 */

			function query (el) {
				return typeof el === 'string'
					? document.querySelector(el)
					: el
			}

			/**
			 * Append operation that takes a callback.
			 *
			 * @param {Node} el
			 * @param {Node} target
			 * @param {Vue} vm - unused
			 * @param {Function} [cb]
			 */

			function append (el, target, vm, cb) {
				target.appendChild(el)
				if (cb) cb()
			}

			/**
			 * InsertBefore operation that takes a callback.
			 *
			 * @param {Node} el
			 * @param {Node} target
			 * @param {Vue} vm - unused
			 * @param {Function} [cb]
			 */

			function before (el, target, vm, cb) {
				_.before(el, target)
				if (cb) cb()
			}

			/**
			 * Remove operation that takes a callback.
			 *
			 * @param {Node} el
			 * @param {Vue} vm - unused
			 * @param {Function} [cb]
			 */

			function remove (el, vm, cb) {
				_.remove(el)
				if (cb) cb()
			}


			/***/ },
		/* 65 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)

			/**
			 * Listen on the given `event` with `fn`.
			 *
			 * @param {String} event
			 * @param {Function} fn
			 */

			exports.$on = function (event, fn) {
				(this._events[event] || (this._events[event] = []))
					.push(fn)
				modifyListenerCount(this, event, 1)
				return this
			}

			/**
			 * Adds an `event` listener that will be invoked a single
			 * time then automatically removed.
			 *
			 * @param {String} event
			 * @param {Function} fn
			 */

			exports.$once = function (event, fn) {
				var self = this
				function on () {
					self.$off(event, on)
					fn.apply(this, arguments)
				}
				on.fn = fn
				this.$on(event, on)
				return this
			}

			/**
			 * Remove the given callback for `event` or all
			 * registered callbacks.
			 *
			 * @param {String} event
			 * @param {Function} fn
			 */

			exports.$off = function (event, fn) {
				var cbs
				// all
				if (!arguments.length) {
					if (this.$parent) {
						for (event in this._events) {
							cbs = this._events[event]
							if (cbs) {
								modifyListenerCount(this, event, -cbs.length)
							}
						}
					}
					this._events = {}
					return this
				}
				// specific event
				cbs = this._events[event]
				if (!cbs) {
					return this
				}
				if (arguments.length === 1) {
					modifyListenerCount(this, event, -cbs.length)
					this._events[event] = null
					return this
				}
				// specific handler
				var cb
				var i = cbs.length
				while (i--) {
					cb = cbs[i]
					if (cb === fn || cb.fn === fn) {
						modifyListenerCount(this, event, -1)
						cbs.splice(i, 1)
						break
					}
				}
				return this
			}

			/**
			 * Trigger an event on self.
			 *
			 * @param {String} event
			 */

			exports.$emit = function (event) {
				var cbs = this._events[event]
				this._shouldPropagate = !cbs
				if (cbs) {
					cbs = cbs.length > 1
						? _.toArray(cbs)
						: cbs
					var args = _.toArray(arguments, 1)
					for (var i = 0, l = cbs.length; i < l; i++) {
						var res = cbs[i].apply(this, args)
						if (res === true) {
							this._shouldPropagate = true
						}
					}
				}
				return this
			}

			/**
			 * Recursively broadcast an event to all children instances.
			 *
			 * @param {String} event
			 * @param {...*} additional arguments
			 */

			exports.$broadcast = function (event) {
				// if no child has registered for this event,
				// then there's no need to broadcast.
				if (!this._eventsCount[event]) return
				var children = this.$children
				for (var i = 0, l = children.length; i < l; i++) {
					var child = children[i]
					child.$emit.apply(child, arguments)
					if (child._shouldPropagate) {
						child.$broadcast.apply(child, arguments)
					}
				}
				return this
			}

			/**
			 * Recursively propagate an event up the parent chain.
			 *
			 * @param {String} event
			 * @param {...*} additional arguments
			 */

			exports.$dispatch = function () {
				this.$emit.apply(this, arguments)
				var parent = this.$parent
				while (parent) {
					parent.$emit.apply(parent, arguments)
					parent = parent._shouldPropagate
						? parent.$parent
						: null
				}
				return this
			}

			/**
			 * Modify the listener counts on all parents.
			 * This bookkeeping allows $broadcast to return early when
			 * no child has listened to a certain event.
			 *
			 * @param {Vue} vm
			 * @param {String} event
			 * @param {Number} count
			 */

			var hookRE = /^hook:/
			function modifyListenerCount (vm, event, count) {
				var parent = vm.$parent
				// hooks do not get broadcasted so no need
				// to do bookkeeping for them
				if (!parent || !count || hookRE.test(event)) return
				while (parent) {
					parent._eventsCount[event] =
						(parent._eventsCount[event] || 0) + count
					parent = parent.$parent
				}
			}


			/***/ },
		/* 66 */
		/***/ function(module, exports, __webpack_require__) {

			var _ = __webpack_require__(1)
			var compiler = __webpack_require__(14)

			/**
			 * Set instance target element and kick off the compilation
			 * process. The passed in `el` can be a selector string, an
			 * existing Element, or a DocumentFragment (for block
			 * instances).
			 *
			 * @param {Element|DocumentFragment|string} el
			 * @public
			 */

			exports.$mount = function (el) {
				if (this._isCompiled) {
					("development") !== 'production' && _.warn(
						'$mount() should be called only once.'
					)
					return
				}
				el = _.query(el)
				if (!el) {
					el = document.createElement('div')
				}
				this._compile(el)
				this._initDOMHooks()
				if (_.inDoc(this.$el)) {
					this._callHook('attached')
					ready.call(this)
				} else {
					this.$once('hook:attached', ready)
				}
				return this
			}

			/**
			 * Mark an instance as ready.
			 */

			function ready () {
				this._isAttached = true
				this._isReady = true
				this._callHook('ready')
			}

			/**
			 * Teardown the instance, simply delegate to the internal
			 * _destroy.
			 */

			exports.$destroy = function (remove, deferCleanup) {
				this._destroy(remove, deferCleanup)
			}

			/**
			 * Partially compile a piece of DOM and return a
			 * decompile function.
			 *
			 * @param {Element|DocumentFragment} el
			 * @param {Vue} [host]
			 * @return {Function}
			 */

			exports.$compile = function (el, host, scope, frag) {
				return compiler.compile(el, this.$options, true)(
					this, el, host, scope, frag
				)
			}


			/***/ }
		/******/ ])
});
;
/*! selectize.js - v0.12.1 | https://github.com/brianreavis/selectize.js | Apache License (v2) */
!function(a,b){"function"==typeof define&&define.amd?define("sifter",b):"object"==typeof exports?module.exports=b():a.Sifter=b()}(this,function(){var a=function(a,b){this.items=a,this.settings=b||{diacritics:!0}};a.prototype.tokenize=function(a){if(a=d(String(a||"").toLowerCase()),!a||!a.length)return[];var b,c,f,h,i=[],j=a.split(/ +/);for(b=0,c=j.length;c>b;b++){if(f=e(j[b]),this.settings.diacritics)for(h in g)g.hasOwnProperty(h)&&(f=f.replace(new RegExp(h,"g"),g[h]));i.push({string:j[b],regex:new RegExp(f,"i")})}return i},a.prototype.iterator=function(a,b){var c;c=f(a)?Array.prototype.forEach||function(a){for(var b=0,c=this.length;c>b;b++)a(this[b],b,this)}:function(a){for(var b in this)this.hasOwnProperty(b)&&a(this[b],b,this)},c.apply(a,[b])},a.prototype.getScoreFunction=function(a,b){var c,d,e,f;c=this,a=c.prepareSearch(a,b),e=a.tokens,d=a.options.fields,f=e.length;var g=function(a,b){var c,d;return a?(a=String(a||""),d=a.search(b.regex),-1===d?0:(c=b.string.length/a.length,0===d&&(c+=.5),c)):0},h=function(){var a=d.length;return a?1===a?function(a,b){return g(b[d[0]],a)}:function(b,c){for(var e=0,f=0;a>e;e++)f+=g(c[d[e]],b);return f/a}:function(){return 0}}();return f?1===f?function(a){return h(e[0],a)}:"and"===a.options.conjunction?function(a){for(var b,c=0,d=0;f>c;c++){if(b=h(e[c],a),0>=b)return 0;d+=b}return d/f}:function(a){for(var b=0,c=0;f>b;b++)c+=h(e[b],a);return c/f}:function(){return 0}},a.prototype.getSortFunction=function(a,c){var d,e,f,g,h,i,j,k,l,m,n;if(f=this,a=f.prepareSearch(a,c),n=!a.query&&c.sort_empty||c.sort,l=function(a,b){return"$score"===a?b.score:f.items[b.id][a]},h=[],n)for(d=0,e=n.length;e>d;d++)(a.query||"$score"!==n[d].field)&&h.push(n[d]);if(a.query){for(m=!0,d=0,e=h.length;e>d;d++)if("$score"===h[d].field){m=!1;break}m&&h.unshift({field:"$score",direction:"desc"})}else for(d=0,e=h.length;e>d;d++)if("$score"===h[d].field){h.splice(d,1);break}for(k=[],d=0,e=h.length;e>d;d++)k.push("desc"===h[d].direction?-1:1);return i=h.length,i?1===i?(g=h[0].field,j=k[0],function(a,c){return j*b(l(g,a),l(g,c))}):function(a,c){var d,e,f;for(d=0;i>d;d++)if(f=h[d].field,e=k[d]*b(l(f,a),l(f,c)))return e;return 0}:null},a.prototype.prepareSearch=function(a,b){if("object"==typeof a)return a;b=c({},b);var d=b.fields,e=b.sort,g=b.sort_empty;return d&&!f(d)&&(b.fields=[d]),e&&!f(e)&&(b.sort=[e]),g&&!f(g)&&(b.sort_empty=[g]),{options:b,query:String(a||"").toLowerCase(),tokens:this.tokenize(a),total:0,items:[]}},a.prototype.search=function(a,b){var c,d,e,f,g=this;return d=this.prepareSearch(a,b),b=d.options,a=d.query,f=b.score||g.getScoreFunction(d),a.length?g.iterator(g.items,function(a,e){c=f(a),(b.filter===!1||c>0)&&d.items.push({score:c,id:e})}):g.iterator(g.items,function(a,b){d.items.push({score:1,id:b})}),e=g.getSortFunction(d,b),e&&d.items.sort(e),d.total=d.items.length,"number"==typeof b.limit&&(d.items=d.items.slice(0,b.limit)),d};var b=function(a,b){return"number"==typeof a&&"number"==typeof b?a>b?1:b>a?-1:0:(a=h(String(a||"")),b=h(String(b||"")),a>b?1:b>a?-1:0)},c=function(a){var b,c,d,e;for(b=1,c=arguments.length;c>b;b++)if(e=arguments[b])for(d in e)e.hasOwnProperty(d)&&(a[d]=e[d]);return a},d=function(a){return(a+"").replace(/^\s+|\s+$|/g,"")},e=function(a){return(a+"").replace(/([.?*+^$[\]\\(){}|-])/g,"\\$1")},f=Array.isArray||$&&$.isArray||function(a){return"[object Array]"===Object.prototype.toString.call(a)},g={a:"[aÀÁÂÃÄÅàáâãäåĀāąĄ]",c:"[cÇçćĆčČ]",d:"[dđĐďĎ]",e:"[eÈÉÊËèéêëěĚĒēęĘ]",i:"[iÌÍÎÏìíîïĪī]",l:"[lłŁ]",n:"[nÑñňŇńŃ]",o:"[oÒÓÔÕÕÖØòóôõöøŌō]",r:"[rřŘ]",s:"[sŠšśŚ]",t:"[tťŤ]",u:"[uÙÚÛÜùúûüůŮŪū]",y:"[yŸÿýÝ]",z:"[zŽžżŻźŹ]"},h=function(){var a,b,c,d,e="",f={};for(c in g)if(g.hasOwnProperty(c))for(d=g[c].substring(2,g[c].length-1),e+=d,a=0,b=d.length;b>a;a++)f[d.charAt(a)]=c;var h=new RegExp("["+e+"]","g");return function(a){return a.replace(h,function(a){return f[a]}).toLowerCase()}}();return a}),function(a,b){"function"==typeof define&&define.amd?define("microplugin",b):"object"==typeof exports?module.exports=b():a.MicroPlugin=b()}(this,function(){var a={};a.mixin=function(a){a.plugins={},a.prototype.initializePlugins=function(a){var c,d,e,f=this,g=[];if(f.plugins={names:[],settings:{},requested:{},loaded:{}},b.isArray(a))for(c=0,d=a.length;d>c;c++)"string"==typeof a[c]?g.push(a[c]):(f.plugins.settings[a[c].name]=a[c].options,g.push(a[c].name));else if(a)for(e in a)a.hasOwnProperty(e)&&(f.plugins.settings[e]=a[e],g.push(e));for(;g.length;)f.require(g.shift())},a.prototype.loadPlugin=function(b){var c=this,d=c.plugins,e=a.plugins[b];if(!a.plugins.hasOwnProperty(b))throw new Error('Unable to find "'+b+'" plugin');d.requested[b]=!0,d.loaded[b]=e.fn.apply(c,[c.plugins.settings[b]||{}]),d.names.push(b)},a.prototype.require=function(a){var b=this,c=b.plugins;if(!b.plugins.loaded.hasOwnProperty(a)){if(c.requested[a])throw new Error('Plugin has circular dependency ("'+a+'")');b.loadPlugin(a)}return c.loaded[a]},a.define=function(b,c){a.plugins[b]={name:b,fn:c}}};var b={isArray:Array.isArray||function(a){return"[object Array]"===Object.prototype.toString.call(a)}};return a}),function(a,b){"function"==typeof define&&define.amd?define("selectize",["jquery","sifter","microplugin"],b):"object"==typeof exports?module.exports=b(require("jquery"),require("sifter"),require("microplugin")):a.Selectize=b(a.jQuery,a.Sifter,a.MicroPlugin)}(this,function(a,b,c){"use strict";var d=function(a,b){if("string"!=typeof b||b.length){var c="string"==typeof b?new RegExp(b,"i"):b,d=function(a){var b=0;if(3===a.nodeType){var e=a.data.search(c);if(e>=0&&a.data.length>0){var f=a.data.match(c),g=document.createElement("span");g.className="highlight";var h=a.splitText(e),i=(h.splitText(f[0].length),h.cloneNode(!0));g.appendChild(i),h.parentNode.replaceChild(g,h),b=1}}else if(1===a.nodeType&&a.childNodes&&!/(script|style)/i.test(a.tagName))for(var j=0;j<a.childNodes.length;++j)j+=d(a.childNodes[j]);return b};return a.each(function(){d(this)})}},e=function(){};e.prototype={on:function(a,b){this._events=this._events||{},this._events[a]=this._events[a]||[],this._events[a].push(b)},off:function(a,b){var c=arguments.length;return 0===c?delete this._events:1===c?delete this._events[a]:(this._events=this._events||{},void(a in this._events!=!1&&this._events[a].splice(this._events[a].indexOf(b),1)))},trigger:function(a){if(this._events=this._events||{},a in this._events!=!1)for(var b=0;b<this._events[a].length;b++)this._events[a][b].apply(this,Array.prototype.slice.call(arguments,1))}},e.mixin=function(a){for(var b=["on","off","trigger"],c=0;c<b.length;c++)a.prototype[b[c]]=e.prototype[b[c]]};var f=/Mac/.test(navigator.userAgent),g=65,h=13,i=27,j=37,k=38,l=80,m=39,n=40,o=78,p=8,q=46,r=16,s=f?91:17,t=f?18:17,u=9,v=1,w=2,x=!/android/i.test(window.navigator.userAgent)&&!!document.createElement("form").validity,y=function(a){return"undefined"!=typeof a},z=function(a){return"undefined"==typeof a||null===a?null:"boolean"==typeof a?a?"1":"0":a+""},A=function(a){return(a+"").replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/"/g,"&quot;")},B=function(a){return(a+"").replace(/\$/g,"$$$$")},C={};C.before=function(a,b,c){var d=a[b];a[b]=function(){return c.apply(a,arguments),d.apply(a,arguments)}},C.after=function(a,b,c){var d=a[b];a[b]=function(){var b=d.apply(a,arguments);return c.apply(a,arguments),b}};var D=function(a){var b=!1;return function(){b||(b=!0,a.apply(this,arguments))}},E=function(a,b){var c;return function(){var d=this,e=arguments;window.clearTimeout(c),c=window.setTimeout(function(){a.apply(d,e)},b)}},F=function(a,b,c){var d,e=a.trigger,f={};a.trigger=function(){var c=arguments[0];return-1===b.indexOf(c)?e.apply(a,arguments):void(f[c]=arguments)},c.apply(a,[]),a.trigger=e;for(d in f)f.hasOwnProperty(d)&&e.apply(a,f[d])},G=function(a,b,c,d){a.on(b,c,function(b){for(var c=b.target;c&&c.parentNode!==a[0];)c=c.parentNode;return b.currentTarget=c,d.apply(this,[b])})},H=function(a){var b={};if("selectionStart"in a)b.start=a.selectionStart,b.length=a.selectionEnd-b.start;else if(document.selection){a.focus();var c=document.selection.createRange(),d=document.selection.createRange().text.length;c.moveStart("character",-a.value.length),b.start=c.text.length-d,b.length=d}return b},I=function(a,b,c){var d,e,f={};if(c)for(d=0,e=c.length;e>d;d++)f[c[d]]=a.css(c[d]);else f=a.css();b.css(f)},J=function(b,c){if(!b)return 0;var d=a("<test>").css({position:"absolute",top:-99999,left:-99999,width:"auto",padding:0,whiteSpace:"pre"}).text(b).appendTo("body");I(c,d,["letterSpacing","fontSize","fontFamily","fontWeight","textTransform"]);var e=d.width();return d.remove(),e},K=function(a){var b=null,c=function(c,d){var e,f,g,h,i,j,k,l;c=c||window.event||{},d=d||{},c.metaKey||c.altKey||(d.force||a.data("grow")!==!1)&&(e=a.val(),c.type&&"keydown"===c.type.toLowerCase()&&(f=c.keyCode,g=f>=97&&122>=f||f>=65&&90>=f||f>=48&&57>=f||32===f,f===q||f===p?(l=H(a[0]),l.length?e=e.substring(0,l.start)+e.substring(l.start+l.length):f===p&&l.start?e=e.substring(0,l.start-1)+e.substring(l.start+1):f===q&&"undefined"!=typeof l.start&&(e=e.substring(0,l.start)+e.substring(l.start+1))):g&&(j=c.shiftKey,k=String.fromCharCode(c.keyCode),k=j?k.toUpperCase():k.toLowerCase(),e+=k)),h=a.attr("placeholder"),!e&&h&&(e=h),i=J(e,a)+4,i!==b&&(b=i,a.width(i),a.triggerHandler("resize")))};a.on("keydown keyup update blur",c),c()},L=function(c,d){var e,f,g,h,i=this;h=c[0],h.selectize=i;var j=window.getComputedStyle&&window.getComputedStyle(h,null);if(g=j?j.getPropertyValue("direction"):h.currentStyle&&h.currentStyle.direction,g=g||c.parents("[dir]:first").attr("dir")||"",a.extend(i,{order:0,settings:d,$input:c,tabIndex:c.attr("tabindex")||"",tagType:"select"===h.tagName.toLowerCase()?v:w,rtl:/rtl/i.test(g),eventNS:".selectize"+ ++L.count,highlightedValue:null,isOpen:!1,isDisabled:!1,isRequired:c.is("[required]"),isInvalid:!1,isLocked:!1,isFocused:!1,isInputHidden:!1,isSetup:!1,isShiftDown:!1,isCmdDown:!1,isCtrlDown:!1,ignoreFocus:!1,ignoreBlur:!1,ignoreHover:!1,hasOptions:!1,currentResults:null,lastValue:"",caretPos:0,loading:0,loadedSearches:{},$activeOption:null,$activeItems:[],optgroups:{},options:{},userOptions:{},items:[],renderCache:{},onSearchChange:null===d.loadThrottle?i.onSearchChange:E(i.onSearchChange,d.loadThrottle)}),i.sifter=new b(this.options,{diacritics:d.diacritics}),i.settings.options){for(e=0,f=i.settings.options.length;f>e;e++)i.registerOption(i.settings.options[e]);delete i.settings.options}if(i.settings.optgroups){for(e=0,f=i.settings.optgroups.length;f>e;e++)i.registerOptionGroup(i.settings.optgroups[e]);delete i.settings.optgroups}i.settings.mode=i.settings.mode||(1===i.settings.maxItems?"single":"multi"),"boolean"!=typeof i.settings.hideSelected&&(i.settings.hideSelected="multi"===i.settings.mode),i.initializePlugins(i.settings.plugins),i.setupCallbacks(),i.setupTemplates(),i.setup()};return e.mixin(L),c.mixin(L),a.extend(L.prototype,{setup:function(){var b,c,d,e,g,h,i,j,k,l=this,m=l.settings,n=l.eventNS,o=a(window),p=a(document),q=l.$input;if(i=l.settings.mode,j=q.attr("class")||"",b=a("<div>").addClass(m.wrapperClass).addClass(j).addClass(i),c=a("<div>").addClass(m.inputClass).addClass("items").appendTo(b),d=a('<input type="text" autocomplete="off" />').appendTo(c).attr("tabindex",q.is(":disabled")?"-1":l.tabIndex),h=a(m.dropdownParent||b),e=a("<div>").addClass(m.dropdownClass).addClass(i).hide().appendTo(h),g=a("<div>").addClass(m.dropdownContentClass).appendTo(e),l.settings.copyClassesToDropdown&&e.addClass(j),b.css({width:q[0].style.width}),l.plugins.names.length&&(k="plugin-"+l.plugins.names.join(" plugin-"),b.addClass(k),e.addClass(k)),(null===m.maxItems||m.maxItems>1)&&l.tagType===v&&q.attr("multiple","multiple"),l.settings.placeholder&&d.attr("placeholder",m.placeholder),!l.settings.splitOn&&l.settings.delimiter){var u=l.settings.delimiter.replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&");l.settings.splitOn=new RegExp("\\s*"+u+"+\\s*")}q.attr("autocorrect")&&d.attr("autocorrect",q.attr("autocorrect")),q.attr("autocapitalize")&&d.attr("autocapitalize",q.attr("autocapitalize")),l.$wrapper=b,l.$control=c,l.$control_input=d,l.$dropdown=e,l.$dropdown_content=g,e.on("mouseenter","[data-selectable]",function(){return l.onOptionHover.apply(l,arguments)}),e.on("mousedown click","[data-selectable]",function(){return l.onOptionSelect.apply(l,arguments)}),G(c,"mousedown","*:not(input)",function(){return l.onItemSelect.apply(l,arguments)}),K(d),c.on({mousedown:function(){return l.onMouseDown.apply(l,arguments)},click:function(){return l.onClick.apply(l,arguments)}}),d.on({mousedown:function(a){a.stopPropagation()},keydown:function(){return l.onKeyDown.apply(l,arguments)},keyup:function(){return l.onKeyUp.apply(l,arguments)},keypress:function(){return l.onKeyPress.apply(l,arguments)},resize:function(){l.positionDropdown.apply(l,[])},blur:function(){return l.onBlur.apply(l,arguments)},focus:function(){return l.ignoreBlur=!1,l.onFocus.apply(l,arguments)},paste:function(){return l.onPaste.apply(l,arguments)}}),p.on("keydown"+n,function(a){l.isCmdDown=a[f?"metaKey":"ctrlKey"],l.isCtrlDown=a[f?"altKey":"ctrlKey"],l.isShiftDown=a.shiftKey}),p.on("keyup"+n,function(a){a.keyCode===t&&(l.isCtrlDown=!1),a.keyCode===r&&(l.isShiftDown=!1),a.keyCode===s&&(l.isCmdDown=!1)}),p.on("mousedown"+n,function(a){if(l.isFocused){if(a.target===l.$dropdown[0]||a.target.parentNode===l.$dropdown[0])return!1;l.$control.has(a.target).length||a.target===l.$control[0]||l.blur(a.target)}}),o.on(["scroll"+n,"resize"+n].join(" "),function(){l.isOpen&&l.positionDropdown.apply(l,arguments)}),o.on("mousemove"+n,function(){l.ignoreHover=!1}),this.revertSettings={$children:q.children().detach(),tabindex:q.attr("tabindex")},q.attr("tabindex",-1).hide().after(l.$wrapper),a.isArray(m.items)&&(l.setValue(m.items),delete m.items),x&&q.on("invalid"+n,function(a){a.preventDefault(),l.isInvalid=!0,l.refreshState()}),l.updateOriginalInput(),l.refreshItems(),l.refreshState(),l.updatePlaceholder(),l.isSetup=!0,q.is(":disabled")&&l.disable(),l.on("change",this.onChange),q.data("selectize",l),q.addClass("selectized"),l.trigger("initialize"),m.preload===!0&&l.onSearchChange("")},setupTemplates:function(){var b=this,c=b.settings.labelField,d=b.settings.optgroupLabelField,e={optgroup:function(a){return'<div class="optgroup">'+a.html+"</div>"},optgroup_header:function(a,b){return'<div class="optgroup-header">'+b(a[d])+"</div>"},option:function(a,b){return'<div class="option">'+b(a[c])+"</div>"},item:function(a,b){return'<div class="item">'+b(a[c])+"</div>"},option_create:function(a,b){return'<div class="create">Add <strong>'+b(a.input)+"</strong>&hellip;</div>"}};b.settings.render=a.extend({},e,b.settings.render)},setupCallbacks:function(){var a,b,c={initialize:"onInitialize",change:"onChange",item_add:"onItemAdd",item_remove:"onItemRemove",clear:"onClear",option_add:"onOptionAdd",option_remove:"onOptionRemove",option_clear:"onOptionClear",optgroup_add:"onOptionGroupAdd",optgroup_remove:"onOptionGroupRemove",optgroup_clear:"onOptionGroupClear",dropdown_open:"onDropdownOpen",dropdown_close:"onDropdownClose",type:"onType",load:"onLoad",focus:"onFocus",blur:"onBlur"};for(a in c)c.hasOwnProperty(a)&&(b=this.settings[c[a]],b&&this.on(a,b))},onClick:function(a){var b=this;b.isFocused||(b.focus(),a.preventDefault())},onMouseDown:function(b){{var c=this,d=b.isDefaultPrevented();a(b.target)}if(c.isFocused){if(b.target!==c.$control_input[0])return"single"===c.settings.mode?c.isOpen?c.close():c.open():d||c.setActiveItem(null),!1}else d||window.setTimeout(function(){c.focus()},0)},onChange:function(){this.$input.trigger("change")},onPaste:function(b){var c=this;c.isFull()||c.isInputHidden||c.isLocked?b.preventDefault():c.settings.splitOn&&setTimeout(function(){for(var b=a.trim(c.$control_input.val()||"").split(c.settings.splitOn),d=0,e=b.length;e>d;d++)c.createItem(b[d])},0)},onKeyPress:function(a){if(this.isLocked)return a&&a.preventDefault();var b=String.fromCharCode(a.keyCode||a.which);return this.settings.create&&"multi"===this.settings.mode&&b===this.settings.delimiter?(this.createItem(),a.preventDefault(),!1):void 0},onKeyDown:function(a){var b=(a.target===this.$control_input[0],this);if(b.isLocked)return void(a.keyCode!==u&&a.preventDefault());switch(a.keyCode){case g:if(b.isCmdDown)return void b.selectAll();break;case i:return void(b.isOpen&&(a.preventDefault(),a.stopPropagation(),b.close()));case o:if(!a.ctrlKey||a.altKey)break;case n:if(!b.isOpen&&b.hasOptions)b.open();else if(b.$activeOption){b.ignoreHover=!0;var c=b.getAdjacentOption(b.$activeOption,1);c.length&&b.setActiveOption(c,!0,!0)}return void a.preventDefault();case l:if(!a.ctrlKey||a.altKey)break;case k:if(b.$activeOption){b.ignoreHover=!0;var d=b.getAdjacentOption(b.$activeOption,-1);d.length&&b.setActiveOption(d,!0,!0)}return void a.preventDefault();case h:return void(b.isOpen&&b.$activeOption&&(b.onOptionSelect({currentTarget:b.$activeOption}),a.preventDefault()));case j:return void b.advanceSelection(-1,a);case m:return void b.advanceSelection(1,a);case u:return b.settings.selectOnTab&&b.isOpen&&b.$activeOption&&(b.onOptionSelect({currentTarget:b.$activeOption}),b.isFull()||a.preventDefault()),void(b.settings.create&&b.createItem()&&a.preventDefault());case p:case q:return void b.deleteSelection(a)}return!b.isFull()&&!b.isInputHidden||(f?a.metaKey:a.ctrlKey)?void 0:void a.preventDefault()},onKeyUp:function(a){var b=this;if(b.isLocked)return a&&a.preventDefault();var c=b.$control_input.val()||"";b.lastValue!==c&&(b.lastValue=c,b.onSearchChange(c),b.refreshOptions(),b.trigger("type",c))},onSearchChange:function(a){var b=this,c=b.settings.load;c&&(b.loadedSearches.hasOwnProperty(a)||(b.loadedSearches[a]=!0,b.load(function(d){c.apply(b,[a,d])})))},onFocus:function(a){var b=this,c=b.isFocused;return b.isDisabled?(b.blur(),a&&a.preventDefault(),!1):void(b.ignoreFocus||(b.isFocused=!0,"focus"===b.settings.preload&&b.onSearchChange(""),c||b.trigger("focus"),b.$activeItems.length||(b.showInput(),b.setActiveItem(null),b.refreshOptions(!!b.settings.openOnFocus)),b.refreshState()))},onBlur:function(a,b){var c=this;if(c.isFocused&&(c.isFocused=!1,!c.ignoreFocus)){if(!c.ignoreBlur&&document.activeElement===c.$dropdown_content[0])return c.ignoreBlur=!0,void c.onFocus(a);var d=function(){c.close(),c.setTextboxValue(""),c.setActiveItem(null),c.setActiveOption(null),c.setCaret(c.items.length),c.refreshState(),(b||document.body).focus(),c.ignoreFocus=!1,c.trigger("blur")};c.ignoreFocus=!0,c.settings.create&&c.settings.createOnBlur?c.createItem(null,!1,d):d()}},onOptionHover:function(a){this.ignoreHover||this.setActiveOption(a.currentTarget,!1)},onOptionSelect:function(b){var c,d,e=this;b.preventDefault&&(b.preventDefault(),b.stopPropagation()),d=a(b.currentTarget),d.hasClass("create")?e.createItem(null,function(){e.settings.closeAfterSelect&&e.close()}):(c=d.attr("data-value"),"undefined"!=typeof c&&(e.lastQuery=null,e.setTextboxValue(""),e.addItem(c),e.settings.closeAfterSelect?e.close():!e.settings.hideSelected&&b.type&&/mouse/.test(b.type)&&e.setActiveOption(e.getOption(c))))},onItemSelect:function(a){var b=this;b.isLocked||"multi"===b.settings.mode&&(a.preventDefault(),b.setActiveItem(a.currentTarget,a))},load:function(a){var b=this,c=b.$wrapper.addClass(b.settings.loadingClass);b.loading++,a.apply(b,[function(a){b.loading=Math.max(b.loading-1,0),a&&a.length&&(b.addOption(a),b.refreshOptions(b.isFocused&&!b.isInputHidden)),b.loading||c.removeClass(b.settings.loadingClass),b.trigger("load",a)}])},setTextboxValue:function(a){var b=this.$control_input,c=b.val()!==a;c&&(b.val(a).triggerHandler("update"),this.lastValue=a)},getValue:function(){return this.tagType===v&&this.$input.attr("multiple")?this.items:this.items.join(this.settings.delimiter)},setValue:function(a,b){var c=b?[]:["change"];F(this,c,function(){this.clear(b),this.addItems(a,b)})},setActiveItem:function(b,c){var d,e,f,g,h,i,j,k,l=this;if("single"!==l.settings.mode){if(b=a(b),!b.length)return a(l.$activeItems).removeClass("active"),l.$activeItems=[],void(l.isFocused&&l.showInput());if(d=c&&c.type.toLowerCase(),"mousedown"===d&&l.isShiftDown&&l.$activeItems.length){for(k=l.$control.children(".active:last"),g=Array.prototype.indexOf.apply(l.$control[0].childNodes,[k[0]]),h=Array.prototype.indexOf.apply(l.$control[0].childNodes,[b[0]]),g>h&&(j=g,g=h,h=j),e=g;h>=e;e++)i=l.$control[0].childNodes[e],-1===l.$activeItems.indexOf(i)&&(a(i).addClass("active"),l.$activeItems.push(i));c.preventDefault()}else"mousedown"===d&&l.isCtrlDown||"keydown"===d&&this.isShiftDown?b.hasClass("active")?(f=l.$activeItems.indexOf(b[0]),l.$activeItems.splice(f,1),b.removeClass("active")):l.$activeItems.push(b.addClass("active")[0]):(a(l.$activeItems).removeClass("active"),l.$activeItems=[b.addClass("active")[0]]);l.hideInput(),this.isFocused||l.focus()}},setActiveOption:function(b,c,d){var e,f,g,h,i,j=this;j.$activeOption&&j.$activeOption.removeClass("active"),j.$activeOption=null,b=a(b),b.length&&(j.$activeOption=b.addClass("active"),(c||!y(c))&&(e=j.$dropdown_content.height(),f=j.$activeOption.outerHeight(!0),c=j.$dropdown_content.scrollTop()||0,g=j.$activeOption.offset().top-j.$dropdown_content.offset().top+c,h=g,i=g-e+f,g+f>e+c?j.$dropdown_content.stop().animate({scrollTop:i},d?j.settings.scrollDuration:0):c>g&&j.$dropdown_content.stop().animate({scrollTop:h},d?j.settings.scrollDuration:0)))},selectAll:function(){var a=this;"single"!==a.settings.mode&&(a.$activeItems=Array.prototype.slice.apply(a.$control.children(":not(input)").addClass("active")),a.$activeItems.length&&(a.hideInput(),a.close()),a.focus())},hideInput:function(){var a=this;a.setTextboxValue(""),a.$control_input.css({opacity:0,position:"absolute",left:a.rtl?1e4:-1e4}),a.isInputHidden=!0},showInput:function(){this.$control_input.css({opacity:1,position:"relative",left:0}),this.isInputHidden=!1},focus:function(){var a=this;a.isDisabled||(a.ignoreFocus=!0,a.$control_input[0].focus(),window.setTimeout(function(){a.ignoreFocus=!1,a.onFocus()},0))},blur:function(a){this.$control_input[0].blur(),this.onBlur(null,a)},getScoreFunction:function(a){return this.sifter.getScoreFunction(a,this.getSearchOptions())},getSearchOptions:function(){var a=this.settings,b=a.sortField;return"string"==typeof b&&(b=[{field:b}]),{fields:a.searchField,conjunction:a.searchConjunction,sort:b}},search:function(b){var c,d,e,f=this,g=f.settings,h=this.getSearchOptions();if(g.score&&(e=f.settings.score.apply(this,[b]),"function"!=typeof e))throw new Error('Selectize "score" setting must be a function that returns a function');if(b!==f.lastQuery?(f.lastQuery=b,d=f.sifter.search(b,a.extend(h,{score:e})),f.currentResults=d):d=a.extend(!0,{},f.currentResults),g.hideSelected)for(c=d.items.length-1;c>=0;c--)-1!==f.items.indexOf(z(d.items[c].id))&&d.items.splice(c,1);return d},refreshOptions:function(b){var c,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s;"undefined"==typeof b&&(b=!0);var t=this,u=a.trim(t.$control_input.val()),v=t.search(u),w=t.$dropdown_content,x=t.$activeOption&&z(t.$activeOption.attr("data-value"));for(g=v.items.length,"number"==typeof t.settings.maxOptions&&(g=Math.min(g,t.settings.maxOptions)),h={},i=[],c=0;g>c;c++)for(j=t.options[v.items[c].id],k=t.render("option",j),l=j[t.settings.optgroupField]||"",m=a.isArray(l)?l:[l],e=0,f=m&&m.length;f>e;e++)l=m[e],t.optgroups.hasOwnProperty(l)||(l=""),h.hasOwnProperty(l)||(h[l]=[],i.push(l)),h[l].push(k);for(this.settings.lockOptgroupOrder&&i.sort(function(a,b){var c=t.optgroups[a].$order||0,d=t.optgroups[b].$order||0;return c-d}),n=[],c=0,g=i.length;g>c;c++)l=i[c],t.optgroups.hasOwnProperty(l)&&h[l].length?(o=t.render("optgroup_header",t.optgroups[l])||"",o+=h[l].join(""),n.push(t.render("optgroup",a.extend({},t.optgroups[l],{html:o})))):n.push(h[l].join(""));if(w.html(n.join("")),t.settings.highlight&&v.query.length&&v.tokens.length)for(c=0,g=v.tokens.length;g>c;c++)d(w,v.tokens[c].regex);if(!t.settings.hideSelected)for(c=0,g=t.items.length;g>c;c++)t.getOption(t.items[c]).addClass("selected");p=t.canCreate(u),p&&(w.prepend(t.render("option_create",{input:u})),s=a(w[0].childNodes[0])),t.hasOptions=v.items.length>0||p,t.hasOptions?(v.items.length>0?(r=x&&t.getOption(x),r&&r.length?q=r:"single"===t.settings.mode&&t.items.length&&(q=t.getOption(t.items[0])),q&&q.length||(q=s&&!t.settings.addPrecedence?t.getAdjacentOption(s,1):w.find("[data-selectable]:first"))):q=s,t.setActiveOption(q),b&&!t.isOpen&&t.open()):(t.setActiveOption(null),b&&t.isOpen&&t.close())},addOption:function(b){var c,d,e,f=this;if(a.isArray(b))for(c=0,d=b.length;d>c;c++)f.addOption(b[c]);else(e=f.registerOption(b))&&(f.userOptions[e]=!0,f.lastQuery=null,f.trigger("option_add",e,b))},registerOption:function(a){var b=z(a[this.settings.valueField]);return!b||this.options.hasOwnProperty(b)?!1:(a.$order=a.$order||++this.order,this.options[b]=a,b)},registerOptionGroup:function(a){var b=z(a[this.settings.optgroupValueField]);return b?(a.$order=a.$order||++this.order,this.optgroups[b]=a,b):!1},addOptionGroup:function(a,b){b[this.settings.optgroupValueField]=a,(a=this.registerOptionGroup(b))&&this.trigger("optgroup_add",a,b)},removeOptionGroup:function(a){this.optgroups.hasOwnProperty(a)&&(delete this.optgroups[a],this.renderCache={},this.trigger("optgroup_remove",a))},clearOptionGroups:function(){this.optgroups={},this.renderCache={},this.trigger("optgroup_clear")},updateOption:function(b,c){var d,e,f,g,h,i,j,k=this;if(b=z(b),f=z(c[k.settings.valueField]),null!==b&&k.options.hasOwnProperty(b)){if("string"!=typeof f)throw new Error("Value must be set in option data");j=k.options[b].$order,f!==b&&(delete k.options[b],g=k.items.indexOf(b),-1!==g&&k.items.splice(g,1,f)),c.$order=c.$order||j,k.options[f]=c,h=k.renderCache.item,i=k.renderCache.option,h&&(delete h[b],delete h[f]),i&&(delete i[b],delete i[f]),-1!==k.items.indexOf(f)&&(d=k.getItem(b),e=a(k.render("item",c)),d.hasClass("active")&&e.addClass("active"),d.replaceWith(e)),k.lastQuery=null,k.isOpen&&k.refreshOptions(!1)}},removeOption:function(a,b){var c=this;a=z(a);var d=c.renderCache.item,e=c.renderCache.option;d&&delete d[a],e&&delete e[a],delete c.userOptions[a],delete c.options[a],c.lastQuery=null,c.trigger("option_remove",a),c.removeItem(a,b)},clearOptions:function(){var a=this;a.loadedSearches={},a.userOptions={},a.renderCache={},a.options=a.sifter.items={},a.lastQuery=null,a.trigger("option_clear"),a.clear()},getOption:function(a){return this.getElementWithValue(a,this.$dropdown_content.find("[data-selectable]"))},getAdjacentOption:function(b,c){var d=this.$dropdown.find("[data-selectable]"),e=d.index(b)+c;return e>=0&&e<d.length?d.eq(e):a()},getElementWithValue:function(b,c){if(b=z(b),"undefined"!=typeof b&&null!==b)for(var d=0,e=c.length;e>d;d++)if(c[d].getAttribute("data-value")===b)return a(c[d]);return a()},getItem:function(a){return this.getElementWithValue(a,this.$control.children())},addItems:function(b,c){for(var d=a.isArray(b)?b:[b],e=0,f=d.length;f>e;e++)this.isPending=f-1>e,this.addItem(d[e],c)},addItem:function(b,c){var d=c?[]:["change"];F(this,d,function(){var d,e,f,g,h,i=this,j=i.settings.mode;return b=z(b),-1!==i.items.indexOf(b)?void("single"===j&&i.close()):void(i.options.hasOwnProperty(b)&&("single"===j&&i.clear(c),"multi"===j&&i.isFull()||(d=a(i.render("item",i.options[b])),h=i.isFull(),i.items.splice(i.caretPos,0,b),i.insertAtCaret(d),(!i.isPending||!h&&i.isFull())&&i.refreshState(),i.isSetup&&(f=i.$dropdown_content.find("[data-selectable]"),i.isPending||(e=i.getOption(b),g=i.getAdjacentOption(e,1).attr("data-value"),i.refreshOptions(i.isFocused&&"single"!==j),g&&i.setActiveOption(i.getOption(g))),!f.length||i.isFull()?i.close():i.positionDropdown(),i.updatePlaceholder(),i.trigger("item_add",b,d),i.updateOriginalInput({silent:c})))))})},removeItem:function(a,b){var c,d,e,f=this;c="object"==typeof a?a:f.getItem(a),a=z(c.attr("data-value")),d=f.items.indexOf(a),-1!==d&&(c.remove(),c.hasClass("active")&&(e=f.$activeItems.indexOf(c[0]),f.$activeItems.splice(e,1)),f.items.splice(d,1),f.lastQuery=null,!f.settings.persist&&f.userOptions.hasOwnProperty(a)&&f.removeOption(a,b),d<f.caretPos&&f.setCaret(f.caretPos-1),f.refreshState(),f.updatePlaceholder(),f.updateOriginalInput({silent:b}),f.positionDropdown(),f.trigger("item_remove",a,c))},createItem:function(b,c){var d=this,e=d.caretPos;b=b||a.trim(d.$control_input.val()||"");var f=arguments[arguments.length-1];if("function"!=typeof f&&(f=function(){}),"boolean"!=typeof c&&(c=!0),!d.canCreate(b))return f(),!1;d.lock();var g="function"==typeof d.settings.create?this.settings.create:function(a){var b={};return b[d.settings.labelField]=a,b[d.settings.valueField]=a,b},h=D(function(a){if(d.unlock(),!a||"object"!=typeof a)return f();var b=z(a[d.settings.valueField]);return"string"!=typeof b?f():(d.setTextboxValue(""),d.addOption(a),d.setCaret(e),d.addItem(b),d.refreshOptions(c&&"single"!==d.settings.mode),void f(a))}),i=g.apply(this,[b,h]);return"undefined"!=typeof i&&h(i),!0},refreshItems:function(){this.lastQuery=null,this.isSetup&&this.addItem(this.items),this.refreshState(),this.updateOriginalInput()},refreshState:function(){var a,b=this;b.isRequired&&(b.items.length&&(b.isInvalid=!1),b.$control_input.prop("required",a)),b.refreshClasses()},refreshClasses:function(){var b=this,c=b.isFull(),d=b.isLocked;b.$wrapper.toggleClass("rtl",b.rtl),b.$control.toggleClass("focus",b.isFocused).toggleClass("disabled",b.isDisabled).toggleClass("required",b.isRequired).toggleClass("invalid",b.isInvalid).toggleClass("locked",d).toggleClass("full",c).toggleClass("not-full",!c).toggleClass("input-active",b.isFocused&&!b.isInputHidden).toggleClass("dropdown-active",b.isOpen).toggleClass("has-options",!a.isEmptyObject(b.options)).toggleClass("has-items",b.items.length>0),b.$control_input.data("grow",!c&&!d)},isFull:function(){return null!==this.settings.maxItems&&this.items.length>=this.settings.maxItems},updateOriginalInput:function(a){var b,c,d,e,f=this;if(a=a||{},f.tagType===v){for(d=[],b=0,c=f.items.length;c>b;b++)e=f.options[f.items[b]][f.settings.labelField]||"",d.push('<option value="'+A(f.items[b])+'" selected="selected">'+A(e)+"</option>");d.length||this.$input.attr("multiple")||d.push('<option value="" selected="selected"></option>'),f.$input.html(d.join(""))}else f.$input.val(f.getValue()),f.$input.attr("value",f.$input.val());f.isSetup&&(a.silent||f.trigger("change",f.$input.val()))},updatePlaceholder:function(){if(this.settings.placeholder){var a=this.$control_input;this.items.length?a.removeAttr("placeholder"):a.attr("placeholder",this.settings.placeholder),a.triggerHandler("update",{force:!0})}},open:function(){var a=this;a.isLocked||a.isOpen||"multi"===a.settings.mode&&a.isFull()||(a.focus(),a.isOpen=!0,a.refreshState(),a.$dropdown.css({visibility:"hidden",display:"block"}),a.positionDropdown(),a.$dropdown.css({visibility:"visible"}),a.trigger("dropdown_open",a.$dropdown))},close:function(){var a=this,b=a.isOpen;"single"===a.settings.mode&&a.items.length&&a.hideInput(),a.isOpen=!1,a.$dropdown.hide(),a.setActiveOption(null),a.refreshState(),b&&a.trigger("dropdown_close",a.$dropdown)},positionDropdown:function(){var a=this.$control,b="body"===this.settings.dropdownParent?a.offset():a.position();b.top+=a.outerHeight(!0),this.$dropdown.css({width:a.outerWidth(),top:b.top,left:b.left})},clear:function(a){var b=this;b.items.length&&(b.$control.children(":not(input)").remove(),b.items=[],b.lastQuery=null,b.setCaret(0),b.setActiveItem(null),b.updatePlaceholder(),b.updateOriginalInput({silent:a}),b.refreshState(),b.showInput(),b.trigger("clear"))},insertAtCaret:function(b){var c=Math.min(this.caretPos,this.items.length);0===c?this.$control.prepend(b):a(this.$control[0].childNodes[c]).before(b),this.setCaret(c+1)},deleteSelection:function(b){var c,d,e,f,g,h,i,j,k,l=this;if(e=b&&b.keyCode===p?-1:1,f=H(l.$control_input[0]),l.$activeOption&&!l.settings.hideSelected&&(i=l.getAdjacentOption(l.$activeOption,-1).attr("data-value")),g=[],l.$activeItems.length){for(k=l.$control.children(".active:"+(e>0?"last":"first")),h=l.$control.children(":not(input)").index(k),e>0&&h++,c=0,d=l.$activeItems.length;d>c;c++)g.push(a(l.$activeItems[c]).attr("data-value"));
b&&(b.preventDefault(),b.stopPropagation())}else(l.isFocused||"single"===l.settings.mode)&&l.items.length&&(0>e&&0===f.start&&0===f.length?g.push(l.items[l.caretPos-1]):e>0&&f.start===l.$control_input.val().length&&g.push(l.items[l.caretPos]));if(!g.length||"function"==typeof l.settings.onDelete&&l.settings.onDelete.apply(l,[g])===!1)return!1;for("undefined"!=typeof h&&l.setCaret(h);g.length;)l.removeItem(g.pop());return l.showInput(),l.positionDropdown(),l.refreshOptions(!0),i&&(j=l.getOption(i),j.length&&l.setActiveOption(j)),!0},advanceSelection:function(a,b){var c,d,e,f,g,h,i=this;0!==a&&(i.rtl&&(a*=-1),c=a>0?"last":"first",d=H(i.$control_input[0]),i.isFocused&&!i.isInputHidden?(f=i.$control_input.val().length,g=0>a?0===d.start&&0===d.length:d.start===f,g&&!f&&i.advanceCaret(a,b)):(h=i.$control.children(".active:"+c),h.length&&(e=i.$control.children(":not(input)").index(h),i.setActiveItem(null),i.setCaret(a>0?e+1:e))))},advanceCaret:function(a,b){var c,d,e=this;0!==a&&(c=a>0?"next":"prev",e.isShiftDown?(d=e.$control_input[c](),d.length&&(e.hideInput(),e.setActiveItem(d),b&&b.preventDefault())):e.setCaret(e.caretPos+a))},setCaret:function(b){var c=this;if(b="single"===c.settings.mode?c.items.length:Math.max(0,Math.min(c.items.length,b)),!c.isPending){var d,e,f,g;for(f=c.$control.children(":not(input)"),d=0,e=f.length;e>d;d++)g=a(f[d]).detach(),b>d?c.$control_input.before(g):c.$control.append(g)}c.caretPos=b},lock:function(){this.close(),this.isLocked=!0,this.refreshState()},unlock:function(){this.isLocked=!1,this.refreshState()},disable:function(){var a=this;a.$input.prop("disabled",!0),a.$control_input.prop("disabled",!0).prop("tabindex",-1),a.isDisabled=!0,a.lock()},enable:function(){var a=this;a.$input.prop("disabled",!1),a.$control_input.prop("disabled",!1).prop("tabindex",a.tabIndex),a.isDisabled=!1,a.unlock()},destroy:function(){var b=this,c=b.eventNS,d=b.revertSettings;b.trigger("destroy"),b.off(),b.$wrapper.remove(),b.$dropdown.remove(),b.$input.html("").append(d.$children).removeAttr("tabindex").removeClass("selectized").attr({tabindex:d.tabindex}).show(),b.$control_input.removeData("grow"),b.$input.removeData("selectize"),a(window).off(c),a(document).off(c),a(document.body).off(c),delete b.$input[0].selectize},render:function(a,b){var c,d,e="",f=!1,g=this,h=/^[\t \r\n]*<([a-z][a-z0-9\-_]*(?:\:[a-z][a-z0-9\-_]*)?)/i;return("option"===a||"item"===a)&&(c=z(b[g.settings.valueField]),f=!!c),f&&(y(g.renderCache[a])||(g.renderCache[a]={}),g.renderCache[a].hasOwnProperty(c))?g.renderCache[a][c]:(e=g.settings.render[a].apply(this,[b,A]),("option"===a||"option_create"===a)&&(e=e.replace(h,"<$1 data-selectable")),"optgroup"===a&&(d=b[g.settings.optgroupValueField]||"",e=e.replace(h,'<$1 data-group="'+B(A(d))+'"')),("option"===a||"item"===a)&&(e=e.replace(h,'<$1 data-value="'+B(A(c||""))+'"')),f&&(g.renderCache[a][c]=e),e)},clearCache:function(a){var b=this;"undefined"==typeof a?b.renderCache={}:delete b.renderCache[a]},canCreate:function(a){var b=this;if(!b.settings.create)return!1;var c=b.settings.createFilter;return!(!a.length||"function"==typeof c&&!c.apply(b,[a])||"string"==typeof c&&!new RegExp(c).test(a)||c instanceof RegExp&&!c.test(a))}}),L.count=0,L.defaults={options:[],optgroups:[],plugins:[],delimiter:",",splitOn:null,persist:!0,diacritics:!0,create:!1,createOnBlur:!1,createFilter:null,highlight:!0,openOnFocus:!0,maxOptions:1e3,maxItems:null,hideSelected:null,addPrecedence:!1,selectOnTab:!1,preload:!1,allowEmptyOption:!1,closeAfterSelect:!1,scrollDuration:60,loadThrottle:300,loadingClass:"loading",dataAttr:"data-data",optgroupField:"optgroup",valueField:"value",labelField:"text",optgroupLabelField:"label",optgroupValueField:"value",lockOptgroupOrder:!1,sortField:"$order",searchField:["text"],searchConjunction:"and",mode:null,wrapperClass:"selectize-control",inputClass:"selectize-input",dropdownClass:"selectize-dropdown",dropdownContentClass:"selectize-dropdown-content",dropdownParent:null,copyClassesToDropdown:!0,render:{}},a.fn.selectize=function(b){var c=a.fn.selectize.defaults,d=a.extend({},c,b),e=d.dataAttr,f=d.labelField,g=d.valueField,h=d.optgroupField,i=d.optgroupLabelField,j=d.optgroupValueField,k=function(b,c){var h,i,j,k,l=b.attr(e);if(l)for(c.options=JSON.parse(l),h=0,i=c.options.length;i>h;h++)c.items.push(c.options[h][g]);else{var m=a.trim(b.val()||"");if(!d.allowEmptyOption&&!m.length)return;for(j=m.split(d.delimiter),h=0,i=j.length;i>h;h++)k={},k[f]=j[h],k[g]=j[h],c.options.push(k);c.items=j}},l=function(b,c){var k,l,m,n,o=c.options,p={},q=function(a){var b=e&&a.attr(e);return"string"==typeof b&&b.length?JSON.parse(b):null},r=function(b,e){b=a(b);var i=z(b.attr("value"));if(i||d.allowEmptyOption)if(p.hasOwnProperty(i)){if(e){var j=p[i][h];j?a.isArray(j)?j.push(e):p[i][h]=[j,e]:p[i][h]=e}}else{var k=q(b)||{};k[f]=k[f]||b.text(),k[g]=k[g]||i,k[h]=k[h]||e,p[i]=k,o.push(k),b.is(":selected")&&c.items.push(i)}},s=function(b){var d,e,f,g,h;for(b=a(b),f=b.attr("label"),f&&(g=q(b)||{},g[i]=f,g[j]=f,c.optgroups.push(g)),h=a("option",b),d=0,e=h.length;e>d;d++)r(h[d],f)};for(c.maxItems=b.attr("multiple")?null:1,n=b.children(),k=0,l=n.length;l>k;k++)m=n[k].tagName.toLowerCase(),"optgroup"===m?s(n[k]):"option"===m&&r(n[k])};return this.each(function(){if(!this.selectize){var e,f=a(this),g=this.tagName.toLowerCase(),h=f.attr("placeholder")||f.attr("data-placeholder");h||d.allowEmptyOption||(h=f.children('option[value=""]').text());var i={placeholder:h,options:[],optgroups:[],items:[]};"select"===g?l(f,i):k(f,i),e=new L(f,a.extend(!0,{},c,i,b))}})},a.fn.selectize.defaults=L.defaults,a.fn.selectize.support={validity:x},L.define("drag_drop",function(){if(!a.fn.sortable)throw new Error('The "drag_drop" plugin requires jQuery UI "sortable".');if("multi"===this.settings.mode){var b=this;b.lock=function(){var a=b.lock;return function(){var c=b.$control.data("sortable");return c&&c.disable(),a.apply(b,arguments)}}(),b.unlock=function(){var a=b.unlock;return function(){var c=b.$control.data("sortable");return c&&c.enable(),a.apply(b,arguments)}}(),b.setup=function(){var c=b.setup;return function(){c.apply(this,arguments);var d=b.$control.sortable({items:"[data-value]",forcePlaceholderSize:!0,disabled:b.isLocked,start:function(a,b){b.placeholder.css("width",b.helper.css("width")),d.css({overflow:"visible"})},stop:function(){d.css({overflow:"hidden"});var c=b.$activeItems?b.$activeItems.slice():null,e=[];d.children("[data-value]").each(function(){e.push(a(this).attr("data-value"))}),b.setValue(e),b.setActiveItem(c)}})}}()}}),L.define("dropdown_header",function(b){var c=this;b=a.extend({title:"Untitled",headerClass:"selectize-dropdown-header",titleRowClass:"selectize-dropdown-header-title",labelClass:"selectize-dropdown-header-label",closeClass:"selectize-dropdown-header-close",html:function(a){return'<div class="'+a.headerClass+'"><div class="'+a.titleRowClass+'"><span class="'+a.labelClass+'">'+a.title+'</span><a href="javascript:void(0)" class="'+a.closeClass+'">&times;</a></div></div>'}},b),c.setup=function(){var d=c.setup;return function(){d.apply(c,arguments),c.$dropdown_header=a(b.html(b)),c.$dropdown.prepend(c.$dropdown_header)}}()}),L.define("optgroup_columns",function(b){var c=this;b=a.extend({equalizeWidth:!0,equalizeHeight:!0},b),this.getAdjacentOption=function(b,c){var d=b.closest("[data-group]").find("[data-selectable]"),e=d.index(b)+c;return e>=0&&e<d.length?d.eq(e):a()},this.onKeyDown=function(){var a=c.onKeyDown;return function(b){var d,e,f,g;return!this.isOpen||b.keyCode!==j&&b.keyCode!==m?a.apply(this,arguments):(c.ignoreHover=!0,g=this.$activeOption.closest("[data-group]"),d=g.find("[data-selectable]").index(this.$activeOption),g=b.keyCode===j?g.prev("[data-group]"):g.next("[data-group]"),f=g.find("[data-selectable]"),e=f.eq(Math.min(f.length-1,d)),void(e.length&&this.setActiveOption(e)))}}();var d=function(){var a,b=d.width,c=document;return"undefined"==typeof b&&(a=c.createElement("div"),a.innerHTML='<div style="width:50px;height:50px;position:absolute;left:-50px;top:-50px;overflow:auto;"><div style="width:1px;height:100px;"></div></div>',a=a.firstChild,c.body.appendChild(a),b=d.width=a.offsetWidth-a.clientWidth,c.body.removeChild(a)),b},e=function(){var e,f,g,h,i,j,k;if(k=a("[data-group]",c.$dropdown_content),f=k.length,f&&c.$dropdown_content.width()){if(b.equalizeHeight){for(g=0,e=0;f>e;e++)g=Math.max(g,k.eq(e).height());k.css({height:g})}b.equalizeWidth&&(j=c.$dropdown_content.innerWidth()-d(),h=Math.round(j/f),k.css({width:h}),f>1&&(i=j-h*(f-1),k.eq(f-1).css({width:i})))}};(b.equalizeHeight||b.equalizeWidth)&&(C.after(this,"positionDropdown",e),C.after(this,"refreshOptions",e))}),L.define("remove_button",function(b){if("single"!==this.settings.mode){b=a.extend({label:"&times;",title:"Remove",className:"remove",append:!0},b);var c=this,d='<a href="javascript:void(0)" class="'+b.className+'" tabindex="-1" title="'+A(b.title)+'">'+b.label+"</a>",e=function(a,b){var c=a.search(/(<\/[^>]+>\s*)$/);return a.substring(0,c)+b+a.substring(c)};this.setup=function(){var f=c.setup;return function(){if(b.append){var g=c.settings.render.item;c.settings.render.item=function(){return e(g.apply(this,arguments),d)}}f.apply(this,arguments),this.$control.on("click","."+b.className,function(b){if(b.preventDefault(),!c.isLocked){var d=a(b.currentTarget).parent();c.setActiveItem(d),c.deleteSelection()&&c.setCaret(c.items.length)}})}}()}}),L.define("restore_on_backspace",function(a){var b=this;a.text=a.text||function(a){return a[this.settings.labelField]},this.onKeyDown=function(){var c=b.onKeyDown;return function(b){var d,e;return b.keyCode===p&&""===this.$control_input.val()&&!this.$activeItems.length&&(d=this.caretPos-1,d>=0&&d<this.items.length)?(e=this.options[this.items[d]],this.deleteSelection(b)&&(this.setTextboxValue(a.text.apply(this,[e])),this.refreshOptions(!0)),void b.preventDefault()):c.apply(this,arguments)}}()}),L});
/**
 * vue-selectize v0.0.3
 * 
 * Copyright (c)  Michael Owens, contributors.
 * Licensed under the ISC license.
 */
!function(e,t){function t(){return function(e,t){t=t||{};var s=t.directive||"selectize";e.directive(s,i)}}var i={twoWay:!0,selectizeSettings:{},bind:function(){var e,t=this.el.getAttribute("options"),i=this.el.getAttribute("settings"),s=this;if(t&&(e=this.vm.$eval(t),this.vm.$watch(t,this.optionsChange.bind(this))),this.selectizeSettings={options:e,onChange:function(e){s.set(e),s.nativeEvent("change").call()},onFocus:this.nativeEvent("focus").bind(this),onBlur:this.nativeEvent("blur").bind(this)},i){var n=this.vm.$eval(i);this.selectizeSettings=$.extend({},this.selectizeSettings,n),this.vm.$watch(i,this.settingsChange.bind(this),{deep:!0})}$(this.el).selectize(this.selectizeSettings)},nativeEvent:function(e){var t=this;return function(){var i=new Event(e);t.el.dispatchEvent(i)}},optionsChange:function(e){var t=this.el.selectize,i=this.el.selectize.getValue();t.clearOptions(),t.addOption(e),t.refreshOptions(!1),t.setValue(i)},settingsChange:function(e){var t=this.el.selectize.getValue();this.selectizeSettings=$.extend({},this.selectizeSettings,e),this.el.selectize.destroy(),$(this.el).selectize(this.selectizeSettings),this.el.selectize.setValue(t)},update:function(e){this.el.selectize.setValue(e)},unbind:function(){this.el.selectize.destroy()}};"object"==typeof exports&&"object"==typeof module?module.exports=t():"function"==typeof define&&define.amd?define([],t):"object"==typeof exports?exports["vue-selectize"]=t():e["vue-selectize"]=t()}(this);
/** Shrinkwrap URL:
 *      /v2/bundles/js?modules=fastclick%401.0.6%2Co-autoinit%401.2.0&shrinkwrap=
 */
!function(t){function e(o){if(n[o])return n[o].exports;var i=n[o]={exports:{},id:o,loaded:!1};return t[o].call(i.exports,i,i.exports,e),i.loaded=!0,i.exports}var n={};return e.m=t,e.c=n,e.p="",e(0)}([function(t,e,n){"use strict";n(1),window.Origami={fastclick:n(2),"o-autoinit":n(4)}},function(t,e){t.exports={name:"__MAIN__",dependencies:{fastclick:"fastclick#*","o-autoinit":"o-autoinit#^1.0.0"}}},function(t,e,n){t.exports=n(3)},function(t,e){"use strict";var n=!1;!function(){/**
	  * @preserve FastClick: polyfill to remove click delays on browsers with touch UIs.
	  *
	  * @codingstandard ftlabs-jsv2
	  * @copyright The Financial Times Limited [All Rights Reserved]
	  * @license MIT License (see LICENSE.txt)
	  */
function e(t,n){function o(t,e){return function(){return t.apply(e,arguments)}}var r;if(n=n||{},this.trackingClick=!1,this.trackingClickStart=0,this.targetElement=null,this.touchStartX=0,this.touchStartY=0,this.lastTouchIdentifier=0,this.touchBoundary=n.touchBoundary||10,this.layer=t,this.tapDelay=n.tapDelay||200,this.tapTimeout=n.tapTimeout||700,!e.notNeeded(t)){for(var a=["onMouse","onClick","onTouchStart","onTouchMove","onTouchEnd","onTouchCancel"],c=this,s=0,u=a.length;u>s;s++)c[a[s]]=o(c[a[s]],c);i&&(t.addEventListener("mouseover",this.onMouse,!0),t.addEventListener("mousedown",this.onMouse,!0),t.addEventListener("mouseup",this.onMouse,!0)),t.addEventListener("click",this.onClick,!0),t.addEventListener("touchstart",this.onTouchStart,!1),t.addEventListener("touchmove",this.onTouchMove,!1),t.addEventListener("touchend",this.onTouchEnd,!1),t.addEventListener("touchcancel",this.onTouchCancel,!1),Event.prototype.stopImmediatePropagation||(t.removeEventListener=function(e,n,o){var i=Node.prototype.removeEventListener;"click"===e?i.call(t,e,n.hijacked||n,o):i.call(t,e,n,o)},t.addEventListener=function(e,n,o){var i=Node.prototype.addEventListener;"click"===e?i.call(t,e,n.hijacked||(n.hijacked=function(t){t.propagationStopped||n(t)}),o):i.call(t,e,n,o)}),"function"==typeof t.onclick&&(r=t.onclick,t.addEventListener("click",function(t){r(t)},!1),t.onclick=null)}}var o=navigator.userAgent.indexOf("Windows Phone")>=0,i=navigator.userAgent.indexOf("Android")>0&&!o,r=/iP(ad|hone|od)/.test(navigator.userAgent)&&!o,a=r&&/OS 4_\d(_\d)?/.test(navigator.userAgent),c=r&&/OS [6-7]_\d/.test(navigator.userAgent),s=navigator.userAgent.indexOf("BB10")>0;e.prototype.needsClick=function(t){switch(t.nodeName.toLowerCase()){case"button":case"select":case"textarea":if(t.disabled)return!0;break;case"input":if(r&&"file"===t.type||t.disabled)return!0;break;case"label":case"iframe":case"video":return!0}return/\bneedsclick\b/.test(t.className)},e.prototype.needsFocus=function(t){switch(t.nodeName.toLowerCase()){case"textarea":return!0;case"select":return!i;case"input":switch(t.type){case"button":case"checkbox":case"file":case"image":case"radio":case"submit":return!1}return!t.disabled&&!t.readOnly;default:return/\bneedsfocus\b/.test(t.className)}},e.prototype.sendClick=function(t,e){var n,o;document.activeElement&&document.activeElement!==t&&document.activeElement.blur(),o=e.changedTouches[0],n=document.createEvent("MouseEvents"),n.initMouseEvent(this.determineEventType(t),!0,!0,window,1,o.screenX,o.screenY,o.clientX,o.clientY,!1,!1,!1,!1,0,null),n.forwardedTouchEvent=!0,t.dispatchEvent(n)},e.prototype.determineEventType=function(t){return i&&"select"===t.tagName.toLowerCase()?"mousedown":"click"},e.prototype.focus=function(t){var e;r&&t.setSelectionRange&&0!==t.type.indexOf("date")&&"time"!==t.type&&"month"!==t.type?(e=t.value.length,t.setSelectionRange(e,e)):t.focus()},e.prototype.updateScrollParent=function(t){var e,n;if(e=t.fastClickScrollParent,!e||!e.contains(t)){n=t;do{if(n.scrollHeight>n.offsetHeight){e=n,t.fastClickScrollParent=n;break}n=n.parentElement}while(n)}e&&(e.fastClickLastScrollTop=e.scrollTop)},e.prototype.getTargetElementFromEventTarget=function(t){return t.nodeType===Node.TEXT_NODE?t.parentNode:t},e.prototype.onTouchStart=function(t){var e,n,o;if(t.targetTouches.length>1)return!0;if(e=this.getTargetElementFromEventTarget(t.target),n=t.targetTouches[0],r){if(o=window.getSelection(),o.rangeCount&&!o.isCollapsed)return!0;if(!a){if(n.identifier&&n.identifier===this.lastTouchIdentifier)return t.preventDefault(),!1;this.lastTouchIdentifier=n.identifier,this.updateScrollParent(e)}}return this.trackingClick=!0,this.trackingClickStart=t.timeStamp,this.targetElement=e,this.touchStartX=n.pageX,this.touchStartY=n.pageY,t.timeStamp-this.lastClickTime<this.tapDelay&&t.preventDefault(),!0},e.prototype.touchHasMoved=function(t){var e=t.changedTouches[0],n=this.touchBoundary;return Math.abs(e.pageX-this.touchStartX)>n||Math.abs(e.pageY-this.touchStartY)>n?!0:!1},e.prototype.onTouchMove=function(t){return this.trackingClick?((this.targetElement!==this.getTargetElementFromEventTarget(t.target)||this.touchHasMoved(t))&&(this.trackingClick=!1,this.targetElement=null),!0):!0},e.prototype.findControl=function(t){return void 0!==t.control?t.control:t.htmlFor?document.getElementById(t.htmlFor):t.querySelector("button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea")},e.prototype.onTouchEnd=function(t){var e,n,o,s,u,l=this.targetElement;if(!this.trackingClick)return!0;if(t.timeStamp-this.lastClickTime<this.tapDelay)return this.cancelNextClick=!0,!0;if(t.timeStamp-this.trackingClickStart>this.tapTimeout)return!0;if(this.cancelNextClick=!1,this.lastClickTime=t.timeStamp,n=this.trackingClickStart,this.trackingClick=!1,this.trackingClickStart=0,c&&(u=t.changedTouches[0],l=document.elementFromPoint(u.pageX-window.pageXOffset,u.pageY-window.pageYOffset)||l,l.fastClickScrollParent=this.targetElement.fastClickScrollParent),o=l.tagName.toLowerCase(),"label"===o){if(e=this.findControl(l)){if(this.focus(l),i)return!1;l=e}}else if(this.needsFocus(l))return t.timeStamp-n>100||r&&window.top!==window&&"input"===o?(this.targetElement=null,!1):(this.focus(l),this.sendClick(l,t),r&&"select"===o||(this.targetElement=null,t.preventDefault()),!1);return r&&!a&&(s=l.fastClickScrollParent,s&&s.fastClickLastScrollTop!==s.scrollTop)?!0:(this.needsClick(l)||(t.preventDefault(),this.sendClick(l,t)),!1)},e.prototype.onTouchCancel=function(){this.trackingClick=!1,this.targetElement=null},e.prototype.onMouse=function(t){return this.targetElement?t.forwardedTouchEvent?!0:t.cancelable&&(!this.needsClick(this.targetElement)||this.cancelNextClick)?(t.stopImmediatePropagation?t.stopImmediatePropagation():t.propagationStopped=!0,t.stopPropagation(),t.preventDefault(),!1):!0:!0},e.prototype.onClick=function(t){var e;return this.trackingClick?(this.targetElement=null,this.trackingClick=!1,!0):"submit"===t.target.type&&0===t.detail?!0:(e=this.onMouse(t),e||(this.targetElement=null),e)},e.prototype.destroy=function(){var t=this.layer;i&&(t.removeEventListener("mouseover",this.onMouse,!0),t.removeEventListener("mousedown",this.onMouse,!0),t.removeEventListener("mouseup",this.onMouse,!0)),t.removeEventListener("click",this.onClick,!0),t.removeEventListener("touchstart",this.onTouchStart,!1),t.removeEventListener("touchmove",this.onTouchMove,!1),t.removeEventListener("touchend",this.onTouchEnd,!1),t.removeEventListener("touchcancel",this.onTouchCancel,!1)},e.notNeeded=function(t){var e,n,o,r;if("undefined"==typeof window.ontouchstart)return!0;if(n=+(/Chrome\/([0-9]+)/.exec(navigator.userAgent)||[,0])[1]){if(!i)return!0;if(e=document.querySelector("meta[name=viewport]")){if(-1!==e.content.indexOf("user-scalable=no"))return!0;if(n>31&&document.documentElement.scrollWidth<=window.outerWidth)return!0}}if(s&&(o=navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/),o[1]>=10&&o[2]>=3&&(e=document.querySelector("meta[name=viewport]")))){if(-1!==e.content.indexOf("user-scalable=no"))return!0;if(document.documentElement.scrollWidth<=window.outerWidth)return!0}return"none"===t.style.msTouchAction||"manipulation"===t.style.touchAction?!0:(r=+(/Firefox\/([0-9]+)/.exec(navigator.userAgent)||[,0])[1],r>=27&&(e=document.querySelector("meta[name=viewport]"),e&&(-1!==e.content.indexOf("user-scalable=no")||document.documentElement.scrollWidth<=window.outerWidth))?!0:"none"===t.style.touchAction||"manipulation"===t.style.touchAction?!0:!1)},e.attach=function(t,n){return new e(t,n)},"function"==typeof n&&"object"==typeof n.amd&&n.amd?n(function(){return e}):"undefined"!=typeof t&&t.exports?(t.exports=e.attach,t.exports.FastClick=e):window.FastClick=e}()},function(t,e,n){t.exports=n(5)},function(t,e){"use strict";function n(t){t in o||(o[t]=!0,document.dispatchEvent(new CustomEvent("o."+t)))}var o={};if(window.addEventListener("load",n.bind(null,"load")),window.addEventListener("load",n.bind(null,"DOMContentLoaded")),document.addEventListener("DOMContentLoaded",n.bind(null,"DOMContentLoaded")),document.onreadystatechange=function(){"complete"===document.readyState?(n("DOMContentLoaded"),n("load")):"interactive"!==document.readyState||document.attachEvent||n("DOMContentLoaded")},"complete"===document.readyState?(n("DOMContentLoaded"),n("load")):"interactive"!==document.readyState||document.attachEvent||n("DOMContentLoaded"),document.attachEvent){var i=!1,r=50;try{i=null==window.frameElement&&document.documentElement}catch(a){}i&&i.doScroll&&!function c(){if(!("DOMContentLoaded"in o)){try{i.doScroll("left")}catch(t){return 5e3>r?setTimeout(c,r*=1.2):void 0}n("DOMContentLoaded")}}()}}]);
!function(e,t,n){"use strict";!function o(e,t,n){function a(s,l){if(!t[s]){if(!e[s]){var i="function"==typeof require&&require;if(!l&&i)return i(s,!0);if(r)return r(s,!0);var u=new Error("Cannot find module '"+s+"'");throw u.code="MODULE_NOT_FOUND",u}var c=t[s]={exports:{}};e[s][0].call(c.exports,function(t){var n=e[s][1][t];return a(n?n:t)},c,c.exports,o,e,t,n)}return t[s].exports}for(var r="function"==typeof require&&require,s=0;s<n.length;s++)a(n[s]);return a}({1:[function(o,a,r){var s=function(e){return e&&e.__esModule?e:{"default":e}};Object.defineProperty(r,"__esModule",{value:!0});var l,i,u,c,d=o("./modules/handle-dom"),f=o("./modules/utils"),p=o("./modules/handle-swal-dom"),m=o("./modules/handle-click"),v=o("./modules/handle-key"),y=s(v),h=o("./modules/default-params"),b=s(h),g=o("./modules/set-params"),w=s(g);r["default"]=u=c=function(){function o(e){var t=a;return t[e]===n?b["default"][e]:t[e]}var a=arguments[0];if(d.addClass(t.body,"stop-scrolling"),p.resetInput(),a===n)return f.logStr("SweetAlert expects at least 1 attribute!"),!1;var r=f.extend({},b["default"]);switch(typeof a){case"string":r.title=a,r.text=arguments[1]||"",r.type=arguments[2]||"";break;case"object":if(a.title===n)return f.logStr('Missing "title" argument!'),!1;r.title=a.title;for(var s in b["default"])r[s]=o(s);r.confirmButtonText=r.showCancelButton?"Confirm":b["default"].confirmButtonText,r.confirmButtonText=o("confirmButtonText"),r.doneFunction=arguments[1]||null;break;default:return f.logStr('Unexpected type of argument! Expected "string" or "object", got '+typeof a),!1}w["default"](r),p.fixVerticalPosition(),p.openModal(arguments[1]);for(var u=p.getModal(),v=u.querySelectorAll("button"),h=["onclick","onmouseover","onmouseout","onmousedown","onmouseup","onfocus"],g=function(e){return m.handleButton(e,r,u)},C=0;C<v.length;C++)for(var S=0;S<h.length;S++){var x=h[S];v[C][x]=g}p.getOverlay().onclick=g,l=e.onkeydown;var k=function(e){return y["default"](e,r,u)};e.onkeydown=k,e.onfocus=function(){setTimeout(function(){i!==n&&(i.focus(),i=n)},0)},c.enableButtons()},u.setDefaults=c.setDefaults=function(e){if(!e)throw new Error("userParams is required");if("object"!=typeof e)throw new Error("userParams has to be a object");f.extend(b["default"],e)},u.close=c.close=function(){var o=p.getModal();d.fadeOut(p.getOverlay(),5),d.fadeOut(o,5),d.removeClass(o,"showSweetAlert"),d.addClass(o,"hideSweetAlert"),d.removeClass(o,"visible");var a=o.querySelector(".sa-icon.sa-success");d.removeClass(a,"animate"),d.removeClass(a.querySelector(".sa-tip"),"animateSuccessTip"),d.removeClass(a.querySelector(".sa-long"),"animateSuccessLong");var r=o.querySelector(".sa-icon.sa-error");d.removeClass(r,"animateErrorIcon"),d.removeClass(r.querySelector(".sa-x-mark"),"animateXMark");var s=o.querySelector(".sa-icon.sa-warning");return d.removeClass(s,"pulseWarning"),d.removeClass(s.querySelector(".sa-body"),"pulseWarningIns"),d.removeClass(s.querySelector(".sa-dot"),"pulseWarningIns"),setTimeout(function(){var e=o.getAttribute("data-custom-class");d.removeClass(o,e)},300),d.removeClass(t.body,"stop-scrolling"),e.onkeydown=l,e.previousActiveElement&&e.previousActiveElement.focus(),i=n,clearTimeout(o.timeout),!0},u.showInputError=c.showInputError=function(e){var t=p.getModal(),n=t.querySelector(".sa-input-error");d.addClass(n,"show");var o=t.querySelector(".sa-error-container");d.addClass(o,"show"),o.querySelector("p").innerHTML=e,setTimeout(function(){u.enableButtons()},1),t.querySelector("input").focus()},u.resetInputError=c.resetInputError=function(e){if(e&&13===e.keyCode)return!1;var t=p.getModal(),n=t.querySelector(".sa-input-error");d.removeClass(n,"show");var o=t.querySelector(".sa-error-container");d.removeClass(o,"show")},u.disableButtons=c.disableButtons=function(){var e=p.getModal(),t=e.querySelector("button.confirm"),n=e.querySelector("button.cancel");t.disabled=!0,n.disabled=!0},u.enableButtons=c.enableButtons=function(){var e=p.getModal(),t=e.querySelector("button.confirm"),n=e.querySelector("button.cancel");t.disabled=!1,n.disabled=!1},"undefined"!=typeof e?e.sweetAlert=e.swal=u:f.logStr("SweetAlert is a frontend module!"),a.exports=r["default"]},{"./modules/default-params":2,"./modules/handle-click":3,"./modules/handle-dom":4,"./modules/handle-key":5,"./modules/handle-swal-dom":6,"./modules/set-params":8,"./modules/utils":9}],2:[function(e,t,n){Object.defineProperty(n,"__esModule",{value:!0});var o={title:"",text:"",type:null,allowOutsideClick:!1,showConfirmButton:!0,showCancelButton:!1,closeOnConfirm:!0,closeOnCancel:!0,confirmButtonText:"OK",confirmButtonColor:"#8CD4F5",cancelButtonText:"Cancel",imageUrl:null,imageSize:null,timer:null,customClass:"",html:!1,animation:!0,allowEscapeKey:!0,inputType:"text",inputPlaceholder:"",inputValue:"",showLoaderOnConfirm:!1};n["default"]=o,t.exports=n["default"]},{}],3:[function(t,n,o){Object.defineProperty(o,"__esModule",{value:!0});var a=t("./utils"),r=(t("./handle-swal-dom"),t("./handle-dom")),s=function(t,n,o){function s(e){m&&n.confirmButtonColor&&(p.style.backgroundColor=e)}var u,c,d,f=t||e.event,p=f.target||f.srcElement,m=-1!==p.className.indexOf("confirm"),v=-1!==p.className.indexOf("sweet-overlay"),y=r.hasClass(o,"visible"),h=n.doneFunction&&"true"===o.getAttribute("data-has-done-function");switch(m&&n.confirmButtonColor&&(u=n.confirmButtonColor,c=a.colorLuminance(u,-.04),d=a.colorLuminance(u,-.14)),f.type){case"mouseover":s(c);break;case"mouseout":s(u);break;case"mousedown":s(d);break;case"mouseup":s(c);break;case"focus":var b=o.querySelector("button.confirm"),g=o.querySelector("button.cancel");m?g.style.boxShadow="none":b.style.boxShadow="none";break;case"click":var w=o===p,C=r.isDescendant(o,p);if(!w&&!C&&y&&!n.allowOutsideClick)break;m&&h&&y?l(o,n):h&&y||v?i(o,n):r.isDescendant(o,p)&&"BUTTON"===p.tagName&&sweetAlert.close()}},l=function(e,t){var n=!0;r.hasClass(e,"show-input")&&(n=e.querySelector("input").value,n||(n="")),t.doneFunction(n),t.closeOnConfirm&&sweetAlert.close(),t.showLoaderOnConfirm&&sweetAlert.disableButtons()},i=function(e,t){var n=String(t.doneFunction).replace(/\s/g,""),o="function("===n.substring(0,9)&&")"!==n.substring(9,10);o&&t.doneFunction(!1),t.closeOnCancel&&sweetAlert.close()};o["default"]={handleButton:s,handleConfirm:l,handleCancel:i},n.exports=o["default"]},{"./handle-dom":4,"./handle-swal-dom":6,"./utils":9}],4:[function(n,o,a){Object.defineProperty(a,"__esModule",{value:!0});var r=function(e,t){return new RegExp(" "+t+" ").test(" "+e.className+" ")},s=function(e,t){r(e,t)||(e.className+=" "+t)},l=function(e,t){var n=" "+e.className.replace(/[\t\r\n]/g," ")+" ";if(r(e,t)){for(;n.indexOf(" "+t+" ")>=0;)n=n.replace(" "+t+" "," ");e.className=n.replace(/^\s+|\s+$/g,"")}},i=function(e){var n=t.createElement("div");return n.appendChild(t.createTextNode(e)),n.innerHTML},u=function(e){e.style.opacity="",e.style.display="block"},c=function(e){if(e&&!e.length)return u(e);for(var t=0;t<e.length;++t)u(e[t])},d=function(e){e.style.opacity="",e.style.display="none"},f=function(e){if(e&&!e.length)return d(e);for(var t=0;t<e.length;++t)d(e[t])},p=function(e,t){for(var n=t.parentNode;null!==n;){if(n===e)return!0;n=n.parentNode}return!1},m=function(e){e.style.left="-9999px",e.style.display="block";var t,n=e.clientHeight;return t="undefined"!=typeof getComputedStyle?parseInt(getComputedStyle(e).getPropertyValue("padding-top"),10):parseInt(e.currentStyle.padding),e.style.left="",e.style.display="none","-"+parseInt((n+t)/2)+"px"},v=function(e,t){if(+e.style.opacity<1){t=t||16,e.style.opacity=0,e.style.display="block";var n=+new Date,o=function(e){function t(){return e.apply(this,arguments)}return t.toString=function(){return e.toString()},t}(function(){e.style.opacity=+e.style.opacity+(new Date-n)/100,n=+new Date,+e.style.opacity<1&&setTimeout(o,t)});o()}e.style.display="block"},y=function(e,t){t=t||16,e.style.opacity=1;var n=+new Date,o=function(e){function t(){return e.apply(this,arguments)}return t.toString=function(){return e.toString()},t}(function(){e.style.opacity=+e.style.opacity-(new Date-n)/100,n=+new Date,+e.style.opacity>0?setTimeout(o,t):e.style.display="none"});o()},h=function(n){if("function"==typeof MouseEvent){var o=new MouseEvent("click",{view:e,bubbles:!1,cancelable:!0});n.dispatchEvent(o)}else if(t.createEvent){var a=t.createEvent("MouseEvents");a.initEvent("click",!1,!1),n.dispatchEvent(a)}else t.createEventObject?n.fireEvent("onclick"):"function"==typeof n.onclick&&n.onclick()},b=function(t){"function"==typeof t.stopPropagation?(t.stopPropagation(),t.preventDefault()):e.event&&e.event.hasOwnProperty("cancelBubble")&&(e.event.cancelBubble=!0)};a.hasClass=r,a.addClass=s,a.removeClass=l,a.escapeHtml=i,a._show=u,a.show=c,a._hide=d,a.hide=f,a.isDescendant=p,a.getTopMargin=m,a.fadeIn=v,a.fadeOut=y,a.fireClick=h,a.stopEventPropagation=b},{}],5:[function(t,o,a){Object.defineProperty(a,"__esModule",{value:!0});var r=t("./handle-dom"),s=t("./handle-swal-dom"),l=function(t,o,a){var l=t||e.event,i=l.keyCode||l.which,u=a.querySelector("button.confirm"),c=a.querySelector("button.cancel"),d=a.querySelectorAll("button[tabindex]");if(-1!==[9,13,32,27].indexOf(i)){for(var f=l.target||l.srcElement,p=-1,m=0;m<d.length;m++)if(f===d[m]){p=m;break}9===i?(f=-1===p?u:p===d.length-1?d[0]:d[p+1],r.stopEventPropagation(l),f.focus(),o.confirmButtonColor&&s.setFocusStyle(f,o.confirmButtonColor)):13===i?("INPUT"===f.tagName&&(f=u,u.focus()),f=-1===p?u:n):27===i&&o.allowEscapeKey===!0?(f=c,r.fireClick(f,l)):f=n}};a["default"]=l,o.exports=a["default"]},{"./handle-dom":4,"./handle-swal-dom":6}],6:[function(n,o,a){var r=function(e){return e&&e.__esModule?e:{"default":e}};Object.defineProperty(a,"__esModule",{value:!0});var s=n("./utils"),l=n("./handle-dom"),i=n("./default-params"),u=r(i),c=n("./injected-html"),d=r(c),f=".sweet-alert",p=".sweet-overlay",m=function(){var e=t.createElement("div");for(e.innerHTML=d["default"];e.firstChild;)t.body.appendChild(e.firstChild)},v=function(e){function t(){return e.apply(this,arguments)}return t.toString=function(){return e.toString()},t}(function(){var e=t.querySelector(f);return e||(m(),e=v()),e}),y=function(){var e=v();return e?e.querySelector("input"):void 0},h=function(){return t.querySelector(p)},b=function(e,t){var n=s.hexToRgb(t);e.style.boxShadow="0 0 2px rgba("+n+", 0.8), inset 0 0 0 1px rgba(0, 0, 0, 0.05)"},g=function(n){var o=v();l.fadeIn(h(),10),l.show(o),l.addClass(o,"showSweetAlert"),l.removeClass(o,"hideSweetAlert"),e.previousActiveElement=t.activeElement;var a=o.querySelector("button.confirm");a.focus(),setTimeout(function(){l.addClass(o,"visible")},500);var r=o.getAttribute("data-timer");if("null"!==r&&""!==r){var s=n;o.timeout=setTimeout(function(){var e=(s||null)&&"true"===o.getAttribute("data-has-done-function");e?s(null):sweetAlert.close()},r)}},w=function(){var e=v(),t=y();l.removeClass(e,"show-input"),t.value=u["default"].inputValue,t.setAttribute("type",u["default"].inputType),t.setAttribute("placeholder",u["default"].inputPlaceholder),C()},C=function(e){if(e&&13===e.keyCode)return!1;var t=v(),n=t.querySelector(".sa-input-error");l.removeClass(n,"show");var o=t.querySelector(".sa-error-container");l.removeClass(o,"show")},S=function(){var e=v();e.style.marginTop=l.getTopMargin(v())};a.sweetAlertInitialize=m,a.getModal=v,a.getOverlay=h,a.getInput=y,a.setFocusStyle=b,a.openModal=g,a.resetInput=w,a.resetInputError=C,a.fixVerticalPosition=S},{"./default-params":2,"./handle-dom":4,"./injected-html":7,"./utils":9}],7:[function(e,t,n){Object.defineProperty(n,"__esModule",{value:!0});var o='<div class="sweet-overlay" tabIndex="-1"></div><div class="sweet-alert"><div class="sa-icon sa-error">\n      <span class="sa-x-mark">\n        <span class="sa-line sa-left"></span>\n        <span class="sa-line sa-right"></span>\n      </span>\n    </div><div class="sa-icon sa-warning">\n      <span class="sa-body"></span>\n      <span class="sa-dot"></span>\n    </div><div class="sa-icon sa-info"></div><div class="sa-icon sa-success">\n      <span class="sa-line sa-tip"></span>\n      <span class="sa-line sa-long"></span>\n\n      <div class="sa-placeholder"></div>\n      <div class="sa-fix"></div>\n    </div><div class="sa-icon sa-custom"></div><h2>Title</h2>\n    <p>Text</p>\n    <fieldset>\n      <input type="text" tabIndex="3" />\n      <div class="sa-input-error"></div>\n    </fieldset><div class="sa-error-container">\n      <div class="icon">!</div>\n      <p>Not valid!</p>\n    </div><div class="sa-button-container">\n      <button class="cancel" tabIndex="2">Cancel</button>\n      <div class="sa-confirm-button-container">\n        <button class="confirm" tabIndex="1">OK</button><div class="la-ball-fall">\n          <div></div>\n          <div></div>\n          <div></div>\n        </div>\n      </div>\n    </div></div>';n["default"]=o,t.exports=n["default"]},{}],8:[function(e,t,o){Object.defineProperty(o,"__esModule",{value:!0});var a=e("./utils"),r=e("./handle-swal-dom"),s=e("./handle-dom"),l=["error","warning","info","success","input","prompt"],i=function(e){var t=r.getModal(),o=t.querySelector("h2"),i=t.querySelector("p"),u=t.querySelector("button.cancel"),c=t.querySelector("button.confirm");if(o.innerHTML=e.html?e.title:s.escapeHtml(e.title).split("\n").join("<br>"),i.innerHTML=e.html?e.text:s.escapeHtml(e.text||"").split("\n").join("<br>"),e.text&&s.show(i),e.customClass)s.addClass(t,e.customClass),t.setAttribute("data-custom-class",e.customClass);else{var d=t.getAttribute("data-custom-class");s.removeClass(t,d),t.setAttribute("data-custom-class","")}if(s.hide(t.querySelectorAll(".sa-icon")),e.type&&!a.isIE8()){var f=function(){for(var o=!1,a=0;a<l.length;a++)if(e.type===l[a]){o=!0;break}if(!o)return logStr("Unknown alert type: "+e.type),{v:!1};var i=["success","error","warning","info"],u=n;-1!==i.indexOf(e.type)&&(u=t.querySelector(".sa-icon.sa-"+e.type),s.show(u));var c=r.getInput();switch(e.type){case"success":s.addClass(u,"animate"),s.addClass(u.querySelector(".sa-tip"),"animateSuccessTip"),s.addClass(u.querySelector(".sa-long"),"animateSuccessLong");break;case"error":s.addClass(u,"animateErrorIcon"),s.addClass(u.querySelector(".sa-x-mark"),"animateXMark");break;case"warning":s.addClass(u,"pulseWarning"),s.addClass(u.querySelector(".sa-body"),"pulseWarningIns"),s.addClass(u.querySelector(".sa-dot"),"pulseWarningIns");break;case"input":case"prompt":c.setAttribute("type",e.inputType),c.value=e.inputValue,c.setAttribute("placeholder",e.inputPlaceholder),s.addClass(t,"show-input"),setTimeout(function(){c.focus(),c.addEventListener("keyup",swal.resetInputError)},400)}}();if("object"==typeof f)return f.v}if(e.imageUrl){var p=t.querySelector(".sa-icon.sa-custom");p.style.backgroundImage="url("+e.imageUrl+")",s.show(p);var m=80,v=80;if(e.imageSize){var y=e.imageSize.toString().split("x"),h=y[0],b=y[1];h&&b?(m=h,v=b):logStr("Parameter imageSize expects value with format WIDTHxHEIGHT, got "+e.imageSize)}p.setAttribute("style",p.getAttribute("style")+"width:"+m+"px; height:"+v+"px")}t.setAttribute("data-has-cancel-button",e.showCancelButton),e.showCancelButton?u.style.display="inline-block":s.hide(u),t.setAttribute("data-has-confirm-button",e.showConfirmButton),e.showConfirmButton?c.style.display="inline-block":s.hide(c),e.cancelButtonText&&(u.innerHTML=s.escapeHtml(e.cancelButtonText)),e.confirmButtonText&&(c.innerHTML=s.escapeHtml(e.confirmButtonText)),e.confirmButtonColor&&(c.style.backgroundColor=e.confirmButtonColor,c.style.borderLeftColor=e.confirmLoadingButtonColor,c.style.borderRightColor=e.confirmLoadingButtonColor,r.setFocusStyle(c,e.confirmButtonColor)),t.setAttribute("data-allow-outside-click",e.allowOutsideClick);var g=e.doneFunction?!0:!1;t.setAttribute("data-has-done-function",g),e.animation?"string"==typeof e.animation?t.setAttribute("data-animation",e.animation):t.setAttribute("data-animation","pop"):t.setAttribute("data-animation","none"),t.setAttribute("data-timer",e.timer)};o["default"]=i,t.exports=o["default"]},{"./handle-dom":4,"./handle-swal-dom":6,"./utils":9}],9:[function(t,n,o){Object.defineProperty(o,"__esModule",{value:!0});var a=function(e,t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n]);return e},r=function(e){var t=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(e);return t?parseInt(t[1],16)+", "+parseInt(t[2],16)+", "+parseInt(t[3],16):null},s=function(){return e.attachEvent&&!e.addEventListener},l=function(t){e.console&&e.console.log("SweetAlert: "+t)},i=function(e,t){e=String(e).replace(/[^0-9a-f]/gi,""),e.length<6&&(e=e[0]+e[0]+e[1]+e[1]+e[2]+e[2]),t=t||0;var n,o,a="#";for(o=0;3>o;o++)n=parseInt(e.substr(2*o,2),16),n=Math.round(Math.min(Math.max(0,n+n*t),255)).toString(16),a+=("00"+n).substr(n.length);return a};o.extend=a,o.hexToRgb=r,o.isIE8=s,o.logStr=l,o.colorLuminance=i},{}]},{},[1]),"function"==typeof define&&define.amd?define(function(){return sweetAlert}):"undefined"!=typeof module&&module.exports&&(module.exports=sweetAlert)}(window,document);
/*!
 * accounting.js v0.4.2, copyright 2014 Open Exchange Rates, MIT license, http://openexchangerates.github.io/accounting.js
 */
(function(p,z){function q(a){return!!(""===a||a&&a.charCodeAt&&a.substr)}function m(a){return u?u(a):"[object Array]"===v.call(a)}function r(a){return"[object Object]"===v.call(a)}function s(a,b){var d,a=a||{},b=b||{};for(d in b)b.hasOwnProperty(d)&&null==a[d]&&(a[d]=b[d]);return a}function j(a,b,d){var c=[],e,h;if(!a)return c;if(w&&a.map===w)return a.map(b,d);for(e=0,h=a.length;e<h;e++)c[e]=b.call(d,a[e],e,a);return c}function n(a,b){a=Math.round(Math.abs(a));return isNaN(a)?b:a}function x(a){var b=c.settings.currency.format;"function"===typeof a&&(a=a());return q(a)&&a.match("%v")?{pos:a,neg:a.replace("-","").replace("%v","-%v"),zero:a}:!a||!a.pos||!a.pos.match("%v")?!q(b)?b:c.settings.currency.format={pos:b,neg:b.replace("%v","-%v"),zero:b}:a}var c={version:"0.4.1",settings:{currency:{symbol:"$",format:"%s%v",decimal:".",thousand:",",precision:2,grouping:3},number:{precision:0,grouping:3,thousand:",",decimal:"."}}},w=Array.prototype.map,u=Array.isArray,v=Object.prototype.toString,o=c.unformat=c.parse=function(a,b){if(m(a))return j(a,function(a){return o(a,b)});a=a||0;if("number"===typeof a)return a;var b=b||".",c=RegExp("[^0-9-"+b+"]",["g"]),c=parseFloat((""+a).replace(/\((.*)\)/,"-$1").replace(c,"").replace(b,"."));return!isNaN(c)?c:0},y=c.toFixed=function(a,b){var b=n(b,c.settings.number.precision),d=Math.pow(10,b);return(Math.round(c.unformat(a)*d)/d).toFixed(b)},t=c.formatNumber=c.format=function(a,b,d,i){if(m(a))return j(a,function(a){return t(a,b,d,i)});var a=o(a),e=s(r(b)?b:{precision:b,thousand:d,decimal:i},c.settings.number),h=n(e.precision),f=0>a?"-":"",g=parseInt(y(Math.abs(a||0),h),10)+"",l=3<g.length?g.length%3:0;return f+(l?g.substr(0,l)+e.thousand:"")+g.substr(l).replace(/(\d{3})(?=\d)/g,"$1"+e.thousand)+(h?e.decimal+y(Math.abs(a),h).split(".")[1]:"")},A=c.formatMoney=function(a,b,d,i,e,h){if(m(a))return j(a,function(a){return A(a,b,d,i,e,h)});var a=o(a),f=s(r(b)?b:{symbol:b,precision:d,thousand:i,decimal:e,format:h},c.settings.currency),g=x(f.format);return(0<a?g.pos:0>a?g.neg:g.zero).replace("%s",f.symbol).replace("%v",t(Math.abs(a),n(f.precision),f.thousand,f.decimal))};c.formatColumn=function(a,b,d,i,e,h){if(!a)return[];var f=s(r(b)?b:{symbol:b,precision:d,thousand:i,decimal:e,format:h},c.settings.currency),g=x(f.format),l=g.pos.indexOf("%s")<g.pos.indexOf("%v")?!0:!1,k=0,a=j(a,function(a){if(m(a))return c.formatColumn(a,f);a=o(a);a=(0<a?g.pos:0>a?g.neg:g.zero).replace("%s",f.symbol).replace("%v",t(Math.abs(a),n(f.precision),f.thousand,f.decimal));if(a.length>k)k=a.length;return a});return j(a,function(a){return q(a)&&a.length<k?l?a.replace(f.symbol,f.symbol+Array(k-a.length+1).join(" ")):Array(k-a.length+1).join(" ")+a:a})};if("undefined"!==typeof exports){if("undefined"!==typeof module&&module.exports)exports=module.exports=c;exports.accounting=c}else"function"===typeof define&&define.amd?define([],function(){return c}):(c.noConflict=function(a){return function(){p.accounting=a;c.noConflict=z;return c}}(p.accounting),p.accounting=c)})(this);

//! moment.js
//! version : 2.10.6
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com
!function(a,b){"object"==typeof exports&&"undefined"!=typeof module?module.exports=b():"function"==typeof define&&define.amd?define(b):a.moment=b()}(this,function(){"use strict";function a(){return Hc.apply(null,arguments)}function b(a){Hc=a}function c(a){return"[object Array]"===Object.prototype.toString.call(a)}function d(a){return a instanceof Date||"[object Date]"===Object.prototype.toString.call(a)}function e(a,b){var c,d=[];for(c=0;c<a.length;++c)d.push(b(a[c],c));return d}function f(a,b){return Object.prototype.hasOwnProperty.call(a,b)}function g(a,b){for(var c in b)f(b,c)&&(a[c]=b[c]);return f(b,"toString")&&(a.toString=b.toString),f(b,"valueOf")&&(a.valueOf=b.valueOf),a}function h(a,b,c,d){return Ca(a,b,c,d,!0).utc()}function i(){return{empty:!1,unusedTokens:[],unusedInput:[],overflow:-2,charsLeftOver:0,nullInput:!1,invalidMonth:null,invalidFormat:!1,userInvalidated:!1,iso:!1}}function j(a){return null==a._pf&&(a._pf=i()),a._pf}function k(a){if(null==a._isValid){var b=j(a);a._isValid=!(isNaN(a._d.getTime())||!(b.overflow<0)||b.empty||b.invalidMonth||b.invalidWeekday||b.nullInput||b.invalidFormat||b.userInvalidated),a._strict&&(a._isValid=a._isValid&&0===b.charsLeftOver&&0===b.unusedTokens.length&&void 0===b.bigHour)}return a._isValid}function l(a){var b=h(NaN);return null!=a?g(j(b),a):j(b).userInvalidated=!0,b}function m(a,b){var c,d,e;if("undefined"!=typeof b._isAMomentObject&&(a._isAMomentObject=b._isAMomentObject),"undefined"!=typeof b._i&&(a._i=b._i),"undefined"!=typeof b._f&&(a._f=b._f),"undefined"!=typeof b._l&&(a._l=b._l),"undefined"!=typeof b._strict&&(a._strict=b._strict),"undefined"!=typeof b._tzm&&(a._tzm=b._tzm),"undefined"!=typeof b._isUTC&&(a._isUTC=b._isUTC),"undefined"!=typeof b._offset&&(a._offset=b._offset),"undefined"!=typeof b._pf&&(a._pf=j(b)),"undefined"!=typeof b._locale&&(a._locale=b._locale),Jc.length>0)for(c in Jc)d=Jc[c],e=b[d],"undefined"!=typeof e&&(a[d]=e);return a}function n(b){m(this,b),this._d=new Date(null!=b._d?b._d.getTime():NaN),Kc===!1&&(Kc=!0,a.updateOffset(this),Kc=!1)}function o(a){return a instanceof n||null!=a&&null!=a._isAMomentObject}function p(a){return 0>a?Math.ceil(a):Math.floor(a)}function q(a){var b=+a,c=0;return 0!==b&&isFinite(b)&&(c=p(b)),c}function r(a,b,c){var d,e=Math.min(a.length,b.length),f=Math.abs(a.length-b.length),g=0;for(d=0;e>d;d++)(c&&a[d]!==b[d]||!c&&q(a[d])!==q(b[d]))&&g++;return g+f}function s(){}function t(a){return a?a.toLowerCase().replace("_","-"):a}function u(a){for(var b,c,d,e,f=0;f<a.length;){for(e=t(a[f]).split("-"),b=e.length,c=t(a[f+1]),c=c?c.split("-"):null;b>0;){if(d=v(e.slice(0,b).join("-")))return d;if(c&&c.length>=b&&r(e,c,!0)>=b-1)break;b--}f++}return null}function v(a){var b=null;if(!Lc[a]&&"undefined"!=typeof module&&module&&module.exports)try{b=Ic._abbr,require("./locale/"+a),w(b)}catch(c){}return Lc[a]}function w(a,b){var c;return a&&(c="undefined"==typeof b?y(a):x(a,b),c&&(Ic=c)),Ic._abbr}function x(a,b){return null!==b?(b.abbr=a,Lc[a]=Lc[a]||new s,Lc[a].set(b),w(a),Lc[a]):(delete Lc[a],null)}function y(a){var b;if(a&&a._locale&&a._locale._abbr&&(a=a._locale._abbr),!a)return Ic;if(!c(a)){if(b=v(a))return b;a=[a]}return u(a)}function z(a,b){var c=a.toLowerCase();Mc[c]=Mc[c+"s"]=Mc[b]=a}function A(a){return"string"==typeof a?Mc[a]||Mc[a.toLowerCase()]:void 0}function B(a){var b,c,d={};for(c in a)f(a,c)&&(b=A(c),b&&(d[b]=a[c]));return d}function C(b,c){return function(d){return null!=d?(E(this,b,d),a.updateOffset(this,c),this):D(this,b)}}function D(a,b){return a._d["get"+(a._isUTC?"UTC":"")+b]()}function E(a,b,c){return a._d["set"+(a._isUTC?"UTC":"")+b](c)}function F(a,b){var c;if("object"==typeof a)for(c in a)this.set(c,a[c]);else if(a=A(a),"function"==typeof this[a])return this[a](b);return this}function G(a,b,c){var d=""+Math.abs(a),e=b-d.length,f=a>=0;return(f?c?"+":"":"-")+Math.pow(10,Math.max(0,e)).toString().substr(1)+d}function H(a,b,c,d){var e=d;"string"==typeof d&&(e=function(){return this[d]()}),a&&(Qc[a]=e),b&&(Qc[b[0]]=function(){return G(e.apply(this,arguments),b[1],b[2])}),c&&(Qc[c]=function(){return this.localeData().ordinal(e.apply(this,arguments),a)})}function I(a){return a.match(/\[[\s\S]/)?a.replace(/^\[|\]$/g,""):a.replace(/\\/g,"")}function J(a){var b,c,d=a.match(Nc);for(b=0,c=d.length;c>b;b++)Qc[d[b]]?d[b]=Qc[d[b]]:d[b]=I(d[b]);return function(e){var f="";for(b=0;c>b;b++)f+=d[b]instanceof Function?d[b].call(e,a):d[b];return f}}function K(a,b){return a.isValid()?(b=L(b,a.localeData()),Pc[b]=Pc[b]||J(b),Pc[b](a)):a.localeData().invalidDate()}function L(a,b){function c(a){return b.longDateFormat(a)||a}var d=5;for(Oc.lastIndex=0;d>=0&&Oc.test(a);)a=a.replace(Oc,c),Oc.lastIndex=0,d-=1;return a}function M(a){return"function"==typeof a&&"[object Function]"===Object.prototype.toString.call(a)}function N(a,b,c){dd[a]=M(b)?b:function(a){return a&&c?c:b}}function O(a,b){return f(dd,a)?dd[a](b._strict,b._locale):new RegExp(P(a))}function P(a){return a.replace("\\","").replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g,function(a,b,c,d,e){return b||c||d||e}).replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&")}function Q(a,b){var c,d=b;for("string"==typeof a&&(a=[a]),"number"==typeof b&&(d=function(a,c){c[b]=q(a)}),c=0;c<a.length;c++)ed[a[c]]=d}function R(a,b){Q(a,function(a,c,d,e){d._w=d._w||{},b(a,d._w,d,e)})}function S(a,b,c){null!=b&&f(ed,a)&&ed[a](b,c._a,c,a)}function T(a,b){return new Date(Date.UTC(a,b+1,0)).getUTCDate()}function U(a){return this._months[a.month()]}function V(a){return this._monthsShort[a.month()]}function W(a,b,c){var d,e,f;for(this._monthsParse||(this._monthsParse=[],this._longMonthsParse=[],this._shortMonthsParse=[]),d=0;12>d;d++){if(e=h([2e3,d]),c&&!this._longMonthsParse[d]&&(this._longMonthsParse[d]=new RegExp("^"+this.months(e,"").replace(".","")+"$","i"),this._shortMonthsParse[d]=new RegExp("^"+this.monthsShort(e,"").replace(".","")+"$","i")),c||this._monthsParse[d]||(f="^"+this.months(e,"")+"|^"+this.monthsShort(e,""),this._monthsParse[d]=new RegExp(f.replace(".",""),"i")),c&&"MMMM"===b&&this._longMonthsParse[d].test(a))return d;if(c&&"MMM"===b&&this._shortMonthsParse[d].test(a))return d;if(!c&&this._monthsParse[d].test(a))return d}}function X(a,b){var c;return"string"==typeof b&&(b=a.localeData().monthsParse(b),"number"!=typeof b)?a:(c=Math.min(a.date(),T(a.year(),b)),a._d["set"+(a._isUTC?"UTC":"")+"Month"](b,c),a)}function Y(b){return null!=b?(X(this,b),a.updateOffset(this,!0),this):D(this,"Month")}function Z(){return T(this.year(),this.month())}function $(a){var b,c=a._a;return c&&-2===j(a).overflow&&(b=c[gd]<0||c[gd]>11?gd:c[hd]<1||c[hd]>T(c[fd],c[gd])?hd:c[id]<0||c[id]>24||24===c[id]&&(0!==c[jd]||0!==c[kd]||0!==c[ld])?id:c[jd]<0||c[jd]>59?jd:c[kd]<0||c[kd]>59?kd:c[ld]<0||c[ld]>999?ld:-1,j(a)._overflowDayOfYear&&(fd>b||b>hd)&&(b=hd),j(a).overflow=b),a}function _(b){a.suppressDeprecationWarnings===!1&&"undefined"!=typeof console&&console.warn&&console.warn("Deprecation warning: "+b)}function aa(a,b){var c=!0;return g(function(){return c&&(_(a+"\n"+(new Error).stack),c=!1),b.apply(this,arguments)},b)}function ba(a,b){od[a]||(_(b),od[a]=!0)}function ca(a){var b,c,d=a._i,e=pd.exec(d);if(e){for(j(a).iso=!0,b=0,c=qd.length;c>b;b++)if(qd[b][1].exec(d)){a._f=qd[b][0];break}for(b=0,c=rd.length;c>b;b++)if(rd[b][1].exec(d)){a._f+=(e[6]||" ")+rd[b][0];break}d.match(ad)&&(a._f+="Z"),va(a)}else a._isValid=!1}function da(b){var c=sd.exec(b._i);return null!==c?void(b._d=new Date(+c[1])):(ca(b),void(b._isValid===!1&&(delete b._isValid,a.createFromInputFallback(b))))}function ea(a,b,c,d,e,f,g){var h=new Date(a,b,c,d,e,f,g);return 1970>a&&h.setFullYear(a),h}function fa(a){var b=new Date(Date.UTC.apply(null,arguments));return 1970>a&&b.setUTCFullYear(a),b}function ga(a){return ha(a)?366:365}function ha(a){return a%4===0&&a%100!==0||a%400===0}function ia(){return ha(this.year())}function ja(a,b,c){var d,e=c-b,f=c-a.day();return f>e&&(f-=7),e-7>f&&(f+=7),d=Da(a).add(f,"d"),{week:Math.ceil(d.dayOfYear()/7),year:d.year()}}function ka(a){return ja(a,this._week.dow,this._week.doy).week}function la(){return this._week.dow}function ma(){return this._week.doy}function na(a){var b=this.localeData().week(this);return null==a?b:this.add(7*(a-b),"d")}function oa(a){var b=ja(this,1,4).week;return null==a?b:this.add(7*(a-b),"d")}function pa(a,b,c,d,e){var f,g=6+e-d,h=fa(a,0,1+g),i=h.getUTCDay();return e>i&&(i+=7),c=null!=c?1*c:e,f=1+g+7*(b-1)-i+c,{year:f>0?a:a-1,dayOfYear:f>0?f:ga(a-1)+f}}function qa(a){var b=Math.round((this.clone().startOf("day")-this.clone().startOf("year"))/864e5)+1;return null==a?b:this.add(a-b,"d")}function ra(a,b,c){return null!=a?a:null!=b?b:c}function sa(a){var b=new Date;return a._useUTC?[b.getUTCFullYear(),b.getUTCMonth(),b.getUTCDate()]:[b.getFullYear(),b.getMonth(),b.getDate()]}function ta(a){var b,c,d,e,f=[];if(!a._d){for(d=sa(a),a._w&&null==a._a[hd]&&null==a._a[gd]&&ua(a),a._dayOfYear&&(e=ra(a._a[fd],d[fd]),a._dayOfYear>ga(e)&&(j(a)._overflowDayOfYear=!0),c=fa(e,0,a._dayOfYear),a._a[gd]=c.getUTCMonth(),a._a[hd]=c.getUTCDate()),b=0;3>b&&null==a._a[b];++b)a._a[b]=f[b]=d[b];for(;7>b;b++)a._a[b]=f[b]=null==a._a[b]?2===b?1:0:a._a[b];24===a._a[id]&&0===a._a[jd]&&0===a._a[kd]&&0===a._a[ld]&&(a._nextDay=!0,a._a[id]=0),a._d=(a._useUTC?fa:ea).apply(null,f),null!=a._tzm&&a._d.setUTCMinutes(a._d.getUTCMinutes()-a._tzm),a._nextDay&&(a._a[id]=24)}}function ua(a){var b,c,d,e,f,g,h;b=a._w,null!=b.GG||null!=b.W||null!=b.E?(f=1,g=4,c=ra(b.GG,a._a[fd],ja(Da(),1,4).year),d=ra(b.W,1),e=ra(b.E,1)):(f=a._locale._week.dow,g=a._locale._week.doy,c=ra(b.gg,a._a[fd],ja(Da(),f,g).year),d=ra(b.w,1),null!=b.d?(e=b.d,f>e&&++d):e=null!=b.e?b.e+f:f),h=pa(c,d,e,g,f),a._a[fd]=h.year,a._dayOfYear=h.dayOfYear}function va(b){if(b._f===a.ISO_8601)return void ca(b);b._a=[],j(b).empty=!0;var c,d,e,f,g,h=""+b._i,i=h.length,k=0;for(e=L(b._f,b._locale).match(Nc)||[],c=0;c<e.length;c++)f=e[c],d=(h.match(O(f,b))||[])[0],d&&(g=h.substr(0,h.indexOf(d)),g.length>0&&j(b).unusedInput.push(g),h=h.slice(h.indexOf(d)+d.length),k+=d.length),Qc[f]?(d?j(b).empty=!1:j(b).unusedTokens.push(f),S(f,d,b)):b._strict&&!d&&j(b).unusedTokens.push(f);j(b).charsLeftOver=i-k,h.length>0&&j(b).unusedInput.push(h),j(b).bigHour===!0&&b._a[id]<=12&&b._a[id]>0&&(j(b).bigHour=void 0),b._a[id]=wa(b._locale,b._a[id],b._meridiem),ta(b),$(b)}function wa(a,b,c){var d;return null==c?b:null!=a.meridiemHour?a.meridiemHour(b,c):null!=a.isPM?(d=a.isPM(c),d&&12>b&&(b+=12),d||12!==b||(b=0),b):b}function xa(a){var b,c,d,e,f;if(0===a._f.length)return j(a).invalidFormat=!0,void(a._d=new Date(NaN));for(e=0;e<a._f.length;e++)f=0,b=m({},a),null!=a._useUTC&&(b._useUTC=a._useUTC),b._f=a._f[e],va(b),k(b)&&(f+=j(b).charsLeftOver,f+=10*j(b).unusedTokens.length,j(b).score=f,(null==d||d>f)&&(d=f,c=b));g(a,c||b)}function ya(a){if(!a._d){var b=B(a._i);a._a=[b.year,b.month,b.day||b.date,b.hour,b.minute,b.second,b.millisecond],ta(a)}}function za(a){var b=new n($(Aa(a)));return b._nextDay&&(b.add(1,"d"),b._nextDay=void 0),b}function Aa(a){var b=a._i,e=a._f;return a._locale=a._locale||y(a._l),null===b||void 0===e&&""===b?l({nullInput:!0}):("string"==typeof b&&(a._i=b=a._locale.preparse(b)),o(b)?new n($(b)):(c(e)?xa(a):e?va(a):d(b)?a._d=b:Ba(a),a))}function Ba(b){var f=b._i;void 0===f?b._d=new Date:d(f)?b._d=new Date(+f):"string"==typeof f?da(b):c(f)?(b._a=e(f.slice(0),function(a){return parseInt(a,10)}),ta(b)):"object"==typeof f?ya(b):"number"==typeof f?b._d=new Date(f):a.createFromInputFallback(b)}function Ca(a,b,c,d,e){var f={};return"boolean"==typeof c&&(d=c,c=void 0),f._isAMomentObject=!0,f._useUTC=f._isUTC=e,f._l=c,f._i=a,f._f=b,f._strict=d,za(f)}function Da(a,b,c,d){return Ca(a,b,c,d,!1)}function Ea(a,b){var d,e;if(1===b.length&&c(b[0])&&(b=b[0]),!b.length)return Da();for(d=b[0],e=1;e<b.length;++e)(!b[e].isValid()||b[e][a](d))&&(d=b[e]);return d}function Fa(){var a=[].slice.call(arguments,0);return Ea("isBefore",a)}function Ga(){var a=[].slice.call(arguments,0);return Ea("isAfter",a)}function Ha(a){var b=B(a),c=b.year||0,d=b.quarter||0,e=b.month||0,f=b.week||0,g=b.day||0,h=b.hour||0,i=b.minute||0,j=b.second||0,k=b.millisecond||0;this._milliseconds=+k+1e3*j+6e4*i+36e5*h,this._days=+g+7*f,this._months=+e+3*d+12*c,this._data={},this._locale=y(),this._bubble()}function Ia(a){return a instanceof Ha}function Ja(a,b){H(a,0,0,function(){var a=this.utcOffset(),c="+";return 0>a&&(a=-a,c="-"),c+G(~~(a/60),2)+b+G(~~a%60,2)})}function Ka(a){var b=(a||"").match(ad)||[],c=b[b.length-1]||[],d=(c+"").match(xd)||["-",0,0],e=+(60*d[1])+q(d[2]);return"+"===d[0]?e:-e}function La(b,c){var e,f;return c._isUTC?(e=c.clone(),f=(o(b)||d(b)?+b:+Da(b))-+e,e._d.setTime(+e._d+f),a.updateOffset(e,!1),e):Da(b).local()}function Ma(a){return 15*-Math.round(a._d.getTimezoneOffset()/15)}function Na(b,c){var d,e=this._offset||0;return null!=b?("string"==typeof b&&(b=Ka(b)),Math.abs(b)<16&&(b=60*b),!this._isUTC&&c&&(d=Ma(this)),this._offset=b,this._isUTC=!0,null!=d&&this.add(d,"m"),e!==b&&(!c||this._changeInProgress?bb(this,Ya(b-e,"m"),1,!1):this._changeInProgress||(this._changeInProgress=!0,a.updateOffset(this,!0),this._changeInProgress=null)),this):this._isUTC?e:Ma(this)}function Oa(a,b){return null!=a?("string"!=typeof a&&(a=-a),this.utcOffset(a,b),this):-this.utcOffset()}function Pa(a){return this.utcOffset(0,a)}function Qa(a){return this._isUTC&&(this.utcOffset(0,a),this._isUTC=!1,a&&this.subtract(Ma(this),"m")),this}function Ra(){return this._tzm?this.utcOffset(this._tzm):"string"==typeof this._i&&this.utcOffset(Ka(this._i)),this}function Sa(a){return a=a?Da(a).utcOffset():0,(this.utcOffset()-a)%60===0}function Ta(){return this.utcOffset()>this.clone().month(0).utcOffset()||this.utcOffset()>this.clone().month(5).utcOffset()}function Ua(){if("undefined"!=typeof this._isDSTShifted)return this._isDSTShifted;var a={};if(m(a,this),a=Aa(a),a._a){var b=a._isUTC?h(a._a):Da(a._a);this._isDSTShifted=this.isValid()&&r(a._a,b.toArray())>0}else this._isDSTShifted=!1;return this._isDSTShifted}function Va(){return!this._isUTC}function Wa(){return this._isUTC}function Xa(){return this._isUTC&&0===this._offset}function Ya(a,b){var c,d,e,g=a,h=null;return Ia(a)?g={ms:a._milliseconds,d:a._days,M:a._months}:"number"==typeof a?(g={},b?g[b]=a:g.milliseconds=a):(h=yd.exec(a))?(c="-"===h[1]?-1:1,g={y:0,d:q(h[hd])*c,h:q(h[id])*c,m:q(h[jd])*c,s:q(h[kd])*c,ms:q(h[ld])*c}):(h=zd.exec(a))?(c="-"===h[1]?-1:1,g={y:Za(h[2],c),M:Za(h[3],c),d:Za(h[4],c),h:Za(h[5],c),m:Za(h[6],c),s:Za(h[7],c),w:Za(h[8],c)}):null==g?g={}:"object"==typeof g&&("from"in g||"to"in g)&&(e=_a(Da(g.from),Da(g.to)),g={},g.ms=e.milliseconds,g.M=e.months),d=new Ha(g),Ia(a)&&f(a,"_locale")&&(d._locale=a._locale),d}function Za(a,b){var c=a&&parseFloat(a.replace(",","."));return(isNaN(c)?0:c)*b}function $a(a,b){var c={milliseconds:0,months:0};return c.months=b.month()-a.month()+12*(b.year()-a.year()),a.clone().add(c.months,"M").isAfter(b)&&--c.months,c.milliseconds=+b-+a.clone().add(c.months,"M"),c}function _a(a,b){var c;return b=La(b,a),a.isBefore(b)?c=$a(a,b):(c=$a(b,a),c.milliseconds=-c.milliseconds,c.months=-c.months),c}function ab(a,b){return function(c,d){var e,f;return null===d||isNaN(+d)||(ba(b,"moment()."+b+"(period, number) is deprecated. Please use moment()."+b+"(number, period)."),f=c,c=d,d=f),c="string"==typeof c?+c:c,e=Ya(c,d),bb(this,e,a),this}}function bb(b,c,d,e){var f=c._milliseconds,g=c._days,h=c._months;e=null==e?!0:e,f&&b._d.setTime(+b._d+f*d),g&&E(b,"Date",D(b,"Date")+g*d),h&&X(b,D(b,"Month")+h*d),e&&a.updateOffset(b,g||h)}function cb(a,b){var c=a||Da(),d=La(c,this).startOf("day"),e=this.diff(d,"days",!0),f=-6>e?"sameElse":-1>e?"lastWeek":0>e?"lastDay":1>e?"sameDay":2>e?"nextDay":7>e?"nextWeek":"sameElse";return this.format(b&&b[f]||this.localeData().calendar(f,this,Da(c)))}function db(){return new n(this)}function eb(a,b){var c;return b=A("undefined"!=typeof b?b:"millisecond"),"millisecond"===b?(a=o(a)?a:Da(a),+this>+a):(c=o(a)?+a:+Da(a),c<+this.clone().startOf(b))}function fb(a,b){var c;return b=A("undefined"!=typeof b?b:"millisecond"),"millisecond"===b?(a=o(a)?a:Da(a),+a>+this):(c=o(a)?+a:+Da(a),+this.clone().endOf(b)<c)}function gb(a,b,c){return this.isAfter(a,c)&&this.isBefore(b,c)}function hb(a,b){var c;return b=A(b||"millisecond"),"millisecond"===b?(a=o(a)?a:Da(a),+this===+a):(c=+Da(a),+this.clone().startOf(b)<=c&&c<=+this.clone().endOf(b))}function ib(a,b,c){var d,e,f=La(a,this),g=6e4*(f.utcOffset()-this.utcOffset());return b=A(b),"year"===b||"month"===b||"quarter"===b?(e=jb(this,f),"quarter"===b?e/=3:"year"===b&&(e/=12)):(d=this-f,e="second"===b?d/1e3:"minute"===b?d/6e4:"hour"===b?d/36e5:"day"===b?(d-g)/864e5:"week"===b?(d-g)/6048e5:d),c?e:p(e)}function jb(a,b){var c,d,e=12*(b.year()-a.year())+(b.month()-a.month()),f=a.clone().add(e,"months");return 0>b-f?(c=a.clone().add(e-1,"months"),d=(b-f)/(f-c)):(c=a.clone().add(e+1,"months"),d=(b-f)/(c-f)),-(e+d)}function kb(){return this.clone().locale("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")}function lb(){var a=this.clone().utc();return 0<a.year()&&a.year()<=9999?"function"==typeof Date.prototype.toISOString?this.toDate().toISOString():K(a,"YYYY-MM-DD[T]HH:mm:ss.SSS[Z]"):K(a,"YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]")}function mb(b){var c=K(this,b||a.defaultFormat);return this.localeData().postformat(c)}function nb(a,b){return this.isValid()?Ya({to:this,from:a}).locale(this.locale()).humanize(!b):this.localeData().invalidDate()}function ob(a){return this.from(Da(),a)}function pb(a,b){return this.isValid()?Ya({from:this,to:a}).locale(this.locale()).humanize(!b):this.localeData().invalidDate()}function qb(a){return this.to(Da(),a)}function rb(a){var b;return void 0===a?this._locale._abbr:(b=y(a),null!=b&&(this._locale=b),this)}function sb(){return this._locale}function tb(a){switch(a=A(a)){case"year":this.month(0);case"quarter":case"month":this.date(1);case"week":case"isoWeek":case"day":this.hours(0);case"hour":this.minutes(0);case"minute":this.seconds(0);case"second":this.milliseconds(0)}return"week"===a&&this.weekday(0),"isoWeek"===a&&this.isoWeekday(1),"quarter"===a&&this.month(3*Math.floor(this.month()/3)),this}function ub(a){return a=A(a),void 0===a||"millisecond"===a?this:this.startOf(a).add(1,"isoWeek"===a?"week":a).subtract(1,"ms")}function vb(){return+this._d-6e4*(this._offset||0)}function wb(){return Math.floor(+this/1e3)}function xb(){return this._offset?new Date(+this):this._d}function yb(){var a=this;return[a.year(),a.month(),a.date(),a.hour(),a.minute(),a.second(),a.millisecond()]}function zb(){var a=this;return{years:a.year(),months:a.month(),date:a.date(),hours:a.hours(),minutes:a.minutes(),seconds:a.seconds(),milliseconds:a.milliseconds()}}function Ab(){return k(this)}function Bb(){return g({},j(this))}function Cb(){return j(this).overflow}function Db(a,b){H(0,[a,a.length],0,b)}function Eb(a,b,c){return ja(Da([a,11,31+b-c]),b,c).week}function Fb(a){var b=ja(this,this.localeData()._week.dow,this.localeData()._week.doy).year;return null==a?b:this.add(a-b,"y")}function Gb(a){var b=ja(this,1,4).year;return null==a?b:this.add(a-b,"y")}function Hb(){return Eb(this.year(),1,4)}function Ib(){var a=this.localeData()._week;return Eb(this.year(),a.dow,a.doy)}function Jb(a){return null==a?Math.ceil((this.month()+1)/3):this.month(3*(a-1)+this.month()%3)}function Kb(a,b){return"string"!=typeof a?a:isNaN(a)?(a=b.weekdaysParse(a),"number"==typeof a?a:null):parseInt(a,10)}function Lb(a){return this._weekdays[a.day()]}function Mb(a){return this._weekdaysShort[a.day()]}function Nb(a){return this._weekdaysMin[a.day()]}function Ob(a){var b,c,d;for(this._weekdaysParse=this._weekdaysParse||[],b=0;7>b;b++)if(this._weekdaysParse[b]||(c=Da([2e3,1]).day(b),d="^"+this.weekdays(c,"")+"|^"+this.weekdaysShort(c,"")+"|^"+this.weekdaysMin(c,""),this._weekdaysParse[b]=new RegExp(d.replace(".",""),"i")),this._weekdaysParse[b].test(a))return b}function Pb(a){var b=this._isUTC?this._d.getUTCDay():this._d.getDay();return null!=a?(a=Kb(a,this.localeData()),this.add(a-b,"d")):b}function Qb(a){var b=(this.day()+7-this.localeData()._week.dow)%7;return null==a?b:this.add(a-b,"d")}function Rb(a){return null==a?this.day()||7:this.day(this.day()%7?a:a-7)}function Sb(a,b){H(a,0,0,function(){return this.localeData().meridiem(this.hours(),this.minutes(),b)})}function Tb(a,b){return b._meridiemParse}function Ub(a){return"p"===(a+"").toLowerCase().charAt(0)}function Vb(a,b,c){return a>11?c?"pm":"PM":c?"am":"AM"}function Wb(a,b){b[ld]=q(1e3*("0."+a))}function Xb(){return this._isUTC?"UTC":""}function Yb(){return this._isUTC?"Coordinated Universal Time":""}function Zb(a){return Da(1e3*a)}function $b(){return Da.apply(null,arguments).parseZone()}function _b(a,b,c){var d=this._calendar[a];return"function"==typeof d?d.call(b,c):d}function ac(a){var b=this._longDateFormat[a],c=this._longDateFormat[a.toUpperCase()];return b||!c?b:(this._longDateFormat[a]=c.replace(/MMMM|MM|DD|dddd/g,function(a){return a.slice(1)}),this._longDateFormat[a])}function bc(){return this._invalidDate}function cc(a){return this._ordinal.replace("%d",a)}function dc(a){return a}function ec(a,b,c,d){var e=this._relativeTime[c];return"function"==typeof e?e(a,b,c,d):e.replace(/%d/i,a)}function fc(a,b){var c=this._relativeTime[a>0?"future":"past"];return"function"==typeof c?c(b):c.replace(/%s/i,b)}function gc(a){var b,c;for(c in a)b=a[c],"function"==typeof b?this[c]=b:this["_"+c]=b;this._ordinalParseLenient=new RegExp(this._ordinalParse.source+"|"+/\d{1,2}/.source)}function hc(a,b,c,d){var e=y(),f=h().set(d,b);return e[c](f,a)}function ic(a,b,c,d,e){if("number"==typeof a&&(b=a,a=void 0),a=a||"",null!=b)return hc(a,b,c,e);var f,g=[];for(f=0;d>f;f++)g[f]=hc(a,f,c,e);return g}function jc(a,b){return ic(a,b,"months",12,"month")}function kc(a,b){return ic(a,b,"monthsShort",12,"month")}function lc(a,b){return ic(a,b,"weekdays",7,"day")}function mc(a,b){return ic(a,b,"weekdaysShort",7,"day")}function nc(a,b){return ic(a,b,"weekdaysMin",7,"day")}function oc(){var a=this._data;return this._milliseconds=Wd(this._milliseconds),this._days=Wd(this._days),this._months=Wd(this._months),a.milliseconds=Wd(a.milliseconds),a.seconds=Wd(a.seconds),a.minutes=Wd(a.minutes),a.hours=Wd(a.hours),a.months=Wd(a.months),a.years=Wd(a.years),this}function pc(a,b,c,d){var e=Ya(b,c);return a._milliseconds+=d*e._milliseconds,a._days+=d*e._days,a._months+=d*e._months,a._bubble()}function qc(a,b){return pc(this,a,b,1)}function rc(a,b){return pc(this,a,b,-1)}function sc(a){return 0>a?Math.floor(a):Math.ceil(a)}function tc(){var a,b,c,d,e,f=this._milliseconds,g=this._days,h=this._months,i=this._data;return f>=0&&g>=0&&h>=0||0>=f&&0>=g&&0>=h||(f+=864e5*sc(vc(h)+g),g=0,h=0),i.milliseconds=f%1e3,a=p(f/1e3),i.seconds=a%60,b=p(a/60),i.minutes=b%60,c=p(b/60),i.hours=c%24,g+=p(c/24),e=p(uc(g)),h+=e,g-=sc(vc(e)),d=p(h/12),h%=12,i.days=g,i.months=h,i.years=d,this}function uc(a){return 4800*a/146097}function vc(a){return 146097*a/4800}function wc(a){var b,c,d=this._milliseconds;if(a=A(a),"month"===a||"year"===a)return b=this._days+d/864e5,c=this._months+uc(b),"month"===a?c:c/12;switch(b=this._days+Math.round(vc(this._months)),a){case"week":return b/7+d/6048e5;case"day":return b+d/864e5;case"hour":return 24*b+d/36e5;case"minute":return 1440*b+d/6e4;case"second":return 86400*b+d/1e3;case"millisecond":return Math.floor(864e5*b)+d;default:throw new Error("Unknown unit "+a)}}function xc(){return this._milliseconds+864e5*this._days+this._months%12*2592e6+31536e6*q(this._months/12)}function yc(a){return function(){return this.as(a)}}function zc(a){return a=A(a),this[a+"s"]()}function Ac(a){return function(){return this._data[a]}}function Bc(){return p(this.days()/7)}function Cc(a,b,c,d,e){return e.relativeTime(b||1,!!c,a,d)}function Dc(a,b,c){var d=Ya(a).abs(),e=ke(d.as("s")),f=ke(d.as("m")),g=ke(d.as("h")),h=ke(d.as("d")),i=ke(d.as("M")),j=ke(d.as("y")),k=e<le.s&&["s",e]||1===f&&["m"]||f<le.m&&["mm",f]||1===g&&["h"]||g<le.h&&["hh",g]||1===h&&["d"]||h<le.d&&["dd",h]||1===i&&["M"]||i<le.M&&["MM",i]||1===j&&["y"]||["yy",j];return k[2]=b,k[3]=+a>0,k[4]=c,Cc.apply(null,k)}function Ec(a,b){return void 0===le[a]?!1:void 0===b?le[a]:(le[a]=b,!0)}function Fc(a){var b=this.localeData(),c=Dc(this,!a,b);return a&&(c=b.pastFuture(+this,c)),b.postformat(c)}function Gc(){var a,b,c,d=me(this._milliseconds)/1e3,e=me(this._days),f=me(this._months);a=p(d/60),b=p(a/60),d%=60,a%=60,c=p(f/12),f%=12;var g=c,h=f,i=e,j=b,k=a,l=d,m=this.asSeconds();return m?(0>m?"-":"")+"P"+(g?g+"Y":"")+(h?h+"M":"")+(i?i+"D":"")+(j||k||l?"T":"")+(j?j+"H":"")+(k?k+"M":"")+(l?l+"S":""):"P0D"}var Hc,Ic,Jc=a.momentProperties=[],Kc=!1,Lc={},Mc={},Nc=/(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Q|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g,Oc=/(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g,Pc={},Qc={},Rc=/\d/,Sc=/\d\d/,Tc=/\d{3}/,Uc=/\d{4}/,Vc=/[+-]?\d{6}/,Wc=/\d\d?/,Xc=/\d{1,3}/,Yc=/\d{1,4}/,Zc=/[+-]?\d{1,6}/,$c=/\d+/,_c=/[+-]?\d+/,ad=/Z|[+-]\d\d:?\d\d/gi,bd=/[+-]?\d+(\.\d{1,3})?/,cd=/[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i,dd={},ed={},fd=0,gd=1,hd=2,id=3,jd=4,kd=5,ld=6;H("M",["MM",2],"Mo",function(){return this.month()+1}),H("MMM",0,0,function(a){return this.localeData().monthsShort(this,a)}),H("MMMM",0,0,function(a){return this.localeData().months(this,a)}),z("month","M"),N("M",Wc),N("MM",Wc,Sc),N("MMM",cd),N("MMMM",cd),Q(["M","MM"],function(a,b){b[gd]=q(a)-1}),Q(["MMM","MMMM"],function(a,b,c,d){var e=c._locale.monthsParse(a,d,c._strict);null!=e?b[gd]=e:j(c).invalidMonth=a});var md="January_February_March_April_May_June_July_August_September_October_November_December".split("_"),nd="Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),od={};a.suppressDeprecationWarnings=!1;var pd=/^\s*(?:[+-]\d{6}|\d{4})-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/,qd=[["YYYYYY-MM-DD",/[+-]\d{6}-\d{2}-\d{2}/],["YYYY-MM-DD",/\d{4}-\d{2}-\d{2}/],["GGGG-[W]WW-E",/\d{4}-W\d{2}-\d/],["GGGG-[W]WW",/\d{4}-W\d{2}/],["YYYY-DDD",/\d{4}-\d{3}/]],rd=[["HH:mm:ss.SSSS",/(T| )\d\d:\d\d:\d\d\.\d+/],["HH:mm:ss",/(T| )\d\d:\d\d:\d\d/],["HH:mm",/(T| )\d\d:\d\d/],["HH",/(T| )\d\d/]],sd=/^\/?Date\((\-?\d+)/i;a.createFromInputFallback=aa("moment construction falls back to js Date. This is discouraged and will be removed in upcoming major release. Please refer to https://github.com/moment/moment/issues/1407 for more info.",function(a){a._d=new Date(a._i+(a._useUTC?" UTC":""))}),H(0,["YY",2],0,function(){return this.year()%100}),H(0,["YYYY",4],0,"year"),H(0,["YYYYY",5],0,"year"),H(0,["YYYYYY",6,!0],0,"year"),z("year","y"),N("Y",_c),N("YY",Wc,Sc),N("YYYY",Yc,Uc),N("YYYYY",Zc,Vc),N("YYYYYY",Zc,Vc),Q(["YYYYY","YYYYYY"],fd),Q("YYYY",function(b,c){c[fd]=2===b.length?a.parseTwoDigitYear(b):q(b)}),Q("YY",function(b,c){c[fd]=a.parseTwoDigitYear(b)}),a.parseTwoDigitYear=function(a){return q(a)+(q(a)>68?1900:2e3)};var td=C("FullYear",!1);H("w",["ww",2],"wo","week"),H("W",["WW",2],"Wo","isoWeek"),z("week","w"),z("isoWeek","W"),N("w",Wc),N("ww",Wc,Sc),N("W",Wc),N("WW",Wc,Sc),R(["w","ww","W","WW"],function(a,b,c,d){b[d.substr(0,1)]=q(a)});var ud={dow:0,doy:6};H("DDD",["DDDD",3],"DDDo","dayOfYear"),z("dayOfYear","DDD"),N("DDD",Xc),N("DDDD",Tc),Q(["DDD","DDDD"],function(a,b,c){c._dayOfYear=q(a)}),a.ISO_8601=function(){};var vd=aa("moment().min is deprecated, use moment.min instead. https://github.com/moment/moment/issues/1548",function(){var a=Da.apply(null,arguments);return this>a?this:a}),wd=aa("moment().max is deprecated, use moment.max instead. https://github.com/moment/moment/issues/1548",function(){var a=Da.apply(null,arguments);return a>this?this:a});Ja("Z",":"),Ja("ZZ",""),N("Z",ad),N("ZZ",ad),Q(["Z","ZZ"],function(a,b,c){c._useUTC=!0,c._tzm=Ka(a)});var xd=/([\+\-]|\d\d)/gi;a.updateOffset=function(){};var yd=/(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/,zd=/^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/;Ya.fn=Ha.prototype;var Ad=ab(1,"add"),Bd=ab(-1,"subtract");a.defaultFormat="YYYY-MM-DDTHH:mm:ssZ";var Cd=aa("moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.",function(a){return void 0===a?this.localeData():this.locale(a)});H(0,["gg",2],0,function(){return this.weekYear()%100}),H(0,["GG",2],0,function(){return this.isoWeekYear()%100}),Db("gggg","weekYear"),Db("ggggg","weekYear"),Db("GGGG","isoWeekYear"),Db("GGGGG","isoWeekYear"),z("weekYear","gg"),z("isoWeekYear","GG"),N("G",_c),N("g",_c),N("GG",Wc,Sc),N("gg",Wc,Sc),N("GGGG",Yc,Uc),N("gggg",Yc,Uc),N("GGGGG",Zc,Vc),N("ggggg",Zc,Vc),R(["gggg","ggggg","GGGG","GGGGG"],function(a,b,c,d){b[d.substr(0,2)]=q(a)}),R(["gg","GG"],function(b,c,d,e){c[e]=a.parseTwoDigitYear(b)}),H("Q",0,0,"quarter"),z("quarter","Q"),N("Q",Rc),Q("Q",function(a,b){b[gd]=3*(q(a)-1)}),H("D",["DD",2],"Do","date"),z("date","D"),N("D",Wc),N("DD",Wc,Sc),N("Do",function(a,b){return a?b._ordinalParse:b._ordinalParseLenient}),Q(["D","DD"],hd),Q("Do",function(a,b){b[hd]=q(a.match(Wc)[0],10)});var Dd=C("Date",!0);H("d",0,"do","day"),H("dd",0,0,function(a){return this.localeData().weekdaysMin(this,a)}),H("ddd",0,0,function(a){return this.localeData().weekdaysShort(this,a)}),H("dddd",0,0,function(a){return this.localeData().weekdays(this,a)}),H("e",0,0,"weekday"),H("E",0,0,"isoWeekday"),z("day","d"),z("weekday","e"),z("isoWeekday","E"),N("d",Wc),N("e",Wc),N("E",Wc),N("dd",cd),N("ddd",cd),N("dddd",cd),R(["dd","ddd","dddd"],function(a,b,c){var d=c._locale.weekdaysParse(a);null!=d?b.d=d:j(c).invalidWeekday=a}),R(["d","e","E"],function(a,b,c,d){b[d]=q(a)});var Ed="Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),Fd="Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),Gd="Su_Mo_Tu_We_Th_Fr_Sa".split("_");H("H",["HH",2],0,"hour"),H("h",["hh",2],0,function(){return this.hours()%12||12}),Sb("a",!0),Sb("A",!1),z("hour","h"),N("a",Tb),N("A",Tb),N("H",Wc),N("h",Wc),N("HH",Wc,Sc),N("hh",Wc,Sc),Q(["H","HH"],id),Q(["a","A"],function(a,b,c){c._isPm=c._locale.isPM(a),c._meridiem=a}),Q(["h","hh"],function(a,b,c){b[id]=q(a),j(c).bigHour=!0});var Hd=/[ap]\.?m?\.?/i,Id=C("Hours",!0);H("m",["mm",2],0,"minute"),z("minute","m"),N("m",Wc),N("mm",Wc,Sc),Q(["m","mm"],jd);var Jd=C("Minutes",!1);H("s",["ss",2],0,"second"),z("second","s"),N("s",Wc),N("ss",Wc,Sc),Q(["s","ss"],kd);var Kd=C("Seconds",!1);H("S",0,0,function(){return~~(this.millisecond()/100)}),H(0,["SS",2],0,function(){return~~(this.millisecond()/10)}),H(0,["SSS",3],0,"millisecond"),H(0,["SSSS",4],0,function(){return 10*this.millisecond()}),H(0,["SSSSS",5],0,function(){return 100*this.millisecond()}),H(0,["SSSSSS",6],0,function(){return 1e3*this.millisecond()}),H(0,["SSSSSSS",7],0,function(){return 1e4*this.millisecond()}),H(0,["SSSSSSSS",8],0,function(){return 1e5*this.millisecond()}),H(0,["SSSSSSSSS",9],0,function(){return 1e6*this.millisecond()}),z("millisecond","ms"),N("S",Xc,Rc),N("SS",Xc,Sc),N("SSS",Xc,Tc);var Ld;for(Ld="SSSS";Ld.length<=9;Ld+="S")N(Ld,$c);for(Ld="S";Ld.length<=9;Ld+="S")Q(Ld,Wb);var Md=C("Milliseconds",!1);H("z",0,0,"zoneAbbr"),H("zz",0,0,"zoneName");var Nd=n.prototype;Nd.add=Ad,Nd.calendar=cb,Nd.clone=db,Nd.diff=ib,Nd.endOf=ub,Nd.format=mb,Nd.from=nb,Nd.fromNow=ob,Nd.to=pb,Nd.toNow=qb,Nd.get=F,Nd.invalidAt=Cb,Nd.isAfter=eb,Nd.isBefore=fb,Nd.isBetween=gb,Nd.isSame=hb,Nd.isValid=Ab,Nd.lang=Cd,Nd.locale=rb,Nd.localeData=sb,Nd.max=wd,Nd.min=vd,Nd.parsingFlags=Bb,Nd.set=F,Nd.startOf=tb,Nd.subtract=Bd,Nd.toArray=yb,Nd.toObject=zb,Nd.toDate=xb,Nd.toISOString=lb,Nd.toJSON=lb,Nd.toString=kb,Nd.unix=wb,Nd.valueOf=vb,Nd.year=td,Nd.isLeapYear=ia,Nd.weekYear=Fb,Nd.isoWeekYear=Gb,Nd.quarter=Nd.quarters=Jb,Nd.month=Y,Nd.daysInMonth=Z,Nd.week=Nd.weeks=na,Nd.isoWeek=Nd.isoWeeks=oa,Nd.weeksInYear=Ib,Nd.isoWeeksInYear=Hb,Nd.date=Dd,Nd.day=Nd.days=Pb,Nd.weekday=Qb,Nd.isoWeekday=Rb,Nd.dayOfYear=qa,Nd.hour=Nd.hours=Id,Nd.minute=Nd.minutes=Jd,Nd.second=Nd.seconds=Kd,
Nd.millisecond=Nd.milliseconds=Md,Nd.utcOffset=Na,Nd.utc=Pa,Nd.local=Qa,Nd.parseZone=Ra,Nd.hasAlignedHourOffset=Sa,Nd.isDST=Ta,Nd.isDSTShifted=Ua,Nd.isLocal=Va,Nd.isUtcOffset=Wa,Nd.isUtc=Xa,Nd.isUTC=Xa,Nd.zoneAbbr=Xb,Nd.zoneName=Yb,Nd.dates=aa("dates accessor is deprecated. Use date instead.",Dd),Nd.months=aa("months accessor is deprecated. Use month instead",Y),Nd.years=aa("years accessor is deprecated. Use year instead",td),Nd.zone=aa("moment().zone is deprecated, use moment().utcOffset instead. https://github.com/moment/moment/issues/1779",Oa);var Od=Nd,Pd={sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},Qd={LTS:"h:mm:ss A",LT:"h:mm A",L:"MM/DD/YYYY",LL:"MMMM D, YYYY",LLL:"MMMM D, YYYY h:mm A",LLLL:"dddd, MMMM D, YYYY h:mm A"},Rd="Invalid date",Sd="%d",Td=/\d{1,2}/,Ud={future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},Vd=s.prototype;Vd._calendar=Pd,Vd.calendar=_b,Vd._longDateFormat=Qd,Vd.longDateFormat=ac,Vd._invalidDate=Rd,Vd.invalidDate=bc,Vd._ordinal=Sd,Vd.ordinal=cc,Vd._ordinalParse=Td,Vd.preparse=dc,Vd.postformat=dc,Vd._relativeTime=Ud,Vd.relativeTime=ec,Vd.pastFuture=fc,Vd.set=gc,Vd.months=U,Vd._months=md,Vd.monthsShort=V,Vd._monthsShort=nd,Vd.monthsParse=W,Vd.week=ka,Vd._week=ud,Vd.firstDayOfYear=ma,Vd.firstDayOfWeek=la,Vd.weekdays=Lb,Vd._weekdays=Ed,Vd.weekdaysMin=Nb,Vd._weekdaysMin=Gd,Vd.weekdaysShort=Mb,Vd._weekdaysShort=Fd,Vd.weekdaysParse=Ob,Vd.isPM=Ub,Vd._meridiemParse=Hd,Vd.meridiem=Vb,w("en",{ordinalParse:/\d{1,2}(th|st|nd|rd)/,ordinal:function(a){var b=a%10,c=1===q(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";return a+c}}),a.lang=aa("moment.lang is deprecated. Use moment.locale instead.",w),a.langData=aa("moment.langData is deprecated. Use moment.localeData instead.",y);var Wd=Math.abs,Xd=yc("ms"),Yd=yc("s"),Zd=yc("m"),$d=yc("h"),_d=yc("d"),ae=yc("w"),be=yc("M"),ce=yc("y"),de=Ac("milliseconds"),ee=Ac("seconds"),fe=Ac("minutes"),ge=Ac("hours"),he=Ac("days"),ie=Ac("months"),je=Ac("years"),ke=Math.round,le={s:45,m:45,h:22,d:26,M:11},me=Math.abs,ne=Ha.prototype;ne.abs=oc,ne.add=qc,ne.subtract=rc,ne.as=wc,ne.asMilliseconds=Xd,ne.asSeconds=Yd,ne.asMinutes=Zd,ne.asHours=$d,ne.asDays=_d,ne.asWeeks=ae,ne.asMonths=be,ne.asYears=ce,ne.valueOf=xc,ne._bubble=tc,ne.get=zc,ne.milliseconds=de,ne.seconds=ee,ne.minutes=fe,ne.hours=ge,ne.days=he,ne.weeks=Bc,ne.months=ie,ne.years=je,ne.humanize=Fc,ne.toISOString=Gc,ne.toString=Gc,ne.toJSON=Gc,ne.locale=rb,ne.localeData=sb,ne.toIsoString=aa("toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)",Gc),ne.lang=Cd,H("X",0,0,"unix"),H("x",0,0,"valueOf"),N("x",_c),N("X",bd),Q("X",function(a,b,c){c._d=new Date(1e3*parseFloat(a,10))}),Q("x",function(a,b,c){c._d=new Date(q(a))}),a.version="2.10.6",b(Da),a.fn=Od,a.min=Fa,a.max=Ga,a.utc=h,a.unix=Zb,a.months=jc,a.isDate=d,a.locale=w,a.invalid=l,a.duration=Ya,a.isMoment=o,a.weekdays=lc,a.parseZone=$b,a.localeData=y,a.isDuration=Ia,a.monthsShort=kc,a.weekdaysMin=nc,a.defineLocale=x,a.weekdaysShort=mc,a.normalizeUnits=A,a.relativeTimeThreshold=Ec;var oe=a;return oe});
/*!
	Zoom 1.7.14
	license: MIT
	http://www.jacklmoore.com/zoom
*/
(function($){var defaults={url:false,callback:false,target:false,duration:120,on:"mouseover",touch:true,onZoomIn:false,onZoomOut:false,magnify:1};$.zoom=function(target,source,img,magnify){var targetHeight,targetWidth,sourceHeight,sourceWidth,xRatio,yRatio,offset,$target=$(target),position=$target.css("position"),$source=$(source);$target.css("position",/(absolute|fixed)/.test(position)?position:"relative");$target.css("overflow","hidden");img.style.width=img.style.height="";$(img).addClass("zoomImg").css({position:"absolute",top:0,left:0,opacity:0,width:img.width*magnify,height:img.height*magnify,border:"none",maxWidth:"none",maxHeight:"none"}).appendTo(target);return{init:function(){targetWidth=$target.outerWidth();targetHeight=$target.outerHeight();if(source===$target[0]){sourceWidth=targetWidth;sourceHeight=targetHeight}else{sourceWidth=$source.outerWidth();sourceHeight=$source.outerHeight()}xRatio=(img.width-targetWidth)/sourceWidth;yRatio=(img.height-targetHeight)/sourceHeight;offset=$source.offset()},move:function(e){var left=e.pageX-offset.left,top=e.pageY-offset.top;top=Math.max(Math.min(top,sourceHeight),0);left=Math.max(Math.min(left,sourceWidth),0);img.style.left=left*-xRatio+"px";img.style.top=top*-yRatio+"px"}}};$.fn.zoom=function(options){return this.each(function(){var settings=$.extend({},defaults,options||{}),target=settings.target||this,source=this,$source=$(source),$target=$(target),img=document.createElement("img"),$img=$(img),mousemove="mousemove.zoom",clicked=false,touched=false,$urlElement;if(!settings.url){$urlElement=$source.find("img");if($urlElement[0]){settings.url=$urlElement.data("src")||$urlElement.attr("src")}if(!settings.url){return}}(function(){var position=$target.css("position");var overflow=$target.css("overflow");$source.one("zoom.destroy",function(){$source.off(".zoom");$target.css("position",position);$target.css("overflow",overflow);$img.remove()})})();img.onload=function(){var zoom=$.zoom(target,source,img,settings.magnify);function start(e){zoom.init();zoom.move(e);$img.stop().fadeTo($.support.opacity?settings.duration:0,1,$.isFunction(settings.onZoomIn)?settings.onZoomIn.call(img):false)}function stop(){$img.stop().fadeTo(settings.duration,0,$.isFunction(settings.onZoomOut)?settings.onZoomOut.call(img):false)}if(settings.on==="grab"){$source.on("mousedown.zoom",function(e){if(e.which===1){$(document).one("mouseup.zoom",function(){stop();$(document).off(mousemove,zoom.move)});start(e);$(document).on(mousemove,zoom.move);e.preventDefault()}})}else if(settings.on==="click"){$source.on("click.zoom",function(e){if(clicked){return}else{clicked=true;start(e);$(document).on(mousemove,zoom.move);$(document).one("click.zoom",function(){stop();clicked=false;$(document).off(mousemove,zoom.move)});return false}})}else if(settings.on==="toggle"){$source.on("click.zoom",function(e){if(clicked){stop()}else{start(e)}clicked=!clicked})}else if(settings.on==="mouseover"){zoom.init();$source.on("mouseenter.zoom",start).on("mouseleave.zoom",stop).on(mousemove,zoom.move)}if(settings.touch){$source.on("touchstart.zoom",function(e){e.preventDefault();if(touched){touched=false;stop()}else{touched=true;start(e.originalEvent.touches[0]||e.originalEvent.changedTouches[0])}}).on("touchmove.zoom",function(e){e.preventDefault();zoom.move(e.originalEvent.touches[0]||e.originalEvent.changedTouches[0])})}if($.isFunction(settings.callback)){settings.callback.call(img)}};img.src=settings.url})};$.fn.zoom.defaults=defaults})(window.jQuery);
/*
 *  Bootstrap TouchSpin - v3.0.1
 *  A mobile and touch friendly input spinner component for Bootstrap 3.
 *  http://www.virtuosoft.eu/code/bootstrap-touchspin/
 *
 *  Made by István Ujj-Mészáros
 *  Under Apache License v2.0 License
 */
!function(a){"use strict";function b(a,b){return a+".touchspin_"+b}function c(c,d){return a.map(c,function(a){return b(a,d)})}var d=0;a.fn.TouchSpin=function(b){if("destroy"===b)return void this.each(function(){var b=a(this),d=b.data();a(document).off(c(["mouseup","touchend","touchcancel","mousemove","touchmove","scroll","scrollstart"],d.spinnerid).join(" "))});var e={min:0,max:100,initval:"",step:1,decimals:0,stepinterval:100,forcestepdivisibility:"round",stepintervaldelay:500,verticalbuttons:!1,verticalupclass:"glyphicon glyphicon-chevron-up",verticaldownclass:"glyphicon glyphicon-chevron-down",prefix:"",postfix:"",prefix_extraclass:"",postfix_extraclass:"",booster:!0,boostat:10,maxboostedstep:!1,mousewheel:!0,buttondown_class:"btn btn-default",buttonup_class:"btn btn-default",buttondown_txt:"-",buttonup_txt:"+"},f={min:"min",max:"max",initval:"init-val",step:"step",decimals:"decimals",stepinterval:"step-interval",verticalbuttons:"vertical-buttons",verticalupclass:"vertical-up-class",verticaldownclass:"vertical-down-class",forcestepdivisibility:"force-step-divisibility",stepintervaldelay:"step-interval-delay",prefix:"prefix",postfix:"postfix",prefix_extraclass:"prefix-extra-class",postfix_extraclass:"postfix-extra-class",booster:"booster",boostat:"boostat",maxboostedstep:"max-boosted-step",mousewheel:"mouse-wheel",buttondown_class:"button-down-class",buttonup_class:"button-up-class",buttondown_txt:"button-down-txt",buttonup_txt:"button-up-txt"};return this.each(function(){function g(){if(!J.data("alreadyinitialized")){if(J.data("alreadyinitialized",!0),d+=1,J.data("spinnerid",d),!J.is("input"))return void console.log("Must be an input.");j(),h(),u(),m(),p(),q(),r(),s(),D.input.css("display","block")}}function h(){""!==B.initval&&""===J.val()&&J.val(B.initval)}function i(a){l(a),u();var b=D.input.val();""!==b&&(b=Number(D.input.val()),D.input.val(b.toFixed(B.decimals)))}function j(){B=a.extend({},e,K,k(),b)}function k(){var b={};return a.each(f,function(a,c){var d="bts-"+c;J.is("[data-"+d+"]")&&(b[a]=J.data(d))}),b}function l(b){B=a.extend({},B,b)}function m(){var a=J.val(),b=J.parent();""!==a&&(a=Number(a).toFixed(B.decimals)),J.data("initvalue",a).val(a),J.addClass("form-control"),b.hasClass("input-group")?n(b):o()}function n(b){b.addClass("bootstrap-touchspin");var c,d,e=J.prev(),f=J.next(),g='<span class="input-group-addon bootstrap-touchspin-prefix">'+B.prefix+"</span>",h='<span class="input-group-addon bootstrap-touchspin-postfix">'+B.postfix+"</span>";e.hasClass("input-group-btn")?(c='<button class="'+B.buttondown_class+' bootstrap-touchspin-down" type="button">'+B.buttondown_txt+"</button>",e.append(c)):(c='<span class="input-group-btn"><button class="'+B.buttondown_class+' bootstrap-touchspin-down" type="button">'+B.buttondown_txt+"</button></span>",a(c).insertBefore(J)),f.hasClass("input-group-btn")?(d='<button class="'+B.buttonup_class+' bootstrap-touchspin-up" type="button">'+B.buttonup_txt+"</button>",f.prepend(d)):(d='<span class="input-group-btn"><button class="'+B.buttonup_class+' bootstrap-touchspin-up" type="button">'+B.buttonup_txt+"</button></span>",a(d).insertAfter(J)),a(g).insertBefore(J),a(h).insertAfter(J),C=b}function o(){var b;b=B.verticalbuttons?'<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix">'+B.prefix+'</span><span class="input-group-addon bootstrap-touchspin-postfix">'+B.postfix+'</span><span class="input-group-btn-vertical"><button class="'+B.buttondown_class+' bootstrap-touchspin-up" type="button"><i class="'+B.verticalupclass+'"></i></button><button class="'+B.buttonup_class+' bootstrap-touchspin-down" type="button"><i class="'+B.verticaldownclass+'"></i></button></span></div>':'<div class="input-group bootstrap-touchspin"><span class="input-group-btn"><button class="'+B.buttondown_class+' bootstrap-touchspin-down" type="button">'+B.buttondown_txt+'</button></span><span class="input-group-addon bootstrap-touchspin-prefix">'+B.prefix+'</span><span class="input-group-addon bootstrap-touchspin-postfix">'+B.postfix+'</span><span class="input-group-btn"><button class="'+B.buttonup_class+' bootstrap-touchspin-up" type="button">'+B.buttonup_txt+"</button></span></div>",C=a(b).insertBefore(J),a(".bootstrap-touchspin-prefix",C).after(J),J.hasClass("input-sm")?C.addClass("input-group-sm"):J.hasClass("input-lg")&&C.addClass("input-group-lg")}function p(){D={down:a(".bootstrap-touchspin-down",C),up:a(".bootstrap-touchspin-up",C),input:a("input",C),prefix:a(".bootstrap-touchspin-prefix",C).addClass(B.prefix_extraclass),postfix:a(".bootstrap-touchspin-postfix",C).addClass(B.postfix_extraclass)}}function q(){""===B.prefix&&D.prefix.hide(),""===B.postfix&&D.postfix.hide()}function r(){J.on("keydown",function(a){var b=a.keyCode||a.which;38===b?("up"!==M&&(w(),z()),a.preventDefault()):40===b&&("down"!==M&&(x(),y()),a.preventDefault())}),J.on("keyup",function(a){var b=a.keyCode||a.which;38===b?A():40===b&&A()}),J.on("blur",function(){u()}),D.down.on("keydown",function(a){var b=a.keyCode||a.which;(32===b||13===b)&&("down"!==M&&(x(),y()),a.preventDefault())}),D.down.on("keyup",function(a){var b=a.keyCode||a.which;(32===b||13===b)&&A()}),D.up.on("keydown",function(a){var b=a.keyCode||a.which;(32===b||13===b)&&("up"!==M&&(w(),z()),a.preventDefault())}),D.up.on("keyup",function(a){var b=a.keyCode||a.which;(32===b||13===b)&&A()}),D.down.on("mousedown.touchspin",function(a){D.down.off("touchstart.touchspin"),J.is(":disabled")||(x(),y(),a.preventDefault(),a.stopPropagation())}),D.down.on("touchstart.touchspin",function(a){D.down.off("mousedown.touchspin"),J.is(":disabled")||(x(),y(),a.preventDefault(),a.stopPropagation())}),D.up.on("mousedown.touchspin",function(a){D.up.off("touchstart.touchspin"),J.is(":disabled")||(w(),z(),a.preventDefault(),a.stopPropagation())}),D.up.on("touchstart.touchspin",function(a){D.up.off("mousedown.touchspin"),J.is(":disabled")||(w(),z(),a.preventDefault(),a.stopPropagation())}),D.up.on("mouseout touchleave touchend touchcancel",function(a){M&&(a.stopPropagation(),A())}),D.down.on("mouseout touchleave touchend touchcancel",function(a){M&&(a.stopPropagation(),A())}),D.down.on("mousemove touchmove",function(a){M&&(a.stopPropagation(),a.preventDefault())}),D.up.on("mousemove touchmove",function(a){M&&(a.stopPropagation(),a.preventDefault())}),a(document).on(c(["mouseup","touchend","touchcancel"],d).join(" "),function(a){M&&(a.preventDefault(),A())}),a(document).on(c(["mousemove","touchmove","scroll","scrollstart"],d).join(" "),function(a){M&&(a.preventDefault(),A())}),J.on("mousewheel DOMMouseScroll",function(a){if(B.mousewheel&&J.is(":focus")){var b=a.originalEvent.wheelDelta||-a.originalEvent.deltaY||-a.originalEvent.detail;a.stopPropagation(),a.preventDefault(),0>b?x():w()}})}function s(){J.on("touchspin.uponce",function(){A(),w()}),J.on("touchspin.downonce",function(){A(),x()}),J.on("touchspin.startupspin",function(){z()}),J.on("touchspin.startdownspin",function(){y()}),J.on("touchspin.stopspin",function(){A()}),J.on("touchspin.updatesettings",function(a,b){i(b)})}function t(a){switch(B.forcestepdivisibility){case"round":return(Math.round(a/B.step)*B.step).toFixed(B.decimals);case"floor":return(Math.floor(a/B.step)*B.step).toFixed(B.decimals);case"ceil":return(Math.ceil(a/B.step)*B.step).toFixed(B.decimals);default:return a}}function u(){var a,b,c;a=J.val(),""!==a&&(B.decimals>0&&"."===a||(b=parseFloat(a),isNaN(b)&&(b=0),c=b,b.toString()!==a&&(c=b),b<B.min&&(c=B.min),b>B.max&&(c=B.max),c=t(c),Number(a).toString()!==c.toString()&&(J.val(c),J.trigger("change"))))}function v(){if(B.booster){var a=Math.pow(2,Math.floor(L/B.boostat))*B.step;return B.maxboostedstep&&a>B.maxboostedstep&&(a=B.maxboostedstep,E=Math.round(E/a)*a),Math.max(B.step,a)}return B.step}function w(){u(),E=parseFloat(D.input.val()),isNaN(E)&&(E=0);var a=E,b=v();E+=b,E>B.max&&(E=B.max,J.trigger("touchspin.on.max"),A()),D.input.val(Number(E).toFixed(B.decimals)),a!==E&&J.trigger("change")}function x(){u(),E=parseFloat(D.input.val()),isNaN(E)&&(E=0);var a=E,b=v();E-=b,E<B.min&&(E=B.min,J.trigger("touchspin.on.min"),A()),D.input.val(E.toFixed(B.decimals)),a!==E&&J.trigger("change")}function y(){A(),L=0,M="down",J.trigger("touchspin.on.startspin"),J.trigger("touchspin.on.startdownspin"),H=setTimeout(function(){F=setInterval(function(){L++,x()},B.stepinterval)},B.stepintervaldelay)}function z(){A(),L=0,M="up",J.trigger("touchspin.on.startspin"),J.trigger("touchspin.on.startupspin"),I=setTimeout(function(){G=setInterval(function(){L++,w()},B.stepinterval)},B.stepintervaldelay)}function A(){switch(clearTimeout(H),clearTimeout(I),clearInterval(F),clearInterval(G),M){case"up":J.trigger("touchspin.on.stopupspin"),J.trigger("touchspin.on.stopspin");break;case"down":J.trigger("touchspin.on.stopdownspin"),J.trigger("touchspin.on.stopspin")}L=0,M=!1}var B,C,D,E,F,G,H,I,J=a(this),K=J.data(),L=0,M=!1;g()})}}(jQuery);
/*
     _ _      _       _
 ___| (_) ___| | __  (_)___
/ __| | |/ __| |/ /  | / __|
\__ \ | | (__|   < _ | \__ \
|___/_|_|\___|_|\_(_)/ |___/
                   |__/

 Version: 1.5.7
  Author: Ken Wheeler
 Website: http://kenwheeler.github.io
    Docs: http://kenwheeler.github.io/slick
    Repo: http://github.com/kenwheeler/slick
  Issues: http://github.com/kenwheeler/slick/issues

 */
!function(a){"use strict";"function"==typeof define&&define.amd?define(["jquery"],a):"undefined"!=typeof exports?module.exports=a(require("jquery")):a(jQuery)}(function(a){"use strict";var b=window.Slick||{};b=function(){function c(c,d){var f,e=this;e.defaults={accessibility:!0,adaptiveHeight:!1,appendArrows:a(c),appendDots:a(c),arrows:!0,asNavFor:null,prevArrow:'<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',nextArrow:'<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',autoplay:!1,autoplaySpeed:3e3,centerMode:!1,centerPadding:"50px",cssEase:"ease",customPaging:function(a,b){return'<button type="button" data-role="none" role="button" aria-required="false" tabindex="0">'+(b+1)+"</button>"},dots:!1,dotsClass:"slick-dots",draggable:!0,easing:"linear",edgeFriction:.35,fade:!1,focusOnSelect:!1,infinite:!0,initialSlide:0,lazyLoad:"ondemand",mobileFirst:!1,pauseOnHover:!0,pauseOnDotsHover:!1,respondTo:"window",responsive:null,rows:1,rtl:!1,slide:"",slidesPerRow:1,slidesToShow:1,slidesToScroll:1,speed:500,swipe:!0,swipeToSlide:!1,touchMove:!0,touchThreshold:5,useCSS:!0,variableWidth:!1,vertical:!1,verticalSwiping:!1,waitForAnimate:!0,zIndex:1e3},e.initials={animating:!1,dragging:!1,autoPlayTimer:null,currentDirection:0,currentLeft:null,currentSlide:0,direction:1,$dots:null,listWidth:null,listHeight:null,loadIndex:0,$nextArrow:null,$prevArrow:null,slideCount:null,slideWidth:null,$slideTrack:null,$slides:null,sliding:!1,slideOffset:0,swipeLeft:null,$list:null,touchObject:{},transformsEnabled:!1,unslicked:!1},a.extend(e,e.initials),e.activeBreakpoint=null,e.animType=null,e.animProp=null,e.breakpoints=[],e.breakpointSettings=[],e.cssTransitions=!1,e.hidden="hidden",e.paused=!1,e.positionProp=null,e.respondTo=null,e.rowCount=1,e.shouldClick=!0,e.$slider=a(c),e.$slidesCache=null,e.transformType=null,e.transitionType=null,e.visibilityChange="visibilitychange",e.windowWidth=0,e.windowTimer=null,f=a(c).data("slick")||{},e.options=a.extend({},e.defaults,f,d),e.currentSlide=e.options.initialSlide,e.originalSettings=e.options,"undefined"!=typeof document.mozHidden?(e.hidden="mozHidden",e.visibilityChange="mozvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(e.hidden="webkitHidden",e.visibilityChange="webkitvisibilitychange"),e.autoPlay=a.proxy(e.autoPlay,e),e.autoPlayClear=a.proxy(e.autoPlayClear,e),e.changeSlide=a.proxy(e.changeSlide,e),e.clickHandler=a.proxy(e.clickHandler,e),e.selectHandler=a.proxy(e.selectHandler,e),e.setPosition=a.proxy(e.setPosition,e),e.swipeHandler=a.proxy(e.swipeHandler,e),e.dragHandler=a.proxy(e.dragHandler,e),e.keyHandler=a.proxy(e.keyHandler,e),e.autoPlayIterator=a.proxy(e.autoPlayIterator,e),e.instanceUid=b++,e.htmlExpr=/^(?:\s*(<[\w\W]+>)[^>]*)$/,e.registerBreakpoints(),e.init(!0),e.checkResponsive(!0)}var b=0;return c}(),b.prototype.addSlide=b.prototype.slickAdd=function(b,c,d){var e=this;if("boolean"==typeof c)d=c,c=null;else if(0>c||c>=e.slideCount)return!1;e.unload(),"number"==typeof c?0===c&&0===e.$slides.length?a(b).appendTo(e.$slideTrack):d?a(b).insertBefore(e.$slides.eq(c)):a(b).insertAfter(e.$slides.eq(c)):d===!0?a(b).prependTo(e.$slideTrack):a(b).appendTo(e.$slideTrack),e.$slides=e.$slideTrack.children(this.options.slide),e.$slideTrack.children(this.options.slide).detach(),e.$slideTrack.append(e.$slides),e.$slides.each(function(b,c){a(c).attr("data-slick-index",b)}),e.$slidesCache=e.$slides,e.reinit()},b.prototype.animateHeight=function(){var a=this;if(1===a.options.slidesToShow&&a.options.adaptiveHeight===!0&&a.options.vertical===!1){var b=a.$slides.eq(a.currentSlide).outerHeight(!0);a.$list.animate({height:b},a.options.speed)}},b.prototype.animateSlide=function(b,c){var d={},e=this;e.animateHeight(),e.options.rtl===!0&&e.options.vertical===!1&&(b=-b),e.transformsEnabled===!1?e.options.vertical===!1?e.$slideTrack.animate({left:b},e.options.speed,e.options.easing,c):e.$slideTrack.animate({top:b},e.options.speed,e.options.easing,c):e.cssTransitions===!1?(e.options.rtl===!0&&(e.currentLeft=-e.currentLeft),a({animStart:e.currentLeft}).animate({animStart:b},{duration:e.options.speed,easing:e.options.easing,step:function(a){a=Math.ceil(a),e.options.vertical===!1?(d[e.animType]="translate("+a+"px, 0px)",e.$slideTrack.css(d)):(d[e.animType]="translate(0px,"+a+"px)",e.$slideTrack.css(d))},complete:function(){c&&c.call()}})):(e.applyTransition(),b=Math.ceil(b),d[e.animType]=e.options.vertical===!1?"translate3d("+b+"px, 0px, 0px)":"translate3d(0px,"+b+"px, 0px)",e.$slideTrack.css(d),c&&setTimeout(function(){e.disableTransition(),c.call()},e.options.speed))},b.prototype.asNavFor=function(b){var c=this,d=c.options.asNavFor;d&&null!==d&&(d=a(d).not(c.$slider)),null!==d&&"object"==typeof d&&d.each(function(){var c=a(this).slick("getSlick");c.unslicked||c.slideHandler(b,!0)})},b.prototype.applyTransition=function(a){var b=this,c={};c[b.transitionType]=b.options.fade===!1?b.transformType+" "+b.options.speed+"ms "+b.options.cssEase:"opacity "+b.options.speed+"ms "+b.options.cssEase,b.options.fade===!1?b.$slideTrack.css(c):b.$slides.eq(a).css(c)},b.prototype.autoPlay=function(){var a=this;a.autoPlayTimer&&clearInterval(a.autoPlayTimer),a.slideCount>a.options.slidesToShow&&a.paused!==!0&&(a.autoPlayTimer=setInterval(a.autoPlayIterator,a.options.autoplaySpeed))},b.prototype.autoPlayClear=function(){var a=this;a.autoPlayTimer&&clearInterval(a.autoPlayTimer)},b.prototype.autoPlayIterator=function(){var a=this;a.options.infinite===!1?1===a.direction?(a.currentSlide+1===a.slideCount-1&&(a.direction=0),a.slideHandler(a.currentSlide+a.options.slidesToScroll)):(0===a.currentSlide-1&&(a.direction=1),a.slideHandler(a.currentSlide-a.options.slidesToScroll)):a.slideHandler(a.currentSlide+a.options.slidesToScroll)},b.prototype.buildArrows=function(){var b=this;b.options.arrows===!0&&(b.$prevArrow=a(b.options.prevArrow).addClass("slick-arrow"),b.$nextArrow=a(b.options.nextArrow).addClass("slick-arrow"),b.slideCount>b.options.slidesToShow?(b.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),b.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),b.htmlExpr.test(b.options.prevArrow)&&b.$prevArrow.prependTo(b.options.appendArrows),b.htmlExpr.test(b.options.nextArrow)&&b.$nextArrow.appendTo(b.options.appendArrows),b.options.infinite!==!0&&b.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true")):b.$prevArrow.add(b.$nextArrow).addClass("slick-hidden").attr({"aria-disabled":"true",tabindex:"-1"}))},b.prototype.buildDots=function(){var c,d,b=this;if(b.options.dots===!0&&b.slideCount>b.options.slidesToShow){for(d='<ul class="'+b.options.dotsClass+'">',c=0;c<=b.getDotCount();c+=1)d+="<li>"+b.options.customPaging.call(this,b,c)+"</li>";d+="</ul>",b.$dots=a(d).appendTo(b.options.appendDots),b.$dots.find("li").first().addClass("slick-active").attr("aria-hidden","false")}},b.prototype.buildOut=function(){var b=this;b.$slides=b.$slider.children(b.options.slide+":not(.slick-cloned)").addClass("slick-slide"),b.slideCount=b.$slides.length,b.$slides.each(function(b,c){a(c).attr("data-slick-index",b).data("originalStyling",a(c).attr("style")||"")}),b.$slidesCache=b.$slides,b.$slider.addClass("slick-slider"),b.$slideTrack=0===b.slideCount?a('<div class="slick-track"/>').appendTo(b.$slider):b.$slides.wrapAll('<div class="slick-track"/>').parent(),b.$list=b.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(),b.$slideTrack.css("opacity",0),(b.options.centerMode===!0||b.options.swipeToSlide===!0)&&(b.options.slidesToScroll=1),a("img[data-lazy]",b.$slider).not("[src]").addClass("slick-loading"),b.setupInfinite(),b.buildArrows(),b.buildDots(),b.updateDots(),b.setSlideClasses("number"==typeof b.currentSlide?b.currentSlide:0),b.options.draggable===!0&&b.$list.addClass("draggable")},b.prototype.buildRows=function(){var b,c,d,e,f,g,h,a=this;if(e=document.createDocumentFragment(),g=a.$slider.children(),a.options.rows>1){for(h=a.options.slidesPerRow*a.options.rows,f=Math.ceil(g.length/h),b=0;f>b;b++){var i=document.createElement("div");for(c=0;c<a.options.rows;c++){var j=document.createElement("div");for(d=0;d<a.options.slidesPerRow;d++){var k=b*h+(c*a.options.slidesPerRow+d);g.get(k)&&j.appendChild(g.get(k))}i.appendChild(j)}e.appendChild(i)}a.$slider.html(e),a.$slider.children().children().children().css({width:100/a.options.slidesPerRow+"%",display:"inline-block"})}},b.prototype.checkResponsive=function(b,c){var e,f,g,d=this,h=!1,i=d.$slider.width(),j=window.innerWidth||a(window).width();if("window"===d.respondTo?g=j:"slider"===d.respondTo?g=i:"min"===d.respondTo&&(g=Math.min(j,i)),d.options.responsive&&d.options.responsive.length&&null!==d.options.responsive){f=null;for(e in d.breakpoints)d.breakpoints.hasOwnProperty(e)&&(d.originalSettings.mobileFirst===!1?g<d.breakpoints[e]&&(f=d.breakpoints[e]):g>d.breakpoints[e]&&(f=d.breakpoints[e]));null!==f?null!==d.activeBreakpoint?(f!==d.activeBreakpoint||c)&&(d.activeBreakpoint=f,"unslick"===d.breakpointSettings[f]?d.unslick(f):(d.options=a.extend({},d.originalSettings,d.breakpointSettings[f]),b===!0&&(d.currentSlide=d.options.initialSlide),d.refresh(b)),h=f):(d.activeBreakpoint=f,"unslick"===d.breakpointSettings[f]?d.unslick(f):(d.options=a.extend({},d.originalSettings,d.breakpointSettings[f]),b===!0&&(d.currentSlide=d.options.initialSlide),d.refresh(b)),h=f):null!==d.activeBreakpoint&&(d.activeBreakpoint=null,d.options=d.originalSettings,b===!0&&(d.currentSlide=d.options.initialSlide),d.refresh(b),h=f),b||h===!1||d.$slider.trigger("breakpoint",[d,h])}},b.prototype.changeSlide=function(b,c){var f,g,h,d=this,e=a(b.target);switch(e.is("a")&&b.preventDefault(),e.is("li")||(e=e.closest("li")),h=0!==d.slideCount%d.options.slidesToScroll,f=h?0:(d.slideCount-d.currentSlide)%d.options.slidesToScroll,b.data.message){case"previous":g=0===f?d.options.slidesToScroll:d.options.slidesToShow-f,d.slideCount>d.options.slidesToShow&&d.slideHandler(d.currentSlide-g,!1,c);break;case"next":g=0===f?d.options.slidesToScroll:f,d.slideCount>d.options.slidesToShow&&d.slideHandler(d.currentSlide+g,!1,c);break;case"index":var i=0===b.data.index?0:b.data.index||e.index()*d.options.slidesToScroll;d.slideHandler(d.checkNavigable(i),!1,c),e.children().trigger("focus");break;default:return}},b.prototype.checkNavigable=function(a){var c,d,b=this;if(c=b.getNavigableIndexes(),d=0,a>c[c.length-1])a=c[c.length-1];else for(var e in c){if(a<c[e]){a=d;break}d=c[e]}return a},b.prototype.cleanUpEvents=function(){var b=this;b.options.dots&&null!==b.$dots&&(a("li",b.$dots).off("click.slick",b.changeSlide),b.options.pauseOnDotsHover===!0&&b.options.autoplay===!0&&a("li",b.$dots).off("mouseenter.slick",a.proxy(b.setPaused,b,!0)).off("mouseleave.slick",a.proxy(b.setPaused,b,!1))),b.options.arrows===!0&&b.slideCount>b.options.slidesToShow&&(b.$prevArrow&&b.$prevArrow.off("click.slick",b.changeSlide),b.$nextArrow&&b.$nextArrow.off("click.slick",b.changeSlide)),b.$list.off("touchstart.slick mousedown.slick",b.swipeHandler),b.$list.off("touchmove.slick mousemove.slick",b.swipeHandler),b.$list.off("touchend.slick mouseup.slick",b.swipeHandler),b.$list.off("touchcancel.slick mouseleave.slick",b.swipeHandler),b.$list.off("click.slick",b.clickHandler),a(document).off(b.visibilityChange,b.visibility),b.$list.off("mouseenter.slick",a.proxy(b.setPaused,b,!0)),b.$list.off("mouseleave.slick",a.proxy(b.setPaused,b,!1)),b.options.accessibility===!0&&b.$list.off("keydown.slick",b.keyHandler),b.options.focusOnSelect===!0&&a(b.$slideTrack).children().off("click.slick",b.selectHandler),a(window).off("orientationchange.slick.slick-"+b.instanceUid,b.orientationChange),a(window).off("resize.slick.slick-"+b.instanceUid,b.resize),a("[draggable!=true]",b.$slideTrack).off("dragstart",b.preventDefault),a(window).off("load.slick.slick-"+b.instanceUid,b.setPosition),a(document).off("ready.slick.slick-"+b.instanceUid,b.setPosition)},b.prototype.cleanUpRows=function(){var b,a=this;a.options.rows>1&&(b=a.$slides.children().children(),b.removeAttr("style"),a.$slider.html(b))},b.prototype.clickHandler=function(a){var b=this;b.shouldClick===!1&&(a.stopImmediatePropagation(),a.stopPropagation(),a.preventDefault())},b.prototype.destroy=function(b){var c=this;c.autoPlayClear(),c.touchObject={},c.cleanUpEvents(),a(".slick-cloned",c.$slider).detach(),c.$dots&&c.$dots.remove(),c.options.arrows===!0&&(c.$prevArrow&&c.$prevArrow.length&&(c.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),c.htmlExpr.test(c.options.prevArrow)&&c.$prevArrow.remove()),c.$nextArrow&&c.$nextArrow.length&&(c.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),c.htmlExpr.test(c.options.nextArrow)&&c.$nextArrow.remove())),c.$slides&&(c.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function(){a(this).attr("style",a(this).data("originalStyling"))}),c.$slideTrack.children(this.options.slide).detach(),c.$slideTrack.detach(),c.$list.detach(),c.$slider.append(c.$slides)),c.cleanUpRows(),c.$slider.removeClass("slick-slider"),c.$slider.removeClass("slick-initialized"),c.unslicked=!0,b||c.$slider.trigger("destroy",[c])},b.prototype.disableTransition=function(a){var b=this,c={};c[b.transitionType]="",b.options.fade===!1?b.$slideTrack.css(c):b.$slides.eq(a).css(c)},b.prototype.fadeSlide=function(a,b){var c=this;c.cssTransitions===!1?(c.$slides.eq(a).css({zIndex:c.options.zIndex}),c.$slides.eq(a).animate({opacity:1},c.options.speed,c.options.easing,b)):(c.applyTransition(a),c.$slides.eq(a).css({opacity:1,zIndex:c.options.zIndex}),b&&setTimeout(function(){c.disableTransition(a),b.call()},c.options.speed))},b.prototype.fadeSlideOut=function(a){var b=this;b.cssTransitions===!1?b.$slides.eq(a).animate({opacity:0,zIndex:b.options.zIndex-2},b.options.speed,b.options.easing):(b.applyTransition(a),b.$slides.eq(a).css({opacity:0,zIndex:b.options.zIndex-2}))},b.prototype.filterSlides=b.prototype.slickFilter=function(a){var b=this;null!==a&&(b.unload(),b.$slideTrack.children(this.options.slide).detach(),b.$slidesCache.filter(a).appendTo(b.$slideTrack),b.reinit())},b.prototype.getCurrent=b.prototype.slickCurrentSlide=function(){var a=this;return a.currentSlide},b.prototype.getDotCount=function(){var a=this,b=0,c=0,d=0;if(a.options.infinite===!0)for(;b<a.slideCount;)++d,b=c+a.options.slidesToShow,c+=a.options.slidesToScroll<=a.options.slidesToShow?a.options.slidesToScroll:a.options.slidesToShow;else if(a.options.centerMode===!0)d=a.slideCount;else for(;b<a.slideCount;)++d,b=c+a.options.slidesToShow,c+=a.options.slidesToScroll<=a.options.slidesToShow?a.options.slidesToScroll:a.options.slidesToShow;return d-1},b.prototype.getLeft=function(a){var c,d,f,b=this,e=0;return b.slideOffset=0,d=b.$slides.first().outerHeight(!0),b.options.infinite===!0?(b.slideCount>b.options.slidesToShow&&(b.slideOffset=-1*b.slideWidth*b.options.slidesToShow,e=-1*d*b.options.slidesToShow),0!==b.slideCount%b.options.slidesToScroll&&a+b.options.slidesToScroll>b.slideCount&&b.slideCount>b.options.slidesToShow&&(a>b.slideCount?(b.slideOffset=-1*(b.options.slidesToShow-(a-b.slideCount))*b.slideWidth,e=-1*(b.options.slidesToShow-(a-b.slideCount))*d):(b.slideOffset=-1*b.slideCount%b.options.slidesToScroll*b.slideWidth,e=-1*b.slideCount%b.options.slidesToScroll*d))):a+b.options.slidesToShow>b.slideCount&&(b.slideOffset=(a+b.options.slidesToShow-b.slideCount)*b.slideWidth,e=(a+b.options.slidesToShow-b.slideCount)*d),b.slideCount<=b.options.slidesToShow&&(b.slideOffset=0,e=0),b.options.centerMode===!0&&b.options.infinite===!0?b.slideOffset+=b.slideWidth*Math.floor(b.options.slidesToShow/2)-b.slideWidth:b.options.centerMode===!0&&(b.slideOffset=0,b.slideOffset+=b.slideWidth*Math.floor(b.options.slidesToShow/2)),c=b.options.vertical===!1?-1*a*b.slideWidth+b.slideOffset:-1*a*d+e,b.options.variableWidth===!0&&(f=b.slideCount<=b.options.slidesToShow||b.options.infinite===!1?b.$slideTrack.children(".slick-slide").eq(a):b.$slideTrack.children(".slick-slide").eq(a+b.options.slidesToShow),c=f[0]?-1*f[0].offsetLeft:0,b.options.centerMode===!0&&(f=b.options.infinite===!1?b.$slideTrack.children(".slick-slide").eq(a):b.$slideTrack.children(".slick-slide").eq(a+b.options.slidesToShow+1),c=f[0]?-1*f[0].offsetLeft:0,c+=(b.$list.width()-f.outerWidth())/2)),c},b.prototype.getOption=b.prototype.slickGetOption=function(a){var b=this;return b.options[a]},b.prototype.getNavigableIndexes=function(){var e,a=this,b=0,c=0,d=[];for(a.options.infinite===!1?e=a.slideCount:(b=-1*a.options.slidesToScroll,c=-1*a.options.slidesToScroll,e=2*a.slideCount);e>b;)d.push(b),b=c+a.options.slidesToScroll,c+=a.options.slidesToScroll<=a.options.slidesToShow?a.options.slidesToScroll:a.options.slidesToShow;return d},b.prototype.getSlick=function(){return this},b.prototype.getSlideCount=function(){var c,d,e,b=this;return e=b.options.centerMode===!0?b.slideWidth*Math.floor(b.options.slidesToShow/2):0,b.options.swipeToSlide===!0?(b.$slideTrack.find(".slick-slide").each(function(c,f){return f.offsetLeft-e+a(f).outerWidth()/2>-1*b.swipeLeft?(d=f,!1):void 0}),c=Math.abs(a(d).attr("data-slick-index")-b.currentSlide)||1):b.options.slidesToScroll},b.prototype.goTo=b.prototype.slickGoTo=function(a,b){var c=this;c.changeSlide({data:{message:"index",index:parseInt(a)}},b)},b.prototype.init=function(b){var c=this;a(c.$slider).hasClass("slick-initialized")||(a(c.$slider).addClass("slick-initialized"),c.buildRows(),c.buildOut(),c.setProps(),c.startLoad(),c.loadSlider(),c.initializeEvents(),c.updateArrows(),c.updateDots()),b&&c.$slider.trigger("init",[c]),c.options.accessibility===!0&&c.initADA()},b.prototype.initArrowEvents=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.on("click.slick",{message:"previous"},a.changeSlide),a.$nextArrow.on("click.slick",{message:"next"},a.changeSlide))},b.prototype.initDotEvents=function(){var b=this;b.options.dots===!0&&b.slideCount>b.options.slidesToShow&&a("li",b.$dots).on("click.slick",{message:"index"},b.changeSlide),b.options.dots===!0&&b.options.pauseOnDotsHover===!0&&b.options.autoplay===!0&&a("li",b.$dots).on("mouseenter.slick",a.proxy(b.setPaused,b,!0)).on("mouseleave.slick",a.proxy(b.setPaused,b,!1))},b.prototype.initializeEvents=function(){var b=this;b.initArrowEvents(),b.initDotEvents(),b.$list.on("touchstart.slick mousedown.slick",{action:"start"},b.swipeHandler),b.$list.on("touchmove.slick mousemove.slick",{action:"move"},b.swipeHandler),b.$list.on("touchend.slick mouseup.slick",{action:"end"},b.swipeHandler),b.$list.on("touchcancel.slick mouseleave.slick",{action:"end"},b.swipeHandler),b.$list.on("click.slick",b.clickHandler),a(document).on(b.visibilityChange,a.proxy(b.visibility,b)),b.$list.on("mouseenter.slick",a.proxy(b.setPaused,b,!0)),b.$list.on("mouseleave.slick",a.proxy(b.setPaused,b,!1)),b.options.accessibility===!0&&b.$list.on("keydown.slick",b.keyHandler),b.options.focusOnSelect===!0&&a(b.$slideTrack).children().on("click.slick",b.selectHandler),a(window).on("orientationchange.slick.slick-"+b.instanceUid,a.proxy(b.orientationChange,b)),a(window).on("resize.slick.slick-"+b.instanceUid,a.proxy(b.resize,b)),a("[draggable!=true]",b.$slideTrack).on("dragstart",b.preventDefault),a(window).on("load.slick.slick-"+b.instanceUid,b.setPosition),a(document).on("ready.slick.slick-"+b.instanceUid,b.setPosition)},b.prototype.initUI=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.show(),a.$nextArrow.show()),a.options.dots===!0&&a.slideCount>a.options.slidesToShow&&a.$dots.show(),a.options.autoplay===!0&&a.autoPlay()},b.prototype.keyHandler=function(a){var b=this;a.target.tagName.match("TEXTAREA|INPUT|SELECT")||(37===a.keyCode&&b.options.accessibility===!0?b.changeSlide({data:{message:"previous"}}):39===a.keyCode&&b.options.accessibility===!0&&b.changeSlide({data:{message:"next"}}))},b.prototype.lazyLoad=function(){function g(b){a("img[data-lazy]",b).each(function(){var b=a(this),c=a(this).attr("data-lazy"),d=document.createElement("img");d.onload=function(){b.animate({opacity:0},100,function(){b.attr("src",c).animate({opacity:1},200,function(){b.removeAttr("data-lazy").removeClass("slick-loading")})})},d.src=c})}var c,d,e,f,b=this;b.options.centerMode===!0?b.options.infinite===!0?(e=b.currentSlide+(b.options.slidesToShow/2+1),f=e+b.options.slidesToShow+2):(e=Math.max(0,b.currentSlide-(b.options.slidesToShow/2+1)),f=2+(b.options.slidesToShow/2+1)+b.currentSlide):(e=b.options.infinite?b.options.slidesToShow+b.currentSlide:b.currentSlide,f=e+b.options.slidesToShow,b.options.fade===!0&&(e>0&&e--,f<=b.slideCount&&f++)),c=b.$slider.find(".slick-slide").slice(e,f),g(c),b.slideCount<=b.options.slidesToShow?(d=b.$slider.find(".slick-slide"),g(d)):b.currentSlide>=b.slideCount-b.options.slidesToShow?(d=b.$slider.find(".slick-cloned").slice(0,b.options.slidesToShow),g(d)):0===b.currentSlide&&(d=b.$slider.find(".slick-cloned").slice(-1*b.options.slidesToShow),g(d))},b.prototype.loadSlider=function(){var a=this;a.setPosition(),a.$slideTrack.css({opacity:1}),a.$slider.removeClass("slick-loading"),a.initUI(),"progressive"===a.options.lazyLoad&&a.progressiveLazyLoad()},b.prototype.next=b.prototype.slickNext=function(){var a=this;a.changeSlide({data:{message:"next"}})},b.prototype.orientationChange=function(){var a=this;a.checkResponsive(),a.setPosition()},b.prototype.pause=b.prototype.slickPause=function(){var a=this;a.autoPlayClear(),a.paused=!0},b.prototype.play=b.prototype.slickPlay=function(){var a=this;a.paused=!1,a.autoPlay()},b.prototype.postSlide=function(a){var b=this;b.$slider.trigger("afterChange",[b,a]),b.animating=!1,b.setPosition(),b.swipeLeft=null,b.options.autoplay===!0&&b.paused===!1&&b.autoPlay(),b.options.accessibility===!0&&b.initADA()},b.prototype.prev=b.prototype.slickPrev=function(){var a=this;a.changeSlide({data:{message:"previous"}})},b.prototype.preventDefault=function(a){a.preventDefault()},b.prototype.progressiveLazyLoad=function(){var c,d,b=this;c=a("img[data-lazy]",b.$slider).length,c>0&&(d=a("img[data-lazy]",b.$slider).first(),d.attr("src",d.attr("data-lazy")).removeClass("slick-loading").load(function(){d.removeAttr("data-lazy"),b.progressiveLazyLoad(),b.options.adaptiveHeight===!0&&b.setPosition()}).error(function(){d.removeAttr("data-lazy"),b.progressiveLazyLoad()}))},b.prototype.refresh=function(b){var c=this,d=c.currentSlide;c.destroy(!0),a.extend(c,c.initials,{currentSlide:d}),c.init(),b||c.changeSlide({data:{message:"index",index:d}},!1)},b.prototype.registerBreakpoints=function(){var c,d,e,b=this,f=b.options.responsive||null;if("array"===a.type(f)&&f.length){b.respondTo=b.options.respondTo||"window";for(c in f)if(e=b.breakpoints.length-1,d=f[c].breakpoint,f.hasOwnProperty(c)){for(;e>=0;)b.breakpoints[e]&&b.breakpoints[e]===d&&b.breakpoints.splice(e,1),e--;b.breakpoints.push(d),b.breakpointSettings[d]=f[c].settings}b.breakpoints.sort(function(a,c){return b.options.mobileFirst?a-c:c-a})}},b.prototype.reinit=function(){var b=this;b.$slides=b.$slideTrack.children(b.options.slide).addClass("slick-slide"),b.slideCount=b.$slides.length,b.currentSlide>=b.slideCount&&0!==b.currentSlide&&(b.currentSlide=b.currentSlide-b.options.slidesToScroll),b.slideCount<=b.options.slidesToShow&&(b.currentSlide=0),b.registerBreakpoints(),b.setProps(),b.setupInfinite(),b.buildArrows(),b.updateArrows(),b.initArrowEvents(),b.buildDots(),b.updateDots(),b.initDotEvents(),b.checkResponsive(!1,!0),b.options.focusOnSelect===!0&&a(b.$slideTrack).children().on("click.slick",b.selectHandler),b.setSlideClasses(0),b.setPosition(),b.$slider.trigger("reInit",[b]),b.options.autoplay===!0&&b.focusHandler()},b.prototype.resize=function(){var b=this;a(window).width()!==b.windowWidth&&(clearTimeout(b.windowDelay),b.windowDelay=window.setTimeout(function(){b.windowWidth=a(window).width(),b.checkResponsive(),b.unslicked||b.setPosition()},50))},b.prototype.removeSlide=b.prototype.slickRemove=function(a,b,c){var d=this;return"boolean"==typeof a?(b=a,a=b===!0?0:d.slideCount-1):a=b===!0?--a:a,d.slideCount<1||0>a||a>d.slideCount-1?!1:(d.unload(),c===!0?d.$slideTrack.children().remove():d.$slideTrack.children(this.options.slide).eq(a).remove(),d.$slides=d.$slideTrack.children(this.options.slide),d.$slideTrack.children(this.options.slide).detach(),d.$slideTrack.append(d.$slides),d.$slidesCache=d.$slides,d.reinit(),void 0)},b.prototype.setCSS=function(a){var d,e,b=this,c={};b.options.rtl===!0&&(a=-a),d="left"==b.positionProp?Math.ceil(a)+"px":"0px",e="top"==b.positionProp?Math.ceil(a)+"px":"0px",c[b.positionProp]=a,b.transformsEnabled===!1?b.$slideTrack.css(c):(c={},b.cssTransitions===!1?(c[b.animType]="translate("+d+", "+e+")",b.$slideTrack.css(c)):(c[b.animType]="translate3d("+d+", "+e+", 0px)",b.$slideTrack.css(c)))},b.prototype.setDimensions=function(){var a=this;a.options.vertical===!1?a.options.centerMode===!0&&a.$list.css({padding:"0px "+a.options.centerPadding}):(a.$list.height(a.$slides.first().outerHeight(!0)*a.options.slidesToShow),a.options.centerMode===!0&&a.$list.css({padding:a.options.centerPadding+" 0px"})),a.listWidth=a.$list.width(),a.listHeight=a.$list.height(),a.options.vertical===!1&&a.options.variableWidth===!1?(a.slideWidth=Math.ceil(a.listWidth/a.options.slidesToShow),a.$slideTrack.width(Math.ceil(a.slideWidth*a.$slideTrack.children(".slick-slide").length))):a.options.variableWidth===!0?a.$slideTrack.width(5e3*a.slideCount):(a.slideWidth=Math.ceil(a.listWidth),a.$slideTrack.height(Math.ceil(a.$slides.first().outerHeight(!0)*a.$slideTrack.children(".slick-slide").length)));var b=a.$slides.first().outerWidth(!0)-a.$slides.first().width();a.options.variableWidth===!1&&a.$slideTrack.children(".slick-slide").width(a.slideWidth-b)},b.prototype.setFade=function(){var c,b=this;b.$slides.each(function(d,e){c=-1*b.slideWidth*d,b.options.rtl===!0?a(e).css({position:"relative",right:c,top:0,zIndex:b.options.zIndex-2,opacity:0}):a(e).css({position:"relative",left:c,top:0,zIndex:b.options.zIndex-2,opacity:0})}),b.$slides.eq(b.currentSlide).css({zIndex:b.options.zIndex-1,opacity:1})},b.prototype.setHeight=function(){var a=this;if(1===a.options.slidesToShow&&a.options.adaptiveHeight===!0&&a.options.vertical===!1){var b=a.$slides.eq(a.currentSlide).outerHeight(!0);a.$list.css("height",b)}},b.prototype.setOption=b.prototype.slickSetOption=function(b,c,d){var f,g,e=this;if("responsive"===b&&"array"===a.type(c))for(g in c)if("array"!==a.type(e.options.responsive))e.options.responsive=[c[g]];else{for(f=e.options.responsive.length-1;f>=0;)e.options.responsive[f].breakpoint===c[g].breakpoint&&e.options.responsive.splice(f,1),f--;e.options.responsive.push(c[g])}else e.options[b]=c;d===!0&&(e.unload(),e.reinit())},b.prototype.setPosition=function(){var a=this;a.setDimensions(),a.setHeight(),a.options.fade===!1?a.setCSS(a.getLeft(a.currentSlide)):a.setFade(),a.$slider.trigger("setPosition",[a])},b.prototype.setProps=function(){var a=this,b=document.body.style;a.positionProp=a.options.vertical===!0?"top":"left","top"===a.positionProp?a.$slider.addClass("slick-vertical"):a.$slider.removeClass("slick-vertical"),(void 0!==b.WebkitTransition||void 0!==b.MozTransition||void 0!==b.msTransition)&&a.options.useCSS===!0&&(a.cssTransitions=!0),a.options.fade&&("number"==typeof a.options.zIndex?a.options.zIndex<3&&(a.options.zIndex=3):a.options.zIndex=a.defaults.zIndex),void 0!==b.OTransform&&(a.animType="OTransform",a.transformType="-o-transform",a.transitionType="OTransition",void 0===b.perspectiveProperty&&void 0===b.webkitPerspective&&(a.animType=!1)),void 0!==b.MozTransform&&(a.animType="MozTransform",a.transformType="-moz-transform",a.transitionType="MozTransition",void 0===b.perspectiveProperty&&void 0===b.MozPerspective&&(a.animType=!1)),void 0!==b.webkitTransform&&(a.animType="webkitTransform",a.transformType="-webkit-transform",a.transitionType="webkitTransition",void 0===b.perspectiveProperty&&void 0===b.webkitPerspective&&(a.animType=!1)),void 0!==b.msTransform&&(a.animType="msTransform",a.transformType="-ms-transform",a.transitionType="msTransition",void 0===b.msTransform&&(a.animType=!1)),void 0!==b.transform&&a.animType!==!1&&(a.animType="transform",a.transformType="transform",a.transitionType="transition"),a.transformsEnabled=null!==a.animType&&a.animType!==!1},b.prototype.setSlideClasses=function(a){var c,d,e,f,b=this;d=b.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden","true"),b.$slides.eq(a).addClass("slick-current"),b.options.centerMode===!0?(c=Math.floor(b.options.slidesToShow/2),b.options.infinite===!0&&(a>=c&&a<=b.slideCount-1-c?b.$slides.slice(a-c,a+c+1).addClass("slick-active").attr("aria-hidden","false"):(e=b.options.slidesToShow+a,d.slice(e-c+1,e+c+2).addClass("slick-active").attr("aria-hidden","false")),0===a?d.eq(d.length-1-b.options.slidesToShow).addClass("slick-center"):a===b.slideCount-1&&d.eq(b.options.slidesToShow).addClass("slick-center")),b.$slides.eq(a).addClass("slick-center")):a>=0&&a<=b.slideCount-b.options.slidesToShow?b.$slides.slice(a,a+b.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false"):d.length<=b.options.slidesToShow?d.addClass("slick-active").attr("aria-hidden","false"):(f=b.slideCount%b.options.slidesToShow,e=b.options.infinite===!0?b.options.slidesToShow+a:a,b.options.slidesToShow==b.options.slidesToScroll&&b.slideCount-a<b.options.slidesToShow?d.slice(e-(b.options.slidesToShow-f),e+f).addClass("slick-active").attr("aria-hidden","false"):d.slice(e,e+b.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false")),"ondemand"===b.options.lazyLoad&&b.lazyLoad()},b.prototype.setupInfinite=function(){var c,d,e,b=this;if(b.options.fade===!0&&(b.options.centerMode=!1),b.options.infinite===!0&&b.options.fade===!1&&(d=null,b.slideCount>b.options.slidesToShow)){for(e=b.options.centerMode===!0?b.options.slidesToShow+1:b.options.slidesToShow,c=b.slideCount;c>b.slideCount-e;c-=1)d=c-1,a(b.$slides[d]).clone(!0).attr("id","").attr("data-slick-index",d-b.slideCount).prependTo(b.$slideTrack).addClass("slick-cloned");for(c=0;e>c;c+=1)d=c,a(b.$slides[d]).clone(!0).attr("id","").attr("data-slick-index",d+b.slideCount).appendTo(b.$slideTrack).addClass("slick-cloned");b.$slideTrack.find(".slick-cloned").find("[id]").each(function(){a(this).attr("id","")})}},b.prototype.setPaused=function(a){var b=this;b.options.autoplay===!0&&b.options.pauseOnHover===!0&&(b.paused=a,a?b.autoPlayClear():b.autoPlay())},b.prototype.selectHandler=function(b){var c=this,d=a(b.target).is(".slick-slide")?a(b.target):a(b.target).parents(".slick-slide"),e=parseInt(d.attr("data-slick-index"));return e||(e=0),c.slideCount<=c.options.slidesToShow?(c.setSlideClasses(e),c.asNavFor(e),void 0):(c.slideHandler(e),void 0)},b.prototype.slideHandler=function(a,b,c){var d,e,f,g,h=null,i=this;return b=b||!1,i.animating===!0&&i.options.waitForAnimate===!0||i.options.fade===!0&&i.currentSlide===a||i.slideCount<=i.options.slidesToShow?void 0:(b===!1&&i.asNavFor(a),d=a,h=i.getLeft(d),g=i.getLeft(i.currentSlide),i.currentLeft=null===i.swipeLeft?g:i.swipeLeft,i.options.infinite===!1&&i.options.centerMode===!1&&(0>a||a>i.getDotCount()*i.options.slidesToScroll)?(i.options.fade===!1&&(d=i.currentSlide,c!==!0?i.animateSlide(g,function(){i.postSlide(d)}):i.postSlide(d)),void 0):i.options.infinite===!1&&i.options.centerMode===!0&&(0>a||a>i.slideCount-i.options.slidesToScroll)?(i.options.fade===!1&&(d=i.currentSlide,c!==!0?i.animateSlide(g,function(){i.postSlide(d)}):i.postSlide(d)),void 0):(i.options.autoplay===!0&&clearInterval(i.autoPlayTimer),e=0>d?0!==i.slideCount%i.options.slidesToScroll?i.slideCount-i.slideCount%i.options.slidesToScroll:i.slideCount+d:d>=i.slideCount?0!==i.slideCount%i.options.slidesToScroll?0:d-i.slideCount:d,i.animating=!0,i.$slider.trigger("beforeChange",[i,i.currentSlide,e]),f=i.currentSlide,i.currentSlide=e,i.setSlideClasses(i.currentSlide),i.updateDots(),i.updateArrows(),i.options.fade===!0?(c!==!0?(i.fadeSlideOut(f),i.fadeSlide(e,function(){i.postSlide(e)
})):i.postSlide(e),i.animateHeight(),void 0):(c!==!0?i.animateSlide(h,function(){i.postSlide(e)}):i.postSlide(e),void 0)))},b.prototype.startLoad=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.hide(),a.$nextArrow.hide()),a.options.dots===!0&&a.slideCount>a.options.slidesToShow&&a.$dots.hide(),a.$slider.addClass("slick-loading")},b.prototype.swipeDirection=function(){var a,b,c,d,e=this;return a=e.touchObject.startX-e.touchObject.curX,b=e.touchObject.startY-e.touchObject.curY,c=Math.atan2(b,a),d=Math.round(180*c/Math.PI),0>d&&(d=360-Math.abs(d)),45>=d&&d>=0?e.options.rtl===!1?"left":"right":360>=d&&d>=315?e.options.rtl===!1?"left":"right":d>=135&&225>=d?e.options.rtl===!1?"right":"left":e.options.verticalSwiping===!0?d>=35&&135>=d?"left":"right":"vertical"},b.prototype.swipeEnd=function(){var c,b=this;if(b.dragging=!1,b.shouldClick=b.touchObject.swipeLength>10?!1:!0,void 0===b.touchObject.curX)return!1;if(b.touchObject.edgeHit===!0&&b.$slider.trigger("edge",[b,b.swipeDirection()]),b.touchObject.swipeLength>=b.touchObject.minSwipe)switch(b.swipeDirection()){case"left":c=b.options.swipeToSlide?b.checkNavigable(b.currentSlide+b.getSlideCount()):b.currentSlide+b.getSlideCount(),b.slideHandler(c),b.currentDirection=0,b.touchObject={},b.$slider.trigger("swipe",[b,"left"]);break;case"right":c=b.options.swipeToSlide?b.checkNavigable(b.currentSlide-b.getSlideCount()):b.currentSlide-b.getSlideCount(),b.slideHandler(c),b.currentDirection=1,b.touchObject={},b.$slider.trigger("swipe",[b,"right"])}else b.touchObject.startX!==b.touchObject.curX&&(b.slideHandler(b.currentSlide),b.touchObject={})},b.prototype.swipeHandler=function(a){var b=this;if(!(b.options.swipe===!1||"ontouchend"in document&&b.options.swipe===!1||b.options.draggable===!1&&-1!==a.type.indexOf("mouse")))switch(b.touchObject.fingerCount=a.originalEvent&&void 0!==a.originalEvent.touches?a.originalEvent.touches.length:1,b.touchObject.minSwipe=b.listWidth/b.options.touchThreshold,b.options.verticalSwiping===!0&&(b.touchObject.minSwipe=b.listHeight/b.options.touchThreshold),a.data.action){case"start":b.swipeStart(a);break;case"move":b.swipeMove(a);break;case"end":b.swipeEnd(a)}},b.prototype.swipeMove=function(a){var d,e,f,g,h,b=this;return h=void 0!==a.originalEvent?a.originalEvent.touches:null,!b.dragging||h&&1!==h.length?!1:(d=b.getLeft(b.currentSlide),b.touchObject.curX=void 0!==h?h[0].pageX:a.clientX,b.touchObject.curY=void 0!==h?h[0].pageY:a.clientY,b.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(b.touchObject.curX-b.touchObject.startX,2))),b.options.verticalSwiping===!0&&(b.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(b.touchObject.curY-b.touchObject.startY,2)))),e=b.swipeDirection(),"vertical"!==e?(void 0!==a.originalEvent&&b.touchObject.swipeLength>4&&a.preventDefault(),g=(b.options.rtl===!1?1:-1)*(b.touchObject.curX>b.touchObject.startX?1:-1),b.options.verticalSwiping===!0&&(g=b.touchObject.curY>b.touchObject.startY?1:-1),f=b.touchObject.swipeLength,b.touchObject.edgeHit=!1,b.options.infinite===!1&&(0===b.currentSlide&&"right"===e||b.currentSlide>=b.getDotCount()&&"left"===e)&&(f=b.touchObject.swipeLength*b.options.edgeFriction,b.touchObject.edgeHit=!0),b.swipeLeft=b.options.vertical===!1?d+f*g:d+f*(b.$list.height()/b.listWidth)*g,b.options.verticalSwiping===!0&&(b.swipeLeft=d+f*g),b.options.fade===!0||b.options.touchMove===!1?!1:b.animating===!0?(b.swipeLeft=null,!1):(b.setCSS(b.swipeLeft),void 0)):void 0)},b.prototype.swipeStart=function(a){var c,b=this;return 1!==b.touchObject.fingerCount||b.slideCount<=b.options.slidesToShow?(b.touchObject={},!1):(void 0!==a.originalEvent&&void 0!==a.originalEvent.touches&&(c=a.originalEvent.touches[0]),b.touchObject.startX=b.touchObject.curX=void 0!==c?c.pageX:a.clientX,b.touchObject.startY=b.touchObject.curY=void 0!==c?c.pageY:a.clientY,b.dragging=!0,void 0)},b.prototype.unfilterSlides=b.prototype.slickUnfilter=function(){var a=this;null!==a.$slidesCache&&(a.unload(),a.$slideTrack.children(this.options.slide).detach(),a.$slidesCache.appendTo(a.$slideTrack),a.reinit())},b.prototype.unload=function(){var b=this;a(".slick-cloned",b.$slider).remove(),b.$dots&&b.$dots.remove(),b.$prevArrow&&b.htmlExpr.test(b.options.prevArrow)&&b.$prevArrow.remove(),b.$nextArrow&&b.htmlExpr.test(b.options.nextArrow)&&b.$nextArrow.remove(),b.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden","true").css("width","")},b.prototype.unslick=function(a){var b=this;b.$slider.trigger("unslick",[b,a]),b.destroy()},b.prototype.updateArrows=function(){var b,a=this;b=Math.floor(a.options.slidesToShow/2),a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&!a.options.infinite&&(a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false"),a.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false"),0===a.currentSlide?(a.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true"),a.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false")):a.currentSlide>=a.slideCount-a.options.slidesToShow&&a.options.centerMode===!1?(a.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")):a.currentSlide>=a.slideCount-1&&a.options.centerMode===!0&&(a.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")))},b.prototype.updateDots=function(){var a=this;null!==a.$dots&&(a.$dots.find("li").removeClass("slick-active").attr("aria-hidden","true"),a.$dots.find("li").eq(Math.floor(a.currentSlide/a.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden","false"))},b.prototype.visibility=function(){var a=this;document[a.hidden]?(a.paused=!0,a.autoPlayClear()):a.options.autoplay===!0&&(a.paused=!1,a.autoPlay())},b.prototype.initADA=function(){var b=this;b.$slides.add(b.$slideTrack.find(".slick-cloned")).attr({"aria-hidden":"true",tabindex:"-1"}).find("a, input, button, select").attr({tabindex:"-1"}),b.$slideTrack.attr("role","listbox"),b.$slides.not(b.$slideTrack.find(".slick-cloned")).each(function(c){a(this).attr({role:"option","aria-describedby":"slick-slide"+b.instanceUid+c})}),null!==b.$dots&&b.$dots.attr("role","tablist").find("li").each(function(c){a(this).attr({role:"presentation","aria-selected":"false","aria-controls":"navigation"+b.instanceUid+c,id:"slick-slide"+b.instanceUid+c})}).first().attr("aria-selected","true").end().find("button").attr("role","button").end().closest("div").attr("role","toolbar"),b.activateADA()},b.prototype.activateADA=function(){var a=this,b=a.$slider.find("*").is(":focus");a.$slideTrack.find(".slick-active").attr({"aria-hidden":"false",tabindex:"0"}).find("a, input, button, select").attr({tabindex:"0"}),b&&a.$slideTrack.find(".slick-active").focus()},b.prototype.focusHandler=function(){var b=this;b.$slider.on("focus.slick blur.slick","*",function(c){c.stopImmediatePropagation();var d=a(this);setTimeout(function(){b.isPlay&&(d.is(":focus")?(b.autoPlayClear(),b.paused=!0):(b.paused=!1,b.autoPlay()))},0)})},a.fn.slick=function(){var g,a=this,c=arguments[0],d=Array.prototype.slice.call(arguments,1),e=a.length,f=0;for(f;e>f;f++)if("object"==typeof c||"undefined"==typeof c?a[f].slick=new b(a[f],c):g=a[f].slick[c].apply(a[f].slick,d),"undefined"!=typeof g)return g;return a}});
$(function () {
    Origami.fastclick(document.body);
    var overlay = $('#body-overlay');
    var html = $('html');
    var closebtn = $('.close-btn');
    var wrap = $("body");
    $('.menu-toggler').click(function () {
        if (html.hasClass('open-slide-menu')) {
            html.removeClass('open-slide-menu');
            overlay.css({display: 'none'});
        } else {
            html.addClass('open-slide-menu');
            overlay.css({display: 'block'});
        }
    });

    overlay.click(function () {
        if (html.hasClass('open-slide-menu')) {
            html.removeClass('open-slide-menu');
            overlay.css({display: 'none'});
        }
    });

    closebtn.click(function () {
        if (html.hasClass('open-slide-menu')) {
            html.removeClass('open-slide-menu');
            overlay.css({display: 'none'});
        }
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() > 37) {
            wrap.addClass("header-fixed");
        }
        else {
            wrap.removeClass("header-fixed");
        }
    });
});

var Layout = function () {

     // IE mode
    var isRTL = false;
    var isIE8 = false;
    var isIE9 = false;
    var isIE10 = false;
    var isIE11 = false;

    var responsive = true;

    var responsiveHandlers = [];

    var handleInit = function() {

        if ($('body').css('direction') === 'rtl') {
            isRTL = true;
        }

        isIE8 = !! navigator.userAgent.match(/MSIE 8.0/);
        isIE9 = !! navigator.userAgent.match(/MSIE 9.0/);
        isIE10 = !! navigator.userAgent.match(/MSIE 10.0/);
        isIE11 = !! navigator.userAgent.match(/MSIE 11.0/);
        
        if (isIE10) {
            jQuery('html').addClass('ie10'); // detect IE10 version
        }
        if (isIE11) {
            jQuery('html').addClass('ie11'); // detect IE11 version
        }
    }

    // runs callback functions set by App.addResponsiveHandler().
    var runResponsiveHandlers = function () {
        // reinitialize other subscribed elements
        for (var i in responsiveHandlers) {
            var each = responsiveHandlers[i];
            each.call();
        }
    }

    // handle the layout reinitialization on window resize
    var handleResponsiveOnResize = function () {
        var resize;
        if (isIE8) {
            var currheight;
            $(window).resize(function () {
                if (currheight == document.documentElement.clientHeight) {
                    return; //quite event since only body resized not window.
                }
                if (resize) {
                    clearTimeout(resize);
                }
                resize = setTimeout(function () {
                    runResponsiveHandlers();
                }, 50); // wait 50ms until window resize finishes.                
                currheight = document.documentElement.clientHeight; // store last body client height
            });
        } else {
            $(window).resize(function () {
                if (resize) {
                    clearTimeout(resize);
                }
                resize = setTimeout(function () {
                    runResponsiveHandlers();
                }, 50); // wait 50ms until window resize finishes.
            });
        }
    }

    var handleIEFixes = function() {
        //fix html5 placeholder attribute for ie7 & ie8
        if (isIE8 || isIE9) { // ie8 & ie9
            // this is html5 placeholder fix for inputs, inputs with placeholder-no-fix class will be skipped(e.g: we need this for password fields)
            jQuery('input[placeholder]:not(.placeholder-no-fix), textarea[placeholder]:not(.placeholder-no-fix)').each(function () {

                var input = jQuery(this);

                if (input.val() == '' && input.attr("placeholder") != '') {
                    input.addClass("placeholder").val(input.attr('placeholder'));
                }

                input.focus(function () {
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                });

                input.blur(function () {
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                        input.val(input.attr('placeholder'));
                    }
                });
            });
        }
    }

    // Handles scrollable contents using jQuery SlimScroll plugin.
    var handleScrollers = function () {
        $('.scroller').each(function () {
            var height;
            if ($(this).attr("data-height")) {
                height = $(this).attr("data-height");
            } else {
                height = $(this).css('height');
            }
            $(this).slimScroll({
                allowPageScroll: true, // allow page scroll when the element scroll is ended
                size: '7px',
                color: ($(this).attr("data-handle-color")  ? $(this).attr("data-handle-color") : '#bbb'),
                railColor: ($(this).attr("data-rail-color")  ? $(this).attr("data-rail-color") : '#eaeaea'),
                position: isRTL ? 'left' : 'right',
                height: height,
                alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
                railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
                disableFadeOut: true
            });
        });
    }

    var handleSearch = function() {    
        $('.search-btn').click(function () {            
            if($('.search-btn').hasClass('show-search-icon')){
                if ($(window).width()>767) {
                    $('.search-box').fadeOut(300);
                } else {
                    $('.search-box').fadeOut(0);
                }
                $('.search-btn').removeClass('show-search-icon');
            } else {
                if ($(window).width()>767) {
                    $('.search-box').fadeIn(300);
                } else {
                    $('.search-box').fadeIn(0);
                }
                $('.search-btn').addClass('show-search-icon');
            } 
        }); 

        // close search box on body click
        if($('.search-btn').size() != 0) {
            $('.search-box, .search-btn').on('click', function(e){
                e.stopPropagation();
            });

            $('body').on('click', function() {
                if ($('.search-btn').hasClass('show-search-icon')) {
                    $('.search-btn').removeClass("show-search-icon");
                    $('.search-box').fadeOut(300);
                }
            });
        }
    }

    var handleMenu = function() {
        $(".header .navbar-toggle").click(function () {
            if ($(".header .navbar-collapse").hasClass("open")) {
                $(".header .navbar-collapse").slideDown(300)
                .removeClass("open");
            } else {             
                $(".header .navbar-collapse").slideDown(300)
                .addClass("open");
            }
        });
    }
    var handleSubMenuExt = function() {
        $(".header-navigation .dropdown").on("hover", function() {
            if ($(this).children(".header-navigation-content-ext").show()) {
                if ($(".header-navigation-content-ext").height()>=$(".header-navigation-description").height()) {
                    $(".header-navigation-description").css("height", $(".header-navigation-content-ext").height()+22);
                }
            }
        });        
    }

    var handleSidebarMenu = function () {
        $(".sidebar .dropdown > a").click(function (event) {
            if ($(this).next().hasClass('dropdown-menu')) {
                event.preventDefault();
                if ($(this).hasClass("collapsed") == false) {
                    $(this).addClass("collapsed");
                    $(this).siblings(".dropdown-menu").slideDown(300);
                } else {
                    $(this).removeClass("collapsed");
                    $(this).siblings(".dropdown-menu").slideUp(300);
                }
            } 
        });
    }

    function handleDifInits() { 
        $(".header .navbar-toggle span:nth-child(2)").addClass("short-icon-bar");
        $(".header .navbar-toggle span:nth-child(4)").addClass("short-icon-bar");
    }

    function handleUniform() {
        if (!jQuery().uniform) {
            return;
        }
        var test = $("input[type=checkbox]:not(.toggle), input[type=radio]:not(.toggle, .star)");
        if (test.size() > 0) {
            test.each(function () {
                    if ($(this).parents(".checker").size() == 0) {
                        $(this).show();
                        $(this).uniform();
                    }
                });
        }
    }

    var handleFancybox = function () {
        if (!jQuery.fancybox) {
            return;
        }
        
        jQuery(".fancybox-fast-view").fancybox();

        if (jQuery(".fancybox-button").size() > 0) {            
            jQuery(".fancybox-button").fancybox({
                groupAttr: 'data-rel',
                prevEffect: 'none',
                nextEffect: 'none',
                closeBtn: true,
                helpers: {
                    title: {
                        type: 'inside'
                    }
                }
            });

            $('.fancybox-video').fancybox({
                type: 'iframe'
            });
        }
    }

    // Handles Bootstrap Accordions.
    var handleAccordions = function () {
       
        jQuery('body').on('shown.bs.collapse', '.accordion.scrollable', function (e) {
            Layout.scrollTo($(e.target), -100);
        });
        
    }

    // Handles Bootstrap Tabs.
    var handleTabs = function () {
        // fix content height on tab click
        $('body').on('shown.bs.tab', '.nav.nav-tabs', function () {
            handleSidebarAndContentHeight();
        });

        //activate tab if tab id provided in the URL
        if (location.hash) {
            var tabid = location.hash.substr(1);
            $('a[href="#' + tabid + '"]').click();
        }
    }

    var handleMobiToggler = function () {
        $(".mobi-toggler").on("click", function(event) {
            event.preventDefault();//the default action of the event will not be triggered
            
            $(".header").toggleClass("menuOpened");
            $(".header").find(".header-navigation").toggle(300);
        });
    }

    var handleTheme = function () {
    
        var panel = $('.color-panel');
    
        // handle theme colors
        var setColor = function (color) {
            $('#style-color').attr("href", "../../assets/corporate/css/themes/" + color + ".css");
            $('.corporate .site-logo img').attr("src", "../../assets/corporate/img/logos/logo-corp-" + color + ".png");
            $('.ecommerce .site-logo img').attr("src", "../../assets/corporate/img/logos/logo-shop-" + color + ".png");
        }

        $('.icon-color', panel).click(function () {
            $('.color-mode').show();
            $('.icon-color-close').show();
        });

        $('.icon-color-close', panel).click(function () {
            $('.color-mode').hide();
            $('.icon-color-close').hide();
        });

        $('li', panel).click(function () {
            var color = $(this).attr("data-style");
            setColor(color);
            $('.inline li', panel).removeClass("current");
            $(this).addClass("current");
        });
    }
	
    return {
        init: function () {
            // init core variables
            handleTheme();
            handleInit();
            handleResponsiveOnResize();
            handleIEFixes();
            handleSearch();
            handleFancybox();
            handleDifInits();
            handleSidebarMenu();
            handleAccordions();
            handleMenu();
            handleScrollers();
            handleSubMenuExt();
            handleMobiToggler();
        },

        initUniform: function (els) {
            if (els) {
                jQuery(els).each(function () {
                        if ($(this).parents(".checker").size() == 0) {
                            $(this).show();
                            $(this).uniform();
                        }
                    });
            } else {
                handleUniform();
            }
        },

        initTwitter: function () {
            !function(d,s,id){
                var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}
            }(document,"script","twitter-wjs");
        },

        initTouchspin: function () {
            $(".product-quantity .form-control").TouchSpin({
                buttondown_class: "btn quantity-down",
                buttonup_class: "btn quantity-up"
            });
            $(".quantity-down").html("<i class='fa fa-angle-down'></i>");
            $(".quantity-up").html("<i class='fa fa-angle-up'></i>");
        },

        initFixHeaderWithPreHeader: function () {
            jQuery(window).scroll(function() {                
                if (jQuery(window).scrollTop()>37){
                    jQuery("body").addClass("page-header-fixed");
                }
                else {
                    jQuery("body").removeClass("page-header-fixed");
                }
            });
        },

        initNavScrolling: function () {
            function NavScrolling () {
                if (jQuery(window).scrollTop()>60){
                    jQuery(".header").addClass("reduce-header");
                }
                else {
                    jQuery(".header").removeClass("reduce-header");
                }
            }
            
            NavScrolling();
            
            jQuery(window).scroll(function() {
                NavScrolling ();
            });
        },

        initOWL: function () {
            $(".owl-carousel6-brands").owlCarousel({
                pagination: false,
                navigation: true,
                items: 6,
                addClassActive: true,
                itemsCustom : [
                    [0, 1],
                    [320, 1],
                    [480, 2],
                    [700, 3],
                    [975, 5],
                    [1200, 6],
                    [1400, 6],
                    [1600, 6]
                ],
            });

            $(".owl-carousel5").owlCarousel({
                pagination: false,
                navigation: true,
                items: 5,
                addClassActive: true,
                itemsCustom : [
                    [0, 1],
                    [320, 1],
                    [480, 2],
                    [660, 2],
                    [700, 3],
                    [768, 3],
                    [992, 4],
                    [1024, 4],
                    [1200, 5],
                    [1400, 5],
                    [1600, 5]
                ],
            });

            $(".owl-carousel4").owlCarousel({
                pagination: false,
                navigation: true,
                items: 4,
                addClassActive: true,
            });

            $(".owl-carousel3").owlCarousel({
                pagination: false,
                navigation: true,
                items: 3,
                addClassActive: true,
                itemsCustom : [
                    [0, 1],
                    [320, 1],
                    [480, 2],
                    [700, 3],
                    [768, 2],
                    [1024, 3],
                    [1200, 3],
                    [1400, 3],
                    [1600, 3]
                ],
            });

            $(".owl-carousel2").owlCarousel({
                pagination: false,
                navigation: true,
                items: 2,
                addClassActive: true,
                itemsCustom : [
                    [0, 1],
                    [320, 1],
                    [480, 2],
                    [700, 3],
                    [975, 2],
                    [1200, 2],
                    [1400, 2],
                    [1600, 2]
                ],
            });
        },

        initImageZoom: function () {
            $('.product-main-image').zoom({url: $('.product-main-image img').attr('data-BigImgSrc')});
        },

        initSliderRange: function () {
            $( "#slider-range" ).slider({
              range: true,
              min: 0,
              max: 500,
              values: [ 50, 250 ],
              slide: function( event, ui ) {
                $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
              }
            });
            $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
            " - $" + $( "#slider-range" ).slider( "values", 1 ) );
        },

        // wrapper function to scroll(focus) to an element
        scrollTo: function (el, offeset) {
            var pos = (el && el.size() > 0) ? el.offset().top : 0;
            if (el) {
                if ($('body').hasClass('page-header-fixed')) {
                    pos = pos - $('.header').height(); 
                }            
                pos = pos + (offeset ? offeset : -1 * el.height());
            }

            jQuery('html,body').animate({
                scrollTop: pos
            }, 'slow');
        },

        //public function to add callback a function which will be called on window resize
        addResponsiveHandler: function (func) {
            responsiveHandlers.push(func);
        },

        scrollTop: function () {
            App.scrollTo();
        },

        gridOption1: function () {
            $(function(){
                $('.grid-v1').mixitup();
            });    
        }

    };
}();
Vue.filter('formatNumber', function (data, currency) {
    if (currency === 'IDR') {
        return accounting.formatNumber(data, 0, '.', ',');
    }

    return accounting.formatNumber(data, 2, '.', ',');
});

var scrolltotop = {
    //startline: Integer. Number of pixels from top of doc scrollbar is scrolled before showing control
    //scrollto: Keyword (Integer, or "Scroll_to_Element_ID"). How far to scroll document up when control is clicked on (0=top).
    setting: {startline: 100, scrollto: 0, scrollduration: 1000, fadeduration: [500, 100]},
    controlHTML: '<img src="/shop-assets/img/up.png" style="width:40px; height:40px" />', //HTML for control, which is auto wrapped in DIV w/ ID="topcontrol"
    controlattrs: {offsetx: 10, offsety: 10}, //offset of control relative to right/ bottom of window corner
    anchorkeyword: '#top', //Enter href value of HTML anchors on the page that should also act as "Scroll Up" links

    state: {isvisible: false, shouldvisible: false},

    scrollup: function () {
        if (!this.cssfixedsupport) //if control is positioned using JavaScript
            this.$control.css({opacity: 0}) //hide control immediately after clicking it
        var dest = isNaN(this.setting.scrollto) ? this.setting.scrollto : parseInt(this.setting.scrollto)
        if (typeof dest == "string" && jQuery('#' + dest).length == 1) //check element set by string exists
            dest = jQuery('#' + dest).offset().top
        else
            dest = 0
        this.$body.animate({scrollTop: dest}, this.setting.scrollduration);
    },

    keepfixed: function () {
        var $window = jQuery(window)
        var controlx = $window.scrollLeft() + $window.width() - this.$control.width() - this.controlattrs.offsetx
        var controly = $window.scrollTop() + $window.height() - this.$control.height() - this.controlattrs.offsety
        this.$control.css({left: controlx + 'px', top: controly + 'px'})
    },

    togglecontrol: function () {
        var scrolltop = jQuery(window).scrollTop()
        if (!this.cssfixedsupport)
            this.keepfixed()
        this.state.shouldvisible = (scrolltop >= this.setting.startline) ? true : false
        if (this.state.shouldvisible && !this.state.isvisible) {
            this.$control.stop().animate({opacity: 1}, this.setting.fadeduration[0])
            this.state.isvisible = true
        }
        else if (this.state.shouldvisible == false && this.state.isvisible) {
            this.$control.stop().animate({opacity: 0}, this.setting.fadeduration[1])
            this.state.isvisible = false
        }
    },

    init: function () {
        jQuery(document).ready(function ($) {
            var mainobj = scrolltotop
            var iebrws = document.all
            mainobj.cssfixedsupport = !iebrws || iebrws && document.compatMode == "CSS1Compat" && window.XMLHttpRequest //not IE or IE7+ browsers in standards mode
            mainobj.$body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body')
            mainobj.$control = $('<div id="topcontrol">' + mainobj.controlHTML + '</div>')
                .css({
                    position: mainobj.cssfixedsupport ? 'fixed' : 'absolute',
                    bottom: mainobj.controlattrs.offsety,
                    right: mainobj.controlattrs.offsetx,
                    opacity: 0,
                    cursor: 'pointer'
                })
                .attr({title: 'Scroll Back to Top'})
                .click(function () {
                    mainobj.scrollup();
                    return false
                })
                .appendTo('body')
            if (document.all && !window.XMLHttpRequest && mainobj.$control.text() != '') //loose check for IE6 and below, plus whether control contains any text
                mainobj.$control.css({width: mainobj.$control.width()}) //IE6- seems to require an explicit width on a DIV containing text
            mainobj.togglecontrol()
            $('a[href="' + mainobj.anchorkeyword + '"]').click(function () {
                mainobj.scrollup()
                return false
            })
            $(window).bind('scroll resize', function (e) {
                mainobj.togglecontrol()
            })
        })
    }
}

scrolltotop.init()
//# sourceMappingURL=main.js.map
