var BrandPage = function () {

    return {
        init: function () {

            $('#new-arrival').on('change', '#new-arrival-content', function() {

                var title = '';

                if ($(this).attr('value') == 'register') {
                    title = 'Step 2: Account &amp; Billing Details';
                } else {
                    title = 'Step 2: Billing Details';
                }

                $('#payment-address .accordion-toggle').html(title);
            });

        }
    };

}();
