var socket = io(window.location.hostname + ':6001');
new Vue({
    el: '#app',
    data: {
        notification: 0,
        query: '',
        data: [],
        currentPage: 1,
        totalPage: 1,
        options: [],
        selected: 0
    },
    computed: {
        isLast: function () {
            return this.currentPage === this.totalPage;
        },
        isFirst: function () {
            return this.currentPage === 1;
        }
    },
    watch: {
        'selected': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        },
        'query': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        }
    },
    methods: {
        getData: function (page) {
            if (!page) {
                page = 1;
            }
            var self = this;
            $.get('/api/users/cashiers', {
                query: this.query,
                filter: this.selected,
                page: page
            }).done(function (data) {
                self.data = data.data;
                self.currentPage = data.current_page;
                self.totalPage = data.last_page;
            });
        },
        prev: function () {
            if (this.currentPage === 1) {
                return;
            }
            this.getData(this.currentPage - 1);
        },
        next: function () {
            if (this.currentPage === this.totalPage) {
                return;
            }
            this.getData(this.currentPage + 1);
        },
        activate: function (data) {
            var self = this;
            bootbox.confirm('Are you sure to activate ' + data.user.username + '?', function (result) {
                if (result) {
                    $.get('/api/user/' + data.user.id + '/activate')
                        .done(function () {
                            toastr.success('cashier ' + data.user.username + ' activated');
                            self.getData();
                        });
                }
            });
        },
        deactivate: function (data) {
            var self = this;
            bootbox.confirm('Are you sure to deactivate ' + data.user.username + '?', function (result) {
                if (result) {
                    $.get('/api/user/' + data.user.id + '/deactivate')
                        .done(function () {
                            toastr.success('cashier ' + data.user.username + ' deactivated');
                            self.getData();
                        });
                }
            });
        },
        deleteAccount: function (data) {
            var self = this;
            bootbox.confirm('Are you sure to delete ' + data.user.username + '?', function (result) {
                if (result) {
                    $.get('/api/user/' + data.user.id + '/delete')
                        .done(function () {
                            toastr.success('cashier ' + data.user.username + ' deleted');
                            self.getData();
                        });
                }
            });
        },
        getMerchants: function () {
            var self = this;
            $.get('/api/users/merchants/select')
                .done(function (data) {
                    self.options = data;
                });
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        var self = this;
        App.init();
        Layout.init();
        socket.on('orders:created', function (message) {
            self.notification += 1;
        });
        this.getData();
        this.getMerchants();
        this.getNotif();
    }
});
//# sourceMappingURL=cashiers.js.map
