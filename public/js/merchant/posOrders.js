Vue.filter('length', function (data) {
    return data.length;
});
var socket = io(window.location.hostname + ':6001');

new Vue({
    el: '#app',
    data: {
        notification: 0,
        data: [],
        options: [],
        selected: '',
        currentPage: 1,
        totalPage: 1,
        paymentType: 'all'
    },
    computed: {
        isLast: function () {
            return this.currentPage === this.totalPage;
        },
        isFirst: function () {
            return this.currentPage === 1;
        }
    },
    watch: {
        'selected': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        },
        'query': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        },
        'paymentType': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        }
    },
    methods: {
        getData: function (page) {
            if (!page) {
                page = 1;
            }

            var self = this;
            $.get('/api/pos/orders', {
                page: page,
                type: this.paymentType
            }).done(function (data) {
                self.data = data.data;
                self.currentPage = data.current_page;
                self.totalPage = data.last_page;
            });
        },
        prev: function () {
            if (this.currentPage === 1) {
                return;
            }
            this.getData(this.currentPage - 1);
        },
        next: function () {
            if (this.currentPage === this.totalPage) {
                return;
            }
            this.getData(this.currentPage + 1);
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        }
    },
    ready: function () {
        var self = this;
        App.init();
        Layout.init();
        this.getData();
        this.getNotif();
        socket.on('merchant.' + obid.authId + ':orders.confirmed', function (message) {
            self.notification += 1;
        });
    }
});
//# sourceMappingURL=posOrders.js.map
