Vue.component('modal', {
    template: '#modal',
    props: ['show']
});
Vue.filter('length', function (data) {
    return data.length;
});
var vm = new Vue({
    el: '#app',
    data: {
        notification: 0,
        data: [],
        options: [],
        selected: '',
        currentPage: 1,
        totalPage: 1,
        query: '',
        newData: [],
        formItem: {
            orderId: '',
            noResi: '',
            berat: '',
            type: ''
        },
        showModalShipmentForm: false,
        showModalView: false,
        viewItem: {}
    },
    computed: {
        isLast: function () {
            return this.currentPage === this.totalPage;
        },
        isFirst: function () {
            return this.currentPage === 1;
        }
    },
    watch: {
        'query': function (nv, ov) {
            if (nv === ov) {
                return;
            }
            this.currentPage = 1;
            this.getData();
        }
    },
    methods: {
        getData: function (page) {
            if (!page) {
                page = 1;
            }

            var self = this;
            $.get('/api/orders/confirmed', {
                query: this.query,
                page: page
            }).done(function (data) {
                self.data = data.data;
                self.currentPage = data.current_page;
                self.totalPage = data.last_page;
            });
        },
        prev: function () {
            if (this.currentPage === 1) {
                return;
            }
            this.getData(this.currentPage - 1);
        },
        next: function () {
            if (this.currentPage === this.totalPage) {
                return;
            }
            this.getData(this.currentPage + 1);
        },
        showMItem: function (data) {
            this.formItem.orderId = data['id'];
            this.formItem.noResi = '';
            this.formItem.berat = data['total_weight'];
            this.formItem.type = 'JNE - ' + data['shippingType'];
            this.showModalShipmentForm = true;
        },
        submitResi: function (e) {
            e.preventDefault();
            var formData = new FormData();

            formData.append('orderId', this.formItem.orderId);
            formData.append('noResi', this.formItem.noResi);
            var self = this;
            $.ajax({
                url: '/api/orders/resi',
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false
            }).done(function (data) {
                if (data.created) {
                    toastr.success("Order Berhasil dikonfirm");
                    self.showModalShipmentForm = false;
                    self.getData();
                } else {
                    toastr.error("Order gagal dikonfirm");
                }
            });
        },
        view: function (data) {
            this.showModalView = true;
            this.viewItem = data;
        },
        getNotif: function () {
            var self = this;
            $.get('/api/notif')
                .done(function (data) {
                    self.notification = data.count;
                });
        },
        printInvoice: function (orderId) {
            var newTab = window.open('', '_blank');
            newTab.location = "http://shipper.originalbrands.localapp/orders/" + orderId + "/print";
        }
    },
    ready: function () {
        App.init();
        Layout.init();
        this.getData();
    }
});
//# sourceMappingURL=shippings.js.map
