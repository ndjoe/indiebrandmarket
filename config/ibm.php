<?php

return [
    'categories' => [
        'top' => 1,
        'bottom' => 2,
        'outerwear' => 3,
        'footwear' => 4,
        'accesories' => 5
    ],
    'sizes' => [
        'allsize' => 1,
        'clothing' => [
            's' => 2,
            'm' => 3,
            'l' => 4,
            'xl' => 5
        ],
        'shoes' => [
            '35' => 6,
            '36' => 7,
            '37' => 8
        ],
        'bottom' => [
            '26' => 9,
            '27' => 10,
            '28' => 11,
            '29' => 12,
            '30' => 13
        ]
    ],
    'subcategories' => [
        ['t-shirt', 'shirt', 'hat'],
        ['short', 'trouser'],
        ['jacket'],
        ['sneaker'],
        ['ring']
    ]
];