<?php

return [

    // The default gateway to use
    'default' => 'paypal',

    // Add in each gateway here
    'gateways' => [
        'paypal' => [
            'driver' => 'PayPal_Express',
            'options' => [
                'solutionType' => '',
                'landingPage' => '',
                'headerImageUrl' => 'http://originalbrands.co.id/wp-content/uploads/2015/11/LOGO-WEBSITE-300x30.png'
            ]
        ]
    ]
];