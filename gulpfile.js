var elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    /**
     * mandatory.js & style.css
     */
    mix.scripts([
        'plugins/js/jquery.min.js',
        'plugins/js/jquery-migrate.min.js',
        'plugins/js/vue.js',
        'plugins/js/moment.min.js',
        'plugins/js/accounting.min.js',
        'plugins/js/jquery-ui/jquery-ui.min.js',
        'plugins/js/bootstrap.js',
        'plugins/js/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
        'plugins/js/jquery-slimscroll/jquery.slimscroll.min.js',
        'plugins/js/jquery.blockui.min.js',
        'plugins/js/jquery.cokie.min.js',
        'plugins/js/uniform/jquery.uniform.min.js',
        'plugins/js/bootstrap-switch/js/bootstrap-switch.min.js',
        'plugins/bootbox/bootbox.min.js',
        'plugins/bootstrap-toastr/toastr.js',
        'plugins/socket.io.js',
        'app.js',
        'layout.js',
        'filters.js',
        'errorhandling.js'
    ], 'public/js/mandatory.js');
    mix.sass('bootstrap.scss', 'public/css/bootstrap.css')
        .sass('layouts/layout7/layout.scss', 'public/css/layout.css')
        .sass([
            'global/components-rounded.scss',
            'global/plugins.scss'
        ], 'public/css/components.css')
        .styles([
            './public/css/bootstrap.css',
            './resources/assets/js/plugins/css/font-awesome.css',
            './resources/assets/js/plugins/css/simple-line-icons.css',
            './resources/assets/js/plugins/css/uniform.default.css',
            './resources/assets/js/plugins/bootstrap-toastr/toastr.css',
            './public/css/layout.css',
            './public/assets/css/components.css',
            'custom.css'
        ], 'public/css/style.css');

    /**
     * Dashboard Merchant
     */
    mix.scripts([
        'plugins/morris/raphael-min.js',
        'plugins/morris/morris.min.js',
        'merchant/dashboard.js'
    ], 'public/js/merchant/dashboard.js')
        .styles('./resources/assets/js/plugins/morris/morris.css', 'public/css/merchant/dashboard.css');

    /**
     * index products merchant
     *
     * */
    mix.scripts([
        'plugins/selectize/selectize.min.js',
        'plugins/vue-selectize.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'plugins/bootstrap-touchspin/bootstrap.touchspin.min.js',
        'merchant/products.js'
    ], 'public/js/merchant/products.js')
        .styles([
            './resources/assets/js/plugins/selectize/selectize.css',
            './resources/assets/js/plugins/selectize/selectize.bootstrap3.css',
            './resources/assets/js/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css',
            'bootstrap-datepicker.standalone.min.css'
        ], 'public/css/merchant/products.css');

    /**
     * Create Product Merchant
     */
    mix.scripts([
        'plugins/bootstrap-fileinput/bootstrap-fileinput.js',
        'plugins/selectize/selectize.min.js',
        'plugins/vue-selectize.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'plugins/bootstrap-touchspin/bootstrap.touchspin.min.js',
        'plugins/validator.min.js',
        'merchant/createProduct.js'
    ], 'public/js/merchant/createProduct.js')
        .styles([
            './resources/assets/js/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
            './resources/assets/js/plugins/selectize/selectize.css',
            './resources/assets/js/plugins/selectize/selectize.bootstrap3.css',
            './resources/assets/js/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css',
            'bootstrap-datepicker.standalone.min.css'
        ], 'public/css/merchant/createProduct.css');

    /**
     * posOrders.js
     */
    mix.scripts([
        'merchant/posOrders.js'
    ], 'public/js/merchant/posOrders.js');

    /**
     * onlineOrders.js
     */
    mix.scripts([
        'merchant/onlineOrders.js'
    ], 'public/js/merchant/onlineOrders.js');

    /**
     * obidReport
     */
    mix.scripts([
        'plugins/morris/raphael-min.js',
        'plugins/morris/morris.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'merchant/obidReport.js'
    ], 'public/js/merchant/obidReport.js')
        .styles(['./resources/assets/js/plugins/morris/morris.css'], 'public/css/merchant/obidReport.css');

    /**
     * pos.js
     */
    mix.scripts(['merchant/pos.js'], 'public/js/merchant/pos.js');

    /**
     * createCashier.js
     */
    mix.scripts([
        'plugins/validator.min.js',
        'merchant/createCashier.js'
    ], 'public/js/merchant/createCashier.js');

    /**
     * edit product merchant
     */
    mix.scripts([
        'plugins/selectize/selectize.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'plugins/bootstrap-touchspin/bootstrap.touchspin.min.js',
        'plugins/validator.min.js',
        'merchant/editProduct.js'
    ], 'public/js/merchant/editProduct.js').styles([
        './resources/assets/js/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
        './resources/assets/js/plugins/selectize/selectize.css',
        './resources/assets/js/plugins/selectize/selectize.bootstrap3.css',
        './resources/assets/js/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css',
        'bootstrap-datepicker.standalone.min.css'
    ], 'public/css/merchant/editProduct.css');

    /**
     * merchant account setting
     */
    mix.scripts([
        'plugins/validator.min.js',
        'merchant/accountSetting.js'
    ], 'public/js/merchant/accountSetting.js')
        .styles([
            './resources/assets/js/plugins/bootstrap-fileinput/bootstrap-fileinput.css'
        ], 'public/css/merchant/accountSetting.css');

    /**
     * admin member index
     */
    mix.scripts([
        'admin/members.js'
    ], 'public/js/admin/members.js');

    /**
     * admin merchant index
     */
    mix.scripts([
        'admin/merchants.js'
    ], 'public/js/admin/merchants.js');

    /**
     * admin staffs index
     */

    mix.scripts([
        'admin/staffs.js'
    ], 'public/js/admin/staffs.js');

    /**
     * admin products
     */
    mix.scripts([
        'plugins/selectize/selectize.min.js',
        'plugins/vue-selectize.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'plugins/bootstrap-touchspin/bootstrap.touchspin.min.js',
        'admin/products.js'
    ], 'public/js/admin/products.js')
        .styles([
            './resources/assets/js/plugins/selectize/selectize.css',
            './resources/assets/js/plugins/selectize/selectize.bootstrap3.css',
            './resources/assets/js/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css',
            'bootstrap-datepicker.standalone.min.css'
        ], 'public/css/admin/products.css');

    /**
     * admin orders
     */
    mix.scripts(['admin/orders.js'], 'public/js/admin/orders.js');

    /**
     * admin shippings
     */
    mix.scripts(['admin/shippings.js'], 'public/js/admin/shippings.js');

    /**
     * reg merchant
     */
    mix.scripts([
        'plugins/validator.min.js',
        'plugins/bootstrap-fileinput/bootstrap-fileinput.js',
        'plugins/selectize/selectize.min.js',
        'plugins/vue-selectize.min.js',
        'reg/regmerchant.js'
    ], 'public/js/reg/regmerchant.js')
        .styles([
            './resources/assets/js/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
            './resources/assets/js/plugins/selectize/selectize.css',
            './resources/assets/js/plugins/selectize/selectize.bootstrap3.css',
        ], 'public/css/reg/regmerchant.css');


    /**
     * Dashboard Admin
     */
    mix.scripts([
        'plugins/morris/raphael-min.js',
        'plugins/morris/morris.min.js',
        'admin/dashboard.js'
    ], 'public/js/admin/dashboard.js')
        .styles('./resources/assets/js/plugins/morris/morris.css', 'public/css/admin/dashboard.css');

    /**
     * edit member admin
     */
    mix.scripts([
        'plugins/validator.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'admin/editMember.js'
    ], 'public/js/admin/editmember.js')
        .styles([
            'bootstrap-datepicker.standalone.min.css'
        ], 'public/css/admin/editmember.css');

    /**
     * edit staff admin
     */
    mix.scripts([
        'plugins/validator.min.js',
        'admin/editStaff.js'
    ], 'public/js/admin/editstaff.js');


    /**
     * create staff admin
     */
    mix.scripts([
        'plugins/validator.min.js',
        'admin/createStaff.js'
    ], 'public/js/admin/createstaff.js');

    /**
     * edit merchant admin
     */
    mix.scripts([
        'plugins/validator.min.js',
        'admin/editMerchant.js'
    ], 'public/js/admin/editmerchant.js')
        .styles([
            './resources/assets/js/plugins/bootstrap-fileinput/bootstrap-fileinput.css'
        ], 'public/css/admin/editmerchant.css');

    /**
     * Create Product admin
     */
    mix.scripts([
        'plugins/bootstrap-fileinput/bootstrap-fileinput.js',
        'plugins/selectize/selectize.min.js',
        'plugins/vue-selectize.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'plugins/bootstrap-touchspin/bootstrap.touchspin.min.js',
        'plugins/validator.min.js',
        'admin/createProduct.js'
    ], 'public/js/admin/createProduct.js')
        .styles([
            './resources/assets/js/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
            './resources/assets/js/plugins/selectize/selectize.css',
            './resources/assets/js/plugins/selectize/selectize.bootstrap3.css',
            './resources/assets/js/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css',
            'bootstrap-datepicker.standalone.min.css'
        ], 'public/css/admin/createProduct.css');

    /**
     * edit product admin
     */
    mix.scripts([
        'plugins/selectize/selectize.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'plugins/bootstrap-touchspin/bootstrap.touchspin.min.js',
        'plugins/validator.min.js',
        'admin/editProduct.js'
    ], 'public/js/admin/editProduct.js').styles([
        './resources/assets/js/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
        './resources/assets/js/plugins/selectize/selectize.css',
        './resources/assets/js/plugins/selectize/selectize.bootstrap3.css',
        './resources/assets/js/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css',
        'bootstrap-datepicker.standalone.min.css'
    ], 'public/css/admin/editProduct.css');

    /**
     * demografi
     */
    mix.scripts([
        'plugins/morris/raphael-min.js',
        'plugins/morris/morris.min.js',
        'admin/demografi.js'
    ], 'public/js/admin/demografi.js')
        .styles([
            './resources/assets/js/plugins/morris/morris.css'
        ], 'public/css/admin/demografi.css');

    /**
     * report admin
     */

    mix.scripts([
        'plugins/morris/raphael-min.js',
        'plugins/morris/morris.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'admin/report.js'
    ], 'public/js/admin/report.js')
        .styles(['./resources/assets/js/plugins/morris/morris.css'], 'public/css/admin/report.css');

    /**
     * slider admin
     */
    mix.scripts([
        'admin/slider.js'
    ], 'public/js/admin/slider.js');

    /**
     * coupons admin
     */
    mix.scripts([
        'plugins/bootstrap-datepicker.min.js',
        'admin/coupons.js'
    ], 'public/js/admin/coupons.js')
        .styles([
            'bootstrap-datepicker.standalone.min.css'
        ], 'public/css/admin/coupons.css');

    /**
     * admin guests
     */
    mix.scripts([
        'admin/guests.js'
    ], 'public/js/admin/guests.js');

    /**
     * admin cashiers
     */
    mix.scripts([
        'admin/cashiers.js'
    ], 'public/js/admin/cashiers.js');

    /**
     * edit cashier
     */
    mix.scripts([
        'plugins/validator.min.js',
        'admin/editCashier.js'
    ], 'public/js/admin/editcashier.js');

    /**
     * demografi mercant
     */
    mix.scripts([
        'plugins/morris/raphael-min.js',
        'plugins/morris/morris.min.js',
        'merchant/demografi.js'
    ], 'public/js/merchant/demografi.js')
        .styles([
            './resources/assets/js/plugins/morris/morris.css'
        ], 'public/css/merchant/demografi.css');

    /**
     * currency admin
     */
    mix.scripts([
        'admin/currency.js'
    ], 'public/js/admin/currency.js');

    /**
     * Dashboard Purchase
     */
    mix.scripts([
        'plugins/morris/raphael-min.js',
        'plugins/morris/morris.min.js',
        'purchase/dashboard.js'
    ], 'public/js/purchase/dashboard.js')
        .styles(
            './resources/assets/js/plugins/morris/morris.css',
            'public/css/purchase/dashboard.css'
        );


    /**
     * staff orders
     */
    mix.scripts(
        ['purchase/orders.js'],
        'public/js/purchase/orders.js'
    );

    /**
     * Dashboard inventory
     */
    mix.scripts([
        'plugins/morris/raphael-min.js',
        'plugins/morris/morris.min.js',
        'inventory/dashboard.js'
    ], 'public/js/inventory/dashboard.js')
        .styles(
            './resources/assets/js/plugins/morris/morris.css',
            'public/css/inventory/dashboard.css'
        );

    /**
     * invoice merchant
     */
    mix.styles('invoice-2.css', 'public/css/invoice.css');
    /**
     * staff products
     */
    mix.scripts([
        'plugins/selectize/selectize.min.js',
        'plugins/vue-selectize.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'plugins/bootstrap-touchspin/bootstrap.touchspin.min.js',
        'inventory/products.js'
    ], 'public/js/inventory/products.js')
        .styles([
            './resources/assets/js/plugins/selectize/selectize.css',
            './resources/assets/js/plugins/selectize/selectize.bootstrap3.css',
            './resources/assets/js/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css',
            'bootstrap-datepicker.standalone.min.css'
        ], 'public/css/inventory/products.css');

    /**
     * edit product staff
     */
    mix.scripts([
        'plugins/selectize/selectize.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'plugins/bootstrap-touchspin/bootstrap.touchspin.min.js',
        'plugins/validator.min.js',
        'inventory/editproduct.js'
    ], 'public/js/inventory/editProduct.js').styles([
        './resources/assets/js/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
        './resources/assets/js/plugins/selectize/selectize.css',
        './resources/assets/js/plugins/selectize/selectize.bootstrap3.css',
        './resources/assets/js/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css',
        'bootstrap-datepicker.standalone.min.css'
    ], 'public/css/inventory/editProduct.css');

    /**
     * Create Product admin
     */
    mix.scripts([
        'plugins/bootstrap-fileinput/bootstrap-fileinput.js',
        'plugins/selectize/selectize.min.js',
        'plugins/vue-selectize.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'plugins/bootstrap-touchspin/bootstrap.touchspin.min.js',
        'plugins/validator.min.js',
        'inventory/createProduct.js'
    ], 'public/js/inventory/createProduct.js')
        .styles([
            './resources/assets/js/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
            './resources/assets/js/plugins/selectize/selectize.css',
            './resources/assets/js/plugins/selectize/selectize.bootstrap3.css',
            './resources/assets/js/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css',
            'bootstrap-datepicker.standalone.min.css'
        ], 'public/css/inventory/createProduct.css');

    /**
     * Registration token
     */
    mix.scripts([
        'plugins/bootstrap-datepicker.min.js',
        'admin/tokens.js'
    ], 'public/js/admin/tokens.js')
        .styles([
            'bootstrap-datepicker.standalone.min.css'
        ], 'public/css/admin/tokens.css');

    /**
     * Dashboard shipping
     */
    mix.scripts([
        'plugins/morris/raphael-min.js',
        'plugins/morris/morris.min.js',
        'shipping/dashboard.js'
    ], 'public/js/shipping/dashboard.js')
        .styles(
            './resources/assets/js/plugins/morris/morris.css',
            'public/css/shipping/dashboard.css'
        );


    /**
     * staff shippings
     */
    mix.scripts(['shipping/shippings.js'], 'public/js/shipping/shippings.js');

    /**
     * Dashboard finance
     */
    mix.scripts([
        'plugins/morris/raphael-min.js',
        'plugins/morris/morris.min.js',
        'finance/dashboard.js'
    ], 'public/js/finance/dashboard.js')
        .styles(
            './resources/assets/js/plugins/morris/morris.css',
            'public/css/finance/dashboard.css'
        );

    /**
     * report staff
     */
    mix.scripts([
        'plugins/morris/raphael-min.js',
        'plugins/morris/morris.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'finance/report.js'
    ], 'public/js/finance/report.js')
        .styles([
                './resources/assets/js/plugins/morris/morris.css'
            ],
            'public/css/finance/report.css'
        );

    /**
     * currency staff
     */
    mix.scripts([
        'finance/currency.js'
    ], 'public/js/finance/currency.js');

    /**
     * error page
     */
    mix.scripts([
        'plugins/js/jquery.min.js',
        'plugins/js/bootstrap.js',
        'plugins/js/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
        'plugins/js/jquery-slimscroll/jquery.slimscroll.min.js',
        'plugins/js/jquery.blockui.min.js',
        'plugins/js/jquery.cokie.min.js',
        'plugins/js/uniform/jquery.uniform.min.js',
        'plugins/js/bootstrap-switch/js/bootstrap-switch.min.js',
        'app.js',
        'layout.js',
    ], 'public/js/error.js');
    mix.sass('bootstrap.scss', 'public/css/bootstrap-error.css')
        .sass('layouts/layout7/layout.scss', 'public/css/layout-error.css')
        .sass([
            'global/components-rounded.scss',
            'global/plugins.scss',
            'pages/error.scss'
        ], 'public/css/components-error.css')
        .styles([
            './public/css/bootstrap-error.css',
            './resources/assets/js/plugins/css/font-awesome.css',
            './resources/assets/js/plugins/css/simple-line-icons.css',
            './resources/assets/js/plugins/css/uniform.default.css',
            './public/css/layout-error.css',
            './public/css/components-error.css'
        ], 'public/css/error.css');

    /**
     * main shop
     */
    mix.styles([
        './public/css/bootstrap.css',
        './resources/assets/js/plugins/css/font-awesome.css',
        './resources/assets/js/plugins/bootstrap-toastr/toastr.css',
        './resources/assets/js/plugins/pace/minimal.css',
        './resources/assets/js/plugins/alert/sweetalert.css',
        './resources/assets/js/plugins/slick/slick.css',
        './resources/assets/js/plugins/slick/slick-theme.css',
        './resources/assets/js/plugins/selectize/selectize.css',
        './resources/assets/js/plugins/selectize/selectize.bootstrap3.css',
        'plugins/animate.css',
        './resources/assets/js/plugins/css/uniform.default.css',
        './resources/assets/js/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css',
        'bootstrap.vertical-tabs.min.css',
        'shop/components.css',
        'shop/style-shop.css',
        'shop/style.css',
        'shop/style-responsive.css',
        'shop/red.css',
        'shop/header.css',
        'shop/custom.css'
    ], 'public/shop-assets/css/main.css')
        .scripts([
            'plugins/pace/pace.min.js',
            'shop/jquery.min.js',
            'plugins/js/jquery-migrate.min.js',
            'plugins/js/bootstrap.js',
            'plugins/js/uniform/jquery.uniform.min.js',
            'plugins/bootstrap-toastr/toastr.js',
            'plugins/jquery-slimscroll/jquery.slimscroll.min.js',
            'plugins/js/vue.js',
            'plugins/selectize/selectize.min.js',
            'plugins/vue-selectize.min.js',
            'plugins/fastclick.js',
            'plugins/alert/sweetalert.min.js',
            'plugins/accounting.min.js',
            'plugins/js/moment.min.js',
            'plugins/zoom-master/jquery.zoom.min.js',
            'plugins/bootstrap-touchspin/bootstrap.touchspin.min.js',
            'plugins/slick/slick.min.js',
            'shop/header.js',
            'shop/layout.js',
            'filters.js',
            'shop/back-to-top.js',
        ], 'public/shop-assets/js/main.js');

    /**
     * shop default
     */

    mix.scripts('shop/shop.js', 'public/shop-assets/js/shop.js');

    /**
     * shop front page
     */
    mix.scripts('shop/shop-front.js', 'public/shop-assets/js/shop-front.js');

    /**
     * shop single product
     */
    mix.scripts('shop/shop-single.js', 'public/shop-assets/js/shop-single.js');


    /**
     * shop single product
     */
    mix.scripts([
        'plugins/validator.min.js',
        'shop/shop-login.js'
    ], 'public/shop-assets/js/shop-login.js');

    /**
     * shop account activation
     */
    mix.scripts('shop/shop-activation.js', 'public/shop-assets/js/shop-account-activation.js');

    /**
     * shop brand page
     */
    mix.scripts('shop/shop-brand.js', 'public/shop-assets/js/shop-brand-page.js');

    /**
     * shop cart
     */
    mix.scripts('shop/shop-cart.js', 'public/shop-assets/js/shop-cart.js');

    /**
     * shop checkout
     */
    mix.scripts([
        'plugins/validator.min.js',
        'shop/shop-checkout.js'
    ], 'public/shop-assets/js/shop-checkout.js');

    /**
     * admin obid financial Report
     */
    mix.scripts([
        'plugins/morris/raphael-min.js',
        'plugins/morris/morris.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'admin/obidReport.js'
    ], 'public/js/admin/obidreport.js')
        .styles(['./resources/assets/js/plugins/morris/morris.css'], 'public/css/admin/obidreport.css');

    /**
     * shop edit profile
     */
    mix.scripts([
        'plugins/validator.min.js',
        'shop/shop-account.js'
    ], 'public/shop-assets/js/shop-account.js');

    /**
     * coupons merchant
     */
    mix.scripts([
        'plugins/bootstrap-datepicker.min.js',
        'merchant/coupon.js'
    ], 'public/js/merchant/coupons.js')
        .styles([
            'bootstrap-datepicker.standalone.min.css'
        ], 'public/css/merchant/coupons.css');

    /**
     * edit cashier merchant
     */
    mix.scripts([
        'plugins/validator.min.js',
        'merchant/editcashier.js'
    ], 'public/js/merchant/editcashier.js');

    /**
     * pos.js
     */
    mix.scripts(['merchant/staffs.js'], 'public/js/merchant/staffs.js');

    /**
     * create staff
     */
    mix.scripts([
        'plugins/validator.min.js',
        'merchant/createstaff.js'
    ], 'public/js/merchant/createstaff.js');

    /**
     * edit cashier merchant
     */
    mix.scripts([
        'plugins/validator.min.js',
        'merchant/editstaff.js'
    ], 'public/js/merchant/editstaff.js');

    /**
     * Dashboard staff merchant
     */
    mix.scripts([
        'plugins/morris/raphael-min.js',
        'plugins/morris/morris.min.js',
        'staff/dashboard.js'
    ], 'public/js/staffmerchant/dashboard.js')
        .styles(
            './resources/assets/js/plugins/morris/morris.css',
            'public/css/staffmerchant/dashboard.css'
        );


    /**
     * index products staff merchant
     *
     * */
    mix.scripts([
        'plugins/selectize/selectize.min.js',
        'plugins/vue-selectize.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'plugins/bootstrap-touchspin/bootstrap.touchspin.min.js',
        'staff/products.js'
    ], 'public/js/staffmerchant/products.js')
        .styles([
            './resources/assets/js/plugins/selectize/selectize.css',
            './resources/assets/js/plugins/selectize/selectize.bootstrap3.css',
            './resources/assets/js/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css',
            'bootstrap-datepicker.standalone.min.css'
        ], 'public/css/staffmerchant/products.css');

    /**
     * Create Product Staff Merchant
     */
    mix.scripts([
        'plugins/bootstrap-fileinput/bootstrap-fileinput.js',
        'plugins/selectize/selectize.min.js',
        'plugins/vue-selectize.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'plugins/bootstrap-touchspin/bootstrap.touchspin.min.js',
        'plugins/validator.min.js',
        'staff/createproduct.js'
    ], 'public/js/staffmerchant/createproduct.js')
        .styles([
            './resources/assets/js/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
            './resources/assets/js/plugins/selectize/selectize.css',
            './resources/assets/js/plugins/selectize/selectize.bootstrap3.css',
            './resources/assets/js/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css',
            'bootstrap-datepicker.standalone.min.css'
        ], 'public/css/staffmerchant/createproduct.css');


    /**
     * edit product staff merchant
     */
    mix.scripts([
        'plugins/selectize/selectize.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'plugins/bootstrap-touchspin/bootstrap.touchspin.min.js',
        'plugins/validator.min.js',
        'staff/editproduct.js'
    ], 'public/js/staffmerchant/editproduct.js').styles([
        './resources/assets/js/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
        './resources/assets/js/plugins/selectize/selectize.css',
        './resources/assets/js/plugins/selectize/selectize.bootstrap3.css',
        './resources/assets/js/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css',
        'bootstrap-datepicker.standalone.min.css'
    ], 'public/css/staffmerchant/editproduct.css');

    /**
     * obidReport
     */
    mix.scripts([
        'plugins/morris/raphael-min.js',
        'plugins/morris/morris.min.js',
        'plugins/bootstrap-datepicker.min.js',
        'finance/obidreport.js'
    ], 'public/js/finance/obidreport.js')
        .styles(['./resources/assets/js/plugins/morris/morris.css'],
            'public/css/finance/obidreport.css');

});

var newShop = './resources/assets/new-shop/';
elixir(function (mix) {
    mix.styles([
        newShop + 'css/fonts.css',
        newShop + 'css/bootstrap.css',
        newShop + 'css/owl.carousel.css',
        newShop + 'css/font-awesome.css',
        newShop + 'css/animate.css',
        newShop + 'css/chosen.css',
        newShop + 'js/fancybox/source/jquery.fancybox.css',
        newShop + 'js/fancybox/source/helpers/jquery.fancybox-thumbs.css',
        newShop + 'js/arcticmodal/jquery.arcticmodal.css'
    ], 'public/new-shop/css/main.css');

    mix.webpack('new-shop/shop.js', 'public/new-shop/js/shop.js');
});